package com.irplus.dao.menu;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.irplus.dao.hibernate.entities.IrMMenus;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.StatusIdInfo;
import com.irplus.dto.menu.AddMenu;
import com.irplus.util.BusinessException;
import com.irplus.util.IRPlusConstants;

@Component
@Transactional

public class IrMMenusDaoImpl implements IrMMenusDao{

	private static final Log log = LogFactory.getLog(IrMMenusDaoImpl.class);

	@Autowired
	private SessionFactory sessionFactory;
	
	public Session getMyCurrentSession(){
		 Session session = null;
		 try 
		 {    
		     session = sessionFactory.getCurrentSession();
		 } 
		 catch (HibernateException e) 
		 {    
			 log.error("Exception Raised at MenusDaoImpl"+e);
		 }
		 return session;
	}

	@Override
	public IRPlusResponseDetails createMenu(AddMenu menuInfo) throws BusinessException {
		
		log.info("Inside IrMMenusDaoImpl");
		
		IRPlusResponseDetails menuResponseDetails=new IRPlusResponseDetails();
		
		try {
			
			IrMMenus irMMenus=new IrMMenus();
			
			irMMenus.setMenuname(menuInfo.getMenuName());
			irMMenus.setModulepath(menuInfo.getModulepath());
			irMMenus.setDescription(menuInfo.getDescription());
			irMMenus.setMenuicon(menuInfo.getMenuicon());
			irMMenus.setModulepath(menuInfo.getModulepath());
			
			irMMenus.setSortingorder(menuInfo.getSortingorder());
			
			irMMenus.setIsmodule(menuInfo.getIsmodule());
			irMMenus.setIsactive(menuInfo.getStatus());
			
			irMMenus.setIsmodule(menuInfo.getIsmodule());
			irMMenus.setUserid(menuInfo.getUserid());		
			
			getMyCurrentSession().save(irMMenus);
			
		//	errM
			menuResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			menuResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
			menuResponseDetails.setValidationSuccess(true);
			//log.debug("");
		}
		catch (Exception e) {
			log.error("Exception in create Menu",e);
			menuResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			menuResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			throw new HibernateException(e);
		}
		
		return menuResponseDetails;
	}

/*	Create menu without duplicate name */
	
	public IRPlusResponseDetails createMenuWithoutDpct(AddMenu menuInfo) throws BusinessException {
		
		log.info("Inside IrMMenusDaoImpl");
		
		IRPlusResponseDetails menuResponseDetails=new IRPlusResponseDetails();
		
		Criteria criteria = getMyCurrentSession().createCriteria(IrMMenus.class);
		
		List<AddMenu> listInfo = new ArrayList<AddMenu>();
		
		List<IrMMenus> list = criteria.list();
		
		List<String> errorMsg = new ArrayList<String>(); 
 		
		boolean flag = false;
 		
 		IrMMenus irMMenus=new IrMMenus();
	
 		try{
		
 			if(list!= null && !list.isEmpty()){
		
					for (IrMMenus irMMenus1 : list){
						
						if(menuInfo.getMenuName().equalsIgnoreCase(irMMenus1.getMenuname())){
							flag = true;
							break;
						}
					}
						if(flag){
							
							menuResponseDetails.setValidationSuccess(false);											
							errorMsg.add("Validation Error: Duplicate Name Entered");							
							menuResponseDetails.setErrMsgs(errorMsg);				
							menuResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
							menuResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
						}else{
							
						
							
							irMMenus.setMenuname(menuInfo.getMenuName());
							irMMenus.setModulepath(menuInfo.getModulepath());
							irMMenus.setDescription(menuInfo.getDescription());
							irMMenus.setMenuicon(menuInfo.getMenuicon());
							irMMenus.setModulepath(menuInfo.getModulepath());
							
							irMMenus.setSortingorder(menuInfo.getSortingorder());
							
							irMMenus.setIsmodule(menuInfo.getIsmodule());
							irMMenus.setIsactive(menuInfo.getStatus());
							
							irMMenus.setIsmodule(menuInfo.getIsmodule());
							irMMenus.setUserid(menuInfo.getUserid());		
							
							getMyCurrentSession().save(irMMenus);errorMsg.add("Menu successfully saved DB : Success");
							
							menuResponseDetails.setErrMsgs(errorMsg);
							menuResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
							menuResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
							menuResponseDetails.setValidationSuccess(true);
						}		
			
		}else{
			
			irMMenus.setMenuname(menuInfo.getMenuName());
			irMMenus.setModulepath(menuInfo.getModulepath());
			irMMenus.setDescription(menuInfo.getDescription());
			irMMenus.setMenuicon(menuInfo.getMenuicon());
			irMMenus.setModulepath(menuInfo.getModulepath());
			
			irMMenus.setSortingorder(menuInfo.getSortingorder());
			
			irMMenus.setIsmodule(menuInfo.getIsmodule());
			irMMenus.setIsactive(menuInfo.getStatus());
			
			irMMenus.setIsmodule(menuInfo.getIsmodule());
			irMMenus.setUserid(menuInfo.getUserid());		
			
			getMyCurrentSession().save(irMMenus);errorMsg.add("Menu successfully saved DB : Success");
			
			menuResponseDetails.setErrMsgs(errorMsg);
			menuResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			menuResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
			menuResponseDetails.setValidationSuccess(true);
			
		}
		}catch (Exception e) {
			log.error("Exception in create Menu",e);
			menuResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			menuResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			throw new HibernateException(e);
		}
		return menuResponseDetails;
	}
	
	// Isactive update
	
	
	// Menuid based getting the menu from db
	@Override
	public IRPlusResponseDetails getMenu(String menuid) throws BusinessException {
		
		IRPlusResponseDetails menuResponseDetails=new IRPlusResponseDetails();		
		
		Query query = getMyCurrentSession().createQuery("from IrMMenus menus where menus.menuid =:MenuId");
		
		List<String> errMsg = new ArrayList<String>();
		
		query.setParameter("MenuId", Integer.parseInt(menuid));
		
		List<IrMMenus> menulist = (List<IrMMenus>)query.list();
		
		Iterator<IrMMenus> iterator=menulist.iterator();
		
		List<AddMenu> addMenuList = new ArrayList<>();
		
		AddMenu menuInfo=null;
		
		IrMMenus irMMenus=null;
		
		try {
			while (iterator.hasNext()) {
				
				 irMMenus = (IrMMenus) iterator.next();
				
				menuInfo=new AddMenu();
				
				menuInfo.setMenuid(irMMenus.getMenuid());
				menuInfo.setMenuicon(irMMenus.getMenuicon());
				menuInfo.setMenuName((String) irMMenus.getMenuname());
				menuInfo.setDescription((String) irMMenus.getDescription());
				menuInfo.setSortingorder(irMMenus.getSortingorder());
				menuInfo.setModulepath((String)irMMenus.getModulepath());
				
				menuInfo.setStatus(irMMenus.getIsactive());
				menuInfo.setIsmodule(irMMenus.getIsmodule());
				
				menuInfo.setUserid(irMMenus.getUserid());
				menuInfo.setModifieddate(irMMenus.getModifieddate());
				menuInfo.setCreateddate(irMMenus.getCreateddate());
				
				addMenuList.add(menuInfo);
				menuResponseDetails.setMenu(menuInfo);
			}
					if(!addMenuList.isEmpty()){
						
						log.debug("get successful, menu instance found");
						menuResponseDetails.setMenus(addMenuList);
						errMsg.add("Success Menu :: MenuId found In Db");
						menuResponseDetails.setErrMsgs(errMsg);
						menuResponseDetails.setValidationSuccess(true);		
						menuResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
						menuResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
					}
					else{
						log.debug("get successful, no menu instance found");
						errMsg.add("Error Menu :: MenuId not found In Db");
						menuResponseDetails.setErrMsgs(errMsg);						
						menuResponseDetails.setValidationSuccess(false);
						menuResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
						menuResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
					}
					
		}catch (Exception e) {
			log.error("Exception in find Menu",e);
			menuResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			menuResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			throw new HibernateException(e);
		}
		return menuResponseDetails;
	}

	/*	isactive status updating */
	public IRPlusResponseDetails updateMenuStatusPrm(StatusIdInfo siInfo) throws BusinessException {

		log.info("Inside IrMMenusDaoImpl");
		
		IRPlusResponseDetails menuResponseDetails=new IRPlusResponseDetails();
		
		Integer menuInfoId=siInfo.getId();
		
		//this.getMenu(menuInfoId);
		
		IrMMenus irmenuDup = new IrMMenus();
		IrMMenus irmenuOrg=null;
		List<String> errMsg = new ArrayList<String>();
								
					try {
								
					Query query = getMyCurrentSession().createQuery("from IrMMenus where menuid = :menuInfoId");
				//		Query query = getMyCurrentSession().get(IrMMenus.class , menuInfoId);       
						query.setParameter("menuInfoId", menuInfoId);
			
						List<IrMMenus> irlist = (List<IrMMenus>) query.list();  //list out the items
												
						Iterator<IrMMenus> list_iterator =null;
							
						if(!irlist.isEmpty()){			
							
							list_iterator= irlist.iterator();
							
							while (list_iterator.hasNext()) {
								
								irmenuOrg = (IrMMenus) list_iterator.next();
								irmenuDup = irmenuOrg;					
								irmenuOrg.setMenuname(irmenuDup.getMenuname());
								irmenuOrg.setMenuicon(irmenuDup.getMenuicon());
								irmenuOrg.setModulepath(irmenuDup.getModulepath());
								irmenuOrg.setDescription((String)irmenuDup.getDescription());
								irmenuOrg.setSortingorder(irmenuDup.getSortingorder());
								irmenuOrg.setModifieddate(irmenuDup.getModifieddate());
					
								irmenuOrg.setIsmodule(irmenuDup.getIsmodule());
								irmenuOrg.setUserid(irmenuDup.getUserid());
								irmenuOrg.setIsactive(siInfo.getStatus());
								
							}
							
							getMyCurrentSession().update(irmenuOrg);    //update the items
							
							menuResponseDetails.setValidationSuccess(true);
							errMsg.add(" Success : Successfully updated Status ");
							menuResponseDetails.setErrMsgs(errMsg);
							menuResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
							menuResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
							}
							else
							{
							errMsg.add(" Error : Not updated Status ");
							menuResponseDetails.setErrMsgs(errMsg);
							menuResponseDetails.setValidationSuccess(false);
							menuResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
							menuResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
						}
						
							} catch (Exception e) {
					log.error("Exception in update Menu",e);
					menuResponseDetails.setValidationSuccess(false);
					menuResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
					menuResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
					throw new HibernateException(e);
				}
		return menuResponseDetails;
	}

//close	
	
	@Override
	public IRPlusResponseDetails updateMenu(AddMenu menuInfo) throws BusinessException {

		log.info("Inside IrMMenusDaoImpl");
		
		IRPlusResponseDetails menuResponseDetails=new IRPlusResponseDetails();
		
		Integer menuInfoId=menuInfo.getMenuid();
		
		List<String> errMsg = new ArrayList<String>();
		
		//this.getMenu(menuInfoId);
		
		IrMMenus irmenu=null;
		
		if(menuInfoId!= null) {
						
		try {
		
			//here may be 'serializable no ask' not 'menuid' carefull

			Query query = getMyCurrentSession().createQuery("from IrMMenus where menuid = :menuInfoId");
			
			query.setParameter("menuInfoId", menuInfoId);

	//		Class<IrMMenus> irmenu123 =query.getClass();
			
			 List<IrMMenus> irlist =  query.list();  //list out the items
									
	// adding listing strt
			 
			 Criteria criteria = getMyCurrentSession().createCriteria(IrMMenus.class);
			
			List<IrMMenus> irmenusAll_list =  criteria.list();
			 
			Iterator<IrMMenus> list_iterator_all = irmenusAll_list.iterator();
			
	// adding listing close
			
			Iterator<IrMMenus> list_iterator =null;
				
			if(!irlist.isEmpty()){			
				
				list_iterator= irlist.iterator();
				
				boolean flag=false;
				
				while (list_iterator_all.hasNext()) {
					
					IrMMenus irMMenus = (IrMMenus) list_iterator_all.next();
					
					if(menuInfo.getMenuid()==irMMenus.getMenuid()){         // raju == raju
						if(menuInfo.getMenuName().equalsIgnoreCase(irMMenus.getMenuname())){
							flag=false;
						}					
					}
					if(menuInfo.getMenuid()!=irMMenus.getMenuid()){         // raju == raju
						if(menuInfo.getMenuName().equalsIgnoreCase(irMMenus.getMenuname())){
							flag=true;
						}					
					}
					
				}
						
						if(flag){
							errMsg.add("Validation Error :: Duplicate Name Entered : "+menuInfo.getMenuName());
							menuResponseDetails.setErrMsgs(errMsg);
							menuResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
							menuResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
							
						}else{
							while (list_iterator.hasNext()) {
								
								irmenu = (IrMMenus) list_iterator.next();
															
								irmenu.setMenuname(menuInfo.getMenuName());
								irmenu.setMenuicon(menuInfo.getMenuicon());
								irmenu.setModulepath(menuInfo.getModulepath());
								irmenu.setDescription(menuInfo.getDescription());
								irmenu.setSortingorder(menuInfo.getSortingorder());
								irmenu.setModifieddate(menuInfo.getModifieddate());
					
								irmenu.setIsmodule(menuInfo.getIsmodule());
								irmenu.setUserid(menuInfo.getUserid());
								irmenu.setIsactive(menuInfo.getStatus());
								
							}
							
							getMyCurrentSession().update(irmenu);    //update the items
							errMsg.add("Success :: Menu Updated : "+menuInfo.getMenuName());
							menuResponseDetails.setErrMsgs(errMsg);
							menuResponseDetails.setValidationSuccess(true);
							menuResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
							menuResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
						}
				
				}
				else
				{
					
				menuResponseDetails.setValidationSuccess(false);
				menuResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
				menuResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
			}
			
		} catch (Exception e) {
			log.error("Exception in update Menu",e);
			menuResponseDetails.setValidationSuccess(false);
			menuResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			menuResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			throw new HibernateException(e);
		}
		}
		else {
			menuResponseDetails.setValidationSuccess(false);
			menuResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
			menuResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
		}
		
		return menuResponseDetails;
	}

	@Override
	public IRPlusResponseDetails deleteMenu(String menuid) throws BusinessException {
		
		log.info("Inside IrMMenusDaoImpl");
		log.debug("deleting IrMMenus instance");
		
		IRPlusResponseDetails menuResponseDetails=new IRPlusResponseDetails();
		List<String> errMsg = new ArrayList<String>();
		IrMMenus irmenu=null;
		
		try {
			
			Query query = getMyCurrentSession().createQuery("delete IrMMenus m where m.menuid = :menuId");
			
			query.setParameter("menuId",Integer.parseInt(menuid));
			
			int result = query.executeUpdate();
			
			
			if (result==0) {
				errMsg.add("Error :Not found id  :: Not Deleted row");
				menuResponseDetails.setErrMsgs(errMsg);				
				menuResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
				menuResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
			} else if(result==1){
				log.debug("One menu is delted result "+result + " : record is deleted");
				menuResponseDetails.setValidationSuccess(true);
				errMsg.add("Deleted row");
				menuResponseDetails.setErrMsgs(errMsg);
				menuResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
				menuResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
			}
			
			
		} catch (Exception e) {
			
			log.error("Exception in delete Menu",e);
			menuResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			menuResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			log.error("delete menu failed", e);
			throw new HibernateException(e);
		}
		return menuResponseDetails;
	}	
		
	public IRPlusResponseDetails findAllMenus() throws BusinessException{
		
		log.info("Inside IrMMenusDaoImpl getAllmenus()");
		log.debug("retriving IrMMenus instance");
		
		IRPlusResponseDetails menuResponseDetails=new IRPlusResponseDetails();
		
		IrMMenus irMMenus=null;
		
		System.out.println("findAllMenus() calling");
		
		//Criteria criteria = getMyCurrentSession().createCriteria(IrMMenus.class);
		
		
		Query menus = getMyCurrentSession().createQuery("from IrMMenus irMMenus where irMMenus.isactive!=?");
		
		menus.setParameter(0,2);
		
		List<AddMenu> listInfo=new ArrayList<AddMenu>();
		
		List<IrMMenus> list=menus.list();
		 
			 Iterator<IrMMenus>	iterate	= list.iterator();
			 int i=0;
			 if(!list.isEmpty()) {
			 try {
					 while (iterate.hasNext()) {
				 			irMMenus = (IrMMenus) iterate.next();
				 			
				 			AddMenu menuInfo=new AddMenu();
				 			
				 			menuInfo.setMenuid(irMMenus.getMenuid());
				 			menuInfo.setMenuName( irMMenus.getMenuname());
				 			menuInfo.setMenuicon(irMMenus.getMenuicon());
				 			menuInfo.setModulepath(irMMenus.getModulepath());
				 			menuInfo.setSortingorder(irMMenus.getSortingorder());
				 			menuInfo.setDescription((String)irMMenus.getDescription());				 			
				 			menuInfo.setCreateddate(irMMenus.getCreateddate());
				 			menuInfo.setModifieddate(irMMenus.getModifieddate());						 					 			
				 			menuInfo.setIsmodule(irMMenus.getIsmodule());				 			
				 			menuInfo.setUserid(irMMenus.getUserid());
				 			menuInfo.setStatus(irMMenus.getIsactive());
				 			
				 			listInfo.add(menuInfo);
				 			log.debug(" Record is retrived ::"+ irMMenus.getModulepath());				 			
						}					 	
					 
						 if(!listInfo.isEmpty()) {
							 
						 menuResponseDetails.setMenus(listInfo);
						 menuResponseDetails.setValidationSuccess(true);
						 
						 menuResponseDetails.setRecordsTotal(listInfo.size());
						 menuResponseDetails.setRecordsFiltered(listInfo.size());
						 
						 menuResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
						 menuResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
						 log.debug("Inside IrMMenusDaoImpl  :: Retriving menus is succeefully");
					 	}
					 	else
					 	{
					 		log.debug("Inside IrMMenusDaoImpl  :: Retriving menus is empty");
					 		menuResponseDetails.setValidationSuccess(false);
							menuResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
							menuResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
					 	}
						 
			}catch (Exception e) {
				
				log.error("Exception in create Menu",e);
				menuResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
				menuResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				
				throw new HibernateException(e);
			} 
			 }else {
				 	log.warn("Menu items are empty");
				 	log.debug("Menu items are empty");
					menuResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
					menuResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);					
			 }
		return menuResponseDetails;
	}
	
	
public IRPlusResponseDetails getActiveMenus() throws BusinessException{
		
		
		IRPlusResponseDetails menuResponseDetails=new IRPlusResponseDetails();
		
		IrMMenus irMMenus=null;
		
		
		Query menus = getMyCurrentSession().createQuery("from IrMMenus menus where menus.isactive=1");
		
	//	Criteria criteria = getMyCurrentSession().createCriteria(IrMMenus.class);
		
		List<AddMenu> listInfo=new ArrayList<AddMenu>();
		
		List<IrMMenus> list=menus.list();
		 
			 Iterator<IrMMenus>	iterate	= list.iterator();
			 int i=0;
			 if(!list.isEmpty()) {
			 try {
					 while (iterate.hasNext()) {
				 			irMMenus = (IrMMenus) iterate.next();
				 			
				 			AddMenu menuInfo=new AddMenu();
				 			
				 			menuInfo.setMenuid(irMMenus.getMenuid());
				 			menuInfo.setMenuName( irMMenus.getMenuname());
				 			menuInfo.setMenuicon(irMMenus.getMenuicon());
				 			menuInfo.setModulepath(irMMenus.getModulepath());
				 			menuInfo.setSortingorder(irMMenus.getSortingorder());
				 			menuInfo.setDescription((String)irMMenus.getDescription());				 			
				 			menuInfo.setCreateddate(irMMenus.getCreateddate());
				 			menuInfo.setModifieddate(irMMenus.getModifieddate());						 					 			
				 			menuInfo.setIsmodule(irMMenus.getIsmodule());				 			
				 			menuInfo.setUserid(irMMenus.getUserid());
				 			menuInfo.setStatus(irMMenus.getIsactive());
				 			
				 			listInfo.add(menuInfo);
				 			log.debug(" Record is retrived ::"+ irMMenus.getModulepath());				 			
						}					 	
					 
						 if(!listInfo.isEmpty()) {
							 
						 menuResponseDetails.setMenus(listInfo);
						 menuResponseDetails.setValidationSuccess(true);
						 
						 menuResponseDetails.setRecordsTotal(listInfo.size());
						 menuResponseDetails.setRecordsFiltered(listInfo.size());
						 
						 menuResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
						 menuResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
						 log.debug("Inside IrMMenusDaoImpl  :: Retriving menus is succeefully");
					 	}
					 	else
					 	{
					 		log.debug("Inside IrMMenusDaoImpl  :: Retriving menus is empty");
					 		menuResponseDetails.setValidationSuccess(false);
							menuResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
							menuResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
					 	}
						 
			}catch (Exception e) {
				
				log.error("Exception in create Menu",e);
				menuResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
				menuResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				
				throw new HibernateException(e);
			} 
			 }else {
				 	log.warn("Menu items are empty");
				 	log.debug("Menu items are empty");
					menuResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
					menuResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);					
			 }
		return menuResponseDetails;
	}
}