package com.irplus.dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.irplus.dao.hibernate.entities.IRSBankLicensing;
import com.irplus.dao.hibernate.entities.IRSLicense;
import com.irplus.dao.hibernate.entities.IRSSite;
import com.irplus.dao.hibernate.entities.IrMBusinessProcess;
import com.irplus.dao.hibernate.entities.IrMFileTypes;
import com.irplus.dao.hibernate.entities.IrMRoles;
import com.irplus.dao.hibernate.entities.IrMUsers;
import com.irplus.dao.hibernate.entities.IrSCustomerOtherlicensing;
import com.irplus.dao.hibernate.entities.IrSCustomerlicensing;
import com.irplus.dao.hibernate.entities.IrSSequence;
import com.irplus.dto.BankInfo;
import com.irplus.dto.BusinessProcessInfo;
import com.irplus.dto.CustomerBean;
import com.irplus.dto.FileTypeInfo;
import com.irplus.dto.IRMasterDataDao;
import com.irplus.dto.IRPlusBusinessProcess;
import com.irplus.dto.IRPlusFileTypes;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.LicenseInfo;
import com.irplus.dto.Sequence;
import com.irplus.dto.UserBean;
import com.irplus.util.BusinessException;
import com.irplus.util.IRPlusConstants;
import com.irplus.util.IRPlusUtil;

@Component

@Transactional
public class IRPlusDao {

	private static final Logger LOGGER = Logger.getLogger(IRPlusDao.class);

	@Autowired
	private SessionFactory sessionFactory;


	public IRPlusResponseDetails getMasterData(String siteId) 
	{
		LOGGER.info("Inside IRPlusDao");

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		Session session = null;

		session = sessionFactory.getCurrentSession();
		IRSSite iRSSite=null;
		Long licenseId = null;
		IrMBusinessProcess businessProcess = null;
		
		
		IrMFileTypes fileTypesData = null;

		BusinessProcessInfo processInfo = null;

		List<BusinessProcessInfo> processList = new ArrayList<BusinessProcessInfo>();

		FileTypeInfo fileTypeInfo = null;

		List<FileTypeInfo> fileTypes = new ArrayList<FileTypeInfo>();
		List<Sequence> bankSequence = new ArrayList<Sequence>();

		IRMasterDataDao masterData = new IRMasterDataDao();

		try
		{
			
			iRSSite = (IRSSite) session.get(IRSSite.class,new Long(siteId));
			
			if(iRSSite!=null){
				
				licenseId = iRSSite.getLicense().getLicenseId();
			}
			
			if(licenseId!=null){
				
				Query bp = session.createQuery("FROM IrMBusinessProcess businessProcess WHERE businessProcess.iRSLicense.licenseId=?");
				bp.setParameter(0,licenseId);
				List<IrMBusinessProcess> bplist = bp.list();
				if(bplist.size()>0){
					for(IrMBusinessProcess irMBusinessProcess:bplist){
						
						processInfo = new BusinessProcessInfo();

						processInfo.setBusinessProcessId(irMBusinessProcess.getBusinessProcessId());

						processInfo.setProcessName(irMBusinessProcess.getProcessName());

						processList.add(processInfo);
						
					}
					
				}
				Query fTypes = session.createQuery("FROM IrMFileTypes fileTypes WHERE fileTypes.iRSLicense.licenseId=?");
				fTypes.setParameter(0,licenseId);
				
				List<IrMFileTypes> fileTypeList = fTypes.list();
				
				if(fileTypeList.size()>0){
					
					for(IrMFileTypes irMFileTypes:fileTypeList){
						
						fileTypeInfo = new FileTypeInfo();

						fileTypeInfo.setFiletypeid(irMFileTypes.getFiletypeid());

						fileTypeInfo.setFiletypename(irMFileTypes.getFiletypename());

						fileTypes.add(fileTypeInfo);
				
				
					}
				}
				
				
			}
			
			
			
			
			/*Query query = session.createQuery("from IrMBusinessProcess bp where bp.isactive=1");

			List<IrMBusinessProcess> bplist = (List<IrMBusinessProcess>) query.list();

			Iterator<IrMBusinessProcess> bplistIterator = bplist.iterator();

			while (bplistIterator.hasNext())
			{
				businessProcess = bplistIterator.next();

				processInfo = new BusinessProcessInfo();

				processInfo.setBusinessProcessId(businessProcess.getBusinessProcessId());

				processInfo.setProcessName(businessProcess.getProcessName());

				processList.add(processInfo);
			}
			
			
			*/
			
			
			

			/*query = null;

			query = session.createQuery("from IrMFileTypes fileTyeps where fileTyeps.isactive=1");

			List<IrMFileTypes> ftypeList = (List<IrMFileTypes>) query.list();

			Iterator<IrMFileTypes> fileTypeItr = ftypeList.iterator();

			while (fileTypeItr.hasNext())
			{
				fileTypesData = fileTypeItr.next();

				fileTypeInfo = new FileTypeInfo();

				fileTypeInfo.setFiletypeid(fileTypesData.getFiletypeid());

				fileTypeInfo.setFiletypename(fileTypesData.getFiletypename());

				fileTypes.add(fileTypeInfo);
			}
			
			*/
			

		Query	query = session.createQuery("from IrSSequence sequence where sequence.sequenceId=1");

			List<IrSSequence> sequenceList = (List<IrSSequence>) query.list();
			
			
			for(IrSSequence irSSequence:sequenceList){
				Sequence sequence = new Sequence();
				
				sequence.setSequenceName(irSSequence.getSequenceName());
				sequence.setSequenceNumber(irSSequence.getSequenceNumber());
				bankSequence.add(sequence);
			}
			

		
			
			
			
			
			masterData.setBankFolderSequence(bankSequence);
			
			masterData.setBusPros(processList);

			masterData.setFileTypes(fileTypes);

			irPlusResponseDetails.setMasterData(masterData);

			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);

		}
		catch(Exception e)
		{
			LOGGER.error("Exception in getMasterData",e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			throw new HibernateException(e);
		}

		return irPlusResponseDetails;
	}

	public IRPlusResponseDetails getBankProcess(Long bankId) {
		LOGGER.info("Inside IRPlusDao");

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		Session session = null;

		session = sessionFactory.getCurrentSession();
		IRSSite iRSSite=null;
		Long licenseId = null;
		IrMBusinessProcess businessProcess = null;
		
		
		IrMFileTypes fileTypesData = null;

		BusinessProcessInfo processInfo = null;

		List<BusinessProcessInfo> processList = new ArrayList<BusinessProcessInfo>();

		FileTypeInfo fileTypeInfo = null;

		List<FileTypeInfo> fileTypes = new ArrayList<FileTypeInfo>();
		List<Sequence> bankSequence = new ArrayList<Sequence>();

		IRMasterDataDao masterData = new IRMasterDataDao();

		try
		{
			if(bankId!=null){
				
				Query bp = session.createQuery("FROM IRSBankLicensing bl Where bl.bank.bankId=:bankId");
				bp.setParameter("bankId",bankId);
				List<IRSBankLicensing> bplist = bp.list();
				if(bplist.size()>0){
					for(IRSBankLicensing iRSBankLicensing:bplist){
						
						/*processInfo = new BusinessProcessInfo();

						processInfo.setBusinessProcessId(iRSBankLicensing.getBusinessProcess().getBusinessProcessId());

						processInfo.setProcessName(iRSBankLicensing.getBusinessProcess().getProcessName());

						processList.add(processInfo);*/
						
						fileTypeInfo = new FileTypeInfo();

						fileTypeInfo.setFiletypeid(iRSBankLicensing.getFileType().getFiletypeid());

						fileTypeInfo.setFiletypename(iRSBankLicensing.getFileType().getFiletypename());

						fileTypes.add(fileTypeInfo);
						
					}
					
				}
				
				
				
			}
			
			/*Query query = session.createQuery("from IrMBusinessProcess bp where bp.isactive=1");

			List<IrMBusinessProcess> bplist = (List<IrMBusinessProcess>) query.list();

			Iterator<IrMBusinessProcess> bplistIterator = bplist.iterator();

			while (bplistIterator.hasNext())
			{
				businessProcess = bplistIterator.next();

				processInfo = new BusinessProcessInfo();

				processInfo.setBusinessProcessId(businessProcess.getBusinessProcessId());

				processInfo.setProcessName(businessProcess.getProcessName());

				processList.add(processInfo);
			}
			
			
			*/
			
			/*query = null;

			query = session.createQuery("from IrMFileTypes fileTyeps where fileTyeps.isactive=1");

			List<IrMFileTypes> ftypeList = (List<IrMFileTypes>) query.list();

			Iterator<IrMFileTypes> fileTypeItr = ftypeList.iterator();

			while (fileTypeItr.hasNext())
			{
				fileTypesData = fileTypeItr.next();

				fileTypeInfo = new FileTypeInfo();

				fileTypeInfo.setFiletypeid(fileTypesData.getFiletypeid());

				fileTypeInfo.setFiletypename(fileTypesData.getFiletypename());

				fileTypes.add(fileTypeInfo);
			}
			
			*/
			
			masterData.setFileTypes(fileTypes);

			irPlusResponseDetails.setMasterData(masterData);

			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);

		}
		catch(Exception e)
		{
			LOGGER.error("Exception in getMasterData",e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			throw new HibernateException(e);
		}

		return irPlusResponseDetails;
	}

	public IRPlusResponseDetails validateLicense(LicenseInfo licInfo) {

		LOGGER.info("Inside IRPlusDao validateLicense :"+licInfo);

		IRPlusResponseDetails irPlusResponseDetails = null;

		Session session = null;

		session = sessionFactory.getCurrentSession();

		try
		{

			irPlusResponseDetails = LicenseGenerator.validateLicense(licInfo.getLicPath());



			if (irPlusResponseDetails.getStatusCode()!=IRPlusConstants.ERR_CODE) 
			{ 			  			

				importLicense(irPlusResponseDetails.getLicenseInfo(),irPlusResponseDetails);
			} 

			//irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
		}
		catch(Exception e)
		{
			LOGGER.error("Exception in validateLicense",e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			throw new HibernateException(e);
		}

		return irPlusResponseDetails;


	}

	private void importLicense(LicenseInfo licInfo, IRPlusResponseDetails irPlusResponseDetails) {

		LOGGER.info("Inside IRPlusDao importLicense :"+licInfo);

		Session session = null;

		session = sessionFactory.getCurrentSession();

		IRSSite irsSite = new IRSSite();
		IrMRoles irsRoles = new IrMRoles();
		IRSLicense irsLicense = new IRSLicense();
		IRSSite siteInfo = null;
		Long licenseId = null;
		Long siteId = null;
		try
		{
			//Validating if site informaion already exists

			Query query = session.createQuery("from IRSSite site where site.siteCode=:siteCode");

			query.setParameter("siteCode",licInfo.getSiteCode());

			siteInfo = (IRSSite) query.uniqueResult();

			if(siteInfo==null)
			{
				//*** Setting up Site information **//

				irsSite.setSiteCode(licInfo.getSiteCode());
				irsSite.setSiteName(licInfo.getSiteName());
				irsSite.setContactPerson(licInfo.getCtctPerson());
				irsSite.setContactNo(licInfo.getCtctNo());
				irsSite.setAddress1(licInfo.getAddress1());
				irsSite.setAddress2(licInfo.getAddress2());
				irsSite.setSiteLogo("");
				irsSite.setIsactive(1);
				

				//*** Setting up License information **//

				irsLicense.setLicenseno(licInfo.getLicenseno());
				irsLicense.setLicensetype(IRPlusUtil.irplusEncrypt(licInfo.getLicType()));
				irsLicense.setKeyvalue(licInfo.getMacAddr());
				
				irsLicense.setStartdate(IRPlusUtil.irplusEncrypt(IRPlusUtil.convertDateToString(licInfo.getStartDate())));
				irsLicense.setEnddate(IRPlusUtil.irplusEncrypt(IRPlusUtil.convertDateToString(licInfo.getEndDate())));
				
				irsLicense.setHasSubentity(licInfo.getSubEntity());
				irsLicense.setIsactive(1);
				irsSite.setLicense(irsLicense);
				licenseId = (Long) session.save(irsLicense);
				siteId= (Long) session.save(irsSite);


				//*** Setting up Business process information **//
				List<IRPlusBusinessProcess> busprocList = licInfo.getBusProcess();
				Iterator<IRPlusBusinessProcess> busprocListIt = busprocList.iterator();
				IRPlusBusinessProcess irPlusBusinessProcess = null;
				IrMBusinessProcess businessProcess = null;
				while (busprocListIt.hasNext()) {
					irPlusBusinessProcess = busprocListIt.next();
					businessProcess = new IrMBusinessProcess(irPlusBusinessProcess);
					irsLicense.setLicenseId(licenseId);
					businessProcess.setiRSLicense(irsLicense);
					//businessProcess.setLicenseId(licenseId);
					businessProcess.setIsactive(1);
					session.save(businessProcess);

				}

				//*** Setting up Filetype information **//

				List<IRPlusFileTypes> fileTypeList = licInfo.getFileTypes();
				Iterator<IRPlusFileTypes> fileTypeListIt = fileTypeList.iterator();
				IRPlusFileTypes irPlusFileTypes = null;
				IrMFileTypes fileTypes = null;
				while (fileTypeListIt.hasNext()) {
					irPlusFileTypes = fileTypeListIt.next();
					fileTypes = new IrMFileTypes(irPlusFileTypes);
					irsLicense.setLicenseId(licenseId);
					fileTypes.setiRSLicense(irsLicense);
					//fileTypes.setLicenseid(licenseId);
					fileTypes.setIsactive(1);
					session.save(fileTypes); 
				}
			}
			else
			{
				IRSLicense existingLic = siteInfo.getLicense();
				licenseId = existingLic.getLicenseId();
				existingLic.setLicenseno(licInfo.getLicenseno());
				existingLic.setLicensetype(IRPlusUtil.irplusEncrypt(licInfo.getLicType()));
				existingLic.setKeyvalue(licInfo.getMacAddr());
				existingLic.setHasSubentity(licInfo.getSubEntity());
				existingLic.setStartdate(IRPlusUtil.irplusEncrypt(IRPlusUtil.convertDateToString(licInfo.getStartDate())));
				existingLic.setEnddate(IRPlusUtil.irplusEncrypt(IRPlusUtil.convertDateToString(licInfo.getEndDate())));
				
				irsSite.setLicense(existingLic);
				session.update(existingLic);
				session.update(irsSite);
				
				//Getting file types information

				List<IRPlusFileTypes> fileTypeList = licInfo.getFileTypes();
				Iterator<IRPlusFileTypes> fileTypeListIt = fileTypeList.iterator();
				IRPlusFileTypes irPlusFileTypes = null;
				IrMFileTypes fileTypes = null;
				IrMFileTypes existingFt = null;
				ArrayList<Long> existingFtList = new ArrayList<Long>();
				while (fileTypeListIt.hasNext()) {
					irPlusFileTypes = fileTypeListIt.next();

					LOGGER.debug("Inside fileTypeslist loop");

					Query fpQuery = session.createQuery("from IrMFileTypes ft where ft.licenseid =:licenseId and ft.filetypename =:filetypename");

					fpQuery.setParameter("licenseId", licenseId);

					fpQuery.setParameter("filetypename", irPlusFileTypes.getFiletypename());

					existingFt =  (IrMFileTypes) fpQuery.uniqueResult();

					LOGGER.debug("Found existingFt :"+existingFt);


					if(existingFt!=null)
					{
						existingFt.setIsactive(1);
						existingFtList.add(existingFt.getFiletypeid());
					}
					else
					{
						fileTypes = new IrMFileTypes(irPlusFileTypes);
						fileTypes.setLicenseid(licenseId);
						fileTypes.setIsactive(1);
						Long ftId = (Long) session.save(fileTypes);						
						existingFtList.add(ftId);
					}
				}

				Query updateFtQuery = null;
				if(existingFtList.isEmpty())
				{

					updateFtQuery = session.createQuery("update IrMFileTypes ft set ft.isactive=0 where ft.licenseid =:licenseId");

				}
				else
				{
					updateFtQuery = session.createQuery("update IrMFileTypes ft set ft.isactive=0 where ft.licenseid =:licenseId and ft.filetypeid not in (:idlist)");

					updateFtQuery.setParameterList("idlist", existingFtList);

				}

				updateFtQuery.setParameter("licenseId", licenseId);

				LOGGER.debug("before Updating removed business processes : licenseid:"+licenseId);

				int ftstatus = updateFtQuery.executeUpdate();

				LOGGER.debug("after Updating removed business processes status : "+ftstatus);

				//Getting business process information

				List<IRPlusBusinessProcess> busprocList = licInfo.getBusProcess();
				Iterator<IRPlusBusinessProcess> busprocListIt = busprocList.iterator();
				IRPlusBusinessProcess irPlusBusinessProcess = null;
				IrMBusinessProcess businessProcess = null;				
				IrMBusinessProcess existingBp = null;

				ArrayList<Long> existingBpList = new ArrayList<Long>();

				while (busprocListIt.hasNext()) {

					LOGGER.debug("Inside busProcesslist loop");

					irPlusBusinessProcess = busprocListIt.next();				

					Query busProcquery = session.createQuery("from IrMBusinessProcess bp where bp.licenseId =:licenseId and bp.processName =:processName");

					busProcquery.setParameter("licenseId", licenseId);

					busProcquery.setParameter("processName", irPlusBusinessProcess.getProcessName());

					existingBp =  (IrMBusinessProcess) busProcquery.uniqueResult();

					LOGGER.debug("Found existingBp :"+existingBp);


					if(existingBp!=null)
					{
						existingBp.setHasVisibility(irPlusBusinessProcess.getHasVisibility());

						existingBp.setIsparent(irPlusBusinessProcess.getIsparent());
						
						existingBp.setIsactive(1);

						session.update(existingBp);

						existingBpList.add(existingBp.getBusinessProcessId());
					}
					else
					{
						businessProcess = new IrMBusinessProcess(irPlusBusinessProcess);
						businessProcess.setLicenseId(licenseId);
						businessProcess.setIsactive(1);
						Long bpId =(Long) session.save(businessProcess);
						existingBpList.add(bpId);
					}
				}

				Query updateBusProcquery = null;
				if(existingBpList.isEmpty())
				{

					updateBusProcquery = session.createQuery("update IrMBusinessProcess bp set bp.isactive=0 where bp.licenseId =:licenseId");

				}
				else
				{
					updateBusProcquery = session.createQuery("update IrMBusinessProcess bp set bp.isactive=0 where bp.licenseId =:licenseId and bp.businessProcessId not in (:idlist)");

					updateBusProcquery.setParameterList("idlist", existingBpList);

				}

				updateBusProcquery.setParameter("licenseId", licenseId);

				LOGGER.debug("before Updating removed business processes : licInfo.getLicenseId():"+licenseId);

				int status = updateBusProcquery.executeUpdate();

				LOGGER.debug("after Updating removed business processes status : "+status);

			}

			IrMUsers irmuser = new IrMUsers();

			UserBean userBean = licInfo.getUserInfo();

			//*** Setting up User information **//

			// Validate if user already exists in the system

			Query userQuery = session.createQuery("from IrMUsers d where d.username =:username");

			userQuery.setParameter("username", userBean.getUsername());

			IrMUsers userInfo = (IrMUsers) userQuery.uniqueResult();

			if(userInfo==null)
			{

				irmuser.setFirstName(userBean.getFirstName());
				irmuser.setUsername(userBean.getUsername());
				irmuser.setContactno(userBean.getContactno());
				irsSite.setSiteId(siteId);
				irmuser.setSite(irsSite);
				irmuser.setIsactive(1);
				
				irmuser.setPassword(IRPlusConstants.DEFAULT_PWD);
				
				Query roleQuery = session.createQuery("from IrMRoles role where role.rolename =:rolename");
				roleQuery.setParameter("rolename", IRPlusConstants.SUPERADMIN);
				IrMRoles roleInfo= (IrMRoles) roleQuery.uniqueResult();
				
				if(roleInfo!=null)
				{
				/*	irmuser.setRoleId(roleInfo.getRoleid());*/
					irsRoles.setRoleid(roleInfo.getRoleid());
					irmuser.setIrMroles(irsRoles);
				
				}
				else
				{
					throw new BusinessException(IRPlusConstants.ERR_CODE,IRPlusConstants.LICENSE_VALIDATION_FAILED);
				}

				irsSite.setSiteId(siteId);
				irmuser.setSite(irsSite);
				irsLicense.setUser(irmuser);

				session.save(irmuser);
				
				
				
				
			}
			else
			{
				if(siteInfo==null)
				{
					irPlusResponseDetails.setStatusMsg(IRPlusConstants.USERNAME_ALREADY_EXISTS);
					irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				}
				else
				{
					Query siteQuery = session.createQuery("from IRSSite site where site.license.user.username=:username" );
					siteQuery.setParameter("username", userInfo.getUsername());
					IRSSite exiSite = (IRSSite) siteQuery.uniqueResult();
					if(exiSite!=null&&!exiSite.getSiteCode().trim().equalsIgnoreCase(siteInfo.getSiteCode().trim()))
					{
						irPlusResponseDetails.setStatusMsg(IRPlusConstants.USERNAME_ALREADY_EXISTS);
						irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
					}

				}
			}

			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);


		}
		catch(Exception e)
		{
			LOGGER.error("Exception in importLicense",e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.INVALID_LICENSE);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			throw new HibernateException(e);
		}



	}

	
	
	
	
}
