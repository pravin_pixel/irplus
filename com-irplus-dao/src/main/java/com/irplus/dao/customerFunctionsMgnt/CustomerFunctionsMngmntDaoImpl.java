package com.irplus.dao.customerFunctionsMgnt;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.irplus.dao.hibernate.entities.IrMBusinessProcess;
import com.irplus.dao.hibernate.entities.IrMFileTypes;
import com.irplus.dao.hibernate.entities.IrMReportfileFormats;
import com.irplus.dao.hibernate.entities.IrMUsers;
import com.irplus.dao.hibernate.entities.IrSBankBranch;
import com.irplus.dao.hibernate.entities.IrSBankreporting;
import com.irplus.dao.hibernate.entities.IrSCustomerOtherlicensing;
import com.irplus.dao.hibernate.entities.IrSCustomerReportlicensing;
import com.irplus.dao.hibernate.entities.IrSCustomerlicensing;
import com.irplus.dao.hibernate.entities.IrSCustomerreporting;
import com.irplus.dao.hibernate.entities.IrSCustomers;
import com.irplus.dto.BusinessProcessInfo;
import com.irplus.dto.CustomerFunctionInfo;
import com.irplus.dto.FileTypeInfo;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.util.BusinessException;
import com.irplus.util.IRPlusConstants;

@Transactional
@Component
public class CustomerFunctionsMngmntDaoImpl implements ICustomerFunctionsMngmntDao {

	private static final Logger log = Logger.getLogger(CustomerFunctionsMngmntDaoImpl.class);

	
	@Autowired
	SessionFactory sessionFactory;		

	// 17 11 2017
	@Override
	public IRPlusResponseDetails createCustomerFunctions(CustomerFunctionInfo customerFunctionInfo)throws BusinessException {

		Session session = null; 
				
		session = sessionFactory.getCurrentSession();		
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		List<String> errMsgs = new ArrayList<String>();
		
		log.debug("Inside of createCustomerFunctions");
		
		/*create customer functions ==> 8 tables create /  update data		
				some tables not create data just bring the ids only not exactly creating data
				if Db not contains data let it raise the bugs
				manually have to fill those db fields
		*/		
			
		try{
		
			boolean flag = true;
			
			boolean child_flag = true;
			
			if (customerFunctionInfo.getUserId()==null) {
				errMsgs.add("Validation Error : UserId is null ");
				flag = false;
			} 
			if (customerFunctionInfo.getCustomersId()==null) {
				errMsgs.add("Validation Error : IrSCustomersId is null ");
				flag = false;
			}
			if (customerFunctionInfo.getBankbranchId()==null) {
				errMsgs.add("Validation Error : BankbranchId is null ");
				flag = false;
			}
			if (customerFunctionInfo.getBusinsprocsId()==null) {
			
				errMsgs.add("Validation Error :: Please Select the Fields : DataEntry , Exceptions ,Validations");
				child_flag = false;
			}	
			if (customerFunctionInfo.getFiletypeId()== null ) {
			//	errMsgs.add("Validation Error : getFiletypeId[] is null ");
				errMsgs.add("Validation Error :: Not Available fileTypeIds , Please Select File Type Check Box");
				child_flag = false;
			}
			if(customerFunctionInfo.getFileFormatsId()== null ){
				errMsgs.add(" Validation Error : FileFormatsId[] is null ");
				child_flag = false;
			}
			
		if(child_flag){
			errMsgs.add(" Success : valid fields entered  ");
		}else{
			flag =false;
		}
						
			IrMUsers user = null;
			IrSCustomers irSCustomers = null;
			IrSBankBranch irSBankBranch = null;
			
		if (flag ) {
				
		 user =(IrMUsers) session.get(IrMUsers.class , customerFunctionInfo.getUserId());

		 irSCustomers = (IrSCustomers)  session.get(IrSCustomers.class	, customerFunctionInfo.getCustomersId());
	//	session.flush();
		 irSBankBranch = (IrSBankBranch) session.get(IrSBankBranch.class, customerFunctionInfo.getBankbranchId());
	//	session.flush();

		}
		else{errMsgs.add("Validation Error : Please Enter Valid values ");
		
		irPlusResponseDetails.setErrMsgs(errMsgs);
		irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);	
		
		}		
		
		if (user !=null && irSCustomers!= null && irSBankBranch != null ) {	
			
		IrMFileTypes irMFileTypes = new IrMFileTypes();
			
		IrSCustomerlicensing   irSCustomerlicensing = null;  // save 1 table
		
		Criteria criteria = session.createCriteria(IrMFileTypes.class);
			boolean db_flag = false;
			
			List<IrMFileTypes>  lst = criteria.list();
			
			if (!lst.isEmpty()) {
				db_flag = true;
			}else{
				db_flag = false;
				
				errMsgs.add("Validation Error :: Existing DB Not contains the Data Please fill the Db data");
			}
		
	//	errMsgs.add(" ");
		if( db_flag){
	
			Long[] fileId = new Long[customerFunctionInfo.getFiletypeId().length];
				fileId = customerFunctionInfo.getFiletypeId();

		/*	 Criteria c1 = session.createCriteria(IrSCustomerlicensing.class,"custmLicns_info");
			 					c1.add(Restrictions.idEq(customerFunctionInfo.getCustomersId()));
			 				List<IrSCustomerlicensing> lst1 =	c1.list();
					 int m=0;
			 				for (IrSCustomerlicensing irSCustomerlicensing2 : lst1) {
								
			 					if(irSCustomerlicensing2.getFiletypeid()==fileId[m]){
			 						
			 					}else{
			 						
			 					}
			 					++m;
							}*/
			 			//  Here primary key have to set in DB		
			 	
	 				/*Criteria custmrLicngCrtria = session.createCriteria(IrSCustomerlicensing.class,"custmrLinceng");
	 				custmrLicngCrtria.createAlias(custmrLinceng, IrSCustomers)
	 				custmrLicngCrtria.add(Restrictions.eq(, value));*/
	 				
	 				Query query = session.createQuery("from IrSCustomerlicensing where irSCustomers.customerId = :customerid ");
	 				query.setParameter("customerid", customerFunctionInfo.getCustomersId());
	 				List<IrSCustomerlicensing> list = query.list();
	 			
	 		//		Long[] fileType_arry = new Long[list.size()];
	 			ArrayList<Long>  alist = new ArrayList<Long>();
	 			
	 				if (!list.isEmpty()) {
								 			
						for (IrSCustomerlicensing irSCustmerlicnsing : list) {  // 2 ,1 , 3
	 					
							alist.add(irSCustmerlicnsing.getFiletypeid());							 
	 	
						}
	 				}
		//							Dbalist = 2,3
	 	//							fileId= 1,2     common = 1 ceate			
			
			for (int i = 0; i < fileId.length; i++) {
				
				if (Collections.frequency(alist, fileId[i])==0) {
					
					irSCustomerlicensing = new IrSCustomerlicensing();  // save 1 table
					
					log.debug( " Selected File Process fileTypeIds : "+fileId[i]);		
		
					irSCustomerlicensing.setFiletypeid(fileId[i]);			
//					filetypes_flag = true;
					irSCustomerlicensing.setIrMUsers(user);
					irSCustomerlicensing.setIrSBankbranch(irSBankBranch);
					irSCustomerlicensing.setIrSCustomers(irSCustomers);
			//		irSCustomerlicensing.setIrMBusinessprocess(irMBusinessProcess);
					irSCustomerlicensing.setIsactive(1);
			//		irSCustomerlicensing.setCustomerLicenseId(customerFunctionInfo.getCustomerLicenseId());
					session.save(irSCustomerlicensing);
			//		session.flush();
				}
				
			}
				
		}else
		{
			errMsgs.add("Validation Error :: Not Available given fileType  Ids In DB, Please Check in primary Key values");
		}
		// file types over - 1 get data . 2nd table fill OVER -close
		
		// file types over -2 get . 4 fill table
			
		// 2 save
		
			Query query1 = session.createQuery("from IrSCustomerOtherlicensing where irSCustomers.customerId = :customerid ");
			query1.setParameter("customerid", customerFunctionInfo.getCustomersId());
			List<IrSCustomerOtherlicensing> list1 = query1.list();
			
			List<Long> long_list = new ArrayList<Long>();
			
			if (!list1.isEmpty()) {
				
				for (IrSCustomerOtherlicensing irSCustomerOtherlicensing : list1) {
					
					long_list.add(	irSCustomerOtherlicensing.getIrMBusinessprocess().getBusinessProcessId());
				}
				
			}
					Long[] bussinessIds = customerFunctionInfo.getBusinsprocsId();
					IrSCustomerOtherlicensing irSCustomerOtherlicensing =  null;
			//		Arrays.asList(bussinessIds);
					
				for (Long bid : bussinessIds) {

					if (Collections.frequency(long_list, bid)==0) {
					
						irSCustomerOtherlicensing =  new IrSCustomerOtherlicensing();  
						
						log.debug("Bussiness Process ids :"+bid);
						IrMBusinessProcess bp =(IrMBusinessProcess)	session.get(IrMBusinessProcess.class,bid );	

						irSCustomerOtherlicensing.setIrMBusinessprocess(bp);
						
						irSCustomerOtherlicensing.setIrMUsers(user);
						irSCustomerOtherlicensing.setIrSBankBranch(irSBankBranch);
						irSCustomerOtherlicensing.setIrSCustomers(irSCustomers);
						irSCustomerOtherlicensing.setIsactive(1);
						irSCustomerOtherlicensing.setNotifications(customerFunctionInfo.getNotification());  // isavil defined in the db this field
						
						session.save(irSCustomerOtherlicensing);
					
					}
			
				//	session.flush();
				}
				
					// file types over -5 get . 6 fill table
		
				/*After save functionality "irSCustomerOtherlicensing saving db data" 
				 * 
				 * get data
				 * */
						
					Criteria criteria2 = session.createCriteria(IrSBankreporting.class);  
					
					criteria2.add(Restrictions.idEq(customerFunctionInfo.getIrSBankreportingId())); // bank reportid giving
					
			//		List<IrSBankreporting> reprtList = criteria2.list();
					
					List<IrSBankreporting> bankReportlist = criteria2.list();
					
					IrSCustomerreporting irSCustomerreporting =  null;
				
				if(!bankReportlist.isEmpty()){	
					
					for (IrSBankreporting irSBankreporting : bankReportlist) {
						
						irSCustomerreporting = new IrSCustomerreporting();        /// 3 save
						
						irSCustomerreporting.setIrSBankreporting(irSBankreporting);	  //1
						irSCustomerreporting.setIrMUsers(user);                        // 2
						irSCustomerreporting.setIrSBankbranch(irSBankBranch);
						irSCustomerreporting.setIrSCustomers(irSCustomers);
						irSCustomerreporting.setIsactive(1);
						session.save(irSCustomerreporting);	
					}
					
					
				///	report_flag = true;
				}else{
					
					errMsgs.add(" Validation Error : report value not entered / it is optional ");
				}

				// file types over - 7 IrMReportfileFormats get data . 8 fill table  

				IrMReportfileFormats irMReportfileFormats = null;
				
			//	boolean fileFormat_flag = false;
			//	Integer four[] = null;
				
				Query query2 = session.createQuery("from IrSCustomerReportlicensing where irSCustomers.customerId = :customerid ");
				query2.setParameter("customerid", customerFunctionInfo.getCustomersId());
				List<IrSCustomerReportlicensing> list3 = query2.list();
				
				
				List<Integer> Intgr_list = new ArrayList<Integer>();
				
				for (IrSCustomerReportlicensing irSCustomerReportlicensing : list3) {
					Intgr_list.add(irSCustomerReportlicensing.getIrMReportfileFormats().getFileFormatId());
				}
				
				Integer transmsnFileid[] = new Integer[customerFunctionInfo.getFileFormatsId().length];
				
				log.debug("File formats [] "+transmsnFileid);
				
					transmsnFileid = customerFunctionInfo.getFileFormatsId();
					
					IrSCustomerReportlicensing irSCustomerReportlicensing = null;
					
					
					for (int i = 0; i < transmsnFileid.length; i++) {
												
						if (Collections.frequency(Intgr_list, transmsnFileid[i])==0) {
							
							irSCustomerReportlicensing = new IrSCustomerReportlicensing(); // 4 save
							
							irMReportfileFormats = (IrMReportfileFormats)session.get(IrMReportfileFormats.class, transmsnFileid[i]);
						//	session.flush();
							
							log .debug("Getting ReportFileFormats "+irMReportfileFormats);
							
							log.debug("transmsnFileid  :"+transmsnFileid[i]);
							
							irSCustomerReportlicensing.setIrMReportfileFormats(irMReportfileFormats);						
							irSCustomerReportlicensing.setIrMUsers(user);
							irSCustomerReportlicensing.setIrSBankbranch(irSBankBranch);
							irSCustomerReportlicensing.setIrSCustomers(irSCustomers);
							irSCustomerReportlicensing.setIsactive(1);
							
							 session.save(irSCustomerReportlicensing);
						}
						
					}									
						// 4 dbs data saving 1)irSCustomerlicensing 2)irSCustomerOtherlicensing 
						//3)irSCustomerreporting 4)irSCustomerReportlicensing
						
						
				//		session.save(irSCustomerOtherlicensing);session.flush();
				//		session.save(irSCustomerreporting);session.flush();									
					
					errMsgs.add(" Success :: Data is saved in tables : the Corresponding tables : "
							+ "irSCustomerreporting ,irSCustomerOtherlicensing ,irSCustomerlicensing , irSCustomerReportlicensing");
					irPlusResponseDetails.setErrMsgs(errMsgs);
					irPlusResponseDetails.setValidationSuccess(true);
					irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
					irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
									
								
				return irPlusResponseDetails;
		} else {
			errMsgs.add(" Validation Error :: Please Enter Valid UserId or BankBranchId or CustomerId or BusinessProcessId ");
			irPlusResponseDetails.setErrMsgs(errMsgs);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
		}
/*
		errMsgs.add(" Validation Error :: Please Enter Valid Fields");
		irPlusResponseDetails.setErrMsgs(errMsgs);
		irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);*/
		
	//	
		
	}catch (Exception e) {
		log.error("Exception in createCustomerFunctionsMngmnt",e);
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
		irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		throw new HibernateException(e);
	}
		
		return irPlusResponseDetails;
	}

	//=================================================================================================================================================
	// 11 16 2017
	

	public IRPlusResponseDetails createCustomerFunctions12(CustomerFunctionInfo customerFunctionInfo)throws BusinessException {

		Session session = null; 
				
		session = sessionFactory.getCurrentSession();		
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		List<String> errMsgs = new ArrayList<String>();
		
		/*create customer functions ==> 8 tables create /  update data		
				some tables not create data just bring the ids only not exactly creating data
				if Db not contains data let it raise the bugs
				manually have to fill those db fields
		*/		
				
		try{
		
			boolean flag = true;
			if (customerFunctionInfo.getUserId()==null) {
				errMsgs.add("Validation Error : UserId is null ");
				flag = false;
			} 
			if (customerFunctionInfo.getCustomersId()==null) {
				errMsgs.add("Validation Error : IrSCustomersId is null ");
				flag = false;
			}
			if (customerFunctionInfo.getBankbranchId()==null) {
				errMsgs.add("Validation Error : BankbranchId is null ");
				flag = false;
			}
			if (customerFunctionInfo.getBusinsprocsId()==null) {
			
				errMsgs.add("Validation Error : BusinessprocessId[] is null ");
				flag = false;
			}
			IrMUsers user = null;
			IrSCustomers irSCustomers = null;
			IrSBankBranch irSBankBranch = null;
			
		if (flag) {
				
		 user =(IrMUsers) session.get(IrMUsers.class , customerFunctionInfo.getUserId());
		
		session.flush();
		 irSCustomers = (IrSCustomers)  session.get(IrSCustomers.class	, customerFunctionInfo.getCustomersId());
		session.flush();
		 irSBankBranch = (IrSBankBranch) session.get(IrSBankBranch.class, customerFunctionInfo.getBankbranchId());
		session.flush();
/*		IrMBusinessProcess irMBusinessProcess = (IrMBusinessProcess) session.get(IrMBusinessProcess.class, customerFunctionInfo.getBusinessprocessId());
		session.flush();
*/		
		}else{errMsgs.add("Validation Error : Please Enter Valid ids ");}
		
		if (user !=null && irSCustomers!= null && irSBankBranch != null ) {	
			
		IrMFileTypes irMFileTypes = new IrMFileTypes();
			
		IrSCustomerlicensing   irSCustomerlicensing = new IrSCustomerlicensing();  // save 1 table
		
		Criteria criteria = session.createCriteria(IrMFileTypes.class);
			boolean db_flag = false;
			
			List<IrMFileTypes>  lst = criteria.list();
			
			if (!lst.isEmpty()) {
				db_flag = true;
			}else{
				db_flag = false;
				errMsgs.add("Validation Error :: Existing DB Not contains the Data Please fill the Db data");
			}
		
		errMsgs.add(" ");
		
		boolean filetypes_flag = false;
		
		if(customerFunctionInfo.getFiletypeId()!= null && db_flag){
			
		//	lst.a
			
		Long[] fileId = new Long[customerFunctionInfo.getFiletypeId().length];
				fileId = customerFunctionInfo.getFiletypeId();
		
//			Map<Long ,String>	map = customerFunctionInfo.getFileType_Id_Names();
		
	/*	for (Map.Entry<Long, String> entry : map.entrySet())
		{
		  //  System.out.println(entry.getKey() + "/" + entry.getValue());
		    log.debug(" file type id : "+entry.getKey() + "/ file type Name :" + entry.getValue());
		    
		    irSCustomerlicensing.setFiletypeid(entry.getKey());
		}
	*/	
			
			for (int i = 0; i < fileId.length; i++) {
			
				log.debug( " Selected File Process fileTypeIds : "+fileId[i]);
		
	//  Here primary key have to set in DB			
	//			IrMFileTypes irFileTypes =(IrMFileTypes) session.get(IrMFileTypes.class, fileId[i]);
				irSCustomerlicensing.setFiletypeid(fileId[i]);
				
			}
			filetypes_flag = true;
			irSCustomerlicensing.setIrMUsers(user);
			irSCustomerlicensing.setIrSBankbranch(irSBankBranch);
			irSCustomerlicensing.setIrSCustomers(irSCustomers);
	//		irSCustomerlicensing.setIrMBusinessprocess(irMBusinessProcess);
			irSCustomerlicensing.setIsactive(1);
	//		irSCustomerlicensing.setCustomerLicenseId(customerFunctionInfo.getCustomerLicenseId());
			
		}else
		{
			errMsgs.add("Validation Error :: Not Available fileTypeIds , Please Select File Type Check Box");
		}
		// file types over - 1 get data . 2nd table fill OVER tableS
		
		// file types over -2 get . 4 fill table
		
		boolean business_flag = false;
		
		IrSCustomerOtherlicensing irSCustomerOtherlicensing =  new IrSCustomerOtherlicensing();  // 2 save
		
				if (customerFunctionInfo.getBusinsprocsId()!= null) {
					Long[] bussinessIds = customerFunctionInfo.getBusinsprocsId();
					 boolean bflag = false;
				
		//		bussinessIds;
				for (Long bid : bussinessIds) {
					
				IrMBusinessProcess bp =(IrMBusinessProcess)	session.get(IrMBusinessProcess.class,bid );	
				session.flush();
				irSCustomerOtherlicensing.setIrMBusinessprocess(bp);
				}
				
				business_flag = true;
				irSCustomerOtherlicensing.setIrMUsers(user);
				irSCustomerOtherlicensing.setIrSBankBranch(irSBankBranch);
				irSCustomerOtherlicensing.setIrSCustomers(irSCustomers);
				irSCustomerOtherlicensing.setIsactive(1);
				irSCustomerOtherlicensing.setNotifications(customerFunctionInfo.getNotification());  // isavil defined in the db this field
		//		irSCustomerOtherlicensing.setOtherLicensingId();
				
				}else{
					errMsgs.add("Validation Error :: Please Select the Fields : DataEntry , Exceptions ,Validations");
				}
		
				// file types over -5 get . 6 fill table
		
				/*After save functionality "irSCustomerOtherlicensing saving db data" 
				 * 
				 * get data
				 * */
				
			//	irSCustomerOtherlicensing.getIrMBusinessprocess().getProcessName();
				
				IrSCustomerreporting irSCustomerreporting = new IrSCustomerreporting();        /// 3 save
						
				boolean report_flag = false;
				
				if(customerFunctionInfo.getIrSBankreportingId()==5){
				
					Criteria criteria2 = session.createCriteria(IrSBankreporting.class);
					//IrSBankreporting irSBankreporting =(IrSBankreporting) session.get(IrSBankreporting.class, 5);
					List<IrSBankreporting> bankReportlist = criteria2.list();
					
					for (IrSBankreporting irSBankreporting : bankReportlist) {
						irSCustomerreporting.setIrSBankreporting(irSBankreporting);	  //1
					}
					
					irSCustomerreporting.setIrMUsers(user);                        // 2
					irSCustomerreporting.setIrSBankbranch(irSBankBranch);
					irSCustomerreporting.setIrSCustomers(irSCustomers);
					irSCustomerreporting.setIsactive(1);
					report_flag = true;
				}
				
				// file types over - 7 IrMReportfileFormats get data . 8 fill table   = 
				
			//	IrMReportfileFormats irMReportfileFormats = new IrMReportfileFormats();
				
				
								
				IrMReportfileFormats irMReportfileFormats = null;
				
				boolean fileFormat_flag = false;
				Integer four[] = null;
				
				Integer transmsnFileid[] = new Integer[customerFunctionInfo.getFileFormatsId().length];
				
				if(customerFunctionInfo.getFileFormatsId()!=null && customerFunctionInfo.getFileFormatsId().length!=0){
					
					transmsnFileid = customerFunctionInfo.getFileFormatsId();
					
					for (int i = 0; i < transmsnFileid.length; i++) {
						
						IrSCustomerReportlicensing irSCustomerReportlicensing = new IrSCustomerReportlicensing(); // 4 save
						
						irMReportfileFormats = (IrMReportfileFormats)	session.get(IrMReportfileFormats.class, transmsnFileid[i]);
						session.flush();
						
						log .debug("Getting ReportFileFormats "+irMReportfileFormats);
						irSCustomerReportlicensing.setIrMReportfileFormats(irMReportfileFormats);
						
						irSCustomerReportlicensing.setIrMUsers(user);
						irSCustomerReportlicensing.setIrSBankbranch(irSBankBranch);
						irSCustomerReportlicensing.setIrSCustomers(irSCustomers);
						irSCustomerReportlicensing.setIsactive(1);
						
						 session.save(irSCustomerReportlicensing);session.flush();
					}
					fileFormat_flag = true;
				}else{
					errMsgs.add(" Validation Error : FileFormatsId[] is null ");
					
				}
						if(fileFormat_flag && report_flag && business_flag && filetypes_flag){ 
						
						// 4 dbs data saving 1)irSCustomerlicensing 2)irSCustomerOtherlicensing 
						//3)irSCustomerreporting 4)irSCustomerReportlicensing
						
						session.save(irSCustomerlicensing);session.flush();
						session.save(irSCustomerOtherlicensing);session.flush();
						session.save(irSCustomerreporting);session.flush();									
					
					errMsgs.add(" Success :: Data is saved in tables : the Corresponding tables : "
							+ "irSCustomerreporting ,irSCustomerOtherlicensing ,irSCustomerlicensing , irSCustomerReportlicensing");
					irPlusResponseDetails.setErrMsgs(errMsgs);
					irPlusResponseDetails.setValidationSuccess(true);
					irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
					irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
				
					
					}else{
						
						errMsgs.add(" Validation Error :: Please Enter Valid UserId or BankBranchId or CustomerId or BusinessProcessId ");
						irPlusResponseDetails.setErrMsgs(errMsgs);
						irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
						irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);								
					}				
						return irPlusResponseDetails;
		} else {
			errMsgs.add(" Validation Error :: Please Enter Valid UserId or BankBranchId or CustomerId or BusinessProcessId ");
			irPlusResponseDetails.setErrMsgs(errMsgs);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
		}

		errMsgs.add(" Validation Error :: Please Enter Valid UserId or BankBranchId or CustomerId or BusinessProcessId ");
		irPlusResponseDetails.setErrMsgs(errMsgs);
		irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
		
		
	}catch (Exception e) {
		log.error("Exception in createCustomerFunctionsMngmnt",e);
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
		irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		throw new HibernateException(e);
	}
		
		return irPlusResponseDetails;
	}
	
	// so it is  completed 11 23 17
	@Override
	public IRPlusResponseDetails getCustomerFunctionsById(String customerFunctionInfoId) throws BusinessException {

		/*Four Classes we are getting based on CustomerId 
		 * 
		 * Four Classes = 
		 * 1) CustomerLicencing
		 * 2)CustomerOtherLicencing
		 * 3)CustomerReporting
		 * 4)CustomerReportLicencing
		 * 
		 * */ 
		
		Session session = null; 
				
		session = sessionFactory.getCurrentSession();		
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		List<String> errMsgs = new ArrayList<String>();
		
		log.debug("Inside CustomerFunctionsMngmntDaoImpl :: getCustomerFunctionsById()");
		
		try {
			CustomerFunctionInfo customerFunctionInfo = new CustomerFunctionInfo();
			
			Integer customerId = Integer.parseInt(customerFunctionInfoId);
			
			IrSCustomerlicensing irSCustomerlicensing = null;
			
			/*Getting here irscusomerlicensing Getting One table data */
			
			Query query1 = session.createQuery("from IrSCustomerlicensing where  irSCustomers.customerId=:customerid");
			query1.setParameter("customerid", customerId);
			List<IrSCustomerlicensing> licnce_list = query1.list();
			
			
			boolean customerLicnsng_flag = true;
			
			
			if (!licnce_list.isEmpty()) {
			
				/* Here file typeIds getting is qual to customerids availability
				 * 
				 * */
				
				Long[] fileTypeIds = new Long[licnce_list.size()];  
				
				Map<Long, FileTypeInfo> map = new HashMap<Long ,FileTypeInfo>();
				
				int i =0;
				IrMFileTypes irf = null;
				for (IrSCustomerlicensing irSCustomerlicensing2 : licnce_list) {
					
					irf =(IrMFileTypes)session.get(IrMFileTypes.class, irSCustomerlicensing2.getFiletypeid());		
					
					FileTypeInfo ft= new FileTypeInfo();
					
					ft.setFiletypeid(irf.getFiletypeid());
					ft.setFiletypename(irf.getFiletypename());
					ft.setIsactive(irf.getIsactive());
				//	ft.setLicenseId(irf.ge);
				//	ft.setModifieddate(modifieddate);
					
					
					map.put(irf.getFiletypeid(),ft);
					
					fileTypeIds[i]= irSCustomerlicensing2.getFiletypeid();
					++i;
				}
				
				customerFunctionInfo.setFiletypeId(fileTypeIds);
				
				if(!map.isEmpty()){
					customerFunctionInfo.setMapCusFun_ids_fileTypes(map);  // getting the Ach ,wire ,remitrac
				}
				log.debug(" getcustomerFunctionsId :: fileTypeIds :"+fileTypeIds);
				
			} 
			else{
				customerLicnsng_flag = false;		
				errMsgs.add(" Message Alert :: No FileTypes Available");
			}
			
			/*Table -2 =IrSCustomerOtherlicensing getting DataEntry ,Exceptions ,Validations */
			
			Query query2 = session.createQuery("from IrSCustomerOtherlicensing where  irSCustomers.customerId =:customerid");
			query2.setParameter("customerid", customerId);
			List<IrSCustomerOtherlicensing> otherLicnce_list = query2.list();

			Map<Long , String> map = new HashMap<Long , String>();
			
			if (!otherLicnce_list.isEmpty()) {
				
				for (IrSCustomerOtherlicensing irSCustomerOtherlicensing : otherLicnce_list) {
					
					map.put(irSCustomerOtherlicensing.getIrMBusinessprocess().getBusinessProcessId(), irSCustomerOtherlicensing.getIrMBusinessprocess().getProcessName());
				}
								    
			} else {
				errMsgs.add("Message Alert :: Not available Entries of DataEntry ,Exceptions , Validations");
			}
		

			/*Query query = getMyCurrentSession().createQuery("select distinct roleperm.irMModulemenus.irMMenus from IrMRolepermissions roleperm where roleperm.irMRoles.roleid=:roleid");
			query.setParameter("roleid", userInfo.getRoleId());
			List <IrMMenus> menuList = query.list();
			*/
		//	IrSCustomerOtherlicensing
		//	businessProcessId
			
			
			
			Map<Long , BusinessProcessInfo> bus_id_prsnms = new HashMap<Long , BusinessProcessInfo>();
			
			Query query_bussiness = session.createQuery("select  custmOth.irMBusinessprocess from IrSCustomerOtherlicensing custmOth where custmOth.irSCustomers.customerId =:customer_id");
			
			query_bussiness.setParameter("customer_id", customerId);
		
			List<IrMBusinessProcess> bussns_list = query_bussiness.list();
		
				log.debug(" Inside CustomerFunctnsGetOne :: irMBusinessprocess from IrSCustomerOtherlicensing  :"+bussns_list);
					
			for (IrMBusinessProcess irMBusnProcess : bussns_list) {
				
				BusinessProcessInfo bpf = new BusinessProcessInfo(); 
						
				bpf.setBusinessProcessId(irMBusnProcess.getBusinessProcessId());
				
				//bpf.setHasVisibility(hasVisibility);
				bpf.setIsactive(irMBusnProcess.getIsactive());
				bpf.setProcessName(irMBusnProcess.getProcessName());
			//	bpf.setIsparent(isparent);
				
				bus_id_prsnms.put(irMBusnProcess.getBusinessProcessId(), bpf);
			}
			
			customerFunctionInfo.setBuissines_Ids_ProcNams(bus_id_prsnms);    // Process names getting
			
			
		//	IrSCustomerreporting
			
			Query query_Bankrep = session.createQuery(" from IrSCustomerreporting custmRepo where custmRepo.irSCustomers.customerId =:customer_id");
			
			query_Bankrep.setParameter("customer_id", customerId);

			List<IrSCustomerreporting> query_Bankrep_list = query_Bankrep.list();
		
				log.debug(" Inside CustomerFunctnsGetOne :: irSBankreporting from SCustomerreporting   :"+query_Bankrep_list);
			
				Map<Integer , Integer> report_active_reptid =new HashMap<>();// reportid and isactive
			
				for (IrSCustomerreporting irSCustomerreporting : query_Bankrep_list) {					
					
					report_active_reptid.put(irSCustomerreporting.getIsactive(), irSCustomerreporting.getCustomerReportId());
				}
				customerFunctionInfo.setReport_active_reptid(report_active_reptid);
				
			
			//	IrSCustomerReportlicensing 		
						
				Query query_Reportlic = session.createQuery("from IrSCustomerReportlicensing custmRprtLcn where custmRprtLcn.irSCustomers.customerId =:customer_id");
						
				query_Reportlic.setParameter("customer_id", customerId);
		
				List<IrSCustomerReportlicensing> reportlic_list = query_Reportlic.list();
				
				Integer[] fileFormatIds = new Integer[reportlic_list.size()];
				
				int i = 0;
				for (IrSCustomerReportlicensing irSCustomerReportlicensing : reportlic_list) {					
					fileFormatIds[i]=irSCustomerReportlicensing.getIrMReportfileFormats().getFileFormatId();
					++i;
				}
				
				customerFunctionInfo.setFileFormatsId(fileFormatIds);	
				errMsgs.add("Success :: Customer Fuctions got the data ");
				irPlusResponseDetails .setErrMsgs(errMsgs);
				irPlusResponseDetails .setCustomerFunctionInfo(customerFunctionInfo);
				irPlusResponseDetails .setValidationSuccess(true);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
				
				
		} catch (Exception e) {
			log.error("Exception in getCustomerFunctionsMngmntById()",e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			throw new HibernateException(e);
		}			
			return irPlusResponseDetails;
	}

	
	@Override
	public IRPlusResponseDetails updateCustomerFunctions(CustomerFunctionInfo customerFunctionInfo)throws BusinessException {

		Session session = null; 
				
		session = sessionFactory.getCurrentSession();		
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		List<String> errMsgs = new ArrayList<String>();
		
		log.debug("Inside CustomerFunctionsMngmntDaoImpl :: updateCustomerFunctions");
		
		try{
		
			// 11 23 17 
			
			Integer custmer_id = customerFunctionInfo.getCustomersId();
			
			IrSCustomers customer = null;
			
			if(custmer_id!=null){
				
				customer = (IrSCustomers)	session.get(IrSCustomers.class, custmer_id);
				
					if (customer != null) {						

					boolean flag_custmerFuntnUpdte = false ;
					
	//				buissiness_Ids_ProcNams
		
					Integer[] filetypes = customerFunctionInfo.getFileFormatsId();
					
					Map<Long ,String > filType_Id_Names = customerFunctionInfo.getFileType_Id_Names();
					
					Map<Long ,BusinessProcessInfo > busines_info = customerFunctionInfo.getBuissines_Ids_ProcNams();					
												
					Map<Integer , String> file_formats =  customerFunctionInfo.getFile_formats();						
					
					 Integer notification = customerFunctionInfo.getNotification();
					
					if (filetypes.length !=0){
						
						flag_custmerFuntnUpdte = true ;												
					} else {
						flag_custmerFuntnUpdte = false;
						errMsgs.add(" Validation Error :: Please Enter filetypes  ");
//						irPlusResponseDetails.setErrMsgs(errMsgs);
//						irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
//						irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
					}
					
					if (busines_info!= null && busines_info.isEmpty()){
						flag_custmerFuntnUpdte = true;
						
					} else {
						flag_custmerFuntnUpdte = false;
						errMsgs.add(" Validation Error :: Please Enter DataEntry , Exceptions entries  ");
					}

					if (file_formats!= null && file_formats.isEmpty()){
					
						flag_custmerFuntnUpdte = true;					
					} else {
						flag_custmerFuntnUpdte = false;
						errMsgs.add(" Validation Error :: Please Enter DataEntry , Exceptions entries  ");
					}
					
					if (notification != null && notification != null){
						
						flag_custmerFuntnUpdte = true;					
					} else {
						flag_custmerFuntnUpdte = false;						
						errMsgs.add(" Validation Error :: Please Enter Notification entrie ");
					}
					
					/* Validations success*/
					
					if (flag_custmerFuntnUpdte) {
												
						/*	Here 1)File Types setting				*/
						
						Query query = session.createQuery("from IrSCustomerlicensing custmlinc where custmlinc.irSCustomers.customerId =:customerid");
		 				query.setParameter("customerid", customerFunctionInfo.getCustomersId());		 				
		 				List<IrSCustomerlicensing> list = query.list();
		 				
		 				for (IrSCustomerlicensing irSCustmrlicng : list){		 					
		 					irSCustmrlicng.setIsactive(0);
		 					session.update(irSCustmrlicng);
		 				}
		 				
		 				Long[] db_filetypeids = new Long[list.size()];  
		 				int i =0;
		 				
		 				for (IrSCustomerlicensing irSCustmrlicng : list) {							
	 						db_filetypeids[i] =irSCustmrlicng.getFiletypeid();	 		
	 						++i;
						}
		 				
		 				Map<Long , String> filType_Id = customerFunctionInfo.getFileType_Id_Names();
		 				
		 				Set<Long> fileTypes_update  = filType_Id.keySet();	
		 				
		 				Iterator<Long>  fl= fileTypes_update.iterator();
		 						 			
		 				if(fl!=null){
		 				for (IrSCustomerlicensing irSCustmrlicng : list){		 					
		 					
		 					while (fl.hasNext()) {
								Long long1 = (Long) fl.next();
								
								if(irSCustmrlicng.getFiletypeid()==long1){
				 					irSCustmrlicng.setIsactive(0);
				 					session.update(irSCustmrlicng);
				 					session.flush();
				 				}
							}		 					
		 				}
		 				}
		 						 				
		 					/*	New one creating if not there*/
		 				
		 				for (Long id_update : fileTypes_update) {
		 					
		 				//	if (Collections.frequency(Arrays.asList(db_filetypeids), id_update)==0){
		 				
		 					if (!Arrays.asList(db_filetypeids).contains(id_update)){
		 					
		 						IrSCustomerlicensing s = new  IrSCustomerlicensing();
		 						
		 						s.setFiletypeid(id_update);
		 						s.setIsactive(1);
		 						s.setIrSCustomers((IrSCustomers)session.get(IrSCustomers.class, customerFunctionInfo.getCustomersId()));
		 						s.setIrMUsers((IrMUsers)session.get(IrMUsers.class, customerFunctionInfo.getUserId()));
		 						
		 						session.save(s);
		 						session.flush();
		 					}		 							 				
		 					
						}		 			
		 				
		 			
		 				/*	Here  2) BussinessProcesss setting*/
		 				
		 				IrSCustomerOtherlicensing customerOthrLicng = new IrSCustomerOtherlicensing();
		 				
		 				Map<Long , BusinessProcessInfo> buiss_Ids_ProcNams = customerFunctionInfo.getBuissines_Ids_ProcNams();

		 				buiss_Ids_ProcNams.keySet();
		 				
		 				Query query12 = session.createQuery("from IrSCustomerOtherlicensing custmrOthr where custmrOthr.irSCustomers.customerId =:customerid");
		 				query12.setParameter("customerid", customerFunctionInfo.getCustomersId());		 				
		 				List<IrSCustomerOtherlicensing> list_cutm_Othr = query12.list();
		 				
		 					for (IrSCustomerOtherlicensing irSCustomerOtherlicensing : list_cutm_Othr){
		 						
		 						irSCustomerOtherlicensing.setIsactive(0);   // default inactive
		 						irSCustomerOtherlicensing.setNotifications(0);
		 						session.update(irSCustomerOtherlicensing);
							}
		 					
		 					Set<Long> set_bus = buiss_Ids_ProcNams.keySet();
			 					
		 					int j =0;
		 					Long[] db_bdids = new Long[list_cutm_Othr.size()];
		 					
		 					for (IrSCustomerOtherlicensing irSCustomerOtherlicensing : list_cutm_Othr) {
								
		 						db_bdids[j] = irSCustomerOtherlicensing.getIrMBusinessprocess().getBusinessProcessId();
		 						
		 						for(Long long1 : set_bus){
									
			 						if (irSCustomerOtherlicensing.getIrMBusinessprocess().getBusinessProcessId()==long1){										
			 							irSCustomerOtherlicensing.setIsactive(1);   // active ness done
			 							irSCustomerOtherlicensing.setNotifications(customerFunctionInfo.getNotification()); // notification done
			 							session.update(irSCustomerOtherlicensing);
									}			 						
								}
		 						
		 						++j;		 					
							}
		 					
		 					IrMBusinessProcess ir = null;
		 
		 					//IrSCustomerOtherlicensings irs123 = null; 
		 					
		 					IrMUsers user = null;
		 					
		 					for (Long long1 : db_bdids) {
							
		 						if(Collections.frequency(Arrays.asList(db_bdids) , long1)==0){
		 							
		 							ir = new  IrMBusinessProcess(); // BusinessProcess
		 			 								 							
		 						//	ir.setHasVisibility();
		 						//	ir.setIrSCustomerlicensings();		 					
		 						//	ir.setIrSCustomerOtherlicensings(irSCustomerOtherlicensings);
		 							
		 							ir.setIsactive(1);
		 						//	ir.setIsparent(isparent);
		 						if(long1==1){		 							
		 							ir.setProcessName("DataEntry");
		 						}
		 						if(long1==2){		 							
		 							ir.setProcessName("Exception");
		 						}
		 						if(long1==3){		 							
		 							ir.setProcessName("Validation");
		 						}
		 							
		 						if(customerFunctionInfo.getUserId()!=null){
		 							user = (IrMUsers)session.get(IrMUsers.class, customerFunctionInfo.getUserId());
		 							
		 							ir.setUser(user);
		 						}else{
		 							errMsgs.add(" Validation Alert :: UserId is null , Please Enter UserId");
		 						}
		 						//ir.setLicenseId(licenseId);
		 						
		 						Integer id =(Integer) session.save(ir);
		 						
		 						IrSCustomerOtherlicensing newOne = new IrSCustomerOtherlicensing();
		 						
		 						newOne.setIrMBusinessprocess(ir);
		 						if(user!=null){
		 						newOne.setIrMUsers(user);
		 						}
		 					//	newOne.setIrSBankBranch()
		 						newOne.setIsactive(1);
		 						
		 						if(customerFunctionInfo.getNotification()!=null){
		 							
		 						newOne.setNotifications(customerFunctionInfo.getNotification());
		 						
		 						}
		 						newOne.setIrSCustomers((IrSCustomers)session.get(IrSCustomers.class, customerFunctionInfo.getCustomersId()));
		 					 //	newOne.setOtherLicensingId();
		 						
		 					if(customerFunctionInfo.getBankbranchId()!=null){
		 							
		 						newOne.setIrSBankBranch((IrSBankBranch)session.get(IrSBankBranch.class, customerFunctionInfo.getBankbranchId()));	
		 						
		 					   }else{  errMsgs.add("Message Alert :: Please Enter The BankbranchId"); }
		 						
		 						session.save(newOne);
		 						}
		 						
		 						
							}
		 					
		 					/*	Here  3) Bank Reporting setting */
		 					
		 					// IrSCustomerreporting 
		 					
		 					Query query_cutmrreport = session.createQuery("from IrSCustomerreporting custmrptng where custmrptng.irSCustomers.customerId =:customerid ");
		 					query_cutmrreport.setParameter("customerid", customerFunctionInfo.getCustomersId());		 				
		 				//	IrSCustomerreporting cutomerReporting =(IrSCustomerreporting) query_cutmrreport.uniqueResult();
		 					
		 					IrSBankreporting bankreport =(IrSBankreporting)session.get(IrSBankreporting.class, customerFunctionInfo.getCustomersId());
		 					
		 					if(bankreport!=null){
		 						
		 				//		bankreport.setIsactive(customerFunctionInfo.getReportId());
		 						bankreport.setIrMUsers(user);
		 						session.update(bankreport);
		 					}
		 					
		 					List<IrSCustomerreporting> lstreport = query_cutmrreport.list();
		 					
		 					for (IrSCustomerreporting irSCuing : lstreport) {
		 						irSCuing.setIsactive(customerFunctionInfo.getReportId_activeness());
			 					session.update(irSCuing);
			 					session.flush();
							}
		 					
		 					
		 					/*	Here  4) Transmission file setting */
		 					
		 				//	IrSCustomerReportlicensing 
		 					
		 					Query query_file_formats = session.createQuery("from IrSCustomerReportlicensing custmrptng where custmrptng.irSCustomers.customerId =:customerid ");
		 			
		 						query_file_formats.setParameter("customerid", customerFunctionInfo.getCustomersId());	
		 					
		 					List<IrSCustomerReportlicensing> file_formats_list = query_file_formats.list();
		 					
		 					for (IrSCustomerReportlicensing irSCustomerReportlicensing : file_formats_list) {
								
		 						irSCustomerReportlicensing.setIsactive(0);  // default inactive
							}
		 					
		 					Integer[] m = customerFunctionInfo.getFileFormatsId();
		 					
		 					
		 					for(IrSCustomerReportlicensing irSCustomerReportlicensing : file_formats_list) {
								
		 						for(Integer fileId : m){
		 							
		 							if(irSCustomerReportlicensing.getIrMReportfileFormats().getFileFormatId()==fileId){
		 								
		 								irSCustomerReportlicensing.setIsactive(1);    // if contains active
		 								irSCustomerReportlicensing.setIrMUsers(user);
		 								
		 								session.update(irSCustomerReportlicensing);
		 								
		 							}			 						
			 					}
							}
		 					
		 				//	over
		 					
		 					
					} else {
						
						errMsgs.add(" Validation Error :: Please Enter entries ");
						irPlusResponseDetails.setErrMsgs(errMsgs);
						irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
						irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
					}		
					
													
					}else{	
								errMsgs.add("Validation Error :: Db  Not a Contains this CustomerId Please Enter Proper Customerid");
								irPlusResponseDetails.setErrMsgs(errMsgs);
								irPlusResponseDetails.setValidationSuccess(true);
								irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
								irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);																	
					}

			}else{
				errMsgs.add(" Validation Error : Customerid Not Valid id is null ");
				irPlusResponseDetails.setErrMsgs(errMsgs);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
			}

		}
		catch (Exception e) {
		log.error("Exception in createCustomerFunctionsMngmnt",e);
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
		irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		throw new HibernateException(e);
	}
		
		return irPlusResponseDetails;
}

	@Override
	public IRPlusResponseDetails statusUpdateCustmrFunctnsById(String customerFunctionInfoId) throws BusinessException {

		return null;
	}

	@Override
	public IRPlusResponseDetails showAllCustmrFunctions() throws BusinessException {

		return null;
	}

}