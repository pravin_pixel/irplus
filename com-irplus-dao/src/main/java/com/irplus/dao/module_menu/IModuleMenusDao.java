package com.irplus.dao.module_menu;

import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.StatusIdInfo;
import com.irplus.dto.menu_modules_association.ModuleMenuBean;
import com.irplus.util.BusinessException;

public interface IModuleMenusDao {
		
    public IRPlusResponseDetails createMenuModule(ModuleMenuBean moduleMenuInfo) throws BusinessException;

	public IRPlusResponseDetails getMenuModuleById(String moduleMenuId) throws BusinessException;		

	public IRPlusResponseDetails updateMenuModule(ModuleMenuBean moduleMenuInfo) throws BusinessException;

	public IRPlusResponseDetails deleteMenuModuleById(String moduleMenuId) throws BusinessException;
		
	public IRPlusResponseDetails findAllMenuModule() throws BusinessException;
	
/*New services added*/
	
	public IRPlusResponseDetails findAllByStatus() throws BusinessException;
	
	public IRPlusResponseDetails UpdateStatus(StatusIdInfo sidInfo) throws BusinessException ;
	
	public IRPlusResponseDetails DeleteUpdateStatus(StatusIdInfo sidInfo) throws BusinessException ;
	
	public IRPlusResponseDetails createMenuModuleNoDuplicate(ModuleMenuBean moduleMenuInfo) throws BusinessException ;
	
}
