package com.irplus.dao.bankdao;

import com.irplus.dto.BankFilterInfo;
import com.irplus.dto.BankInfo;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.util.BusinessException;

public interface IBankMgmtDao {


    public IRPlusResponseDetails createBank(BankInfo bankInfo) throws BusinessException;

	public IRPlusResponseDetails listBanks(String siteId) throws BusinessException;
	
	public IRPlusResponseDetails filterBanks(BankFilterInfo bankFilterInfo) throws BusinessException;

	public IRPlusResponseDetails updateBank(BankInfo bankInfo) throws BusinessException;
	
	public IRPlusResponseDetails updateBankStatus(BankInfo bankInfo)  throws BusinessException;
	
	public IRPlusResponseDetails updateBranchStatus(BankInfo bankInfo) throws BusinessException;
	
	public IRPlusResponseDetails getBankInfo(String bankId) throws BusinessException;

	public IRPlusResponseDetails deleteBank(BankInfo bankInfo) throws BusinessException;

	public IRPlusResponseDetails addBankLicense(BankInfo bankInfo) throws BusinessException;

	public IRPlusResponseDetails filterBranches(BankFilterInfo bankFilterInfo) throws BusinessException;


    
}
