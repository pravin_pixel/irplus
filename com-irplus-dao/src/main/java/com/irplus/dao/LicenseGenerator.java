package com.irplus.dao;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.prefs.Preferences;

import javax.security.auth.x500.X500Principal;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Enumeration;

import com.irplus.dao.hibernate.entities.IRSLicense;
import com.irplus.dao.hibernate.entities.IRSSite;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.LicenseInfo;
import com.irplus.util.IRPlusConstants;

import de.schlichtherle.license.CipherParam;
import de.schlichtherle.license.KeyStoreParam;
import de.schlichtherle.license.LicenseContent;
import de.schlichtherle.license.LicenseManager;
import de.schlichtherle.license.LicenseParam;


public class LicenseGenerator
{
	private static final Logger LOGGER = Logger.getLogger(LicenseGenerator.class);


	// built by our app
	private static KeyStoreParam privateKeyStoreParam;
	private static CipherParam cipherParam;

	public LicenseGenerator()
	{

	}


	public static IRPlusResponseDetails validateLicense(String fileLocation) throws Exception
	{
		// load all the properties we need to run
		//loadOurPropertiesFile();
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		// set up an implementation of the KeyStoreParam interface that returns 
		// the information required to work with the keystore containing the private key:
		privateKeyStoreParam = new KeyStoreParam() 
		{
			public InputStream getStream() throws IOException 
			{
				final String resourceName = IRPlusConstants.KEYSTORE_FILENAME;

				File keyFile = new File(IRPlusConstants.LIC_DIRECTORY+"\\"+resourceName);
				final InputStream in = new FileInputStream(keyFile); //getClass().getResourceAsStream(resourceName);

				return in;
			}
			public String getAlias() 
			{
				return IRPlusConstants.ALIAS;
			}
			public String getStorePwd() 
			{
				return IRPlusConstants.KEYSTORE_PASSWORD;
			}
			public String getKeyPwd() 
			{
				return IRPlusConstants.KEY_PASSWORD;
			}
		};

		// Set up an implementation of the CipherParam interface to return the password to be
		// used when performing the PKCS-5 encryption.
		cipherParam = new CipherParam() 
		{
			public String getKeyPwd() 
			{
				return IRPlusConstants.CIPHER_PARAM_PASSWORD;
			}
		};

		// Set up an implementation of the LicenseParam interface.
		// Note that the subject string returned by getSubject() must match the subject property
		// of any LicenseContent instance to be used with this LicenseParam instance.
		LicenseParam licenseParam = new LicenseParam() 
		{
			public String getSubject() 
			{
				return IRPlusConstants.APP_NAME;
			}
			public Preferences getPreferences() 
			{
				// TODO why is this needed for the app that creates the license?
				//return Preferences.userNodeForPackage(LicenseServer.class);
				return null;
			}
			public KeyStoreParam getKeyStoreParam() 
			{
				return privateKeyStoreParam;
			}
			public CipherParam getCipherParam() 
			{
				return cipherParam;
			}
		};

		// create the license file
		LicenseManager lm = new LicenseManager(licenseParam);
		try
		{
			// write the file to the same directory we read it in from
			//String filename = IRPlusConstants.LIC_DIRECTORY + "\\" + licenseData.getIrssite().getSiteCode().trim() + IRPlusConstants.LIC_FILE_EXTENSION; 

			//lm.store(createLicenseContent(licenseParam,licenseData), new File(filename));
			//LOGGER.debug("File Name :"+filename);
			//String filename = "C:\\IRP9000.lic";
			verifyLicense(fileLocation,irPlusResponseDetails);
			//System.exit(EXIT_STATUS_ALL_GOOD);
		}
		catch (Exception e)
		{
			LOGGER.error("Exception :",e);

			irPlusResponseDetails.setStatusMsg(IRPlusConstants.LICENSE_VALIDATION_FAILED);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);

			//throw e;

		}
		return irPlusResponseDetails;
	}


	public static void verifyLicense(String file,IRPlusResponseDetails irPlusResponseDetails) {


		boolean install = true;//options.get("install") == null ? false : Boolean.parseBoolean(options.get("install"));
		try {
			KeyStoreParam keyStoreParam = new KeyStoreParamImpl(IRPlusConstants.KEYSTORE_FILENAME, IRPlusConstants.ALIAS, IRPlusConstants.KEYSTORE_PASSWORD, IRPlusConstants.KEY_PASSWORD);
			CipherParam cipherParam = new CipherParamImpl(IRPlusConstants.CIPHER_PARAM_PASSWORD);
			LicenseParam licenseParam = new LicenseParamImpl(IRPlusConstants.APP_NAME, keyStoreParam, cipherParam);
			LicenseManager lm = new LicenseManager(licenseParam);            

			if (install) {
				//URL url = getClass().getClassLoader().getResource("DSLicenseServer.lic");

				LOGGER.debug(file);
				lm.install(new File(file));
			}
			LicenseContent lc = lm.verify();
			LicenseInfo licenseInfo = (LicenseInfo) lc.getExtra();
			//HashMap<String,String> map = (HashMap<String,String>) lc.getExtra();
			LOGGER.debug("LicenseChecker:: licenseData " + licenseInfo);
			LOGGER.debug("LicenseChecker:: License valid till " + lc.getNotAfter());
			LOGGER.debug("LicenseChecker:: Holder info " + lc.getHolder());
			if(licenseInfo.getLicType()!=null&&licenseInfo.getLicType().equalsIgnoreCase(IRPlusConstants.VALIDITY_LICENSE))
			{
				Date today = new Date();
				if(today.before(licenseInfo.getStartDate()))
				{
					irPlusResponseDetails.setStatusMsg(IRPlusConstants.LICENSE_INACTIVE);
					irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);

				}
				else if(today.after(lc.getNotAfter()))
				{
					irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				}irPlusResponseDetails.setStatusMsg(IRPlusConstants.LICENSE_INACTIVE);
					
			}

			validateNode(licenseInfo.getMacAddr(),irPlusResponseDetails);
			if (irPlusResponseDetails.getStatusCode()!=IRPlusConstants.ERR_CODE) 
			{ 			  			
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
				irPlusResponseDetails.setLicenseInfo(licenseInfo);
			} 

		} catch (Exception e) {
			LOGGER.error("License error", e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.LICENSE_VALIDATION_FAILED);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		}
	}
	private static IRPlusResponseDetails validateNode(String macAddress,IRPlusResponseDetails irPlusResponseDetails) {
		  
		try {
			InetAddress ip = InetAddress.getLocalHost();
			//String currentMac = "90-4C-E5-3B-1A-E2";
			System.out.println("Current IP address : " + ip.getHostAddress());
			List<String> macList = new ArrayList<String>();
			String macAddr = null;

			Enumeration<NetworkInterface> networks = NetworkInterface.getNetworkInterfaces();
			while(networks.hasMoreElements()) {
				NetworkInterface network = networks.nextElement();
				byte[] mac = network.getHardwareAddress();

				if(mac != null) {
					System.out.print("Current MAC address : ");

					StringBuilder sb = new StringBuilder();
					for (int i = 0; i < mac.length; i++) {

						sb.append( String.format("%02X%s", mac[i], (i < mac.length - 1) ? "-" : ""));

					}
					System.out.println(sb.toString());
					macAddr =sb.toString();
					macList.add(macAddr);

				}
			}
			System.out.println("Is Current Mac in the List "+macList+"Ans :"+macList.contains(macAddress));

			if(!macList.contains(macAddress))
			{
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.INVALID_NODE);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);

			}

		} catch (UnknownHostException e) {
			LOGGER.error("Node error :UnknownHostException", e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.LICENSE_VALIDATION_FAILED);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);

		} catch (SocketException e){
			LOGGER.error("Node error :SocketException", e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.LICENSE_VALIDATION_FAILED);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);

		}
		return irPlusResponseDetails;
	}

}