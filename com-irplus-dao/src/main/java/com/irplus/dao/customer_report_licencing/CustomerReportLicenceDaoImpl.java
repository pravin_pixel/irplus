package com.irplus.dao.customer_report_licencing;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.irplus.dao.hibernate.entities.IrMReportfileFormats;
import com.irplus.dao.hibernate.entities.IrMUsers;
import com.irplus.dao.hibernate.entities.IrSBankBranch;
import com.irplus.dao.hibernate.entities.IrSCustomerReportlicensing;
import com.irplus.dao.hibernate.entities.IrSCustomers;
import com.irplus.dto.CustomerReportLicenceInfo;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.util.BusinessException;
import com.irplus.util.IRPlusConstants;

@Transactional
@Component

public class CustomerReportLicenceDaoImpl implements ICustomerReportLicenceDao{
	
	private static final Logger log = Logger.getLogger(CustomerReportLicenceDaoImpl.class);
	
	@Autowired
	SessionFactory sessionFactory;

	public Session getMyCurrentSession() {		
		Session session =null;
		
		try {	
		session=sessionFactory.getCurrentSession();
		}catch (HibernateException e) {
		log.error("Exception raised DaoImpl :: At current session creation time ::"+e);
		throw new HibernateException(e);
		}		
		return session; 
	}

	@Override
	public IRPlusResponseDetails createCustomerReportLicence(CustomerReportLicenceInfo customerReportLicenceInfo)
			throws BusinessException {
			
			log.info("Inside CustomerReportLicenceDaoImpl");
				
				IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
				
				try {
					IrSCustomerReportlicensing irSCustomerReportlicensing = new IrSCustomerReportlicensing();
					
					irSCustomerReportlicensing.setIrMReportfileFormats((IrMReportfileFormats)getMyCurrentSession().get(IrMReportfileFormats.class,customerReportLicenceInfo.getIrMReportfileFormatsId()));
					irSCustomerReportlicensing.setIrMUsers((IrMUsers)getMyCurrentSession().get(IrMUsers.class,customerReportLicenceInfo.getIrMUsersId()));
					irSCustomerReportlicensing.setIrSBankbranch((IrSBankBranch)getMyCurrentSession().get(IrSBankBranch.class,customerReportLicenceInfo.getIrSBankbranchId()));
					irSCustomerReportlicensing.setIrSCustomers((IrSCustomers)getMyCurrentSession().get(IrSCustomers.class,customerReportLicenceInfo.getIrSCustomersId()));
					irSCustomerReportlicensing.setIsactive(customerReportLicenceInfo.getIsactive());	
						
					getMyCurrentSession().save(irSCustomerReportlicensing);
					irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
					irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);

				}
				catch (Exception e) {
					log.error("Exception in create Menu",e);
					irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
					irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
					throw new HibernateException(e);
				}
				
				return irPlusResponseDetails;
		}


	@Override
	public IRPlusResponseDetails getCustomerReportLicenceById(String customerReportLicenceInfoId)
			throws BusinessException {
			log.info("Inside CustomerReportLicenceDaoImpl");
			
			IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
			
			IrSCustomerReportlicensing irSCustomers = null;
			
			CustomerReportLicenceInfo customerBean = new CustomerReportLicenceInfo();
			
			List<CustomerReportLicenceInfo> listCustomBean = new ArrayList<CustomerReportLicenceInfo>();
			
			try {
				
				irSCustomers =(IrSCustomerReportlicensing) getMyCurrentSession().get(IrSCustomerReportlicensing.class, Integer.parseInt(customerReportLicenceInfoId));
				
				if (irSCustomers!=null) {
					 
					
					customerBean.setIrMReportfileFormatsId(irSCustomers.getIrMReportfileFormats().getFileFormatId());
					customerBean.setIrMUsersId(irSCustomers.getIrMUsers().getUserid());
					customerBean.setIrSBankbranchId(irSCustomers.getIrSBankbranch().getBranchId());
					customerBean.setIrSCustomersId(irSCustomers.getIrSCustomers().getCustomerId());
					customerBean.setIsactive(irSCustomers.getIsactive());
					customerBean.setReportLicensingId(irSCustomers.getReportLicensingId());
					customerBean.setCreateddate(irSCustomers.getCreateddate());
					customerBean.setModifieddate(irSCustomers.getModifieddate());
					
					listCustomBean.add(customerBean);
					
					irPlusResponseDetails.setCustomerReportLicenceInfos(listCustomBean);
					irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
					irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
				} else {
					
					irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
					irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);				
				}
				
			} catch (Exception e) {

				irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				log.error("Exception Raised inside customer DaoImple :: "+e);
				throw new HibernateException(e);
			}
					
			return irPlusResponseDetails;
		}

	@Override
	public IRPlusResponseDetails updateCustomerReportLicence(CustomerReportLicenceInfo customerReportLicenceInfo)
			throws BusinessException {

		log.info("Inside CustomerReportlicensing DaoImpl");
		log.debug("Update CustomerReportlicensing  instance");
					
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		try{	
		IrSCustomerReportlicensing irSCustomers = (IrSCustomerReportlicensing) getMyCurrentSession().get(IrSCustomerReportlicensing.class, customerReportLicenceInfo.getReportLicensingId());
		
		irSCustomers.setIrMReportfileFormats((IrMReportfileFormats)getMyCurrentSession().get(IrMReportfileFormats.class,customerReportLicenceInfo.getIrMReportfileFormatsId()));
		irSCustomers.setIrMUsers((IrMUsers)getMyCurrentSession().get(IrMUsers.class,customerReportLicenceInfo.getIrMUsersId()));
		irSCustomers.setIrSBankbranch((IrSBankBranch)getMyCurrentSession().get(IrSBankBranch.class,customerReportLicenceInfo.getIrSBankbranchId()));
		irSCustomers.setIrSCustomers((IrSCustomers)getMyCurrentSession().get(IrSCustomers.class ,customerReportLicenceInfo.getIrSCustomersId()));
		irSCustomers.setIsactive(customerReportLicenceInfo.getIsactive());
		irSCustomers.setCreateddate(customerReportLicenceInfo.getCreateddat());
		
		getMyCurrentSession().saveOrUpdate(irSCustomers);
		
		irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
		irPlusResponseDetails.setValidationSuccess(true);
		
		}catch (Exception e) {

			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			log.error("Exception Raised inside CustomerReportlicensing DaoImpl :: updateCustomerReportlicensing Id : "+e);
		
			throw new HibernateException(e);
		}
		return irPlusResponseDetails;
	}


	@Override
	public IRPlusResponseDetails deleteCustomerReportLicenceById(String ReportLicenceInfoId) throws BusinessException {

		log.info("Inside ReportLicenceInfoDaoImpl");
		log.debug("deleting ReportLicenceInfoId instance");
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		try{	
			Query query = getMyCurrentSession().createQuery("delete IrSCustomerReportlicensing d where d.reportLicensingId =:role_Id");
			query.setParameter("role_Id", Integer.parseInt(ReportLicenceInfoId));
			
			int result=query.executeUpdate();
			if(result==0){
				
				log.debug("delete ReportLicenceInfoId failed to delete");
				irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
				
			}else {
				log.debug("succcessfully deleted customer :: customerId"+ReportLicenceInfoId);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			}
					
		}catch (Exception e) {
			log.error("Exception raised in delete customer",e);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			throw new HibernateException(e);
		}
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails showAllCustomerReportLicences() throws BusinessException {
		log.info("Inside CustomerReportLicencesDaoImpl");
		log.debug("Show all CustomerReportLicences instances");
		
		IRPlusResponseDetails irPlusResponseDetails = new  IRPlusResponseDetails();
		List<CustomerReportLicenceInfo> listCustomBean = new ArrayList<CustomerReportLicenceInfo>();
		try {
			
			Criteria criteria = getMyCurrentSession().createCriteria(IrSCustomerReportlicensing.class);
			
			List<IrSCustomerReportlicensing> list = criteria.list();
			
			if (!list.isEmpty()) {
							
				for (IrSCustomerReportlicensing irSCustomers : list) {
			
					CustomerReportLicenceInfo customerBean = new CustomerReportLicenceInfo();
					
					customerBean.setIrMReportfileFormatsId(irSCustomers.getIrMReportfileFormats().getFileFormatId());
					customerBean.setIrMUsersId(irSCustomers.getIrMUsers().getUserid());
					customerBean.setIrSBankbranchId(irSCustomers.getIrSBankbranch().getBranchId());
					customerBean.setIrSCustomersId(irSCustomers.getIrSCustomers().getCustomerId());
					customerBean.setIsactive(irSCustomers.getIsactive());
					customerBean.setModifieddate(irSCustomers.getModifieddate());
					customerBean.setReportLicensingId(irSCustomers.getReportLicensingId());
					customerBean.setCreateddate(irSCustomers.getCreateddate());
					
					listCustomBean.add(customerBean);
				}
				irPlusResponseDetails.setCustomerReportLicenceInfos(listCustomBean);
			
				irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
				
			} else {
				log.debug("Customers not available :: Empty Data :: Data required to show roles");
				irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
				
			}
			
		} catch (Exception e) {
			log.error("Exception raised in show list customers",e);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			
			throw new HibernateException(e);
		}
		
		return irPlusResponseDetails;
		}	

}
