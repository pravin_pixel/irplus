package com.irplus.dao.customer_report_licencing;

import com.irplus.dto.CustomerReportLicenceInfo;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.util.BusinessException;

public interface ICustomerReportLicenceDao {

	 	public IRPlusResponseDetails createCustomerReportLicence(CustomerReportLicenceInfo  customerReportLicenceInfo) throws BusinessException;

		public IRPlusResponseDetails getCustomerReportLicenceById(String customerReportLicenceInfoId) throws BusinessException;
		
		public IRPlusResponseDetails updateCustomerReportLicence(CustomerReportLicenceInfo  customerReportLicenceInfo) throws BusinessException;

		public IRPlusResponseDetails deleteCustomerReportLicenceById(String ReportLicenceInfoId) throws BusinessException;
		
		public IRPlusResponseDetails showAllCustomerReportLicences() throws BusinessException;
		
}
