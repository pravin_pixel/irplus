package com.irplus.dao.module;

import com.irplus.dto.menu.MenuResponseDetails;
import com.irplus.dto.module.ModuleBean;
import com.irplus.dto.module.ModuleResponseDetails;
import com.irplus.util.BusinessException;

public interface ModulesDao {
		
    public ModuleResponseDetails createModule(ModuleBean moduleInfo) throws BusinessException;

	public ModuleResponseDetails getModuleById(String menuid) throws BusinessException;		

	public ModuleResponseDetails updateModule(ModuleBean moduleInfo) throws BusinessException;

	public ModuleResponseDetails deleteModuleById(String moduleId) throws BusinessException;
		
	public ModuleResponseDetails findAllModules() throws BusinessException;
	
}
