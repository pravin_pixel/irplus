package com.irplus.dao.hibernate.entities;

import java.util.Date;

public class IrSCustomerOtherlicensing implements java.io.Serializable {

	private Integer otherLicensingId;	
	private IrMBusinessProcess irMBusinessprocess;	
	private IrMUsers irMUsers;
	private IrSBankBranch irSBankBranch;
	private IrSCustomers irSCustomers;	
	private int isAvail;
	
	private Integer isactive;
	private Integer notifications;
	
	private Date createddate;
	private Date modifieddate;	
	
	public IrSCustomerOtherlicensing() {
	}

	public IrSCustomerOtherlicensing(IrMBusinessProcess irMBusinessprocess, IrMUsers irMUsers,
			IrSBankBranch irSBankbranch, IrSCustomers irSCustomers, Integer isactive, Date createddate,
			Date modifieddate) {
		this.irMBusinessprocess = irMBusinessprocess;
		this.irMUsers = irMUsers;
		this.irSBankBranch = irSBankbranch;
		this.irSCustomers = irSCustomers;
		this.isactive = isactive;
		this.createddate = createddate;
		this.modifieddate = modifieddate;
	}

	public Integer getOtherLicensingId() {
		return this.otherLicensingId;
	}

	public void setOtherLicensingId(Integer otherLicensingId) {
		this.otherLicensingId = otherLicensingId;
	}

	public IrMBusinessProcess getIrMBusinessprocess() {
		return this.irMBusinessprocess;
	}

	public void setIrMBusinessprocess(IrMBusinessProcess irMBusinessprocess) {
		this.irMBusinessprocess = irMBusinessprocess;
	}

	public IrMUsers getIrMUsers() {
		return this.irMUsers;
	}

	public void setIrMUsers(IrMUsers irMUsers) {
		this.irMUsers = irMUsers;
	}

	public IrSBankBranch getIrSBankBranch() {
		return irSBankBranch;
	}
	
	public Integer getNotifications() {
		return notifications;
	}

	public void setNotifications(Integer notifications) {
		this.notifications = notifications;
	}

	public void setIrSBankBranch(IrSBankBranch irSBankBranch) {
		this.irSBankBranch = irSBankBranch;
	}

	public IrSCustomers getIrSCustomers() {
		return this.irSCustomers;
	}

	public void setIrSCustomers(IrSCustomers irSCustomers) {
		this.irSCustomers = irSCustomers;
	}

	public Integer getIsactive() {
		return this.isactive;
	}

	public void setIsactive(Integer isactive) {
		this.isactive = isactive;
	}
	public int getIsAvail() {
		return isAvail;
	}

	public void setIsAvail(int isAvail) {
		this.isAvail = isAvail;
	}

	public Date getCreateddate() {
		return this.createddate;
	}

	public void setCreateddate(Date createddate) {
		this.createddate = createddate;
	}

	public Date getModifieddate() {
		return this.modifieddate;
	}

	public void setModifieddate(Date modifieddate) {
		this.modifieddate = modifieddate;
	}

}