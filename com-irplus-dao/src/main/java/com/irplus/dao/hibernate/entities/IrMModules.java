package com.irplus.dao.hibernate.entities;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class IrMModules implements java.io.Serializable {

	private Integer moduleid;
	private String modulename;
	private String moduleicon;
	private String description;
	private String modulepath;
	private Integer isreport;
	private Integer userid;
	private Date createddate;
	private Date modifieddate;
	private Integer isactive;
	
	private Set irMModulemenuses = new LinkedHashSet(0);
//	private Set irSBankreportings = new HashSet(0);

	private List<IrMModulemenus> moduleMenusList = new ArrayList<IrMModulemenus>();
	
	
	
	public List<IrMModulemenus> getModuleMenusList() {
		return moduleMenusList;
	}

	public void setModuleMenusList(List<IrMModulemenus> moduleMenusList) {
		this.moduleMenusList = moduleMenusList;
	}

	public IrMModules() {
	}

	public IrMModules(//IrMStatus irMStatus, 
			String modulename, String moduleicon, String description,
			String modulepath, Integer isreport, Integer userid, Date createddate, Date modifieddate,
			Set irMModulemenuses
	//		, Set irSBankreportings
			) {
	
		//	this.irMStatus = irMStatus;
		this.modulename = modulename;
		this.moduleicon = moduleicon;
		this.description = description;
		this.modulepath = modulepath;
		this.isreport = isreport;
		this.userid = userid;
		this.createddate = createddate;
		this.modifieddate = modifieddate;
		this.irMModulemenuses = irMModulemenuses;
		
//		this.irSBankreportings = irSBankreportings;
	}

	public Integer getModuleid() {
		return this.moduleid;
	}

	public void setModuleid(Integer moduleid) {
		this.moduleid = moduleid;
	}


	public Integer getIsactive() {
		return isactive;
	}

	public void setIsactive(Integer isactive) {
		this.isactive = isactive;
	}

	public String getModulename() {
		return this.modulename;
	}

	public void setModulename(String modulename) {
		this.modulename = modulename;
	}

	public String getModuleicon() {
		return this.moduleicon;
	}

	public void setModuleicon(String moduleicon) {
		this.moduleicon = moduleicon;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getModulepath() {
		return this.modulepath;
	}

	public void setModulepath(String modulepath) {
		this.modulepath = modulepath;
	}

	public Integer getIsreport() {
		return this.isreport;
	}

	public void setIsreport(Integer isreport) {
		this.isreport = isreport;
	}

	public Integer getUserid() {
		return this.userid;
	}

	public void setUserid(Integer userid) {
		this.userid = userid;
	}

	public Date getCreateddate() {
		return this.createddate;
	}

	public void setCreateddate(Date createddate) {
		this.createddate = createddate;
	}

	public Date getModifieddate() {
		return this.modifieddate;
	}

	public void setModifieddate(Date modifieddate) {
		this.modifieddate = modifieddate;
	}

	public Set getIrMModulemenuses() {
		return this.irMModulemenuses;
	}

	public void setIrMModulemenuses(Set irMModulemenuses) {
		this.irMModulemenuses = irMModulemenuses;
	}

/*
	public Set getIrSBankreportings() {
		return this.irSBankreportings;
	}

	public void setIrSBankreportings(Set irSBankreportings) {
		this.irSBankreportings = irSBankreportings;
	}
*/

}