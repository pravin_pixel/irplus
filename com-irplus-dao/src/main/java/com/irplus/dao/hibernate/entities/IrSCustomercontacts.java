package com.irplus.dao.hibernate.entities;

import java.util.Date;

import com.irplus.dto.ContactInfo;
import com.irplus.dto.CustomerContactsBean;

import java.io.Serializable;

public class IrSCustomercontacts implements java.io.Serializable {

	private int customerContactId; //This is primarykey
	
	private IrMUsers irMUsers;
	
	private IrSCustomers irSCustomers;
	
	private String firstName;
	private String lastName;
	private String contactRole;
	private String contactNo;
	private String emailId;
	private String address1;
	private String address2;
	private String country;
	private String state;
	private String city;
	private String zipcode;
	private Character isdefault;
	private Integer isactive;
	private Date createddate;
	private Date modifieddate;

	
	public IrSCustomercontacts(){}
	
	
public IrSCustomercontacts(CustomerContactsBean contactInfo) {
		
		
		this.firstName = contactInfo.getFirstName();
		this.lastName= contactInfo.getLastName();
		this.contactRole= contactInfo.getContactRole();
		this.contactNo= contactInfo.getContactNo();
		this.emailId= contactInfo.getEmailId();
		this.address1= contactInfo.getAddress1();
		this.address2= contactInfo.getAddress2();
		this.isdefault= contactInfo.getIsdefault();
		this.country= contactInfo.getCountry();
		this.state= contactInfo.getState();
		this.city= contactInfo.getCity();
		this.zipcode= contactInfo.getZipcode();
		this.isactive= contactInfo.getIsactive();
		
	}
	public IrSCustomercontacts(int customerContactId) {
		this.customerContactId = customerContactId;
	}

 
	
	
	public int getCustomerContactId() {
		return this.customerContactId;
	}

	public void setCustomerContactId(int customerContactId) {
		this.customerContactId = customerContactId;
	}

	public IrMUsers getIrMUsers() {
		return this.irMUsers;
	}

	public void setIrMUsers(IrMUsers irMUsers) {
		this.irMUsers = irMUsers;
	}

	public IrSCustomers getIrSCustomers() {
		return this.irSCustomers;
	}

	public void setIrSCustomers(IrSCustomers irSCustomers) {
		this.irSCustomers = irSCustomers;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getContactRole() {
		return this.contactRole;
	}

	public void setContactRole(String contactRole) {
		this.contactRole = contactRole;
	}

	public String getContactNo() {
		return this.contactNo;
	}

	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}

	public String getEmailId() {
		return this.emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getAddress1() {
		return this.address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return this.address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getZipcode() {
		return this.zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public Character getIsdefault() {
		return this.isdefault;
	}

	public void setIsdefault(Character isdefault) {
		this.isdefault = isdefault;
	}

	public Integer getIsactive() {
		return this.isactive;
	}

	public void setIsactive(Integer isactive) {
		this.isactive = isactive;
	}

	public Date getCreateddate() {
		return this.createddate;
	}

	public void setCreateddate(Date createddate) {
		this.createddate = createddate;
	}

	public Date getModifieddate() {
		return this.modifieddate;
	}

	public void setModifieddate(Date modifieddate) {
		this.modifieddate = modifieddate;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("IrSCustomercontacts [customerContactId=");
		builder.append(customerContactId);
		builder.append(", firstName=");
		builder.append(firstName);
		builder.append(", lastName=");
		builder.append(lastName);
		builder.append(", contactRole=");
		builder.append(contactRole);
		builder.append(", contactNo=");
		builder.append(contactNo);
		builder.append(", emailId=");
		builder.append(emailId);
		builder.append(", address1=");
		builder.append(address1);
		builder.append(", address2=");
		builder.append(address2);
		builder.append(", isdefault=");
		builder.append(isdefault);
		builder.append(", country=");
		builder.append(country);
		builder.append(", state=");
		builder.append(state);
		builder.append(", city=");
		builder.append(city);
		builder.append(", zipcode=");
		builder.append(zipcode);
		builder.append(", isactive=");
		builder.append(isactive);
		builder.append(", irMUsers=");
		builder.append(irMUsers);
		builder.append(", createddate=");
		builder.append(createddate);
		builder.append(", modifieddate=");
		builder.append(modifieddate);
		builder.append("]");
		return builder.toString();
	}
}
