package com.irplus.dao.hibernate.entities;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;


public class IrSFormsetup implements java.io.Serializable {

	private Integer formSetupId;
	private IrMUsers irMUsers;
	private IrSBankBranch irSBankbranch;
	private IrSCustomers irSCustomers;
	
	private String formCode;
	private String formName;
	private String batchClassDataentry;
	private String batchClassValidation;
	private String watchFolderDataentry;
	private String watchFolderValidation;
	
	
	private Boolean isdefault;
	private Integer isactive;
	private Date createddate;
	private Date modifieddate;
	
	private Set irSFormvalidations = new HashSet(0);

	public IrSFormsetup() {
	}

	public IrSFormsetup(int formSetupId) {
		this.formSetupId = formSetupId;
	}

	public IrSFormsetup(Integer formSetupId, IrMUsers irMUsers, IrSBankBranch irSBankbranch, IrSCustomers irSCustomers,
			String formCode, String formName, String batchClassDataentry,
			String batchClassValidation, String watchFolderDataentry, String watchFolderValidation,
			Boolean isdefault, Integer isactive, Date createddate, Date modifieddate, Set irSFormvalidations) {
		this.formSetupId = formSetupId;
		this.irMUsers = irMUsers;
		this.irSBankbranch = irSBankbranch;
		this.irSCustomers = irSCustomers;
		this.formCode = formCode;
		this.formName = formName;
		this.batchClassDataentry = batchClassDataentry;
		this.batchClassValidation = batchClassValidation;
		this.watchFolderDataentry = watchFolderDataentry;
		this.watchFolderValidation = watchFolderValidation;
		this.isdefault = isdefault;
		this.isactive = isactive;
		this.createddate = createddate;
		this.modifieddate = modifieddate;
		this.irSFormvalidations = irSFormvalidations;
	}


	public Integer getFormSetupId() {
		return formSetupId;
	}

	public void setFormSetupId(Integer formSetupId) {
		this.formSetupId = formSetupId;
	}

	public IrMUsers getIrMUsers() {
		return this.irMUsers;
	}

	public void setIrMUsers(IrMUsers irMUsers) {
		this.irMUsers = irMUsers;
	}


	public IrSBankBranch getIrSBankbranch() {
		return irSBankbranch;
	}

	public void setIrSBankbranch(IrSBankBranch irSBankbranch) {
		this.irSBankbranch = irSBankbranch;
	}

	public IrSCustomers getIrSCustomers() {
		return this.irSCustomers;
	}

	public void setIrSCustomers(IrSCustomers irSCustomers) {
		this.irSCustomers = irSCustomers;
	}

	public String getFormCode() {
		return this.formCode;
	}

	public void setFormCode(String formCode) {
		this.formCode = formCode;
	}

	public String getFormName() {
		return this.formName;
	}

	public void setFormName(String formName) {
		this.formName = formName;
	}

	public String getBatchClassDataentry() {
		return this.batchClassDataentry;
	}

	public void setBatchClassDataentry(String batchClassDataentry) {
		this.batchClassDataentry = batchClassDataentry;
	}

	public String getBatchClassValidation() {
		return this.batchClassValidation;
	}

	public void setBatchClassValidation(String batchClassValidation) {
		this.batchClassValidation = batchClassValidation;
	}

	public String getWatchFolderDataentry() {
		return this.watchFolderDataentry;
	}

	public void setWatchFolderDataentry(String watchFolderDataentry) {
		this.watchFolderDataentry = watchFolderDataentry;
	}

	public String getWatchFolderValidation() {
		return this.watchFolderValidation;
	}

	public void setWatchFolderValidation(String watchFolderValidation) {
		this.watchFolderValidation = watchFolderValidation;
	}

	public Boolean getIsdefault() {
		return this.isdefault;
	}

	public void setIsdefault(Boolean isdefault) {
		this.isdefault = isdefault;
	}

	public Integer getIsactive() {
		return this.isactive;
	}

	public void setIsactive(Integer isactive) {
		this.isactive = isactive;
	}

	public Date getCreateddate() {
		return this.createddate;
	}

	public void setCreateddate(Date createddate) {
		this.createddate = createddate;
	}

	public Date getModifieddate() {
		return this.modifieddate;
	}

	public void setModifieddate(Date modifieddate) {
		this.modifieddate = modifieddate;
	}

	public Set getIrSFormvalidations() {
		return this.irSFormvalidations;
	}

	public void setIrSFormvalidations(Set irSFormvalidations) {
		this.irSFormvalidations = irSFormvalidations;
	}

}
