package com.irplus.dao.hibernate.entities;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class IrMRoles implements java.io.Serializable {


	private Integer roleid;
	private int isactive;
	private IrMUsers irMUsers;
	private String rolename;
	private Integer isrestricted;
	private Boolean bankVisibility;
	private Date createddate;
	private Date modifieddate;
	private Set irMRolepermissionses = new HashSet(0);

	public IrMRoles() {
	}

/*	public IrMRoles(IrMStatus irMStatus, String rolename, Date createddate) {
		this.irMStatus = irMStatus;
		this.rolename = rolename;
		this.createddate = createddate;
	}*/

	/*public IrMRoles(IrMStatus irMStatus, IrMUsers irMUsers, String rolename, Integer isrestricted,
			Boolean bankVisibility, Date createddate, Date modifieddate, Set irMRolepermissionses) {
		this.irMStatus = irMStatus;
		this.irMUsers = irMUsers;
		this.rolename = rolename;
		this.isrestricted = isrestricted;
		this.bankVisibility = bankVisibility;
		this.createddate = createddate;
		this.modifieddate = modifieddate;
	//	this.irMRolepermissionses = irMRolepermissionses;
	}*/

	public Integer getRoleid() {
		return this.roleid;
	}

	public void setRoleid(Integer roleid) {
		this.roleid = roleid;
	}

	public int getIsactive() {
		return isactive;
	}

	public void setIsactive(int isactive) {
		this.isactive = isactive;
	}

	public IrMUsers getIrMUsers() {
		return this.irMUsers;
	}

	public void setIrMUsers(IrMUsers irMUsers) {
		this.irMUsers = irMUsers;
	}

	public String getRolename() {
		return this.rolename;
	}

	public void setRolename(String rolename) {
		this.rolename = rolename;
	}

	public Integer getIsrestricted() {
		return this.isrestricted;
	}

	public void setIsrestricted(Integer isrestricted) {
		this.isrestricted = isrestricted;
	}

	public Boolean getBankVisibility() {
		return this.bankVisibility;
	}

	public void setBankVisibility(Boolean bankVisibility) {
		this.bankVisibility = bankVisibility;
	}

	public Date getCreateddate() {
		return this.createddate;
	}

	public void setCreateddate(Date createddate) {
		this.createddate = createddate;
	}

	public Date getModifieddate() {
		return this.modifieddate;
	}

	public void setModifieddate(Date modifieddate) {
		this.modifieddate = modifieddate;
	}

	public Set getIrMRolepermissionses() {
		return this.irMRolepermissionses;
	}

	public void setIrMRolepermissionses(Set irMRolepermissionses) {
		this.irMRolepermissionses = irMRolepermissionses;
	}

}
