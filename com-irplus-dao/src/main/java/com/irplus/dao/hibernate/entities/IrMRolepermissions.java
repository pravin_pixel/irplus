package com.irplus.dao.hibernate.entities;

import java.util.Date;

public class IrMRolepermissions implements java.io.Serializable {

	private Integer permissionid;
	private IrMModulemenus irMModulemenus;
	private IrMRoles irMRoles;
//	private IrMStatus irMStatus;
	private IrMUsers irMUsers;
	private Boolean addprm;
	private Boolean editprm;
	private Boolean deleteprm;
	private Boolean viewprm;
	private Boolean approval;
	private Integer isactive;
	private Date createddate;
	private Date modifieddate;

	public IrMRolepermissions() {
	}

/*	public IrMRolepermissions(IrMModulemenus irMModulemenus, IrMRoles irMRoles, IrMStatus irMStatus, IrMUsers irMUsers,
			Boolean addprm, Boolean editprm, Boolean deleteprm, Boolean viewprm, Boolean approval, Date createddate,
			Date modifieddate) {
		this.irMModulemenus = irMModulemenus;
		this.irMRoles = irMRoles;
		this.irMStatus = irMStatus;
		this.irMUsers = irMUsers;
		this.addprm = addprm;
		this.editprm = editprm;
		this.deleteprm = deleteprm;
		this.viewprm = viewprm;
		this.approval = approval;
		this.createddate = createddate;
		this.modifieddate = modifieddate;
	}
*/
	public Integer getPermissionid() {
		return this.permissionid;
	}

	public void setPermissionid(Integer permissionid) {
		this.permissionid = permissionid;
	}

	public IrMModulemenus getIrMModulemenus() {
		return this.irMModulemenus;
	}

	public void setIrMModulemenus(IrMModulemenus irMModulemenus) {
		this.irMModulemenus = irMModulemenus;
	}

	public IrMRoles getIrMRoles() {
		return this.irMRoles;
	}

	public void setIrMRoles(IrMRoles irMRoles) {
		this.irMRoles = irMRoles;
	}

	public Integer getIsactive() {
		return isactive;
	}

	public void setIsactive(Integer isactive) {
		this.isactive = isactive;
	}

	public IrMUsers getIrMUsers() {
		return this.irMUsers;
	}

	public void setIrMUsers(IrMUsers irMUsers) {
		this.irMUsers = irMUsers;
	}

	public Boolean getAddprm() {
		return this.addprm;
	}

	public void setAddprm(Boolean addprm) {
		this.addprm = addprm;
	}

	public Boolean getEditprm() {
		return this.editprm;
	}

	public void setEditprm(Boolean editprm) {
		this.editprm = editprm;
	}

	public Boolean getDeleteprm() {
		return this.deleteprm;
	}

	public void setDeleteprm(Boolean deleteprm) {
		this.deleteprm = deleteprm;
	}

	public Boolean getViewprm() {
		return this.viewprm;
	}

	public void setViewprm(Boolean viewprm) {
		this.viewprm = viewprm;
	}

	public Boolean getApproval() {
		return this.approval;
	}

	public void setApproval(Boolean approval) {
		this.approval = approval;
	}

	public Date getCreateddate() {
		return this.createddate;
	}

	public void setCreateddate(Date createddate) {
		this.createddate = createddate;
	}

	public Date getModifieddate() {
		return this.modifieddate;
	}

	public void setModifieddate(Date modifieddate) {
		this.modifieddate = modifieddate;
	}

}
