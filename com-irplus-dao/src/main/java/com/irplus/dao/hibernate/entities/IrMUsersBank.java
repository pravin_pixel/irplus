package com.irplus.dao.hibernate.entities;

import java.util.Date;

import com.irplus.dto.UserBankBean;

public class IrMUsersBank implements java.io.Serializable {
	
	
	private int adminId;
	
	private int userBankId;	
	private Byte isdefault;
	private Integer isactive;
	private Date createddate;
	private Date modifieddate;
	private IrMUsers irMUsers;
	private IrSBankBranch irSBankbranch;
	private IRSBank bank;
	public IRSBank getBank() {
		return bank;
	}
	public void setBank(IRSBank bank) {
		this.bank = bank;
	}
	public IrMUsersBank() {
	}
	public IrMUsersBank(int userBankId){
		this.userBankId = userBankId;
	}
	public int getUserBankId() {
		return this.userBankId;
	}
	public void setUserBankId(int userBankId) {
		this.userBankId = userBankId;
	}
	public IrMUsers getIrMUsers() {
		return irMUsers;
	}
	public void setIrMUsers(IrMUsers irMUsers) {
		this.irMUsers = irMUsers;
	}
	public int getAdminId() {
		return adminId;
	}
	public void setAdminId(int adminId) {
		this.adminId = adminId;
	}
	public IrSBankBranch getIrSBankbranch() {
		return irSBankbranch;
	}

	public void setIrSBankbranch(IrSBankBranch irSBankbranch) {
		this.irSBankbranch = irSBankbranch;
	}

	public Byte getIsdefault() {
		return this.isdefault;
	}

	public void setIsdefault(Byte isdefault) {
		this.isdefault = isdefault;
	}

	public Integer getIsactive() {
		return this.isactive;
	}

	public void setIsactive(Integer isactive) {
		this.isactive = isactive;
	}

	public Date getCreateddate() {
		return this.createddate;
	}

	public void setCreateddate(Date createddate) {
		this.createddate = createddate;
	}

	public Date getModifieddate() {
		return this.modifieddate;
	}

	public void setModifieddate(Date modifieddate) {
		this.modifieddate = modifieddate;
	}

}