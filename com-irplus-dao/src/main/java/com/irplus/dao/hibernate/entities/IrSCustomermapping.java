package com.irplus.dao.hibernate.entities;

import java.sql.Timestamp;

public class IrSCustomermapping implements java.io.Serializable {

	private Integer customerMapId;

	/*private String nachaFields;*/
	private String referenceData;
	private IrMUsers irMUsers;
	private IrSBankBranch irSBankbranch;
	private IrSCustomers irSCustomers;
	
	private Integer isactive; 
	
	private Timestamp createddate;
	private Timestamp modifieddate;
	
	
	private IrSNatchaField irsNatchaField;

	
	public IrSNatchaField getIrsNatchaField() {
		return irsNatchaField;
	}

	public void setIrsNatchaField(IrSNatchaField irsNatchaField) {
		this.irsNatchaField = irsNatchaField;
	}



	public String getReferenceData() {
		return referenceData;
	}

	public void setReferenceData(String referenceData) {
		this.referenceData = referenceData;
	}

	public IrMUsers getIrMUsers() {
		return irMUsers;
	}

	public void setIrMUsers(IrMUsers irMUsers) {
		this.irMUsers = irMUsers;
	}

	public IrSBankBranch getIrSBankbranch() {
		return irSBankbranch;
	}

	public void setIrSBankbranch(IrSBankBranch irSBankbranch) {
		this.irSBankbranch = irSBankbranch;
	}

	public IrSCustomers getIrSCustomers() {
		return irSCustomers;
	}

	public void setIrSCustomers(IrSCustomers irSCustomers) {
		this.irSCustomers = irSCustomers;
	}

	public Timestamp getCreateddate() {
		return createddate;
	}

	public void setCreateddate(Timestamp createddate) {
		this.createddate = createddate;
	}

	public Timestamp getModifieddate() {
		return modifieddate;
	}

	public void setModifieddate(Timestamp modifieddate) {
		this.modifieddate = modifieddate;
	}

	public Integer getIsactive() {
		return isactive;
	}

	public void setIsactive(Integer isactive) {
		this.isactive = isactive;
	}

	public IrSCustomermapping() {
	}

	public Integer getCustomerMapId() {
		return this.customerMapId;
	}

	public void setCustomerMapId(Integer customerMapId) {
		this.customerMapId = customerMapId;
	}
	
}
