package com.irplus.dao.hibernate.entities;


import java.sql.Timestamp;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;


public class IRSLicense implements java.io.Serializable {

    private static final long serialVersionUID = -3176074231815769436L;
	
	
    private Long licenseId;
    private String licenseno;
    private String keyvalue;
    private byte[] licensetype;
    private byte[] startdate;
    private byte[] enddate;
    private Integer hasSubentity;
    private IrMUsers user;
    private IRSSite site;
    
	private int isactive;
    private Timestamp createddate;
    private Timestamp modifieddate;
	
    private Set irMBusinessProcess = new HashSet(0);
    
    private Set irMFileTypes = new HashSet(0);


	public Set getIrMFileTypes() {
		return irMFileTypes;
	}

	public void setIrMFileTypes(Set irMFileTypes) {
		this.irMFileTypes = irMFileTypes;
	}

	public Set getIrMBusinessProcess() {
		return irMBusinessProcess;
	}

	public void setIrMBusinessProcess(Set irMBusinessProcess) {
		this.irMBusinessProcess = irMBusinessProcess;
	}

	public Long getLicenseId() {
		return licenseId;
	}
	
    public IRSSite getSite() {
		return site;
	}
	public void setSite(IRSSite site) {
		this.site = site;
	}
	
	public void setLicenseId(Long licenseId) {
		this.licenseId = licenseId;
	}
	public String getLicenseno() {
		return licenseno;
	}
	public void setLicenseno(String licenseno) {
		this.licenseno = licenseno;
	}
	public String getKeyvalue() {
		return keyvalue;
	}
	public void setKeyvalue(String keyvalue) {
		this.keyvalue = keyvalue;
	}
	public byte[] getLicensetype() {
		return licensetype;
	}
	public void setLicensetype(byte[] licensetype) {
		this.licensetype = licensetype;
	}
	public byte[] getStartdate() {
		return startdate;
	}
	public void setStartdate(byte[] startdate) {
		this.startdate = startdate;
	}
	public byte[] getEnddate() {
		return enddate;
	}
	public void setEnddate(byte[] enddate) {
		this.enddate = enddate;
	}
	public Integer getHasSubentity() {
		return hasSubentity;
	}
	public void setHasSubentity(Integer hasSubentity) {
		this.hasSubentity = hasSubentity;
	}
	public IrMUsers getUser() {
		return user;
	}
	public void setUser(IrMUsers user) {
		this.user = user;
	}
	public int getIsactive() {
		return isactive;
	}
	public void setIsactive(int isactive) {
		this.isactive = isactive;
	}
	public Timestamp getCreateddate() {
		return createddate;
	}
	public void setCreateddate(Timestamp createddate) {
		this.createddate = createddate;
	}
	public Timestamp getModifieddate() {
		return modifieddate;
	}
	public void setModifieddate(Timestamp modifieddate) {
		this.modifieddate = modifieddate;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("IRSLicense [licenseId=").append(licenseId).append(", licenseno=").append(licenseno)
				.append(", keyvalue=").append(keyvalue).append(", licensetype=").append(Arrays.toString(licensetype))
				.append(", startdate=").append(Arrays.toString(startdate)).append(", enddate=")
				.append(Arrays.toString(enddate)).append(", hasSubentity=").append(hasSubentity).append(", user=")
				.append(user).append(", isactive=").append(isactive).append(", irMBusinessProcess=").append(irMBusinessProcess).append(", createddate=").append(createddate)
				.append(", site=").append(site).append(", irMFileTypes=").append(irMFileTypes)
				.append(", modifieddate=").append(modifieddate).append("]");
		return builder.toString();
	}
	
	
	
	
}
