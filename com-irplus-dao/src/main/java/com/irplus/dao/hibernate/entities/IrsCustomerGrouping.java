package com.irplus.dao.hibernate.entities;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class IrsCustomerGrouping {

	private Integer customerGrpId ;
	
	private Integer isactive ;
	private String customerGroupName ;
	private Integer userid;
	private String createddate;
	private String createdtime;
	/*private Timestamp modifieddate;*/
	private IrMUsers irMUsers;
	

	public String getCreateddate() {
		return createddate;
	}
	public void setCreateddate(String createddate) {
		this.createddate = createddate;
	}
	public String getCreatedtime() {
		return createdtime;
	}
	public void setCreatedtime(String createdtime) {
		this.createdtime = createdtime;
	}
	
	public Set getIrSCustomergroupingdet() {
		return irSCustomergroupingdet;
	}
	public void setIrSCustomergroupingdet(Set irSCustomergroupingdet) {
		this.irSCustomergroupingdet = irSCustomergroupingdet;
	}
	private Set irSCustomergroupingdet = new HashSet(0);
	
	
	
	public IrMUsers getIrMUsers() {
		return irMUsers;
	}
	public void setIrMUsers(IrMUsers irMUsers) {
		this.irMUsers = irMUsers;
	}

	
	
	public Integer getCustomerGrpId() {
		return customerGrpId;
	}
	public void setCustomerGrpId(Integer customerGrpId) {
		this.customerGrpId = customerGrpId;
	}
	public Integer getIsactive() {
		return isactive;
	}
	public void setIsactive(Integer isactive) {
		this.isactive = isactive;
	}
	public String getCustomerGroupName() {
		return customerGroupName;
	}
	public void setCustomerGroupName(String customerGroupName) {
		this.customerGroupName = customerGroupName;
	}
	public Integer getUserid() {
		return userid;
	}
	public void setUserid(Integer userid) {
		this.userid = userid;
	}

		
}