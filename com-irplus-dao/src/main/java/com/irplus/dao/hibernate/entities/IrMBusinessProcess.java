package com.irplus.dao.hibernate.entities;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import com.irplus.dto.ContactInfo;
import com.irplus.dto.IRPlusBusinessProcess;

public class IrMBusinessProcess {

	private Long businessProcessId;
	//private BankBranch bankBranch;
	private String processName;
	private Long licenseId;
	private Character hasVisibility;
	private Character isparent;
	private int isactive;
	private IrMUsers user;
	private Timestamp createddate;
    private Timestamp modifieddate;
    private IRSLicense iRSLicense;

	private Set irSCustomerOtherlicensings = new HashSet(0);
	private Set irSCustomerlicensings = new HashSet(0);
    private Set iRSBankLicensing = new HashSet(0);

	
	public Set getiRSBankLicensing() {
		return iRSBankLicensing;
	}
	public void setiRSBankLicensing(Set iRSBankLicensing) {
		this.iRSBankLicensing = iRSBankLicensing;
	}
	public IRSLicense getiRSLicense() {
		return iRSLicense;
	}
	public void setiRSLicense(IRSLicense iRSLicense) {
		this.iRSLicense = iRSLicense;
	}
	public IrMBusinessProcess() {
		
	}
	public IrMBusinessProcess(IRPlusBusinessProcess irPlusBusinessProcess) {
		this.processName = irPlusBusinessProcess.getProcessName();
		this.hasVisibility = irPlusBusinessProcess.getHasVisibility();
		this.isparent = irPlusBusinessProcess.getIsparent();
	}
	public Long getBusinessProcessId() {
		return businessProcessId;
	}
	public void setBusinessProcessId(Long businessProcessId) {
		this.businessProcessId = businessProcessId;
	}
	public String getProcessName() {
		return processName;
	}
	public void setProcessName(String processName) {
		this.processName = processName;
	}
	
	
	public Long getLicenseId() {
		return licenseId;
	}
	public void setLicenseId(Long licenseId) {
		this.licenseId = licenseId;
	}
	public Character getHasVisibility() {
		return hasVisibility;
	}
	public void setHasVisibility(Character hasVisibility) {
		this.hasVisibility = hasVisibility;
	}
	public Character getIsparent() {
		return isparent;
	}
	public void setIsparent(Character isparent) {
		this.isparent = isparent;
	}
	public int getIsactive() {
		return isactive;
	}
	public void setIsactive(int isactive) {
		this.isactive = isactive;
	}
	public IrMUsers getUser() {
		return user;
	}
	public void setUser(IrMUsers user) {
		this.user = user;
	}
	public Timestamp getCreateddate() {
		return createddate;
	}
	public void setCreateddate(Timestamp createddate) {
		this.createddate = createddate;
	}
	public Timestamp getModifieddate() {
		return modifieddate;
	}
	public void setModifieddate(Timestamp modifieddate) {
		this.modifieddate = modifieddate;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("IrMBusinessProcess [businessProcessId=").append(businessProcessId).append(", processName=")
				.append(processName).append(", licenseId=").append(licenseId).append(", hasVisibility=")
				.append(hasVisibility).append(", isparent=").append(isparent).append(", isactive=").append(isactive)
				.append(", user=").append(user).append(", iRSBankLicensing=").append(iRSBankLicensing).append(", iRSLicense=").append(iRSLicense).append(", createddate=").append(createddate).append(", modifieddate=")
				.append(modifieddate).append("]");
		return builder.toString();
	}
	public Set getIrSCustomerOtherlicensings() {
		return irSCustomerOtherlicensings;
	}
	public void setIrSCustomerOtherlicensings(Set irSCustomerOtherlicensings) {
		this.irSCustomerOtherlicensings = irSCustomerOtherlicensings;
	}
	public Set getIrSCustomerlicensings() {
		return irSCustomerlicensings;
	}
	public void setIrSCustomerlicensings(Set irSCustomerlicensings) {
		this.irSCustomerlicensings = irSCustomerlicensings;
	}
	
	
}
