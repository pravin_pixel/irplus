package com.irplus.dao.hibernate.entities;

public class IrFPPaymentEntryRecord {
	private int paymentId;
	private char recordTypeCode;
	private String transactionCode;
	private String receivingDFIIdentification;
	private String checkDigit;
	private String dfiAccountNumber;
	private String amount;
	private String individualIdentificationNumber;
	private IrFPBatchHeaderRecord irFPBatchHeaderRecord;
private IrFPFileHeaderRecord irFPFileHeaderRecord;
	

	public IrFPFileHeaderRecord getIrFPFileHeaderRecord() {
		return irFPFileHeaderRecord;
	}

	public void setIrFPFileHeaderRecord(IrFPFileHeaderRecord irFPFileHeaderRecord) {
		this.irFPFileHeaderRecord = irFPFileHeaderRecord;
	}

	public IrFPPaymentEntryRecord() {
		
	}

	public int getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(int paymentId) {
		this.paymentId = paymentId;
	}

	public char getRecordTypeCode() {
		return recordTypeCode;
	}

	public void setRecordTypeCode(char recordTypeCode) {
		this.recordTypeCode = recordTypeCode;
	}

	public String getTransactionCode() {
		return transactionCode;
	}

	public void setTransactionCode(String transactionCode) {
		this.transactionCode = transactionCode;
	}

	public String getReceivingDFIIdentification() {
		return receivingDFIIdentification;
	}

	public void setReceivingDFIIdentification(String receivingDFIIdentification) {
		this.receivingDFIIdentification = receivingDFIIdentification;
	}

	public String getCheckDigit() {
		return checkDigit;
	}

	public void setCheckDigit(String checkDigit) {
		this.checkDigit = checkDigit;
	}

	public String getDfiAccountNumber() {
		return dfiAccountNumber;
	}

	public void setDfiAccountNumber(String dfiAccountNumber) {
		this.dfiAccountNumber = dfiAccountNumber;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getIndividualIdentificationNumber() {
		return individualIdentificationNumber;
	}

	public void setIndividualIdentificationNumber(String individualIdentificationNumber) {
		this.individualIdentificationNumber = individualIdentificationNumber;
	}

	public IrFPBatchHeaderRecord getIrFPBatchHeaderRecord() {
		return irFPBatchHeaderRecord;
	}

	public void setIrFPBatchHeaderRecord(IrFPBatchHeaderRecord irFPBatchHeaderRecord) {
		this.irFPBatchHeaderRecord = irFPBatchHeaderRecord;
	}

	
	
	
	
	
}
