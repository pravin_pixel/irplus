package com.irplus.dao.hibernate.entities;



public class IrFPFileControlRecord {
	
	private int fileControlRecordId;
	private char recordTypeCode;
	private int batchCount;
	private String blockCount;
	private int entryAddendaCount;
	private String entryHash;
	private Double totalDebitEntryDollarAmountInFile;
	private Double totalcreditEntryDollarAmountInFile;
	private String reserved;

	private IrFPFileHeaderRecord irFPFileHeaderRecord;

	public int getFileControlRecordId() {
		return fileControlRecordId;
	}

	public void setFileControlRecordId(int fileControlRecordId) {
		this.fileControlRecordId = fileControlRecordId;
	}

	public char getRecordTypeCode() {
		return recordTypeCode;
	}

	public void setRecordTypeCode(char recordTypeCode) {
		this.recordTypeCode = recordTypeCode;
	}

	public int getBatchCount() {
		return batchCount;
	}

	public void setBatchCount(int batchCount) {
		this.batchCount = batchCount;
	}

	public String getBlockCount() {
		return blockCount;
	}

	public void setBlockCount(String blockCount) {
		this.blockCount = blockCount;
	}

	public int getEntryAddendaCount() {
		return entryAddendaCount;
	}

	public void setEntryAddendaCount(int entryAddendaCount) {
		this.entryAddendaCount = entryAddendaCount;
	}

	public String getEntryHash() {
		return entryHash;
	}

	public void setEntryHash(String entryHash) {
		this.entryHash = entryHash;
	}

	public Double getTotalDebitEntryDollarAmountInFile() {
		return totalDebitEntryDollarAmountInFile;
	}

	public void setTotalDebitEntryDollarAmountInFile(Double totalDebitEntryDollarAmountInFile) {
		this.totalDebitEntryDollarAmountInFile = totalDebitEntryDollarAmountInFile;
	}

	public Double getTotalcreditEntryDollarAmountInFile() {
		return totalcreditEntryDollarAmountInFile;
	}

	public void setTotalcreditEntryDollarAmountInFile(Double totalcreditEntryDollarAmountInFile) {
		this.totalcreditEntryDollarAmountInFile = totalcreditEntryDollarAmountInFile;
	}

	public String getReserved() {
		return reserved;
	}

	public void setReserved(String reserved) {
		this.reserved = reserved;
	}

	public IrFPFileHeaderRecord getIrFPFileHeaderRecord() {
		return irFPFileHeaderRecord;
	}

	public void setIrFPFileHeaderRecord(IrFPFileHeaderRecord irFPFileHeaderRecord) {
		this.irFPFileHeaderRecord = irFPFileHeaderRecord;
	}
	

	

}
