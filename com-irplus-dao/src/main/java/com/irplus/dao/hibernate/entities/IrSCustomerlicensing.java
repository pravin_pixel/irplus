package com.irplus.dao.hibernate.entities;

import java.util.Date;

public class IrSCustomerlicensing implements java.io.Serializable{

	private Integer customerLicenseId;
	
	private IrMBusinessProcess irMBusinessprocess;
	private IrMUsers irMUsers;
	private IrSBankBranch irSBankbranch;
	private IrSCustomers irSCustomers;
	private IrMFileTypes fileType;
	private Long filetypeid;
	
	private Integer isactive;
	private Date createddate;
	private Date modifieddate;

	public IrSCustomerlicensing() {
	}

	public IrSCustomerlicensing(IrMBusinessProcess irMBusinessprocess, IrMUsers irMUsers, IrSBankBranch irSBankbranch,
			IrSCustomers irSCustomers, Long filetypeid, Integer isactive, Date createddate, Date modifieddate) {
		this.irMBusinessprocess = irMBusinessprocess;
		this.irMUsers = irMUsers;
		this.irSBankbranch = irSBankbranch;
		this.irSCustomers = irSCustomers;
		this.filetypeid = filetypeid;
		this.isactive = isactive;
		this.createddate = createddate;
		this.modifieddate = modifieddate;
	}

	public Integer getCustomerLicenseId() {
		return this.customerLicenseId;
	}

	public void setCustomerLicenseId(Integer customerLicenseId) {
		this.customerLicenseId = customerLicenseId;
	}

	public IrMBusinessProcess getIrMBusinessprocess() {
		return this.irMBusinessprocess;
	}

	public void setIrMBusinessprocess(IrMBusinessProcess irMBusinessprocess) {
		this.irMBusinessprocess = irMBusinessprocess;
	}

	public IrMUsers getIrMUsers() {
		return this.irMUsers;
	}

	public void setIrMUsers(IrMUsers irMUsers) {
		this.irMUsers = irMUsers;
	}

	public IrSBankBranch getIrSBankbranch() {
		return this.irSBankbranch;
	}

	public void setIrSBankbranch(IrSBankBranch irSBankbranch) {
		this.irSBankbranch = irSBankbranch;
	}

	public IrSCustomers getIrSCustomers() {
		return this.irSCustomers;
	}
	public IrMFileTypes getFileType() {
		return fileType;
	}

	public void setFileType(IrMFileTypes fileType) {
		this.fileType = fileType;
	}

	public void setIrSCustomers(IrSCustomers irSCustomers) {
		this.irSCustomers = irSCustomers;
	}
	
	public Long getFiletypeid() {
		return filetypeid;
	}

	public void setFiletypeid(Long filetypeid) {
		this.filetypeid = filetypeid;
	}

	public Integer getIsactive() {
		return this.isactive;
	}

	public void setIsactive(Integer isactive) {
		this.isactive = isactive;
	}

	public Date getCreateddate() {
		return this.createddate;
	}

	public void setCreateddate(Date createddate) {
		this.createddate = createddate;
	}

	public Date getModifieddate() {
		return this.modifieddate;
	}

	public void setModifieddate(Date modifieddate) {
		this.modifieddate = modifieddate;
	}

}
