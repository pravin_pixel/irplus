package com.irplus.dao.hibernate.entities;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import com.irplus.dto.ContactInfo;
import com.irplus.dto.IRPlusFileTypes;

public class IrMFileTypes {

	private Long filetypeid;
	//private BankBranch bankBranch;
	private String filetypename;
	private Set irMReportfileFormatses = new HashSet(0);
	
	private Long licenseid;
	private int isactive;
	private IrMUsers user;
	private IRSLicense iRSLicense;
	 
	public IRSLicense getiRSLicense() {
		return iRSLicense;
	}
	public void setiRSLicense(IRSLicense iRSLicense) {
		this.iRSLicense = iRSLicense;
	}
	private Timestamp createddate;
    private Timestamp modifieddate;
    public IrMFileTypes() {
		
	}
	public IrMFileTypes(IRPlusFileTypes irPlusFileTypes) {
		this.filetypename = irPlusFileTypes.getFiletypename();
		
	}
	public Long getFiletypeid() {
		return filetypeid;
	}
	public void setFiletypeid(Long filetypeid) {
		this.filetypeid = filetypeid;
	}
	public String getFiletypename() {
		return filetypename;
	}
	public void setFiletypename(String filetypename) {
		this.filetypename = filetypename;
	}
	
	
	public int getIsactive() {
		return isactive;
	}
	public void setIsactive(int isactive) {
		this.isactive = isactive;
	}
	public IrMUsers getUser() {
		return user;
	}
	public void setUser(IrMUsers user) {
		this.user = user;
	}
	public Timestamp getCreateddate() {
		return createddate;
	}
	public void setCreateddate(Timestamp createddate) {
		this.createddate = createddate;
	}
	public Timestamp getModifieddate() {
		return modifieddate;
	}
	public void setModifieddate(Timestamp modifieddate) {
		this.modifieddate = modifieddate;
	}
	public Long getLicenseid() {
		return licenseid;
	}
	public void setLicenseid(Long licenseid) {
		this.licenseid = licenseid;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("IrMFileTypes [filetypeid=").append(filetypeid).append(", filetypename=").append(filetypename)
				.append(", licenseid=").append(licenseid).append(", iRSLicense=").append(iRSLicense).append(", isactive=").append(isactive).append(", user=")
				.append(user).append(", createddate=").append(createddate).append(", modifieddate=")
				.append(modifieddate).append("]");
		return builder.toString();
	}
	public Set getIrMReportfileFormatses() {
		return irMReportfileFormatses;
	}
	public void setIrMReportfileFormatses(Set irMReportfileFormatses) {
		this.irMReportfileFormatses = irMReportfileFormatses;
	}
	
	
	
}
