package com.irplus.dao.customer_grouping;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.irplus.dao.hibernate.entities.IRSBank;
import com.irplus.dao.hibernate.entities.IRSBankLicensing;
import com.irplus.dao.hibernate.entities.IrFPBatchHeaderRecord;
import com.irplus.dao.hibernate.entities.IrFPFileHeaderRecord;
import com.irplus.dao.hibernate.entities.IrMFileTypes;
import com.irplus.dao.hibernate.entities.IrMRoles;
import com.irplus.dao.hibernate.entities.IrMUsers;
import com.irplus.dao.hibernate.entities.IrMUsersBank;
import com.irplus.dao.hibernate.entities.IrSBankBranch;
import com.irplus.dao.hibernate.entities.IrSBankContact;
import com.irplus.dao.hibernate.entities.IrSCustomercontacts;
import com.irplus.dao.hibernate.entities.IrSCustomers;
import com.irplus.dao.hibernate.entities.IrsCustomerGrouping;
import com.irplus.dao.hibernate.entities.IrsCustomerGroupingDetails;
import com.irplus.dto.BankBranchInfo;
import com.irplus.dto.BankCustmrMap;
import com.irplus.dto.BankFilterInfo;
import com.irplus.dto.BankInfo;
import com.irplus.dto.BankViewInfo;
import com.irplus.dto.CustomerBean;
import com.irplus.dto.CustomerContactsBean;
import com.irplus.dto.CustomerGroupingDetails;
import com.irplus.dto.CustomerGroupingMngmntInfo;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.StatusIdInfo;
import com.irplus.dto.fileprocessing.FileHeaderRecordBean;
import com.irplus.dto.users.UserBean;
import com.irplus.util.BusinessException;
import com.irplus.util.IRPlusConstants;
import com.irplus.util.IRPlusUtil;

@Component
@Transactional

public class CustomerGroupingMngmntDaoImpl  implements ICustomerGroupingMngmntDao {

	private static final Logger log = Logger.getLogger(CustomerGroupingMngmntDaoImpl.class);
	
	@Autowired
	SessionFactory sessionFactory;
		
	/*public IRPlusResponseDetails createCustomerGrp(CustomerGroupingMngmntInfo customerGroupingMngmntInfo)throws BusinessException {
		
		List<String> errMsgs = new ArrayList<String>();		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();		
		Session session = sessionFactory.getCurrentSession();
		
		boolean flag = true;
	
		try {
		
			log.debug("customerGroupingMngmntInfo "+customerGroupingMngmntInfo.getAdminid());
			log.debug("customerGroupingMngmntInfo "+customerGroupingMngmntInfo.getCustomerGroupName());
			
		if(customerGroupingMngmntInfo.getAdminid()==null){
				flag =false;
				errMsgs.add("Validation Error :: AdmindId null - Please get login session userid ");
			}
		if(customerGroupingMngmntInfo.getCustomerGroupName()==null){
			flag =false;
			errMsgs.add("Validation Error :: CustomerGroupName is null Please enter ");
		}		
						
			if(customerGroupingMngmntInfo.getMapBankCustmrIds().isEmpty()){
				flag =false;
				errMsgs.add("Validation Error :: Bank CustomerIds are null - Please enter ");				
			}		
			
			if(flag){
			
				boolean grpNameflag = true; 
							
				List<BankCustmrMap> lst = customerGroupingMngmntInfo.getBankCustmrMapArray();
								
				if(!lst.isEmpty()){
					
				String grnamme = lst.get(0).getCustomerGroupName();	
				Criteria criteria = session.createCriteria(IrsCustomerGrouping.class);
				
				List<IrsCustomerGrouping> irslt = criteria.list();
				
				if(!irslt.isEmpty()){
				
					for(IrsCustomerGrouping irsCustomerGrouping : irslt) {
						
						if (irsCustomerGrouping.getCustomerGroupName().equalsIgnoreCase(grnamme)) {
							grpNameflag = false;
						}						
					}
				}
				
				if(grpNameflag ){
					
					Integer[] cusmrIds = new Integer[lst.size()];
					
					int i =0 ;
					
					for(BankCustmrMap bankCustmrMap1 : lst){						
						cusmrIds[i] = bankCustmrMap1.getCustomerId();
						++i;
					}
					
					int j= 0;
					for (BankCustmrMap bankCustmrMap1 : lst){
							
						for (BankCustmrMap bankCustmrMap2 : lst) {
							
						if (bankCustmrMap1.getCustomerId()== bankCustmrMap2.getCustomerId() && 
								bankCustmrMap1.getBankId()== bankCustmrMap2.getBankId()	
								){
							
							++j;							
						}
						
					}
					}
					
					boolean	flag_dup = false;
				
					if(j>=2){
						
						flag_dup =true ;
					}
					IrsCustomerGrouping  irscgrp = new IrsCustomerGrouping();
		//			if(flag_dup==false){
						
						BankCustmrMap  bnkctmap = lst.get(0);
						
						log.debug(" BankCustmrMap : "+lst.get(0));
						log.debug("bnkctmap.getCustomerGroupName() "+bnkctmap.getCustomerGroupName());
						log.debug(" BankCustmrMap getUserId: "+bnkctmap.getUserId());
						log.debug(" BankCustmrMap getBankId: "+bnkctmap.getBankId());
						
						irscgrp.setCustomerGroupName(bnkctmap.getCustomerGroupName());
						irscgrp.setIsactive(1);
						
						irscgrp.setUserid(bnkctmap.getUserId());
						
						Integer id = (Integer)session.save(irscgrp);						
						session.flush();
					
						log.debug("CustomerGroupName id : "+ id);
						
						Integer succes = null;
						if(id!=null){
						for(BankCustmrMap bncustp : lst){
							
							IrsCustomerGroupingDetails irscustmrGrpDtls = new IrsCustomerGroupingDetails();
										
							irscustmrGrpDtls.setIrMUsers((IrMUsers)session.get(IrMUsers.class , bncustp.getUserId()));
						    irscustmrGrpDtls.setIrsCustomerGrouping((IrsCustomerGrouping)session.get(IrsCustomerGrouping.class , id));
						   
						    if (bncustp.getBankId()!=null) {
								
						    	irscustmrGrpDtls.setIrSBankbranch((IrSBankBranch)session.get(IrSBankBranch.class, bncustp.getBankId()));
							} else {
								errMsgs.add("Validation Error : bank Id is null ");
								irPlusResponseDetails.setErrMsgs(errMsgs);
								irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
								irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
							}
						    
						    irscustmrGrpDtls.setIrSCustomers((IrSCustomers)session.get(IrSCustomers.class,bncustp.getCustomerId()));
						    irscustmrGrpDtls.setIsactive(1);						    
						    succes = (Integer) session.save(irscustmrGrpDtls);	
						    
						}
						
						if(succes!=null){
						errMsgs.add("Success : CustmrGrpDetails saved Data ");
						
//						errMsgs.add("Validation Error : This Customer Group Name Already Available ");
						irPlusResponseDetails.setErrMsgs(errMsgs);
						irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
						irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);			
						}
						}else{
							
							errMsgs.add("Validation Error : CustmrGrp Id is null ");
//							errMsgs.add("Validation Error : This Customer Group Name Already Available ");
							irPlusResponseDetails.setErrMsgs(errMsgs);
							irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
							irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);							
						}
						
						//adding below comment
						
					}else {
						
						errMsgs.add("Validation Error : Duplicate field entered");						
						irPlusResponseDetails.setErrMsgs(errMsgs);
						irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
						irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
					}
					
				}else {
					
					errMsgs.add("Validation Error : Duplicate CustomerGroupName ");
					errMsgs.add("Validation Error : This Customer Group Name Already Available ");
					irPlusResponseDetails.setErrMsgs(errMsgs);
					irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
					irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				}

				}else{
				errMsgs.add("Validation Error : Empty - Please Enter Fields ");				
				irPlusResponseDetails.setErrMsgs(errMsgs);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			}
				}else {
					
					errMsgs.add("Validation Error : Duplicate CustomerGroupName ");
					errMsgs.add("Validation Error : This Customer Group Name Already Available ");
					irPlusResponseDetails.setErrMsgs(errMsgs);
					irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
					irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				}
			
			
		} catch (Exception e) {			

			log.error("Exception in create createCustomerGrp",e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			throw new HibernateException(e);
		}
	
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails createCustomerGrp(CustomerGroupingMngmntInfo customerGroupingMngmntInfo)
			throws BusinessException {
		
		
		List<String> errMsgs = new ArrayList<String>();		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();		
		Session session = sessionFactory.getCurrentSession();		
		boolean flag = true;
		try {
			
			if(customerGroupingMngmntInfo.getAdminid()==null){
				flag =false;
				errMsgs.add("Validation Error :: AdmindId null - Please get login session userid ");
			}
			if(customerGroupingMngmntInfo.getCustomerGroupName()==null){
				flag =false;
				errMsgs.add("Validation Error :: CustomerGroupName is null Please enter ");
			}			
			
			if(customerGroupingMngmntInfo.getMapBankCustmrIds().isEmpty()){
				flag =false;
				errMsgs.add("Validation Error :: Bank CustomerIds are null - Please enter ");
				
			}
			
			
			if(flag){
				
				IrsCustomerGrouping  irscgrp = new IrsCustomerGrouping();
				
				Criteria criteria = session.createCriteria(IrsCustomerGrouping.class);
				criteria.add(Restrictions.like("customerGroupName", customerGroupingMngmntInfo.getCustomerGroupName()));
			
				if(criteria.list().isEmpty()){
					
					irscgrp.setCustomerGroupName(customerGroupingMngmntInfo.getCustomerGroupName());
					irscgrp.setIsactive(1);
					irscgrp.setUserid(customerGroupingMngmntInfo.getAdminid());
				
					IrsCustomerGroupingDetails irsCustmrGrpngDetails = null;
					boolean mapFlag = true;
					boolean mapSaveFlag = false;
					Integer id = null;
					
								Map<Integer, Integer> map = customerGroupingMngmntInfo.getMapBankCustmrIds();
								
								for (Map.Entry<Integer, Integer> entry : map.entrySet())
								{
									if(entry.getKey()== null && entry.getValue()==null ){
										mapFlag = false;
									}
								}	
								if(mapFlag){
									id =(Integer)  session.save(irscgrp);
								}
								
					if (id!=null){
									
									for (Map.Entry<Integer, Integer> entry : map.entrySet())
									{
										
									irsCustmrGrpngDetails = new IrsCustomerGroupingDetails();
									 
								    log.debug(entry.getKey() + "/" + entry.getValue());
								    
								    if(entry.getKey()!= null && entry.getValue()!=null ){
								    	
								    irsCustmrGrpngDetails.setIrMUsers((IrMUsers)session.get(IrMUsers.class , customerGroupingMngmntInfo.getUserid()));
								    irsCustmrGrpngDetails.setCustomrGrpDtId(id);
								    irsCustmrGrpngDetails.setIrSBankbranch((IrSBankBranch)session.get(IrSBankBranch.class, entry.getKey()));
								    irsCustmrGrpngDetails.setIrSCustomers((IrSCustomers)session.get(IrSCustomers.class, entry.getValue()));
								    irsCustmrGrpngDetails.setIsactive(1);
								    session.save(irsCustmrGrpngDetails);
								    
								    mapSaveFlag = true;
																   
								    }else{				
								    	
										errMsgs.add("Validation Error :  CustomerID and BankId is null Please check");
										irPlusResponseDetails.setErrMsgs(errMsgs);		
										irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
										irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
								    }
								}
								
								if(mapSaveFlag){
								errMsgs.add(" Success : Created Customer Group ");
								irPlusResponseDetails.setErrMsgs(errMsgs);
								irPlusResponseDetails.setValidationSuccess(true);
								irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
								irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
								}
						
					}else{
						
						errMsgs.add("Error of Save Object ");
						irPlusResponseDetails.setErrMsgs(errMsgs);
						irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
						irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
					}
					
					
				}else {
					
					errMsgs.add("Validation Error : Duplicate CustomerGroupName ");
					errMsgs.add("Validation Error : This Customer Group Name Already Available ");
					irPlusResponseDetails.setErrMsgs(errMsgs);
					irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
					irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				}
							
			}
			
			if(flag==false){
			errMsgs.add("Validation Error : Please Enter valid fields ");
			irPlusResponseDetails.setErrMsgs(errMsgs);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
			}
			
		} catch (Exception e) {
			
			log.error("Exception in create createCustomerGrp",e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			throw new HibernateException(e);
			
		}
		irPlusResponseDetails.setErrMsgs(errMsgs);
		return irPlusResponseDetails;
	}
	
	
//	json input for create 
	
//	{
	
	
//	}
	
	 
	
	
	@Override
	public IRPlusResponseDetails showOneCustomerGrpById(String customerGroupingMngmntId) throws BusinessException {

		Session session = sessionFactory.getCurrentSession();	
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		List<String> errMsgs = new ArrayList<String>();
		
		IrsCustomerGroupingDetails irsCustomerGroupingDetails = null;
		
		Integer id = null;
		 ArrayList<BankCustmrMap> bankCustmrMapArray = new ArrayList<BankCustmrMap>();
		try {
			id = Integer.parseInt(customerGroupingMngmntId);
						
			Criteria criteria = session.createCriteria(IrsCustomerGroupingDetails.class , "cstgrpdt");
			
			List<IrsCustomerGroupingDetails>  lst =  criteria.add(Restrictions.eq("cstgrpdt.irsCustomerGrouping.customerGrpId", id)).list();
			
			Map<String , String> mapBankNameMapedCustmrId = new HashMap<String , String>();  
			
			CustomerGroupingMngmntInfo customerGroupingMngmntInfo = new CustomerGroupingMngmntInfo();
			
			if(lst.isEmpty()){
				
				errMsgs.add(" Validation Error : Please Enter id , status value");
				irPlusResponseDetails.setErrMsgs(errMsgs);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
				
			}else{
				
				IrsCustomerGrouping ircg =(IrsCustomerGrouping) session.get(IrsCustomerGrouping.class, id);
				
				if(ircg!=null){
					
				customerGroupingMngmntInfo.setCustomerGroupName(ircg.getCustomerGroupName());
				customerGroupingMngmntInfo.setCustomerGrpId(id);
			
				for (IrsCustomerGroupingDetails irsCuail : lst){
					
					
					
					if(irsCuail.getIsactive()!=2){
						
						BankCustmrMap cutmrGrp = new BankCustmrMap();
				
				//	mapBankNameMapedCustmrId.put(irsCuail.getIrSBankbranch().getBank().getBankName(), irsCuail.getIrSCustomers().getIrCustomerCode());
					
					cutmrGrp.setCustomrGrpDtlsId(irsCuail.getCustomrGrpDtId());
					cutmrGrp.setBankId(irsCuail.getIrSBankbranch().getBranchId());
					cutmrGrp.setCustomerGroupName(irsCuail.getIrsCustomerGrouping().getCustomerGroupName());
					cutmrGrp.setCustomrGrpId(irsCuail.getIrsCustomerGrouping().getCustomerGrpId());
					cutmrGrp.setCustomerId(irsCuail.getIrSCustomers().getCustomerId());
					cutmrGrp.setBankName(irsCuail.getIrSBankbranch().getBank().getBankName());
					
					bankCustmrMapArray .add(cutmrGrp);
					
					}
					
				}
			//	customerGroupingMngmntInfo.setMapBankNameMapedCustmrId(mapBankNameMapedCustmrId);
				
				customerGroupingMngmntInfo.setBankCustmrMapArray(bankCustmrMapArray);
				}
				
				irPlusResponseDetails.setOneCustmrGrp(customerGroupingMngmntInfo);				
				
				errMsgs.add(" Success : Got one irsCustomerGroupName");
				
				irPlusResponseDetails.setValidationSuccess(true);				
				irPlusResponseDetails.setErrMsgs(errMsgs);
				irPlusResponseDetails.setRecordsFiltered(mapBankNameMapedCustmrId.size());
				irPlusResponseDetails.setRecordsTotal(mapBankNameMapedCustmrId.size());
				irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
				
			}
		
		}catch (Exception e) {
			
			log.error("Exception raised at StatusUpdateCustomerGrp" , e);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			throw new HibernateException(e);
		}
		
		return irPlusResponseDetails;	
	}
	

	@Override
	public IRPlusResponseDetails getCustomrGrpDtalsViewById(String customerGroupingMngmntId) throws BusinessException {

		Session session = sessionFactory.getCurrentSession();	
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		List<String> errMsgs = new ArrayList<String>();		
		IrsCustomerGroupingDetails irsCustomerGroupingDetails = null;
		Integer id = null;
	
		try {
			
			id = Integer.parseInt(customerGroupingMngmntId);	
			
			Criteria criteria = session.createCriteria(IrsCustomerGroupingDetails.class , "cstgrpdt");
			
			List<IrsCustomerGroupingDetails>  lst=  criteria.add(Restrictions.like("cstgrpdt.irsCustomerGrouping.customerGrpId", id)).list();
			
			CustomerGroupingMngmntInfo customerGroupingMngmntInfo = null;
			
			List<CustomerGroupingMngmntInfo> listCustmrgrp = new ArrayList<CustomerGroupingMngmntInfo>();
			if(lst.isEmpty()){
				
				errMsgs.add(" Validation Error : Please Enter id , status value");
				irPlusResponseDetails.setErrMsgs(errMsgs);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
				
			}else{
				
				for (IrsCustomerGroupingDetails customerGrpgDetls : lst) {
					
					customerGroupingMngmntInfo = new CustomerGroupingMngmntInfo();
					
					customerGroupingMngmntInfo.setCustomerGrpId(id);
					
					customerGroupingMngmntInfo.setUsercode(Integer.parseInt(customerGrpgDetls.getIrMUsers().getUserCode()));
					customerGroupingMngmntInfo.setCustomerGroupName(customerGrpgDetls.getIrsCustomerGrouping().getCustomerGroupName());
					customerGroupingMngmntInfo.setCompanyName(customerGrpgDetls.getIrSCustomers().getCompanyName());
					customerGroupingMngmntInfo.setParentBankName(customerGrpgDetls.getIrSBankbranch().getBank().getBankName());
					customerGroupingMngmntInfo.setBankCustomerCode(customerGrpgDetls.getIrSCustomers().getBankCustomerCode());
					customerGroupingMngmntInfo.setBankBranchLocation(customerGrpgDetls.getIrSBankbranch().getBranchLocation());
					customerGroupingMngmntInfo.setCustomerid(customerGrpgDetls.getIrSCustomers().getCustomerId());
					customerGroupingMngmntInfo.setCustomerCompanyIdentNo(customerGrpgDetls.getIrSCustomers().getCompIdentNo());
					
					listCustmrgrp.add(customerGroupingMngmntInfo);
				}
				
				irPlusResponseDetails.setCustmrGrpngMngmntInfoList(listCustmrgrp);
				
				errMsgs.add(" Success : Updated Status irsCustomerGroupingDetails");
				irPlusResponseDetails.setValidationSuccess(true);				
				irPlusResponseDetails.setErrMsgs(errMsgs);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
				
			}
		
		}catch (Exception e) {
			
			log.error("Exception raised at StatusUpdateCustomerGrp " , e);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			throw new HibernateException(e);
		}
		
		return irPlusResponseDetails;	
	}
	
	// views add and Onerow Update code is pending 1 12 17
	
	
	@Override
	public IRPlusResponseDetails updateOneCustomerGrps(CustomerGroupingMngmntInfo customerGroupingMngmntInfo)
			throws BusinessException {
		
		log.debug("Inside of CustomerGroupingMngmntDaoImpl : updateOneCustomerGrps");
		
			List<String> errMsgs = new ArrayList<String>();		
			IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();		
			Session session = sessionFactory.getCurrentSession();		
			boolean flag = true;
		
			try {
			
				if(customerGroupingMngmntInfo.getAdminid() == null){
					flag =false;
					errMsgs.add("Validation Error :: AdmindId null - Please get login session userid ");
			
				}
				if(customerGroupingMngmntInfo.getCustomerGroupName() == null){
					flag =false;
					errMsgs.add("Validation Error :: CustomerGroupName is null Please enter ");
			
				}
				if(customerGroupingMngmntInfo.getCustomerGrpId() == null){
					flag =false;
					errMsgs.add("Validation Error :: CustomerGroupid is null Please enter ");
					
				}
				if(customerGroupingMngmntInfo.getMapBankCustmrIds().isEmpty()){
					flag =false;
					errMsgs.add("Validation Error :: Bank CustomerIds are null - Please enter ");				
				}		
				
				
				
				if(flag){
					
					boolean dup_flag = false;
					
					
					Criteria criteria = session.createCriteria(IrsCustomerGrouping.class);
					List<IrsCustomerGrouping> lst = criteria.list();
					
					for (IrsCustomerGrouping irsCustomerGrouping : lst) {
						
						if(customerGroupingMngmntInfo.getCustomerGrpId()!=irsCustomerGrouping.getCustomerGrpId()){
							
							if(customerGroupingMngmntInfo.getCustomerGroupName().equalsIgnoreCase(irsCustomerGrouping.getCustomerGroupName())){
								
								dup_flag =true;
							
							}
						}
					}
					
					
					if (dup_flag) {
						
						errMsgs.add("Duplicate Error : Group Name Already available ");
						errMsgs.add("Choose Different Group Name ");
						irPlusResponseDetails.setErrMsgs(errMsgs);
						irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
						irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
						
					} else {
						
						IrsCustomerGrouping custmrGrp =(IrsCustomerGrouping) session.get(IrsCustomerGrouping.class, customerGroupingMngmntInfo.getCustomerGrpId());
						
						custmrGrp.setCustomerGroupName(customerGroupingMngmntInfo.getCustomerGroupName());
						custmrGrp.setUserid(customerGroupingMngmntInfo.getUserid());
												
						ArrayList<BankCustmrMap> custmerDetailsId = customerGroupingMngmntInfo.getBankCustmrMapArray();
						
						List<IrsCustomerGroupingDetails>  dbDataList = session.createCriteria(IrsCustomerGroupingDetails.class,"custmrGrpdtls")
						.createAlias("custmrGrpdtls.irsCustomerGrouping", "grpid")
						.add(Restrictions.like("grpid.customerGrpId", customerGroupingMngmntInfo.getCustomerGrpId())).list();
						
						boolean update_flag = false;
						
						for (BankCustmrMap bankCustmrMap : custmerDetailsId) { // 1, 9, 8 ,3
							
							update_flag = true;
							
							for (IrsCustomerGroupingDetails db_bankCustmrmap : dbDataList) { // 1, 8
								
								if(db_bankCustmrmap.getCustomrGrpDtId()== bankCustmrMap.getCustomrGrpDtlsId()){
									
									db_bankCustmrmap.setIrMUsers((IrMUsers)session.get(IrMUsers.class,customerGroupingMngmntInfo.getUserid()));
									db_bankCustmrmap.setIrSBankbranch((IrSBankBranch)session.get(IrSBankBranch.class,bankCustmrMap.getBankId()));
									db_bankCustmrmap.setIrSCustomers((IrSCustomers)session.get(IrSCustomers.class,bankCustmrMap.getCustomerId()));
									db_bankCustmrmap.setIsactive(1);
									db_bankCustmrmap.setIrsCustomerGrouping((IrsCustomerGrouping)session.get(IrsCustomerGrouping.class,bankCustmrMap.getCustomrGrpId()));
									session.update(db_bankCustmrmap);
									
									log.debug(" Updated ");
									update_flag = false;
								}
							}
							
							if(update_flag){
								
								IrsCustomerGroupingDetails cust = new IrsCustomerGroupingDetails();
								
								cust.setIrMUsers((IrMUsers)session.get(IrMUsers.class,customerGroupingMngmntInfo.getUserid()));
								cust.setIrSBankbranch((IrSBankBranch)session.get(IrSBankBranch.class,	bankCustmrMap.getBankId()));
								cust.setIrSCustomers((IrSCustomers)session.get(IrSCustomers.class, bankCustmrMap.getCustomerId()));
								cust.setIsactive(1);
								cust.setIrsCustomerGrouping((IrsCustomerGrouping)session.get(IrsCustomerGrouping.class,bankCustmrMap.getCustomrGrpId()));
								
								session.save(cust);
							}
						
						}
						
						errMsgs.add(" Success :  Updated");			
						irPlusResponseDetails.setValidationSuccess(true);
						irPlusResponseDetails.setErrMsgs(errMsgs);
						irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
						irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
					}
					
					
				}else {
						
						errMsgs.add("Validation Error : Please Enter Fields");
						
						irPlusResponseDetails.setErrMsgs(errMsgs);
						irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
						irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
					}
								
			} catch (Exception e) {			

				log.error("Exception in create createCustomerGrp",e);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				throw new HibernateException(e);
			}
		
			return irPlusResponseDetails;
		}

	 json-update
	 *  
		getCustomrGrpDtlsId = hidden field
	
	
	@Override
	public IRPlusResponseDetails statusUpdatCustmrGrpById(StatusIdInfo statusIdInfo) throws BusinessException{

		Session session = sessionFactory.getCurrentSession();	
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		List<String> errMsgs = new ArrayList<String>();
		IrsCustomerGrouping irsCustomerGroup = null;
		
		try {
			
			if(statusIdInfo.getId() == null && statusIdInfo.getStatus() == null){
				
				errMsgs.add(" Validation Error : Please Enter id , status value");
				irPlusResponseDetails.setErrMsgs(errMsgs);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
				
			}else{
				
				List<IrsCustomerGroupingDetails>  lst = session.createCriteria(IrsCustomerGroupingDetails.class,"custdtls")
				.createAlias("custdtls.irsCustomerGrouping","grp" )
				.add(Restrictions.like("grp.customerGrpId",statusIdInfo.getId())).list();
				
				for (IrsCustomerGroupingDetails irsCustomerGroupingDetails2 : lst) {	
					
					irsCustomerGroupingDetails2.setIsactive(statusIdInfo.getStatus());
					session.update(irsCustomerGroupingDetails2);					
				}
				
				irsCustomerGroup = (IrsCustomerGrouping) session.get(IrsCustomerGrouping.class ,statusIdInfo.getId());				
				irsCustomerGroup.setIsactive(statusIdInfo.getStatus());
				
				session.update(irsCustomerGroup);
				
				errMsgs.add(" Success : Updated Status ");				
				irPlusResponseDetails.setValidationSuccess(true);				
				irPlusResponseDetails.setErrMsgs(errMsgs);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);				
			}
		
		}catch (Exception e) {
			
			log.error("Exception raised at StatusUpdateCustomerGrp" , e);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			throw new HibernateException(e);
		}		
		return irPlusResponseDetails;
	}
		
		
	@Override
	public IRPlusResponseDetails showAllCustomerGrps() throws BusinessException {

		Session session = sessionFactory.getCurrentSession();
		
		List<String> errMsgs = new ArrayList<String>();
		IRPlusResponseDetails irPlusResponseDetails = new  IRPlusResponseDetails();
		Map<String , Integer> mapGrpNameMapedCmpns = null;
		CustomerGroupingMngmntInfo cstmangmnt = null;
		List<CustomerGroupingMngmntInfo> cutmrGrpList = new ArrayList<CustomerGroupingMngmntInfo>();
		
		try {
			
			Criteria criteria1 = session.createCriteria(IrsCustomerGroupingDetails.class,"IrsCustmrGrupingDetls");
			List<IrsCustomerGroupingDetails>  lsts = criteria1.list();
			
			Criteria criteria = session.createCriteria(IrsCustomerGrouping.class);
		//	BankCustmrMap
			List<IrsCustomerGrouping> lst= criteria.list();
			int i =0;
			boolean flag = false;
			if(!lst.isEmpty()){
			
			for (IrsCustomerGrouping irsCustomerGrouping : lst) {
				
				if(irsCustomerGrouping.getIsactive()!=2 ){  // 1 , 2 , 3
					mapGrpNameMapedCmpns = new HashMap<String , Integer>();
				cstmangmnt = new CustomerGroupingMngmntInfo();
				cstmangmnt.setCustomerGrpId(irsCustomerGrouping.getCustomerGrpId());
				cstmangmnt.setCustomerGroupName(irsCustomerGrouping.getCustomerGroupName());
				cstmangmnt.setCreateddate(irsCustomerGrouping.getCreateddate());
				i=0;
				flag=false;
				
				for (IrsCustomerGroupingDetails irsCustoouping : lsts){
					
					if(irsCustoouping.getIrsCustomerGrouping().getCustomerGrpId()==irsCustomerGrouping.getCustomerGrpId()){
						++i;
						flag = true;
					}
				}
				
				if(flag){
				mapGrpNameMapedCmpns.put(irsCustomerGrouping.getCustomerGroupName(), i);
				cstmangmnt.setMapGrpNameMapedCmpnsNos(mapGrpNameMapedCmpns);
				cutmrGrpList.add(cstmangmnt);
				}
				
				}
			}
			
			irPlusResponseDetails.setCustmrGrpngMngmntInfoList(cutmrGrpList);
		
			errMsgs.add("Success :: All records found");
			
			irPlusResponseDetails.setRecordsFiltered(cutmrGrpList.size());
			irPlusResponseDetails.setRecordsTotal(cutmrGrpList.size());
			irPlusResponseDetails.setValidationSuccess(true);
			irPlusResponseDetails.setErrMsgs(errMsgs);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			
			}else{
				errMsgs.add("Message Alert :: No records available");
				irPlusResponseDetails.setErrMsgs(errMsgs);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
			}
						
		} catch (Exception e) {
			
			log.error("Exception raised at StatusUpdateCustomerGrp" , e);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			throw new HibernateException(e);
			
		}	
			return irPlusResponseDetails;
	}
	

		json {
		
		mapGrpNameMapedCmpnsNos :{ "mygrp" : 2 }
		}			
	
	
	public IRPlusResponseDetails bankIdArrays() throws BusinessException {
		
		Session session = sessionFactory.getCurrentSession();
		
		log.debug("Inside of CustomerGroupingDaoImpl : BankBranchArrays");
		
		List<String> errMsgs = new ArrayList<String>();
		
		IRPlusResponseDetails irPlusResponseDetails = new  IRPlusResponseDetails();
					
		Criteria criteria1 = session.createCriteria(IrSBankBranch.class);
		List<IrSBankBranch>  lst1 = criteria1.list();
		
		List<BankBranchInfo> addBanksList = new ArrayList<BankBranchInfo>();
		
			try {
				
				if(!lst1.isEmpty()){
					
					for (IrSBankBranch irSBankBranch : lst1) {

						BankBranchInfo bankBranchInfo = new BankBranchInfo();
						
						bankBranchInfo.setBranchId(irSBankBranch.getBranchId());
						bankBranchInfo.setBranchLocation(irSBankBranch.getBranchLocation());
						bankBranchInfo.setParent_bankId(irSBankBranch.getBank().getBankId());
						bankBranchInfo.setParent_bankName(irSBankBranch.getBank().getBankName());
						
						addBanksList.add(bankBranchInfo);
					}
					
					if(addBanksList.isEmpty()){
						
						errMsgs.add("Message Alert : No Banks Branches available ");
						irPlusResponseDetails.setErrMsgs(errMsgs);
						irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
						irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
						
					}else{
						errMsgs.add("Success : This Branch not contains Customers");					
						irPlusResponseDetails.setBankBranchList(addBanksList);
						irPlusResponseDetails.setValidationSuccess(true);
						irPlusResponseDetails.setErrMsgs(errMsgs);
						irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
						irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
					}
					
				}else{
					
					errMsgs.add("Message Alert : Bank Branches Empty ");
					irPlusResponseDetails.setErrMsgs(errMsgs);
					irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
					irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
					
				}
				
			} catch (Exception e) {
	
				log.error("Exception raised at BankArray " , e);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
				throw new HibernateException(e);
			}
						
		return irPlusResponseDetails;
	}
	
	public IRPlusResponseDetails custmrIdArrays(String barnchId) throws BusinessException {
		
		log.debug("Inside of CustomerGrouping : custmrIdArrays");
		
		Session session = sessionFactory.getCurrentSession();
		
		List<String> errMsgs = new ArrayList<String>();
		
		List<CustomerGroupingMngmntInfo> custmrmngmntList = new ArrayList<CustomerGroupingMngmntInfo>();
		
		IRPlusResponseDetails irPlusResponseDetails = new  IRPlusResponseDetails();
		
		try {
			Long branch_Id = Long.parseLong(barnchId);		

			Query query = getMyCurrentSession().createQuery("select  cutmrperm.irMModulemenus.irMMenus from IrSCustomers cutmrperm where cutmrperm.irMRoles.roleid=:roleid");
			query.setParameter("roleid", userInfo.getRoleId());
			List <IrMMenus> menuList = query.list();
			
			
			Criteria criteria2 = session.createCriteria(IrSCustomers.class , "custmr");
			//		.createAlias("custmr.irSBankBranch" ,"bankBranch" )
			//		.add(Restrictions.like("bankBranch.branchId",branch_Id ));
			
	//		List<IrSCustomers> custmrs = criteria2.list();
		
			List<IrSCustomers>  lst2= criteria2.list();
			
			if(!lst2.isEmpty()){
				
			for (IrSCustomers irSCustomers : lst2) {
				
				if(irSCustomers.getIrSBankBranch().getBranchId()==branch_Id){					
				
					CustomerGroupingMngmntInfo custmr = new CustomerGroupingMngmntInfo();			
				
					custmr.setCustomerid(irSCustomers.getCustomerId());
					custmr.setCompanyName(irSCustomers.getCompanyName());
										
				custmrmngmntList.add(custmr);
				
				}				
			}
			if(custmrmngmntList.isEmpty()){
				
				errMsgs.add("Message Alert : This Branch not contains Customers");
				errMsgs.add("Message Alert : Please Select Another Branch");
				irPlusResponseDetails.setErrMsgs(errMsgs);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
			}else{
			
			irPlusResponseDetails.setCustmrGrpngMngmntInfoList(custmrmngmntList);
			irPlusResponseDetails.setValidationSuccess(true);
			irPlusResponseDetails.setErrMsgs(errMsgs);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			}	
			}else{
							
				errMsgs.add("Message Alert : No Customers Available ");
				irPlusResponseDetails.setErrMsgs(errMsgs);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
			}
			
		} catch (Exception e) {
			log.error("Exception raised at CustmerArray" , e);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			throw new HibernateException(e);
		}
		
		
		return irPlusResponseDetails;
	}
	
	
	*/
	@Override	
	public IRPlusResponseDetails createCustomerMappingGroup(CustomerGroupingMngmntInfo customergroupingMngmntInfo) throws BusinessException {
		
		log.info("Inside CustomerGroupingMngmntDaoImpl :: createCustomerMappingGroup");
		Session session = null;
		
		session = sessionFactory.getCurrentSession();
		List<String> errMsgs = new ArrayList<String>();
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		
		
		
		IrMUsers user = new IrMUsers();
		IrSBankBranch branch = new IrSBankBranch();
	//	IrSCustomercontacts irSCustomercontacts = new IrSCustomercontacts();
		try{
		
			Date date = new Date();
		    DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
		    DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
		    String currentDate = dateFormat.format(date);
		    String currentTime = timeFormat.format(date);
		   
			IrsCustomerGrouping irSCustomersGrouping = new IrsCustomerGrouping();
			
			irSCustomersGrouping.setCustomerGroupName(customergroupingMngmntInfo.getCustomerGroupName());
			irSCustomersGrouping.setIsactive(customergroupingMngmntInfo.getIsactive());
			irSCustomersGrouping.setUserid(customergroupingMngmntInfo.getUserid());
			irSCustomersGrouping.setCreateddate(currentDate);
			irSCustomersGrouping.setCreatedtime(currentTime);
			user.setUserid(customergroupingMngmntInfo.getUserid());
			irSCustomersGrouping.setIrMUsers(user);


				
				Integer customerGrpId=(Integer)session.save(irSCustomersGrouping);
				
				customergroupingMngmntInfo.getCustomergroupingDetails()[0].setCustomerGrpId(customerGrpId);
							session.flush();
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			
		
		}catch (Exception e) {
				log.error("Exception raised in create Customer group");
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				throw new HibernateException(e);
		}
			

	
	return irPlusResponseDetails;
}
	
	
	@Override
	public IRPlusResponseDetails addMappingDetails(CustomerGroupingDetails[] customergroupingdetails) throws BusinessException {
		

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		Session session = null;
		
		session = sessionFactory.getCurrentSession();
		IrMUsers user = new IrMUsers();
		IRSBank bank = new IRSBank();
		IrSCustomers customer = new IrSCustomers();
	
		IrsCustomerGrouping addgrpdetails=null;
		
		try
		{
			Date date = new Date();
		    DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
		    DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
		    String currentDate = dateFormat.format(date);
		    String currentTime = timeFormat.format(date);
		    
			if(customergroupingdetails.length>0)
			{
				addgrpdetails = (IrsCustomerGrouping) session.get(IrsCustomerGrouping.class,customergroupingdetails[0].getCustomerGrpId());
			

			}
			for(int i=0;i<customergroupingdetails.length;i++)
			{
	
				IrsCustomerGroupingDetails custGrpingDetais = new IrsCustomerGroupingDetails(customergroupingdetails[i]);
				
				custGrpingDetais.setIrsCustomerGrouping(addgrpdetails);
				
				bank.setBankId(customergroupingdetails[i].getBankId());			
				custGrpingDetais.setBank(bank);
				
				customer.setCustomerId(customergroupingdetails[i].getCustomerId());			
				custGrpingDetais.setIrSCustomers(customer);
				
				custGrpingDetais.setCreateddate(currentDate);
				custGrpingDetais.setCreatedtime(currentTime);
				user.setUserid(customergroupingdetails[i].getUserId());			
				custGrpingDetais.setIrMUsers(user);
				session.save(custGrpingDetais);
				session.flush();
				log.debug("Added custGrpingDetais Info :"+custGrpingDetais);
			}
		
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
		
		}catch(Exception e)
		{
			log.error("Exception in addContact",e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			throw new HibernateException(e);
		}

		
		return irPlusResponseDetails;
	}
	
	

	@Override
	public IRPlusResponseDetails showAllCustomerGroup() throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new  IRPlusResponseDetails();
	
		Session session = null;
		
		session = sessionFactory.getCurrentSession();
		List<String> errMsgs = new ArrayList<String>();
		
		CustomerGroupingMngmntInfo groupingDetails=null;
		 List<CustomerGroupingMngmntInfo> custmrGrpngMngmntInfoList=new ArrayList<CustomerGroupingMngmntInfo>();

		try {
			log.info("ytryrtyrt");
			Query custGroups = session.createQuery("from IrsCustomerGrouping group where group.isactive=?");
		
			custGroups.setParameter(0,1);
			
			
			List<IrsCustomerGrouping> custGroupsList =custGroups.list();
			
		if(custGroupsList.size()>0){
				for(int i=0;i<custGroupsList.size();i++){
				    groupingDetails = new CustomerGroupingMngmntInfo();
				    groupingDetails.setCreateddate(custGroupsList.get(i).getCreateddate());
				    groupingDetails.setCreatedtime(custGroupsList.get(i).getCreatedtime());
				    groupingDetails.setCustomerGroupName(custGroupsList.get(i).getCustomerGroupName());
			   groupingDetails.setCustomerGrpId(custGroupsList.get(i).getCustomerGrpId());

			

	Query query = session.createQuery("from IrsCustomerGroupingDetails grpdet where grpdet.irsCustomerGrouping.customerGrpId=?");
	
	
	query.setParameter(0,custGroupsList.get(i).getCustomerGrpId());
	List<IrsCustomerGroupingDetails> groupdetlist =query.list();		

	if(groupdetlist.size()>0){
		
			groupingDetails.setTotalMappedBranch(groupdetlist.size());
	
			custmrGrpngMngmntInfoList.add(groupingDetails);

	}
	else
	{
	
		groupingDetails.setTotalMappedBranch(0);
		custmrGrpngMngmntInfoList.add(groupingDetails);
	}
	
				}
		
				irPlusResponseDetails.setCustmrGrpngMngmntInfoList(custmrGrpngMngmntInfoList);
			
			
			
				irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
				
			} else {
				log.info("mmmmmmmmmmmmmmmmm");
				log.debug("Customers not available :: Empty Data :: Data required to show roles");
				irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
		
			}
			
		} catch (Exception e) {
			log.info("qwwwwwwwwww");
			log.error("Exception raised in show list customers",e);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			
			throw new HibernateException(e);
		}
		
		return irPlusResponseDetails;
		}	
	
	

	@Override
	public IRPlusResponseDetails deleteGroup(CustomerGroupingMngmntInfo groupInfo) throws BusinessException {

		

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		Session session = null;

		IrsCustomerGrouping custGrpInfo = null;
		IrsCustomerGroupingDetails custGrpInfoDetails = null;
		try
		{

			session = sessionFactory.getCurrentSession();
			
			custGrpInfo=new IrsCustomerGrouping();
			custGrpInfoDetails=new IrsCustomerGroupingDetails();
			
			custGrpInfo = (IrsCustomerGrouping) session.get(IrsCustomerGrouping.class, groupInfo.getCustomerGrpId());

			if(custGrpInfo!=null)
			{
				custGrpInfo.setIsactive(groupInfo.getIsactive());
				session.update(custGrpInfo);

				
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			}
			else
			{
				irPlusResponseDetails.setValidationSuccess(false);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
			}

		}
		catch(Exception e)
		{
			
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);

			throw new HibernateException(e);
		}

		return irPlusResponseDetails;
	}
	

	@Override
	public IRPlusResponseDetails deleteGroupDetails(CustomerGroupingDetails[] customergroupingdetails) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		Session session = null;

		IrsCustomerGrouping custGrpInfo = null;
		IrsCustomerGroupingDetails custGrpInfoDetails = null;
	try
		{	
		
		
			session = sessionFactory.getCurrentSession();
			
			custGrpInfo=new IrsCustomerGrouping();
			custGrpInfoDetails=new IrsCustomerGroupingDetails();

			Query inactive = session.createQuery("from IrsCustomerGroupingDetails grpdet where grpdet.irsCustomerGrouping.customerGrpId=?");
			inactive.setParameter(0,customergroupingdetails[0].getCustomerGrpId());
			
			List<IrsCustomerGroupingDetails> inactLsist =inactive.list();
		if(inactLsist.size()>0){
				for(int i=0;i<inactLsist.size();i++){

					inactLsist.get(i).setIsactive(customergroupingdetails[0].getIsactive());
					
	
				}
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
				}

			else
			{
				irPlusResponseDetails.setValidationSuccess(false);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
			}

		}
		catch(Exception e)
		{
			
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);

			throw new HibernateException(e);
		}

		return irPlusResponseDetails;
	}
	

	@Override
	public IRPlusResponseDetails getcustomergroupInfo(Integer customerGrpId) throws BusinessException
	{


		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		Session session = null;

		//IRSBank irsBank = null;
		 List<CustomerGroupingMngmntInfo> custmrGrpngMngmntInfoList=new ArrayList<CustomerGroupingMngmntInfo>();

		CustomerGroupingMngmntInfo groupInfo = null;
			
		IrsCustomerGrouping custGrpInfo = null;
		IrsCustomerGroupingDetails custGrpInfoDetails = null;
		try
		{
			session = sessionFactory.getCurrentSession();

			Query group = session.createQuery("from IrsCustomerGroupingDetails grp where grp.irsCustomerGrouping.customerGrpId=?");
			group.setParameter(0,customerGrpId);

			List<IrsCustomerGroupingDetails> grpList =group.list();		
	
			if(grpList.size()>0){
				for(int i=0;i<grpList.size();i++){
					
					groupInfo = new CustomerGroupingMngmntInfo();
					groupInfo.setBankwiseId(grpList.get(i).getBank().getBankId());
					groupInfo.setBankName(grpList.get(i).getBank().getBankName());
					/*groupInfo.setBranchId(grpList.get(i).getBranchId());*/
				
					groupInfo.setBankCustomerCode(grpList.get(i).getIrSCustomers().getBankCustomerCode());
					
					groupInfo.setCreateddate(grpList.get(i).getCreateddate());
					groupInfo.setCreatedtime(grpList.get(i).getCreatedtime());
				/*	groupInfo.setBankCustomerCode(grpList.get(i).getIrSCustomers().getBankCustomerCode());*/
					
					
					groupInfo.setCustomerid(grpList.get(i).getIrSCustomers().getCustomerId());

					groupInfo.setCompanyName(grpList.get(i).getIrSCustomers().getCompanyName());
			
					groupInfo.setCustomerGroupName(grpList.get(i).getIrsCustomerGrouping().getCustomerGroupName());
	
					  Query customer = session.createQuery(" from IrSCustomers as cust inner join cust.irSBankBranch as branch where cust.customerId=:custId");  
					  customer.setParameter("custId",grpList.get(i).getIrSCustomers().getCustomerId());
						
						List<?> customerlist = customer.list();
						if(customerlist.size()>0){
						for(int p=0; p<customerlist.size(); p++) {
							Object[] row = (Object[]) customerlist.get(p);
							IrSCustomers cust = (IrSCustomers)row[0];
							IrSBankBranch branch = (IrSBankBranch)row[1];
							
						    groupInfo.setBranchLocation(branch.getBranchLocation());
						
						
						}
						}
					
					//Query custgroup = session.createQuery("from IrFPBatchHeaderRecord batch where batch.customer.customerId=?");		
					   Query group1 = session.createQuery("select max(c.processedDate) from IrsCustomerGroupingDetails as a inner join a.irSCustomers.irFPBatchHeaderRecord as b inner join b.irFPFileHeaderRecord as c where a.irsCustomerGrouping.customerGrpId=:custgrpId");  
					   group1.setParameter("custgrpId",customerGrpId);
						
						List<?> grouprows = group1.list();
						if(grouprows.size()>0){
						for(int k=0; k<grouprows.size(); k++) {
						
							//groupInfo = new CustomerGroupingMngmntInfo();
							
							Object rows = (Object) grouprows.get(k);
						
					   Query custgroup = session.createQuery("from IrsCustomerGroupingDetails as a inner join a.irSCustomers.irFPBatchHeaderRecord as b inner join b.irFPFileHeaderRecord as c where a.irsCustomerGrouping.customerGrpId=:custGrpId and c.processedDate=:date and b.customer.customerId=:custId");	   
					   custgroup.setParameter("custGrpId",customerGrpId);
					 //  custgroup.setParameter("custGrpId",custgroup.get(i).getIrSCustomers().getCustomerId());
					//custgroup.setParameter("date",grpList.get(i).getIrSCustomers().getCustomerId());
					
					 // String pdate="12/12/2018";
					  custgroup.setParameter("date",rows);
					  
					custgroup.setParameter("custId",grpList.get(i).getIrSCustomers().getCustomerId());
					

					List<?>custgrouplist = custgroup.list();
					if(custgrouplist.size()>0){	
						BigDecimal batchamt = BigDecimal.ZERO;
						int transcount=0;
					
						int transactioncount=0;
						int itemcount=0;
						
					     for(int m=0; m<custgrouplist.size(); m++) {
						//FileDetails = new IrFPBatchHeaderRecord();
						
						Object[] row = (Object[]) custgrouplist.get(m);
						IrsCustomerGroupingDetails icgrp = (IrsCustomerGroupingDetails)row[0];
						IrFPBatchHeaderRecord irBcr = (IrFPBatchHeaderRecord)row[1];
						IrFPFileHeaderRecord irfcr = (IrFPFileHeaderRecord)row[2];			

					    double totalbatchamt=0.00;

							transactioncount+=Integer.parseInt(irBcr.getTransactionCount());
							itemcount+=Integer.parseInt(irBcr.getItemCount());
							String[] convert = (irBcr.getBatchAmount()).split("\\.");
							String a=convert[0];	
						     String b = convert[1];
							a = a.replaceAll(",", "");
							String total =a+"."+b; 
							String value=String.valueOf(total);
							
							 batchamt = batchamt.add(new BigDecimal(value));					
							
						}
					     String dbAmount = batchamt.toString();
							String[] convert = dbAmount.split("\\.");
					        int a = Integer.parseInt(convert[0]);
					        String b = convert[1];

							NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
					     		String	batchAmount = numberFormat.format(a);
					     		
					     	String amountWithComma=null;
					       if(b.equals("00")){
						    	 amountWithComma=batchAmount+".00";
						      }else{
						    	amountWithComma=batchAmount+"."+b;  
						      }
					       


					       groupInfo.setTotalbatchamt(String.valueOf(amountWithComma));
	
						groupInfo.setTotaltransactions(String.valueOf(transactioncount));
						groupInfo.setTotalitems(String.valueOf(itemcount));
					
				
					}

	Query fileQuery = session.createQuery("SELECT COUNT(DISTINCT fhr.fileHeaderRecordId),file.filetypename FROM IrFPFileHeaderRecord as fhr INNER JOIN fhr.irFPBatchHeaderRecord as fbr INNER JOIN fbr.irFPFileHeaderRecord.fileType as file WHERE fbr.customer.customerId=:custId and fhr.processedDate=:date GROUP BY fbr.customer.customerId,file.filetypename");
	fileQuery.setParameter("date",rows);
	fileQuery.setParameter("custId",grpList.get(i).getIrSCustomers().getCustomerId());
					List<?> rowss = fileQuery.list();
					if(rowss.size()>0){
					for(int p=0; p<rowss.size(); p++) {
					
					
						Object[] row = (Object[]) rowss.get(k);

					if(row[1].equals("ACH")) {
						groupInfo.setAchfileCount((Long) row[0]);
					}
					if(row[1].equals("Lockbox")){
						groupInfo.setLockboxfileCount((Long) row[0]);
					}
				
						}
					}	

						}
						}
				
				custmrGrpngMngmntInfoList.add(groupInfo);
				irPlusResponseDetails.setCustmrGrpngMngmntInfoList(custmrGrpngMngmntInfoList);
				
				
				
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			}
			}
			else
			{
				irPlusResponseDetails.setValidationSuccess(false);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
			}			

		}
		catch(Exception e)
		{
			log.error("Exception in updateBank",e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			throw new HibernateException(e);
		}
		return irPlusResponseDetails;
	}


	@Override
	public IRPlusResponseDetails customergrpInact(Integer customerGrpId) throws BusinessException {
		// TODO Auto-generated method stub
		return null;
	}
	@Override	
	public IRPlusResponseDetails updateCustomerMappingGroup(CustomerGroupingMngmntInfo customergroupingMngmntInfo) throws BusinessException {
		
		log.info("todatInside CustomerGroupingMngmntDaoImpl :: updateCustomerMappingGroup");
		Session session = null;
		
		session = sessionFactory.getCurrentSession();
		List<String> errMsgs = new ArrayList<String>();
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		
		
		
		IrMUsers user = new IrMUsers();
		IRSBank bank = new IRSBank();
	//	IrSCustomercontacts irSCustomercontacts = new IrSCustomercontacts();
		try{
			Date date = new Date();
		    DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
		    DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
		    String currentDate = dateFormat.format(date);
		    String currentTime = timeFormat.format(date);
			
			IrsCustomerGrouping irSCustomersGrouping = new IrsCustomerGrouping();
			irSCustomersGrouping.setCreateddate(currentDate);
			irSCustomersGrouping.setCreatedtime(currentTime);
			
			
			irSCustomersGrouping.setCustomerGroupName(customergroupingMngmntInfo.getCustomerGroupName());
			irSCustomersGrouping.setIsactive(customergroupingMngmntInfo.getIsactive());
			irSCustomersGrouping.setUserid(customergroupingMngmntInfo.getUserid());
			user.setUserid(customergroupingMngmntInfo.getUserid());
			irSCustomersGrouping.setIrMUsers(user);

			
				Integer customerGrpId=(Integer)session.save(irSCustomersGrouping);

				customergroupingMngmntInfo.getCustomergroupingDetails()[0].setCustomerGrpId(customerGrpId);
							session.flush();
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			
		
		}catch (Exception e) {
				log.error("Exception raised in create Customer");
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				throw new HibernateException(e);
		}
			

	
	return irPlusResponseDetails;
}
	@Override
	public IRPlusResponseDetails updatecustMappingDetails(CustomerGroupingDetails[] customergroupingdetails) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		Session session = null;
		
		session = sessionFactory.getCurrentSession();
		IrMUsers user = new IrMUsers();
		IRSBank bank = new IRSBank();
		IrSCustomers customer = new IrSCustomers();
	
		IrsCustomerGrouping updategrpdetails=null;
		
		try
		{
			Date date = new Date();
		    DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
		    DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
		    String currentDate = dateFormat.format(date);
		    String currentTime = timeFormat.format(date);
			if(customergroupingdetails.length>0)
			{
				updategrpdetails = (IrsCustomerGrouping) session.get(IrsCustomerGrouping.class,customergroupingdetails[0].getCustomerGrpId());
			

			}
			for(int i=0;i<customergroupingdetails.length;i++)
			{

				IrsCustomerGroupingDetails updateGrpingDetais = new IrsCustomerGroupingDetails(customergroupingdetails[i]);
				
				updateGrpingDetais.setIrsCustomerGrouping(updategrpdetails);
				
				bank.setBankId(customergroupingdetails[i].getBankId());			
				updateGrpingDetais.setBank(bank);
				
				customer.setCustomerId(customergroupingdetails[i].getCustomerId());			
				updateGrpingDetais.setIrSCustomers(customer);
				updateGrpingDetais.setCreateddate(currentDate);
				updateGrpingDetais.setCreatedtime(currentTime);
				
				user.setUserid(customergroupingdetails[i].getUserId());			
				updateGrpingDetais.setIrMUsers(user);
				session.save(updateGrpingDetais);
				session.flush();
				log.debug("Added custGrpingDetais Info :"+updateGrpingDetais);
			}
		
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
		
		}catch(Exception e)
		{
			log.error("Exception in addContact",e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			throw new HibernateException(e);
		}

		
		return irPlusResponseDetails;
	}


	@Override
	public IRPlusResponseDetails createCustomerGrp(CustomerGroupingMngmntInfo customerGroupingMngmntInfo)
			throws BusinessException {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public IRPlusResponseDetails showOneCustomerGrpById(String customerGroupingMngmntId) throws BusinessException {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public IRPlusResponseDetails updateOneCustomerGrps(CustomerGroupingMngmntInfo customerGroupingMngmntInfo)
			throws BusinessException {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public IRPlusResponseDetails statusUpdatCustmrGrpById(StatusIdInfo statusIdInfo) throws BusinessException {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public IRPlusResponseDetails getCustomrGrpDtalsViewById(String customerGroupingMngmntId) throws BusinessException {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public IRPlusResponseDetails showAllCustomerGrps() throws BusinessException {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public IRPlusResponseDetails bankIdArrays() throws BusinessException {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public IRPlusResponseDetails custmrIdArrays(String barnchId) throws BusinessException {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public IRPlusResponseDetails showgroupingReport(CustomerGroupingMngmntInfo customerInfo) throws BusinessException {
	IRPlusResponseDetails irPlusResponseDetails = new  IRPlusResponseDetails();

	Session session = null;
	
	session = sessionFactory.getCurrentSession();
	List<String> errMsgs = new ArrayList<String>();
	CustomerGroupingMngmntInfo FilesInfo=new CustomerGroupingMngmntInfo();
	CustomerGroupingMngmntInfo groupingDetails=null;
	 List<CustomerGroupingMngmntInfo> FiletypeSummary=new ArrayList<CustomerGroupingMngmntInfo>();
	 List<CustomerGroupingMngmntInfo> custmrGrpngMngmntInfoList=new ArrayList<CustomerGroupingMngmntInfo>();

	try {

		   String[] monthnames = {"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};

		   
			Date date = new SimpleDateFormat("MM/dd/yyyy").parse(customerInfo.getFromdate());
			String fromDate = new SimpleDateFormat("MM-dd-yyyy").format(date);
		
			
			Date todate = new SimpleDateFormat("MM/dd/yyyy").parse(customerInfo.getTodate());
			String ToDate = new SimpleDateFormat("MM-dd-yyyy").format(todate);

		/*	Total files*/
/*			
			String sql="select f.file_id from  ir_m_users_bank ub inner join ir_s_customergrouping gr on ub.userid=gr.userid inner join ir_s_customergroupingdetails g on gr.customer_grp_id=g.customer_grp_id inner join ir_s_bankbranch b on g.branch_id=b.branch_id inner join ir_f_file_info f on f.branch_id=g.branch_id inner join ir_f_batch_info bat on f.file_id=bat.file_id where f.processed_date between :start and :end  and g.isactive=1 and ub.userid=:userId and ub.isactive=1 and gr.isactive=1   group by f.file_id";	
			SQLQuery custgroupfile = session.createSQLQuery(sql);
	
		
			custgroupfile.setParameter("start",fromDate);
			custgroupfile.setParameter("end",ToDate);
			custgroupfile.setParameter("userId",customerInfo.getUserid());
List<?> custgroupfileList = custgroupfile.list();
if(custgroupfileList.size()>0){
	FilesInfo.setTotalFiles(custgroupfileList.size());
}
else
{
	
	FilesInfo.setTotalFiles(0);
	
}*/

/*total ach and lkbx*/

/*		   Query totalQuery = session.createQuery("SELECT count(DISTINCT fhr.fileHeaderRecordId),file.filetypename from IrMUsersBank as user inner join user.irSBankbranch as branch inner join branch.irSCustomersgrouping as cust inner join cust.irsCustomerGrouping as group inner join  branch.irFPFileHeaderRecord as fhr inner join  fhr.fileType as file  WHERE fhr.processedDate between :start and :end and user.irMUsers.userid=:userId and user.isactive=:Isactive and group.isactive=:isactive and cust.isactive=:status GROUP BY file.filetypename");	   
			
						totalQuery.setParameter("start",fromDate);
						totalQuery.setParameter("end",ToDate);
						totalQuery.setParameter("userId",customerInfo.getUserid());
						totalQuery.setParameter("Isactive",1);
						totalQuery.setParameter("isactive",1);
						totalQuery.setParameter("status",1);
			
				List<?> totalQuerylist = totalQuery.list();
				if(totalQuerylist.size()>0){
				
				for(int n=0; n<totalQuerylist.size(); n++) {
				
				
				
					Object[] row = (Object[]) totalQuerylist.get(n);

				if(row[1].equals("ACH")) {
					FilesInfo.setAchfileCount((Long) row[0]);
				}
				if(row[1].equals("Lockbox")){
					FilesInfo.setLockboxfileCount((Long) row[0]);
				}
			
					}
			
				}*/
			
				
				/*total customer*/
/*          	Query daywiseCustomers= session.createQuery("select cust.irSCustomers.customerId from IrMUsersBank as user inner join user.irSBankbranch as branch inner join branch.irSCustomersgrouping as cust inner join cust.irsCustomerGrouping as grp  WHERE  user.irMUsers.userid=:userId and user.isactive=:Isactive and grp.isactive=:isactive and cust.isactive=:status group by cust.irSCustomers.customerId");		
*/				
      	
     /* 	String daywiseCust="select g.customer_id from  ir_m_users_bank ub inner join ir_s_customergrouping gr on ub.userid=gr.userid inner join ir_s_customergroupingdetails g on gr.customer_grp_id=g.customer_grp_id inner join ir_s_bankbranch b on g.branch_id=b.branch_id inner join ir_f_file_info f on f.branch_id=g.branch_id inner join ir_f_batch_info bat on f.file_id=bat.file_id and  bat.customer_id = g.customer_id where f.processed_date between :start and :end and g.isactive=1 and ub.userid=:userId and ub.isactive=1 and gr.isactive=1 group by g.customer_id";	

		SQLQuery daywiseCustomers = session.createSQLQuery(daywiseCust);

      	
		daywiseCustomers.setParameter("start",fromDate);
		daywiseCustomers.setParameter("end",ToDate);
		daywiseCustomers.setParameter("userId",customerInfo.getUserid());
		List<?> daywiseCustomersList = daywiseCustomers.list();
					
		if(daywiseCustomersList.size()>0){

			FilesInfo.setTotalCustomers(daywiseCustomersList.size());
		}
		else
		{
			
			FilesInfo.setTotalCustomers(0);
			
		}*/
	/*	total banks*/
		
      	/*String Totalebanks="select g.branch_id from  ir_m_users_bank ub inner join ir_s_customergrouping gr on ub.userid=gr.userid inner join ir_s_customergroupingdetails g on gr.customer_grp_id=g.customer_grp_id inner join ir_s_bankbranch b on g.branch_id=b.branch_id inner join ir_f_file_info f on f.branch_id=g.branch_id inner join ir_f_batch_info bat on f.file_id=bat.file_id  and  bat.customer_id = g.customer_id where f.processed_date between :start and :end and g.isactive=1 and ub.userid=:userId and ub.isactive=1 and gr.isactive=1 group by g.branch_id";	
      	SQLQuery Totdaywisebanks = session.createSQLQuery(Totalebanks);
			Query Totdaywisebanks = session.createQuery("SELECT cust.irSBankBranch.branchId FROM IrMUsersBank as user inner join user.irSBankbranch as branch inner join branch.irSCustomersgrouping as cust inner join cust.irsCustomerGrouping as grp  WHERE user.irMUsers.userid=:userId and user.isactive=:Isactive and grp.isactive=:isactive and cust.isactive=:status group by cust.irSBankBranch.branchId");						
			Totdaywisebanks.setParameter("userId",customerInfo.getUserid());
		Totdaywisebanks.setParameter("start",fromDate);
		Totdaywisebanks.setParameter("end",ToDate);
		List<?> TotdaywisebankList = Totdaywisebanks.list();
		
		if(TotdaywisebankList.size()>0){
		FilesInfo.setTotalBanks(TotdaywisebankList.size());
		}
		
		else
		{
			
			FilesInfo.setTotalBanks(0);
		}*/
		/*bank wised details*/
					
/*			String dayAmt="select distinct(d.customer_grp_id) from ir_m_users_bank u inner join ir_s_bankbranch b on u.branch_id=b.branch_id inner join ir_s_customergroupingdetails d on d.branch_id=b.branch_id   inner join ir_s_customergrouping c on c.customer_grp_id=d.customer_grp_id inner join ir_f_file_info f on f.branch_id=b.branch_id  inner join ir_f_batch_info bt on bt.file_id=f.file_id  where f.processed_date between :start and :end and u.isactive=1 and d.isactive=1 and c.isactive=1 and u.userid=:userId group by d.customer_grp_id";
*/		
		String dayAmt="select  distinct(g.customer_grp_id) from  ir_m_users_bank ub inner join ir_s_customergrouping gr on ub.userid=gr.userid inner join ir_s_customergroupingdetails g on gr.customer_grp_id=g.customer_grp_id inner join ir_s_bank b on g.bank_id=b.bank_id inner join ir_f_file_info f on f.bank_id=g.bank_id inner join ir_f_batch_info bat on f.file_id=bat.file_id  and  bat.customer_id = g.customer_id where f.processed_date between :start and :end  and g.isactive=1 and ub.userid=:userId and ub.isactive=1 and gr.isactive=1 group by g.customer_grp_id";
		
		SQLQuery daywiseAmount = session.createSQLQuery(dayAmt);

		daywiseAmount.setParameter("start",fromDate);
		daywiseAmount.setParameter("end",ToDate);
		daywiseAmount.setParameter("userId",customerInfo.getUserid());

		List<?> daywiseAmountlist =daywiseAmount.list();		
		
		if(daywiseAmountlist.size()>0)
		{
		for(int j=0;j<daywiseAmountlist.size();j++){

			Object custGrpId = (Object) daywiseAmountlist.get(j);
		
			Query grpname = session.createQuery("from IrsCustomerGrouping grp where grp.customerGrpId=:grpId");						
			grpname.setParameter("grpId",custGrpId);
		
			List<IrsCustomerGrouping> grpnameList =grpname.list();
			if(grpnameList.size()>0){

				for(int t=0;t<grpnameList.size();t++){
					groupingDetails=new CustomerGroupingMngmntInfo();
					
					groupingDetails.setCustomerGroupName(grpnameList.get(t).getCustomerGroupName());
					groupingDetails.setCustomerGrpId(grpnameList.get(t).getCustomerGrpId());
			
			/*		Query customer = session.createQuery("from IrsCustomerGroupingDetails grpdet where grpdet.irsCustomerGrouping.customerGrpId=?");
					
					
					customer.setParameter(0,grpnameList.get(t).getCustomerGrpId());
					List<IrsCustomerGroupingDetails> groupdetlist =customer.list();		

					if(groupdetlist.size()>0){						
							groupingDetails.setTotalMappedBranch(groupdetlist.size());

					}*/
					String cust="select  b.customer_id  from  ir_s_customergrouping c inner join ir_s_customergroupingdetails g on c.customer_grp_id=g.customer_grp_id  inner join  ir_m_users_bank u on u.bank_id=g.bank_id inner join ir_s_bank br on br.bank_id=u.bank_id inner join   ir_s_customers cs on cs.customer_id=g.customer_id  inner join ir_f_file_info f on f.bank_id=g.bank_id  inner join ir_f_batch_info b on b.file_id=f.file_id  and b.customer_id=cs.customer_id  where u.userid=:userId and u.isactive=1 and c.isactive=1 and g.isactive=1 and c.customer_grp_id=:grpId  and f.processed_date between :start and :end group by b.customer_id";
					SQLQuery custList = session.createSQLQuery(cust);	
					custList.setParameter("start",fromDate);
					custList.setParameter("end",ToDate);
					custList.setParameter("userId",customerInfo.getUserid());
					custList.setParameter("grpId",custGrpId);
					List<?> custListval =custList.list();		
					if(custListval.size()>0){		
						groupingDetails.setTotalMappedBranch(custListval.size());
					}
					
					
			String bankwiseBatchAmt="select  b.batch_amount  from  ir_s_customergrouping c inner join ir_s_customergroupingdetails g on c.customer_grp_id=g.customer_grp_id  inner join  ir_m_users_bank u on u.bank_id=g.bank_id inner join ir_s_bank br on br.bank_id=u.bank_id inner join   ir_s_customers cs on cs.customer_id=g.customer_id  inner join ir_f_file_info f on f.bank_id=g.bank_id  inner join ir_f_batch_info b on b.file_id=f.file_id  and b.customer_id=cs.customer_id  where u.userid=:userId and u.isactive=1 and c.isactive=1 and g.isactive=1 and c.customer_grp_id=:grpId  and f.processed_date between :start and :end";
			SQLQuery bankwiseBatchAmtdet = session.createSQLQuery(bankwiseBatchAmt);	
			bankwiseBatchAmtdet.setParameter("start",fromDate);
			bankwiseBatchAmtdet.setParameter("end",ToDate);
			bankwiseBatchAmtdet.setParameter("userId",customerInfo.getUserid());
			bankwiseBatchAmtdet.setParameter("grpId",custGrpId);
			List<?> bankwiseBatchAmtdetList =bankwiseBatchAmtdet.list();		
			if(bankwiseBatchAmtdetList.size()>0){

			BigDecimal batchamt = BigDecimal.ZERO;

				for(int g=0;g<bankwiseBatchAmtdetList.size();g++){

					Object batch = (Object) bankwiseBatchAmtdetList.get(g);

					String[] convert = ((String) batch).split("\\.");
					String a=convert[0];	
				     String b = convert[1];
					a = a.replaceAll(",", "");
					String total =a+"."+b; 
					String value=String.valueOf(total);
					
					 batchamt = batchamt.add(new BigDecimal(value));
				
				
				String dbAmount = batchamt.toString();
				String[] converts = dbAmount.split("\\.");
				int aa = Integer.parseInt(converts[0]);
				String bb = converts[1];

				NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
						String	batchAmount = numberFormat.format(aa);
						
					String amountWithComma=null;
				if(bb.equals("00")){
					 amountWithComma=batchAmount+".00";
				  }else{
					amountWithComma=batchAmount+"."+bb;  
				  }
				  groupingDetails.setBankwiseAmount(amountWithComma);
					
					
					
				}
			}
			
			String bankwiseDet="select  sum(cast(b.transaction_count as int))qty, sum(cast(b.item_count as int))item  from  ir_s_customergrouping c inner join ir_s_customergroupingdetails g on c.customer_grp_id=g.customer_grp_id  inner join  ir_m_users_bank u on u.bank_id=g.bank_id inner join ir_s_bank br on br.bank_id=u.bank_id inner join   ir_s_customers cs on cs.customer_id=g.customer_id  inner join ir_f_file_info f on f.bank_id=g.bank_id  inner join ir_f_batch_info b on b.file_id=f.file_id  and b.customer_id=cs.customer_id  where u.userid=:userId and u.isactive=1 and c.isactive=1 and g.isactive=1 and c.customer_grp_id=:GrpId and f.processed_date between :start and :end";
			
			
/*				String bankwiseDet="select   sum(cast(b.transaction_count as int))qty, sum(cast(b.item_count as int))item,sum(cast(b.batch_amount as DECIMAL(9,2)))amont  from  ir_s_customergrouping c inner join ir_s_customergroupingdetails g on c.customer_grp_id=g.customer_grp_id  inner join  ir_m_users_bank u on u.branch_id=g.branch_id inner join ir_s_bankbranch br on br.branch_id=u.branch_id inner join   ir_s_customers cs on cs.customer_id=g.customer_id  inner join ir_f_file_info f on f.branch_id=g.branch_id  inner join ir_f_batch_info b on b.file_id=f.file_id  and b.customer_id=cs.customer_id  where u.userid=131 and u.isactive=1 and c.isactive=1 and g.isactive=1 and c.customer_grp_id=285 and f.processed_date between '07-09-2018' and '07-09-2018'";
*/
			SQLQuery bankwiseDetails = session.createSQLQuery(bankwiseDet);	
			bankwiseDetails.setParameter("start",fromDate);
			bankwiseDetails.setParameter("end",ToDate);
			bankwiseDetails.setParameter("userId",customerInfo.getUserid());
			bankwiseDetails.setParameter("GrpId",custGrpId);
			List<?> bankwiseDetailsList =bankwiseDetails.list();		
			if(bankwiseDetailsList.size()>0){

				for(int m=0;m<bankwiseDetailsList.size();m++){

					Object[] rowss = (Object[]) bankwiseDetailsList.get(m);
			
				       groupingDetails.setTotaltransactions(String.valueOf(rowss[0]));
				       groupingDetails.setTotalitems(String.valueOf(rowss[1]));
				
				}
					
			}
			else
			{
			    groupingDetails.setTotaltransactions("0");
			    groupingDetails.setTotalitems("0");
			}
			

					/*String Bankwisefile="select count(distinct f.file_id),t.filetypename from ir_m_users_bank u inner join ir_s_bankbranch b on u.branch_id=b.branch_id inner join ir_s_customergroupingdetails d on d.branch_id=b.branch_id  inner join ir_s_customergrouping c on c.customer_grp_id=d.customer_grp_id inner join ir_f_file_info f on f.branch_id=b.branch_id  inner join ir_f_batch_info bt on bt.file_id=f.file_id and bt.customer_id = d.customer_id inner join ir_m_filetypes t on f.file_type_id=t.filetypeid where f.processed_date  between :start and :end and u.isactive=1 and d.isactive=1 and c.isactive=1 and u.userid=:userId and c.customer_grp_id=:grpId group by t.filetypename,t.filetypeid";
					SQLQuery Bankwise = session.createSQLQuery(Bankwisefile);
                  
                   Bankwise.setParameter("start",fromDate);
					   Bankwise.setParameter("end",ToDate);
					   Bankwise.setParameter("userId",customerInfo.getUserid());
				
					   Bankwise.setParameter("grpId",custGrpId);
						Query Bankwise = session.createQuery("SELECT count(DISTINCT fhr.fileHeaderRecordId),file.filetypename from IrMUsersBank as user inner join user.irSBankbranch as branch inner join branch.irSCustomersgrouping as cust inner join cust.irsCustomerGrouping as group inner join  branch.irFPFileHeaderRecord as fhr inner join  fhr.fileType as file  WHERE fhr.processedDate between :start and :end and user.irMUsers.userid=:userId and user.isactive=:Isactive and group.isactive=:isactive and cust.isactive=:status and group.customerGrpId=:grpId GROUP BY file.filetypename");	   

			
					List<?> Bankwisefilelist = Bankwise.list();
					if(Bankwisefilelist.size()>0){
					
					for(int m=0; m<Bankwisefilelist.size(); m++) {
						Object[] rowss = (Object[]) Bankwisefilelist.get(m);
					
					if(rowss[1].equals("ACH")) {
						 
						groupingDetails.setBankAchfileCount(((Number) rowss[0]).longValue());
					}
					if(rowss[1].equals("Lockbox")){
						groupingDetails.setBankLockboxfileCount(((Number) rowss[0]).longValue());
					}
			
						}
					}*/
						}
			
			}
			
			custmrGrpngMngmntInfoList.add(groupingDetails);
			
		}
		irPlusResponseDetails.setCustmrGrpngMngmntInfoList(custmrGrpngMngmntInfoList);
												
					} 
				
				else {				
						groupingDetails=null;
					  custmrGrpngMngmntInfoList.add(groupingDetails);
					  irPlusResponseDetails.setCustmrGrpngMngmntInfoList(custmrGrpngMngmntInfoList);
					}
		
		/*String bankwiseDet="select  b.batch_amount  from  ir_s_customergrouping c inner join ir_s_customergroupingdetails g on c.customer_grp_id=g.customer_grp_id  inner join  ir_m_users_bank u on u.branch_id=g.branch_id inner join ir_s_bankbranch br on br.branch_id=u.branch_id inner join   ir_s_customers cs on cs.customer_id=g.customer_id  inner join ir_f_file_info f on f.branch_id=g.branch_id  inner join ir_f_batch_info b on b.file_id=f.file_id  and b.customer_id=cs.customer_id  where u.userid=:userId and u.isactive=1 and c.isactive=1 and g.isactive=1  and f.processed_date between :start and :end";
		SQLQuery bankwiseDetails = session.createSQLQuery(bankwiseDet);	
		bankwiseDetails.setParameter("start",fromDate);
		bankwiseDetails.setParameter("end",ToDate);
		bankwiseDetails.setParameter("userId",customerInfo.getUserid());
	
		List<?> bankwiseDetailsList =bankwiseDetails.list();		
		if(bankwiseDetailsList.size()>0){

		BigDecimal batchamt = BigDecimal.ZERO;

			for(int g=0;g<bankwiseDetailsList.size();g++){

				Object batch = (Object) bankwiseDetailsList.get(g);

				String[] convert = ((String) batch).split("\\.");
				String a=convert[0];	
			     String b = convert[1];
				a = a.replaceAll(",", "");
				String total =a+"."+b; 
				String value=String.valueOf(total);
				 batchamt = batchamt.add(new BigDecimal(value));
			
			String dbAmount = batchamt.toString();
			String[] converts = dbAmount.split("\\.");
			int aa = Integer.parseInt(converts[0]);
			String bb = converts[1];

			NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
					String	batchAmount = numberFormat.format(aa);
					
				String amountWithComma=null;
			if(bb.equals("00")){
				 amountWithComma=batchAmount+".00";
			  }else{
				amountWithComma=batchAmount+"."+bb;  
			  }
			
			  FilesInfo.setTotalbatchamt(String.valueOf(amountWithComma));
				
			}
		}
		else
		{
			 FilesInfo.setTotalbatchamt("0.00");
		}
		*/

/*Query licenseid = session.createQuery(" SELECT site.license.licenseId FROM IRSSite as site WHERE site.siteId=?");			
licenseid.setParameter(0,customerInfo.getSiteId());
List<?> licenseidlist = licenseid.list();

if(licenseidlist.size()>0){
for(int i=0; i<licenseidlist.size(); i++) {
	groupingDetails=new CustomerGroupingMngmntInfo();
Object row = (Object) licenseidlist.get(i);

	
Query fileType = session.createQuery(" SELECT file.filetypeid,file.filetypename FROM IrMFileTypes as file WHERE file.iRSLicense.licenseId=?");			
fileType.setParameter(0,row);
List<?> filTypelist = fileType.list();

if(filTypelist.size()>0){
for(int j=0; j<filTypelist.size(); j++) {
groupingDetails=new CustomerGroupingMngmntInfo();
Object[] rows = (Object[]) filTypelist.get(j);



Query processDetails = session.createQuery("from IrMUsersBank as user inner join user.irSBankbranch as branch inner join branch.irSCustomersgrouping as cust inner join cust.irsCustomerGrouping as group inner join branch.irFPFileHeaderRecord as fhr inner join fhr.irFPBatchHeaderRecord as fbr WHERE fhr.processedDate between :start and :end and user.irMUsers.userid=:userId and user.isactive=:Isactive and group.isactive=:isactive and cust.isactive=:status and fhr.fileType.filetypeid=:Filetype");			
processDetails.setParameter("start",fromDate);
processDetails.setParameter("end",ToDate);
processDetails.setParameter("userId",customerInfo.getUserid());
processDetails.setParameter("Isactive",1);
processDetails.setParameter("isactive",1);
processDetails.setParameter("status",1);
processDetails.setParameter("Filetype",rows[0]);

List<?> processDetailslist =processDetails.list();		

if(processDetailslist.size()>0){

BigDecimal batchamt = BigDecimal.ZERO;
int transactioncount=0;
int itemcount=0;

for(int n=0; n<processDetailslist.size(); n++) {

groupingDetails=new CustomerGroupingMngmntInfo();

Object[] rowval = (Object[]) processDetailslist.get(n);
IrFPFileHeaderRecord irFhr = (IrFPFileHeaderRecord)rowval[4];
IrFPBatchHeaderRecord irBcr = (IrFPBatchHeaderRecord)rowval[5];
transactioncount+=Integer.parseInt(irBcr.getTransactionCount());

itemcount+=Integer.parseInt(irBcr.getItemCount());
String[] convert = (irBcr.getBatchAmount()).split("\\.");
String a=convert[0];	
String b = convert[1];
a = a.replaceAll(",", "");
String total =a+"."+b; 
String value=String.valueOf(total);

batchamt = batchamt.add(new BigDecimal(value));

}

String dbAmount = batchamt.toString();
String[] convert = dbAmount.split("\\.");
int a = Integer.parseInt(convert[0]);
String b = convert[1];

NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
String	batchAmount = numberFormat.format(a);

String amountWithComma=null;
if(b.equals("00")){
amountWithComma=batchAmount+".00";
}else{
amountWithComma=batchAmount+"."+b;  
}

groupingDetails.setTotSummaryAmount(String.valueOf(amountWithComma));
groupingDetails.setTotSummaryItems(String.valueOf(itemcount));
groupingDetails.setTotSummaryTrans(String.valueOf(transactioncount));
groupingDetails.setFileTypeName(rows[1]);
FiletypeSummary.add(groupingDetails);

irPlusResponseDetails.setFiletypeSummary(FiletypeSummary);
}
else
{

groupingDetails.setFileTypeName(rows[1]);
groupingDetails.setTotSummaryAmount("0.00");
groupingDetails.setTotSummaryItems("0");
groupingDetails.setTotSummaryTrans("0");
FiletypeSummary.add(groupingDetails);
irPlusResponseDetails.setFiletypeSummary(FiletypeSummary);

}

}
}
}
}*/

			irPlusResponseDetails.setCustomerGroupingMngmntResponse(FilesInfo);	
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
	} 

	catch(Exception e)
	{
		log.error("Exception in updateBank",e);
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
		irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		throw new HibernateException(e);
	}
	
	return irPlusResponseDetails;
	}


@Override
public IRPlusResponseDetails filterGrpdatewise(CustomerGroupingMngmntInfo grpdateinfo) throws BusinessException {

	IRPlusResponseDetails irPlusResponseDetails = new  IRPlusResponseDetails();

	Session session = null;
	
	session = sessionFactory.getCurrentSession();
	List<String> errMsgs = new ArrayList<String>();
	
	CustomerGroupingMngmntInfo groupingDetails=null;
	 List<CustomerGroupingMngmntInfo> custmrGrpngMngmntInfoList=new ArrayList<CustomerGroupingMngmntInfo>();

	try {
	
		Query customergroup = session.createQuery("from IrsCustomerGrouping group where group.isactive=?");
	
		customergroup.setParameter(0,1);
		
		
		List<IrsCustomerGrouping> irsCustomerGrouping =customergroup.list();
		
	if(irsCustomerGrouping.size()>0){
			for(int i=0;i<irsCustomerGrouping.size();i++){
			    groupingDetails = new CustomerGroupingMngmntInfo();
			 
			    groupingDetails.setCustomerGroupName(irsCustomerGrouping.get(i).getCustomerGroupName());
			    groupingDetails.setFromdate(grpdateinfo.getFromdate());
			    groupingDetails.setTodate(grpdateinfo.getTodate());
			    groupingDetails.setCustomerGrpId(irsCustomerGrouping.get(i).getCustomerGrpId());
		
		   Query grpquery = session.createQuery("from IrsCustomerGroupingDetails as a inner join a.irSCustomers.irFPBatchHeaderRecord as b inner join b.irFPFileHeaderRecord as c where a.irsCustomerGrouping.customerGrpId=:custGrpId and c.processedDate between :start and :end");	   
	  grpquery.setParameter("custGrpId",irsCustomerGrouping.get(i).getCustomerGrpId());

	  grpquery.setParameter("start",grpdateinfo.getFromdate());
	  grpquery.setParameter("end",grpdateinfo.getTodate());

List<?> custgroupdetlist = grpquery.list();
if(custgroupdetlist.size()>0){	
	BigDecimal batchamt = BigDecimal.ZERO;
	int transcount=0;
	double batchamount=0.00;
	int debitamount=0;
	int creditamount=0;
     for(int m=0; m<custgroupdetlist.size(); m++) {
	//FileDetails = new IrFPBatchHeaderRecord();
	
	Object[] row = (Object[]) custgroupdetlist.get(m);
	IrsCustomerGroupingDetails icgrp = (IrsCustomerGroupingDetails)row[0];
	IrFPBatchHeaderRecord irBcr = (IrFPBatchHeaderRecord)row[1];
	IrFPFileHeaderRecord irfcr = (IrFPFileHeaderRecord)row[2];

	String[] convert = (irBcr.getBatchAmount()).split("\\.");
	String a=convert[0];	
     String b = convert[1];
	a = a.replaceAll(",", "");
	String total =a+"."+b; 
	String value=String.valueOf(total);
	
	 batchamt = batchamt.add(new BigDecimal(value));
	
     }
     String dbAmount = batchamt.toString();
		String[] convert = dbAmount.split("\\.");
     int a = Integer.parseInt(convert[0]);
     String b = convert[1];

		NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
  		String	batchAmount = numberFormat.format(a);
  		
  	String amountWithComma=null;
    if(b.equals("00")){
	    	 amountWithComma=batchAmount+".00";
	      }else{
	    	amountWithComma=batchAmount+"."+b;  
	      }

    groupingDetails.setTotalbatchamt(String.valueOf(amountWithComma));

}

		   Query newgrpquery = session.createQuery("SELECT COUNT(DISTINCT fhr.fileHeaderRecordId),file.filetypename from IrsCustomerGroupingDetails as a inner join a.irSCustomers.irFPBatchHeaderRecord as fbr INNER JOIN fbr.irFPFileHeaderRecord as fhr INNER JOIN fhr.fileType as file where a.irsCustomerGrouping.customerGrpId=:custGrpId and fhr.processedDate between :start and :end GROUP BY file.filetypename");	   
		   newgrpquery.setParameter("custGrpId",irsCustomerGrouping.get(i).getCustomerGrpId());
		   newgrpquery.setParameter("start",grpdateinfo.getFromdate());
		   newgrpquery.setParameter("end",grpdateinfo.getTodate());
			List<?> rowval = newgrpquery.list();

			if(rowval.size()>0){
			for(int k=0; k<rowval.size(); k++) {
			
			
				Object[] row = (Object[]) rowval.get(k);

			if(row[1].equals("ACH")) {
				groupingDetails.setAchfileCount((Long) row[0]);
			}
			if(row[1].equals("Lockbox")){
				groupingDetails.setLockboxfileCount((Long) row[0]);
			}
		
				}

			}

custmrGrpngMngmntInfoList.add(groupingDetails);
irPlusResponseDetails.setCustmrGrpngMngmntInfoList(custmrGrpngMngmntInfoList);

			}

			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			
		} 
	
	else {
			irPlusResponseDetails.setValidationSuccess(false);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
		}	
	
	
	
	} 
	
	
	catch(Exception e)
	{
		log.error("Exception in updateBank",e);
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
		irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		throw new HibernateException(e);
	}
	
	return irPlusResponseDetails;
	}	


@Override
public IRPlusResponseDetails getcustomergroupDatewise(CustomerGroupingMngmntInfo customergrpdateinfo) throws BusinessException
{


	IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

	Session session = null;

	 List<CustomerGroupingMngmntInfo> custmrGrpngMngmntInfoList=new ArrayList<CustomerGroupingMngmntInfo>();

	CustomerGroupingMngmntInfo groupInfo = null;
		
	IrsCustomerGrouping custGrpInfo = null;
	IrsCustomerGroupingDetails custGrpInfoDetails = null;
	try
	{

		session = sessionFactory.getCurrentSession();

		Query group = session.createQuery("from IrsCustomerGroupingDetails grp where grp.irsCustomerGrouping.customerGrpId=?");
		group.setParameter(0,customergrpdateinfo.getCustomerGrpId());

		List<IrsCustomerGroupingDetails> grpList =group.list();		

		if(grpList.size()>0){
			for(int i=0;i<grpList.size();i++){
				
				groupInfo = new CustomerGroupingMngmntInfo();

				groupInfo.setCustomerGroupName(grpList.get(i).getIrsCustomerGrouping().getCustomerGroupName());

				   Query custgroup = session.createQuery("from IrsCustomerGroupingDetails as a inner join a.irSCustomers.irFPBatchHeaderRecord as b inner join b.irFPFileHeaderRecord as c where a.irsCustomerGrouping.customerGrpId=:custGrpId and c.processedDate between :start and :end and b.customer.customerId=:custId");	   
				   custgroup.setParameter("custGrpId",customergrpdateinfo.getCustomerGrpId());
				  custgroup.setParameter("start",customergrpdateinfo.getFromdate());
				custgroup.setParameter("end",customergrpdateinfo.getTodate());
				
				custgroup.setParameter("custId",grpList.get(i).getIrSCustomers().getCustomerId());

				List<?>custgrouplist = custgroup.list();
				if(custgrouplist.size()>0){	
					BigDecimal batchamt = BigDecimal.ZERO;
					int transcount=0;
				
					int transactioncount=0;
					int itemcount=0;
					
				     for(int m=0; m<custgrouplist.size(); m++) {
			
					Object[] row = (Object[]) custgrouplist.get(m);
					IrsCustomerGroupingDetails icgrp = (IrsCustomerGroupingDetails)row[0];
					IrFPBatchHeaderRecord irBcr = (IrFPBatchHeaderRecord)row[1];
					IrFPFileHeaderRecord irfcr = (IrFPFileHeaderRecord)row[2];			

						transactioncount+=Integer.parseInt(irBcr.getTransactionCount());
						itemcount+=Integer.parseInt(irBcr.getItemCount());
						String[] convert = (irBcr.getBatchAmount()).split("\\.");
						String a=convert[0];	
					     String b = convert[1];
						a = a.replaceAll(",", "");
						String total =a+"."+b; 
						String value=String.valueOf(total);
						
						 batchamt = batchamt.add(new BigDecimal(value));							
						groupInfo.setHighestdate(irfcr.getProcessedDate());
						
					}
				     
				     String dbAmount = batchamt.toString();
						String[] convert = dbAmount.split("\\.");
				        int a = Integer.parseInt(convert[0]);
				        String b = convert[1];

						NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
				     		String	batchAmount = numberFormat.format(a);
				     		
				     	String amountWithComma=null;
				       if(b.equals("00")){
					    	 amountWithComma=batchAmount+".00";
					      }else{
					    	amountWithComma=batchAmount+"."+b;  
					      }

				       groupInfo.setTotalbatchamt(String.valueOf(amountWithComma));		     
					groupInfo.setTotaltransactions(String.valueOf(transactioncount));
					groupInfo.setTotalitems(String.valueOf(itemcount));
				
			
				}

Query fileQuery = session.createQuery("SELECT COUNT(DISTINCT fhr.fileHeaderRecordId),file.filetypename FROM IrFPFileHeaderRecord as fhr INNER JOIN fhr.irFPBatchHeaderRecord as fbr INNER JOIN fbr.irFPFileHeaderRecord.fileType as file WHERE fbr.customer.customerId=:custId and fhr.processedDate between :start and :end GROUP BY fbr.customer.customerId,file.filetypename");

fileQuery.setParameter("start",customergrpdateinfo.getFromdate());
fileQuery.setParameter("end",customergrpdateinfo.getTodate());
fileQuery.setParameter("custId",grpList.get(i).getIrSCustomers().getCustomerId());
				List<?> rows = fileQuery.list();
				if(rows.size()>0){
				for(int k=0; k<rows.size(); k++) {
				
				
					Object[] row = (Object[]) rows.get(k);

				if(row[1].equals("ACH")) {
					groupInfo.setAchfileCount((Long) row[0]);
				}
				if(row[1].equals("Lockbox")){
					groupInfo.setLockboxfileCount((Long) row[0]);
				}
			
					}
				}	

			custmrGrpngMngmntInfoList.add(groupInfo);
			irPlusResponseDetails.setCustmrGrpngMngmntInfoList(custmrGrpngMngmntInfoList);
			
			}
			
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
		
		}
		else
		{
			irPlusResponseDetails.setValidationSuccess(false);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
		}			

	}
	catch(Exception e)
	{
		log.error("Exception in updateBank",e);
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
		irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		throw new HibernateException(e);
	}
	return irPlusResponseDetails;
}
@Override
public IRPlusResponseDetails showcustomerWiseReport() throws BusinessException
{


	IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

	Session session = null;

	//IRSBank irsBank = null;
	 List<CustomerGroupingMngmntInfo> custmrGrpngMngmntInfoList=new ArrayList<CustomerGroupingMngmntInfo>();

	CustomerGroupingMngmntInfo groupInfo = null;
		
	IrsCustomerGrouping custGrpInfo = null;
	IrsCustomerGroupingDetails custGrpInfoDetails = null;
	try
	{
		session = sessionFactory.getCurrentSession();

		Query customerwise = session.createQuery("SELECT a.customer.customerId FROM IrFPBatchHeaderRecord a GROUP BY a.customer.customerId");			

		
		
		if(customerwise.list().size()>0){
			for(int i=0;i<customerwise.list().size();i++){
				
				groupInfo = new CustomerGroupingMngmntInfo();
				groupInfo.setCustId(customerwise.list().get(i));

				Query custgroups = session.createQuery("from IrSCustomers cust where cust.customerId=?");
				custgroups.setParameter(0,customerwise.list().get(i));
				List<IrSCustomers> custgroupslist =custgroups.list();
				if(custgroupslist.size()>0){
					for(int j=0;j<custgroupslist.size();j++){
						groupInfo.setCompanyName(custgroupslist.get(j).getCompanyName());
						groupInfo.setBankCustomerCode(custgroupslist.get(j).getBankCustomerCode());
						
					}
					
				}
				Query custgroup = session.createQuery("from IrFPBatchHeaderRecord batch where batch.customer.customerId=?");			
				custgroup.setParameter(0,customerwise.list().get(i));
				List<IrFPBatchHeaderRecord> custgrpList =custgroup.list();		
			
				double totalbatchamt=0.00;
				if(custgrpList.size()>0){
					BigDecimal batchamt = BigDecimal.ZERO;
					int transactioncount=0;
					int itemcount=0;
					
					for(int j=0;j<custgrpList.size();j++){
						
					
						transactioncount+=Integer.parseInt(custgrpList.get(j).getTransactionCount());
						itemcount+=Integer.parseInt(custgrpList.get(j).getItemCount());
						String[] convert = (custgrpList.get(j).getBatchAmount()).split("\\.");
						String a=convert[0];	
					     String b = convert[1];
						a = a.replaceAll(",", "");
						String total =a+"."+b; 
						String value=String.valueOf(total);
						
						 batchamt = batchamt.add(new BigDecimal(value));
						
					}
	
					
					String dbAmount = batchamt.toString();
					String[] convert = dbAmount.split("\\.");
			        int a = Integer.parseInt(convert[0]);
			        String b = convert[1];

					NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
			     		String	batchAmount = numberFormat.format(a);
			     		
			     	String amountWithComma=null;
			       if(b.equals("00")){
				    	 amountWithComma=batchAmount+".00";
				      }else{
				    	amountWithComma=batchAmount+"."+b;  
				      }
			       


			       groupInfo.setTotalbatchamt(String.valueOf(amountWithComma));
					
					groupInfo.setTotaltransactions(String.valueOf(transactioncount));
					groupInfo.setTotalitems(String.valueOf(itemcount));
				
			
				}

				
				   Query newcustomerquery = session.createQuery("SELECT COUNT(DISTINCT fhr.fileHeaderRecordId),file.filetypename from IrFPFileHeaderRecord as fhr inner join fhr.irFPBatchHeaderRecord as fbr INNER JOIN fhr.fileType as file where fbr.customer.customerId=:custId GROUP BY file.filetypename");	   

				   newcustomerquery.setParameter("custId",customerwise.list().get(i));
				 
					List<?> customerquery = newcustomerquery.list();
					if(customerquery.size()>0){
					for(int k=0; k<customerquery.size(); k++) {
					
					
						Object[] row = (Object[]) customerquery.get(k);

					if(row[1].equals("ACH")) {
						groupInfo.setAchfileCount((Long) row[0]);
					}
					if(row[1].equals("Lockbox")){
						groupInfo.setLockboxfileCount((Long) row[0]);
					}
						}
					}

			custmrGrpngMngmntInfoList.add(groupInfo);
			irPlusResponseDetails.setCustmrGrpngMngmntInfoList(custmrGrpngMngmntInfoList);
			
			}	
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
		}
		else
		{
			irPlusResponseDetails.setValidationSuccess(false);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
		}			
	}
	catch(Exception e)
	{
		log.error("Exception in updateBank",e);
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
		irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		throw new HibernateException(e);
	}
	return irPlusResponseDetails;
}

@Override
public IRPlusResponseDetails getcustomerdateWise(CustomerGroupingMngmntInfo customerdateinfo) throws BusinessException
{


	IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

	Session session = null;

	//IRSBank irsBank = null;
	 List<CustomerGroupingMngmntInfo> custmrGrpngMngmntInfoList=new ArrayList<CustomerGroupingMngmntInfo>();

	CustomerGroupingMngmntInfo groupInfo = null;
		
	IrsCustomerGrouping custGrpInfo = null;
	IrsCustomerGroupingDetails custGrpInfoDetails = null;
	try
	{
		
	
		session = sessionFactory.getCurrentSession();

		 String betweendate="";
		 String fromDate1="";
		 String ToDate1="";
	     DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
	     betweendate=customerdateinfo.getCurrentdate();
	     String[] dateParts = betweendate.split("-");

	    fromDate1 = dateParts[0]; 
	    ToDate1 = dateParts[1]; 

	   
		Date date = new SimpleDateFormat("MM/dd/yyyy").parse(fromDate1);
		String fromDate = new SimpleDateFormat("MM-dd-yyyy").format(date);
	
		
		Date todate = new SimpleDateFormat("MM/dd/yyyy").parse(ToDate1);
		String ToDate = new SimpleDateFormat("MM-dd-yyyy").format(todate);
		
		
		Query group= session.createQuery("select fhr.processedDate from IrMUsersBank as user inner join user.bank as branch inner join branch.irFPFileHeaderRecord as fhr inner join fhr.irFPBatchHeaderRecord as fbr WHERE  fhr.processedDate between :start and :end  and user.irMUsers.userid=:userId and user.isactive=:Isactive and fbr.customer.customerId=:customerId and fhr.bank.bankId=:branchId group by fhr.processedDate");			

		group.setParameter("start",fromDate);
		group.setParameter("end",ToDate);
		group.setParameter("userId",customerdateinfo.getUserid());
		group.setParameter("Isactive",1);
		group.setParameter("customerId",customerdateinfo.getCustomerid());
		group.setParameter("branchId",customerdateinfo.getBankwiseId());

		List<?> grouprows = group.list();
	
		if(grouprows.size()>0){
		for(int k=0; k<grouprows.size(); k++) {
		
			groupInfo = new CustomerGroupingMngmntInfo();
			
			Object row = (Object) grouprows.get(k);
			Query group1= session.createQuery("from IrMUsersBank as user inner join user.bank as branch inner join branch.irFPFileHeaderRecord as fhr inner join fhr.irFPBatchHeaderRecord as fbr WHERE  fhr.processedDate=:date  and user.irMUsers.userid=:userId and user.isactive=:Isactive and fbr.customer.customerId=:customerId and fhr.bank.bankId=:branchId");			

			group1.setParameter("date",row);	
			group1.setParameter("userId",customerdateinfo.getUserid());
			group1.setParameter("Isactive",1);
			group1.setParameter("customerId",customerdateinfo.getCustomerid());	
			group1.setParameter("branchId",customerdateinfo.getBankwiseId());

			List<?>custgroup1list = group1.list();
			if(custgroup1list.size()>0){	
				BigDecimal batchamt = BigDecimal.ZERO;
				int transcount=0;
				
				int transactioncount=0;
				int itemcount=0;
				
			     for(int m=0; m<custgroup1list.size(); m++) {
				//FileDetails = new IrFPBatchHeaderRecord();
				
				Object[] roww = (Object[]) custgroup1list.get(m);
				IrFPFileHeaderRecord ifhr = (IrFPFileHeaderRecord)roww[2];
				IrFPBatchHeaderRecord ifbr = (IrFPBatchHeaderRecord)roww[3];
				
				double totalbatchamt=0.00;
				transactioncount+=Integer.parseInt(ifbr.getTransactionCount());
				itemcount+=Integer.parseInt(ifbr.getItemCount());
				String[] convert = (ifbr.getBatchAmount()).split("\\.");
				String a=convert[0];	
			     String b = convert[1];
				a = a.replaceAll(",", "");
				String total =a+"."+b; 
				String value=String.valueOf(total);
				
				 batchamt = batchamt.add(new BigDecimal(value));					
				groupInfo.setHighestdate(ifhr.getProcessedDate());
				
			}
		

			groupInfo.setTotaltransactions(String.valueOf(transactioncount));
			groupInfo.setTotalitems(String.valueOf(itemcount));
			  String dbAmount = batchamt.toString();
				String[] convert = dbAmount.split("\\.");
		        int a = Integer.parseInt(convert[0]);
		        String b = convert[1];

				NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
		     		String	batchAmount = numberFormat.format(a);
		     		
		     	String amountWithComma=null;
		       if(b.equals("00")){
			    	 amountWithComma=batchAmount+".00";
			      }else{
			    	amountWithComma=batchAmount+"."+b;  
			      }

		       groupInfo.setTotalbatchamt(String.valueOf(amountWithComma));			
	
		}
			 Query fileQuery = session.createQuery("SELECT COUNT(DISTINCT fhr.fileHeaderRecordId),file.filetypename from IrMUsersBank as user inner join user.bank as branch inner join branch.irFPFileHeaderRecord as fhr inner join  fhr.fileType as file inner join fhr.irFPBatchHeaderRecord as fbr  where fbr.customer.customerId=:customerId and user.irMUsers.userid=:userId and user.isactive=:Isactive and fhr.processedDate=:date and fhr.bank.bankId=:branchId  GROUP BY file.filetypename,fbr.customer.customerId");	   

			 fileQuery.setParameter("customerId",customerdateinfo.getCustomerid());
			   fileQuery.setParameter("userId",customerdateinfo.getUserid());
			   fileQuery.setParameter("Isactive",1);

			   fileQuery.setParameter("date",row);
			   fileQuery.setParameter("branchId",customerdateinfo.getBankwiseId());

							List<?> rows = fileQuery.list();
							if(rows.size()>0){
							for(int l=0; l<rows.size(); l++) {
							
							
								Object[] rowss = (Object[]) rows.get(l);

							if(rowss[1].equals("ACH")) {
								groupInfo.setAchfileCount((Long) rowss[0]);
							}
							if(rowss[1].equals("Lockbox")){
								groupInfo.setLockboxfileCount((Long) rowss[0]);
							}
						
								}
							}		

			custmrGrpngMngmntInfoList.add(groupInfo);
			irPlusResponseDetails.setCustmrGrpngMngmntInfoList(custmrGrpngMngmntInfoList);

			}	
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
		}
		else
		{
			irPlusResponseDetails.setValidationSuccess(false);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
		}			

	}
	catch(Exception e)
	{
		log.error("Exception in updateBank",e);
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
		irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		throw new HibernateException(e);
	}
	return irPlusResponseDetails;
}
@Override
public IRPlusResponseDetails filterCustomerdatewise(CustomerGroupingMngmntInfo customerdateinfo) throws BusinessException {

	IRPlusResponseDetails irPlusResponseDetails = new  IRPlusResponseDetails();

	Session session = null;
	
	session = sessionFactory.getCurrentSession();
	List<String> errMsgs = new ArrayList<String>();
	
	CustomerGroupingMngmntInfo groupingDetails=null;
	 List<CustomerGroupingMngmntInfo> custmrGrpngMngmntInfoList=new ArrayList<CustomerGroupingMngmntInfo>();
   try
	 {
	 
	   session = sessionFactory.getCurrentSession();
	   
	   
	   /*from date and to date */
		if(customerdateinfo.getBankName().isEmpty()&&customerdateinfo.getCompanyName().isEmpty()&&customerdateinfo.getCustomerid()==null&&customerdateinfo.getFromdate()!=null&&customerdateinfo.getTodate()!=null)
		{
			   log.info("bank,company,cisutid===nullllll and from ,to====not nulll");
				Query group1 = session.createQuery("select b.customer.customerId from IrFPFileHeaderRecord as a inner join a.irFPBatchHeaderRecord as b  where  a.processedDate between :start and :end  group by b.customer.customerId"); 
				
				group1.setParameter("start",customerdateinfo.getFromdate());
				group1.setParameter("end",customerdateinfo.getTodate());

			
			
				List<?>custgroup1list = group1.list();
				if(custgroup1list.size()>0){	
					
			
					
				     for(int m=0; m<custgroup1list.size(); m++) {
					//FileDetails = new IrFPBatchHeaderRecord();
				    	 groupingDetails = new CustomerGroupingMngmntInfo();
					Object row = (Object) custgroup1list.get(m);
					groupingDetails.setCustId(row);
			
					Query custgroups = session.createQuery("from IrSCustomers cust where cust.customerId=?");
					custgroups.setParameter(0,row);
					List<IrSCustomers> custgroupslist =custgroups.list();
					if(custgroupslist.size()>0){
						for(int j=0;j<custgroupslist.size();j++){
							groupingDetails.setCompanyName(custgroupslist.get(j).getCompanyName());
							groupingDetails.setBankCustomerCode(custgroupslist.get(j).getBankCustomerCode());
						}
						
					}			
					
					
					Query group2 = session.createQuery("from IrFPFileHeaderRecord as a inner join a.irFPBatchHeaderRecord as b where a.processedDate between :start and :end and b.customer.customerId=:custId"); 
					
					group2.setParameter("start",customerdateinfo.getFromdate());
					group2.setParameter("end",customerdateinfo.getTodate());
					group2.setParameter("custId",row);

				
				
					List<?>custgroup2list = group2.list();
					if(custgroup2list.size()>0){	
						int transcount=0;
						BigDecimal batchamt = BigDecimal.ZERO;
						int transactioncount=0;
						int itemcount=0;
						
					     for(int n=0; n<custgroup2list.size(); n++) {
						//FileDetails = new IrFPBatchHeaderRecord();
						
						Object[] roww = (Object[]) custgroup2list.get(n);
						IrFPFileHeaderRecord ifhr = (IrFPFileHeaderRecord)roww[0];
						IrFPBatchHeaderRecord ifbr = (IrFPBatchHeaderRecord)roww[1];
						
						
						double totalbatchamt=0.00;

						transactioncount+=Integer.parseInt(ifbr.getTransactionCount());
						itemcount+=Integer.parseInt(ifbr.getItemCount());
						String[] convert = (ifbr.getBatchAmount()).split("\\.");
						String a=convert[0];	
					     String b = convert[1];
						a = a.replaceAll(",", "");
						String total =a+"."+b; 
						String value=String.valueOf(total);
						
						 batchamt = batchamt.add(new BigDecimal(value));						
					//groupingDetails.setHighestdate(ifhr.getProcessedDate());
						
					}
				
					  String dbAmount = batchamt.toString();
						String[] convert = dbAmount.split("\\.");
				        int a = Integer.parseInt(convert[0]);
				        String b = convert[1];

						NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
				     		String	batchAmount = numberFormat.format(a);
				     		
				     	String amountWithComma=null;
				       if(b.equals("00")){
					    	 amountWithComma=batchAmount+".00";
					      }else{
					    	amountWithComma=batchAmount+"."+b;  
					      }
				       


				       groupingDetails.setTotalbatchamt(String.valueOf(amountWithComma));
					groupingDetails.setTotaltransactions(String.valueOf(transactioncount));
					groupingDetails.setTotalitems(String.valueOf(itemcount));
			
			
				}			

				     
				Query fileQuery = session.createQuery("SELECT COUNT(DISTINCT fhr.fileHeaderRecordId),file.filetypename FROM IrFPFileHeaderRecord as fhr INNER JOIN fhr.irFPBatchHeaderRecord as fbr INNER JOIN fbr.irFPFileHeaderRecord.fileType as file WHERE  fhr.processedDate between :start and :end and  fbr.customer.customerId=:custId GROUP BY fbr.customer.customerId,file.filetypename");
				//fileQuery.setParameter("date",pdate);
				fileQuery.setParameter("start",customerdateinfo.getFromdate());
				fileQuery.setParameter("end",customerdateinfo.getTodate());
			
				fileQuery.setParameter("custId",row);
								List<?> rows = fileQuery.list();
								if(rows.size()>0){
								for(int l=0; l<rows.size(); l++) {
								
								
									Object[] rowss = (Object[]) rows.get(l);

								if(rowss[1].equals("ACH")) {
								

									groupingDetails.setAchfileCount((Long) rowss[0]);
								}
								if(rowss[1].equals("Lockbox")){
									groupingDetails.setLockboxfileCount((Long) rowss[0]);
								}
							
									}
								}		

				custmrGrpngMngmntInfoList.add(groupingDetails);
				irPlusResponseDetails.setCustmrGrpngMngmntInfoList(custmrGrpngMngmntInfoList);
				
				
				
				
				
				
				}	
		}
		}
		
		  /*from date and to date and bank */
				if(!customerdateinfo.getBankName().isEmpty()&&customerdateinfo.getCompanyName().isEmpty()&&customerdateinfo.getCustomerid()==null&&customerdateinfo.getFromdate()!=null&&customerdateinfo.getTodate()!=null)
				{
				
			   Query group1 = session.createQuery("select b.customer.customerId from IrFPFileHeaderRecord as a inner join a.irFPBatchHeaderRecord as b  where  a.processedDate between :start and :end and a.bank.bankId=:bankId group by b.customer.customerId"); 
				
				group1.setParameter("start",customerdateinfo.getFromdate());
				group1.setParameter("end",customerdateinfo.getTodate());
				group1.setParameter("bankId",customerdateinfo.getBankName());
			
			
				List<?>custgroup1list = group1.list();
				if(custgroup1list.size()>0){	
					
			
					
				     for(int m=0; m<custgroup1list.size(); m++) {
					//FileDetails = new IrFPBatchHeaderRecord();
				    	 groupingDetails = new CustomerGroupingMngmntInfo();
					Object row = (Object) custgroup1list.get(m);
					groupingDetails.setCustId(row);
			
					Query custgroups = session.createQuery("from IrSCustomers cust where cust.customerId=?");
					custgroups.setParameter(0,row);
					List<IrSCustomers> custgroupslist =custgroups.list();
					if(custgroupslist.size()>0){
						for(int j=0;j<custgroupslist.size();j++){
							groupingDetails.setCompanyName(custgroupslist.get(j).getCompanyName());
							groupingDetails.setBankCustomerCode(custgroupslist.get(j).getBankCustomerCode());
						}
						
					}			
					
					
					Query group2 = session.createQuery("from IrFPFileHeaderRecord as a inner join a.irFPBatchHeaderRecord as b where a.processedDate between :start and :end and b.customer.customerId=:custId and a.bank.bankId=:bankId"); 
					
					group2.setParameter("start",customerdateinfo.getFromdate());
					group2.setParameter("end",customerdateinfo.getTodate());
					group2.setParameter("bankId",customerdateinfo.getBankName());
					group2.setParameter("custId",row);
					
				
				
					List<?>custgroup2list = group2.list();
					if(custgroup2list.size()>0){	
						int transcount=0;
						BigDecimal batchamt = BigDecimal.ZERO;
						int transactioncount=0;
						int itemcount=0;
						
					     for(int n=0; n<custgroup2list.size(); n++) {
						//FileDetails = new IrFPBatchHeaderRecord();
						
						Object[] roww = (Object[]) custgroup2list.get(n);
						IrFPFileHeaderRecord ifhr = (IrFPFileHeaderRecord)roww[0];
						IrFPBatchHeaderRecord ifbr = (IrFPBatchHeaderRecord)roww[1];
						
						
						double totalbatchamt=0.00;

						transactioncount+=Integer.parseInt(ifbr.getTransactionCount());
						itemcount+=Integer.parseInt(ifbr.getItemCount());
						String[] convert = (ifbr.getBatchAmount()).split("\\.");
						String a=convert[0];	
					     String b = convert[1];
						a = a.replaceAll(",", "");
						String total =a+"."+b; 
						String value=String.valueOf(total);
						
						 batchamt = batchamt.add(new BigDecimal(value));						
					//groupingDetails.setHighestdate(ifhr.getProcessedDate());
						
					}
				
					  String dbAmount = batchamt.toString();
						String[] convert = dbAmount.split("\\.");
				        int a = Integer.parseInt(convert[0]);
				        String b = convert[1];

						NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
				     		String	batchAmount = numberFormat.format(a);
				     		
				     	String amountWithComma=null;
				       if(b.equals("00")){
					    	 amountWithComma=batchAmount+".00";
					      }else{
					    	amountWithComma=batchAmount+"."+b;  
					      }
				       


				       groupingDetails.setTotalbatchamt(String.valueOf(amountWithComma));
					groupingDetails.setTotaltransactions(String.valueOf(transactioncount));
					groupingDetails.setTotalitems(String.valueOf(itemcount));
			
			
				}			

				     
				Query fileQuery = session.createQuery("SELECT COUNT(DISTINCT fhr.fileHeaderRecordId),file.filetypename FROM IrFPFileHeaderRecord as fhr INNER JOIN fhr.irFPBatchHeaderRecord as fbr INNER JOIN fbr.irFPFileHeaderRecord.fileType as file WHERE  fhr.processedDate between :start and :end and  fbr.customer.customerId=:custId and fhr.bank.bankId=:bankId GROUP BY fbr.customer.customerId,file.filetypename");
				//fileQuery.setParameter("date",pdate);
				fileQuery.setParameter("start",customerdateinfo.getFromdate());
				fileQuery.setParameter("end",customerdateinfo.getTodate());
				fileQuery.setParameter("bankId",customerdateinfo.getBankName());
				fileQuery.setParameter("custId",row);
								List<?> rows = fileQuery.list();
								if(rows.size()>0){
								for(int l=0; l<rows.size(); l++) {
								
								
									Object[] rowss = (Object[]) rows.get(l);

								if(rowss[1].equals("ACH")) {
								

									groupingDetails.setAchfileCount((Long) rowss[0]);
								}
								if(rowss[1].equals("Lockbox")){
									groupingDetails.setLockboxfileCount((Long) rowss[0]);
								}
							
									}
								}		

				custmrGrpngMngmntInfoList.add(groupingDetails);
				irPlusResponseDetails.setCustmrGrpngMngmntInfoList(custmrGrpngMngmntInfoList);
				
				
				
				
				
				
				}	
		}
			   
			   
		}
				
				/*customer id */
				if(customerdateinfo.getBankName().isEmpty()&&customerdateinfo.getCompanyName().isEmpty()&&customerdateinfo.getCustomerid()!=null&&customerdateinfo.getFromdate()==null&&customerdateinfo.getTodate()==null)
				{
	
			
					Query custgroups = session.createQuery("from IrSCustomers cust where cust.customerId=?");
					custgroups.setParameter(0,customerdateinfo.getCustomerid());
					List<IrSCustomers> custgroupslist =custgroups.list();
					if(custgroupslist.size()>0){
						for(int j=0;j<custgroupslist.size();j++){
							 groupingDetails = new CustomerGroupingMngmntInfo();
							groupingDetails.setCompanyName(custgroupslist.get(j).getCompanyName());
							groupingDetails.setBankCustomerCode(custgroupslist.get(j).getBankCustomerCode());
								
					
					
					Query group2 = session.createQuery("from IrFPFileHeaderRecord as a inner join a.irFPBatchHeaderRecord as b where b.customer.customerId=:custId"); 
				
					
					group2.setParameter("custId",customerdateinfo.getCustomerid());
					
				
				
					List<?>custgroup2list = group2.list();
					if(custgroup2list.size()>0){	
						int transcount=0;
						BigDecimal batchamt = BigDecimal.ZERO;
						int transactioncount=0;
						int itemcount=0;
						
					     for(int n=0; n<custgroup2list.size(); n++) {
						//FileDetails = new IrFPBatchHeaderRecord();
					    	
						Object[] roww = (Object[]) custgroup2list.get(n);
						IrFPFileHeaderRecord ifhr = (IrFPFileHeaderRecord)roww[0];
						IrFPBatchHeaderRecord ifbr = (IrFPBatchHeaderRecord)roww[1];
						
						
						double totalbatchamt=0.00;

						transactioncount+=Integer.parseInt(ifbr.getTransactionCount());
						itemcount+=Integer.parseInt(ifbr.getItemCount());
						String[] convert = (ifbr.getBatchAmount()).split("\\.");
						String a=convert[0];	
					     String b = convert[1];
						a = a.replaceAll(",", "");
						String total =a+"."+b; 
						String value=String.valueOf(total);
						
						 batchamt = batchamt.add(new BigDecimal(value));						
					//groupingDetails.setHighestdate(ifhr.getProcessedDate());
						
					}
				
					  String dbAmount = batchamt.toString();
						String[] convert = dbAmount.split("\\.");
				        int a = Integer.parseInt(convert[0]);
				        String b = convert[1];

						NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
				     		String	batchAmount = numberFormat.format(a);
				     		
				     	String amountWithComma=null;
				       if(b.equals("00")){
					    	 amountWithComma=batchAmount+".00";
					      }else{
					    	amountWithComma=batchAmount+"."+b;  
					      }
				       


				       groupingDetails.setTotalbatchamt(String.valueOf(amountWithComma));
					groupingDetails.setTotaltransactions(String.valueOf(transactioncount));
					groupingDetails.setTotalitems(String.valueOf(itemcount));
			
			
				}			

				     
				Query fileQuery = session.createQuery("SELECT COUNT(DISTINCT fhr.fileHeaderRecordId),file.filetypename FROM IrFPFileHeaderRecord as fhr INNER JOIN fhr.irFPBatchHeaderRecord as fbr INNER JOIN fbr.irFPFileHeaderRecord.fileType as file WHERE  fbr.customer.customerId=:custId  GROUP BY fbr.customer.customerId,file.filetypename");

			
				fileQuery.setParameter("custId",customerdateinfo.getCustomerid());
								List<?> rows = fileQuery.list();
								if(rows.size()>0){
								for(int l=0; l<rows.size(); l++) {
								
								
									Object[] rowss = (Object[]) rows.get(l);

								if(rowss[1].equals("ACH")) {
								

									groupingDetails.setAchfileCount((Long) rowss[0]);
								}
								if(rowss[1].equals("Lockbox")){
									groupingDetails.setLockboxfileCount((Long) rowss[0]);
								}
							
									}
								}		
	}
						
					}
				custmrGrpngMngmntInfoList.add(groupingDetails);
				irPlusResponseDetails.setCustmrGrpngMngmntInfoList(custmrGrpngMngmntInfoList);

				}	
				


				irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			}
		/*	else
			{
				irPlusResponseDetails.setValidationSuccess(false);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
			}*/			

		
	
	catch(Exception e)
	{
		log.error("Exception in updateBank",e);
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
		irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		throw new HibernateException(e);
	}
	
	return irPlusResponseDetails;
	}	

/*@Override
public IRPlusResponseDetails getdatacustomerDatewise(CustomerGroupingMngmntInfo customerdatewiseinfo) throws BusinessException
{


	IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

	Session session = null;

	//IRSBank irsBank = null;
	 List<CustomerGroupingMngmntInfo> custmrGrpngMngmntInfoList=new ArrayList<CustomerGroupingMngmntInfo>();

	CustomerGroupingMngmntInfo groupInfo = null;
		
	IrsCustomerGrouping custGrpInfo = null;
	IrsCustomerGroupingDetails custGrpInfoDetails = null;
	try
	{
		
		

		session = sessionFactory.getCurrentSession();

			Query group1 = session.createQuery("from IrFPFileHeaderRecord as a inner join a.irFPBatchHeaderRecord as b  where a.processedDate between :start and :end and  b.customer.customerId=:custId"); 
					
			group1.setParameter("start",customerdatewiseinfo.getFromdate());
			group1.setParameter("end",customerdatewiseinfo.getTodate());
		
			group1.setParameter("custId",customerdatewiseinfo.getCustomerid());
		
		
			List<?>custgroup1list = group1.list();
			if(custgroup1list.size()>0){	
				
				int transcount=0;
				BigDecimal batchamt = BigDecimal.ZERO;
				int transactioncount=0;
				int itemcount=0;
				
			     for(int m=0; m<custgroup1list.size(); m++) {
				//FileDetails = new IrFPBatchHeaderRecord();
			    	 groupInfo=new CustomerGroupingMngmntInfo();
				Object[] roww = (Object[]) custgroup1list.get(m);
				IrFPFileHeaderRecord ifhr = (IrFPFileHeaderRecord)roww[0];
				IrFPBatchHeaderRecord ifbr = (IrFPBatchHeaderRecord)roww[1];
				
				
			

				transactioncount+=Integer.parseInt(ifbr.getTransactionCount());
				itemcount+=Integer.parseInt(ifbr.getItemCount());
				String[] convert = (ifbr.getBatchAmount()).split("\\.");
				String a=convert[0];	
			     String b = convert[1];
				a = a.replaceAll(",", "");
				String total =a+"."+b; 
				String value=String.valueOf(total);
				
				 batchamt = batchamt.add(new BigDecimal(value));						
				groupInfo.setHighestdate(ifhr.getProcessedDate());
				
				
				
				
				
				Query fileQuery = session.createQuery("SELECT COUNT(DISTINCT fhr.fileHeaderRecordId),file.filetypename FROM IrFPFileHeaderRecord as fhr INNER JOIN fhr.irFPBatchHeaderRecord as fbr INNER JOIN fbr.irFPFileHeaderRecord.fileType as file WHERE fhr.processedDate between :start and :end and  fbr.customer.customerId=:custId GROUP BY fbr.customer.customerId,file.filetypename");
				//fileQuery.setParameter("date",pdate);
				fileQuery.setParameter("start",customerdatewiseinfo.getFromdate());
				fileQuery.setParameter("end",customerdatewiseinfo.getTodate());
			
				fileQuery.setParameter("custId",customerdatewiseinfo.getCustomerid());
								List<?> rows = fileQuery.list();
								if(rows.size()>0){
								for(int l=0; l<rows.size(); l++) {
								
								
									Object[] rowss = (Object[]) rows.get(l);

								if(rowss[1].equals("ACH")) {
									groupInfo.setAchfileCount((Long) rowss[0]);
									
								}
								if(rowss[1].equals("Lockbox")){
									groupInfo.setLockboxfileCount((Long) rowss[0]);
								}
							
									}
								}	
				

				
			}
		
			groupInfo.setTotaltransactions(String.valueOf(transactioncount));
			groupInfo.setTotalitems(String.valueOf(itemcount));
		
			  String dbAmount = batchamt.toString();
				String[] convert = dbAmount.split("\\.");
		        int a = Integer.parseInt(convert[0]);
		        String b = convert[1];

				NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
		     		String	batchAmount = numberFormat.format(a);
		     		
		     	String amountWithComma=null;
		       if(b.equals("00")){
			    	 amountWithComma=batchAmount+".00";
			      }else{
			    	amountWithComma=batchAmount+"."+b;  
			      }
		       groupInfo.setTotalbatchamt(String.valueOf(amountWithComma));
			
			
		
			

			
			custmrGrpngMngmntInfoList.add(groupInfo);
			irPlusResponseDetails.setCustmrGrpngMngmntInfoList(custmrGrpngMngmntInfoList);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
		}
			else
			{
				irPlusResponseDetails.setValidationSuccess(false);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
			}
			

	}
	catch(Exception e)
	{
		log.error("Exception in updateBank",e);
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
		irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		throw new HibernateException(e);
	}
	return irPlusResponseDetails;
}*/


@Override
public IRPlusResponseDetails getdatacustomerDatewise(CustomerGroupingMngmntInfo customerdatewiseinfo) throws BusinessException
{


	IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

	Session session = null;

	//IRSBank irsBank = null;
	 List<CustomerGroupingMngmntInfo> custmrGrpngMngmntInfoList=new ArrayList<CustomerGroupingMngmntInfo>();

	CustomerGroupingMngmntInfo groupingDetails = null;
		
	IrsCustomerGrouping custGrpInfo = null;
	IrsCustomerGroupingDetails custGrpInfoDetails = null;
	try
	{

		session = sessionFactory.getCurrentSession();

		if(customerdatewiseinfo.getFromdate()!=null) {


			Criteria criteria = session.createCriteria(IrFPBatchHeaderRecord.class,"batchlist");
		criteria.createAlias("batchlist.customer", "batch");
		criteria.createAlias("batchlist.irFPFileHeaderRecord", "file");
		criteria.createAlias("batchlist.irFPFileHeaderRecord.bank", "bank");

		criteria.createAlias("batchlist.customer.irsCustomerGroupingDetails.irsCustomerGrouping", "customerGrp");

	
		if(customerdatewiseinfo.getBankName()!=null&&!customerdatewiseinfo.getBankName().isEmpty())
			{
				criteria.add(Restrictions.eq("bank.bankName",(customerdatewiseinfo.getBankName())));

			}
			if(customerdatewiseinfo.getCustomerid()!=null)
			{
				criteria.add(Restrictions.eq("batch.customerId",new Integer(customerdatewiseinfo.getCustomerid())));

			}
		
			if(customerdatewiseinfo.getCompanyName()!=null&&!customerdatewiseinfo.getCompanyName().isEmpty())
			{
				criteria.add(Restrictions.eq("batch.companyName",customerdatewiseinfo.getCompanyName()));
			}

			if(customerdatewiseinfo.getFromdate()!=null&&!customerdatewiseinfo.getFromdate().isEmpty())
			{
				criteria.add(Restrictions.ge("file.processedDate",(customerdatewiseinfo.getFromdate())));

			}	if(customerdatewiseinfo.getTodate()!=null&&!customerdatewiseinfo.getTodate().isEmpty())
			{
				criteria.add(Restrictions.le("file.processedDate",(customerdatewiseinfo.getTodate())));

			}
			
			List<IrFPBatchHeaderRecord>custgroup2list = (List)criteria.list();
			
			if(custgroup2list.size()>0){	
				int transcount=0;
				BigDecimal batchamt = BigDecimal.ZERO;
				int transactioncount=0;
				int itemcount=0;
				
			     for(int n=0; n<custgroup2list.size(); n++) {
			    	 groupingDetails=new CustomerGroupingMngmntInfo();

				groupingDetails.setCompanyName(custgroup2list.get(n).getCustomer().getCompanyName());
				groupingDetails.setBankCustomerCode(custgroup2list.get(n).getCustomer().getBankCustomerCode());
				groupingDetails.setCustId(custgroup2list.get(n).getCustomer().getCustomerId());
				double totalbatchamt=0.00;
			
				
				transactioncount+=Integer.parseInt(custgroup2list.get(n).getTransactionCount());
				itemcount+=Integer.parseInt(custgroup2list.get(n).getItemCount());
				String[] convert = (custgroup2list.get(n).getBatchAmount()).split("\\.");
				String a=convert[0];	
			     String b = convert[1];
				a = a.replaceAll(",", "");
				String total =a+"."+b; 
				String value=String.valueOf(total);
				
				 batchamt = batchamt.add(new BigDecimal(value));			
				 
				 
				 if(customerdatewiseinfo.getBankName()!=null&&!customerdatewiseinfo.getBankName().isEmpty()) {
						Query fileQuery = session.createQuery("SELECT COUNT(DISTINCT fhr.fileHeaderRecordId),file.filetypename FROM IrFPFileHeaderRecord as fhr INNER JOIN fhr.irFPBatchHeaderRecord as fbr INNER JOIN fbr.irFPFileHeaderRecord.fileType as file WHERE  fhr.bank.bankName=:bankName GROUP BY fbr.customer.customerId,file.filetypename");
					
					//	fileQuery.setParameter("bankName",custgroup2list.get(n).getCustomer().getCustomerId());
					
						fileQuery.setParameter("bankName",customerdatewiseinfo.getBankName());
										List<?> rows = fileQuery.list();
										if(rows.size()>0){
										for(int l=0; l<rows.size(); l++) {
										
										
											Object[] rowss = (Object[]) rows.get(l);

										if(rowss[1].equals("ACH")) {
										

											groupingDetails.setAchfileCount((Long) rowss[0]);
										}
										if(rowss[1].equals("Lockbox")){
											groupingDetails.setLockboxfileCount((Long) rowss[0]);
										}
									
											}
										}	
					 }
				 
				 if(customerdatewiseinfo.getCompanyName()!=null&&!customerdatewiseinfo.getCompanyName().isEmpty()) {
						Query fileQuery = session.createQuery("SELECT COUNT(DISTINCT fhr.fileHeaderRecordId),file.filetypename FROM IrFPFileHeaderRecord as fhr INNER JOIN fhr.irFPBatchHeaderRecord as fbr INNER JOIN fbr.irFPFileHeaderRecord.fileType as file WHERE  fhr.bank.bankName=:bankName GROUP BY fbr.customer.customerId,file.filetypename");
					
						//fileQuery.setParameter("bankName",custgroup2list.get(n).getCustomer().getCustomerId());
					
						fileQuery.setParameter("bankName",customerdatewiseinfo.getBankName());
										List<?> rows = fileQuery.list();
										if(rows.size()>0){
										for(int l=0; l<rows.size(); l++) {
										
										
											Object[] rowss = (Object[]) rows.get(l);

										if(rowss[1].equals("ACH")) {
										

											groupingDetails.setAchfileCount((Long) rowss[0]);
										}
										if(rowss[1].equals("Lockbox")){
											groupingDetails.setLockboxfileCount((Long) rowss[0]);
										}
									
											}
										}	
					 }
				 if(!customerdatewiseinfo.getFromdate().equals("NaN-NaN-NaN")) {
					 
					Query fileQuery = session.createQuery("SELECT COUNT(DISTINCT fhr.fileHeaderRecordId),file.filetypename FROM IrFPFileHeaderRecord as fhr INNER JOIN fhr.irFPBatchHeaderRecord as fbr INNER JOIN fbr.irFPFileHeaderRecord.fileType as file WHERE  fhr.processedDate between :start and :end and  fbr.customer.customerId=:custId GROUP BY fbr.customer.customerId,file.filetypename");
				
					fileQuery.setParameter("start",customerdatewiseinfo.getFromdate());
					fileQuery.setParameter("end",customerdatewiseinfo.getTodate());
				
					fileQuery.setParameter("custId",custgroup2list.get(n).getCustomer().getCustomerId());
									List<?> rows = fileQuery.list();
									if(rows.size()>0){
									for(int l=0; l<rows.size(); l++) {
									
									
										Object[] rowss = (Object[]) rows.get(l);

									if(rowss[1].equals("ACH")) {
									

										groupingDetails.setAchfileCount((Long) rowss[0]);
									}
									if(rowss[1].equals("Lockbox")){
										groupingDetails.setLockboxfileCount((Long) rowss[0]);
									}
								
										}
									}	
									 
				 
			}
			     }
			     String dbAmount = batchamt.toString();
					String[] convert = dbAmount.split("\\.");
			        int a = Integer.parseInt(convert[0]);
			        String b = convert[1];

					NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
			     		String	batchAmount = numberFormat.format(a);
			     		
			     	String amountWithComma=null;
			       if(b.equals("00")){
				    	 amountWithComma=batchAmount+".00";
				      }else{
				    	amountWithComma=batchAmount+"."+b;  
				      }
			       
			       groupingDetails.setTotalbatchamt(String.valueOf(amountWithComma));
			       groupingDetails.setTotaltransactions(String.valueOf(transactioncount));
			       groupingDetails.setTotalitems(String.valueOf(itemcount));
			       custmrGrpngMngmntInfoList.add(groupingDetails);

			}
			     }

			/*from and to date with customer id*/
			
			if(customerdatewiseinfo.getFromdate()!=null && !customerdatewiseinfo.getBankCustomercode().isEmpty()) {
					
			Criteria criteria = session.createCriteria(IrFPBatchHeaderRecord.class,"batchlist");
		criteria.createAlias("batchlist.customer", "batch");
		criteria.createAlias("batchlist.irFPFileHeaderRecord", "file");
		criteria.createAlias("batchlist.irFPFileHeaderRecord.bank", "bank");
		/*criteria.createAlias("batchlist.irFPFileHeaderRecord.fileType", "filetype");*/
		criteria.createAlias("batchlist.customer.irsCustomerGroupingDetails.irsCustomerGrouping", "customerGrp");
		/*	criteria.add(Restrictions.eq("bank.siteId",bankFilterInfo.getSiteId()));*/
	
		
		
		
		if(customerdatewiseinfo.getBankName()!=null&&!customerdatewiseinfo.getBankName().isEmpty())
			{
				criteria.add(Restrictions.eq("bank.bankName",(customerdatewiseinfo.getBankName())));

			}
			if(!customerdatewiseinfo.getBankCustomercode().isEmpty()&& customerdatewiseinfo.getBankCustomercode()!=null)
			{
				criteria.add(Restrictions.eq("batch.bankCustomerCode",(customerdatewiseinfo.getBankCustomercode())));

		    }
			if(customerdatewiseinfo.getCompanyName()!=null&&!customerdatewiseinfo.getCompanyName().isEmpty())
			{
				criteria.add(Restrictions.eq("batch.companyName",customerdatewiseinfo.getCompanyName()));
			}

			if(customerdatewiseinfo.getFromdate()!=null&&!customerdatewiseinfo.getFromdate().isEmpty())
			{
				criteria.add(Restrictions.ge("file.processedDate",(customerdatewiseinfo.getFromdate())));

			}	if(customerdatewiseinfo.getTodate()!=null&&!customerdatewiseinfo.getTodate().isEmpty())
			{
				criteria.add(Restrictions.le("file.processedDate",(customerdatewiseinfo.getTodate())));

			}
			
			List<IrFPBatchHeaderRecord>custgroup2list = (List)criteria.list();
			
			
			if(custgroup2list.size()>0){	
				int transcount=0;
				BigDecimal batchamt = BigDecimal.ZERO;
				int transactioncount=0;
				int itemcount=0;
				
			     for(int n=0; n<custgroup2list.size(); n++) {
			    	 groupingDetails=new CustomerGroupingMngmntInfo();

				groupingDetails.setCompanyName(custgroup2list.get(n).getCustomer().getCompanyName());
				groupingDetails.setBankCustomerCode(custgroup2list.get(n).getCustomer().getBankCustomerCode());
				groupingDetails.setCustId(custgroup2list.get(n).getCustomer().getCustomerId());
				double totalbatchamt=0.00;
			
				
				transactioncount+=Integer.parseInt(custgroup2list.get(n).getTransactionCount());
				itemcount+=Integer.parseInt(custgroup2list.get(n).getItemCount());
				String[] convert = (custgroup2list.get(n).getBatchAmount()).split("\\.");
				String a=convert[0];	
			     String b = convert[1];
				a = a.replaceAll(",", "");
				String total =a+"."+b; 
				String value=String.valueOf(total);
				
				 batchamt = batchamt.add(new BigDecimal(value));						
					Query fileQuery = session.createQuery("SELECT COUNT(DISTINCT fhr.fileHeaderRecordId),file.filetypename FROM IrFPFileHeaderRecord as fhr INNER JOIN fhr.irFPBatchHeaderRecord as fbr INNER JOIN fbr.irFPFileHeaderRecord.fileType as file WHERE  fhr.processedDate between :start and :end and  fbr.customer.customerId=:custId GROUP BY fbr.customer.customerId,file.filetypename");
				
					fileQuery.setParameter("start",customerdatewiseinfo.getFromdate());
					fileQuery.setParameter("end",customerdatewiseinfo.getTodate());
				
					fileQuery.setParameter("custId",custgroup2list.get(n).getCustomer().getCustomerId());
									List<?> rows = fileQuery.list();
									if(rows.size()>0){
									for(int l=0; l<rows.size(); l++) {
									
									
										Object[] rowss = (Object[]) rows.get(l);

									if(rowss[1].equals("ACH")) {
									

										groupingDetails.setAchfileCount((Long) rowss[0]);
									}
									if(rowss[1].equals("Lockbox")){
										groupingDetails.setLockboxfileCount((Long) rowss[0]);
									}
								
										}
									}	
									 
				 
			}
		
			     String dbAmount = batchamt.toString();
					String[] convert = dbAmount.split("\\.");
			        int a = Integer.parseInt(convert[0]);
			        String b = convert[1];

					NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
			     		String	batchAmount = numberFormat.format(a);
			     		
			     	String amountWithComma=null;
			       if(b.equals("00")){
				    	 amountWithComma=batchAmount+".00";
				      }else{
				    	amountWithComma=batchAmount+"."+b;  
				      }
			       
			       groupingDetails.setTotalbatchamt(String.valueOf(amountWithComma));
			       groupingDetails.setTotaltransactions(String.valueOf(transactioncount));
			       groupingDetails.setTotalitems(String.valueOf(itemcount));
			       custmrGrpngMngmntInfoList.add(groupingDetails);

			}
			     }
			
			/*bank name*/
			if(customerdatewiseinfo.getFromdate().equals("NaN-NaN-NaN")) {
			
			Criteria criteria = session.createCriteria(IrFPBatchHeaderRecord.class,"batchlist");
		criteria.createAlias("batchlist.customer", "batch");
		criteria.createAlias("batchlist.irFPFileHeaderRecord", "file");
		criteria.createAlias("batchlist.irFPFileHeaderRecord.bank", "bank");
		/*criteria.createAlias("batchlist.irFPFileHeaderRecord.fileType", "filetype");*/
		criteria.createAlias("batchlist.customer.irsCustomerGroupingDetails.irsCustomerGrouping", "customerGrp");
		/*	criteria.add(Restrictions.eq("bank.siteId",bankFilterInfo.getSiteId()));*/

		
		
		
		if(customerdatewiseinfo.getBankName()!=null&&!customerdatewiseinfo.getBankName().isEmpty())
			{
				criteria.add(Restrictions.eq("bank.bankName",(customerdatewiseinfo.getBankName())));

			}
			if(!customerdatewiseinfo.getBankCustomercode().isEmpty()&& customerdatewiseinfo.getBankCustomercode()!=null)
			{
				criteria.add(Restrictions.eq("batch.bankCustomerCode",(customerdatewiseinfo.getBankCustomercode())));

		    }
			if(customerdatewiseinfo.getCompanyName()!=null&&!customerdatewiseinfo.getCompanyName().isEmpty())
			{
				criteria.add(Restrictions.eq("batch.companyName",customerdatewiseinfo.getCompanyName()));
			}

		/*	if(GrpListInfo.getFromdate().equals("NaN-NaN-NaN"))
			{
				criteria.add(Restrictions.ge("file.processedDate",(GrpListInfo.getFromdate())));

			}	if(GrpListInfo.getTodate()!=null&&!GrpListInfo.getTodate().isEmpty())
			{
				criteria.add(Restrictions.le("file.processedDate",(GrpListInfo.getTodate())));

			}*/
			
			List<IrFPBatchHeaderRecord>custgroup2list = (List)criteria.list();
			

			if(custgroup2list.size()>0){	
			
				int transcount=0;
				BigDecimal batchamt = BigDecimal.ZERO;
				int transactioncount=0;
				int itemcount=0;
				
			     for(int n=0; n<custgroup2list.size(); n++) {
			    	 groupingDetails=new CustomerGroupingMngmntInfo();

				groupingDetails.setCompanyName(custgroup2list.get(n).getCustomer().getCompanyName());
				groupingDetails.setBankCustomerCode(custgroup2list.get(n).getCustomer().getBankCustomerCode());
				groupingDetails.setCustId(custgroup2list.get(n).getCustomer().getCustomerId());
				double totalbatchamt=0.00;
			
				
				transactioncount+=Integer.parseInt(custgroup2list.get(n).getTransactionCount());
				itemcount+=Integer.parseInt(custgroup2list.get(n).getItemCount());
				String[] convert = (custgroup2list.get(n).getBatchAmount()).split("\\.");
				String a=convert[0];	
			     String b = convert[1];
				a = a.replaceAll(",", "");
				String total =a+"."+b; 
				String value=String.valueOf(total);
				
				 batchamt = batchamt.add(new BigDecimal(value));		
				 
				 
				 if(customerdatewiseinfo.getBankName()!=null&&!customerdatewiseinfo.getBankName().isEmpty()) {
					Query fileQuery = session.createQuery("SELECT COUNT(DISTINCT fhr.fileHeaderRecordId),file.filetypename FROM IrFPFileHeaderRecord as fhr INNER JOIN fhr.irFPBatchHeaderRecord as fbr INNER JOIN fbr.irFPFileHeaderRecord.fileType as file WHERE  fhr.bank.bankName=:bankName GROUP BY fbr.customer.customerId,file.filetypename");
				
					
				
					fileQuery.setParameter("bankName",customerdatewiseinfo.getBankName());
									List<?> rows = fileQuery.list();
									if(rows.size()>0){
									for(int l=0; l<rows.size(); l++) {
									
									
										Object[] rowss = (Object[]) rows.get(l);

									if(rowss[1].equals("ACH")) {
									

										groupingDetails.setAchfileCount((Long) rowss[0]);
									}
									if(rowss[1].equals("Lockbox")){
										groupingDetails.setLockboxfileCount((Long) rowss[0]);
									}
								
										}
									}	
				 }
				 
					if(customerdatewiseinfo.getCompanyName()!=null&&!customerdatewiseinfo.getCompanyName().isEmpty()) {
						Query fileQuery = session.createQuery("SELECT COUNT(DISTINCT fhr.fileHeaderRecordId),file.filetypename FROM IrFPFileHeaderRecord as fhr INNER JOIN fhr.irFPBatchHeaderRecord as fbr INNER JOIN fbr.irFPFileHeaderRecord.fileType as file  WHERE  fbr.customer.companyName=:companyName GROUP BY fbr.customer.customerId,file.filetypename");
					
						
					
						fileQuery.setParameter("companyName",customerdatewiseinfo.getCompanyName());
										List<?> rows = fileQuery.list();
										if(rows.size()>0){
										for(int l=0; l<rows.size(); l++) {
										
										
											Object[] rowss = (Object[]) rows.get(l);

										if(rowss[1].equals("ACH")) {
										

											groupingDetails.setAchfileCount((Long) rowss[0]);
										}
										if(rowss[1].equals("Lockbox")){
											groupingDetails.setLockboxfileCount((Long) rowss[0]);
										}
									
											}
										}	
					 }
					 
					if(customerdatewiseinfo.getCompanyName()!=null&&!customerdatewiseinfo.getCompanyName().isEmpty()&&customerdatewiseinfo.getBankName()!=null&&!customerdatewiseinfo.getBankName().isEmpty()) {
						Query fileQuery = session.createQuery("SELECT COUNT(DISTINCT fhr.fileHeaderRecordId),file.filetypename FROM IrFPFileHeaderRecord as fhr INNER JOIN fhr.irFPBatchHeaderRecord as fbr INNER JOIN fbr.irFPFileHeaderRecord.fileType as file WHERE  fbr.customer.companyName=:companyName and fhr.bank.bankName=:bankName GROUP BY fbr.customer.customerId,file.filetypename");
					
						fileQuery.setParameter("bankName",customerdatewiseinfo.getBankName());
						fileQuery.setParameter("companyName",customerdatewiseinfo.getCompanyName());
										List<?> rows = fileQuery.list();
										if(rows.size()>0){
										for(int l=0; l<rows.size(); l++) {
										
										
											Object[] rowss = (Object[]) rows.get(l);

										if(rowss[1].equals("ACH")) {
										

											groupingDetails.setAchfileCount((Long) rowss[0]);
										}
										if(rowss[1].equals("Lockbox")){
											groupingDetails.setLockboxfileCount((Long) rowss[0]);
										}
									
											}
										}	
					 }	 
				 
			}
		

			     String dbAmount = batchamt.toString();
					String[] convert = dbAmount.split("\\.");
			        int a = Integer.parseInt(convert[0]);
			        String b = convert[1];

					NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
			     		String	batchAmount = numberFormat.format(a);
			     		
			     	String amountWithComma=null;
			       if(b.equals("00")){
				    	 amountWithComma=batchAmount+".00";
				      }else{
				    	amountWithComma=batchAmount+"."+b;  
				      }
			       
			       groupingDetails.setTotalbatchamt(String.valueOf(amountWithComma));
			       groupingDetails.setTotaltransactions(String.valueOf(transactioncount));
			       groupingDetails.setTotalitems(String.valueOf(itemcount));
			       custmrGrpngMngmntInfoList.add(groupingDetails);

			}
			     }
			
			
					
			
			irPlusResponseDetails.setCustmrGrpngMngmntInfoList(custmrGrpngMngmntInfoList);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
	}
	catch(Exception e)
	{
		log.error("Exception in updateBank",e);
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
		irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		throw new HibernateException(e);
	}
	return irPlusResponseDetails;
}


@Override
public IRPlusResponseDetails showVolumeTrendReport(CustomerGroupingMngmntInfo currentDate) throws BusinessException {

	IRPlusResponseDetails irPlusResponseDetails = new  IRPlusResponseDetails();

	Session session = null;
	
	session = sessionFactory.getCurrentSession();
	List<String> errMsgs = new ArrayList<String>();
	
	CustomerGroupingMngmntInfo groupingDetails=null;
	 List<CustomerGroupingMngmntInfo> custmrGrpngMngmntInfoList=new ArrayList<CustomerGroupingMngmntInfo>();

	try {
	
		   Date date=new Date();
		   DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

		   String procesdate=dateFormat.format(date);
	
		   Query newgrpquery = session.createQuery("SELECT COUNT(DISTINCT a.fileHeaderRecordId),file.filetypename,file.filetypeid from  IrFPFileHeaderRecord as a INNER JOIN a.irFPBatchHeaderRecord as b INNER JOIN a.fileType as file where a.processedDate between DATEADD(day, -7,:start) and :start GROUP BY file.filetypename,file.filetypeid");	   
		  

		   newgrpquery.setParameter("start",procesdate);
	
			List<?> rowval = newgrpquery.list();
			if(rowval.size()>0){
			for(int l=0; l<rowval.size(); l++) {
			
				groupingDetails=new CustomerGroupingMngmntInfo();
				Object[] rowvalue = (Object[]) rowval.get(l);
				groupingDetails.setAchfileCount((Long) rowvalue[0]);
				groupingDetails.setAchfiletype((String) rowvalue[1]);
				
		
				
				
				Query batch =session.createQuery(" from IrFPFileHeaderRecord as headerrecord \r\n" + 
						"									inner join headerrecord.irFPBatchHeaderRecord as controlrecord inner join headerrecord.fileType as file where file.filetypeid=:filetypeid and headerrecord.processedDate between DATEADD(day, -7,:start) and :start");

				batch.setParameter("start",procesdate);
				batch.setParameter("filetypeid",rowvalue[2]);
				

				List<?> list = batch.list();
				if(list.size()>0){	
					BigDecimal batchamt = BigDecimal.ZERO;
				
					int transcount=0;
				
				for(int i=0; i<list.size(); i++) {
					/*groupingDetails = new CustomerGroupingMngmntInfo();*/
					
					
					Object[] row = (Object[]) list.get(i);
					IrFPFileHeaderRecord irFhr = (IrFPFileHeaderRecord)row[0];
					IrFPBatchHeaderRecord irBcr = (IrFPBatchHeaderRecord)row[1];

					if(list.size()>0){	
						groupingDetails.setWeekbatches(list.size());
					}
					
					else
					{
						groupingDetails.setWeekbatches(0);
					}
					
					transcount+=Integer.parseInt(irBcr.getTransactionCount());
					String[] convert = (irBcr.getBatchAmount()).split("\\.");
					String a=convert[0];	
				     String b = convert[1];
					a = a.replaceAll(",", "");
					String total =a+"."+b; 
					String value=String.valueOf(total);
					
					 batchamt = batchamt.add(new BigDecimal(value));
				}
				
				 String dbAmount = batchamt.toString();
					String[] convert = dbAmount.split("\\.");
			        int a = Integer.parseInt(convert[0]);
			        String b = convert[1];

					NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
			     		String	batchAmount = numberFormat.format(a);
			     		
			     	String amountWithComma=null;
			       if(b.equals("00")){
				    	 amountWithComma=batchAmount+".00";
				      }else{
				    	amountWithComma=batchAmount+"."+b;  
				      }
			       groupingDetails.setWeekbatchamount(String.valueOf(amountWithComma));
		
				groupingDetails.setWeektransactions(String.valueOf(transcount));
				
				}

				custmrGrpngMngmntInfoList.add(groupingDetails);
			}	
		
			irPlusResponseDetails.setCustmrGrpngMngmntInfoList(custmrGrpngMngmntInfoList);
		}	

		
	
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
		
		

/*	else {
			irPlusResponseDetails.setValidationSuccess(false);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
		}	*/

	} 
	
	
	catch(Exception e)
	{
		log.error("Exception in updateBank",e);
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
		irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		throw new HibernateException(e);
	}
	
	return irPlusResponseDetails;
	}	


@Override
	public IRPlusResponseDetails showVolumeTrendReportWeekwise() throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new  IRPlusResponseDetails();
	
		Session session = null;
		
		session = sessionFactory.getCurrentSession();
		List<String> errMsgs = new ArrayList<String>();
		
		CustomerGroupingMngmntInfo groupingDetails=null;
		 List<CustomerGroupingMngmntInfo> custmrGrpngMngmntInfoList=new ArrayList<CustomerGroupingMngmntInfo>();

		try {
		
			   Date date=new Date();
			   DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

			   String procesdate=dateFormat.format(date);
		

			   Query newgrpquery = session.createQuery("SELECT a.processedDate  from  IrFPFileHeaderRecord as a INNER JOIN a.irFPBatchHeaderRecord as b  where a.processedDate between DATEADD(day, -7,:start) and :start GROUP BY a.processedDate");	   
			

			   newgrpquery.setParameter("start",procesdate);
			
		
				List<?> rowval = newgrpquery.list();
				if(rowval.size()>0){
				for(int l=0; l<rowval.size(); l++) {
				
					groupingDetails=new CustomerGroupingMngmntInfo();
					Object rowvalue = (Object) rowval.get(l);

					   Query newtrendquery = session.createQuery("SELECT COUNT(DISTINCT a.fileHeaderRecordId),file.filetypename,file.filetypeid from  IrFPFileHeaderRecord as a INNER JOIN a.irFPBatchHeaderRecord as b INNER JOIN a.fileType as file where a.processedDate=:start GROUP BY file.filetypename,file.filetypeid");	   
					   newtrendquery.setParameter("start",rowvalue);
				
						List<?> trendval = newtrendquery.list();
						if(trendval.size()>0){
						for(int k=0; k<trendval.size(); k++) {
					
							
							Object[] trendvalues = (Object[]) trendval.get(k);
							groupingDetails.setAchfileCount((Long) trendvalues[0]);
							groupingDetails.setAchfiletype((String) trendvalues[1]);
							groupingDetails.setHighestdate((String) rowvalue);
						
				Query batch =session.createQuery(" from IrFPFileHeaderRecord as headerrecord \r\n" + 
							"									inner join headerrecord.irFPBatchHeaderRecord as controlrecord inner join headerrecord.fileType as file where file.filetypeid=:filetypeid and headerrecord.processedDate=:start");

					batch.setParameter("start",rowvalue);
					batch.setParameter("filetypeid",trendvalues[2]);
					

					List<?> list = batch.list();
					if(list.size()>0){	
					
						BigDecimal batchamt = BigDecimal.ZERO;
						int transcount=0;
					
					for(int i=0; i<list.size(); i++) {
					
						
						
						Object[] row = (Object[]) list.get(i);
						IrFPFileHeaderRecord irFhr = (IrFPFileHeaderRecord)row[0];
						IrFPBatchHeaderRecord irBcr = (IrFPBatchHeaderRecord)row[1];

						if(list.size()>0){	
							groupingDetails.setWeekbatches(list.size());
						}
						
						else
						{
							groupingDetails.setWeekbatches(0);
						}
					
						transcount+=Integer.parseInt(irBcr.getTransactionCount());
						String[] convert = (irBcr.getBatchAmount()).split("\\.");
						String a=convert[0];	
					     String b = convert[1];
						a = a.replaceAll(",", "");
						String total =a+"."+b; 
						String value=String.valueOf(total);
						
						 batchamt = batchamt.add(new BigDecimal(value));
					}
					
					 String dbAmount = batchamt.toString();
						String[] convert = dbAmount.split("\\.");
				        int a = Integer.parseInt(convert[0]);
				        String b = convert[1];

						NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
				     		String	batchAmount = numberFormat.format(a);
				     		
				     	String amountWithComma=null;
				       if(b.equals("00")){
					    	 amountWithComma=batchAmount+".00";
					      }else{
					    	amountWithComma=batchAmount+"."+b;  
					      }
				       groupingDetails.setWeekbatchamount(String.valueOf(amountWithComma));
				
				
					groupingDetails.setWeektransactions(String.valueOf(transcount));
					custmrGrpngMngmntInfoList.add(groupingDetails);
					}
				
						}
						
						}
					
				}	
			
				irPlusResponseDetails.setCustmrGrpngMngmntInfoList(custmrGrpngMngmntInfoList);
			}	

			
		
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			
			

	/*	else {
				irPlusResponseDetails.setValidationSuccess(false);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
			}	*/

		} 
		
		
		catch(Exception e)
		{
			log.error("Exception in updateBank",e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			throw new HibernateException(e);
		}
		
		return irPlusResponseDetails;
		}




@Override
public IRPlusResponseDetails volumeTrendrangewise(CustomerGroupingMngmntInfo trendRange) throws BusinessException {
	

	
	IRPlusResponseDetails irPlusResponseDetails = new  IRPlusResponseDetails();

	Session session = null;
	
	session = sessionFactory.getCurrentSession();
	List<String> errMsgs = new ArrayList<String>();
	
	CustomerGroupingMngmntInfo groupingDetails=null;
	 List<CustomerGroupingMngmntInfo> custmrGrpngMngmntInfoList=new ArrayList<CustomerGroupingMngmntInfo>();

	try {

		  Query newgrpquery = session.createQuery("SELECT COUNT(DISTINCT a.fileHeaderRecordId),file.filetypename,file.filetypeid from  IrFPFileHeaderRecord as a INNER JOIN a.irFPBatchHeaderRecord as b INNER JOIN a.fileType as file where a.processedDate between :start and :end GROUP BY file.filetypename,file.filetypeid");	   
		
		   newgrpquery.setParameter("start",trendRange.getFromdate());
		   newgrpquery.setParameter("end",trendRange.getTodate());
	
	
			List<?> rowval = newgrpquery.list();
			if(rowval.size()>0){
			for(int l=0; l<rowval.size(); l++) {

				groupingDetails=new CustomerGroupingMngmntInfo();
				Object[] rowvalue = (Object[]) rowval.get(l);
				groupingDetails.setAchfileCount((Long) rowvalue[0]);
				groupingDetails.setAchfiletype((String) rowvalue[1]);
				
		
				
				
				Query batch =session.createQuery(" from IrFPFileHeaderRecord as headerrecord \r\n" + 
						"									inner join headerrecord.irFPBatchHeaderRecord as controlrecord inner join headerrecord.fileType as file where file.filetypeid=:filetypeid and headerrecord.processedDate between :start and :end");

				batch.setParameter("start",trendRange.getFromdate());
				batch.setParameter("end",trendRange.getTodate());
				batch.setParameter("filetypeid",rowvalue[2]);
				

				List<?> list = batch.list();
				if(list.size()>0){	
				
					BigDecimal batchamt = BigDecimal.ZERO;
					int transcount=0;
				
				for(int i=0; i<list.size(); i++) {
					/*groupingDetails = new CustomerGroupingMngmntInfo();*/
					
					
					Object[] row = (Object[]) list.get(i);
					IrFPFileHeaderRecord irFhr = (IrFPFileHeaderRecord)row[0];
					IrFPBatchHeaderRecord irBcr = (IrFPBatchHeaderRecord)row[1];

					if(list.size()>0){	
						groupingDetails.setWeekbatches(list.size());
					}
					
					else
					{
						groupingDetails.setWeekbatches(0);
					}
					
					transcount+=Integer.parseInt(irBcr.getTransactionCount());
					String[] convert = (irBcr.getBatchAmount()).split("\\.");
					String a=convert[0];	
				     String b = convert[1];
					a = a.replaceAll(",", "");
					String total =a+"."+b; 
					String value=String.valueOf(total);
					
					 batchamt = batchamt.add(new BigDecimal(value));
				}
		
			
				 String dbAmount = batchamt.toString();
					String[] convert = dbAmount.split("\\.");
			        int a = Integer.parseInt(convert[0]);
			        String b = convert[1];

					NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
			     		String	batchAmount = numberFormat.format(a);
			     		
			     	String amountWithComma=null;
			       if(b.equals("00")){
				    	 amountWithComma=batchAmount+".00";
				      }else{
				    	amountWithComma=batchAmount+"."+b;  
				      }
			       groupingDetails.setWeekbatchamount(String.valueOf(amountWithComma));
				groupingDetails.setWeektransactions(String.valueOf(transcount));
				
				}

				custmrGrpngMngmntInfoList.add(groupingDetails);
			}	
		
			irPlusResponseDetails.setCustmrGrpngMngmntInfoList(custmrGrpngMngmntInfoList);
		}	

		
	
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
		
		

/*	else {
			irPlusResponseDetails.setValidationSuccess(false);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
		}	*/

	} 
	
	
	
	catch(Exception e)
	{
		log.error("Exception in updateBank",e);
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
		irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		throw new HibernateException(e);
	}
	
	return irPlusResponseDetails;
	}	

@Override
public IRPlusResponseDetails volumeTrendBankrangewise(CustomerGroupingMngmntInfo BankRange) throws BusinessException {

	IRPlusResponseDetails irPlusResponseDetails = new  IRPlusResponseDetails();

	Session session = null;
	
	session = sessionFactory.getCurrentSession();
	List<String> errMsgs = new ArrayList<String>();
	
	CustomerGroupingMngmntInfo groupingDetails=null;
	 List<CustomerGroupingMngmntInfo> custmrGrpngMngmntInfoList=new ArrayList<CustomerGroupingMngmntInfo>();

	try {

		  Query newgrpquery = session.createQuery("SELECT COUNT(DISTINCT a.fileHeaderRecordId),file.filetypename,file.filetypeid from  IrFPFileHeaderRecord as a INNER JOIN a.irFPBatchHeaderRecord as b INNER JOIN a.fileType as file INNER JOIN a.bank as bank  where a.processedDate between :start and :end  and bank.bankId=:bankId GROUP BY file.filetypename,file.filetypeid");	   
		 
		   newgrpquery.setParameter("bankId",BankRange.getBankId());
		   newgrpquery.setParameter("start",BankRange.getFromdate());
		   newgrpquery.setParameter("end",BankRange.getTodate());
		
	
			List<?> rowval = newgrpquery.list();
			if(rowval.size()>0){
			for(int l=0; l<rowval.size(); l++) {
			
				groupingDetails=new CustomerGroupingMngmntInfo();
				Object[] rowvalue = (Object[]) rowval.get(l);
				groupingDetails.setAchfileCount((Long) rowvalue[0]);
				groupingDetails.setAchfiletype((String) rowvalue[1]);
	
				Query batch =session.createQuery(" from IrFPFileHeaderRecord as headerrecord \r\n" + 
						"									inner join headerrecord.irFPBatchHeaderRecord as controlrecord inner join headerrecord.fileType as file inner join headerrecord.bank as bank where file.filetypeid=:filetypeid and headerrecord.processedDate between :start and :end and bank.bankId=:bankId");

				batch.setParameter("start",BankRange.getFromdate());
				batch.setParameter("end",BankRange.getTodate());
				batch.setParameter("filetypeid",rowvalue[2]);
				batch.setParameter("bankId",BankRange.getBankId());

				List<?> list = batch.list();
				if(list.size()>0){	
				
					BigDecimal batchamt = BigDecimal.ZERO;
					int transcount=0;
				
				for(int i=0; i<list.size(); i++) {
					/*groupingDetails = new CustomerGroupingMngmntInfo();*/
	
					Object[] row = (Object[]) list.get(i);
					IrFPFileHeaderRecord irFhr = (IrFPFileHeaderRecord)row[0];
					IrFPBatchHeaderRecord irBcr = (IrFPBatchHeaderRecord)row[1];

					if(list.size()>0){	
						groupingDetails.setWeekbatches(list.size());
					}
					
					else
					{
						groupingDetails.setWeekbatches(0);
					}
					
					transcount+=Integer.parseInt(irBcr.getTransactionCount());

					String[] convert = (irBcr.getBatchAmount()).split("\\.");
					String a=convert[0];	
				     String b = convert[1];
					a = a.replaceAll(",", "");
					String total =a+"."+b; 
					String value=String.valueOf(total);
					
					 batchamt = batchamt.add(new BigDecimal(value));
				}
			
				
				groupingDetails.setWeektransactions(String.valueOf(transcount));
				String dbAmount = batchamt.toString();
				String[] convert = dbAmount.split("\\.");
		        int a = Integer.parseInt(convert[0]);
		        String b = convert[1];

				NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
		     		String	batchAmount = numberFormat.format(a);
		     		
		     	String amountWithComma=null;
		       if(b.equals("00")){
			    	 amountWithComma=batchAmount+".00";
			      }else{
			    	amountWithComma=batchAmount+"."+b;  
			      }
		       groupingDetails.setWeekbatchamount(String.valueOf(amountWithComma));
				}

				custmrGrpngMngmntInfoList.add(groupingDetails);
			}	
		
			irPlusResponseDetails.setCustmrGrpngMngmntInfoList(custmrGrpngMngmntInfoList);
		}	

		
	
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
		
		

/*	else {
			irPlusResponseDetails.setValidationSuccess(false);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
		}	*/

	} 
	
	
	
	catch(Exception e)
	{
		log.error("Exception in updateBank",e);
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
		irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		throw new HibernateException(e);
	}
	
	return irPlusResponseDetails;
	}	


@Override
	public IRPlusResponseDetails volumeTrendSummary(CustomerGroupingMngmntInfo trendDateRange) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new  IRPlusResponseDetails();
	
		Session session = null;
		
		session = sessionFactory.getCurrentSession();
		List<String> errMsgs = new ArrayList<String>();
		
		CustomerGroupingMngmntInfo groupingDetails=null;
		 List<CustomerGroupingMngmntInfo> custmrGrpngMngmntInfoList=new ArrayList<CustomerGroupingMngmntInfo>();

		try {
			   Date date=new Date();
			   DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

			   String procesdate=dateFormat.format(date);
		

			   Query newgrpquery = session.createQuery("SELECT a.processedDate  from  IrFPFileHeaderRecord as a INNER JOIN a.irFPBatchHeaderRecord as b  where a.processedDate between :start and :end GROUP BY a.processedDate");	   
			

			   newgrpquery.setParameter("start",trendDateRange.getFromdate());
			   newgrpquery.setParameter("end",trendDateRange.getTodate());
		
				List<?> rowval = newgrpquery.list();
				if(rowval.size()>0){
				for(int l=0; l<rowval.size(); l++) {
					groupingDetails=new CustomerGroupingMngmntInfo();
					Object rowvalue = (Object) rowval.get(l);
					
					
					   Query newtrendquery = session.createQuery("SELECT COUNT(DISTINCT a.fileHeaderRecordId),file.filetypename,file.filetypeid from  IrFPFileHeaderRecord as a INNER JOIN a.irFPBatchHeaderRecord as b INNER JOIN a.fileType as file where a.processedDate between :start and :end GROUP BY file.filetypename,file.filetypeid");	   
			

					   newtrendquery.setParameter("start",trendDateRange.getFromdate());
					   newtrendquery.setParameter("end",trendDateRange.getTodate());
				
						List<?> trendval = newtrendquery.list();
						if(trendval.size()>0){
						for(int k=0; k<trendval.size(); k++) {
					
							
							Object[] trendvalues = (Object[]) trendval.get(k);
							groupingDetails.setAchfileCount((Long) trendvalues[0]);
							groupingDetails.setAchfiletype((String) trendvalues[1]);
							groupingDetails.setHighestdate((String) rowvalue);
				Query batch =session.createQuery(" from IrFPFileHeaderRecord as headerrecord \r\n" + 
							"									inner join headerrecord.irFPBatchHeaderRecord as controlrecord inner join headerrecord.fileType as file where file.filetypeid=:filetypeid and headerrecord.processedDate between :start and :end");

				batch.setParameter("start",trendDateRange.getFromdate());
				batch.setParameter("end",trendDateRange.getTodate());
					batch.setParameter("filetypeid",trendvalues[2]);
					

					List<?> list = batch.list();
					if(list.size()>0){	
						BigDecimal batchamt = BigDecimal.ZERO;
						int transcount=0;
					
					for(int i=0; i<list.size(); i++) {
					
						
						
						Object[] row = (Object[]) list.get(i);
						IrFPFileHeaderRecord irFhr = (IrFPFileHeaderRecord)row[0];
						IrFPBatchHeaderRecord irBcr = (IrFPBatchHeaderRecord)row[1];

						if(list.size()>0){	
							groupingDetails.setWeekbatches(list.size());
						}
						
						else
						{
							groupingDetails.setWeekbatches(0);
						}
						transcount+=Integer.parseInt(irBcr.getTransactionCount());
						String[] convert = (irBcr.getBatchAmount()).split("\\.");
						String a=convert[0];	
					     String b = convert[1];
						a = a.replaceAll(",", "");
						String total =a+"."+b; 
						String value=String.valueOf(total);
						
						 batchamt = batchamt.add(new BigDecimal(value));
					}
					
				
					String dbAmount = batchamt.toString();
					String[] convert = dbAmount.split("\\.");
			        int a = Integer.parseInt(convert[0]);
			        String b = convert[1];

					NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
			     		String	batchAmount = numberFormat.format(a);
			     		
			     	String amountWithComma=null;
			       if(b.equals("00")){
				    	 amountWithComma=batchAmount+".00";
				      }else{
				    	amountWithComma=batchAmount+"."+b;  
				      }
			       groupingDetails.setWeekbatchamount(String.valueOf(amountWithComma));
					groupingDetails.setWeektransactions(String.valueOf(transcount));
					custmrGrpngMngmntInfoList.add(groupingDetails);
					}
				
						}
						
						}
					
				}	
			
				irPlusResponseDetails.setCustmrGrpngMngmntInfoList(custmrGrpngMngmntInfoList);
			}	

			
		
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			
			

	/*	else {
				irPlusResponseDetails.setValidationSuccess(false);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
			}	*/

		} 
		
		
		catch(Exception e)
		{
			log.error("Exception in updateBank",e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			throw new HibernateException(e);
		}
		
		return irPlusResponseDetails;
		}

@Override
	public IRPlusResponseDetails getbankwiseReport(String bankid) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new  IRPlusResponseDetails();
	
		Session session = null;
		
		session = sessionFactory.getCurrentSession();
		List<String> errMsgs = new ArrayList<String>();
		
		CustomerGroupingMngmntInfo groupingDetails=null;
		 List<CustomerGroupingMngmntInfo> custmrGrpngMngmntInfoList=new ArrayList<CustomerGroupingMngmntInfo>();

		try {
			
			   Date date=new Date();
			   DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

			   String procesdate=dateFormat.format(date);

			   Query newgrpquery = session.createQuery("SELECT COUNT(DISTINCT a.fileHeaderRecordId),file.filetypename,file.filetypeid from  IrFPFileHeaderRecord as a INNER JOIN a.irFPBatchHeaderRecord as b INNER JOIN a.fileType as file INNER JOIN a.bank as bank where a.processedDate between DATEADD(day, -7,:start) and :start and bank.bankId=:bankId GROUP BY file.filetypename,file.filetypeid");	   
			 
			   newgrpquery.setParameter("bankId",new Long(bankid));
			   newgrpquery.setParameter("start",procesdate);
		
		
				List<?> rowval = newgrpquery.list();
				if(rowval.size()>0){
				for(int l=0; l<rowval.size(); l++) {
				
					groupingDetails=new CustomerGroupingMngmntInfo();
					Object[] rowvalue = (Object[]) rowval.get(l);
					groupingDetails.setAchfileCount((Long) rowvalue[0]);
					groupingDetails.setAchfiletype((String) rowvalue[1]);

					Query batch =session.createQuery(" from IrFPFileHeaderRecord as headerrecord \r\n" + 
							"									inner join headerrecord.irFPBatchHeaderRecord as controlrecord inner join headerrecord.fileType as file inner join headerrecord.bank as bank where file.filetypeid=:filetypeid and headerrecord.processedDate between DATEADD(day, -7,:start) and :start and bank.bankId=:bankId");

					batch.setParameter("start",procesdate);
					batch.setParameter("filetypeid",rowvalue[2]);
					batch.setParameter("bankId",new Long(bankid));

					List<?> list = batch.list();
					if(list.size()>0){	
					
						BigDecimal batchamt = BigDecimal.ZERO;
						int transcount=0;
					
					for(int i=0; i<list.size(); i++) {
					
						Object[] row = (Object[]) list.get(i);
						IrFPFileHeaderRecord irFhr = (IrFPFileHeaderRecord)row[0];
						IrFPBatchHeaderRecord irBcr = (IrFPBatchHeaderRecord)row[1];
						IRSBank bank = (IRSBank)row[3];
						groupingDetails.setBankName(bank.getBankName());
						if(list.size()>0){	
							groupingDetails.setWeekbatches(list.size());
						}
						
						else
						{
							groupingDetails.setWeekbatches(0);
						}
						
						transcount+=Integer.parseInt(irBcr.getTransactionCount());
						String[] convert = (irBcr.getBatchAmount()).split("\\.");
						String a=convert[0];	
					     String b = convert[1];
						a = a.replaceAll(",", "");
						String total =a+"."+b; 
						String value=String.valueOf(total);
						 batchamt = batchamt.add(new BigDecimal(value));
					}
					String dbAmount = batchamt.toString();
					String[] convert = dbAmount.split("\\.");
				    int a = Integer.parseInt(convert[0]);
				    String b = convert[1];

					NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
				 		String	batchAmount = numberFormat.format(a);
				 		
				 	String amountWithComma=null;
				   if(b.equals("00")){
				    	 amountWithComma=batchAmount+".00";
				      }else{
				    	amountWithComma=batchAmount+"."+b;  
				      }

				   groupingDetails.setWeekbatchamount(String.valueOf(amountWithComma));
				
					groupingDetails.setWeektransactions(String.valueOf(transcount));
					
					}

					custmrGrpngMngmntInfoList.add(groupingDetails);
				}	
			
				irPlusResponseDetails.setCustmrGrpngMngmntInfoList(custmrGrpngMngmntInfoList);
			}	

			
		
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);

		} 
		
		
		catch(Exception e)
		{
			log.error("Exception in updateBank",e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			throw new HibernateException(e);
		}
		
		return irPlusResponseDetails;
		}	

@Override
	public IRPlusResponseDetails getbankDaywiseReport(String bankid) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new  IRPlusResponseDetails();
	
		Session session = null;
		
		session = sessionFactory.getCurrentSession();
		List<String> errMsgs = new ArrayList<String>();
		
		CustomerGroupingMngmntInfo groupingDetails=null;
		 List<CustomerGroupingMngmntInfo> custmrGrpngMngmntInfoList=new ArrayList<CustomerGroupingMngmntInfo>();

		try {
		
			   Date date=new Date();
			   DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

			   String procesdate=dateFormat.format(date);
		

			   Query newgrpquery = session.createQuery("SELECT a.processedDate  from  IrFPFileHeaderRecord as a INNER JOIN a.irFPBatchHeaderRecord as b INNER JOIN a.bank as bank where a.processedDate between DATEADD(day, -7,:start) and :start and bank.bankId=:bankId GROUP BY a.processedDate");	   
			
			   newgrpquery.setParameter("bankId",new Long(bankid));
			   newgrpquery.setParameter("start",procesdate);
			 
		
				List<?> rowval = newgrpquery.list();
				if(rowval.size()>0){
				for(int l=0; l<rowval.size(); l++) {
				
					groupingDetails=new CustomerGroupingMngmntInfo();
					Object rowvalue = (Object) rowval.get(l);

					   Query newtrendquery = session.createQuery("SELECT COUNT(DISTINCT a.fileHeaderRecordId),file.filetypename,file.filetypeid from  IrFPFileHeaderRecord as a INNER JOIN a.irFPBatchHeaderRecord as b INNER JOIN a.fileType as file INNER JOIN a.bank as bank where a.processedDate=:start and bank.bankId=:bankId GROUP BY file.filetypename,file.filetypeid");	   
			
					   newtrendquery.setParameter("bankId",new Long(bankid));
					   newtrendquery.setParameter("start",rowvalue);
					 
				
						List<?> trendval = newtrendquery.list();
						if(trendval.size()>0){
						for(int k=0; k<trendval.size(); k++) {
					
							
							Object[] trendvalues = (Object[]) trendval.get(k);
							groupingDetails.setAchfileCount((Long) trendvalues[0]);
							groupingDetails.setAchfiletype((String) trendvalues[1]);
							groupingDetails.setHighestdate((String) rowvalue);
						
				Query batch =session.createQuery(" from IrFPFileHeaderRecord as headerrecord \r\n" + 
							"									inner join headerrecord.irFPBatchHeaderRecord as controlrecord inner join headerrecord.fileType as file inner join headerrecord.bank as bank where file.filetypeid=:filetypeid and headerrecord.processedDate=:start and bank.bankId=:bankId");

					batch.setParameter("start",rowvalue);
					batch.setParameter("filetypeid",trendvalues[2]);
					batch.setParameter("bankId",new Long(bankid));

					List<?> list = batch.list();
					if(list.size()>0){	
					
						BigDecimal batchamt = BigDecimal.ZERO;
						int transcount=0;
					
					for(int i=0; i<list.size(); i++) {
					
						
						
						Object[] row = (Object[]) list.get(i);
						IrFPFileHeaderRecord irFhr = (IrFPFileHeaderRecord)row[0];
						IrFPBatchHeaderRecord irBcr = (IrFPBatchHeaderRecord)row[1];

						if(list.size()>0){	
							groupingDetails.setWeekbatches(list.size());
						}
						
						else
						{
							groupingDetails.setWeekbatches(0);
						}
					
						transcount+=Integer.parseInt(irBcr.getTransactionCount());
						String[] convert = (irBcr.getBatchAmount()).split("\\.");
						String a=convert[0];	
					     String b = convert[1];
						a = a.replaceAll(",", "");
						String total =a+"."+b; 
						String value=String.valueOf(total);
						 batchamt = batchamt.add(new BigDecimal(value));
					}
					String dbAmount = batchamt.toString();
					String[] convert = dbAmount.split("\\.");
				    int a = Integer.parseInt(convert[0]);
				    String b = convert[1];

					NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
				 		String	batchAmount = numberFormat.format(a);
				 		
				 	String amountWithComma=null;
				   if(b.equals("00")){
				    	 amountWithComma=batchAmount+".00";
				      }else{
				    	amountWithComma=batchAmount+"."+b;  
				      }

				   groupingDetails.setWeekbatchamount(String.valueOf(amountWithComma));
					
					groupingDetails.setWeektransactions(String.valueOf(transcount));
					custmrGrpngMngmntInfoList.add(groupingDetails);
					}
				
						}
						
						}
					
				}	
				irPlusResponseDetails.setCustmrGrpngMngmntInfoList(custmrGrpngMngmntInfoList);
			}	

			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);

		} 
		
		
		catch(Exception e)
		{
			log.error("Exception in updateBank",e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			throw new HibernateException(e);
		}
		
		return irPlusResponseDetails;
		}

@Override
public IRPlusResponseDetails CustomerGrpFilter(CustomerGroupingMngmntInfo GrpListInfo) throws BusinessException
{
	log.info("Inside filterFiles : "+GrpListInfo);

	IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

	Session session = null;
	CustomerGroupingMngmntInfo groupingDetails=null;
	 List<CustomerGroupingMngmntInfo> custmrGrpngMngmntInfoList=new ArrayList<CustomerGroupingMngmntInfo>();

	try
	{
	
		session = sessionFactory.getCurrentSession();

		
		/*from and to date with no customer id*/

		if(GrpListInfo.getFromdate()!=null && GrpListInfo.getBankCustomercode().isEmpty()) {
		
		Query group1 = session.createQuery("select customer.bankCustomerCode from IrFPFileHeaderRecord as a inner join a.irFPBatchHeaderRecord as b  inner join b.customer as customer  where  a.processedDate between :start and :end  group by customer.bankCustomerCode"); 
		
		group1.setParameter("start",GrpListInfo.getFromdate());
		group1.setParameter("end",GrpListInfo.getTodate());

	
		List<?>custgroup1list = group1.list();

		if(custgroup1list.size()>0){	
		     for(int m=0; m<custgroup1list.size(); m++) {
		
		    	 groupingDetails = new CustomerGroupingMngmntInfo();
			Object row = (Object) custgroup1list.get(m);
			groupingDetails.setCustId(row);

		Criteria criteria = session.createCriteria(IrFPBatchHeaderRecord.class,"batchlist");
	criteria.createAlias("batchlist.customer", "batch");
	criteria.createAlias("batchlist.irFPFileHeaderRecord", "file");
	criteria.createAlias("batchlist.irFPFileHeaderRecord.bank", "bank");

	criteria.createAlias("batchlist.customer.irsCustomerGroupingDetails.irsCustomerGrouping", "customerGrp");

	
	if(GrpListInfo.getBankName()!=null&&!GrpListInfo.getBankName().isEmpty())
		{
			criteria.add(Restrictions.eq("bank.bankName",(GrpListInfo.getBankName())));

		}
		/*if(GrpListInfo.getCustomerid()!=null)
		{
			criteria.add(Restrictions.eq("batch.customerId",new Integer(GrpListInfo.getCustomerid())));

		}*/
		if(row!=null)
		{
			criteria.add(Restrictions.eq("batch.bankCustomerCode",row));

		}
		if(GrpListInfo.getCompanyName()!=null&&!GrpListInfo.getCompanyName().isEmpty())
		{
			criteria.add(Restrictions.eq("batch.companyName",GrpListInfo.getCompanyName()));
		}

		if(GrpListInfo.getFromdate()!=null&&!GrpListInfo.getFromdate().isEmpty())
		{
			criteria.add(Restrictions.ge("file.processedDate",(GrpListInfo.getFromdate())));

		}	if(GrpListInfo.getTodate()!=null&&!GrpListInfo.getTodate().isEmpty())
		{
			criteria.add(Restrictions.le("file.processedDate",(GrpListInfo.getTodate())));

		}
		
		List<IrFPBatchHeaderRecord>custgroup2list = (List)criteria.list();
		
		
		if(custgroup2list.size()>0){	
			int transcount=0;
			BigDecimal batchamt = BigDecimal.ZERO;
			int transactioncount=0;
			int itemcount=0;
			
		     for(int n=0; n<custgroup2list.size(); n++) {
		    	 groupingDetails=new CustomerGroupingMngmntInfo();

			groupingDetails.setCompanyName(custgroup2list.get(n).getCustomer().getCompanyName());
			groupingDetails.setBankCustomerCode(custgroup2list.get(n).getCustomer().getBankCustomerCode());
			groupingDetails.setCustId(custgroup2list.get(n).getCustomer().getCustomerId());
			double totalbatchamt=0.00;
		
			
			transactioncount+=Integer.parseInt(custgroup2list.get(n).getTransactionCount());
			itemcount+=Integer.parseInt(custgroup2list.get(n).getItemCount());
			String[] convert = (custgroup2list.get(n).getBatchAmount()).split("\\.");
			String a=convert[0];	
		     String b = convert[1];
			a = a.replaceAll(",", "");
			String total =a+"."+b; 
			String value=String.valueOf(total);
			
			 batchamt = batchamt.add(new BigDecimal(value));			
			 
			 
			 if(GrpListInfo.getBankName()!=null&&!GrpListInfo.getBankName().isEmpty()) {
					Query fileQuery = session.createQuery("SELECT COUNT(DISTINCT fhr.fileHeaderRecordId),file.filetypename FROM IrFPFileHeaderRecord as fhr INNER JOIN fhr.irFPBatchHeaderRecord as fbr INNER JOIN fbr.irFPFileHeaderRecord.fileType as file WHERE  fhr.bank.bankName=:bankName GROUP BY fbr.customer.customerId,file.filetypename");
				
				//	fileQuery.setParameter("bankName",custgroup2list.get(n).getCustomer().getCustomerId());
				
					fileQuery.setParameter("bankName",GrpListInfo.getBankName());
									List<?> rows = fileQuery.list();
									if(rows.size()>0){
									for(int l=0; l<rows.size(); l++) {
									
									
										Object[] rowss = (Object[]) rows.get(l);

									if(rowss[1].equals("ACH")) {
									

										groupingDetails.setAchfileCount((Long) rowss[0]);
									}
									if(rowss[1].equals("Lockbox")){
										groupingDetails.setLockboxfileCount((Long) rowss[0]);
									}
								
										}
									}	
				 }
			 
			 
			 if(GrpListInfo.getCompanyName()!=null&&!GrpListInfo.getCompanyName().isEmpty()) {
					Query fileQuery = session.createQuery("SELECT COUNT(DISTINCT fhr.fileHeaderRecordId),file.filetypename FROM IrFPFileHeaderRecord as fhr INNER JOIN fhr.irFPBatchHeaderRecord as fbr INNER JOIN fbr.irFPFileHeaderRecord.fileType as file WHERE  fhr.bank.bankName=:bankName GROUP BY fbr.customer.customerId,file.filetypename");
				
					//fileQuery.setParameter("bankName",custgroup2list.get(n).getCustomer().getCustomerId());
				
					fileQuery.setParameter("bankName",GrpListInfo.getBankName());
									List<?> rows = fileQuery.list();
									if(rows.size()>0){
									for(int l=0; l<rows.size(); l++) {
									
									
										Object[] rowss = (Object[]) rows.get(l);

									if(rowss[1].equals("ACH")) {
									

										groupingDetails.setAchfileCount((Long) rowss[0]);
									}
									if(rowss[1].equals("Lockbox")){
										groupingDetails.setLockboxfileCount((Long) rowss[0]);
									}
								
										}
									}	
				 }
			 
			 if(!GrpListInfo.getFromdate().equals("NaN-NaN-NaN")) {
				 
				Query fileQuery = session.createQuery("SELECT COUNT(DISTINCT fhr.fileHeaderRecordId),file.filetypename FROM IrFPFileHeaderRecord as fhr INNER JOIN fhr.irFPBatchHeaderRecord as fbr INNER JOIN fbr.irFPFileHeaderRecord.fileType as file WHERE  fhr.processedDate between :start and :end and  fbr.customer.customerId=:custId GROUP BY fbr.customer.customerId,file.filetypename");
			
				fileQuery.setParameter("start",GrpListInfo.getFromdate());
				fileQuery.setParameter("end",GrpListInfo.getTodate());
			
				fileQuery.setParameter("custId",custgroup2list.get(n).getCustomer().getCustomerId());
								List<?> rows = fileQuery.list();
								if(rows.size()>0){
								for(int l=0; l<rows.size(); l++) {
								
								
									Object[] rowss = (Object[]) rows.get(l);

								if(rowss[1].equals("ACH")) {
								

									groupingDetails.setAchfileCount((Long) rowss[0]);
								}
								if(rowss[1].equals("Lockbox")){
									groupingDetails.setLockboxfileCount((Long) rowss[0]);
								}
							
									}
								}	
								 
			 
		}
		     }
		     String dbAmount = batchamt.toString();
				String[] convert = dbAmount.split("\\.");
		        int a = Integer.parseInt(convert[0]);
		        String b = convert[1];

				NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
		     		String	batchAmount = numberFormat.format(a);
		     		
		     	String amountWithComma=null;
		       if(b.equals("00")){
			    	 amountWithComma=batchAmount+".00";
			      }else{
			    	amountWithComma=batchAmount+"."+b;  
			      }
		       
		       groupingDetails.setTotalbatchamt(String.valueOf(amountWithComma));
		       groupingDetails.setTotaltransactions(String.valueOf(transactioncount));
		       groupingDetails.setTotalitems(String.valueOf(itemcount));
		       custmrGrpngMngmntInfoList.add(groupingDetails);

		}
		     }
				}
		
		
		
		}
		
		
		
		
		
		/*from and to date with customer id*/
		
		if(GrpListInfo.getFromdate()!=null && !GrpListInfo.getBankCustomercode().isEmpty()) {
				
		Criteria criteria = session.createCriteria(IrFPBatchHeaderRecord.class,"batchlist");
	criteria.createAlias("batchlist.customer", "batch");
	criteria.createAlias("batchlist.irFPFileHeaderRecord", "file");
	criteria.createAlias("batchlist.irFPFileHeaderRecord.bank", "bank");
	/*criteria.createAlias("batchlist.irFPFileHeaderRecord.fileType", "filetype");*/
	criteria.createAlias("batchlist.customer.irsCustomerGroupingDetails.irsCustomerGrouping", "customerGrp");
	/*	criteria.add(Restrictions.eq("bank.siteId",bankFilterInfo.getSiteId()));*/
	
	
	
	
	if(GrpListInfo.getBankName()!=null&&!GrpListInfo.getBankName().isEmpty())
		{
			criteria.add(Restrictions.eq("bank.bankName",(GrpListInfo.getBankName())));

		}
		if(!GrpListInfo.getBankCustomercode().isEmpty()&& GrpListInfo.getBankName()!=null)
		{
			criteria.add(Restrictions.eq("batch.bankCustomerCode",(GrpListInfo.getBankCustomercode())));

	}
		if(GrpListInfo.getCompanyName()!=null&&!GrpListInfo.getCompanyName().isEmpty())
		{
			criteria.add(Restrictions.eq("batch.companyName",GrpListInfo.getCompanyName()));
		}

		if(GrpListInfo.getFromdate()!=null&&!GrpListInfo.getFromdate().isEmpty())
		{
			criteria.add(Restrictions.ge("file.processedDate",(GrpListInfo.getFromdate())));

		}	if(GrpListInfo.getTodate()!=null&&!GrpListInfo.getTodate().isEmpty())
		{
			criteria.add(Restrictions.le("file.processedDate",(GrpListInfo.getTodate())));

		}
		
		List<IrFPBatchHeaderRecord>custgroup2list = (List)criteria.list();
		
		
		if(custgroup2list.size()>0){	
			int transcount=0;
			BigDecimal batchamt = BigDecimal.ZERO;
			int transactioncount=0;
			int itemcount=0;
			
		     for(int n=0; n<custgroup2list.size(); n++) {
		    	 groupingDetails=new CustomerGroupingMngmntInfo();

			groupingDetails.setCompanyName(custgroup2list.get(n).getCustomer().getCompanyName());
			groupingDetails.setBankCustomerCode(custgroup2list.get(n).getCustomer().getBankCustomerCode());
			groupingDetails.setCustId(custgroup2list.get(n).getCustomer().getCustomerId());
			double totalbatchamt=0.00;
		
			
			transactioncount+=Integer.parseInt(custgroup2list.get(n).getTransactionCount());
			itemcount+=Integer.parseInt(custgroup2list.get(n).getItemCount());
			String[] convert = (custgroup2list.get(n).getBatchAmount()).split("\\.");
			String a=convert[0];	
		     String b = convert[1];
			a = a.replaceAll(",", "");
			String total =a+"."+b; 
			String value=String.valueOf(total);
			
			 batchamt = batchamt.add(new BigDecimal(value));						
				Query fileQuery = session.createQuery("SELECT COUNT(DISTINCT fhr.fileHeaderRecordId),file.filetypename FROM IrFPFileHeaderRecord as fhr INNER JOIN fhr.irFPBatchHeaderRecord as fbr INNER JOIN fbr.irFPFileHeaderRecord.fileType as file WHERE  fhr.processedDate between :start and :end and  fbr.customer.customerId=:custId GROUP BY fbr.customer.customerId,file.filetypename");
			
				fileQuery.setParameter("start",GrpListInfo.getFromdate());
				fileQuery.setParameter("end",GrpListInfo.getTodate());
			
				fileQuery.setParameter("custId",custgroup2list.get(n).getCustomer().getCustomerId());
								List<?> rows = fileQuery.list();
								if(rows.size()>0){
								for(int l=0; l<rows.size(); l++) {
								
								
									Object[] rowss = (Object[]) rows.get(l);

								if(rowss[1].equals("ACH")) {
								

									groupingDetails.setAchfileCount((Long) rowss[0]);
								}
								if(rowss[1].equals("Lockbox")){
									groupingDetails.setLockboxfileCount((Long) rowss[0]);
								}
							
									}
								}	
								 
			 
		}
	
		     String dbAmount = batchamt.toString();
				String[] convert = dbAmount.split("\\.");
		        int a = Integer.parseInt(convert[0]);
		        String b = convert[1];

				NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
		     		String	batchAmount = numberFormat.format(a);
		     		
		     	String amountWithComma=null;
		       if(b.equals("00")){
			    	 amountWithComma=batchAmount+".00";
			      }else{
			    	amountWithComma=batchAmount+"."+b;  
			      }
		       
		       groupingDetails.setTotalbatchamt(String.valueOf(amountWithComma));
		       groupingDetails.setTotaltransactions(String.valueOf(transactioncount));
		       groupingDetails.setTotalitems(String.valueOf(itemcount));
		       custmrGrpngMngmntInfoList.add(groupingDetails);

		}
		     }
		
		/*bank name*/
		if(GrpListInfo.getFromdate().equals("NaN-NaN-NaN")) {
			
		Criteria criteria = session.createCriteria(IrFPBatchHeaderRecord.class,"batchlist");
	criteria.createAlias("batchlist.customer", "batch");
	criteria.createAlias("batchlist.irFPFileHeaderRecord", "file");
	criteria.createAlias("batchlist.irFPFileHeaderRecord.bank", "bank");
	/*criteria.createAlias("batchlist.irFPFileHeaderRecord.fileType", "filetype");*/
	criteria.createAlias("batchlist.customer.irsCustomerGroupingDetails.irsCustomerGrouping", "customerGrp");
	/*	criteria.add(Restrictions.eq("bank.siteId",bankFilterInfo.getSiteId()));*/
	
	

/*	log.info("getttFiletypeId"+GrpListInfo.getCustomerGroupName());*/
	
	
	
	if(GrpListInfo.getBankName()!=null&&!GrpListInfo.getBankName().isEmpty())
		{
			criteria.add(Restrictions.eq("bank.bankName",(GrpListInfo.getBankName())));

		}
		if(!GrpListInfo.getBankCustomercode().isEmpty()&& GrpListInfo.getBankCustomercode()!=null)
		{
			criteria.add(Restrictions.eq("batch.bankCustomerCode",(GrpListInfo.getBankCustomercode())));

	    }
		if(GrpListInfo.getCompanyName()!=null&&!GrpListInfo.getCompanyName().isEmpty())
		{
			criteria.add(Restrictions.eq("batch.companyName",GrpListInfo.getCompanyName()));
		}

	/*	if(GrpListInfo.getFromdate().equals("NaN-NaN-NaN"))
		{
			criteria.add(Restrictions.ge("file.processedDate",(GrpListInfo.getFromdate())));

		}	if(GrpListInfo.getTodate()!=null&&!GrpListInfo.getTodate().isEmpty())
		{
			criteria.add(Restrictions.le("file.processedDate",(GrpListInfo.getTodate())));

		}*/
		
		List<IrFPBatchHeaderRecord>custgroup2list = (List)criteria.list();
		
	
		if(custgroup2list.size()>0){	
		
			int transcount=0;
			BigDecimal batchamt = BigDecimal.ZERO;
			int transactioncount=0;
			int itemcount=0;
			
		     for(int n=0; n<custgroup2list.size(); n++) {
		    	 groupingDetails=new CustomerGroupingMngmntInfo();

			groupingDetails.setCompanyName(custgroup2list.get(n).getCustomer().getCompanyName());
			groupingDetails.setBankCustomerCode(custgroup2list.get(n).getCustomer().getBankCustomerCode());
			groupingDetails.setCustId(custgroup2list.get(n).getCustomer().getCustomerId());
			double totalbatchamt=0.00;
		
			
			transactioncount+=Integer.parseInt(custgroup2list.get(n).getTransactionCount());
			itemcount+=Integer.parseInt(custgroup2list.get(n).getItemCount());
			String[] convert = (custgroup2list.get(n).getBatchAmount()).split("\\.");
			String a=convert[0];	
		     String b = convert[1];
			a = a.replaceAll(",", "");
			String total =a+"."+b; 
			String value=String.valueOf(total);
			
			 batchamt = batchamt.add(new BigDecimal(value));		
			 
			 
			 if(GrpListInfo.getBankName()!=null&&!GrpListInfo.getBankName().isEmpty()) {
				Query fileQuery = session.createQuery("SELECT COUNT(DISTINCT fhr.fileHeaderRecordId),file.filetypename FROM IrFPFileHeaderRecord as fhr INNER JOIN fhr.irFPBatchHeaderRecord as fbr INNER JOIN fbr.irFPFileHeaderRecord.fileType as file WHERE  fhr.bank.bankName=:bankName GROUP BY fbr.customer.customerId,file.filetypename");
			
				
			
				fileQuery.setParameter("bankName",GrpListInfo.getBankName());
								List<?> rows = fileQuery.list();
								if(rows.size()>0){
								for(int l=0; l<rows.size(); l++) {
								
								
									Object[] rowss = (Object[]) rows.get(l);

								if(rowss[1].equals("ACH")) {
								

									groupingDetails.setAchfileCount((Long) rowss[0]);
								}
								if(rowss[1].equals("Lockbox")){
									groupingDetails.setLockboxfileCount((Long) rowss[0]);
								}
							
									}
								}	
			 }
			 
				if(GrpListInfo.getCompanyName()!=null&&!GrpListInfo.getCompanyName().isEmpty()) {
					Query fileQuery = session.createQuery("SELECT COUNT(DISTINCT fhr.fileHeaderRecordId),file.filetypename FROM IrFPFileHeaderRecord as fhr INNER JOIN fhr.irFPBatchHeaderRecord as fbr INNER JOIN fbr.irFPFileHeaderRecord.fileType as file  WHERE  fbr.customer.companyName=:companyName GROUP BY fbr.customer.customerId,file.filetypename");
				
					
				
					fileQuery.setParameter("companyName",GrpListInfo.getCompanyName());
									List<?> rows = fileQuery.list();
									if(rows.size()>0){
									for(int l=0; l<rows.size(); l++) {
									
									
										Object[] rowss = (Object[]) rows.get(l);

									if(rowss[1].equals("ACH")) {
									

										groupingDetails.setAchfileCount((Long) rowss[0]);
									}
									if(rowss[1].equals("Lockbox")){
										groupingDetails.setLockboxfileCount((Long) rowss[0]);
									}
								
										}
									}	
				 }
				 
				if(GrpListInfo.getCompanyName()!=null&&!GrpListInfo.getCompanyName().isEmpty()&&GrpListInfo.getBankName()!=null&&!GrpListInfo.getBankName().isEmpty()) {
					Query fileQuery = session.createQuery("SELECT COUNT(DISTINCT fhr.fileHeaderRecordId),file.filetypename FROM IrFPFileHeaderRecord as fhr INNER JOIN fhr.irFPBatchHeaderRecord as fbr INNER JOIN fbr.irFPFileHeaderRecord.fileType as file WHERE  fbr.customer.companyName=:companyName and fhr.bank.bankName=:bankName GROUP BY fbr.customer.customerId,file.filetypename");
				
					fileQuery.setParameter("bankName",GrpListInfo.getBankName());
					fileQuery.setParameter("companyName",GrpListInfo.getCompanyName());
									List<?> rows = fileQuery.list();
									if(rows.size()>0){
									for(int l=0; l<rows.size(); l++) {
									
									
										Object[] rowss = (Object[]) rows.get(l);

									if(rowss[1].equals("ACH")) {
									

										groupingDetails.setAchfileCount((Long) rowss[0]);
									}
									if(rowss[1].equals("Lockbox")){
										groupingDetails.setLockboxfileCount((Long) rowss[0]);
									}
								
										}
									}	
				 }	 
			 
		}
	
		     
		     
		     String dbAmount = batchamt.toString();
				String[] convert = dbAmount.split("\\.");
		        int a = Integer.parseInt(convert[0]);
		        String b = convert[1];

				NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
		     		String	batchAmount = numberFormat.format(a);
		     		
		     	String amountWithComma=null;
		       if(b.equals("00")){
			    	 amountWithComma=batchAmount+".00";
			      }else{
			    	amountWithComma=batchAmount+"."+b;  
			      }
		       
		       groupingDetails.setTotalbatchamt(String.valueOf(amountWithComma));
		       groupingDetails.setTotaltransactions(String.valueOf(transactioncount));
		       groupingDetails.setTotalitems(String.valueOf(itemcount));
		       custmrGrpngMngmntInfoList.add(groupingDetails);

		}
		     }
		
		
				
		
		irPlusResponseDetails.setCustmrGrpngMngmntInfoList(custmrGrpngMngmntInfoList);
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
		
	}
	catch(Exception e)
	{
		log.error("Exception in listBank",e);
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
		irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);

		throw new HibernateException(e);
	}

	return irPlusResponseDetails;
}



@Override
public IRPlusResponseDetails showDaywiseReport(CustomerGroupingMngmntInfo currentDate) throws BusinessException
{


	IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

	Session session = null;

	 List<CustomerGroupingMngmntInfo> custmrGrpngMngmntInfoList=new ArrayList<CustomerGroupingMngmntInfo>();
	 List<CustomerGroupingMngmntInfo> FiletypeSummary=new ArrayList<CustomerGroupingMngmntInfo>();
	CustomerGroupingMngmntInfo bankInfo = null;
	CustomerGroupingMngmntInfo groupInfo = new CustomerGroupingMngmntInfo();
	IrsCustomerGrouping custGrpInfo = null;
	IrsCustomerGroupingDetails custGrpInfoDetails = null;
	try
	{

		   String[] monthnames = {"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};

	/*	   	 String betweendate="";
			 String fromDate1="";
			 String ToDate1="";
		     DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
		     betweendate=currentDate.getCurrentdate();
		     String[] dateParts = betweendate.split("-");

		    fromDate1 = dateParts[0]; 
		    ToDate1 = dateParts[1]; */

		   
			Date date = new SimpleDateFormat("MM/dd/yyyy").parse(currentDate.getFromdate());
			String fromDate = new SimpleDateFormat("MM-dd-yyyy").format(date);
		
			
			Date todate = new SimpleDateFormat("MM/dd/yyyy").parse(currentDate.getTodate());
			String ToDate = new SimpleDateFormat("MM-dd-yyyy").format(todate);


		session = sessionFactory.getCurrentSession();
		
		Query daywisefiles = session.createQuery("FROM IrMUsersBank as user inner join user.bank as branch inner join branch.irFPFileHeaderRecord as fhr WHERE fhr.processedDate between :start and :end and user.irMUsers.userid=:userId and user.isactive=:Isactive");			

		daywisefiles.setParameter("start",fromDate);
		daywisefiles.setParameter("end",ToDate);
		daywisefiles.setParameter("userId",currentDate.getUserid());
		daywisefiles.setParameter("Isactive",1);
		if(daywisefiles.list().size()>0){
			
			groupInfo.setTotalFiles(daywisefiles.list().size());
		}
		  
		Query newcustomerquery = session.createQuery("SELECT COUNT(DISTINCT fhr.fileHeaderRecordId),file.filetypename from IrMUsersBank as user inner join user.bank as branch inner join branch.irFPFileHeaderRecord as fhr inner join  fhr.fileType as file  WHERE fhr.processedDate between :start and :end and user.irMUsers.userid=:userId and user.isactive=:Isactive GROUP BY file.filetypename");	   

		newcustomerquery.setParameter("start",fromDate);
		newcustomerquery.setParameter("end",ToDate);
		newcustomerquery.setParameter("userId",currentDate.getUserid());
		newcustomerquery.setParameter("Isactive",1);
		   
		   
		 
			List<?> customerquery = newcustomerquery.list();
			if(customerquery.size()>0){
			for(int k=0; k<customerquery.size(); k++) {
			
				groupInfo.setTotalCustomers(customerquery.size());
			
				Object[] row = (Object[]) customerquery.get(k);

			if(row[1].equals("ACH")) {
				groupInfo.setAchfileCount((Long) row[0]);
			}
			if(row[1].equals("Lockbox")){
				groupInfo.setLockboxfileCount((Long) row[0]);
			}
		
				}
		
			}
		
			Query daywisebanks = session.createQuery("SELECT fhr.bank.bankId FROM IrMUsersBank as user inner join user.bank as branch inner join branch.irFPFileHeaderRecord as fhr WHERE fhr.processedDate between :start and :end and user.irMUsers.userid=:userId and user.isactive=:Isactive group by fhr.bank.bankId");			
		
			daywisebanks.setParameter("start",fromDate);
			daywisebanks.setParameter("end",ToDate);
			daywisebanks.setParameter("userId",currentDate.getUserid());
			daywisebanks.setParameter("Isactive",1);
		
			List<?> daywisebankslist = daywisebanks.list();
			groupInfo.setTotalBanks(daywisebankslist.size());
			if(daywisebankslist.size()>0){
				for(int i=0; i<daywisebankslist.size(); i++) {
				
					bankInfo=new CustomerGroupingMngmntInfo();
				Object row = (Object) daywisebankslist.get(i);
				Query daywise= session.createQuery("FROM IrMUsersBank as user inner join user.bank as branch inner join branch.irFPFileHeaderRecord as fhr inner join fhr.irFPBatchHeaderRecord as fbr WHERE fhr.processedDate between :start and :end and user.irMUsers.userid=:userId and user.isactive=:Isactive and fhr.bank.bankId=:branchId");			
				daywise.setParameter("start",fromDate);
				daywise.setParameter("end",ToDate);
				daywise.setParameter("userId",currentDate.getUserid());
				daywise.setParameter("Isactive",1);
				daywise.setParameter("branchId",row);
				
				List<?> custgrpList =daywise.list();		
				
				double totalbatchamt=0.00;
				if(custgrpList.size()>0){
					
					BigDecimal batchamt = BigDecimal.ZERO;
					int transactioncount=0;
					int itemcount=0;
					
					for(int j=0; j<custgrpList.size(); j++) {
						
						
						
						Object[] rows = (Object[]) custgrpList.get(j);
						IrFPFileHeaderRecord irFhr = (IrFPFileHeaderRecord)rows[2];
						IrFPBatchHeaderRecord irBcr = (IrFPBatchHeaderRecord)rows[3];
						transactioncount+=Integer.parseInt(irBcr.getTransactionCount());
					
						
						bankInfo.setHighdate(currentDate.getCurrentdate());
						
						bankInfo.setBankwiseId(irFhr.getBank().getBankId());
						bankInfo.setBankName(irFhr.getBank().getBankName());
						itemcount+=Integer.parseInt(irBcr.getItemCount());
						String[] convert = (irBcr.getBatchAmount()).split("\\.");
						String a=convert[0];	
					     String b = convert[1];
						a = a.replaceAll(",", "");
						String total =a+"."+b; 
						String value=String.valueOf(total);
						
						 batchamt = batchamt.add(new BigDecimal(value));
						
					}

					String dbAmount = batchamt.toString();
					String[] convert = dbAmount.split("\\.");
			        int a = Integer.parseInt(convert[0]);
			        String b = convert[1];

					NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
			     		String	batchAmount = numberFormat.format(a);
			     		
			     	String amountWithComma=null;
			       if(b.equals("00")){
				    	 amountWithComma=batchAmount+".00";
				      }else{
				    	amountWithComma=batchAmount+"."+b;  
				      }

				bankInfo.setBankwiseAmount(String.valueOf(amountWithComma));
			       bankInfo.setTotalitems(String.valueOf(itemcount));
			       bankInfo.setTotaltransactions(String.valueOf(transactioncount));
				}
				

				   Query Bankwise = session.createQuery("SELECT COUNT(DISTINCT fhr.fileHeaderRecordId),file.filetypename from IrMUsersBank as user inner join user.bank as branch inner join branch.irFPFileHeaderRecord as fhr inner join  fhr.fileType as file  WHERE fhr.processedDate between :start and :end and user.irMUsers.userid=:userId and user.isactive=:Isactive and fhr.bank.bankId=:branchId  GROUP BY file.filetypename");	   

				   Bankwise.setParameter("start",fromDate);
				   Bankwise.setParameter("end",ToDate);
				   Bankwise.setParameter("userId",currentDate.getUserid());
				   Bankwise.setParameter("Isactive",1);
				   Bankwise.setParameter("branchId",row);
					
					List<?> Bankwisefilelist = Bankwise.list();
					if(Bankwisefilelist.size()>0){
				
					for(int k=0; k<Bankwisefilelist.size(); k++) {

						Object[] rowss = (Object[]) Bankwisefilelist.get(k);

					if(rowss[1].equals("ACH")) {
						bankInfo.setBankAchfileCount((Long) rowss[0]);
					}
					if(rowss[1].equals("Lockbox")){
						bankInfo.setBankLockboxfileCount((Long) rowss[0]);
					}
				
						}
					}
				
					  custmrGrpngMngmntInfoList.add(bankInfo);
				
				}
				//groupInfo.setTotalBanks(daywisebankslist.size());
				irPlusResponseDetails.setCustmrGrpngMngmntInfoList(custmrGrpngMngmntInfoList);
			}
			
			else
			{
				
				bankInfo=null;
				  custmrGrpngMngmntInfoList.add(bankInfo);
				  irPlusResponseDetails.setCustmrGrpngMngmntInfoList(custmrGrpngMngmntInfoList);
			}
			
			Query daywiseCustomers= session.createQuery("select fbr.customer.customerId from IrMUsersBank as user inner join user.bank as branch inner join branch.irFPFileHeaderRecord as fhr inner join fhr.irFPBatchHeaderRecord as fbr WHERE fhr.processedDate between :start and :end and user.irMUsers.userid=:userId and user.isactive=:Isactive group by fbr.customer.customerId");		
		
			daywiseCustomers.setParameter("start",fromDate);
			daywiseCustomers.setParameter("end",ToDate);
			daywiseCustomers.setParameter("userId",currentDate.getUserid());
			daywiseCustomers.setParameter("Isactive",1);
				
	if(daywiseCustomers.list().size()>0){
		
		groupInfo.setTotalCustomers(daywiseCustomers.list().size());
	
	}
	else
	{
		groupInfo.setTotalCustomers(0);
	}
	
	
	
Query daywiseAmount= session.createQuery("from IrMUsersBank as user inner join user.bank as branch inner join branch.irFPFileHeaderRecord as fhr inner join fhr.irFPBatchHeaderRecord as fbr WHERE fhr.processedDate between :start and :end and user.irMUsers.userid=:userId and user.isactive=:Isactive");			
daywiseAmount.setParameter("start",fromDate);
daywiseAmount.setParameter("end",ToDate);
daywiseAmount.setParameter("userId",currentDate.getUserid());
daywiseAmount.setParameter("Isactive",1);

List<?> daywiseAmountlist =daywiseAmount.list();		

double totalbatchamt=0.00;
if(daywiseAmountlist.size()>0){

BigDecimal batchamt = BigDecimal.ZERO;
int transactioncount=0;
int itemcount=0;

for(int j=0;j<daywiseAmountlist.size();j++){

	Object[] rows = (Object[]) daywiseAmountlist.get(j);
	IrFPFileHeaderRecord irFhr = (IrFPFileHeaderRecord)rows[2];
	IrFPBatchHeaderRecord irBcr = (IrFPBatchHeaderRecord)rows[3];

	String[] convert = (irBcr.getBatchAmount()).split("\\.");
	String a=convert[0];	
     String b = convert[1];
	a = a.replaceAll(",", "");
	String total =a+"."+b; 
	String value=String.valueOf(total);
	
	 batchamt = batchamt.add(new BigDecimal(value));
	
}

String dbAmount = batchamt.toString();
String[] convert = dbAmount.split("\\.");
int a = Integer.parseInt(convert[0]);
String b = convert[1];

NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
		String	batchAmount = numberFormat.format(a);
		
	String amountWithComma=null;
if(b.equals("00")){
	 amountWithComma=batchAmount+".00";
  }else{
	amountWithComma=batchAmount+"."+b;  
  }
groupInfo.setTotalbatchamt(String.valueOf(amountWithComma));

}
else
{

   groupInfo.setTotalbatchamt("0.00");

}
	


Query licenseid = session.createQuery(" SELECT site.license.licenseId FROM IRSSite as site WHERE site.siteId=?");			
licenseid.setParameter(0,currentDate.getSiteId());
List<?> licenseidlist = licenseid.list();

if(licenseidlist.size()>0){
	for(int i=0; i<licenseidlist.size(); i++) {
		bankInfo=new CustomerGroupingMngmntInfo();
	Object row = (Object) licenseidlist.get(i);
	
		
Query fileType = session.createQuery(" SELECT file.filetypeid,file.filetypename FROM IrMFileTypes as file WHERE file.iRSLicense.licenseId=?");			
fileType.setParameter(0,row);
List<?> filTypelist = fileType.list();

if(filTypelist.size()>0){
for(int j=0; j<filTypelist.size(); j++) {
	bankInfo=new CustomerGroupingMngmntInfo();
	Object[] rows = (Object[]) filTypelist.get(j);
	

	
Query processDetails = session.createQuery("from IrMUsersBank as user inner join user.bank as branch inner join branch.irFPFileHeaderRecord as fhr inner join fhr.irFPBatchHeaderRecord as fbr WHERE fhr.processedDate between :start and :end and user.irMUsers.userid=:userId and user.isactive=:Isactive and fhr.fileType.filetypeid=:Filetype");			
processDetails.setParameter("start",fromDate);
processDetails.setParameter("end",ToDate);
processDetails.setParameter("userId",currentDate.getUserid());
processDetails.setParameter("Isactive",1);
processDetails.setParameter("Filetype",rows[0]);

List<?> processDetailslist =processDetails.list();		


if(processDetailslist.size()>0){

BigDecimal batchamt = BigDecimal.ZERO;
int transactioncount=0;
int itemcount=0;

for(int n=0; n<processDetailslist.size(); n++) {

	bankInfo=new CustomerGroupingMngmntInfo();

Object[] rowval = (Object[]) processDetailslist.get(n);
IrFPFileHeaderRecord irFhr = (IrFPFileHeaderRecord)rowval[2];
IrFPBatchHeaderRecord irBcr = (IrFPBatchHeaderRecord)rowval[3];
transactioncount+=Integer.parseInt(irBcr.getTransactionCount());

itemcount+=Integer.parseInt(irBcr.getItemCount());
String[] convert = (irBcr.getBatchAmount()).split("\\.");
String a=convert[0];	
 String b = convert[1];
a = a.replaceAll(",", "");
String total =a+"."+b; 
String value=String.valueOf(total);

 batchamt = batchamt.add(new BigDecimal(value));

}

String dbAmount = batchamt.toString();
String[] convert = dbAmount.split("\\.");
int a = Integer.parseInt(convert[0]);
String b = convert[1];

NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
	String	batchAmount = numberFormat.format(a);
	
String amountWithComma=null;
if(b.equals("00")){
 amountWithComma=batchAmount+".00";
}else{
amountWithComma=batchAmount+"."+b;  
}

bankInfo.setTotSummaryAmount(String.valueOf(amountWithComma));
bankInfo.setTotSummaryItems(String.valueOf(itemcount));
bankInfo.setTotSummaryTrans(String.valueOf(transactioncount));
bankInfo.setFileTypeName(rows[1]);
FiletypeSummary.add(bankInfo);


//groupInfo.setTotalBanks(daywisebankslist.size());
irPlusResponseDetails.setFiletypeSummary(FiletypeSummary);
}
else
{

bankInfo.setFileTypeName(rows[1]);
bankInfo.setTotSummaryAmount("0.00");
bankInfo.setTotSummaryItems("0");
bankInfo.setTotSummaryTrans("0");
FiletypeSummary.add(bankInfo);
irPlusResponseDetails.setFiletypeSummary(FiletypeSummary);

}

}
}
	}
}

			irPlusResponseDetails.setCustomerGroupingMngmntResponse(groupInfo);
			
	
			
			
				
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
	
	}
	catch(Exception e)
	{
		log.error("Exception in ShowCustomerReport",e);
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
		irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		throw new HibernateException(e);
	}
	return irPlusResponseDetails;
}

@Override
public IRPlusResponseDetails showDaywiseCustomerReport(CustomerGroupingMngmntInfo customerInfo) throws BusinessException
{


	IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

	Session session = null;

	 List<CustomerGroupingMngmntInfo> custmrGrpngMngmntInfoList=new ArrayList<CustomerGroupingMngmntInfo>();

	CustomerGroupingMngmntInfo bankInfo = null;
	CustomerGroupingMngmntInfo groupInfo = new CustomerGroupingMngmntInfo();
	IrsCustomerGrouping custGrpInfo = null;
	IrsCustomerGroupingDetails custGrpInfoDetails = null;
	try
	{

		session = sessionFactory.getCurrentSession();
		
		
	   	 String betweendate="";
		 String fromDate1="";
		 String ToDate1="";
	     DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
	     betweendate=customerInfo.getCurrentdate();
	     String[] dateParts = betweendate.split("-");

	    fromDate1 = dateParts[0]; 
	    ToDate1 = dateParts[1]; 

	   
		Date date = new SimpleDateFormat("MM/dd/yyyy").parse(fromDate1);
		String fromDate = new SimpleDateFormat("MM-dd-yyyy").format(date);
	
		
		Date todate = new SimpleDateFormat("MM/dd/yyyy").parse(ToDate1);
		String ToDate = new SimpleDateFormat("MM-dd-yyyy").format(todate);
	
		Query custId = session.createQuery("select fbr.customer.customerId from  IrMUsersBank as user inner join user.bank as branch inner join branch.irFPFileHeaderRecord as fhr inner join fhr.irFPBatchHeaderRecord as fbr WHERE fhr.processedDate between :start and :end and user.irMUsers.userid=:userId and user.isactive=:Isactive and fhr.bank.bankId=:branchId group by fbr.customer.customerId");			
		custId.setParameter("start",fromDate);
		custId.setParameter("end",ToDate);
		custId.setParameter("userId",customerInfo.getUserid());
		custId.setParameter("Isactive",1);
		custId.setParameter("branchId",customerInfo.getBankwiseId());
		List<?> custIdlist = custId.list();
		
		if(custIdlist.size()>0){
			for(int m=0; m<custIdlist.size(); m++) {
				bankInfo=new CustomerGroupingMngmntInfo();
				Object custIdval = (Object) custIdlist.get(m);
				
				

			Query daywiseCust= session.createQuery("from IrMUsersBank as user inner join user.bank as branch inner join branch.irFPFileHeaderRecord as fhr inner join fhr.irFPBatchHeaderRecord as fbr WHERE fhr.processedDate between :start and :end and  user.irMUsers.userid=:userId and user.isactive=:Isactive and fhr.bank.bankId=:branchId and fbr.customer.customerId=:customerId");			
			daywiseCust.setParameter("start",fromDate);
			daywiseCust.setParameter("end",ToDate);
			daywiseCust.setParameter("userId",customerInfo.getUserid());
			daywiseCust.setParameter("Isactive",1);
			daywiseCust.setParameter("branchId",customerInfo.getBankwiseId());
			daywiseCust.setParameter("customerId",custIdval);
			
			
			
			bankInfo.setCustId(custIdval);
			bankInfo.setBankwiseId(customerInfo.getBankwiseId());
			
			List<?> daywiseCustList =daywiseCust.list();		
			
			double totalbatchamt=0.00;
			if(daywiseCustList.size()>0){
			
				BigDecimal batchamt = BigDecimal.ZERO;
				int transactioncount=0;
				int itemcount=0;
				
				for(int j=0; j<daywiseCustList.size(); j++) {
					
				
				
					Object[] rows = (Object[]) daywiseCustList.get(j);
					IrFPFileHeaderRecord irFhr = (IrFPFileHeaderRecord)rows[2];
					IrFPBatchHeaderRecord irBcr = (IrFPBatchHeaderRecord)rows[3];
					
					bankInfo.setCompanyName(irBcr.getCustomer().getCompanyName());
					bankInfo.setBankCustomerCode(irBcr.getCustomer().getBankCustomerCode());
					

					transactioncount+=Integer.parseInt(irBcr.getTransactionCount());
					
					itemcount+=Integer.parseInt(irBcr.getItemCount());
					String[] convert = (irBcr.getBatchAmount()).split("\\.");
					String a=convert[0];	
				     String b = convert[1];
					a = a.replaceAll(",", "");
					String total =a+"."+b; 
					String value=String.valueOf(total);
					
					 batchamt = batchamt.add(new BigDecimal(value));
					
				}

				String dbAmount = batchamt.toString();
				String[] convert = dbAmount.split("\\.");
		        int a = Integer.parseInt(convert[0]);
		        String b = convert[1];

				NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
		     		String	batchAmount = numberFormat.format(a);
		     		
		     	String amountWithComma=null;
		       if(b.equals("00")){
			    	 amountWithComma=batchAmount+".00";
			      }else{
			    	amountWithComma=batchAmount+"."+b;  
			      }	
			
			

			bankInfo.setBankwiseAmount(String.valueOf(amountWithComma));
		       bankInfo.setTotalitems(String.valueOf(itemcount));
		       bankInfo.setTotaltransactions(String.valueOf(transactioncount));
		       custmrGrpngMngmntInfoList.add(bankInfo);
			
				
				
			}
			}
		}
			irPlusResponseDetails.setCustmrGrpngMngmntInfoList(custmrGrpngMngmntInfoList);
	
	
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
	
	}
	catch(Exception e)
	{
		log.error("Exception in ShowCustomerReport",e);
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
		irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		throw new HibernateException(e);
	}
	return irPlusResponseDetails;
}




@Override
public IRPlusResponseDetails DaycustomerReport(CustomerGroupingMngmntInfo customerInfo) throws BusinessException
{


	IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

	Session session = null;

	 List<CustomerGroupingMngmntInfo> custmrGrpngMngmntInfoList=new ArrayList<CustomerGroupingMngmntInfo>();

	CustomerGroupingMngmntInfo bankInfo = null;
	CustomerGroupingMngmntInfo groupInfo = new CustomerGroupingMngmntInfo();
	IrsCustomerGrouping custGrpInfo = null;
	IrsCustomerGroupingDetails custGrpInfoDetails = null;
	try
	{
		
		session = sessionFactory.getCurrentSession();
	
	
		 String betweendate="";
		 String fromDate1="";
		 String ToDate1="";
	     DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
	     betweendate=customerInfo.getCurrentdate();
	     String[] dateParts = betweendate.split("-");

	    fromDate1 = dateParts[0]; 
	    ToDate1 = dateParts[1]; 

	   
		Date date = new SimpleDateFormat("MM/dd/yyyy").parse(fromDate1);
		String fromDate = new SimpleDateFormat("MM-dd-yyyy").format(date);
	
		
		Date todate = new SimpleDateFormat("MM/dd/yyyy").parse(ToDate1);
		String ToDate = new SimpleDateFormat("MM-dd-yyyy").format(todate);
	
			Query daywiseCust= session.createQuery("from IrMUsersBank as user inner join user.bank as branch inner join branch.irFPFileHeaderRecord as fhr inner join fhr.irFPBatchHeaderRecord as fbr WHERE  fhr.processedDate between :start and :end and  user.irMUsers.userid=:userId and user.isactive=:Isactive and fhr.bank.bankId=:branchId and fbr.customer.customerId=:customerId");			
			daywiseCust.setParameter("start",fromDate);
			daywiseCust.setParameter("end",ToDate);
			daywiseCust.setParameter("userId",customerInfo.getUserid());
			daywiseCust.setParameter("Isactive",1);
			daywiseCust.setParameter("branchId",customerInfo.getBankwiseId());
			daywiseCust.setParameter("customerId",customerInfo.getCustomerid());
			
			List<?> daywiseCustList =daywiseCust.list();		
			
			double totalbatchamt=0.00;
			if(daywiseCustList.size()>0){
				
				BigDecimal batchamt = BigDecimal.ZERO;
				int transactioncount=0;
				int itemcount=0;
				
				for(int j=0; j<daywiseCustList.size(); j++) {
					bankInfo=new CustomerGroupingMngmntInfo();
				
					Object[] rows = (Object[]) daywiseCustList.get(j);
					IrFPFileHeaderRecord irFhr = (IrFPFileHeaderRecord)rows[2];
					IrFPBatchHeaderRecord irBcr = (IrFPBatchHeaderRecord)rows[3];
					
					bankInfo.setCompanyName(irBcr.getCustomer().getCompanyName());
					bankInfo.setBankCustomerCode(irBcr.getCustomer().getBankCustomerCode());
					bankInfo.setCustomerid(customerInfo.getCustomerid());
					bankInfo.setBankName(irFhr.getBank().getBankName());

					transactioncount+=Integer.parseInt(irBcr.getTransactionCount());
					
					itemcount+=Integer.parseInt(irBcr.getItemCount());
					String[] convert = (irBcr.getBatchAmount()).split("\\.");
					String a=convert[0];	
				     String b = convert[1];
					a = a.replaceAll(",", "");
					String total =a+"."+b; 
					String value=String.valueOf(total);
					
					 batchamt = batchamt.add(new BigDecimal(value));
									}

				String dbAmount = batchamt.toString();
				String[] convert = dbAmount.split("\\.");
		        int a = Integer.parseInt(convert[0]);
		        String b = convert[1];

				NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
		     		String	batchAmount = numberFormat.format(a);
		     		
		     	String amountWithComma=null;
		       if(b.equals("00")){
			    	 amountWithComma=batchAmount+".00";
			      }else{
			    	amountWithComma=batchAmount+"."+b;  
			      }	


			bankInfo.setBankwiseAmount(String.valueOf(amountWithComma));
		       bankInfo.setTotalitems(String.valueOf(itemcount));
		       bankInfo.setTotaltransactions(String.valueOf(transactioncount));

			   Query newcustomerquery = session.createQuery("SELECT COUNT(DISTINCT fhr.fileHeaderRecordId),file.filetypename from IrMUsersBank as user inner join user.bank as branch inner join branch.irFPFileHeaderRecord as fhr inner join  fhr.fileType as file inner join fhr.irFPBatchHeaderRecord as fbr  where fbr.customer.customerId=:customerId and user.irMUsers.userid=:userId and user.isactive=:Isactive and fhr.processedDate between :start and :end and fhr.bank.bankId=:branchId GROUP BY file.filetypename");	   

			   newcustomerquery.setParameter("customerId",customerInfo.getCustomerid());
			   newcustomerquery.setParameter("userId",customerInfo.getUserid());
			   newcustomerquery.setParameter("Isactive",1);

			   newcustomerquery.setParameter("start",fromDate);
			   newcustomerquery.setParameter("end",ToDate);
			   newcustomerquery.setParameter("branchId",customerInfo.getBankwiseId());
			   
				List<?> customerquery = newcustomerquery.list();
				if(customerquery.size()>0){
				for(int k=0; k<customerquery.size(); k++) {
				
				
					Object[] row = (Object[]) customerquery.get(k);

				if(row[1].equals("ACH")) {
					bankInfo.setAchfileCount((Long) row[0]);
				}
				if(row[1].equals("Lockbox")){
					bankInfo.setLockboxfileCount((Long) row[0]);
				}
			
					}

				}
				 custmrGrpngMngmntInfoList.add(bankInfo);
					irPlusResponseDetails.setCustmrGrpngMngmntInfoList(custmrGrpngMngmntInfoList);
			}

			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
	
	}
	catch(Exception e)
	{
		log.error("Exception in ShowCustomerReport",e);
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
		irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		throw new HibernateException(e);
	}
	return irPlusResponseDetails;
}


@Override
public IRPlusResponseDetails showDaywiseCustomerGroupReport(CustomerGroupingMngmntInfo customerInfo) throws BusinessException
{


	IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

	Session session = null;

	 List<CustomerGroupingMngmntInfo> custmrGrpngMngmntInfoList=new ArrayList<CustomerGroupingMngmntInfo>();

	CustomerGroupingMngmntInfo bankInfo = null;
	CustomerGroupingMngmntInfo groupInfo = new CustomerGroupingMngmntInfo();
	IrsCustomerGrouping custGrpInfo = null;
	IrsCustomerGroupingDetails custGrpInfoDetails = null;
	try
	{

		session = sessionFactory.getCurrentSession();
		
		
	    String betweendate="";
		 String fromDate1="";
		 String ToDate1="";
	     DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
	     betweendate=customerInfo.getCurrentdate();
	     String[] dateParts = betweendate.split("-");

	    fromDate1 = dateParts[0]; 
	    ToDate1 = dateParts[1]; 

	   
		Date date = new SimpleDateFormat("MM/dd/yyyy").parse(fromDate1);
		String fromDate = new SimpleDateFormat("MM-dd-yyyy").format(date);
	
		
		Date todate = new SimpleDateFormat("MM/dd/yyyy").parse(ToDate1);
		String ToDate = new SimpleDateFormat("MM-dd-yyyy").format(todate);

				String customerId="select b.customer_id from  ir_s_customergrouping c inner join ir_s_customergroupingdetails g on c.customer_grp_id=g.customer_grp_id  inner join  ir_m_users_bank u on u.bank_id=g.bank_id inner join ir_s_bank br on br.bank_id=u.bank_id inner join   ir_s_customers cs on cs.customer_id=g.customer_id  inner join ir_f_file_info f on f.bank_id=g.bank_id  inner join ir_f_batch_info b on b.file_id=f.file_id  and b.customer_id=cs.customer_id  where u.userid=:userId and u.isactive=1 and c.isactive=1 and g.isactive=1 and f.processed_date between :start and :end and g.customer_grp_id=:grpId group by b.customer_id";

				SQLQuery customerIdval = session.createSQLQuery(customerId);
				customerIdval.setParameter("start",fromDate);
				customerIdval.setParameter("end",ToDate);
				customerIdval.setParameter("userId",customerInfo.getUserid());
				customerIdval.setParameter("grpId",customerInfo.getCustomerGrpId());
				List<?> customerIdvalList = customerIdval.list();
			
				if(customerIdvalList.size()>0){
					for(int q=0; q<customerIdvalList.size(); q++) {
						
						Object getcustId = (Object) customerIdvalList.get(q);
				
						Query custName= session.createQuery("from IrSCustomers cust where cust.customerId=:custId");			
						custName.setParameter("custId",getcustId);
						List<IrSCustomers> custNameList = custName.list();
						if(custNameList.size()>0){
							for(int y=0; y<custNameList.size(); y++) {
								bankInfo=new CustomerGroupingMngmntInfo();
							    bankInfo.setBankCustomercode(custNameList.get(y).getBankCustomerCode());
							    bankInfo.setCompanyName(custNameList.get(y).getCompanyName());
							    bankInfo.setCustomerid(custNameList.get(y).getCustomerId());
							    bankInfo.setCustomerGrpId(customerInfo.getCustomerGrpId());
							    bankInfo.setBankName(custNameList.get(y).getIrSBankBranch().getBank().getBankName());
				
							    
							    
							    String bankwiseBatchAmt="select  b.batch_amount  from  ir_s_customergrouping c inner join ir_s_customergroupingdetails g on c.customer_grp_id=g.customer_grp_id  inner join  ir_m_users_bank u on u.bank_id=g.bank_id inner join ir_s_bank br on br.bank_id=u.bank_id inner join   ir_s_customers cs on cs.customer_id=g.customer_id  inner join ir_f_file_info f on f.bank_id=g.bank_id  inner join ir_f_batch_info b on b.file_id=f.file_id  and b.customer_id=cs.customer_id  where u.userid=:userId and u.isactive=1 and c.isactive=1 and g.isactive=1 and c.customer_grp_id=:grpId  and b.customer_id=:custId and f.processed_date between :start and :end";
								SQLQuery bankwiseBatchAmtdet = session.createSQLQuery(bankwiseBatchAmt);	
								bankwiseBatchAmtdet.setParameter("start",fromDate);
								bankwiseBatchAmtdet.setParameter("end",ToDate);
								bankwiseBatchAmtdet.setParameter("userId",customerInfo.getUserid());
								bankwiseBatchAmtdet.setParameter("grpId",customerInfo.getCustomerGrpId());
								bankwiseBatchAmtdet.setParameter("custId",getcustId);
								
								List<?> bankwiseBatchAmtdetList =bankwiseBatchAmtdet.list();		
								if(bankwiseBatchAmtdetList.size()>0){

								BigDecimal batchamt = BigDecimal.ZERO;

									for(int g=0;g<bankwiseBatchAmtdetList.size();g++){

										Object batch = (Object) bankwiseBatchAmtdetList.get(g);

										String[] convert = ((String) batch).split("\\.");
										String a=convert[0];	
									     String b = convert[1];
										a = a.replaceAll(",", "");
										String total =a+"."+b; 
										String value=String.valueOf(total);
										
										 batchamt = batchamt.add(new BigDecimal(value));
									
									
									String dbAmount = batchamt.toString();
									String[] converts = dbAmount.split("\\.");
									int aa = Integer.parseInt(converts[0]);
									String bb = converts[1];

									NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
											String	batchAmount = numberFormat.format(aa);
											
										String amountWithComma=null;
									if(bb.equals("00")){
										 amountWithComma=batchAmount+".00";
									  }else{
										amountWithComma=batchAmount+"."+bb;  
									  }
									bankInfo.setBankwiseAmount(amountWithComma);
										
										
										
									}
								}
											
				String bankwiseDet="select  sum(cast(b.transaction_count as int))qty, sum(cast(b.item_count as int))item  from  ir_s_customergrouping c inner join ir_s_customergroupingdetails g on c.customer_grp_id=g.customer_grp_id  inner join  ir_m_users_bank u on u.bank_id=g.bank_id inner join ir_s_bank br on br.bank_id=u.bank_id inner join   ir_s_customers cs on cs.customer_id=g.customer_id  inner join ir_f_file_info f on f.bank_id=g.bank_id  inner join ir_f_batch_info b on b.file_id=f.file_id  and b.customer_id=cs.customer_id  where u.userid=:userId and u.isactive=1 and c.isactive=1 and g.isactive=1 and b.customer_id=:custId  and c.customer_grp_id=:GrpId and f.processed_date between :start and :end";
				
								SQLQuery bankwiseDetails = session.createSQLQuery(bankwiseDet);	
								bankwiseDetails.setParameter("start",fromDate);
								bankwiseDetails.setParameter("end",ToDate);
								bankwiseDetails.setParameter("userId",customerInfo.getUserid());
								bankwiseDetails.setParameter("GrpId",customerInfo.getCustomerGrpId());
								bankwiseDetails.setParameter("custId",getcustId);
								List<?> bankwiseDetailsList =bankwiseDetails.list();		
								if(bankwiseDetailsList.size()>0){

									for(int r=0;r<bankwiseDetailsList.size();r++){

										Object[] rowss = (Object[]) bankwiseDetailsList.get(r);
					
										bankInfo.setTotaltransactions(String.valueOf(rowss[0]));
									       bankInfo.setTotalitems(String.valueOf(rowss[1]));
									
									}
									
								
							}
								}
							}
						 custmrGrpngMngmntInfoList.add(bankInfo);
					
						}
					
			}
				 irPlusResponseDetails.setCustmrGrpngMngmntInfoList(custmrGrpngMngmntInfoList);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
	
	
		}
	
	catch(Exception e)
	{
		log.error("Exception in ShowCustomerReport",e);
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
		irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		throw new HibernateException(e);
	}
	return irPlusResponseDetails;
}



@Override
public IRPlusResponseDetails DaycustomerGroupReport(CustomerGroupingMngmntInfo customerInfo) throws BusinessException
{
	IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

	Session session = null;

	 List<CustomerGroupingMngmntInfo> custmrGrpngMngmntInfoList=new ArrayList<CustomerGroupingMngmntInfo>();

	CustomerGroupingMngmntInfo bankInfo = null;
	CustomerGroupingMngmntInfo groupInfo = new CustomerGroupingMngmntInfo();
	IrsCustomerGrouping custGrpInfo = null;
	IrsCustomerGroupingDetails custGrpInfoDetails = null;
	try
	{
		
		session = sessionFactory.getCurrentSession();
	
	
		 String betweendate="";
		 String fromDate1="";
		 String ToDate1="";
	     DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
	     betweendate=customerInfo.getCurrentdate();
	     String[] dateParts = betweendate.split("-");

	    fromDate1 = dateParts[0]; 
	    ToDate1 = dateParts[1]; 

	   
		Date date = new SimpleDateFormat("MM/dd/yyyy").parse(fromDate1);
		String fromDate = new SimpleDateFormat("MM-dd-yyyy").format(date);
	
		
		Date todate = new SimpleDateFormat("MM/dd/yyyy").parse(ToDate1);
		String ToDate = new SimpleDateFormat("MM-dd-yyyy").format(todate);
	
		Query custName= session.createQuery("from IrSCustomers cust where cust.customerId=:custId");			
		custName.setParameter("custId",customerInfo.getCustomerid());
		List<IrSCustomers> custNameList = custName.list();
		if(custNameList.size()>0){
			for(int y=0; y<custNameList.size(); y++) {
				bankInfo=new CustomerGroupingMngmntInfo();
			    bankInfo.setBankCustomercode(custNameList.get(y).getBankCustomerCode());
			    bankInfo.setCompanyName(custNameList.get(y).getCompanyName());
			    bankInfo.setCustomerid(custNameList.get(y).getCustomerId());
			    bankInfo.setCustomerGrpId(customerInfo.getCustomerGrpId());
			    bankInfo.setBankName(custNameList.get(y).getIrSBankBranch().getBank().getBankName());
		
		
	    String bankwiseBatchAmt="select  b.batch_amount  from  ir_s_customergrouping c inner join ir_s_customergroupingdetails g on c.customer_grp_id=g.customer_grp_id  inner join  ir_m_users_bank u on u.bank_id=g.bank_id inner join ir_s_bank br on br.bank_id=u.bank_id inner join   ir_s_customers cs on cs.customer_id=g.customer_id  inner join ir_f_file_info f on f.bank_id=g.bank_id  inner join ir_f_batch_info b on b.file_id=f.file_id  and b.customer_id=cs.customer_id  where u.userid=:userId and u.isactive=1 and c.isactive=1 and g.isactive=1 and c.customer_grp_id=:grpId  and b.customer_id=:custId and f.processed_date between :start and :end";
		SQLQuery bankwiseBatchAmtdet = session.createSQLQuery(bankwiseBatchAmt);	
		bankwiseBatchAmtdet.setParameter("start",fromDate);
		bankwiseBatchAmtdet.setParameter("end",ToDate);
		bankwiseBatchAmtdet.setParameter("userId",customerInfo.getUserid());
		bankwiseBatchAmtdet.setParameter("grpId",customerInfo.getCustomerGrpId());
		bankwiseBatchAmtdet.setParameter("custId",customerInfo.getCustomerid());
		
		List<?> bankwiseBatchAmtdetList =bankwiseBatchAmtdet.list();		
		if(bankwiseBatchAmtdetList.size()>0){

		BigDecimal batchamt = BigDecimal.ZERO;

			for(int g=0;g<bankwiseBatchAmtdetList.size();g++){

				Object batch = (Object) bankwiseBatchAmtdetList.get(g);

				String[] convert = ((String) batch).split("\\.");
				String a=convert[0];	
			     String b = convert[1];
				a = a.replaceAll(",", "");
				String total =a+"."+b; 
				String value=String.valueOf(total);
				
				 batchamt = batchamt.add(new BigDecimal(value));
			
			
			String dbAmount = batchamt.toString();
			String[] converts = dbAmount.split("\\.");
			int aa = Integer.parseInt(converts[0]);
			String bb = converts[1];

			NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
					String	batchAmount = numberFormat.format(aa);
					
				String amountWithComma=null;
			if(bb.equals("00")){
				 amountWithComma=batchAmount+".00";
			  }else{
				amountWithComma=batchAmount+"."+bb;  
			  }
			bankInfo.setBankwiseAmount(amountWithComma);
				
				
				
			}
		}
					
String bankwiseDet="select  sum(cast(b.transaction_count as int))qty, sum(cast(b.item_count as int))item  from  ir_s_customergrouping c inner join ir_s_customergroupingdetails g on c.customer_grp_id=g.customer_grp_id  inner join  ir_m_users_bank u on u.bank_id=g.bank_id inner join ir_s_bank br on br.bank_id=u.bank_id inner join   ir_s_customers cs on cs.customer_id=g.customer_id  inner join ir_f_file_info f on f.bank_id=g.bank_id  inner join ir_f_batch_info b on b.file_id=f.file_id  and b.customer_id=cs.customer_id  where u.userid=:userId and u.isactive=1 and c.isactive=1 and g.isactive=1 and b.customer_id=:custId  and c.customer_grp_id=:GrpId and f.processed_date between :start and :end";

		SQLQuery bankwiseDetails = session.createSQLQuery(bankwiseDet);	
		bankwiseDetails.setParameter("start",fromDate);
		bankwiseDetails.setParameter("end",ToDate);
		bankwiseDetails.setParameter("userId",customerInfo.getUserid());
		bankwiseDetails.setParameter("GrpId",customerInfo.getCustomerGrpId());
		bankwiseDetails.setParameter("custId",customerInfo.getCustomerid());
		List<?> bankwiseDetailsList =bankwiseDetails.list();		
		if(bankwiseDetailsList.size()>0){

			for(int r=0;r<bankwiseDetailsList.size();r++){

				Object[] rowss = (Object[]) bankwiseDetailsList.get(r);
			
				bankInfo.setTotaltransactions(String.valueOf(rowss[0]));
			       bankInfo.setTotalitems(String.valueOf(rowss[1]));
			
			}
			
		
	}
						String Bankwisefile="select count(distinct f.file_id),t.filetypename from ir_m_users_bank u inner join ir_s_bank b on u.bank_id=b.bank_id inner join ir_s_customergroupingdetails d on d.bank_id=b.bank_id  inner join ir_s_customergrouping c on c.customer_grp_id=d.customer_grp_id inner join ir_f_file_info f on f.bank_id=b.bank_id  inner join ir_f_batch_info bt on bt.file_id=f.file_id and bt.customer_id = d.customer_id inner join ir_m_filetypes t on f.file_type_id=t.filetypeid where f.processed_date  between :start and :end and u.isactive=1 and d.isactive=1 and c.isactive=1 and u.userid=:userId and c.customer_grp_id=:grpId and bt.customer_id=:custId group by t.filetypename,t.filetypeid";
						SQLQuery Bankwise = session.createSQLQuery(Bankwisefile);

                       Bankwise.setParameter("start",fromDate);
 					   Bankwise.setParameter("end",ToDate);
 					   Bankwise.setParameter("userId",customerInfo.getUserid());
 					   Bankwise.setParameter("grpId",customerInfo.getCustomerGrpId());
 					   Bankwise.setParameter("custId",customerInfo.getCustomerid());

						List<?> Bankwisefilelist = Bankwise.list();
						if(Bankwisefilelist.size()>0){
						for(int m=0; m<Bankwisefilelist.size(); m++) {
						
							Object[] rowss = (Object[]) Bankwisefilelist.get(m);
						
						if(rowss[1].equals("ACH")) {
							 
							bankInfo.setBankAchfileCount(((Number) rowss[0]).longValue());
						
						}
						if(rowss[1].equals("Lockbox")){
							bankInfo.setBankLockboxfileCount(((Number) rowss[0]).longValue());
						}
				
							}
						}
		
				 custmrGrpngMngmntInfoList.add(bankInfo);
					irPlusResponseDetails.setCustmrGrpngMngmntInfoList(custmrGrpngMngmntInfoList);
			}

			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
	
	}
	}
	catch(Exception e)
	{
		log.error("Exception in ShowCustomerReport",e);
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
		irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		throw new HibernateException(e);
	}
	return irPlusResponseDetails;
}

@Override
public IRPlusResponseDetails getGroupCustomerwise(CustomerGroupingMngmntInfo customerdateinfo) throws BusinessException
{


	IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

	Session session = null;

	//IRSBank irsBank = null;
	 List<CustomerGroupingMngmntInfo> custmrGrpngMngmntInfoList=new ArrayList<CustomerGroupingMngmntInfo>();

	CustomerGroupingMngmntInfo groupInfo = null;
		
	IrsCustomerGrouping custGrpInfo = null;
	IrsCustomerGroupingDetails custGrpInfoDetails = null;
	try
	{
		
	
		session = sessionFactory.getCurrentSession();

		 String betweendate="";
		 String fromDate1="";
		 String ToDate1="";
	     DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
	     betweendate=customerdateinfo.getCurrentdate();
	     String[] dateParts = betweendate.split("-");

	    fromDate1 = dateParts[0]; 
	    ToDate1 = dateParts[1]; 

	   
		Date date = new SimpleDateFormat("MM/dd/yyyy").parse(fromDate1);
		String fromDate = new SimpleDateFormat("MM-dd-yyyy").format(date);
	
		
		Date todate = new SimpleDateFormat("MM/dd/yyyy").parse(ToDate1);
		String ToDate = new SimpleDateFormat("MM-dd-yyyy").format(todate);
		
		
		Query group= session.createQuery("select fhr.processedDate from IrMUsersBank as user inner join user.bank as branch inner join branch.irSCustomersgrouping as cust inner join cust.irsCustomerGrouping as group inner join branch.irFPFileHeaderRecord as fhr inner join fhr.irFPBatchHeaderRecord as fbr WHERE  fhr.processedDate between :start and :end  and user.irMUsers.userid=:userId and user.isactive=:Isactive and fbr.customer.customerId=:customerId and group.customerGrpId=:grpId and group.isactive=:isactive group by fhr.processedDate");			

		group.setParameter("start",fromDate);
		group.setParameter("end",ToDate);
		group.setParameter("userId",customerdateinfo.getUserid());
		group.setParameter("Isactive",1);
		group.setParameter("customerId",customerdateinfo.getCustomerid());
		group.setParameter("grpId",customerdateinfo.getCustomerGrpId());
		group.setParameter("isactive",1);
		
		
		List<?> grouprows = group.list();
	
		if(grouprows.size()>0){
		for(int k=0; k<grouprows.size(); k++) {
		
			groupInfo = new CustomerGroupingMngmntInfo();
			
			Object row = (Object) grouprows.get(k);
			
		
			Query group1= session.createQuery("from IrMUsersBank as user inner join user.bank as branch inner join branch.irSCustomersgrouping as cust inner join cust.irsCustomerGrouping as group inner join branch.irFPFileHeaderRecord as fhr inner join fhr.irFPBatchHeaderRecord as fbr WHERE  fhr.processedDate=:date  and user.irMUsers.userid=:userId and user.isactive=:Isactive and fbr.customer.customerId=:customerId and group.customerGrpId=:grpId and group.isactive=:isactive");			

			group1.setParameter("date",row);	
			group1.setParameter("userId",customerdateinfo.getUserid());
			group1.setParameter("Isactive",1);
			group1.setParameter("customerId",customerdateinfo.getCustomerid());	
			group1.setParameter("grpId",customerdateinfo.getCustomerGrpId());
			group1.setParameter("isactive",1);
		
		
			List<?>custgroup1list = group1.list();
			if(custgroup1list.size()>0){	
				BigDecimal batchamt = BigDecimal.ZERO;
				int transcount=0;
				
				int transactioncount=0;
				int itemcount=0;
				
			     for(int m=0; m<custgroup1list.size(); m++) {
				//FileDetails = new IrFPBatchHeaderRecord();
				
				Object[] roww = (Object[]) custgroup1list.get(m);
				IrFPFileHeaderRecord ifhr = (IrFPFileHeaderRecord)roww[4];
				IrFPBatchHeaderRecord ifbr = (IrFPBatchHeaderRecord)roww[5];
				
				
				double totalbatchamt=0.00;

				transactioncount+=Integer.parseInt(ifbr.getTransactionCount());
				itemcount+=Integer.parseInt(ifbr.getItemCount());
				String[] convert = (ifbr.getBatchAmount()).split("\\.");
				String a=convert[0];	
			     String b = convert[1];
				a = a.replaceAll(",", "");
				String total =a+"."+b; 
				String value=String.valueOf(total);
				
				 batchamt = batchamt.add(new BigDecimal(value));					
				groupInfo.setHighestdate(ifhr.getProcessedDate());
				
			}
		

			groupInfo.setTotaltransactions(String.valueOf(transactioncount));
			groupInfo.setTotalitems(String.valueOf(itemcount));
			  String dbAmount = batchamt.toString();
				String[] convert = dbAmount.split("\\.");
		        int a = Integer.parseInt(convert[0]);
		        String b = convert[1];

				NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
		     		String	batchAmount = numberFormat.format(a);
		     		
		     	String amountWithComma=null;
		       if(b.equals("00")){
			    	 amountWithComma=batchAmount+".00";
			      }else{
			    	amountWithComma=batchAmount+"."+b;  
			      }
		       


		       groupInfo.setTotalbatchamt(String.valueOf(amountWithComma));
			
	
		}
			String Bankwisefile="select count(distinct f.file_id),t.filetypename from ir_m_users_bank u inner join ir_s_bank b on u.bank_id=b.bank_id inner join ir_s_customergroupingdetails d on d.bank_id=b.bank_id  inner join ir_s_customergrouping c on c.customer_grp_id=d.customer_grp_id inner join ir_f_file_info f on f.bank_id=b.bank_id  inner join ir_f_batch_info bt on bt.file_id=f.file_id and bt.customer_id = d.customer_id inner join ir_m_filetypes t on f.file_type_id=t.filetypeid where f.processed_date=:date and u.isactive=1 and d.isactive=1 and c.isactive=1 and u.userid=:userId and c.customer_grp_id=:grpId and bt.customer_id=:custId group by t.filetypename,t.filetypeid";
			SQLQuery Bankwise = session.createSQLQuery(Bankwisefile);
          
    
			Bankwise.setParameter("date",row);	
			Bankwise.setParameter("userId",customerdateinfo.getUserid());
		
			Bankwise.setParameter("custId",customerdateinfo.getCustomerid());	
			Bankwise.setParameter("grpId",customerdateinfo.getCustomerGrpId());
			
/*						Query Bankwise = session.createQuery("SELECT count(DISTINCT fhr.fileHeaderRecordId),file.filetypename from IrMUsersBank as user inner join user.irSBankbranch as branch inner join branch.irSCustomersgrouping as cust inner join cust.irsCustomerGrouping as group inner join  branch.irFPFileHeaderRecord as fhr inner join  fhr.fileType as file  WHERE fhr.processedDate between :start and :end and user.irMUsers.userid=:userId and user.isactive=:Isactive and group.isactive=:isactive and cust.isactive=:status and group.customerGrpId=:grpId GROUP BY file.filetypename");	   
*/
	
		
			List<?> Bankwisefilelist = Bankwise.list();
			if(Bankwisefilelist.size()>0){
			
			for(int m=0; m<Bankwisefilelist.size(); m++) {
				Object[] rowss = (Object[]) Bankwisefilelist.get(m);
			
			if(rowss[1].equals("ACH")) {
				groupInfo.setBankAchfileCount(((Number) rowss[0]).longValue());
			}
			if(rowss[1].equals("Lockbox")){
				groupInfo.setBankLockboxfileCount(((Number) rowss[0]).longValue());
			}
				}
			}		

			custmrGrpngMngmntInfoList.add(groupInfo);
			irPlusResponseDetails.setCustmrGrpngMngmntInfoList(custmrGrpngMngmntInfoList);

			}	
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
		}
		else
		{
			irPlusResponseDetails.setValidationSuccess(false);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
		}			

	}
	catch(Exception e)
	{
		log.error("Exception in updateBank",e);
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
		irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		throw new HibernateException(e);
	}
	return irPlusResponseDetails;
}

@Override
public IRPlusResponseDetails filterGroups(CustomerGroupingMngmntInfo grpFilterInfo) throws BusinessException
{

	IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

	Session session = null;

	List<BankViewInfo>banks = new ArrayList<BankViewInfo> ();

	try
	{
	
		session = sessionFactory.getCurrentSession();

		Criteria criteria = session.createCriteria(IrsCustomerGrouping.class,"Grp");
		criteria.setProjection(Projections.groupProperty("Grp.customerGrpId"));

		criteria.createAlias("Grp.irSCustomergroupingdet", "grpDet");
		criteria.createAlias("grpDet.irSBankBranch", "branch");
		criteria.createAlias("branch.bank", "bank");
		criteria.createAlias("grpDet.irSCustomers", "customer");
		
		if(grpFilterInfo.getCustomerGroupName()!=null&&!grpFilterInfo.getCustomerGroupName().isEmpty())
		{
			criteria.add(Restrictions.eq("Grp.customerGroupName",grpFilterInfo.getCustomerGroupName()));
		}
		if(grpFilterInfo.getBankName()!=null&&!grpFilterInfo.getBankName().isEmpty())
		{
			criteria.add(Restrictions.eq("bank.bankName",grpFilterInfo.getBankName()));
		}
		if(grpFilterInfo.getCompanyName()!=null&&!grpFilterInfo.getCompanyName().isEmpty())
		{
			criteria.add(Restrictions.eq("customer.companyName",grpFilterInfo.getCompanyName()));
		}
		if(grpFilterInfo.getBankCustomercode()!=null&&!grpFilterInfo.getBankCustomercode().isEmpty())
		{
			criteria.add(Restrictions.eq("customer.bankCustomerCode",grpFilterInfo.getBankCustomercode()));
		}
			criteria.add(Restrictions.eq("Grp.isactive",1));
		
			CustomerGroupingMngmntInfo grpinfo =null;
			List<IrsCustomerGrouping> grplist = criteria.list();
			 List<CustomerGroupingMngmntInfo> custmrGrpngMngmntInfoList=new ArrayList<CustomerGroupingMngmntInfo>();
	
			if(grplist.size()>0){
				for(int j=0; j<grplist.size(); j++) {
					Object grpId = (Object) grplist.get(j);
					
			Query group= session.createQuery("from IrsCustomerGrouping as grp  where grp.customerGrpId=:grpId and grp.isactive=:isactive");			

			group.setParameter("grpId",grpId);
			group.setParameter("isactive",1);
		
		
			

			List<IrsCustomerGrouping> grplists = group.list();
			if(grplists.size()>0){
			for(int i=0; i<grplists.size(); i++) {
				
				grpinfo = new CustomerGroupingMngmntInfo();

				grpinfo.setCustomerGroupName(grplists.get(i).getCustomerGroupName());
				grpinfo.setCreateddate(grplists.get(i).getCreateddate());
				grpinfo.setCreatedtime(grplists.get(i).getCreatedtime());
				grpinfo.setCustomerGrpId(grplists.get(i).getCustomerGrpId());
			
				Query query = session.createQuery("from IrsCustomerGroupingDetails grpdet where grpdet.irsCustomerGrouping.customerGrpId=:grpId");
				
				
				query.setParameter("grpId",grpId);
						List<IrsCustomerGroupingDetails> groupdetlist =query.list();		

						if(groupdetlist.size()>0){
							
							grpinfo.setTotalMappedBranch(groupdetlist.size());
							
						}	
						custmrGrpngMngmntInfoList.add(grpinfo);
			}
			}
			
		
				}
			}	
			
			
		irPlusResponseDetails.setCustmrGrpngMngmntInfoList(custmrGrpngMngmntInfoList);
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
	}
	catch(Exception e)
	{
		
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
		irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);

		throw new HibernateException(e);
	}

	return irPlusResponseDetails;
}


@Override
public IRPlusResponseDetails AutocompleteGroup(String term,String userid) throws BusinessException
{
	Session session = null;
	IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
	session = sessionFactory.getCurrentSession();
	BankInfo banklist = null;
	 String data;
	 List<BankInfo> list = new ArrayList<BankInfo>();
	 List<BankInfo> bankInfo=new ArrayList<BankInfo>();
	try {
		
		Query banks = session.createQuery("from IrsCustomerGrouping grp where grp.isactive=? and grp.customerGroupName LIKE ?");
		banks.setParameter(0,1);	
		banks.setParameter(1, term + "%");	
		
		List<IrsCustomerGrouping> userBankList=banks.list();
		
	int count=0;
	if(userBankList.size()>0){
			for(IrsCustomerGrouping irMUsersBank:userBankList){
				banklist = new BankInfo();
			
				banklist.setGroupname(irMUsersBank.getCustomerGroupName());
			
				bankInfo.add(banklist);
				irPlusResponseDetails.setList(bankInfo);
			}
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
	}
	else
	{
		irPlusResponseDetails.setValidationSuccess(false);
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
		irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
	}
	}	


	catch(Exception e)
	{
	
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
		irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		throw new HibernateException(e);
	}
	return irPlusResponseDetails;
	
	
}
}