package com.irplus.dao.customers;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.exception.ConstraintViolationException;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.irplus.dao.customer_contacts.ICustomerContactsDao;
import com.irplus.dao.customer_licencing.ICustomerLicencingDao;
import com.irplus.dao.hibernate.entities.IRSBank;
import com.irplus.dao.hibernate.entities.IRSBankLicensing;
import com.irplus.dao.hibernate.entities.IRSSite;
import com.irplus.dao.hibernate.entities.IrMBusinessProcess;
import com.irplus.dao.hibernate.entities.IrMFieldtypes;
import com.irplus.dao.hibernate.entities.IrMFileTypes;
import com.irplus.dao.hibernate.entities.IrMUsers;
import com.irplus.dao.hibernate.entities.IrMUsersBank;
import com.irplus.dao.hibernate.entities.IrSBankBranch;
import com.irplus.dao.hibernate.entities.IrSBankContact;
import com.irplus.dao.hibernate.entities.IrSCustomerOtherlicensing;
import com.irplus.dao.hibernate.entities.IrSCustomercontacts;
import com.irplus.dao.hibernate.entities.IrSCustomerlicensing;
import com.irplus.dao.hibernate.entities.IrSCustomers;
import com.irplus.dao.hibernate.entities.IrSFileTypes;
import com.irplus.dao.hibernate.entities.IrSFormsetup;
import com.irplus.dao.hibernate.entities.IrSFormvalidation;
import com.irplus.dao.hibernate.entities.IrsCustomerGrouping;
import com.irplus.dao.hibernate.entities.IrsCustomerGroupingDetails;
import com.irplus.dto.BankBranchInfo;
import com.irplus.dto.BankInfo;
import com.irplus.dto.BusinessProcessInfo;
import com.irplus.dto.ContactInfo;
import com.irplus.dto.CustomerBean;
import com.irplus.dto.CustomerContactsBean;
import com.irplus.dto.CustomerFilter;
import com.irplus.dto.CustomerGroupingDetails;
import com.irplus.dto.CustomerGroupingMngmntInfo;
import com.irplus.dto.DataEntryFormValidationDTO;
import com.irplus.dto.FileTypeInfo;
import com.irplus.dto.FormsManagementDTO;
import com.irplus.dto.IRMasterDataDao;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.LocationViewInfo;
import com.irplus.dto.ParentBank;
import com.irplus.dto.Sequence;
import com.irplus.dto.StatusIdInfo;
import com.irplus.util.BusinessException;
import com.irplus.util.IRPlusConstants;
import com.irplus.util.IRPlusUtil;

/**
 * This Dao Class - customer tables curd activities impacted here
 * @author arjun
 *
 **/
@Transactional
@Component

public class CustomerDaoImpl implements ICustomerDao {
		
private static final Logger log = Logger.getLogger(CustomerDaoImpl.class);
	
	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	ICustomerContactsDao iCustomerContactsDao;
	
	@Override
	public IRPlusResponseDetails getCustomerById(CustomerBean custBean) throws BusinessException {
		
		Session session = null;
		
		session = sessionFactory.getCurrentSession();
		log.info("Inside CustomerDaoImpl ");
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		List<String> errMsgs = new ArrayList<String>();
		
		IrSCustomers irSCustomers = null;
		
		int customerId;
		
		CustomerBean customerBean = new CustomerBean();
		
		CustomerContactsBean customerContacts = null;
		
		List<CustomerBean> listCustomBean = new ArrayList<CustomerBean>();
		
		List<CustomerContactsBean> contactsList = new ArrayList<CustomerContactsBean>();
	
		try {
			
			irSCustomers =(IrSCustomers) session.get(IrSCustomers.class,custBean.getCustomerId());
			
			if (irSCustomers!=null) {
				customerId= irSCustomers.getCustomerId();
				customerBean.setCustomerId(irSCustomers.getCustomerId());
				customerBean.setCompanyName(irSCustomers.getCompanyName());
				customerBean.setCompIdentNo(irSCustomers.getCompIdentNo());
				customerBean.setBankCustomerCode(irSCustomers.getBankCustomerCode());
				customerBean.setDdaBankAc(irSCustomers.getDdaBankAc());
				customerBean.setPhotofile(irSCustomers.getPhotofile());
				customerBean.setWebsite(irSCustomers.getWebsite());
				customerBean.setIrCustomerCode(irSCustomers.getIrCustomerCode());
				customerBean.setUserId(irSCustomers.getIrMUsers().getUserid());
				customerBean.setParentBankId(irSCustomers.getIrSBankBranch().getBank().getBankId());
				customerBean.setBankBranchId(irSCustomers.getIrSBankBranch().getBranchId());
				customerBean.setBranchLocation(irSCustomers.getIrSBankBranch().getBranchLocation());
				customerBean.setDefaultBankName(irSCustomers.getIrSBankBranch().getBank().getBankName());
				
				
				
			
				Query query = session.createQuery("from IrSCustomercontacts contacts where contacts.irSCustomers.customerId=:code ");
				
				query.setParameter("code",irSCustomers.getCustomerId());
				
				List<IrSCustomercontacts> irsContactsList = query.list();
				
			
				for(IrSCustomercontacts contacts:irsContactsList){
					
					customerContacts = new CustomerContactsBean();
					
					customerContacts.setCustomerContactId(contacts.getCustomerContactId());			
					customerContacts.setFirstName(contacts.getFirstName());
					customerContacts.setLastName(contacts.getLastName());
					customerContacts.setContactRole(contacts.getContactRole());
					customerContacts.setContactNo(contacts.getContactNo());
					customerContacts.setEmailId(contacts.getEmailId());
					customerContacts.setAddress1(contacts.getAddress1());
					customerContacts.setAddress2(contacts.getAddress2());
					customerContacts.setIsdefault(contacts.getIsdefault());
					customerContacts.setCountry(contacts.getCountry());
					customerContacts.setState(contacts.getState());
					customerContacts.setCity(contacts.getCity());
					customerContacts.setZipcode(contacts.getZipcode());
					customerContacts.setIsactive(contacts.getIsactive());
					
					contactsList.add(customerContacts);
				}
				
											
				CustomerContactsBean[] contactArray = new CustomerContactsBean[contactsList.size()];
				customerBean.setCustomerContact(contactsList.toArray(contactArray));
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				

				
				
				
				errMsgs.add("Success : Got Id :  the Customer Details");
				irPlusResponseDetails.setErrMsgs(errMsgs);
				irPlusResponseDetails.setValidationSuccess(true);
				irPlusResponseDetails.setCustomer(customerBean);
				getCustomerLicenses(customerBean);
				
			//	irPlusResponseDetails.setRecordsFiltered(customerContactsBeans_array.length);
			//	irPlusResponseDetails.setRecordsTotal(customerContactsBeans_array.length);
				
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
				
			} else {

				errMsgs.add("Error :: Not find Any Customer Details of This no : Please Enter Valid Customer");
				irPlusResponseDetails.setErrMsgs(errMsgs);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);				
			}
			
		} catch (Exception e) {

			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			log.error("Exception Raised inside customer DaoImple :: "+e);
			throw new HibernateException(e);
		}
				
		return irPlusResponseDetails;
	}

// done update 11-10-17
	
	@Override
	public IRPlusResponseDetails updateCustomer(CustomerBean customersBean) throws BusinessException {
		
		log.info("Inside CustomerDaoImpl");
		log.debug("Update Customer instance");
		Session session = null;
		
		session = sessionFactory.getCurrentSession();	
		List<String> errMsgs = new ArrayList<String>();
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		
		IrSCustomercontacts contacts = null;
		CustomerContactsBean customerContacts = null;
		
		
		Criteria criteria = session.createCriteria(IrSCustomers.class);
		List<IrSCustomers> lst = criteria.list();
		
		boolean flag = true;
				
		if (customersBean.getCompanyName()==null) {
			errMsgs.add("Validation Error : Please Enter the  Company Name  ");
		}
		else{		
		
			for (IrSCustomers irSCustomers : lst) {
				
				if(customersBean.getCustomerId() != irSCustomers.getCustomerId()){
					
					if(customersBean.getCompanyName().equalsIgnoreCase(irSCustomers.getCompanyName())){				
						flag = false;
					}			
				}	
			}
		
		if(flag==false){
			errMsgs.add("Validation Error : Duplicate Name Entered this /' Company Name Available /' ");
			
		}
		
		}//else close
		
		if (flag == true) {
			
			if(customersBean.getBankCustomerCode()==null){
				flag = false;
				errMsgs.add("Validation Error : Please enter  ' Bank Custermer code ' ");
			}
			
			if(customersBean.getDdaBankAc()==null){
				flag = false;
				errMsgs.add("Validation Error : Please enter  ' DDA Bank A/C ' ");
				
			}
			if(customersBean.getCompIdentNo()==null){
				flag = false;
				errMsgs.add("Validation Error : Please enter  ' Company Identification No ' ");
		
			}
		}
		
		try{
			if (flag) {
				
					
		IrSCustomers irSCustomers = (IrSCustomers) session.get(IrSCustomers.class, customersBean.getCustomerId());		
		IrSBankBranch branch = new IrSBankBranch();
		
		irSCustomers.setBankCustomerCode(customersBean.getBankCustomerCode());
		irSCustomers.setCity(customersBean.getCity());
		irSCustomers.setCompanyName(customersBean.getCompanyName());
		irSCustomers.setCompIdentNo(customersBean.getCompIdentNo());
		irSCustomers.setCountry(customersBean.getCountry());
		irSCustomers.setCreateddate(customersBean.getCreateddate());
		irSCustomers.setCustomerId(customersBean.getCustomerId());
		irSCustomers.setDdaBankAc(customersBean.getDdaBankAc());
		irSCustomers.setIrCustomerCode(customersBean.getIrCustomerCode());
		
		irSCustomers.setIrMUsers((IrMUsers)session.get(IrMUsers.class, customersBean.getUserId()));
		
		irSCustomers.setIsactive(customersBean.getIsactive());
		irSCustomers.setModifieddate(customersBean.getModifieddate());
		
		irSCustomers.setReferenceName(customersBean.getReferenceName());

		irSCustomers.setWebsite(customersBean.getWebsite());
		irSCustomers.setPhotofile(customersBean.getPhotofile());
		branch.setBranchId(customersBean.getBankBranchId());
		irSCustomers.setIrSBankBranch(branch);
		
	
		
//customer contact;		
		/*	Query query = session.createQuery("from IrSCustomercontacts where irSCustomers.customerId=:code ");
		
		query.setParameter("code",irSCustomers.getCustomerId());
				
		List<IrSCustomercontacts> irsContactsList = query.list();
	
		IrSCustomercontacts irscc = null;
		
		for (IrSCustomercontacts irSCustomercontacts : irsContactsList) {
			
			if(irSCustomercontacts.getIsdefault()>0){				
					irscc =irSCustomercontacts;					
				}
		}
		
		
			if(irscc!=null){
			irscc.setFirstName(customersBean.getFirstName());
			irscc.setLastName(customersBean.getLastName());
			irscc.setContactNo(customersBean.getContactNo());
			irscc.setEmailId(customersBean.getEmailId());
			irscc.setAddress1(customersBean.getAddress1());
			irscc.setAddress2(customersBean.getAddress2());
							
			irscc.setState(customersBean.getState());
			irscc.setCity(customersBean.getCity());					
			irscc.setZipcode(customersBean.getZipcode());
			
			}
		*/
			 if(irSCustomers!=null){	
			//	session.saveOrUpdate(irscc);
				session.saveOrUpdate(irSCustomers);
				customersBean.getCustomerContact()[0].setCustomerId(customersBean.getCustomerId());
				customersBean.setCustomerId(customersBean.getCustomerId());
				session.flush();
				errMsgs.add("Success :: updated ");
				irPlusResponseDetails.setErrMsgs(errMsgs);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
				irPlusResponseDetails.setValidationSuccess(true);
			
			 }else{
				 	errMsgs.add(" Validation Error :: Primary Contact not Available ");
					irPlusResponseDetails.setErrMsgs(errMsgs);
					irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
					irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			 }
		
			}else{ // validation flag 
				errMsgs.add(" Validation Error :: Please Enter Mandatory Fields ");
				irPlusResponseDetails.setErrMsgs(errMsgs);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			}
			}catch (Exception e) {

			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			log.error("Exception Raised inside customerDaoImpl :: updateCustomerId : "+e);
		
			throw new HibernateException(e);
		}
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails deleteCustomer(String customerId) throws BusinessException {
	
		log.info("Inside customerDaoImpl :Delete");
		log.debug("deleting Customer instance : Delete Customer");
		Session session = null;
		
		session = sessionFactory.getCurrentSession();
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		List<String> errMsgs = new ArrayList<String>();
		Integer id= Integer.parseInt(customerId);
		try{	
			
			IrSCustomers customer = (IrSCustomers) session.get(IrSCustomers.class,id);
			if(customer!=null)
			{
				customer.setIsactive(2);
				session.update(customer);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			}
			else
			{
				irPlusResponseDetails.setValidationSuccess(false);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
			}	
			
			
			
			
		}catch(ConstraintViolationException er){
			errMsgs.add("SQL Error :: Constraint Violation Error :: Row is mapped with foreign key : Not able to delete row");
			irPlusResponseDetails.setErrMsgs(errMsgs);
			log.error("SQL Exception ::  raised in delete customer");
		}
		catch (Exception e) {
			log.error("Exception raised in delete customer",e);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			throw new HibernateException(e);
		}
		return irPlusResponseDetails;
	}
	

	@Override
	public IRPlusResponseDetails showAllCustomer(String bankId) throws BusinessException {
		
		log.info("Inside customerDaoImpl");
		log.debug("Show all customers instances");
		
		IRPlusResponseDetails irPlusResponseDetails = new  IRPlusResponseDetails();
		List<CustomerBean> listCustomBean = new ArrayList<CustomerBean>();
		Session session = null;
		
		session = sessionFactory.getCurrentSession();
		List<String> errMsgs = new ArrayList<String>();
		try {
			
			
			Query customersQuery = session.createQuery("from IrSCustomers customer where customer.irSBankBranch.bank.bankId=? and customer.isactive!=2");
			customersQuery.setParameter(0,new Long(bankId));
			
	
			List<IrSCustomers> list = customersQuery.list();
			
			Query query = session.createQuery("from IrSCustomercontacts customerContact where customerContact.isdefault='1' and customerContact.irSCustomers.customerId=:code ");

			if (!list.isEmpty()) {
							
				for (IrSCustomers irSCustomers : list) {
			
					CustomerBean customerBean = new CustomerBean();
					
					if(irSCustomers.getBankCustomerCode()!=null){
					customerBean.setBankCustomerCode(irSCustomers.getBankCustomerCode());}else{
						customerBean.setBankCustomerCode("");	
					}
					if(irSCustomers.getCity()!=null){
					customerBean.setCity(irSCustomers.getCity());}else{
						customerBean.setCity("");
					}
					if(irSCustomers.getCompanyName()!=null){
					customerBean.setCompanyName(irSCustomers.getCompanyName());}else{
						customerBean.setCompanyName("");
					}
					if(irSCustomers.getCompIdentNo()!=null){
					customerBean.setCompIdentNo(irSCustomers.getCompIdentNo());}else{
						customerBean.setCompIdentNo("");	
					}
					if(irSCustomers.getCountry()!=null){
					customerBean.setCountry(irSCustomers.getCountry());}else{
						customerBean.setCountry("");
					}
					if(irSCustomers.getCreateddate()!=null){
					customerBean.setCreateddate(irSCustomers.getCreateddate());}else{
						errMsgs.add("Message Alert - Create date not available");
					}
					if(irSCustomers.getCustomerId()!=null){
						customerBean.setCustomerId(irSCustomers.getCustomerId());}else{
						customerBean.setCustomerId(irSCustomers.getCustomerId());
					}
					if(irSCustomers.getDdaBankAc()!=null){
						customerBean.setDdaBankAc(irSCustomers.getDdaBankAc());}else{
						customerBean.setDdaBankAc("");
					}
					if(irSCustomers.getIrCustomerCode()!=null){
						customerBean.setIrCustomerCode(irSCustomers.getIrCustomerCode());}else{
						customerBean.setIrCustomerCode("");
					}
					if(irSCustomers.getIsactive()!=null){
						customerBean.setIsactive(irSCustomers.getIsactive());}else{
						customerBean.setIsactive(0);
					}
					/*if(irSCustomers.getModifieddate()!=null){
					customerBean.setModifieddate(irSCustomers.getModifieddate());}else{
						customerBean.setModifieddate(i);		
					}*/
					if(irSCustomers.getReferenceName()!=null){
						customerBean.setReferenceName(irSCustomers.getReferenceName());}else{
						customerBean.setReferenceName(irSCustomers.getReferenceName());
					}
					if(irSCustomers.getState()!=null){
						customerBean.setState(irSCustomers.getState());}else{
						customerBean.setState("");
					}
					
					if(irSCustomers.getIrMUsers().getUserid()!=null){
						customerBean.setUserId(irSCustomers.getIrMUsers().getUserid());}else{
						errMsgs.add("Message Alert - user not available");
					}
					if(irSCustomers.getWebsite()!=null){
						customerBean.setWebsite(irSCustomers.getWebsite());}else{
						customerBean.setWebsite("");	
					}
					if(irSCustomers.getZipcode()!=null){
						customerBean.setZipcode(irSCustomers.getZipcode());}else{
						customerBean.setZipcode("");	
					}
					if(irSCustomers.getPhotofile()!=null){
					customerBean.setPhotofile(irSCustomers.getPhotofile());}else{
						customerBean.setPhotofile("");	
					}
					if(irSCustomers.getIrSBankBranch().getBranchId()!=null){					
					customerBean.setBankBranchId(irSCustomers.getIrSBankBranch().getBranchId());}else{
						errMsgs.add("Message Alert - BankBranch not available");
					}
					if(irSCustomers.getIrSBankBranch().getBank().getBankName()!=null){
					customerBean.setDefaultBankName(irSCustomers.getIrSBankBranch().getBank().getBankName());}
					else{
						customerBean.setDefaultBankName("");
					}
					
					if(irSCustomers.getIrSBankBranch().getBranchLocation()!=null){
						customerBean.setBranchLocation(irSCustomers.getIrSBankBranch().getBranchLocation());}
						else{
							customerBean.setBranchLocation("");
						}
					
					
					
					query.setParameter("code",irSCustomers.getCustomerId()); // In CustomerContacts
	//				query.setParameter("isdefault",true);
					List<IrSCustomercontacts> lst =query.list();
					
					CustomerContactsBean[] customerContactsBeans = new CustomerContactsBean[lst.size()];
					
					List<CustomerContactsBean> customerContactsList = new ArrayList<CustomerContactsBean>();
					
					int k =0;
					for (IrSCustomercontacts irSCustomercontacts : lst) {
						
						if(irSCustomercontacts.getIsdefault()>0){
							
							
							customerBean.setEmailId(irSCustomercontacts.getEmailId());
							customerBean.setContactNo(irSCustomercontacts.getContactNo());
							customerBean.setFirstName(irSCustomercontacts.getFirstName());
							customerBean.setLastName(irSCustomercontacts.getLastName());
							customerBean.setCreateddate(irSCustomercontacts.getCreateddate());
							customerBean.setAddress1(irSCustomercontacts.getAddress1());
							customerBean.setAddress2(irSCustomercontacts.getAddress2());
							
						
						}
						listCustomBean.add(customerBean);
					}
									
					BusinessProcessInfo processInfo = null;

					List<BusinessProcessInfo> processList = new ArrayList<BusinessProcessInfo>();

					Query bp = session.createQuery("FROM IrSCustomerOtherlicensing cl Where cl.irSCustomers.customerId=?");
					bp.setParameter(0,irSCustomers.getCustomerId());
					
					List<IrSCustomerOtherlicensing> bplist = bp.list();
					if(bplist.size()>0){
						for(IrSCustomerOtherlicensing irSCustomerOtherlicensing:bplist){
							processInfo = new BusinessProcessInfo();
							processInfo.setBusinessProcessId(irSCustomerOtherlicensing.getIrMBusinessprocess().getBusinessProcessId());

							processInfo.setProcessName(irSCustomerOtherlicensing.getIrMBusinessprocess().getProcessName());

							processList.add(processInfo);
							
							
						}
						customerBean.setBpList(processList);
						listCustomBean.add(customerBean);
					}
				
				
				}

				
				irPlusResponseDetails.setCustomerBean(listCustomBean);
				irPlusResponseDetails.setErrMsgs(errMsgs);
				irPlusResponseDetails.setRecordsTotal(listCustomBean.size());
				irPlusResponseDetails.setRecordsFiltered(listCustomBean.size());
				irPlusResponseDetails.setValidationSuccess(true);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
				
			} else {
		
				log.debug("Customers not available :: Empty Data :: Data required to show roles");
				irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
				irPlusResponseDetails.setErrMsgs(errMsgs);
			}
			
		} catch (Exception e) {
			log.error("Exception raised in show list customers",e);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			
			throw new HibernateException(e);
		}
		
		return irPlusResponseDetails;
		}	
	
	// started ...create 2 nov 2017 ... nov 4 - 2017 done some part
	
	@Autowired
	ICustomerLicencingDao iCustomerLicencingDao ;

	@Override	
	public IRPlusResponseDetails createCustomer(CustomerBean customerBean) throws BusinessException {
		
		log.info("Inside CustomerDaoImpl :: createCustomer");
		Session session = null;
		
		session = sessionFactory.getCurrentSession();
		List<String> errMsgs = new ArrayList<String>();
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		IrMUsers user = new IrMUsers();
		IrSBankBranch branch = new IrSBankBranch();
		IrSCustomercontacts irSCustomercontacts = new IrSCustomercontacts();
		try{
			
			
				IrSCustomers irSCustomers = new IrSCustomers();
				irSCustomers.setBankCustomerCode(customerBean.getBankCustomerCode());
				irSCustomers.setCompanyName(customerBean.getCompanyName());
				irSCustomers.setCompIdentNo(customerBean.getCompIdentNo());
				irSCustomers.setCountry(customerBean.getCountry());
				irSCustomers.setDdaBankAc(customerBean.getDdaBankAc());	
				irSCustomers.setIsactive(customerBean.getIsactive());
				irSCustomers.setReferenceName(customerBean.getReferenceName());
				irSCustomers.setState(customerBean.getState());
				irSCustomers.setPhotofile(customerBean.getPhotofile());
				irSCustomers.setCity(customerBean.getCity());
				irSCustomers.setZipcode(customerBean.getZipcode());
				irSCustomers.setWebsite(customerBean.getWebsite());
				irSCustomers.setIrCustomerCode(customerBean.getIrCustomerCode());
				user.setUserid(customerBean.getUserId());
				irSCustomers.setIrMUsers(user);
				branch.setBranchId(customerBean.getBankBranchId());
				irSCustomers.setIrSBankBranch(branch);

				
				Integer customerId=(Integer)session.save(irSCustomers);
				
				customerBean.getCustomerContact()[0].setCustomerId(customerId);
				customerBean.setCustomerId(customerId);
				session.flush();
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			
		
		}catch (Exception e) {
				log.error("Exception raised in create Customer");
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				throw new HibernateException(e);
		}
			

	
	return irPlusResponseDetails;
}
@Override
public IRPlusResponseDetails showAllBanks(String SiteId) throws BusinessException {

	log.info("Inside customerDaoImpl :showAllBanks");
	Session session = null;
	
	session = sessionFactory.getCurrentSession();	
	IRPlusResponseDetails irPlusResponseDetails = new  IRPlusResponseDetails();
	
	List<String> errMsgs = new ArrayList<String>();
	
	ParentBank p = new ParentBank();
	Map<Long, String> mapBankId_Name = new HashMap<Long, String>();
	
	
	try {
		/* Logic code
		 * 
		 * Parent Bank 1  = [ branches = 1 ,2,3,4];
		 *
		 * */
			
		
		
		Query banks = session.createQuery("from IRSBank bank where bank.siteId=? and bank.isactive!=?");
		banks.setParameter(0,SiteId);
		banks.setParameter(1,2);
		
		List<IRSBank> listBanks = banks.list();	
		if (!listBanks.isEmpty()) {			
		
			for (IRSBank irsBank:listBanks) {
				mapBankId_Name.put(irsBank.getBankId(),irsBank.getBankName());
			}
			p.setParentBank(mapBankId_Name);
				
			irPlusResponseDetails.setParentBank(p);
			
			errMsgs.add("Success :: Added Banks and BankBranches");
			irPlusResponseDetails.setErrMsgs(errMsgs);
			
			irPlusResponseDetails.setValidationSuccess(true);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			
		}else{
			
				errMsgs.add("Message Alert ::No Banks Available");
				log.debug("Customers : Message Alert ::No Banks Available ");
				irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
				irPlusResponseDetails.setErrMsgs(errMsgs);
			}
			
			
		
		
	} catch (Exception e) {
		log.error("Exception raised in show list customers",e);
		irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
		
		throw new HibernateException(e);
	}
	
	return irPlusResponseDetails;
}

/*	11 -nov - 2017..
 * 	THIS UPDATE STATUS =0,1,2
				0=INACTIVE
				1=ACTIVE
				2=DELETE
				*/
	
	public IRPlusResponseDetails updateCutomerStaus(StatusIdInfo statusIdInfo)throws BusinessException{
		
		log.info("Inside customerDaoImpl :updateCutomerStaus");
Session session = null;
		
		session = sessionFactory.getCurrentSession();	
		IRPlusResponseDetails irPlusResponseDetails = new  IRPlusResponseDetails();
		
		List<String> errMsgs = new ArrayList<String>();
		
		IrSCustomers irSCustomers = null;
		
		try {

			irSCustomers = (IrSCustomers)session.get(IrSCustomers.class,statusIdInfo.getId());
			
			if (irSCustomers!=null) {
				
				irSCustomers.setIsactive(statusIdInfo.getStatus());
			
				
				session.update(irSCustomers);
				
				session.flush();
				
				log.debug("Success :: Updated "+irSCustomers.getCustomerId()+": Id Customer Status as :"+irSCustomers.getIsactive());
						
				errMsgs.add("Success :: Updated "+irSCustomers.getCustomerId()+": Id Customer Status as :"+irSCustomers.getIsactive());
				
				irPlusResponseDetails.setErrMsgs(errMsgs);				
				irPlusResponseDetails.setValidationSuccess(true);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
				
			} else {
				
				errMsgs.add("Validation Error :: "+statusIdInfo.getId()+" : This id not Matching cutomerId ");
				log.debug("Customers not available :: Empty Data :: Data required to show roles");
				irPlusResponseDetails.setErrMsgs(errMsgs);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
			}
			
		} catch (Exception e) {
			log.error("Exception raised in show list customers",e);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			
			throw new HibernateException(e);
		}
				
		return irPlusResponseDetails;
	}


public IRPlusResponseDetails showAllFileProcessingOld() throws BusinessException {

	log.info("Inside customerDaoImpl :updateCutomerStaus");
	Session session = null;
	
	session = sessionFactory.getCurrentSession();
	IRPlusResponseDetails irPlusResponseDetails = new  IRPlusResponseDetails();
	
	List<String> errMsgs = new ArrayList<String>();
	List<FileTypeInfo> listFileTypes = new ArrayList<FileTypeInfo>();
	
	try{
		List<IrMFileTypes> listFileTypeInfo = session.createCriteria(IrMFileTypes.class)
				.add(Restrictions.like("isactive",1)).list();
		
	
		if(!listFileTypeInfo.isEmpty()){
		
		for (IrMFileTypes fileType : listFileTypeInfo) {
			
			FileTypeInfo ftinfo = new FileTypeInfo();
						
			ftinfo.setFiletypeid(fileType.getFiletypeid());
			ftinfo.setFiletypename(fileType.getFiletypename());
			ftinfo.setIsactive(fileType.getIsactive());
			//ftinfo.setLicenseId(fileType.getLicenseid());
			
			listFileTypes.add(ftinfo);			
		}
			errMsgs.add(" success : all FileTypes ");	
			irPlusResponseDetails.setFileTypeInfoList(listFileTypes);
			irPlusResponseDetails.setErrMsgs(errMsgs);				
			irPlusResponseDetails.setValidationSuccess(true);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			
		}else{
			
			errMsgs.add(" Validation Error : Data table empty : No Buisiness Process find ");			
			irPlusResponseDetails.setErrMsgs(errMsgs);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
		}
	
		} catch(Exception e){
			log.error("Exception raised in show list customers",e);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			
			throw new HibernateException(e);
		}
	
	return irPlusResponseDetails;
}


public IRPlusResponseDetails showAllBusinessProcessingOld() throws BusinessException {
	
	log.info("Inside customerDaoImpl :showAllBusinessProcessing");
	Session session = null;
	
	session = sessionFactory.getCurrentSession();
	IRPlusResponseDetails irPlusResponseDetails = new  IRPlusResponseDetails();
	
	List<String> errMsgs = new ArrayList<String>();
	
	try{
		IrMBusinessProcess irb = new IrMBusinessProcess();
		
		Criteria criteria = session.createCriteria(IrMBusinessProcess.class)		
		.add(Restrictions.like("isactive", 1));
		
		List<IrMBusinessProcess> lst = criteria.list();
		
		List<BusinessProcessInfo> list = new ArrayList<BusinessProcessInfo>();
	
		if(!lst.isEmpty()){
	
				for (IrMBusinessProcess irMBusPr : lst){
				
					if(irMBusPr.getIsactive()==1){
						
					BusinessProcessInfo bsp = new BusinessProcessInfo();			
					bsp.setBusinessProcessId(irMBusPr.getBusinessProcessId());
					bsp.setProcessName(irMBusPr.getProcessName());
					list.add(bsp);
					}					
				}
				
			irPlusResponseDetails.setBusinessProcessList(list);
			errMsgs.add("Success : All BusinessProcess");
			irPlusResponseDetails.setErrMsgs(errMsgs);
			irPlusResponseDetails.setValidationSuccess(true);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			}
		else{
			errMsgs.add(" Validation Error : Data table empty : No Buisiness Process find ");			
			irPlusResponseDetails.setErrMsgs(errMsgs);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
		}
				
			
		} catch(Exception e){
			log.error("Exception raised in show list customers",e);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			
			throw new HibernateException(e);
		}
	
	return irPlusResponseDetails;
}


/* 12 18 17 , 12 22 17  , 12 26 17  - cmpleted
 
 here wrote the filtering .... pending the taking fields in Object  
*/

@Override
public IRPlusResponseDetails customerFilter(CustomerFilter customerFilter)throws BusinessException {

	log.debug("Inside customerDaoImpl : customerFilter");
	
	Session session = null;
	
	session = sessionFactory.getCurrentSession();
	
	IRPlusResponseDetails irPlusResponseDetails = new  IRPlusResponseDetails();
	
	List<String> errMsgs = new ArrayList<String>();
	
	List<CustomerBean> listCustomBean = new ArrayList<CustomerBean>();
	
	CustomerBean customerBean = null;
	
//	Criteria criteria = session.createCriteria(IrSCustomers.class, "custmr");	
		
	IrSCustomerlicensing liscu = null;
	
	Criteria criteria2 = null;
	IrSCustomers  irSCustomers = null;
	boolean flag = false;
	
	try{
		
		Criteria criteria = session.createCriteria(IrSCustomers.class);	
	
		if(!criteria.list().isEmpty()){
/*
			flag = false;

	*/		if(customerFilter.getCompanyName()!=null){			
				criteria.add(Restrictions.like("companyName", customerFilter.getCompanyName()));
				
				if(!criteria.list().isEmpty()){
				flag = true;}
				
			}else{
				errMsgs.add("Customer Name Is Empty");
			}

			if(customerFilter.getBankCustomerCode()!=null){			
				criteria.add(Restrictions.like("bankCustomerCode", customerFilter.getBankCustomerCode()));
				
				/*if(!criteria.list().isEmpty()){
				flag = true;}else{flag = false;}*/
				
				flag = (!criteria.list().isEmpty()) ? true : false ;
			}
					 	
		if(flag){
		
			boolean flag_isacitve = true;
			boolean flag_buisiness = true;
			boolean flag_filetype = true;
			
			
			log.debug("inside myflag1 " +flag);
			
			if(!criteria.list().isEmpty()) {
				
				 irSCustomers =(IrSCustomers) criteria.list().get(0);									
					
					/*  Third condition */
					
				 	if(customerFilter.getIsactive()!=null){			
						criteria.add(Restrictions.like("isactive", customerFilter.getIsactive()));
					//	if(!criteria.list().isEmpty())
						flag_isacitve = (!criteria.list().isEmpty())? true: false;									
					}			
					
					/*Fourth Condition  select field*/
					
					if(customerFilter.getFiletypeid()!=null){
						
					 criteria2 = session.createCriteria(IrSCustomerlicensing.class ,"custmrlic")					 
								.add(Restrictions.like("custmrlic.irSCustomers.customerId",irSCustomers.getCustomerId()))
					 	        .add(Restrictions.eq("filetypeid",customerFilter.getFiletypeid()));				 
					 flag_filetype = (!criteria2.list().isEmpty())? true : false;			
					}
		
					/*if(customerFilter.getFiletypeid()!=null){
						criteria2.add(Restrictions.like("custmrlic.filetypeid", customerFilter.getFiletypeid()));
						liscu = (IrSCustomerlicensing) criteria2.list().get(0);
					}*/		
					
					/*Fifth Condition  select field */
					if(customerFilter.getBusinessProcessId()!=null && irSCustomers.getCustomerId()!= null){						
						Criteria criteria1 = session.createCriteria(IrSCustomerOtherlicensing.class , "cutmrOthr")
								.add(Restrictions.eq("cutmrOthr.irSCustomers.customerId", irSCustomers.getCustomerId()))
								.add(Restrictions.like("cutmrOthr.irMBusinessprocess.businessProcessId",customerFilter.getBusinessProcessId()));
						flag_buisiness = (!criteria1.list().isEmpty())? true : false ;
					}							
			}	
			
		//	if(liscu != null){
		//	if(true){
			
			/*if(myflag==false){
			if(flag){
				myflag = flag;
			}}*/
			
			
			
			if (flag_isacitve && flag_buisiness && flag_filetype){						
				
				log.debug("inside myflag"+irSCustomers!=null);
				customerBean = new CustomerBean();	
			if(irSCustomers.getBankCustomerCode()!=null){
				customerBean.setBankCustomerCode(irSCustomers.getBankCustomerCode());}else{
					customerBean.setBankCustomerCode("");	
				}
			if(irSCustomers.getCity()!=null){
				customerBean.setCity(irSCustomers.getCity());}else{
					customerBean.setCity("");
				}
			if(irSCustomers.getCompanyName()!=null){
				customerBean.setCompanyName(irSCustomers.getCompanyName());}else{
					customerBean.setCompanyName("");
				}
			if(irSCustomers.getCompIdentNo()!=null){
				customerBean.setCompIdentNo(irSCustomers.getCompIdentNo());}else{
					customerBean.setCompIdentNo("");	
				}
			if(irSCustomers.getCountry()!=null){
				customerBean.setCountry(irSCustomers.getCountry());}else{
					customerBean.setCountry("");
				}
			if(irSCustomers.getCreateddate()!=null){
				customerBean.setCreateddate(irSCustomers.getCreateddate());}else{
					errMsgs.add("Message Alert - Create date not available");
				}
			if(irSCustomers.getCustomerId()!=null){
				customerBean.setCustomerId(irSCustomers.getCustomerId());}else{
					customerBean.setCustomerId(irSCustomers.getCustomerId());
				}
			if(irSCustomers.getDdaBankAc()!=null){
				customerBean.setDdaBankAc(irSCustomers.getDdaBankAc());}else{
					customerBean.setDdaBankAc("");
				}
			if(irSCustomers.getIrCustomerCode()!=null){
				customerBean.setIrCustomerCode(irSCustomers.getIrCustomerCode());}else{
					customerBean.setIrCustomerCode("");
				}
			if(irSCustomers.getIsactive()!=null){
				customerBean.setIsactive(irSCustomers.getIsactive());}else{
					customerBean.setIsactive(0);
				}
			/*if(irSCustomers.getModifieddate()!=null){
					customerBean.setModifieddate(irSCustomers.getModifieddate());}else{
						customerBean.setModifieddate(i);		
					}*/
			if(irSCustomers.getReferenceName()!=null){
				customerBean.setReferenceName(irSCustomers.getReferenceName());}else{
					customerBean.setReferenceName(irSCustomers.getReferenceName());
				}
			if(irSCustomers.getState()!=null){
				customerBean.setState(irSCustomers.getState());}else{
					customerBean.setState("");
				}

			if(irSCustomers.getIrMUsers().getUserid()!=null){
				customerBean.setUserId(irSCustomers.getIrMUsers().getUserid());}else{
					errMsgs.add("Message Alert - user not available");
				}
			if(irSCustomers.getWebsite()!=null){
				customerBean.setWebsite(irSCustomers.getWebsite());}else{
					customerBean.setWebsite("");	
				}
			if(irSCustomers.getZipcode()!=null){
				customerBean.setZipcode(irSCustomers.getZipcode());}else{
					customerBean.setZipcode("");	
				}
			if(irSCustomers.getPhotofile()!=null){
				customerBean.setPhotofile(irSCustomers.getPhotofile());}else{
					customerBean.setPhotofile("");	
				}
			if(irSCustomers.getIrSBankBranch().getBranchId()!=null){					
				customerBean.setBankBranchId(irSCustomers.getIrSBankBranch().getBranchId());}else{
					errMsgs.add("Message Alert - BankBranch not available");
				}
			if(irSCustomers.getIrSBankBranch().getBank().getBankName()!=null){
				customerBean.setDefaultBankName(irSCustomers.getIrSBankBranch().getBank().getBankName());}
			else{
				customerBean.setDefaultBankName("");
			}
									
				List<IrSCustomercontacts> lst = session.createCriteria(IrSCustomercontacts.class)
				.add(Restrictions.like("irSCustomers.customerId", irSCustomers.getCustomerId())).list();
				
				for (IrSCustomercontacts irSCustomercontacts : lst){
					
					if(irSCustomercontacts.getIsdefault()>0){
							
						if(irSCustomercontacts.getEmailId()!=null){
						customerBean.setEmailId(irSCustomercontacts.getEmailId());}else{
							customerBean.setEmailId("NA");
						}
						if(irSCustomercontacts.getContactNo()!=null){
						customerBean.setContactNo(irSCustomercontacts.getContactNo());}else{
							customerBean.setContactNo("NA");
						}
						if(irSCustomercontacts.getFirstName()!=null){
						customerBean.setFirstName(irSCustomercontacts.getFirstName());}else{
							customerBean.setFirstName("NA");
						}
						if(irSCustomercontacts.getLastName()!=null){
						customerBean.setLastName(irSCustomercontacts.getLastName());}else{
							customerBean.setLastName("NA");
						}
						if(irSCustomercontacts.getCreateddate()!=null){
						customerBean.setCreateddate(irSCustomercontacts.getCreateddate());}else{
							errMsgs.add("Not Availabale Created Date");
						}
						if(irSCustomercontacts.getAddress1()!=null){
						customerBean.setAddress1(irSCustomercontacts.getAddress1());}else{
							customerBean.setAddress1("NA");
						}
						if(irSCustomercontacts.getAddress2()!=null){
						customerBean.setAddress2(irSCustomercontacts.getAddress2());}else{
							customerBean.setAddress2("NA");
						}									
					}
				}	 // close for
			}else{
				errMsgs.add("Message Alert : No matched record found");
				irPlusResponseDetails.setErrMsgs(errMsgs);
				irPlusResponseDetails.setValidationSuccess(true);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
			}
		}else{
			errMsgs.add("Message Alert : No matched record found");
			irPlusResponseDetails.setErrMsgs(errMsgs);
			irPlusResponseDetails.setValidationSuccess(true);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
		}
		if(customerBean!=null){			
			errMsgs.add("Success : got the data ");
			irPlusResponseDetails.setErrMsgs(errMsgs);
			irPlusResponseDetails.setCustomer(customerBean);
			irPlusResponseDetails.setValidationSuccess(true);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
		}else{
			errMsgs.add("Validation Error : Not found the record");
			irPlusResponseDetails.setErrMsgs(errMsgs);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
		}		
		}
		}catch(Exception e){
			log.error("Exception raised in customerFilter",e);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);			
			throw new HibernateException(e);
		}
return irPlusResponseDetails;
}

//12 22 17

public IRPlusResponseDetails showAllFileProcessing()throws BusinessException {
	
	log.debug("Inside customerDaoImpl : customerFilter");
	Session session = null;
	
	session = sessionFactory.getCurrentSession();
	IRPlusResponseDetails irPlusResponseDetails = new  IRPlusResponseDetails();

	List<String> errMsgs = new ArrayList<String>();

	List<FileTypeInfo> listfiletypes = new ArrayList<FileTypeInfo>();

		//	Criteria criteria = session.createCriteria(IrSCustomers.class, "custmr");	

	try{
		
		List<IrMFileTypes> lst = session.createCriteria(IrMFileTypes.class, "custmr")
				.add(Restrictions.like("isactive", 1)).list();	

		for (IrMFileTypes irSFileTypes : lst) {
			
			FileTypeInfo ftf = new FileTypeInfo();
			
			ftf.setFiletypeid(irSFileTypes.getFiletypeid());
			ftf.setFiletypename(irSFileTypes.getFiletypename());
			ftf.setIsactive(irSFileTypes.getIsactive());
			
			listfiletypes.add(ftf);
			
		}
		if(!listfiletypes.isEmpty()){
			irPlusResponseDetails.setFileTypeInfoList(listfiletypes);
			errMsgs.add("Success : All File Types ");
			irPlusResponseDetails.setErrMsgs(errMsgs);
			irPlusResponseDetails.setRecordsFiltered(listfiletypes.size());
			irPlusResponseDetails.setRecordsTotal(listfiletypes.size());
			irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			
		}else{
			errMsgs.add("Message Alert : No File Types Available");
			irPlusResponseDetails.setErrMsgs(errMsgs);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);			
		}		
	}catch(Exception e){
		log.error("Exception raised in customerFilter",e);
		irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
		throw new HibernateException(e);
	}
	return irPlusResponseDetails;
}


@Override
public IRPlusResponseDetails showAllBusinessProcessing() throws BusinessException {
//public IRPlusResponseDetails customerFilterOfBuisinessProcess()throws BusinessException {
	log.debug("Inside customerDaoImpl : customerFilter");
	Session session = null;
	
	session = sessionFactory.getCurrentSession();
	IRPlusResponseDetails irPlusResponseDetails = new  IRPlusResponseDetails();
	List<String> errMsgs = new ArrayList<String>();
	List<BusinessProcessInfo> listIrMBusinessProcess = new ArrayList<BusinessProcessInfo>();

	try{

		List<IrMBusinessProcess> lst = session.createCriteria(IrMBusinessProcess.class)
				.add(Restrictions.like("isactive", 1)).list();	

		for (IrMBusinessProcess buisiness : lst) {
			
			BusinessProcessInfo bpi = new BusinessProcessInfo();
			
			bpi.setBusinessProcessId(buisiness.getBusinessProcessId());
			bpi.setIsactive(buisiness.getIsactive());
			bpi.setProcessName(buisiness.getProcessName());
			
			listIrMBusinessProcess.add(bpi);
		}
		if(!listIrMBusinessProcess.isEmpty()){
			
			irPlusResponseDetails.setBusinessProcessList(listIrMBusinessProcess);
			errMsgs.add("Success : All Business Process Types ");
			irPlusResponseDetails.setErrMsgs(errMsgs);
			irPlusResponseDetails.setRecordsFiltered(listIrMBusinessProcess.size());
			irPlusResponseDetails.setRecordsTotal(listIrMBusinessProcess.size());
			irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			
		}else{
			errMsgs.add("Message Alert : No Business Process Available");
			irPlusResponseDetails.setErrMsgs(errMsgs);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);			
		}
		
	}catch(Exception e){
		log.error("Exception raised in customerFilter",e);
		irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
		throw new HibernateException(e);
	}

	return irPlusResponseDetails;
}

@Override
public IRPlusResponseDetails showBankBranch(String bankId) throws BusinessException {
	
	
	log.info("Inside listBankBranches");

	IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

	Session session = null;
	
	session = sessionFactory.getCurrentSession();

	LocationViewInfo locations = new LocationViewInfo();
	
	BusinessProcessInfo processInfo=null;
	FileTypeInfo fileTypeInfo=null;
	List<BusinessProcessInfo> processList = new ArrayList<BusinessProcessInfo>();
	List<FileTypeInfo> fileTypes = new ArrayList<FileTypeInfo>();

	List<BankBranchInfo> branchLists = new ArrayList<BankBranchInfo>();

	
	
	try
	{
		session = sessionFactory.getCurrentSession();
		
		/*isActive Status Code
		
		0-inactive
		1-active
		2-deleted*/	
		

		
		
		String hql = "from IrSBankBranch branch where branch.bank.bankId=? and branch.isactive=?";
		
		Query query = session.createQuery(hql);

		query.setParameter(0,new Long(bankId));
		query.setParameter(1,1);
		

	//List<IrSBankBranch> bankList = (List<IRSBank>)query.list();
		List<IrSBankBranch> branchList = query.list();

		Iterator<IrSBankBranch> bankListIt = branchList.iterator();

	

		BankBranchInfo branch = null;

		for(IrSBankBranch irSBankBranch:branchList){

			branch = new BankBranchInfo();
			branch.setParent_bankId(irSBankBranch.getBank().getBankId());
			branch.setParent_bankName(irSBankBranch.getBank().getBankName());
			branch.setBranchId(irSBankBranch.getBranchId());

			branch.setBranchLocation(irSBankBranch.getBranchLocation());
			
			branchLists.add(branch);
		
		}
		locations.setBankBranchList(branchLists);
		
		/*if(bankId!=null){
			
			Query bp = session.createQuery("FROM IRSBankLicensing bl Where bl.bank.bankId=?");
			bp.setParameter(0,new Long(bankId));
			List<IRSBankLicensing> bplist = bp.list();
			if(bplist.size()>0){
				for(IRSBankLicensing iRSBankLicensing:bplist){
					
					processInfo = new BusinessProcessInfo();

					processInfo.setBusinessProcessId(iRSBankLicensing.getBusinessProcess().getBusinessProcessId());

					processInfo.setProcessName(iRSBankLicensing.getBusinessProcess().getProcessName());

					processList.add(processInfo);
					
					locationViewInfo.setBankBusinessProcess(processList);
					
					fileTypeInfo = new FileTypeInfo();

					fileTypeInfo.setFiletypeid(iRSBankLicensing.getFileType().getFiletypeid());

					fileTypeInfo.setFiletypename(iRSBankLicensing.getFileType().getFiletypename());

					fileTypes.add(fileTypeInfo);
					
					locationViewInfo.setBankFileTypeInfo(fileTypes);
					
					
				}
				locations.add(locationViewInfo);
			}
			
			
			
		}
*/		irPlusResponseDetails.setBankBranches(locations);
		
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
	}
	catch(Exception e)
	{
		log.error("Exception in listBank",e);
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
		irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);

		throw new HibernateException(e);
	}

	return irPlusResponseDetails;
}

	@Override
	public IRPlusResponseDetails addCustomerLicense(CustomerBean customerBean) throws BusinessException {
		log.info("Inside add CustomerLicense :"+customerBean);
	
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
	
		Session session = null;
		IrSCustomers irSCustomers = null;
		IrSCustomerlicensing customerLicensing= null;
		IrMFileTypes fileType = null;
		IrSBankBranch branch = new IrSBankBranch();
		IrMUsers user = new IrMUsers();
	
		try
		{
			
			deleteExistingLicenses(customerBean.getCustomerId());
			session = sessionFactory.getCurrentSession();
				
			customerLicensing=new IrSCustomerlicensing();
			
			log.debug("customer id : "+customerBean.getCustomerId());
			irSCustomers = (IrSCustomers) session.get(IrSCustomers.class,customerBean.getCustomerId());
			ArrayList<IrSCustomerlicensing> customerLicenses = new ArrayList<IrSCustomerlicensing>();
			String ftypeList = customerBean.getFtypelist();
			
			if(ftypeList!=null&&!ftypeList.isEmpty())
			{
				String[] ftype = ftypeList.split(",");

				for (int i = 0; i < ftype.length; i++) 
				{
					fileType = (IrMFileTypes) session.get(IrMFileTypes.class, new Long(ftype[i]));

					customerLicensing=new IrSCustomerlicensing();

					customerLicensing.setFileType(fileType);
					
					branch.setBranchId(customerBean.getBankBranchId());
					customerLicensing.setIrSBankbranch(branch);
					customerLicensing.setIsactive(1);
					user.setUserid(customerBean.getUserId());
					customerLicensing.setIrMUsers(user);
					
					customerLicensing.setIrSCustomers(irSCustomers);

					customerLicenses.add(customerLicensing);

				}
			}
			for (Iterator iterator = customerLicenses.iterator(); iterator.hasNext();) {
				IrSCustomerlicensing irscustomerlicensing = (IrSCustomerlicensing) iterator.next();

				session.save(irscustomerlicensing);
			}
		
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
		}catch(Exception e){
			
				log.error("Exception in deleteBank",e);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
	
				throw new HibernateException(e);
			}
		
		
		return irPlusResponseDetails;
		}

	@Override
	public IRPlusResponseDetails addCustomerOtherLicense(CustomerBean customerBean) throws BusinessException {
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		Session session = null;
		IrSCustomers irSCustomers = null;
		IrMBusinessProcess businessProcess = null;
		IrSCustomerOtherlicensing irSCustomerOtherlicensing = null;
		IrSBankBranch branch = new IrSBankBranch();
		IrMUsers user = new IrMUsers();
		try{
			session = sessionFactory.getCurrentSession();
			irSCustomerOtherlicensing=new IrSCustomerOtherlicensing();
			ArrayList<IrSCustomerOtherlicensing> customerOtherLicenses = new ArrayList<IrSCustomerOtherlicensing>();
			log.debug("customer id : "+customerBean.getCustomerId());
			irSCustomers = (IrSCustomers) session.get(IrSCustomers.class,customerBean.getCustomerId());
			
			
			/*
			String bpList = customerBean.getBplist();

			if(bpList!=null&&!bpList.isEmpty())
			{
				String[] bpType = bpList.split(",");

				for (int i = 0; i < bpType.length; i++) 
				{
					businessProcess = (IrMBusinessProcess) session.get(IrMBusinessProcess.class, new Long(bpType[i]));
					irSCustomerOtherlicensing = new IrSCustomerOtherlicensing();
					
					irSCustomerOtherlicensing.setIrMBusinessprocess(businessProcess);
					branch.setBranchId(customerBean.getBankBranchId());
					irSCustomerOtherlicensing.setIrSBankBranch(branch);
					irSCustomerOtherlicensing.setIsactive(1);
					irSCustomerOtherlicensing.setIsAvail(1);
					user.setUserid(customerBean.getUserId());
					irSCustomerOtherlicensing.setIrMUsers(user);
					irSCustomerOtherlicensing.setIrSCustomers(irSCustomers);
					customerOtherLicenses.add(irSCustomerOtherlicensing);

				}

			}*/

			for (Iterator iterator = customerOtherLicenses.iterator(); iterator.hasNext();) {
				IrSCustomerOtherlicensing customerOtherlicensing = (IrSCustomerOtherlicensing) iterator.next();

				session.save(customerOtherlicensing);
			}
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
		}catch(Exception e){
			
			log.error("Exception in deleteBank",e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);

			throw new HibernateException(e);
		}
	
		return irPlusResponseDetails;
	}
	
	
	//delete existing license
	

	private void deleteExistingLicenses(int customerId) 
	{
		log.info("Inside deleteExistingLicenses customerId :"+customerId);

		Session session = null;

		try
		{
			session = sessionFactory.getCurrentSession();

			Query query = session.createQuery("delete from IrSCustomerlicensing custLic where custLic.irSCustomers.customerId=:customerId");

			query.setParameter("customerId",customerId);

			query.executeUpdate();
		}
		catch(Exception e)
		{
			log.error("Exception in deleteExistingLicenses",e);

			throw new HibernateException(e);
		}	

	}
	
	@Override
	public IRPlusResponseDetails AutocompleteBank(String term,String userid) throws BusinessException
	{
		Session session = null;
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		session = sessionFactory.getCurrentSession();
		BankInfo banklist = null;
		 String data;
		 List<BankInfo> list = new ArrayList<BankInfo>();
		 List<BankInfo> bankInfo=new ArrayList<BankInfo>();
		try {
			
			Query banks = session.createQuery("from IrMUsersBank userBank where userBank.irMUsers.userid=? and userBank.bank.bankName LIKE ? and userBank.bank.isactive!=?");
			banks.setParameter(0,new Integer(userid));
			banks.setParameter(1, term + "%");	
			banks.setParameter(2,2);	
			List<IrMUsersBank> userBankList=banks.list();
			
		int count=0;
		if(userBankList.size()>0){
				for(IrMUsersBank irMUsersBank:userBankList){
					banklist = new BankInfo();
				
					banklist.setBankName(irMUsersBank.getBank().getBankName());
					banklist.setBankId(irMUsersBank.getBank().getBankId());
					bankInfo.add(banklist);
					irPlusResponseDetails.setList(bankInfo);
				}
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
		}
		else
		{
			irPlusResponseDetails.setValidationSuccess(false);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
		}
		}	
	
	
		catch(Exception e)
		{
		
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			throw new HibernateException(e);
		}
		return irPlusResponseDetails;
		
		
	}
	
	@Override
	public IRPlusResponseDetails Autocomplete(String term,String userid) throws BusinessException
	{
		Session session = null;
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		session = sessionFactory.getCurrentSession();
		BankInfo banklist = null;
		 String data;
		 List<BankInfo> list = new ArrayList<BankInfo>();
		 List<BankInfo> bankInfo=new ArrayList<BankInfo>();
		try {
			
			Query banks = session.createQuery("from IrMUsersBank userBank where userBank.irMUsers.userid=? and userBank.isactive=? and userBank.irSBankbranch.bank.bankName LIKE ? AND userBank.irSBankbranch.bank.isactive=?");
			banks.setParameter(0,new Integer(userid));
			banks.setParameter(1,1);
			banks.setParameter(2, term + "%");	
			banks.setParameter(3,1);	
			List<IrMUsersBank> userBankList=banks.list();
			
		int count=0;
		if(userBankList.size()>0){
				for(IrMUsersBank irMUsersBank:userBankList){
					banklist = new BankInfo();
				
					banklist.setBankName(irMUsersBank.getIrSBankbranch().getBank().getBankName());
					banklist.setBankId(irMUsersBank.getIrSBankbranch().getBank().getBankId());
					banklist.setBranchId(irMUsersBank.getIrSBankbranch().getBranchId());
					bankInfo.add(banklist);
					irPlusResponseDetails.setList(bankInfo);
				}
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
		}
		else
		{
			irPlusResponseDetails.setValidationSuccess(false);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
		}
		}	
	
	
		catch(Exception e)
		{
		
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			throw new HibernateException(e);
		}
		return irPlusResponseDetails;
		
		
	}
	
	
	@Override
	public IRPlusResponseDetails AutocompleteCustomer(String term,String userid) throws BusinessException
	{
		Session session = null;
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		session = sessionFactory.getCurrentSession();
		CustomerBean custinfo = null;
		 
	
		 List<CustomerBean> customerlist=new ArrayList<CustomerBean>();
		try {
		
			Query banks = session.createQuery("from IrMUsersBank as userBank inner join userBank.bank.irSBankbranch.irSCustomers as cust where cust.companyName LIKE ? and userBank.irMUsers.userid=? and userBank.isactive=? and cust.isactive=?");
			banks.setParameter(0,term + "%");
			banks.setParameter(1,new Integer(userid));
			banks.setParameter(2,1);
			banks.setParameter(3,1);
			
			List<?> userBankList=banks.list();
			
			int count=0;
			if(userBankList.size()>0){
				
			     for(int m=0; m<userBankList.size(); m++) {
			    	 custinfo= new CustomerBean();
						
						Object[] roww = (Object[]) userBankList.get(m);
						IrSCustomers cust = (IrSCustomers)roww[1];
						custinfo.setCompanyName(cust.getCompanyName());
						custinfo.setBankCustomerCode(cust.getBankCustomerCode());
						customerlist.add(custinfo);
			     }
			 	irPlusResponseDetails.setCustomerBean(customerlist);
			 	irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			}
	
			else
			{
				irPlusResponseDetails.setValidationSuccess(false);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
			}		
			
			
		}	
		catch(Exception e)
		{
		
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			throw new HibernateException(e);
		}
		return irPlusResponseDetails;
		
		
	}
	
	
	@Override
	public IRPlusResponseDetails AutocompleteCustomerCode(String term,String userid) throws BusinessException
	{
		Session session = null;
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		session = sessionFactory.getCurrentSession();
		CustomerBean custinfo = null;
		 
	
		 List<CustomerBean> customerlist=new ArrayList<CustomerBean>();
		try {
		
			
			Query banks = session.createQuery("from IrMUsersBank as userBank inner join userBank.irSBankbranch.irSCustomers as cust where cust.bankCustomerCode LIKE ? and userBank.irMUsers.userid=? and userBank.isactive=? and cust.isactive=?");
			banks.setParameter(0, term + "%");
			banks.setParameter(1,new Integer(userid));
			banks.setParameter(2,1);
			banks.setParameter(3,1);
			
			List<?> userBankList=banks.list();
			
			int count=0;
			if(userBankList.size()>0){
				
			     for(int m=0; m<userBankList.size(); m++) {
			    	 custinfo= new CustomerBean();
						
						Object[] roww = (Object[]) userBankList.get(m);
						IrSCustomers cust = (IrSCustomers)roww[1];
						
						custinfo.setBankCustomerCode(cust.getBankCustomerCode());
						customerlist.add(custinfo);
			     }
			 	irPlusResponseDetails.setCustomerBean(customerlist);
			 	irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			}
	
			else
			{
				irPlusResponseDetails.setValidationSuccess(false);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
			}		
			
			
		}	
		catch(Exception e)
		{
		
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			throw new HibernateException(e);
		}
		return irPlusResponseDetails;
		
		
	}
	

	private void getCustomerLicenses(CustomerBean customerBean)
	{
		log.info("Inside getCustomerLicenses custId :"+customerBean.getCustomerId());

		Session session = null;

		List<IrSCustomerlicensing> licenseList = null;

		IrSCustomerlicensing bankLicense = null;

		try
		{
			session = sessionFactory.getCurrentSession();

			Query query = session.createQuery("FROM IrSCustomerlicensing cl Where cl.irSCustomers.customerId=?");

			query.setParameter(0,customerBean.getCustomerId());

			licenseList = query.list();

			Iterator <IrSCustomerlicensing> licenseListIt = licenseList.iterator();

			

			String ftypeList="";

			while (licenseListIt.hasNext()) {
				bankLicense = licenseListIt.next();
				
				if(bankLicense.getFileType()!=null)
				{
					ftypeList +=ftypeList+bankLicense.getFileType().getFiletypeid()+",";
				}

			}
			
			if(!ftypeList.isEmpty())
			{
				ftypeList = ftypeList.substring(0, ftypeList.length()-1);
				customerBean.setFtypelist(ftypeList);
			}

		}
		catch(Exception e)
		{
			log.error("Exception in deleteExistingLicenses",e);

			throw new HibernateException(e);
		}	
	}

	@Override	
	public IRPlusResponseDetails AddCustomerDataEntry(FormsManagementDTO formsManagement) throws BusinessException {
		
		log.info("Inside AddCustomerDataEntry :: createCustomerMappingGroup");
		Session session = null;
		
		session = sessionFactory.getCurrentSession();
		List<String> errMsgs = new ArrayList<String>();
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		
		
		
		IrMUsers user = new IrMUsers();
		IrSCustomers customer = new IrSCustomers();
	//	IrSCustomercontacts irSCustomercontacts = new IrSCustomercontacts();
		try{
		
			Date date = new Date();
		    DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
		    DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
		    String currentDate = dateFormat.format(date);
		    String currentTime = timeFormat.format(date);
		   
		    IrSFormsetup formsetup = new IrSFormsetup();
			
		    formsetup.setFormCode(formsManagement.getFormCode());
		    formsetup.setFormName(formsManagement.getFormName());
		    formsetup.setWatchFolderDataentry(formsManagement.getWatchFolderDataentry());
		    formsetup.setBatchClassDataentry(formsManagement.getBatchClassDataentry());
		    formsetup.setIsactive(formsManagement.getIsactive());
			user.setUserid(formsManagement.getUserid());
			formsetup.setIrMUsers(user);
			customer.setCustomerId(formsManagement.getCustomerid());
			formsetup.setIrSCustomers(customer);

				
				Integer formsetupId=(Integer)session.save(formsetup);
			
				formsManagement.getDataEntryFormValidationDTO()[0].setFormsetupid(formsetupId);
							session.flush();
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			
		
		}catch (Exception e) {
				log.error("Exception raised in create Customer group");
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				throw new HibernateException(e);
		}
			

	
	return irPlusResponseDetails;
}
	
	
	@Override
	public IRPlusResponseDetails AddCustomerDataEntryDetails(DataEntryFormValidationDTO[] dataentryform) throws BusinessException {
		

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		Session session = null;
		
		session = sessionFactory.getCurrentSession();
		IrMUsers user = new IrMUsers();
		IrSBankBranch bank = new IrSBankBranch();
		IrMFieldtypes field = new IrMFieldtypes();
		IrSCustomers customer = new IrSCustomers();
	
		IrSFormsetup formvalidation=null;
		
		try
		{
			Date date = new Date();
		    DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
		    DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
		    String currentDate = dateFormat.format(date);
		    String currentTime = timeFormat.format(date);
		    
			if(dataentryform.length>0)
			{
				formvalidation = (IrSFormsetup) session.get(IrSFormsetup.class,dataentryform[0].getFormsetupid());
			

			}
			for(int i=0;i<dataentryform.length;i++)
			{
	
				
				log.info("rfwrefwrewre");
				log.info("yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy"+dataentryform[i].getFieldLength());
				log.info("ewrrewrerrrrvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvrrrrrrrrrrrrr"+dataentryform[i].getFieldtypid());
				IrSFormvalidation formvalidDetails = new IrSFormvalidation(dataentryform[i]);
				
				formvalidDetails.setIrSFormsetup(formvalidation);

				formvalidDetails.setFieldName(dataentryform[i].getFieldName());
			/*	formvalidDetails.setFieldName(dataentryform[i].getFieldtypid());*/
				/*field.setFieldTypeId(dataentryform[i].getFieldtypid());			
				formvalidDetails.setIrMFieldtypes(field);*/
			
				formvalidDetails.setFieldLength(dataentryform[i].getFieldLength());
				user.setUserid(dataentryform[i].getUserId());			
				formvalidDetails.setIrMUsers(user);
				session.save(formvalidDetails);
				session.flush();
				log.debug("Added custGrpingDetais Info :"+formvalidDetails);
			}
		
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
		
		}catch(Exception e)
		{
			log.error("Exception in addContact",e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			throw new HibernateException(e);
		}

		
		return irPlusResponseDetails;
	}
	
	
	
	
	
	
	
}