package com.irplus.dao.fileprocessing.fileheaderrecord;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.irplus.dao.hibernate.entities.IRSBank;
import com.irplus.dao.hibernate.entities.IRSSite;
import com.irplus.dao.hibernate.entities.IrFPBatchHeaderRecord;

import com.irplus.dao.hibernate.entities.IrFPFileControlRecord;
import com.irplus.dao.hibernate.entities.IrFPFileHeaderRecord;
import com.irplus.dao.hibernate.entities.IrMBusinessProcess;
import com.irplus.dao.hibernate.entities.IrMFileTypes;
import com.irplus.dao.hibernate.entities.IrFFileAudit;
import com.irplus.dao.hibernate.entities.IrMUsers;
import com.irplus.dao.hibernate.entities.IrMUsersBank;
import com.irplus.dao.hibernate.entities.IrSCustomers;
import com.irplus.dto.BankwiseFileprocess;
import com.irplus.dto.BusinessProcessInfo;
import com.irplus.dto.CustomerGroupingMngmntInfo;
import com.irplus.dto.FileAudit;
import com.irplus.dto.FileTypeInfo;
import com.irplus.dto.IRMasterDataDao;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.Sequence;
import com.irplus.dto.fileprocessing.BatchHeaderRecordBean;

import com.irplus.dto.fileprocessing.FileControlRecordBean;
import com.irplus.dto.fileprocessing.FileHeaderRecordBean;

import com.irplus.util.BusinessException;
import com.irplus.util.IRPlusConstants;


@Component
@Transactional
public class FileHeaderRecordDaoImpl implements FileHeaderRecordDao{
	
	private static final Logger LOGGER = Logger.getLogger(FileHeaderRecordDaoImpl.class);
	
	@Autowired
	private SessionFactory sessionFactory;

	public Session getMyCurrentSession(){
			
			Session session =null;
			
			try {	
			
				session=sessionFactory.getCurrentSession();
			
			}catch (HibernateException e) {
				LOGGER.error("Exception raised DaoImpl :: At current session creation time ::"+e);
			}		
			return session; 
		}

	@Override
	public Boolean insertRecord(FileHeaderRecordBean fileHeaderRecord) {
	   
		
		return null;
	}

	@Override
	public Boolean deleteRecord(FileHeaderRecordBean fileHeaderRecord) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IRPlusResponseDetails listAllRecords(String userId) throws BusinessException{
		LOGGER.info("Inside FileHeaderRecordDaoImpl");
		LOGGER.debug("Show all FileHeaderRecordDaoImpl instances");
		
		IRPlusResponseDetails irPlusResponseDetails = new  IRPlusResponseDetails();
	
		
		try {
			
			int userid = Integer.parseInt(userId);
			FileHeaderRecordBean fhrBean=null;
			FileControlRecordBean fcrBean = null;
			List<FileHeaderRecordBean> fhrList = new ArrayList<FileHeaderRecordBean>();
			IRSBank bank = null;
			
			
			Query banks = getMyCurrentSession().createQuery("from IrMUsersBank userBank where userBank.irMUsers.userid=? and userBank.isactive=?");
			banks.setParameter(0,userid);
			banks.setParameter(1,1);
			List<IrMUsersBank> userBankList=banks.list();
			
			
			if(userBankList.size()>0){
				for(IrMUsersBank irMUsersBank:userBankList){
					Query file = getMyCurrentSession().createQuery("from IrFPFileHeaderRecord fileInfo where fileInfo.bank.bankId=?");
					file.setParameter(0,irMUsersBank.getBank().getBankId());
					List<IrFPFileHeaderRecord> list = file.list();
					
					if(list.size()>0){
						for(IrFPFileHeaderRecord irFPFileHeaderRecord:list) {
							
								fhrBean = new FileHeaderRecordBean();
							
							fhrBean.setFileHeaderRecordId(irFPFileHeaderRecord.getFileHeaderRecordId());
							fhrBean.setRecordTypeCode(irFPFileHeaderRecord.getRecordTypeCode());
							fhrBean.setPriorityCode(irFPFileHeaderRecord.getPriorityCode());
							fhrBean.setImmediateDestination(irFPFileHeaderRecord.getImmediateDestination());
							fhrBean.setImmediateOrigin(irFPFileHeaderRecord.getImmediateOrigin());
							fhrBean.setFileCreationDate(irFPFileHeaderRecord.getFileCreationDate());
							fhrBean.setFileCreationTime(irFPFileHeaderRecord.getFileCreationTime());
							fhrBean.setFileIdModifier(irFPFileHeaderRecord.getFileIdModifier());
							fhrBean.setRecordSize(irFPFileHeaderRecord.getRecordSize());
							fhrBean.setBlockingFactor(irFPFileHeaderRecord.getBlockingFactor());
							fhrBean.setFormatCode(irFPFileHeaderRecord.getFormatCode());
							fhrBean.setImmediateDestinationName(irFPFileHeaderRecord.getImmediateDestinationName());
							fhrBean.setImmediateOriginName(irFPFileHeaderRecord.getImmediateOriginName());
							fhrBean.setAchFileName(irFPFileHeaderRecord.getAchFileName());
							fhrBean.setProcessedDate(irFPFileHeaderRecord.getProcessedDate());
							fhrBean.setProcessedTime(irFPFileHeaderRecord.getProcessedTime());
							fhrBean.setIsProcessed(irFPFileHeaderRecord.getIsProcessed());
							fhrBean.setBankName(irFPFileHeaderRecord.getBank().getBankName());
							fhrBean.setFileType(irFPFileHeaderRecord.getFileType().getFiletypename());
							
							fhrList.add(fhrBean);
				}
					}
							}
			
				irPlusResponseDetails.setAllFileList(fhrList);
				//irPlusResponseDetails.setFhrBean(fhrBean);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			} else {
				LOGGER.debug("FileHeaderRecord not available :: Empty Data :: Data required to showAll FileHeaderRecord");
				irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
			}
			
			
		}catch(Exception e) {
			LOGGER.error("Exception raised in show list FileHeaderRecord",e);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			
			throw new HibernateException(e);
			
		}
		
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails getRecordByBatchId(Integer fileId) throws BusinessException{
		LOGGER.info("Inside FileHeaderRecordDaoImpl");
		LOGGER.debug("GetBatchByFIleID FileHeaderRecordDaoImpl instances");
		
		
		IRPlusResponseDetails irPlusResponseDetails = new  IRPlusResponseDetails();
		List<BatchHeaderRecordBean> batchHeaderRecordBean = new ArrayList<BatchHeaderRecordBean>();

		try {
			
			List<FileHeaderRecordBean> fileHeaderRecordBean = new ArrayList<FileHeaderRecordBean>();
			IrFPFileHeaderRecord fhrList = (IrFPFileHeaderRecord)getMyCurrentSession().get(IrFPFileHeaderRecord.class,fileId);
			
			//List<IrFPFileHeaderRecord> list = criteria.list();
			
			if (fhrList!=null) {
				
							
					FileHeaderRecordBean fhrBean = new FileHeaderRecordBean();
					
					fhrBean.setFileHeaderRecordId(fhrList.getFileHeaderRecordId());
					fhrBean.setRecordTypeCode(fhrList.getRecordTypeCode());
					fhrBean.setPriorityCode(fhrList.getPriorityCode());
					fhrBean.setImmediateDestination(fhrList.getImmediateDestination());
					fhrBean.setImmediateOrigin(fhrList.getImmediateOrigin());
					fhrBean.setFileCreationDate(fhrList.getFileCreationDate());
					fhrBean.setFileCreationTime(fhrList.getFileCreationTime());
					fhrBean.setFileIdModifier(fhrList.getFileIdModifier());
					fhrBean.setRecordSize(fhrList.getRecordSize());
					fhrBean.setBlockingFactor(fhrList.getBlockingFactor());
					fhrBean.setFormatCode(fhrList.getFormatCode());
					fhrBean.setImmediateDestinationName(fhrList.getImmediateDestinationName());
					fhrBean.setImmediateOriginName(fhrList.getImmediateOriginName());
					fhrBean.setAchFileName(fhrList.getAchFileName());
					fhrBean.setProcessedDate(fhrList.getProcessedDate());
					fhrBean.setProcessedTime(fhrList.getProcessedTime());
					fhrBean.setIsProcessed(fhrList.getIsProcessed());
					fhrBean.setFileType(fhrList.getFileType().getFiletypename());
					
					
					System.out.println(fhrList.getAchFileName() + " has " +
							fhrList.getIrFPBatchHeaderRecord().size() +
						    " Batches");

					Query query = getMyCurrentSession().createQuery("from IrFPBatchHeaderRecord where irFPFileHeaderRecord.fileHeaderRecordId=:fhrId ");
					
					query.setParameter("fhrId",fileId);
					
					List<IrFPBatchHeaderRecord> bhrList=query.list();
					
					for (IrFPBatchHeaderRecord irFPBatchHeaderRecord:bhrList) {
						BatchHeaderRecordBean bhrBean = new BatchHeaderRecordBean();
						
						bhrBean.setBatchHeaderRecordId(irFPBatchHeaderRecord.getBatchHeaderRecordId());
						//bhrBean.setServiceClassCode(irFPBatchHeaderRecord.getServiceClassCode());
						//bhrBean.setCompanyName(irFPBatchHeaderRecord.getCompanyName());
						//bhrBean.setDiscretionaryData(irFPBatchHeaderRecord.getDiscretionaryData());
						//bhrBean.setCompanyIdentification(irFPBatchHeaderRecord.getCompanyIdentification());
						bhrBean.setStandardEntryClass(irFPBatchHeaderRecord.getStandardEntryClass()); 
						//bhrBean.setCompanyEntryDescription(irFPBatchHeaderRecord.getCompanyEntryDescription());
						bhrBean.setCompanyDescriptiveDate(irFPBatchHeaderRecord.getCompanyDescriptiveDate());
						bhrBean.setEffectiveEntryDate(irFPBatchHeaderRecord.getEffectiveEntryDate());
						//bhrBean.setReserved(irFPBatchHeaderRecord.getReserved());
						//bhrBean.setOrginatorStatusCode(irFPBatchHeaderRecord.getOrginatorStatusCode());
					//	bhrBean.setOrginatingFinancialInstitution(irFPBatchHeaderRecord.getOrginatingFinancialInstitution());
						bhrBean.setBatchNumber(irFPBatchHeaderRecord.getBatchNumber());
						bhrBean.setCustomerName(irFPBatchHeaderRecord.getCustomer().getCompanyName());
						bhrBean.setBatchAmount(irFPBatchHeaderRecord.getBatchAmount());
						bhrBean.setTransactionCount(irFPBatchHeaderRecord.getTransactionCount());
						bhrBean.setItemCount(irFPBatchHeaderRecord.getItemCount());
						bhrBean.setPaymentCount(irFPBatchHeaderRecord.getPaymentCount());
						bhrBean.setRemittanceCount(irFPBatchHeaderRecord.getRemittanceCount());
						bhrBean.setScandocCount(irFPBatchHeaderRecord.getScandocCount());
						bhrBean.setCustomerName(irFPBatchHeaderRecord.getCustomer().getCompanyName());
						batchHeaderRecordBean.add(bhrBean);
						
						fhrBean.setBatchHeaderRecordBean(batchHeaderRecordBean);
						//bhrBean.setFileHeaderRecordBean(fhrBean);
						//fhrBean.getBatchHeaderRecordBean().add(bhrBean);
					}
					
					fileHeaderRecordBean.add(fhrBean);
					
				
				irPlusResponseDetails.setAllFileList(fileHeaderRecordBean);
				//irPlusResponseDetails.setFhrBean(fhrBean);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			} else {
				LOGGER.debug("FileHeaderRecord not available :: Empty Data :: Data required to showAll FileHeaderRecord");
				irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
			}
			
		}catch(Exception e) {
			LOGGER.error("Exception raised in show list FileHeaderRecord",e);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			
			throw new HibernateException(e);
			
		}
		return irPlusResponseDetails;
	}

	@Override
	public Boolean updateRecord(FileHeaderRecordBean fileHeaderRecord) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IRPlusResponseDetails bankWiseAllFiles() throws BusinessException {
		IRPlusResponseDetails irPlusResponseDetails = new  IRPlusResponseDetails();
		
		try{
			FileHeaderRecordBean fhrBean=null;
			BankwiseFileprocess bankwiseFileprocess = null;
			List<BankwiseFileprocess> allList = new ArrayList<BankwiseFileprocess>();
			List<FileHeaderRecordBean> fhrList = new ArrayList<FileHeaderRecordBean>();
			
		//	working Query fileQuery = getMyCurrentSession().createQuery("select count(fhr.fileHeaderRecordId),sum(fcr.batchCount),sum(fcr.entryAddendaCount),SUM(fcr.totalDebitEntryDollarAmountInFile),SUM(fcr.totalcreditEntryDollarAmountInFile) from IrFPFileControlRecord fcr inner join fcr.irFPFileHeaderRecord fhr");
			
			
			//Query fileQuery = getMyCurrentSession().createQuery("SELECT fhr.bank.bankId,COUNT(fhr.fileHeaderRecordId),SUM(fcr.batchCount),sum(fcr.entryAddendaCount),SUM(fcr.totalDebitEntryDollarAmountInFile),SUM(fcr.totalcreditEntryDollarAmountInFile) FROM IrFPFileHeaderRecord fhr INNER JOIN fhr.irFPFileControlRecord fcr GROUP BY fhr.bank.bankId");
			
			Query fileQuery = getMyCurrentSession().createQuery("SELECT fhr.bank.bankId,fhr.bank.bankName,COUNT(fhr.fileHeaderRecordId),SUM(fcr.batchCount),sum(fcr.entryAddendaCount),SUM(fcr.totalDebitEntryDollarAmountInFile),SUM(fcr.totalcreditEntryDollarAmountInFile) FROM IrFPFileHeaderRecord fhr INNER JOIN fhr.irFPFileControlRecord fcr GROUP BY fhr.bank.bankId,fhr.bank.bankName");
			//SQLQuery fileQuery = getMyCurrentSession().createSQLQuery("select ir_f_file_header_record.bank_id,count(ir_f_file_header_record.file_header_record_id)as totalFiles,sum(batch_count) as batchCount,sum(entry_addenda_count) as entrycount,SUM(total_debit_entry_dollar_amount_in_file) as totaldebit ,SUM(total_credit_entry_dollar_amount_in_file) as totalcredit from ir_f_file_control_record inner join ir_f_file_header_record on ir_f_file_control_record.file_header_record_id=ir_f_file_header_record.file_header_record_id group by ir_f_file_header_record.bank_id");
			List<?> list = fileQuery.list();
			if(list.size()>0){
			for(int i=0; i<list.size(); i++) {
				Object[] row = (Object[]) list.get(i);
			//	IrFPFileHeaderRecord irFhr = (IrFPFileHeaderRecord)row[0];
				bankwiseFileprocess = new BankwiseFileprocess();
				
				
				LOGGER.debug("value from db "+row[0]);;
				bankwiseFileprocess.setBankId((Long) row[0]);
				bankwiseFileprocess.setBankName((String)row[1]);
				bankwiseFileprocess.setTotalFiles(((Long) row[2]));
				bankwiseFileprocess.setBatchCount((Long)row[3]);
				bankwiseFileprocess.setEntryCount((Long)row[4]);
				bankwiseFileprocess.setTotalDebit((double)row[5]);
				bankwiseFileprocess.setTotalCredit((double)row[6]);
					
				allList.add(bankwiseFileprocess);
				
					
				}
				irPlusResponseDetails.setBankWiseFileProcess(allList);
				//irPlusResponseDetails.setFhrBean(fhrBean);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			}
		}catch(Exception e) {
			LOGGER.error("Exception raised in show list FileHeaderRecord",e);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			
			throw new HibernateException(e);
		}
		return irPlusResponseDetails;
	}
	@Override
	public IRPlusResponseDetails customerWiseAllFiles() throws BusinessException {
		IRPlusResponseDetails irPlusResponseDetails = new  IRPlusResponseDetails();
		
		try{
			FileHeaderRecordBean fhrBean=null;
			BankwiseFileprocess bankwiseFileprocess = null;
			List<BankwiseFileprocess> allList = new ArrayList<BankwiseFileprocess>();
			List<FileHeaderRecordBean> fhrList = new ArrayList<FileHeaderRecordBean>();
			
			
		//	working Query fileQuery = getMyCurrentSession().createQuery("select count(fhr.fileHeaderRecordId),sum(fcr.batchCount),sum(fcr.entryAddendaCount),SUM(fcr.totalDebitEntryDollarAmountInFile),SUM(fcr.totalcreditEntryDollarAmountInFile) from IrFPFileControlRecord fcr inner join fcr.irFPFileHeaderRecord fhr");
			
			
			//Query fileQuery = getMyCurrentSession().createQuery("SELECT fhr.bank.bankId,COUNT(fhr.fileHeaderRecordId),SUM(fcr.batchCount),sum(fcr.entryAddendaCount),SUM(fcr.totalDebitEntryDollarAmountInFile),SUM(fcr.totalcreditEntryDollarAmountInFile) FROM IrFPFileHeaderRecord fhr INNER JOIN fhr.irFPFileControlRecord fcr GROUP BY fhr.bank.bankId");
			
			Query fileQuery = getMyCurrentSession().createQuery("SELECT fhr.bank.bankId,fhr.bank.bankName,COUNT(fhr.fileHeaderRecordId),SUM(fcr.batchCount),sum(fcr.entryAddendaCount),SUM(fcr.totalDebitEntryDollarAmountInFile),SUM(fcr.totalcreditEntryDollarAmountInFile) FROM IrFPFileHeaderRecord fhr INNER JOIN fhr.irFPFileControlRecord fcr GROUP BY fhr.bank.bankId,fhr.bank.bankName");
			//SQLQuery fileQuery = getMyCurrentSession().createSQLQuery("select ir_f_file_header_record.bank_id,count(ir_f_file_header_record.file_header_record_id)as totalFiles,sum(batch_count) as batchCount,sum(entry_addenda_count) as entrycount,SUM(total_debit_entry_dollar_amount_in_file) as totaldebit ,SUM(total_credit_entry_dollar_amount_in_file) as totalcredit from ir_f_file_control_record inner join ir_f_file_header_record on ir_f_file_control_record.file_header_record_id=ir_f_file_header_record.file_header_record_id group by ir_f_file_header_record.bank_id");
			List<?> list = fileQuery.list();
			if(list.size()>0){
			for(int i=0; i<list.size(); i++) {
				Object[] row = (Object[]) list.get(i);
			//	IrFPFileHeaderRecord irFhr = (IrFPFileHeaderRecord)row[0];
				bankwiseFileprocess = new BankwiseFileprocess();
				
				
				LOGGER.debug("value from db "+row[0]);;
				bankwiseFileprocess.setBankId((Long) row[0]);
				bankwiseFileprocess.setBankName((String)row[1]);
				bankwiseFileprocess.setTotalFiles(((Long) row[2]));
				bankwiseFileprocess.setBatchCount((Long)row[3]);
				bankwiseFileprocess.setEntryCount((Long)row[4]);
				bankwiseFileprocess.setTotalDebit((double)row[5]);
				bankwiseFileprocess.setTotalCredit((double)row[6]);
					
				allList.add(bankwiseFileprocess);
				
					
				}
				irPlusResponseDetails.setBankWiseFileProcess(allList);
				//irPlusResponseDetails.setFhrBean(fhrBean);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			}
		}catch(Exception e) {
			LOGGER.error("Exception raised in show list FileHeaderRecord",e);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			
			throw new HibernateException(e);
		}
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails getFilesByBankId(String bankId) throws BusinessException {
		IRPlusResponseDetails irPlusResponseDetails = new  IRPlusResponseDetails();
	try{	
		FileHeaderRecordBean fhrBean=null;
		FileControlRecordBean fcrBean = null;
		List<FileControlRecordBean> fcrList = new ArrayList<FileControlRecordBean>();
		List<FileHeaderRecordBean> fhrList = new ArrayList<FileHeaderRecordBean>();
		IRSBank bank = null;
		
		Query file = getMyCurrentSession().createQuery("from IrFPFileHeaderRecord as fhr inner join fhr.irFPFileControlRecord as fcr where fhr.bank.bankId=?");
		file.setParameter(0,new Long(bankId));
		
		//List<IrFPFileHeaderRecord> fileList= file.list();
		
		List<?> list = file.list();
		if(list.size()>0){
		for(int i=0; i<list.size(); i++) {
			Object[] row = (Object[]) list.get(i);
			IrFPFileHeaderRecord irFhr = (IrFPFileHeaderRecord)row[0];
			IrFPFileControlRecord irFcr = (IrFPFileControlRecord)row[1];
			System.out.println("file Id:"+irFhr.getFileHeaderRecordId()+", fileName:"+ irFhr.getAchFileName()+
					   ", batch count:"+ irFcr.getBatchCount()+", entry Count:"+ irFcr.getEntryAddendaCount());
			fhrBean = new FileHeaderRecordBean();
			fcrBean = new FileControlRecordBean();
			fhrBean.setFileHeaderRecordId(irFhr.getFileHeaderRecordId());
			fhrBean.setRecordTypeCode(irFhr.getRecordTypeCode());
			fhrBean.setPriorityCode(irFhr.getPriorityCode());
			fhrBean.setImmediateDestination(irFhr.getImmediateDestination());
			fhrBean.setImmediateOrigin(irFhr.getImmediateOrigin());
			fhrBean.setFileCreationDate(irFhr.getFileCreationDate());
			fhrBean.setFileCreationTime(irFhr.getFileCreationTime());
			fhrBean.setFileIdModifier(irFhr.getFileIdModifier());
			fhrBean.setRecordSize(irFhr.getRecordSize());
			fhrBean.setBlockingFactor(irFhr.getBlockingFactor());
			fhrBean.setFormatCode(irFhr.getFormatCode());
			fhrBean.setImmediateDestinationName(irFhr.getImmediateDestinationName());
			fhrBean.setImmediateOriginName(irFhr.getImmediateOriginName());
			fhrBean.setAchFileName(irFhr.getAchFileName());
			fhrBean.setProcessedDate(irFhr.getProcessedDate());
			fhrBean.setProcessedTime(irFhr.getProcessedTime());
			fhrBean.setIsProcessed(irFhr.getIsProcessed());
			fhrBean.setBankName(irFhr.getBank().getBankName());
			
			fcrBean.setBatchCount(irFcr.getBatchCount());
			fcrBean.setEntryAddendaCount(irFcr.getEntryAddendaCount());
			fcrBean.setTotalDebitEntryDollarAmountInFile(irFcr.getTotalDebitEntryDollarAmountInFile());
			fcrBean.setTotalcreditEntryDollarAmountInFile(irFcr.getTotalcreditEntryDollarAmountInFile());
			//fcrList.add(fcrBean);
			
			fhrBean.getFileControlRecordBean().add(fcrBean);
			
			fhrList.add(fhrBean);
			
		}
			irPlusResponseDetails.setAllFileList(fhrList);
			//irPlusResponseDetails.setFhrBean(fhrBean);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
		} else {
			LOGGER.debug("FileHeaderRecord not available :: Empty Data :: Data required to showAll FileHeaderRecord");
			irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
		}
	}catch(Exception e) {
		LOGGER.error("Exception raised in show list FileHeaderRecord",e);
		irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
		
		throw new HibernateException(e);
	}
	return irPlusResponseDetails;
	
	}
	
	public IRPlusResponseDetails getMasterDataChart(Integer userId,String siteId) throws BusinessException 
	{
		LOGGER.info("Inside IRPlusDao");

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		Session session = null;

		session = sessionFactory.getCurrentSession();
		IRSSite iRSSite=null;
		Long licenseId = null;
		IrMBusinessProcess businessProcess = null;
		
		
		IrMFileTypes fileTypesData = null;

		BusinessProcessInfo processInfo = null;

		List<BusinessProcessInfo> processList = new ArrayList<BusinessProcessInfo>();
		FileTypeInfo fileTypeInfoss = null;
		FileTypeInfo fileTypeInfo = null;
		FileTypeInfo fileTypeInfos = null;
		List<FileTypeInfo> fileTypes = new ArrayList<FileTypeInfo>();
		List<FileTypeInfo> fileTypesss = new ArrayList<FileTypeInfo>();
		List<FileTypeInfo> lockfileTypessss = new ArrayList<FileTypeInfo>();
		List<Sequence> bankSequence = new ArrayList<Sequence>();

		IRMasterDataDao masterData = new IRMasterDataDao();

		try
		{
		
			iRSSite = (IRSSite) session.get(IRSSite.class,new Long(siteId));
			
			if(iRSSite!=null){
				
				licenseId = iRSSite.getLicense().getLicenseId();
			}
			
			if(licenseId!=null){

				Query fTypes = session.createQuery("FROM IrMFileTypes fileTypes WHERE fileTypes.iRSLicense.licenseId=? and fileTypes.isactive=?");
				fTypes.setParameter(0,licenseId);
				fTypes.setParameter(1,1);	
				List<IrMFileTypes> fileTypeList = fTypes.list();
				
				if(fileTypeList.size()>0){
					for(int i=0; i<fileTypeList.size(); i++) {
						
						fileTypeInfo = new FileTypeInfo();

						fileTypeInfo.setFiletypeid(fileTypeList.get(i).getFiletypeid());

						fileTypeInfo.setFiletypename(fileTypeList.get(i).getFiletypename());
					
					fileTypes.add(fileTypeInfo);

					if(fileTypeList.get(i).getFiletypename().equals("ACH"))
					{
					
						for(int month=1;month<=12;month++)
						{
						
						int monthvalues=month;
						
						Query transactions = session.createQuery("from IrMUsersBank as user inner join user.bank as branch inner join branch.irFPFileHeaderRecord as fhr inner join fhr.irFPBatchHeaderRecord as fbr  where  MONTH(fhr.processedDate) IN(:monthval) and   user.irMUsers.userid=:userId and user.isactive=:isactive and fhr.fileType.filetypeid=:filetypeId"); 
							
						transactions.setParameter("isactive",1);
							transactions.setParameter("userId",userId);
							transactions.setParameter("filetypeId",fileTypeList.get(i).getFiletypeid());
							transactions.setParameter("monthval",monthvalues);
							List<?> transactionsList = transactions.list();
							
							 String one="";
							 fileTypeInfos = new FileTypeInfo();
							if(transactionsList.size()>0){	
								
								int transcount=0;
							
							for(int k=0; k<transactionsList.size(); k++) {
							
								Object[] row = (Object[]) transactionsList.get(k);
								IrFPFileHeaderRecord irFhr = (IrFPFileHeaderRecord)row[2];
								IrFPBatchHeaderRecord irBcr = (IrFPBatchHeaderRecord)row[3];
								transcount+=Integer.parseInt(irBcr.getTransactionCount());
							}
						
							fileTypeInfos.setMonthlyTrans(transcount);
							fileTypeInfos.setMonthvalue(monthvalues);
							}
							else {
								int transcount=0;
						
								fileTypeInfos.setMonthlyTrans(transcount);
								fileTypeInfos.setMonthvalue(monthvalues);
								
							}
							fileTypesss.add(fileTypeInfos);
						}
					}
					if(fileTypeList.get(i).getFiletypename().equals("Lockbox"))
					{
						for(int month=1;month<=12;month++)
						{
						int monthvalues=month;
						Query transactions = session.createQuery("from IrMUsersBank as user inner join user.bank as branch inner join branch.irFPFileHeaderRecord as fhr inner join fhr.irFPBatchHeaderRecord as fbr  where  MONTH(fhr.processedDate) IN(:monthval) and   user.irMUsers.userid=:userId and user.isactive=:isactive and fhr.fileType.filetypeid=:filetypeId"); 
						transactions.setParameter("isactive",1);
						transactions.setParameter("userId",userId);
							transactions.setParameter("filetypeId",fileTypeList.get(i).getFiletypeid());
							transactions.setParameter("monthval",monthvalues);
		
							List<?> transactionsList = transactions.list();
							
							 String one="";
							 fileTypeInfoss = new FileTypeInfo();
							if(transactionsList.size()>0){	
								
								int transcount=0;
							
							for(int k=0; k<transactionsList.size(); k++) {
							
								Object[] row = (Object[]) transactionsList.get(k);
								IrFPFileHeaderRecord irFhr = (IrFPFileHeaderRecord)row[2];
								IrFPBatchHeaderRecord irBcr = (IrFPBatchHeaderRecord)row[3];

								
								transcount+=Integer.parseInt(irBcr.getTransactionCount());
							}
						
							fileTypeInfoss.setMonthlyTrans(transcount);
							fileTypeInfoss.setMonthvalue(monthvalues);
							}
							else {
								int transcount=0;
						
								fileTypeInfoss.setMonthlyTrans(transcount);
								fileTypeInfoss.setMonthvalue(monthvalues);
								
							}
							lockfileTypessss.add(fileTypeInfoss);
						}
					}	
					masterData.setLockfileTypesInfo(lockfileTypessss);

					masterData.setFileTypesInfo(fileTypesss);

			masterData.setFileTypes(fileTypes);

			irPlusResponseDetails.setMasterData(masterData);

		}
				}
			}
			
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
		}
		catch(Exception e)
		{
			LOGGER.error("Exception in getMasterData",e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			throw new HibernateException(e);
		}

		return irPlusResponseDetails;
	}
	
	
	@Override
	public IRPlusResponseDetails DaywiseFiles(CustomerGroupingMngmntInfo fileheadrec) throws BusinessException{
		LOGGER.info("Inside FileHeaderRecordDaoImplss");
		LOGGER.debug("Show all FileHeaderRecordDaoImpl instances");
		
		IRPlusResponseDetails irPlusResponseDetails = new  IRPlusResponseDetails();
	
		
		try {
			
			FileHeaderRecordBean fhrBean=null;
			FileControlRecordBean fcrBean = null;
			List<FileHeaderRecordBean> fhrList = new ArrayList<FileHeaderRecordBean>();
			IRSBank bank = null;
			
			 String betweendate="";
			 String fromDate1="";
			 String ToDate1="";
		     DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
		     betweendate=fileheadrec.getCurrentdate();
		     String[] dateParts = betweendate.split("-");

		    fromDate1 = dateParts[0]; 
		    ToDate1 = dateParts[1]; 

		   
			Date date = new SimpleDateFormat("MM/dd/yyyy").parse(fromDate1);
			String fromDate = new SimpleDateFormat("MM-dd-yyyy").format(date);
		
			
			Date todate = new SimpleDateFormat("MM/dd/yyyy").parse(ToDate1);
			String ToDate = new SimpleDateFormat("MM-dd-yyyy").format(todate);
			
			
			Query file= getMyCurrentSession().createQuery("from IrMUsersBank as user inner join user.bank as branch inner join branch.irFPFileHeaderRecord as fhr  WHERE fhr.processedDate between :start and :end and user.irMUsers.userid=:userId and user.isactive=:Isactive and fhr.fileType.filetypeid=:filetypeid");			

			file.setParameter("start",fromDate);
			file.setParameter("end",ToDate);
			file.setParameter("userId",fileheadrec.getUserid());
			file.setParameter("Isactive",1);
			file.setParameter("filetypeid",new Long(fileheadrec.getFileTypeId()));
	
			
			List<?> list = file.list();
			if(list.size()>0){
					for(int j=0; j<list.size(); j++) {
						
						Object[] rows = (Object[]) list.get(j);
						IrFPFileHeaderRecord irFPFileHeaderRecord = (IrFPFileHeaderRecord)rows[2];
				
					fhrBean = new FileHeaderRecordBean();
				
				fhrBean.setFileHeaderRecordId(irFPFileHeaderRecord.getFileHeaderRecordId());
				fhrBean.setRecordTypeCode(irFPFileHeaderRecord.getRecordTypeCode());
				fhrBean.setPriorityCode(irFPFileHeaderRecord.getPriorityCode());
				fhrBean.setImmediateDestination(irFPFileHeaderRecord.getImmediateDestination());
				fhrBean.setImmediateOrigin(irFPFileHeaderRecord.getImmediateOrigin());
				fhrBean.setFileCreationDate(irFPFileHeaderRecord.getFileCreationDate());
				fhrBean.setFileCreationTime(irFPFileHeaderRecord.getFileCreationTime());
				fhrBean.setFileIdModifier(irFPFileHeaderRecord.getFileIdModifier());
				fhrBean.setRecordSize(irFPFileHeaderRecord.getRecordSize());
				fhrBean.setBlockingFactor(irFPFileHeaderRecord.getBlockingFactor());
				fhrBean.setFormatCode(irFPFileHeaderRecord.getFormatCode());
				fhrBean.setImmediateDestinationName(irFPFileHeaderRecord.getImmediateDestinationName());
				fhrBean.setImmediateOriginName(irFPFileHeaderRecord.getImmediateOriginName());
				fhrBean.setAchFileName(irFPFileHeaderRecord.getAchFileName());
				fhrBean.setProcessedDate(irFPFileHeaderRecord.getProcessedDate());
				fhrBean.setProcessedTime(irFPFileHeaderRecord.getProcessedTime());
				fhrBean.setIsProcessed(irFPFileHeaderRecord.getIsProcessed());
				fhrBean.setBankName(irFPFileHeaderRecord.getBank().getBankName());
				fhrBean.setFileType(irFPFileHeaderRecord.getFileType().getFiletypename());
	
				fhrList.add(fhrBean);
				
			}
				irPlusResponseDetails.setAllFileList(fhrList);
			
				irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			} else {
				LOGGER.debug("FileHeaderRecord not available :: Empty Data :: Data required to showAll FileHeaderRecord");
				irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
			}
			
		}catch(Exception e) {
			LOGGER.error("Exception raised in show list FileHeaderRecord",e);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			
			throw new HibernateException(e);
		}
		
		return irPlusResponseDetails;
	}

	
	@Override
	public IRPlusResponseDetails DaywiseFilesGroup(CustomerGroupingMngmntInfo fileheadrec) throws BusinessException{
		LOGGER.info("Inside FileHeaderRecordDaoImplss");
		LOGGER.debug("Show all FileHeaderRecordDaoImpl instances");
		
		IRPlusResponseDetails irPlusResponseDetails = new  IRPlusResponseDetails();
	
		
		try {
			

			FileHeaderRecordBean fhrBean=null;
			FileControlRecordBean fcrBean = null;
			List<FileHeaderRecordBean> fhrList = new ArrayList<FileHeaderRecordBean>();
			IRSBank bank = null;
			
			 String betweendate="";
			 String fromDate1="";
			 String ToDate1="";
		     DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
		     betweendate=fileheadrec.getCurrentdate();
		     String[] dateParts = betweendate.split("-");

		    fromDate1 = dateParts[0]; 
		    ToDate1 = dateParts[1]; 

			Date date = new SimpleDateFormat("MM/dd/yyyy").parse(fromDate1);
			String fromDate = new SimpleDateFormat("MM-dd-yyyy").format(date);
		
			Date todate = new SimpleDateFormat("MM/dd/yyyy").parse(ToDate1);
			String ToDate = new SimpleDateFormat("MM-dd-yyyy").format(todate);
			
			Query file= getMyCurrentSession().createQuery("select fhr.achFileName,fhr.fileType.filetypename,fhr.processedDate,fhr.processedTime,fhr.isProcessed,fhr.bank.bankName,fhr.fileHeaderRecordId from IrMUsersBank as user inner join user.bank as branch inner join branch.irSCustomersgrouping as cust inner join cust.irsCustomerGrouping as group  inner join branch.irFPFileHeaderRecord as fhr  WHERE fhr.processedDate between :start and :end and user.irMUsers.userid=:userId and user.isactive=:Isactive and group.isactive=:isactive and fhr.fileType.filetypeid=:filetypeid group by fhr.achFileName,fhr.fileType.filetypename,fhr.processedDate,fhr.processedTime,fhr.isProcessed,fhr.bank.bankName,fhr.fileHeaderRecordId");			

			file.setParameter("start",fromDate);
			file.setParameter("end",ToDate);
			file.setParameter("userId",fileheadrec.getUserid());
			file.setParameter("Isactive",1);
			file.setParameter("isactive",1);
			file.setParameter("filetypeid",new Long(fileheadrec.getFileTypeId()));
			
			List<?> list = file.list();
			if(list.size()>0){

					for(int j=0; j<list.size(); j++) {
		
						Object[] rows = (Object[]) list.get(j);
	
						fhrBean = new FileHeaderRecordBean();
			
							fhrBean.setAchFileName((String) rows[0]);
							fhrBean.setFileType((String)rows[1]);
							fhrBean.setProcessedDate(((String) rows[2]));
							fhrBean.setProcessedTime((String)rows[3]);
							fhrBean.setIsProcessed((int)rows[4]);
							fhrBean.setBankName((String)rows[5]);
							fhrBean.setFileHeaderRecordId((int)rows[6]);
								
							fhrList.add(fhrBean);
				
			}

				irPlusResponseDetails.setAllFileList(fhrList);
				
				irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			} else {
				LOGGER.debug("FileHeaderRecord not available :: Empty Data :: Data required to showAll FileHeaderRecord");
				irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
			}
			
		}catch(Exception e) {
		
			LOGGER.error("Exception raised in show list FileHeaderRecord",e);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			
			throw new HibernateException(e);
			
		}
		
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails BankwiseFileList(CustomerGroupingMngmntInfo fileinfo) throws BusinessException{
		LOGGER.info("Inside FileHeaderRecordDaoImplss");
		LOGGER.debug("Show all FileHeaderRecordDaoImpl instances");
		
		IRPlusResponseDetails irPlusResponseDetails = new  IRPlusResponseDetails();
	
		
		try {
			
			FileHeaderRecordBean fhrBean=null;
			FileControlRecordBean fcrBean = null;
			List<FileHeaderRecordBean> fhrList = new ArrayList<FileHeaderRecordBean>();
			IRSBank bank = null;
			
			 String betweendate="";
			 String fromDate1="";
			 String ToDate1="";
		     DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
		     betweendate=fileinfo.getCurrentdate();
		     String[] dateParts = betweendate.split("-");

		    fromDate1 = dateParts[0]; 
		    ToDate1 = dateParts[1]; 

		   
			Date date = new SimpleDateFormat("MM/dd/yyyy").parse(fromDate1);
			String fromDate = new SimpleDateFormat("MM-dd-yyyy").format(date);
		
			
			Date todate = new SimpleDateFormat("MM/dd/yyyy").parse(ToDate1);
			String ToDate = new SimpleDateFormat("MM-dd-yyyy").format(todate);
			
			Query file= getMyCurrentSession().createQuery("from IrMUsersBank as user inner join user.bank as branch inner join branch.irFPFileHeaderRecord as fhr  WHERE fhr.processedDate between :start and :end and user.irMUsers.userid=:userId and user.isactive=:Isactive and fhr.fileType.filetypeid=:filetypeid and fhr.bank.bankId=:branchId");			

			file.setParameter("start",fromDate);
			file.setParameter("end",ToDate);
			file.setParameter("userId",fileinfo.getUserid());
			file.setParameter("Isactive",1);
			file.setParameter("filetypeid",new Long(fileinfo.getFileTypeId()));
			file.setParameter("branchId",new Long(fileinfo.getBankwiseId()));
	
			List<?> list = file.list();
			if(list.size()>0){

					for(int j=0; j<list.size(); j++) {
						
						Object[] rows = (Object[]) list.get(j);
						IrFPFileHeaderRecord irFPFileHeaderRecord = (IrFPFileHeaderRecord)rows[2];
					fhrBean = new FileHeaderRecordBean();
				fhrBean.setFileHeaderRecordId(irFPFileHeaderRecord.getFileHeaderRecordId());
				fhrBean.setRecordTypeCode(irFPFileHeaderRecord.getRecordTypeCode());
				fhrBean.setPriorityCode(irFPFileHeaderRecord.getPriorityCode());
				fhrBean.setImmediateDestination(irFPFileHeaderRecord.getImmediateDestination());
				fhrBean.setImmediateOrigin(irFPFileHeaderRecord.getImmediateOrigin());
				fhrBean.setFileCreationDate(irFPFileHeaderRecord.getFileCreationDate());
				fhrBean.setFileCreationTime(irFPFileHeaderRecord.getFileCreationTime());
				fhrBean.setFileIdModifier(irFPFileHeaderRecord.getFileIdModifier());
				fhrBean.setRecordSize(irFPFileHeaderRecord.getRecordSize());
				fhrBean.setBlockingFactor(irFPFileHeaderRecord.getBlockingFactor());
				fhrBean.setFormatCode(irFPFileHeaderRecord.getFormatCode());
				fhrBean.setImmediateDestinationName(irFPFileHeaderRecord.getImmediateDestinationName());
				fhrBean.setImmediateOriginName(irFPFileHeaderRecord.getImmediateOriginName());
				fhrBean.setAchFileName(irFPFileHeaderRecord.getAchFileName());
				fhrBean.setProcessedDate(irFPFileHeaderRecord.getProcessedDate());
				fhrBean.setProcessedTime(irFPFileHeaderRecord.getProcessedTime());
				fhrBean.setIsProcessed(irFPFileHeaderRecord.getIsProcessed());
				fhrBean.setBankName(irFPFileHeaderRecord.getBank().getBankName());
				fhrBean.setFileType(irFPFileHeaderRecord.getFileType().getFiletypename());
	
				fhrList.add(fhrBean);
				
			}
				irPlusResponseDetails.setAllFileList(fhrList);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			} else {
				LOGGER.debug("FileHeaderRecord not available :: Empty Data :: Data required to showAll FileHeaderRecord");
				irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
			}
			
		}catch(Exception e) {
			LOGGER.error("Exception raised in show list FileHeaderRecord",e);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			
			throw new HibernateException(e);
			
		}
		
		return irPlusResponseDetails;
	}
	
	@Override
	public IRPlusResponseDetails GroupwiseFileList(CustomerGroupingMngmntInfo Grpinfo) throws BusinessException{
	
		IRPlusResponseDetails irPlusResponseDetails = new  IRPlusResponseDetails();
Session session = null;
		
		session = sessionFactory.getCurrentSession();
		
		try {

			FileHeaderRecordBean fhrBean=null;
			FileControlRecordBean fcrBean = null;
			List<FileHeaderRecordBean> fhrList = new ArrayList<FileHeaderRecordBean>();
			IRSBank bank = null;
			
			 String betweendate="";
			 String fromDate1="";
			 String ToDate1="";
		     DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
		     betweendate=Grpinfo.getCurrentdate();
		     String[] dateParts = betweendate.split("-");

		    fromDate1 = dateParts[0]; 
		    ToDate1 = dateParts[1]; 

			Date date = new SimpleDateFormat("MM/dd/yyyy").parse(fromDate1);
			String fromDate = new SimpleDateFormat("MM-dd-yyyy").format(date);
			
			Date todate = new SimpleDateFormat("MM/dd/yyyy").parse(ToDate1);
			String ToDate = new SimpleDateFormat("MM-dd-yyyy").format(todate);
			
			String Grpwisefile="select distinct(f.file_id) from ir_m_users_bank u inner join ir_s_bankbranch b on u.branch_id=b.branch_id inner join ir_s_customergroupingdetails d on d.branch_id=b.branch_id  inner join ir_s_customergrouping c on c.customer_grp_id=d.customer_grp_id inner join ir_f_file_info f on f.branch_id=b.branch_id  inner join ir_f_batch_info bt on bt.file_id=f.file_id and bt.customer_id = d.customer_id inner join ir_m_filetypes t on f.file_type_id=t.filetypeid where f.processed_date  between :start and :end and u.isactive=1 and d.isactive=1 and c.isactive=1 and u.userid=:userId and c.customer_grp_id=:grpId and t.filetypeid=:filetypeid";
			SQLQuery Grpfiles = session.createSQLQuery(Grpwisefile);
			Grpfiles.setParameter("start",fromDate);
			Grpfiles.setParameter("end",ToDate);
			Grpfiles.setParameter("userId",Grpinfo.getUserid());
		
			Grpfiles.setParameter("filetypeid",new Long(Grpinfo.getFileTypeId()));
			Grpfiles.setParameter("grpId",new Integer(Grpinfo.getCustomerGrpId()));
			List<?> grplist = Grpfiles.list();
			if(grplist.size()>0){	
			
			for(int i=0; i<grplist.size(); i++) {
				Object row = (Object) grplist.get(i);
				
			Query file= getMyCurrentSession().createQuery("from IrFPFileHeaderRecord file where file.fileHeaderRecordId=:fileId");			
			file.setParameter("fileId",row);
			List<IrFPFileHeaderRecord> filelist = file.list();
			if(filelist.size()>0){
					for(int j=0; j<filelist.size(); j++) {
					fhrBean = new FileHeaderRecordBean();
				fhrBean.setFileHeaderRecordId(filelist.get(j).getFileHeaderRecordId());
				fhrBean.setRecordTypeCode(filelist.get(j).getRecordTypeCode());
				fhrBean.setPriorityCode(filelist.get(j).getPriorityCode());
				fhrBean.setImmediateDestination(filelist.get(j).getImmediateDestination());
				fhrBean.setImmediateOrigin(filelist.get(j).getImmediateOrigin());
				fhrBean.setFileCreationDate(filelist.get(j).getFileCreationDate());
				fhrBean.setFileCreationTime(filelist.get(j).getFileCreationTime());
				fhrBean.setFileIdModifier(filelist.get(j).getFileIdModifier());
				fhrBean.setRecordSize(filelist.get(j).getRecordSize());
				fhrBean.setBlockingFactor(filelist.get(j).getBlockingFactor());
				fhrBean.setFormatCode(filelist.get(j).getFormatCode());
				fhrBean.setImmediateDestinationName(filelist.get(j).getImmediateDestinationName());
				fhrBean.setImmediateOriginName(filelist.get(j).getImmediateOriginName());
				fhrBean.setAchFileName(filelist.get(j).getAchFileName());
				fhrBean.setProcessedDate(filelist.get(j).getProcessedDate());
				fhrBean.setProcessedTime(filelist.get(j).getProcessedTime());
				fhrBean.setIsProcessed(filelist.get(j).getIsProcessed());
				fhrBean.setBankName(filelist.get(j).getBank().getBankName());
				fhrBean.setFileType(filelist.get(j).getFileType().getFiletypename());
	
				fhrList.add(fhrBean);
				
			}
				irPlusResponseDetails.setAllFileList(fhrList);
				
			} 
			}
			irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			}else {
				LOGGER.debug("FileHeaderRecord not available :: Empty Data :: Data required to showAll FileHeaderRecord");
				irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
			}
			
		}catch(Exception e) {
		
			LOGGER.error("Exception raised in show list FileHeaderRecord",e);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			
			throw new HibernateException(e);
			
		}
		
		return irPlusResponseDetails;
	}
	
	@Override
public IRPlusResponseDetails listfileLog() throws BusinessException {
IRPlusResponseDetails irPlusResponseDetails = new  IRPlusResponseDetails();
	
		try {
			FileAudit fileAudit=null;
			List<FileAudit> logList = new ArrayList<FileAudit>();
					
			Query file = getMyCurrentSession().createQuery("from IrFFileAudit");
			
			//List<IrFPFileHeaderRecord> fileList= file.list();
			
			List<IrFFileAudit> list = file.list();
			if(list.size()>0){
			for(IrFFileAudit irFFileAudit:list) {
				
				fileAudit = new FileAudit();
				fileAudit.setAuditDate(irFFileAudit.getAuditDate());
				fileAudit.setFileType(irFFileAudit.getFileType());
				fileAudit.setFileName(irFFileAudit.getFileName());
				fileAudit.setStatus(irFFileAudit.getStatus());
				fileAudit.setDescription(irFFileAudit.getDescription());
				fileAudit.setIsProcessed(irFFileAudit.getIsProcessed());
				fileAudit.setProcessedDateTime(irFFileAudit.getProcessedDateTime());
				logList.add(fileAudit);
				
			}
				irPlusResponseDetails.setFileLogList(logList);
				//irPlusResponseDetails.setFhrBean(fhrBean);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			} else {
				LOGGER.debug("FileHeaderRecord not available :: Empty Data :: Data required to showAll FileHeaderRecord");
				irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
			}
			
		}catch(Exception e) {
			LOGGER.error("Exception raised in show list FileHeaderRecord",e);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			
			throw new HibernateException(e);
			
		}
		
		return irPlusResponseDetails;

	}
	@Override
	public IRPlusResponseDetails filterFiles(FileTypeInfo fileListInfo) throws BusinessException
	{
		LOGGER.info("Inside filterFiles : "+fileListInfo);

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		Session session = null;

		List<FileTypeInfo>file = new ArrayList<FileTypeInfo>();
		List<FileHeaderRecordBean> filelistss = new ArrayList<FileHeaderRecordBean>();
		try
		{

			
			session = sessionFactory.getCurrentSession();

		Criteria criteria = session.createCriteria(IrFPBatchHeaderRecord.class, "batchlist");
		criteria.setProjection(Projections.groupProperty("batchlist.irFPFileHeaderRecord.fileHeaderRecordId"));
	
		criteria.createAlias("batchlist.customer", "batch");
		criteria.createAlias("batchlist.irFPFileHeaderRecord", "file");
		criteria.createAlias("file.bank", "Banklist");
		criteria.createAlias("Banklist","Bank");
		criteria.createAlias("Banklist.irMUsersBank", "userBank");
		criteria.createAlias("userBank.irMUsers", "user");
		criteria.createAlias("batchlist.irFPFileHeaderRecord.fileType", "filetype");
		
	
	    
	    if (fileListInfo.getFromDate().equals("")){
	    	
	    	String ToDate=fileListInfo.getToDate();
	    	String fromDate=fileListInfo.getFromDate();
	    	if(fileListInfo.getBankname()!=null&&!fileListInfo.getBankname().isEmpty())
			{
				criteria.add(Restrictions.eq("Bank.bankName",(fileListInfo.getBankname())));
				
				
			}
		
				if(fileListInfo.getBankCustCode()!=null&&!fileListInfo.getBankCustCode().isEmpty())
			{
				criteria.add(Restrictions.eq("batch.bankCustomerCode",fileListInfo.getBankCustCode())) ;
			}
		
			if(fileListInfo.getCustomername()!=null&&!fileListInfo.getCustomername().isEmpty())
			{
				criteria.add(Restrictions.eq("batch.companyName",fileListInfo.getCustomername()));
		
			}
			
			if(fileListInfo.getFiletypeid()!=null)
			{
				criteria.add(Restrictions.eq("filetype.filetypeid",new Long(fileListInfo.getFiletypeid())));
			}
			
			if(fileListInfo.getFromDate()!=null&&!fileListInfo.getFromDate().isEmpty())
			{
				criteria.add(Restrictions.ge("file.processedDate",fromDate));

			}
			if(fileListInfo.getToDate()!=null&&!fileListInfo.getToDate().isEmpty())
			{
				criteria.add(Restrictions.le("file.processedDate",ToDate));

			}
			if(fileListInfo.getUserId()!=0)
			{
			
				criteria.add(Restrictions.eq("userBank.irMUsers.userid",new Integer(fileListInfo.getUserId())));

			}
	    	}
	 
	    
	    if (!fileListInfo.getFromDate().equals("")){
	    	Date date = new SimpleDateFormat("MM/dd/yyyy").parse(fileListInfo.getFromDate());
			String fromDate = new SimpleDateFormat("MM-dd-yyyy").format(date);
		
			Date todate = new SimpleDateFormat("MM/dd/yyyy").parse(fileListInfo.getToDate());
			String ToDate = new SimpleDateFormat("MM-dd-yyyy").format(todate);
			if(fileListInfo.getBankname()!=null&&!fileListInfo.getBankname().isEmpty())
			{
				criteria.add(Restrictions.eq("Bank.bankName",(fileListInfo.getBankname())));
			}
		
				if(fileListInfo.getBankCustCode()!=null&&!fileListInfo.getBankCustCode().isEmpty())
			{
				criteria.add(Restrictions.eq("batch.bankCustomerCode",fileListInfo.getBankCustCode())) ;
			}
		
			if(fileListInfo.getCustomername()!=null&&!fileListInfo.getCustomername().isEmpty())
			{
				criteria.add(Restrictions.eq("batch.companyName",fileListInfo.getCustomername()));
		    }
			
			if(fileListInfo.getFiletypeid()!=null)
			{
				criteria.add(Restrictions.eq("filetype.filetypeid",new Long(fileListInfo.getFiletypeid())));
			}
			if(fileListInfo.getFromDate()!=null&&!fileListInfo.getFromDate().isEmpty())
			{
				criteria.add(Restrictions.ge("file.processedDate",fromDate));

			}
			if(fileListInfo.getToDate()!=null&&!fileListInfo.getToDate().isEmpty())
			{
				criteria.add(Restrictions.le("file.processedDate",ToDate));

			}
			if(fileListInfo.getUserId()!=0)
			{
				criteria.add(Restrictions.eq("userBank.irMUsers.userid",new Integer(fileListInfo.getUserId())));
			}
			
	    }
		
	
			List<IrFPBatchHeaderRecord> batchList = criteria.list();
		
			if(batchList.size()>0){
				for(int j=0; j<batchList.size(); j++) {
					Object fileid = (Object) batchList.get(j);
			
			Query fileprocess= getMyCurrentSession().createQuery("from IrFPFileHeaderRecord as file inner join file.fileType as filetype inner join file.bank as bank where file.fileHeaderRecordId=:fileId");			

			fileprocess.setParameter("fileId",fileid);
			FileHeaderRecordBean fileList = null;
			

			List<?> rowval = fileprocess.list();
			if(rowval.size()>0){
			for(int i=0; i<rowval.size(); i++) {
				Object[] rows = (Object[]) rowval.get(i);
				IrFPFileHeaderRecord irFhr = (IrFPFileHeaderRecord)rows[0];
				IrMFileTypes filetype = (IrMFileTypes)rows[1];
				IRSBank bank = (IRSBank)rows[2];
			
	
				fileList = new FileHeaderRecordBean();
		
				fileList.setAchFileName(irFhr.getAchFileName());
				
				fileList.setFileTypeId(filetype.getFiletypeid());

				fileList.setFileType(filetype.getFiletypename());

				fileList.setBankName(bank.getBankName());
						
			

				fileList.setProcessedDate(irFhr.getProcessedDate());

				fileList.setProcessedTime(irFhr.getProcessedTime());
				
				fileList.setIsProcessed(irFhr.getIsProcessed());
				fileList.setFileHeaderRecordId(irFhr.getFileHeaderRecordId());
				filelistss.add(fileList);
				
		
			}
			}
				}
			}
			irPlusResponseDetails.setAllFileList(filelistss);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
		}
		catch(Exception e)
		{
			LOGGER.error("Exception in listBank",e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);

			throw new HibernateException(e);
		}

		return irPlusResponseDetails;
	}
	
	
	
	
}
