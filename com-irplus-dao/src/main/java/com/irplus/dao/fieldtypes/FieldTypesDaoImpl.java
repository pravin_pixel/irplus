package com.irplus.dao.fieldtypes;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.irplus.dao.hibernate.entities.IrMFieldtypes;
import com.irplus.dao.hibernate.entities.IrMUsers;
import com.irplus.dto.FieldTypesInfo;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.util.BusinessException;
import com.irplus.util.IRPlusConstants;

@Transactional
@Component
public class FieldTypesDaoImpl implements IFieldTypesDao {

	private static final Logger log = Logger.getLogger(FieldTypesDaoImpl.class);

	@Autowired
	SessionFactory sessionFactory;

	public Session getMyCurrentSession() {
		Session session = null;
		try {
			session = sessionFactory.getCurrentSession();
		} catch (HibernateException e) {
			log.error("Exception raised DaoImpl :: At current session creation time ::" + e);
			throw new HibernateException(e);
		}
		return session;
	}

	@Override
	public IRPlusResponseDetails createFieldTypes(FieldTypesInfo fieldTypesInfo) throws BusinessException {

		log.info("Inside of FieldTypesDaoImpl");
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		IrMFieldtypes irMFieldtypes = new IrMFieldtypes();

		try {
			irMFieldtypes.setDefaultLength(fieldTypesInfo.getDefaultLength());
			irMFieldtypes.setFieldFormat(fieldTypesInfo.getFieldFormat());
			// irMFieldtypes.setFieldTypeId(fieldTypesInfo.getFieldTypeId());
			irMFieldtypes
					.setIrMUsers((IrMUsers) getMyCurrentSession().get(IrMUsers.class, fieldTypesInfo.getUsersId()));
			// irMFieldtypes.setIrSFormvalidations(fieldTypesInfo.getIrSFormvalidations());
			irMFieldtypes.setIsactive(fieldTypesInfo.getIsactive());

			getMyCurrentSession().save(irMFieldtypes);

			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);

		} catch (Exception e) {
			log.error("Exception in create FieldTypes", e);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			throw new HibernateException(e);
		}

		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails listFieldTypes() throws BusinessException {

		log.info("Inside FieldTypesDaoimpl");

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		List<FieldTypesInfo> listFieldTypes = new ArrayList<FieldTypesInfo>();

		try {

			Criteria criteria = getMyCurrentSession().createCriteria(IrMFieldtypes.class);

			List<IrMFieldtypes> listFields = (List<IrMFieldtypes>) criteria.list();

			if (!listFields.isEmpty()) {

				for (IrMFieldtypes irMFieldtypes : listFields) {

					FieldTypesInfo fieldTypesInfo = new FieldTypesInfo();

					fieldTypesInfo.setDefaultLength(irMFieldtypes.getDefaultLength());
					fieldTypesInfo.setFieldFormat(irMFieldtypes.getFieldFormat());
					fieldTypesInfo.setFieldTypeId(irMFieldtypes.getFieldTypeId());
					// fieldTypesInfo.setIrSFormvalidations(irMFieldtypes.getIrSFormvalidations());
					fieldTypesInfo.setIsactive(irMFieldtypes.getIsactive());

					fieldTypesInfo.setUsersId(irMFieldtypes.getIrMUsers().getUserid());

					fieldTypesInfo.setCreateddate(irMFieldtypes.getCreateddate());
					fieldTypesInfo.setModifieddate(irMFieldtypes.getModifieddate());

					listFieldTypes.add(fieldTypesInfo);
				}

				irPlusResponseDetails.setFieldsInfo(listFieldTypes);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);

			} else {
				log.debug("  Empty Data :: Data required to showAll FieldTypes");
				irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
			}

		} catch (Exception e) {
			log.error("Exception raised in show listFieldTypes ", e);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);

			throw new HibernateException(e);
		}

		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails getFieldTypesById(String fieldTypesById) throws BusinessException {

		log.info("Inside of getfieldtypes :: Fieldtypesdaoimpl");
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		FieldTypesInfo fieldsInfo = new FieldTypesInfo();
		List<FieldTypesInfo> listField = new ArrayList<FieldTypesInfo>();
		IrMFieldtypes irMFieldtypes;
		try {
			irMFieldtypes = (IrMFieldtypes) getMyCurrentSession().get(IrMFieldtypes.class,
					Integer.parseInt(fieldTypesById));

			if (irMFieldtypes != null) {

				fieldsInfo.setDefaultLength(irMFieldtypes.getDefaultLength());
				fieldsInfo.setFieldFormat(irMFieldtypes.getFieldFormat());
				fieldsInfo.setFieldTypeId(irMFieldtypes.getFieldTypeId());
				// fieldsInfo.setIrSFormvalidations(irMFieldtypes.getIrSFormvalidations());
				fieldsInfo.setIsactive(irMFieldtypes.getIsactive());
				fieldsInfo.setModifieddate(irMFieldtypes.getModifieddate());
				fieldsInfo.setCreateddate(irMFieldtypes.getCreateddate());
				fieldsInfo.setUsersId(irMFieldtypes.getIrMUsers().getUserid());

				listField.add(fieldsInfo);

				irPlusResponseDetails.setFieldsInfo(listField);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
			} else {

				irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
			}

		} catch (Exception e) {

			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			log.error("Exception Raised inside customer DaoImple :: " + e);
			throw new HibernateException(e);
		}

		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails updateFieldType(FieldTypesInfo fieldTypesInfo) throws BusinessException {

		log.info("Inside of updateField");

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		IrMFieldtypes irMFieldtypes = null;

		try {
			irMFieldtypes = (IrMFieldtypes) getMyCurrentSession().get(IrMFieldtypes.class,
					fieldTypesInfo.getFieldTypeId());

			irMFieldtypes.setDefaultLength(fieldTypesInfo.getDefaultLength());
			irMFieldtypes.setFieldFormat(fieldTypesInfo.getFieldFormat());
			irMFieldtypes.setFieldTypeId(fieldTypesInfo.getFieldTypeId());
			irMFieldtypes
					.setIrMUsers((IrMUsers) getMyCurrentSession().get(IrMUsers.class, fieldTypesInfo.getUsersId()));
			// irMFieldtypes.setIrSFormvalidations();
			irMFieldtypes.setIsactive(fieldTypesInfo.getIsactive());
			irMFieldtypes.setModifieddate(fieldTypesInfo.getModifieddate());

			getMyCurrentSession().update(irMFieldtypes);

			irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			irPlusResponseDetails.setValidationSuccess(true);

		} catch (Exception e) {

			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			log.error("Exception Raised inside customerDaoImpl :: updateCustomerId : " + e);

			throw new HibernateException(e);
		}
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails deleteFieldTypeById(String fieldTypesById) throws BusinessException {

		log.info("inside of deleteFieldType");

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		try {
			Query query = getMyCurrentSession()
					.createQuery("delete IrMFieldtypes i  where i.fieldTypeId =:fieldType_Id");
			query.setParameter("fieldType_Id", Integer.parseInt(fieldTypesById));

			int result = query.executeUpdate();

			if (result == 0) {
				log.debug("deleted FieldType failed to delete");
				irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);

			} else {
				log.debug("succcessfully deleted FieldType :: FieldType" + fieldTypesById);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			}
		} catch (Exception e) {
			log.error("Exception raised in deleted FieldType", e);
			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			throw new HibernateException(e);
		}
		return irPlusResponseDetails;
	}
}