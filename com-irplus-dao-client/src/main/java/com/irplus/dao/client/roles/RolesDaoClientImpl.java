package com.irplus.dao.client.roles;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.irplus.dao.roles.IRolesDao;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.StatusIdInfo;
import com.irplus.dto.roles.RoleBean;
import com.irplus.util.BusinessException;

@Component
public class RolesDaoClientImpl implements IRolesDaoClient {

	private static final Logger log = Logger.getLogger(RolesDaoClientImpl.class);
	
	@Autowired
	IRolesDao rolesDao;
	
	@Override
	public IRPlusResponseDetails createRole(RoleBean roleBean) throws BusinessException {

		IRPlusResponseDetails roleResponseDetails = new IRPlusResponseDetails() ;
		
		log.debug("Inside of RolesDaoClientImpl :: createRole");
	
		roleResponseDetails = rolesDao.createRole(roleBean);
		return roleResponseDetails;
	}

	@Override
	public IRPlusResponseDetails getRoleById(String roleId) throws BusinessException {
		
		log.info("Inside RoleDaoClientImpl :: getRoleId");
		
		return rolesDao.getRoleById(roleId);
	}

	@Override
	public IRPlusResponseDetails updateRole(RoleBean roleBean) throws BusinessException {
		
		log.info("Inside RoleDaoClientImpl :: getRoleId");
		
		return rolesDao.updateRole(roleBean);
	}

	@Override
	public IRPlusResponseDetails deleteRoleById(String roleId) throws BusinessException {
		
		log.info("Inside RoleDaoClientImpl :: getRoleId");
		
		return rolesDao.deleteRoleById(roleId);
	}

	@Override
	public IRPlusResponseDetails ShowAllRoles() throws BusinessException {
		
		log.info("Inside RoleDaoClientImpl :: getRoleId");
		
		return rolesDao.ShowAllRoles();
	}

	@Override
	public IRPlusResponseDetails removeDuplicateRole(RoleBean roleBean) throws BusinessException {

		log.info("Inside RoleDaoClientImpl :: getRoleId");
		
		return rolesDao.removeDuplicateRole(roleBean);
	}

	@Override
	public IRPlusResponseDetails updateRoleStatus(StatusIdInfo statusIdInfo) throws BusinessException {

		log.info("Inside RoleDaoClientImpl :: getRoleId");
		
		return rolesDao.updateRoleStatus(statusIdInfo);
	}

	@Override
	public IRPlusResponseDetails ShowAllRoles_roleprm() throws BusinessException {
		
		log.info("Inside RoleDaoClientImpl :: ShowAllRoles_roleprm");
		
		return rolesDao.ShowAllRoles_roleprm();
	}

	
	
}
