package com.irplus.dao.client.menumgmt;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.irplus.dao.menu.IrMMenusDao;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.StatusIdInfo;
import com.irplus.dto.menu.AddMenu;
import com.irplus.util.BusinessException;

@Component
public class MenuMgmtDaoClientImpl implements IMenuMgmtDaoClient{

	@Autowired
	IrMMenusDao irMMenusDao;
 	
	private static final Logger log = Logger.getLogger(MenuMgmtDaoClientImpl.class);

	//Creating or Adding Menu
	
	@Override
	public IRPlusResponseDetails createMenu(AddMenu menuInfo) throws BusinessException {
	
		log.info("Inside MenuMgmtDaoClientImpl :: createMenu");
		
		 return irMMenusDao.createMenu(menuInfo);
	}
	
	//Getting One Menu
	@Override
	public IRPlusResponseDetails getMenu(String menuId) throws BusinessException {
		log.info("Inside MenuMgmtDaoClientImpl :: listMenus");
		
		return irMMenusDao.getMenu(menuId);
	}

	//Editing Menu
	@Override
	public IRPlusResponseDetails updateMenu(AddMenu menuInfo) throws BusinessException {
		log.info("Inside MenuMgmtDaoClientImpl :: updateMenu");
		
		return irMMenusDao.updateMenu(menuInfo);
	}

	//deleting menu
	@Override
	public IRPlusResponseDetails deleteMenu(String menuId) throws BusinessException {
		log.info("Inside MenuMgmtDaoClientImpl :: deleteMenu");
		
		return irMMenusDao.deleteMenu(menuId);
	}

	@Override
	public IRPlusResponseDetails findAllMenus() throws BusinessException {
		log.info("Inside MenuMgmtDaoClientImpl :: findAllMenus");
		return irMMenusDao.findAllMenus();
	}

	@Override
	public IRPlusResponseDetails updateMenuStatusPrm(StatusIdInfo siInfo) throws BusinessException {
		log.info("Inside MenuMgmtDaoClientImpl :: status update Info");
		return irMMenusDao.updateMenuStatusPrm(siInfo);
	}

	@Override
	public IRPlusResponseDetails createMenuWithoutDpct(AddMenu menuInfo) throws BusinessException {
		log.info("Inside MenuMgmtDaoClientImpl :: add menu Info");
		return irMMenusDao.createMenuWithoutDpct(menuInfo);
	}

	@Override
	public IRPlusResponseDetails getActiveMenus() throws BusinessException {
		
		return irMMenusDao.getActiveMenus();
	}
}
