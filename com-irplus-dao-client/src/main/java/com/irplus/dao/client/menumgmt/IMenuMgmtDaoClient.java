package com.irplus.dao.client.menumgmt;

import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.StatusIdInfo;
import com.irplus.dto.menu.AddMenu;
import com.irplus.util.BusinessException;

public interface IMenuMgmtDaoClient {

	public IRPlusResponseDetails createMenu(AddMenu menuInfo) throws BusinessException;

	public IRPlusResponseDetails getMenu(String menuId) throws BusinessException;

	public IRPlusResponseDetails updateMenu(AddMenu menuInfo) throws BusinessException;

	public IRPlusResponseDetails deleteMenu(String menuId) throws BusinessException;
	
	public IRPlusResponseDetails findAllMenus()throws BusinessException;
	
	public IRPlusResponseDetails updateMenuStatusPrm(StatusIdInfo siInfo) throws BusinessException; 
	
	public IRPlusResponseDetails createMenuWithoutDpct(AddMenu menuInfo) throws BusinessException ;
	
	public IRPlusResponseDetails getActiveMenus() throws BusinessException ;

}