package com.irplus.dao.client.modulemngmt;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.irplus.dao.module.IModulesDao;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.StatusIdInfo;
import com.irplus.dto.module.ModuleBean;
import com.irplus.util.BusinessException;

@Component
public class ModulesDaoClientImpl implements IModulesDaoClient{

	private static final Log log = LogFactory.getLog(ModulesDaoClientImpl.class);
	
	@Autowired 
	IModulesDao modulesDao;
	
	@Override
	public IRPlusResponseDetails createModule(ModuleBean moduleInfo) throws BusinessException {

		log.info("Inside ModuleMgmtDaoClientImpl :: Module create");
		return modulesDao.createModule(moduleInfo);
	}

	@Override
	public IRPlusResponseDetails getModuleById(String moduleId) throws BusinessException {
		log.info("Inside ModuleMgmtDaoClientImpl :: Module getModule");
		return modulesDao.getModuleById(moduleId);
	}

	@Override
	public IRPlusResponseDetails updateModule(ModuleBean moduleInfo) throws BusinessException {
		log.info("Inside ModuleMgmtDaoClientImpl :: Module update");
		return modulesDao.updateModule(moduleInfo);
	}

	@Override
	public IRPlusResponseDetails deleteModuleById(String moduleId) throws BusinessException {
		log.info("Inside ModuleMgmtDaoClientImpl :: Module delete");
		return modulesDao.deleteModuleById(moduleId);
	}

	@Override
	public IRPlusResponseDetails findAllModules() throws BusinessException {
		log.info("Inside ModuleMgmtDaoClientImpl :: find All Modules");
		return modulesDao.findAllModules();
	}

	@Override
	public IRPlusResponseDetails createByConditionModule(ModuleBean moduleInfo) throws BusinessException {
		log.info("Inside ModuleMgmtDaoClientImpl :: create Modules");
		return modulesDao.createByConditionModule(moduleInfo);
	}

	@Override
	public IRPlusResponseDetails createModuleNoDuplct(ModuleBean moduleInfo) throws BusinessException {
		log.info("Inside ModuleMgmtDaoClientImpl :: create Modules");
		return modulesDao.createModuleNoDuplct(moduleInfo);
	}

	@Override
	public IRPlusResponseDetails updateModuleStatus(StatusIdInfo siInfo) throws BusinessException {
		log.info("Inside ModuleMgmtDaoClientImpl :: update status");
		return modulesDao.updateModuleStatus(siInfo);
	}

}
