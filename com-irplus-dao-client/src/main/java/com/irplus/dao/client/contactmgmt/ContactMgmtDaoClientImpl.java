package com.irplus.dao.client.contactmgmt;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.irplus.dao.contactmgmt.IContactMgmtDao;
import com.irplus.dto.ContactInfo;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.util.BusinessException;

@Component
public class ContactMgmtDaoClientImpl implements IContactMgmtDaoClient {

	@Autowired
	IContactMgmtDao iContactMgmtDao;

	
	private static final Logger LOGGER = Logger.getLogger(ContactMgmtDaoClientImpl.class);
	
	@Override
	public IRPlusResponseDetails addContact(ContactInfo contactInfo) throws BusinessException {
        
		LOGGER.info("Inside ContactMgmtDaoClientImpl :: addContact");

        return iContactMgmtDao.addContact(contactInfo);
        
	}

	@Override
	public IRPlusResponseDetails listContacts(String bankId) throws BusinessException {

		LOGGER.info("Inside ContactMgmtDaoClientImpl :: listContacts");

        return iContactMgmtDao.listContacts(bankId);
	}
	
	@Override
	public IRPlusResponseDetails updateContact(ContactInfo contactInfo) throws BusinessException {
        
		LOGGER.info("Inside ContactMgmtDaoClientImpl :: updateContact");

        return iContactMgmtDao.updateContact(contactInfo);
        
	}
	
	@Override
	public IRPlusResponseDetails updateContacts(ContactInfo[] contacts) throws BusinessException {
        
		LOGGER.info("Inside ContactMgmtDaoClientImpl :: updateContact");

        return iContactMgmtDao.updateContacts(contacts);
        
	}
	
	@Override
	public IRPlusResponseDetails deleteContact(String contactId) throws BusinessException {

		LOGGER.info("Inside ContactMgmtDaoClientImpl :: deleteContact");

        return iContactMgmtDao.deleteContact(contactId);
	}
	
	

}
