package com.irplus.dao.client.fileprocessing.batchheaderrecord;

import java.util.List;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.irplus.dao.fileprocessing.batchheaderrecord.BatchHeaderRecordDao;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.fileprocessing.BatchHeaderRecordBean;
import com.irplus.util.BusinessException;

@Component
public class BatchHeaderRecordDaoClientImpl implements BatchHeaderRecordDaoClient{
	
	private static final Logger LOGGER = Logger.getLogger(BatchHeaderRecordDaoClientImpl.class);

	@Autowired
	private BatchHeaderRecordDao batchHeaderRecordDao;

	@Override
	public Boolean insertRecord(BatchHeaderRecordBean batchHeaderRecord) {
		LOGGER.info("Inside BatchHeaderRecordDaoClientImpl :: insertRecord");
		return batchHeaderRecordDao.insertRecord(batchHeaderRecord);
	}

	@Override
	public Boolean deleteRecord(BatchHeaderRecordBean batchHeaderRecord) {
		LOGGER.info("Inside BatchHeaderRecordDaoClientImpl :: deleteRecord");
		return batchHeaderRecordDao.deleteRecord(batchHeaderRecord);
	}

	@Override
	public List<BatchHeaderRecordBean> listAllRecords() {
		LOGGER.info("Inside BatchHeaderRecordDaoClientImpl :: listAllRecords");
		return batchHeaderRecordDao.listAllRecords();
	}

	
	@Override
	public Boolean updateRecord(BatchHeaderRecordBean batchHeaderRecord) {
		LOGGER.info("Inside BatchHeaderRecordDaoClientImpl :: updateRecord");
		return batchHeaderRecordDao.updateRecord(batchHeaderRecord);
	}

	@Override
	public List<BatchHeaderRecordBean> getByQuery(Integer Id) {
		LOGGER.info("Inside BatchHeaderRecordDaoClientImpl :: getByQuery");
		return batchHeaderRecordDao.getByQuery(Id);
	}

	@Override
	public IRPlusResponseDetails getRecordByBatchId(Integer fileId) throws BusinessException {
		LOGGER.info("Inside BatchHeaderRecordDaoClientImpl :: getRecordByBatchId");
		return batchHeaderRecordDao.getRecordByBatchId(fileId);
	}

}
