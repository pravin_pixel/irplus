package com.irplus.dao.client.fieldtypes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.irplus.dao.fieldtypes.IFieldTypesDao;
import com.irplus.dto.FieldTypesInfo;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.util.BusinessException;

@Component
public class FieldTypesDaoClientImpl implements IFieldTypesDaoClient {

	@Autowired
	IFieldTypesDao iFieldTypesDao;

	@Override
	public IRPlusResponseDetails createFieldTypes(FieldTypesInfo fieldTypesInfo) throws BusinessException {

		return iFieldTypesDao.createFieldTypes(fieldTypesInfo);
	}

	@Override
	public IRPlusResponseDetails getFieldTypesById(String fieldTypesInfoId) throws BusinessException {

		return iFieldTypesDao.getFieldTypesById(fieldTypesInfoId);
	}

	@Override
	public IRPlusResponseDetails updateFieldTypes(FieldTypesInfo fieldTypesInfo) throws BusinessException {

		return iFieldTypesDao.updateFieldType(fieldTypesInfo);
	}

	@Override
	public IRPlusResponseDetails deleteFieldTypesById(String fieldTypesInfoId) throws BusinessException {

		return iFieldTypesDao.deleteFieldTypeById(fieldTypesInfoId);
	}

	@Override
	public IRPlusResponseDetails showAllFieldTypes() throws BusinessException {

		return iFieldTypesDao.listFieldTypes();
	}
	
	
	
	
}
