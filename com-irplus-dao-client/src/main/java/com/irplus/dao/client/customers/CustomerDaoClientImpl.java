package com.irplus.dao.client.customers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.irplus.dao.customers.ICustomerDao;
import com.irplus.dto.CustomerBean;
import com.irplus.dto.CustomerFilter;
import com.irplus.dto.DataEntryFormValidationDTO;
import com.irplus.dto.FormsManagementDTO;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.StatusIdInfo;
import com.irplus.util.BusinessException;

@Component
public class CustomerDaoClientImpl  implements ICustomerDaoClient{

	@Autowired
	ICustomerDao iCustomerDao;
	
	@Override
	public IRPlusResponseDetails createCustomer(CustomerBean customerBean) throws BusinessException {

		return iCustomerDao.createCustomer(customerBean);
	}

	@Override
	public IRPlusResponseDetails getCustomerById(CustomerBean customerBean) throws BusinessException {

		return iCustomerDao.getCustomerById(customerBean);
	}

	@Override
	public IRPlusResponseDetails updateCustomer(CustomerBean customersBean) throws BusinessException {

		return iCustomerDao.updateCustomer(customersBean);
	}

	@Override
	public IRPlusResponseDetails deleteCustomer(String custommerId) throws BusinessException {

		return iCustomerDao.deleteCustomer(custommerId);
	}

	@Override
	public IRPlusResponseDetails showAllCustomer(String siteId) throws BusinessException{

		return iCustomerDao.showAllCustomer(siteId);
	}

	@Override
	public IRPlusResponseDetails showAllBanks(String SiteId) throws BusinessException {

		return iCustomerDao.showAllBanks(SiteId);
	}

	@Override
	public IRPlusResponseDetails updateCutomerStaus(StatusIdInfo statusIdInfo) throws BusinessException {

		return iCustomerDao.updateCutomerStaus(statusIdInfo);
	}

	@Override
	public IRPlusResponseDetails customerFilter(CustomerFilter customerFilter) throws BusinessException {
	
		return iCustomerDao.customerFilter(customerFilter);
	}

	@Override
	public IRPlusResponseDetails showAllFileProcessing() throws BusinessException {

		return iCustomerDao.showAllFileProcessing();
	}

	@Override
	public IRPlusResponseDetails showAllBusinessProcessing() throws BusinessException {

		return iCustomerDao.showAllBusinessProcessing();
	}

	@Override
	public IRPlusResponseDetails showBankBranch(String bankId) throws BusinessException {
		
		return iCustomerDao.showBankBranch(bankId);
	}

	@Override
	public IRPlusResponseDetails addCustomerLicense(CustomerBean customerBean) throws BusinessException {
		
		return iCustomerDao.addCustomerLicense(customerBean);
	}

	@Override
	public IRPlusResponseDetails addCustomerOtherLicense(CustomerBean customerBean) throws BusinessException {
	
		return iCustomerDao.addCustomerOtherLicense(customerBean);
	}

	@Override
	public IRPlusResponseDetails Autocomplete(String term,String userid) throws BusinessException{
	
		return iCustomerDao.Autocomplete(term,userid);
	}
	@Override
	public IRPlusResponseDetails AutocompleteCustomer(String term,String userid) throws BusinessException{
	
		return iCustomerDao.AutocompleteCustomer(term,userid);
	}
	@Override
	public IRPlusResponseDetails AutocompleteCustomerCode(String term,String userid) throws BusinessException{
	
		return iCustomerDao.AutocompleteCustomerCode(term,userid);
	}
	@Override
	public IRPlusResponseDetails AutocompleteBank(String term,String userid) throws BusinessException{
	
		return iCustomerDao.AutocompleteBank(term,userid);
	}
	@Override
	public IRPlusResponseDetails AddCustomerDataEntry(FormsManagementDTO formsManagement) throws BusinessException{
	
		return iCustomerDao.AddCustomerDataEntry(formsManagement);
	}
	
	@Override
	public IRPlusResponseDetails AddCustomerDataEntryDetails(DataEntryFormValidationDTO[] dataentryform) throws BusinessException{
	
		return iCustomerDao.AddCustomerDataEntryDetails(dataentryform);
	}
}
