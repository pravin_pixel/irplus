package com.irplus.dao.client.report_file_formats;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.irplus.dao.report_file_formats.IReportFileFormatsDao;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.ReportFileFormatInfo;
import com.irplus.util.BusinessException;

@Component
public class ReportFileFormatsDaoClientImpl  implements IReportFileFormatsDaoClient{

	@Autowired 
	IReportFileFormatsDao iReportFileFormatsDao;
	
	@Override
	public IRPlusResponseDetails createReportFileFormat(ReportFileFormatInfo reportFileFormatInfo)
			throws BusinessException {

		return iReportFileFormatsDao.createReportFileFormat(reportFileFormatInfo);
	}

	@Override
	public IRPlusResponseDetails getReportFileFormatById(String reportFileFormatInfoId) throws BusinessException{

		return iReportFileFormatsDao.getReportFileFormatById(reportFileFormatInfoId);
	}

	@Override
	public IRPlusResponseDetails updateReportFileFormat(ReportFileFormatInfo reportFileFormatInfo)
			throws BusinessException {

		return iReportFileFormatsDao.updateReportFileFormat(reportFileFormatInfo);
	}

	@Override
	public IRPlusResponseDetails deleteReportFileFormatById(String reportFileFormatInfoId) throws BusinessException {

		return iReportFileFormatsDao.deleteReportFileFormatById(reportFileFormatInfoId);
	}

	@Override
	public IRPlusResponseDetails showAllReportFileFormat() throws BusinessException {

		return iReportFileFormatsDao.showAllReportFileFormat();
	}

	
	
	
}
