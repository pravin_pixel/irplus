package com.irplus.dao.client.modulemngmt;



import com.irplus.dto.module.ModuleBean;
import com.irplus.dto.module.ModuleResponseDetails;
import com.irplus.util.BusinessException;

public interface ModulesDaoClient {
		
    public ModuleResponseDetails createModule(ModuleBean moduleInfo) throws BusinessException;

	public ModuleResponseDetails getModuleById(String moduleId) throws BusinessException;		

	public ModuleResponseDetails updateModule(ModuleBean moduleInfo) throws BusinessException;

	public ModuleResponseDetails deleteModuleById(String moduleId) throws BusinessException;
		
	public ModuleResponseDetails findAllModules() throws BusinessException;
	
}
