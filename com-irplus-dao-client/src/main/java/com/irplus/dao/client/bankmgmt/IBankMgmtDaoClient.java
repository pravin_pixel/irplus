package com.irplus.dao.client.bankmgmt;

import com.irplus.dto.BankFilterInfo;
import com.irplus.dto.BankInfo;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.util.BusinessException;

public interface IBankMgmtDaoClient {

	 public IRPlusResponseDetails createBank(BankInfo bankInfo)
	            throws BusinessException;

	public IRPlusResponseDetails listBanks(String siteId) throws BusinessException;
	
	public IRPlusResponseDetails filterBanks(BankFilterInfo bankFilterInfo) throws BusinessException;

	public IRPlusResponseDetails updateBank(BankInfo bankInfo) throws BusinessException;
	
	public IRPlusResponseDetails updateBankStatus(BankInfo bankInfo) throws BusinessException;
	
	public IRPlusResponseDetails updateBranchStatus(BankInfo bankInfo) throws BusinessException; 
	
	public IRPlusResponseDetails getBankInfo(String bankId) throws BusinessException;

	public IRPlusResponseDetails deleteBank(BankInfo bankInfo) throws BusinessException;
	
	public IRPlusResponseDetails createBankBranch(BankInfo bankInfo)
	            throws BusinessException;

	public IRPlusResponseDetails listBankBranches(String siteId) throws BusinessException;

	public IRPlusResponseDetails updateBranch(BankInfo bankInfo) throws BusinessException;

	public IRPlusResponseDetails deleteBranch(BankInfo bankInfo) throws BusinessException;

	public IRPlusResponseDetails addBankLicense(BankInfo bankInfo) throws BusinessException;

	public IRPlusResponseDetails getBranchInfo(BankInfo bankInfo)  throws BusinessException;
	
	public IRPlusResponseDetails getBranchInfo(Long branchId)  throws BusinessException;

	public IRPlusResponseDetails filterBranches(BankFilterInfo bankFilterInfo) throws BusinessException;

	


}


/*
public IRPlusResponseDetails createBank(BankInfo bankInfo) throws BusinessException;

public IRPlusResponseDetails listBanks(String siteId) throws BusinessException;

public IRPlusResponseDetails updateBank(BankInfo bankInfo) throws BusinessException;

public IRPlusResponseDetails deleteBank(String bankId) throws BusinessException;
*/

