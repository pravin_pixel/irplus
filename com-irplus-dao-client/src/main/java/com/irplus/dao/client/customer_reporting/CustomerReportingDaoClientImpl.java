package com.irplus.dao.client.customer_reporting;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.irplus.dao.customerreporting.ICustomerReportingDao;
import com.irplus.dto.BankReportingsInfo;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.customerreporting.CustomerRepotingBean;
import com.irplus.dto.customerreporting.CustomerRepotingResponseDetails;
import com.irplus.util.BusinessException;

@Component
public class CustomerReportingDaoClientImpl implements ICustomerReportingDaoClient {

	@Autowired
	ICustomerReportingDao iCustomerReportingDao;
	
	@Override
	public IRPlusResponseDetails createCustomerReport(CustomerRepotingBean customerReportingBean)
			throws BusinessException {

	//	return iCustomerReportingDao.createCustomerReport(customerReportingBean);
		return iCustomerReportingDao.createCustomerReport(customerReportingBean);
	}

	@Override
	public IRPlusResponseDetails getCustomerReportById(String custommerReportingId) throws BusinessException {

		return iCustomerReportingDao.getCustomerReportById(custommerReportingId);
	}

	@Override
	public CustomerRepotingResponseDetails updateCustomerReports(CustomerRepotingBean customerReportingBean)
			throws BusinessException {

		return iCustomerReportingDao.updateCustomerReports(customerReportingBean);
	}

	@Override
	public CustomerRepotingResponseDetails deleteCustomerReportById(String custommerReportingId)
			throws BusinessException {

		return iCustomerReportingDao.deleteCustomerReportById(custommerReportingId);
	}

	@Override
	public CustomerRepotingResponseDetails showAllCustomerReports() throws BusinessException {

		return iCustomerReportingDao.showAllCustomerReports();
	}

	@Override
	public IRPlusResponseDetails dynamicCustomerReportsBybankId(BankReportingsInfo banksReportsInfo)
			throws BusinessException {

		return iCustomerReportingDao.dynamicCustomerReportsBybankId(banksReportsInfo);
	}

	
}
