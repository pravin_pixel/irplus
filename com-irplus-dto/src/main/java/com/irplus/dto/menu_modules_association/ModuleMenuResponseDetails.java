package com.irplus.dto.menu_modules_association;

import java.util.List;

public class ModuleMenuResponseDetails {

	private boolean isValidationSuccess;
    private int statusCode;
    private String statusMsg;
    private List<String> errMsgs;
    private List<ModuleMenuBean> moduleMenuBeans;
    
    public List<ModuleMenuBean> getModuleMenuBeans() {
		return moduleMenuBeans;
	}
	public void setModuleMenuBeans(List<ModuleMenuBean> moduleMenuBeans) {
		this.moduleMenuBeans = moduleMenuBeans;
	}
	public boolean isValidationSuccess() {
		return isValidationSuccess;
	}
	public void setValidationSuccess(boolean isValidationSuccess) {
		this.isValidationSuccess = isValidationSuccess;
	}
	public int getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
	public String getStatusMsg() {
		return statusMsg;
	}
	public void setStatusMsg(String statusMsg) {
		this.statusMsg = statusMsg;
	}
	
	public List<String> getErrMsgs() {
		return errMsgs;
	}
	public void setErrMsgs(List<String> errMsgs) {
		this.errMsgs = errMsgs;
	}
}