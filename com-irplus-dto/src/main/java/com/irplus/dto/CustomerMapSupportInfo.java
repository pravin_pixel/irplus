package com.irplus.dto;

public class CustomerMapSupportInfo {

	
	CustomerMappingInfo customerMappingInfo[];

	public CustomerMappingInfo[] getCustomerMappingInfo() {
		return customerMappingInfo;
	}

	public void setCustomerMappingInfo(CustomerMappingInfo[] customerMappingInfo) {
		this.customerMappingInfo = customerMappingInfo;
	}
	
	
	
}
