package com.irplus.dto;

public class BankReportingsInfo {

	private Long bankId;
	private Integer reportId;
	
	private String[] reports;
	private Integer[] moduleId;
	
	private Integer userid;	
	private Integer isactive;

	public Long getBankId(){
		return bankId;
	}
	public void setBankId(Long bankId) {
		this.bankId = bankId;
	}
	public Integer getReportId() {
		return reportId;
	}
	public void setReportId(Integer reportId) {
		this.reportId = reportId;
	}
	public Integer getUserid() {
		return userid;
	}
	public Integer[] getModuleId() {
		return moduleId;
	}
	public void setModuleId(Integer[] moduleId) {
		this.moduleId = moduleId;
	}
	public void setUserid(Integer userid) {
		this.userid = userid;
	}
	public String[] getReports() {
		return reports;
	}
	public void setReports(String[] reports) {
		this.reports = reports;
	}
	public Integer getIsactive() {
		return isactive;
	}
	public void setIsactive(Integer isactive) {
		this.isactive = isactive;
	}
			
}