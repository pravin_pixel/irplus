package com.irplus.dto;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class FieldTypesInfo implements java.io.Serializable {

	private int fieldTypeId;
	private Integer usersId;
	private String fieldFormat;
	private Integer defaultLength;
	private Integer isactive;
	private Date createddate;
	private Date modifieddate;
	private Set irSFormvalidations = new HashSet(0);

	public FieldTypesInfo() {
	}

	public FieldTypesInfo(int fieldTypeId) {
		this.fieldTypeId = fieldTypeId;
	}

	public FieldTypesInfo(int fieldTypeId, Integer usersId, String fieldFormat, Integer defaultLength,
			Integer isactive, Date createddate, Date modifieddate, Set irSFormvalidations) {
		this.fieldTypeId = fieldTypeId;
		this.usersId = usersId;
		this.fieldFormat = fieldFormat;
		this.defaultLength = defaultLength;
		this.isactive = isactive;
		this.createddate = createddate;
		this.modifieddate = modifieddate;
		this.irSFormvalidations = irSFormvalidations;
	}

	public int getFieldTypeId() {
		return this.fieldTypeId;
	}

	public void setFieldTypeId(int fieldTypeId) {
		this.fieldTypeId = fieldTypeId;
	}


	public Integer getUsersId() {
		return usersId;
	}

	public void setUsersId(Integer usersId) {
		this.usersId = usersId;
	}

	public String getFieldFormat() {
		return this.fieldFormat;
	}

	public void setFieldFormat(String fieldFormat) {
		this.fieldFormat = fieldFormat;
	}

	public Integer getDefaultLength() {
		return this.defaultLength;
	}

	public void setDefaultLength(Integer defaultLength) {
		this.defaultLength = defaultLength;
	}

	public Integer getIsactive() {
		return this.isactive;
	}

	public void setIsactive(Integer isactive) {
		this.isactive = isactive;
	}

	public Date getCreateddate() {
		return this.createddate;
	}

	public void setCreateddate(Date createddate) {
		this.createddate = createddate;
	}

	public Date getModifieddate() {
		return this.modifieddate;
	}

	public void setModifieddate(Date modifieddate) {
		this.modifieddate = modifieddate;
	}

	public Set getIrSFormvalidations() {
		return this.irSFormvalidations;
	}

	public void setIrSFormvalidations(Set irSFormvalidations) {
		this.irSFormvalidations = irSFormvalidations;
	}

	@Override
	public String toString() {
		return "FieldtypesInfo [fieldTypeId=" + fieldTypeId + ", usersId=" + usersId + ", fieldFormat=" + fieldFormat
				+ ", defaultLength=" + defaultLength + ", isactive=" + isactive + ", createddate=" + createddate
				+ ", modifieddate=" + modifieddate + ", irSFormvalidations=" + irSFormvalidations + "]";
	}
	
}
