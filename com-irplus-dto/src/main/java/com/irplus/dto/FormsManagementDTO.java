package com.irplus.dto;

import java.util.Date;

public class FormsManagementDTO {

	
	/*private IrMUsers irMUsers;
	private IrSBankBranch irSBankbranch;
	private IrSCustomers irSCustomers;*/
	
	private Integer formSetupId;
	private Integer userid;
	private Long  bankbranchId;
	private Integer customerid;
	public Integer getTotalfields() {
		return totalfields;
	}
	public void setTotalfields(Integer totalfields) {
		this.totalfields = totalfields;
	}
	private Integer totalfields;
	private String formCode;
	private String formName;
	private String batchClassDataentry;
	private String batchClassValidation;
	private String watchFolderDataentry;
	private String watchFolderValidation;
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	private String companyName;
	private Boolean isdefault;
	private Integer isactive;
	private Date createddate;
	private Date modifieddate;
	
	private DataEntryFormValidationDTO[] dataEntryFormValidationDTO; 
	
	private Integer formValidatnsLength;
		
	public Integer getFormSetupId() {
		return formSetupId;
	}
	public void setFormSetupId(Integer formSetupId) {
		this.formSetupId = formSetupId;
	}
	public Integer getUserid() {
		return userid;
	}
	public void setUserid(Integer userid) {
		this.userid = userid;
	}
	public Long getBankbranchId() {
		return bankbranchId;
	}
	public void setBankbranchId(Long bankbranchId) {
		this.bankbranchId = bankbranchId;
	}
	public Integer getCustomerid() {
		return customerid;
	}
	public void setCustomerid(Integer customerid) {
		this.customerid = customerid;
	}
	public String getFormCode() {
		return formCode;
	}
	public Integer getFormValidatnsLength() {
		return formValidatnsLength;
	}
	public void setFormValidatnsLength(Integer formValidatnsLength) {
		this.formValidatnsLength = formValidatnsLength;
	}
	
	/*	public Integer getFieldtypid() {
		return fieldtypid;
	}
	public void setFieldtypid(Integer fieldtypid) {
		this.fieldtypid = fieldtypid;
	}
	public String getFieldtypeName() {
		return fieldtypeName;
	}
	public void setFieldtypeName(String fieldtypeName) {
		this.fieldtypeName = fieldtypeName;
	}*/
	public void setFormCode(String formCode) {
		this.formCode = formCode;
	}
	public String getFormName() {
		return formName;
	}
	public void setFormName(String formName) {
		this.formName = formName;
	}
	public String getBatchClassDataentry() {
		return batchClassDataentry;
	}
	public void setBatchClassDataentry(String batchClassDataentry) {
		this.batchClassDataentry = batchClassDataentry;
	}
	public String getBatchClassValidation() {
		return batchClassValidation;
	}
	public void setBatchClassValidation(String batchClassValidation) {
		this.batchClassValidation = batchClassValidation;
	}
	public String getWatchFolderDataentry() {
		return watchFolderDataentry;
	}
	public void setWatchFolderDataentry(String watchFolderDataentry) {
		this.watchFolderDataentry = watchFolderDataentry;
	}
	public String getWatchFolderValidation() {
		return watchFolderValidation;
	}
	public void setWatchFolderValidation(String watchFolderValidation) {
		this.watchFolderValidation = watchFolderValidation;
	}
	public DataEntryFormValidationDTO[] getDataEntryFormValidationDTO() {
		return dataEntryFormValidationDTO;
	}
	public void setDataEntryFormValidationDTO(DataEntryFormValidationDTO[] dataEntryFormValidationDTO) {
		this.dataEntryFormValidationDTO = dataEntryFormValidationDTO;
	}
	public Boolean getIsdefault() {
		return isdefault;
	}
	public void setIsdefault(Boolean isdefault) {
		this.isdefault = isdefault;
	}
	public Integer getIsactive() {
		return isactive;
	}
/*	public Integer getValidationId() {
		return validationId;
	}
	public void setValidationId(Integer validationId) {
		this.validationId = validationId;
	}*/
	
/*	public IrMFieldtypes getIrMFieldtypes() {
		return irMFieldtypes;
	}
	public void setIrMFieldtypes(IrMFieldtypes irMFieldtypes) {
		this.irMFieldtypes = irMFieldtypes;
	}
	*/
/*	
	public Integer getFormsetupid() {
		return formsetupid;
	}
	public void setFormsetupid(Integer formsetupid) {
		this.formsetupid = formsetupid;
	}
	public Integer getFieldLength() {
		return fieldLength;
	}
	public void setFieldLength(Integer fieldLength) {
		this.fieldLength = fieldLength;
	}
	public Boolean getHasValidation() {
		return hasValidation;
	}
	public void setHasValidation(Boolean hasValidation) {
		this.hasValidation = hasValidation;
	}
	*/
	
	public void setIsactive(Integer isactive) {
		this.isactive = isactive;
	}
	public Date getCreateddate() {
		return createddate;
	}
	public void setCreateddate(Date createddate) {
		this.createddate = createddate;
	}
	public Date getModifieddate() {
		return modifieddate;
	}
	public void setModifieddate(Date modifieddate) {
		this.modifieddate = modifieddate;
	}
	
	
}
