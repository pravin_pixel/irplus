package com.irplus.dto;

public class DataEntryFormValidationDTO {

	private Integer userId;
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	private Integer validationId;	
	private Integer fieldtypid;		
	private String fieldName;		

	public String getFieldType() {
		return fieldType;
	}
	public void setFieldType(String fieldType) {
		this.fieldType = fieldType;
	}
	private String fieldType;
	private Integer formsetupid;
	private Integer fieldLength;
	private Boolean hasValidation;
	public Integer getIsactive() {
		return isactive;
	}
	public void setIsactive(Integer isactive) {
		this.isactive = isactive;
	}
	private Integer isactive;
	public DataEntryFormValidationDTO(){}

	public Integer getValidationId() {
		return validationId;
	}
	public void setValidationId(Integer validationId) {
		this.validationId = validationId;
	}
	public Integer getFieldtypid() {
		return fieldtypid;
	}
	public void setFieldtypid(Integer fieldtypid) {
		this.fieldtypid = fieldtypid;
	}

	public String getFieldName() {
		return fieldName;
	}
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	public Integer getFormsetupid() {
		return formsetupid;
	}
	public void setFormsetupid(Integer formsetupid) {
		this.formsetupid = formsetupid;
	}
	public Integer getFieldLength() {
		return fieldLength;
	}
	public void setFieldLength(Integer fieldLength) {
		this.fieldLength = fieldLength;
	}
	public Boolean getHasValidation() {
		return hasValidation;
	}
	public void setHasValidation(Boolean hasValidation) {
		this.hasValidation = hasValidation;
	}

	
}
