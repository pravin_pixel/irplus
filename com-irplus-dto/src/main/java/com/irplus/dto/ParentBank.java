
package com.irplus.dto;

import java.util.Map;

public class ParentBank {

	private Map<Long, String> parentBank;
	
	private BankBranchInfo bankBranchInfo[];

	public ParentBank() {

	}

	public Map<Long, String> getParentBank() {
		return parentBank;
	}

	public void setParentBank(Map<Long, String> parentBank) {
		this.parentBank = parentBank;
	}

	public BankBranchInfo[] getBankBranchInfo() {
		return bankBranchInfo;
	}

	public void setBankBranchInfo(BankBranchInfo[] bankBranchInfo) {
		this.bankBranchInfo = bankBranchInfo;
	}

	

}
