package com.irplus.dto.fileprocessing;

public class RemittanceEntryDetailRecordBean {
	private int remittanceId;
	private String recordTypeCode;
	private String transactionCode;
	private String individualIdentificationNumber;
	private String individualName;
	private String addendaRecordIndicator;
	private String traceNumber;
	private String discretionaryData;
	private FileHeaderRecordBean fileHeaderRecordBean;
	private BatchHeaderRecordBean batchHeaderRecordBean;
	
	public int getRemittanceId() {
		return remittanceId;
	}

	public void setRemittanceId(int remittanceId) {
		this.remittanceId = remittanceId;
	}

	public String getRecordTypeCode() {
		return recordTypeCode;
	}

	public void setRecordTypeCode(String recordTypeCode) {
		this.recordTypeCode = recordTypeCode;
	}

	public String getTransactionCode() {
		return transactionCode;
	}

	public void setTransactionCode(String transactionCode) {
		this.transactionCode = transactionCode;
	}

	public String getIndividualIdentificationNumber() {
		return individualIdentificationNumber;
	}

	public void setIndividualIdentificationNumber(String individualIdentificationNumber) {
		this.individualIdentificationNumber = individualIdentificationNumber;
	}

	public String getIndividualName() {
		return individualName;
	}

	public void setIndividualName(String individualName) {
		this.individualName = individualName;
	}

	public String getAddendaRecordIndicator() {
		return addendaRecordIndicator;
	}

	public void setAddendaRecordIndicator(String addendaRecordIndicator) {
		this.addendaRecordIndicator = addendaRecordIndicator;
	}

	public String getTraceNumber() {
		return traceNumber;
	}

	public void setTraceNumber(String traceNumber) {
		this.traceNumber = traceNumber;
	}

	public String getDiscretionaryData() {
		return discretionaryData;
	}

	public void setDiscretionaryData(String discretionaryData) {
		this.discretionaryData = discretionaryData;
	}

	public FileHeaderRecordBean getFileHeaderRecordBean() {
		return fileHeaderRecordBean;
	}

	public void setFileHeaderRecordBean(FileHeaderRecordBean fileHeaderRecordBean) {
		this.fileHeaderRecordBean = fileHeaderRecordBean;
	}

	public BatchHeaderRecordBean getBatchHeaderRecordBean() {
		return batchHeaderRecordBean;
	}

	public void setBatchHeaderRecordBean(BatchHeaderRecordBean batchHeaderRecordBean) {
		this.batchHeaderRecordBean = batchHeaderRecordBean;
	}

	public RemittanceEntryDetailRecordBean() {
		
	}

}
