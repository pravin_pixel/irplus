package com.irplus.dto.customerreporting;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import com.irplus.dto.ReportsInfo;

public class CustomerRepotingBean {

	private Integer customerReportId;
	private Integer userid;
	private Long bankBranchId;
	private Long bankId;	
	private Integer moduleId ; 
	private Integer customerId;	
	private Timestamp reportTime[];
	private String reportingTime;
	private Integer reportId;
	private String reportName;
	

	/*private String[] reportName;
	private String[] reportGenTime;
	private Integer[] bankReportIds;
	private Integer[] moduleIds;
	*/
	
	private List<ReportsInfo> reportsInfo;
	
	private Integer isactive;	
	private Date createddate;
	private Date modifieddate;
	
	public CustomerRepotingBean() {
	}

	public CustomerRepotingBean(Integer customerReportId, Integer userid, Integer customerId, Timestamp reportTime,
			Integer isactive){
		super();
		this.customerReportId = customerReportId;
		this.userid = userid;
		this.customerId = customerId;		
		this.isactive = isactive;		
	}

	public Integer getCustomerReportId() {
		return this.customerReportId;
	}

	public void setCustomerReportId(Integer customerReportId) {
		this.customerReportId = customerReportId;
	}
	public Timestamp[] getReportTime() {
		return reportTime;
	}

	public String getReportingTime() {
		return reportingTime;
	}
	public Integer getReportId() {
		return reportId;
	}

	public void setReportId(Integer reportId) {
		this.reportId = reportId;
	}

	public String getReportName() {
		return reportName;
	}

	public void setReportName(String reportName) {
		this.reportName = reportName;
	}


	public void setReportingTime(String reportingTime) {
		this.reportingTime = reportingTime;
	}

	public void setReportTime(Timestamp[] reportTime) {
		this.reportTime = reportTime;
	}

/*	public String[] getReportName() {
		return reportName;
	}

	public String[] getReportGenTime() {
		return reportGenTime;
	}

	public void setReportGenTime(String[] reportGenTime) {
		this.reportGenTime = reportGenTime;
	}

	public void setReportName(String[] reportName) {
		this.reportName = reportName;
	}

	public Integer[] getBankReportIds() {
		return bankReportIds;
	}

	public void setBankReportIds(Integer[] bankReportIds) {
		this.bankReportIds = bankReportIds;
	}

	public Integer[] getModuleIds() {
		return moduleIds;
	}

	public void setModuleIds(Integer[] moduleIds) {
		this.moduleIds = moduleIds;
	}*/

	public Integer getIsactive() {
		return this.isactive;
	}

	public Long getBankId() {
		return bankId;
	}

	public void setBankId(Long bankId) {
		this.bankId = bankId;
	}

	public void setIsactive(Integer isactive) {
		this.isactive = isactive;
	}

	public Integer getModuleId() {
		return moduleId;
	}

	public void setModuleId(Integer moduleId) {
		this.moduleId = moduleId;
	}

	public List<ReportsInfo> getReportsInfo() {
		return reportsInfo;
	}

	public void setReportsInfo(List<ReportsInfo> reportsInfo) {
		this.reportsInfo = reportsInfo;
	}

	public Date getCreateddate() {
		return createddate;
	}

	public void setCreateddate(Date createddate) {
		this.createddate = createddate;
	}

	public Date getModifieddate() {
		return modifieddate;
	}

	public void setModifieddate(Date modifieddate) {
		this.modifieddate = modifieddate;
	}

	public Integer getUserid() {
		return userid;
	}

	public Long getBankBranchId() {
		return bankBranchId;
	}

	public void setBankBranchId(Long bankBranchId) {
		this.bankBranchId = bankBranchId;
	}

	public void setUserid(Integer userid) {
		this.userid = userid;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

}