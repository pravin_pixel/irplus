package com.irplus.dto;

import java.sql.Timestamp;

public class CustomerMappingInfo {

	private Integer customerMapId;

	private Integer natchafld;
	
	public Integer getNatchafld() {
		return natchafld;
	}
	public void setNatchafld(Integer natchafld) {
		this.natchafld = natchafld;
	}
	private String bankName;
	private String referenceData;
	private Integer usersid;
	private Long bankbranchid;
	private Integer customersId;
	private Integer isactive;
	
	
	private Timestamp createddate;
	private Timestamp modifieddate;
	
	
	public Integer getCustomerMapId() {
		return customerMapId;
	}
	public void setCustomerMapId(Integer customerMapId) {
		this.customerMapId = customerMapId;
	}

	public String getReferenceData() {
		return referenceData;
	}
	public void setReferenceData(String referenceData) {
		this.referenceData = referenceData;
	}
	public Integer getUsersid() {
		return usersid;
	}
	public void setUsersid(Integer usersid) {
		this.usersid = usersid;
	}
	public Long getBankbranchid() {
		return bankbranchid;
	}
	public void setBankbranchid(Long bankbranchid) {
		this.bankbranchid = bankbranchid;
	}
	public Integer getCustomersId() {
		return customersId;
	}
	public void setCustomersId(Integer customersId) {
		this.customersId = customersId;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public Timestamp getCreateddate() {
		return createddate;
	}
	public void setCreateddate(Timestamp createddate) {
		this.createddate = createddate;
	}
	public Timestamp getModifieddate() {
		return modifieddate;
	}
	public Integer getIsactive() {
		return isactive;
	}
	public void setIsactive(Integer isactive) {
		this.isactive = isactive;
	}
	public void setModifieddate(Timestamp modifieddate) {
		this.modifieddate = modifieddate;
	}
	
}
