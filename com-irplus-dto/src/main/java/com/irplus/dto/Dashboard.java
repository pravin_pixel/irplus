package com.irplus.dto;

import java.util.ArrayList;
import java.util.List;

public class Dashboard {

	private int totalBanks;
	private int totalRoles;
	private int totalUsers;
	private int totalCustomerCount;
	public String getDay() {
		return day;
	}
	public void setDay(String day) {
		this.day = day;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	private String day;
	private String month;
	private String year;
	
	public int getTotalCustomerCount() {
		return totalCustomerCount;
	}
	public void setTotalCustomerCount(int totalCustomerCount) {
		this.totalCustomerCount = totalCustomerCount;
	}
	private List<BankInfo> bankInfo = new ArrayList<BankInfo>();
	private List<LocationViewInfo> branchInfo = new ArrayList<LocationViewInfo>();
	
	
	private String secitemcount;
	public String getSecitemcount() {
		return secitemcount;
	}
	public void setSecitemcount(String secitemcount) {
		this.secitemcount = secitemcount;
	}
	public String getThirditemcount() {
		return thirditemcount;
	}
	public void setThirditemcount(String thirditemcount) {
		this.thirditemcount = thirditemcount;
	}
	private String thirditemcount;
	public List<LocationViewInfo> getBranchInfo() {
		return branchInfo;
	}
	public void setBranchInfo(List<LocationViewInfo> branchInfo) {
		this.branchInfo = branchInfo;
	}
	public List<BankInfo> getBankInfo() {
		return bankInfo;
	}
	public void setBankInfo(List<BankInfo> bankInfo) {
		this.bankInfo = bankInfo;
	}
	public Dashboard() {
		
	}
	public int getTotalBanks() {
		return totalBanks;
	}
	public void setTotalBanks(int totalBanks) {
		this.totalBanks = totalBanks;
	}
	public int getTotalRoles() {
		return totalRoles;
	}
	public void setTotalRoles(int totalRoles) {
		this.totalRoles = totalRoles;
	}
	public int getTotalUsers() {
		return totalUsers;
	}
	public void setTotalUsers(int totalUsers) {
		this.totalUsers = totalUsers;
	}
	
	public String getFirstdate() {
		return firstdate;
	}
	public void setFirstdate(String firstdate) {
		this.firstdate = firstdate;
	}
	public String getSecdate() {
		return secdate;
	}
	public void setSecdate(String secdate) {
		this.secdate = secdate;
	}
	public String getThirddate() {
		return thirddate;
	}
	public void setThirddate(String thirddate) {
		this.thirddate = thirddate;
	}
	public int getTotalBatches() {
		return totalBatches;
	}
	public void setTotalBatches(int totalBatches) {
		this.totalBatches = totalBatches;
	}
	public String getTotalTransactions() {
		return totalTransactions;
	}
	public void setTotalTransactions(String totalTransactions) {
		this.totalTransactions = totalTransactions;
	}
	public String getTotalCreditAmt() {
		return totalCreditAmt;
	}
	public void setTotalCreditAmt(String totalCreditAmt) {
		this.totalCreditAmt = totalCreditAmt;
	}
	public String getTotalDebitAmt() {
		return totalDebitAmt;
	}
	public void setTotalDebitAmt(String totalDebitAmt) {
		this.totalDebitAmt = totalDebitAmt;
	}
	private int sectotalFiles;
	public int getSectotalFiles() {
		return sectotalFiles;
	}
	public void setSectotalFiles(int sectotalFiles) {
		this.sectotalFiles = sectotalFiles;
	}
	public int getThirdtotalFiles() {
		return thirdtotalFiles;
	}
	public void setThirdtotalFiles(int thirdtotalFiles) {
		this.thirdtotalFiles = thirdtotalFiles;
	}
	
	public String getTotaltransaction() {
		return totaltransaction;
	}
	public void setTotaltransaction(String totaltransaction) {
		this.totaltransaction = totaltransaction;
	}
	public String getTotalitems() {
		return totalitems;
	}
	public void setTotalitems(String totalitems) {
		this.totalitems = totalitems;
	}
	public String getTotalBatchamt() {
		return totalBatchamt;
	}
	public void setTotalBatchamt(String totalBatchamt) {
		this.totalBatchamt = totalBatchamt;
	}
	private String totaltransaction;
	private String totalitems;
	private String totalBatchamt;
	
	private int singlefileBatch;
	
	public int getSinglefileBatch() {
		return singlefileBatch;
	}
	public void setSinglefileBatch(int singlefileBatch) {
		this.singlefileBatch = singlefileBatch;
	}
	private int thirdtotalFiles;
	
	private String totalItemcount;
	public String getTotalItemcount() {
		return totalItemcount;
	}
	public void setTotalItemcount(String totalItemcount) {
		this.totalItemcount = totalItemcount;
	}
	private String firstdate;
	private String secdate;
	private String thirddate;
	public int getThirdtotalBatches() {
		return thirdtotalBatches;
	}
	public void setThirdtotalBatches(int thirdtotalBatches) {
		this.thirdtotalBatches = thirdtotalBatches;
	}
	private int thirdtotalBatches;
	private int totalBatches;
	private String totalTransactions;
	private String totalCreditAmt;
	private String totalDebitAmt;
	private String thirdtotalTransactions;
	public String getThirdtotalTransactions() {
		return thirdtotalTransactions;
	}
	public void setThirdtotalTransactions(String thirdtotalTransactions) {
		this.thirdtotalTransactions = thirdtotalTransactions;
	}
	public String getThirdtotalCreditAmt() {
		return thirdtotalCreditAmt;
	}
	public void setThirdtotalCreditAmt(String thirdtotalCreditAmt) {
		this.thirdtotalCreditAmt = thirdtotalCreditAmt;
	}
	public String getThirdtotalDebitAmt() {
		return thirdtotalDebitAmt;
	}
	public void setThirdtotalDebitAmt(String thirdtotalDebitAmt) {
		this.thirdtotalDebitAmt = thirdtotalDebitAmt;
	}
	private String thirdtotalCreditAmt;
	private String thirdtotalDebitAmt;
	public int getSectotalBatches() {
		return sectotalBatches;
	}
	public void setSectotalBatches(int sectotalBatches) {
		this.sectotalBatches = sectotalBatches;
	}
	public String getSectotalTransactions() {
		return sectotalTransactions;
	}
	public void setSectotalTransactions(String sectotalTransactions) {
		this.sectotalTransactions = sectotalTransactions;
	}
	public String getSectotalCreditAmt() {
		return sectotalCreditAmt;
	}
	public void setSectotalCreditAmt(String sectotalCreditAmt) {
		this.sectotalCreditAmt = sectotalCreditAmt;
	}
	public String getSectotalDebitAmt() {
		return sectotalDebitAmt;
	}
	public void setSectotalDebitAmt(String sectotalDebitAmt) {
		this.sectotalDebitAmt = sectotalDebitAmt;
	}
	private String achfilename;
	public String getAchfilename() {
		return achfilename;
	}
	public void setAchfilename(String achfilename) {
		this.achfilename = achfilename;
	}
	private int sectotalBatches;
	private String sectotalTransactions;
	private String sectotalCreditAmt;
	private String sectotalDebitAmt;
	public int getTotalFiles() {
		return totalFiles;
	}
	public void setTotalFiles(int totalFiles) {
		this.totalFiles = totalFiles;
	}

	private int totalFiles;
}
