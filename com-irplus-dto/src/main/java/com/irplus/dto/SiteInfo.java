package com.irplus.dto;

import java.util.Arrays;

public class SiteInfo {
	
	private Long siteId;
	private String siteName;
	private String siteCode;
	private String contactPerson;
	private String contactNo;
	private String address1;
	
	private int isactive;
	private String siteLogo;
	
	private String rootFolderPath;
	private String emphesoftXmlFolderPath;
	private String imagehawkXmlFolderPath;
	private String outputTransmissionFolderPath;
	private String archiveProcessFilePath;
	
	private DBDTO dbInfo;
	
	
	
	
	
	public DBDTO getDbInfo() {
		return dbInfo;
	}

	public void setDbInfo(DBDTO dbInfo) {
		this.dbInfo = dbInfo;
	}

	public String getRootFolderPath() {
		return rootFolderPath;
	}

	public void setRootFolderPath(String rootFolderPath) {
		this.rootFolderPath = rootFolderPath;
	}

	public String getEmphesoftXmlFolderPath() {
		return emphesoftXmlFolderPath;
	}

	public void setEmphesoftXmlFolderPath(String emphesoftXmlFolderPath) {
		this.emphesoftXmlFolderPath = emphesoftXmlFolderPath;
	}

	public String getImagehawkXmlFolderPath() {
		return imagehawkXmlFolderPath;
	}

	public void setImagehawkXmlFolderPath(String imagehawkXmlFolderPath) {
		this.imagehawkXmlFolderPath = imagehawkXmlFolderPath;
	}

	public String getOutputTransmissionFolderPath() {
		return outputTransmissionFolderPath;
	}

	public void setOutputTransmissionFolderPath(String outputTransmissionFolderPath) {
		this.outputTransmissionFolderPath = outputTransmissionFolderPath;
	}

	public String getArchiveProcessFilePath() {
		return archiveProcessFilePath;
	}

	public void setArchiveProcessFilePath(String archiveProcessFilePath) {
		this.archiveProcessFilePath = archiveProcessFilePath;
	}

	public String getSiteLogo() {
		return siteLogo;
	}

	public void setSiteLogo(String siteLogo) {
		this.siteLogo = siteLogo;
	}
	private LicenseInfo licenseInfo;
	
	public SiteInfo(){
		
	}
	
	public LicenseInfo getLicenseInfo() {
		return licenseInfo;
	}

	public void setLicenseInfo(LicenseInfo licenseInfo) {
		this.licenseInfo = licenseInfo;
	}

	public Long getSiteId() {
		return siteId;
	}
	public void setSiteId(Long siteId) {
		this.siteId = siteId;
	}
	public String getSiteName() {
		return siteName;
	}
	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}
	public String getSiteCode() {
		return siteCode;
	}
	public void setSiteCode(String siteCode) {
		this.siteCode = siteCode;
	}
	public String getContactPerson() {
		return contactPerson;
	}
	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}
	public String getContactNo() {
		return contactNo;
	}
	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	
	public int getIsactive() {
		return isactive;
	}
	public void setIsactive(int isactive) {
		this.isactive = isactive;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SiteInfo [siteId=").append(siteId).append(", siteName=").append(siteName)
				.append(", siteCode=").append(siteCode).append(", contactPerson=").append(contactPerson)
				.append(", contactNo=").append(contactNo).append(", isactive=").append(isactive).append(", address1=")
				.append(address1).append(", siteLogo=").append(siteLogo).append(", rootFolderPath=").append(rootFolderPath)
				.append(", emphesoftXmlFolderPath=").append(emphesoftXmlFolderPath).append(", imagehawkXmlFolderPath=").append(imagehawkXmlFolderPath)
				.append(", outputTransmissionFolderPath=").append(outputTransmissionFolderPath).append(", archiveProcessFilePath=").append(archiveProcessFilePath).append(", dbInfo=")
				.append(dbInfo).append(", licenseInfo=").append(licenseInfo)
				.append("]");
		return builder.toString();
	}
}
