package com.irplus.dto.rolepermission;

import java.util.Date;

public class RolepermissionsBean {

	private Integer permissionid;
	private Integer modulemenusId;
	private Integer rolesId;
	private String roleName;
	private Integer statusId;	
	private Integer userId;
	private Integer isactive; 
	private Boolean addprm;
	private Boolean editprm;
	private Boolean deleteprm;
	private Boolean viewprm;
	private Boolean approval;
	
	private Date createddate;
	private Date modifieddate;
	
//	private ModuleMenuBean modulemenuArray[];
	
	public RolepermissionsBean() {
	}

	public Integer getPermissionid() {
		return this.permissionid;
	}

	public void setPermissionid(Integer permissionid) {
		this.permissionid = permissionid;
	}
	
	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public Boolean getAddprm() {
		return this.addprm;
	}

	public void setAddprm(Boolean addprm) {
		this.addprm = addprm;
	}

	public Boolean getEditprm() {
		return this.editprm;
	}
	
	public Integer getIsactive() {
		return isactive;
	}
	
	public void setIsactive(Integer isactive) {
		this.isactive = isactive;
	}
	
	public void setEditprm(Boolean editprm) {
		this.editprm = editprm;
	}

	public Boolean getDeleteprm() {
		return this.deleteprm;
	}

	public void setDeleteprm(Boolean deleteprm) {
		this.deleteprm = deleteprm;
	}

	public Boolean getViewprm() {
		return this.viewprm;
	}

	public void setViewprm(Boolean viewprm) {
		this.viewprm = viewprm;
	}

	public Boolean getApproval() {
		return this.approval;
	}

	public Integer getModulemenusId() {
		return modulemenusId;
	}


	public void setModulemenusId(Integer modulemenusId) {
		this.modulemenusId = modulemenusId;
	}


	public Integer getRolesId() {
		return rolesId;
	}


	public void setRolesId(Integer rolesId) {
		this.rolesId = rolesId;
	}


	public Integer getStatusId() {
		return statusId;
	}


	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}

	
	public Integer getUserId() {
		return userId;
	}


	public void setUserId(Integer userId) {
		this.userId = userId;
	}


	public void setApproval(Boolean approval) {
		this.approval = approval;
	}

/*	public ModuleMenuBean[] getModulemenuArray() {
		return modulemenuArray;
	}


	public void setModulemenuArray(ModuleMenuBean[] modulemenuArray) {
		this.modulemenuArray = modulemenuArray;
	}
*/

	public Date getCreateddate() {
		return this.createddate;
	}

	public void setCreateddate(Date createddate) {
		this.createddate = createddate;
	}

	public Date getModifieddate() {
		return this.modifieddate;
	}

	public void setModifieddate(Date modifieddate) {
		this.modifieddate = modifieddate;
	}

}
