package com.irplus.dto;

import java.util.Date;

public class CustomerLicenceInfo {

	private Integer customerLicenseId;
	private Long businessprocessId;
	private Integer userId;
	private Long bankbranchId;
	private Integer customersId;
	
	private Long filetypeid;
	private Integer isactive;
	private Date createddate;
	private Date modifieddate;

	public Long getBusinessprocessId() {
		return businessprocessId;
	}

	public void setBusinessprocessId(Long businessprocessId) {
		this.businessprocessId = businessprocessId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Long getBankbranchId() {
		return bankbranchId;
	}

	public void setBankbranchId(Long bankbranchId) {
		this.bankbranchId = bankbranchId;
	}

	public Integer getCustomersId() {
		return customersId;
	}

	public void setCustomersId(Integer customersId) {
		this.customersId = customersId;
	}

	public CustomerLicenceInfo() {
	}

	public Integer getCustomerLicenseId() {
		return this.customerLicenseId;
	}

	public void setCustomerLicenseId(Integer customerLicenseId) {
		this.customerLicenseId = customerLicenseId;
	}

	public Long getFiletypeid() {
		return filetypeid;
	}

	public void setFiletypeid(Long filetypeid) {
		this.filetypeid = filetypeid;
	}

	public Integer getIsactive() {
		return this.isactive;
	}

	public void setIsactive(Integer isactive) {
		this.isactive = isactive;
	}

	public Date getCreateddate() {
		return this.createddate;
	}

	public void setCreateddate(Date createddate) {
		this.createddate = createddate;
	}

	public Date getModifieddate() {
		return this.modifieddate;
	}

	public void setModifieddate(java.util.Date date) {
		this.modifieddate = date;
	}
	
	public CustomerLicenceInfo(Integer customerLicenseId, Long businessprocessId, Integer userId,
			Long bankbranchId, Integer customersId, Long filetypeid, Integer isactive, Date createddate,
			Date modifieddate) {
		super();
		this.customerLicenseId = customerLicenseId;
		this.businessprocessId = businessprocessId;
		this.userId = userId;
		this.bankbranchId = bankbranchId;
		this.customersId = customersId;
		this.filetypeid = filetypeid;
		this.isactive = isactive;
		this.createddate = createddate;
		this.modifieddate = modifieddate;
	}
}
