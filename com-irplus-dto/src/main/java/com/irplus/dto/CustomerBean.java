package com.irplus.dto;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class CustomerBean{
	
	private static final long serialVersionUID = -3176074231815769436L;

	private Integer customerId;
	private Integer userId;
	
//	private Set<BankInfo> bankSet; 
/*	private Set<BankBranchInfo> bankBranchSet;
*/	
//	private Integer BankBranchId;
	
	private Long BankBranchId;
	private String branchLocation;
	public String getBranchLocation() {
		return branchLocation;
	}

	public void setBranchLocation(String branchLocation) {
		this.branchLocation = branchLocation;
	}


	private String defaultBankName;
	private Long parentBankId;
	private String irCustomerCode;
	private String companyName;
	private String referenceName;
	private String bankCustomerCode;
	private String ddaBankAc;
	private String compIdentNo;
	private String website;
	private String country;
	private String state;
	private String city;
	private String zipcode;
	private Integer isactive;	
	private String photofile;
/*customerContacts information clubbing*/
	
	private String firstName;
	private String lastName;
	private String contactRole;
	private String contactNo;
	private String emailId;
	private String address1;
	private String address2;
	private Boolean isdefault;
	private Integer custmContacts_primrkey_Id;
	private CustomerContactsBean[] customerContact;
	private Date createddate;
	private Date modifieddate;
	
	
	private String bplist;
	private String ftypelist;
	
	private List<BusinessProcessInfo> bpList;
	
	public List<BusinessProcessInfo> getBpList() {
		return bpList;
	}

	public void setBpList(List<BusinessProcessInfo> bpList) {
		this.bpList = bpList;
	}

	public CustomerBean() {
	}

	public String getBplist() {
		return bplist;
	}

	public void setBplist(String bplist) {
		this.bplist = bplist;
	}

	public String getFtypelist() {
		return ftypelist;
	}

	public void setFtypelist(String ftypelist) {
		this.ftypelist = ftypelist;
	}

	public Integer getCustomerId() {
		return this.customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	
	public String getIrCustomerCode() {
		return this.irCustomerCode;
	}
	
	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Long getParentBankId() {
		return parentBankId;
	}

	public void setParentBankId(Long parentBankId) {
		this.parentBankId = parentBankId;
	}

	public void setIrCustomerCode(String irCustomerCode) {
		this.irCustomerCode = irCustomerCode;
	}

	public String getCompanyName() {
		return this.companyName;
	}

	public String getPhotofile() {
		return photofile;
	}

	public void setPhotofile(String photofile) {
		this.photofile = photofile;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	
	public CustomerContactsBean[] getCustomerContact() {
		return customerContact;
	}

	public void setCustomerContact(CustomerContactsBean[] customerContact) {
		this.customerContact = customerContact;
	}

	public Integer getCustmContacts_primrkey_Id() {
		return custmContacts_primrkey_Id;
	}

	public void setCustmContacts_primrkey_Id(Integer custmContacts_primrkey_Id) {
		this.custmContacts_primrkey_Id = custmContacts_primrkey_Id;
	}

	public String getReferenceName() {
		return this.referenceName;
	}

	public void setReferenceName(String referenceName) {
		this.referenceName = referenceName;
	}


	public String getBankCustomerCode() {
		return this.bankCustomerCode;
	}

	public void setBankCustomerCode(String bankCustomerCode) {
		this.bankCustomerCode = bankCustomerCode;
	}

	public String getDdaBankAc() {
		return this.ddaBankAc;
	}

	public void setDdaBankAc(String ddaBankAc) {
		this.ddaBankAc = ddaBankAc;
	}

	public String getCompIdentNo() {
		return this.compIdentNo;
	}

	public void setCompIdentNo(String compIdentNo) {
		this.compIdentNo = compIdentNo;
	}



	public Boolean getIsdefault() {
		return isdefault;
	}

	public Long getBankBranchId() {
		return BankBranchId;
	}

	public String getDefaultBankName() {
		return defaultBankName;
	}

	public void setDefaultBankName(String defaultBankName) {
		this.defaultBankName = defaultBankName;
	}

	public void setBankBranchId(Long bankBranchId) {
		BankBranchId = bankBranchId;
	}

	public void setIsdefault(Boolean isdefault) {
		this.isdefault = isdefault;
	}

	public String getWebsite() {
		return this.website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getZipcode() {
		return this.zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public Integer getIsactive() {
		return this.isactive;
	}

	public void setIsactive(Integer isactive) {
		this.isactive = isactive;
	}

	public Date getCreateddate() {
		return createddate;
	}

	public void setCreateddate(Date createddate) {
		this.createddate = createddate;
	}

	public Date getModifieddate() {
		return modifieddate;
	}

	public void setModifieddate(Date modifieddate) {
		this.modifieddate = modifieddate;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getContactRole() {
		return contactRole;
	}

	public void setContactRole(String contactRole) {
		this.contactRole = contactRole;
	}

	public String getContactNo() {
		return contactNo;
	}

	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CustomerBean [customerId=").append(customerId).append(", BankBranchId=").append(BankBranchId).append(", branchLocation=").append(branchLocation)
				.append(", defaultBankName=").append(defaultBankName).append(", parentBankId=").append(parentBankId)
				.append(", isdefault=").append(isdefault).append(", isactive=").append(isactive).append(", companyName=")
				.append(companyName).append(", irCustomerCode=").append(irCustomerCode).append(", referenceName=").append(referenceName)
				.append(", bankCustomerCode=").append(bankCustomerCode).append(", customerContact=").append(Arrays.toString(customerContact))
				.append(", ddaBankAc=").append(ddaBankAc).append(", compIdentNo=").append(compIdentNo).append(", photofile=")
				.append(photofile).append(", createddate=").append(createddate).append(", modifieddate=")
				.append(modifieddate).append(", country=").append(country).append(", state=").append(state)
				.append(", ftypelist=").append(ftypelist).append(", bpList=").append(bpList).append(",city=").append(city).append(", bplist=").append(bplist).append(", zipcode=").append(zipcode).append(website).append(", userId=").append(userId)
				.append("]");
		return builder.toString();
	}
	
	
	
	
}
