package com.irplus.dto;

import java.util.Date;

public class DBDTO {


	
	/*This is Host/ip addressing*/	
	private String hostName;
	private String username;
	private String password;
	private int siteid;
	private Date createdDate;
	private Date modifiedDate;

	
	
	public DBDTO() {
		
	}

	
/*	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		result = prime * result + ((hostName == null) ? 0 : hostName.hashCode());
		result = prime * result + ((username == null) ? 0 : username.hashCode());
		return result;
	}
*/

	/*@Override
	public boolean equals(Object obj) {
		
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
				
		
		DBDTO other = (DBDTO) obj;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (hostName == null) {
			if (other.hostName != null)
				return false;
		} else if (!hostName.equals(other.hostName))
			return false;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}
*/

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("DBDTO [username=");
		builder.append(username);
		builder.append(", password=");
		builder.append(password);
		builder.append(", hostName=");
		builder.append(hostName);
		builder.append("]");
		return builder.toString();
	}

	

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	

	public int getSiteid() {
		return siteid;
	}


	public void setSiteid(int siteid) {
		this.siteid = siteid;
	}


	
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}


	public Date getModifiedDate() {
		return modifiedDate;
	}


	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public String getHostName() {
		return hostName;
	}


	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

		
}