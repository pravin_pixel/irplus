package com.irplus.dto;

import java.util.Arrays;
import java.util.Map;

public class CustomerFunctionInfo {

	public CustomerFunctionInfo() {
		
	}
	/*+	 class CustomerLicenceInfo	1 
	 * */ 
		
			private Integer customerLicenseId;
			
			private Long businessprocessId;
			
			private Long businsprocsId[];
			private Integer userId;
			private Long bankbranchId;
			private Integer customersId;
			private Integer filetypeid;
			private Integer isactive;
			
//			class filetypes			2
			
			private Long filetypeId[];
			
			private Map<Long , String> fileType_Id_Names;
			
			//private BankBranch bankBranch;
			private String filetypename;
			private Long licenseid;
			Map<Long, FileTypeInfo> mapCusFun_ids_fileTypes; // It is used in the Get CustomerFunc One 
			
//			Buisiness process		3
			

	//		private Long businessProcessId;
			//private BankBranch bankBranch;
			
			private Map<Long , BusinessProcessInfo> buissines_Ids_ProcNams ;
			
			private String processName;
			private Long licenseId;
			private Character hasVisibility;
			private Character isparent;
		//	private int isactive;												
			
//			CustomerOtherLicencing		4
			
			private Integer otherLicensingId;
			private Integer BusinessprocessId;
			private Long BankbranchId;
			private Integer notification;
			
	//		private IrMBusinessProcess irMBusinessprocess;	
	/*		private IrMUsers irMUsers;
			private IrSBankBranch irSBankBranch;
			private IrSCustomers irSCustomers;
	*/		
			
			
//			IrSCustomerReportLicensing   5
			
			private Integer irMReportfileFormats;
	//		private IrMUsers irMUsers;			
	///		private IrSCustomers irSCustomers;
			
//			private Integer customerReportId;
			
//        Customerreporting   6
				
	//		private IrMUsers irMUsers;	
	//		private IrSBankBranch irSBankbranch;			
	//		private IrSBankreporting irSBankreporting;			
//			private Integer irSCustomersId;
			private Integer irSBankreportingId;

/*			bankreporting   7*/
	//a		
			
			Map<Integer , Integer> report_active_reptid;// reportid and isactive
			
			private Integer reportId;
			
		/*  private IrMModules irMModules;*/

			private Integer irMModulesId;

/*			private IrMUsers irMUsers; */
/*			private IRSBank irSBank;*/
			
			private Integer irSBankId;
			
/*			private Integer isactive;*/
			
			private Integer reportId_activeness;
			
//        report file format 8
			//b
//			private Integer fileFormatOneId;
			
			private Map<Integer , String> file_formats;
			private Integer fileFormatsId[];   // update this function is using
//			private IrMFileTypes irMFiletypes;
			private Integer irMFiletypesId;
//			private IrMUsers irMUsers;
			private String fileformatName;
//			private Integer isactive;
			
			public Integer getCustomerLicenseId() {
				return customerLicenseId;
			}
			public void setCustomerLicenseId(Integer customerLicenseId) {
				this.customerLicenseId = customerLicenseId;
			}
			public Long getBusinessprocessId() {
				return businessprocessId;
			}
			public void setBusinessprocessId(Long businessprocessId) {
				this.businessprocessId = businessprocessId;
			}
			public Integer getUserId() {
				return userId;
			}
			public void setUserId(Integer userId) {
				this.userId = userId;
			}
			public Long getBankbranchId() {
				return bankbranchId;
			}
			public void setBankbranchId(Long bankbranchId) {
				this.bankbranchId = bankbranchId;
			}
			public Integer getCustomersId() {
				return customersId;
			}
			public void setCustomersId(Integer customersId) {
				this.customersId = customersId;
			}
			
			public Long[] getFiletypeId() {
				return filetypeId;
			}
			public Map<Long, BusinessProcessInfo> getBuissines_Ids_ProcNams() {
				return buissines_Ids_ProcNams;
			}
			public void setBuissines_Ids_ProcNams(Map<Long, BusinessProcessInfo> buissines_Ids_ProcNams) {
				this.buissines_Ids_ProcNams = buissines_Ids_ProcNams;
			}
			public Integer getReportId_activeness() {
				return reportId_activeness;
			}
			public void setReportId_activeness(Integer reportId_activeness) {
				this.reportId_activeness = reportId_activeness;
			}
			public void setFiletypeId(Long[] filetypeId) {
				this.filetypeId = filetypeId;
			}
			public Map<Integer, String> getFile_formats() {
				return file_formats;
			}
			public void setFile_formats(Map<Integer, String> file_formats) {
				this.file_formats = file_formats;
			}
			public String getFiletypename() {
				return filetypename;
			}
			public void setFiletypename(String filetypename) {
				this.filetypename = filetypename;
			}
			public Long getLicenseid() {
				return licenseid;
			}
			public Map<Integer, Integer> getReport_active_reptid() {
				return report_active_reptid;
			}
			public void setReport_active_reptid(Map<Integer, Integer> report_active_reptid) {
				this.report_active_reptid = report_active_reptid;
			}
			public Long[] getBusinsprocsId() {
				return businsprocsId;
			}
			public void setBusinsprocsId(Long[] businsprocsId) {
				this.businsprocsId = businsprocsId;
			}
			public void setFileType_Id_Names(Map<Long, String> fileType_Id_Names) {
				this.fileType_Id_Names = fileType_Id_Names;
			}
			public void setLicenseid(Long licenseid) {
				this.licenseid = licenseid;
			}
		/*	
		 * public Long getBusinessProcessId() {
				return businessProcessId;
			}
			public void setBusinessProcessId(Long businessProcessId) {
				this.businessProcessId = businessProcessId;
			}
			*/
			public String getProcessName() {
				return processName;
			}
			public Map getFileType_Id_Names() {
				return fileType_Id_Names;
			}
			
			public void setProcessName(String processName) {
				this.processName = processName;
			}
			public Integer getNotification() {
				return notification;
			}
			public void setNotification(Integer notification) {
				this.notification = notification;
			}
			public Long getLicenseId() {
				return licenseId;
			}
			public Map<Long, FileTypeInfo> getMapCusFun_ids_fileTypes() {
				return mapCusFun_ids_fileTypes;
			}
			public void setMapCusFun_ids_fileTypes(Map<Long, FileTypeInfo> mapCusFun_ids_fileTypes) {
				this.mapCusFun_ids_fileTypes = mapCusFun_ids_fileTypes;
			}
			public void setLicenseId(Long licenseId) {
				this.licenseId = licenseId;
			}
			public Character getHasVisibility() {
				return hasVisibility;
			}
			public void setHasVisibility(Character hasVisibility) {
				this.hasVisibility = hasVisibility;
			}
			public Character getIsparent() {
				return isparent;
			}
			public void setIsparent(Character isparent) {
				this.isparent = isparent;
			}
			public Integer getOtherLicensingId() {
				return otherLicensingId;
			}
			public void setOtherLicensingId(Integer otherLicensingId) {
				this.otherLicensingId = otherLicensingId;
			}
			public Integer getIrMReportfileFormats() {
				return irMReportfileFormats;
			}
			public void setIrMReportfileFormats(Integer irMReportfileFormats) {
				this.irMReportfileFormats = irMReportfileFormats;
			}
		/*	public Integer getIrSCustomersId() {
				return irSCustomersId;
			}
			public void setIrSCustomersId(Integer irSCustomersId) {
				this.irSCustomersId = irSCustomersId;
			}
*/			public Integer getIrSBankreportingId() {
				return irSBankreportingId;
			}
			public void setIrSBankreportingId(Integer irSBankreportingId) {
				this.irSBankreportingId = irSBankreportingId;
			}
			public Integer getReportId() {
				return reportId;
			}
			public Integer[] getFileFormatsId() {
				return fileFormatsId;
			}
			public void setFileFormatsId(Integer[] fileFormatsId) {
				this.fileFormatsId = fileFormatsId;
			}
			public void setReportId(Integer reportId) {
				this.reportId = reportId;
			}
			public Integer getIrMModulesId() {
				return irMModulesId;
			}
			public void setIrMModulesId(Integer irMModulesId) {
				this.irMModulesId = irMModulesId;
			}
			public Integer getIrSBankId() {
				return irSBankId;
			}
			public void setIrSBankId(Integer irSBankId) {
				this.irSBankId = irSBankId;
			}
		
			public Integer getIrMFiletypesId() {
				return irMFiletypesId;
			}
			public void setIrMFiletypesId(Integer irMFiletypesId) {
				this.irMFiletypesId = irMFiletypesId;
			}
			public String getFileformatName() {
				return fileformatName;
			}
			public void setFileformatName(String fileformatName) {
				this.fileformatName = fileformatName;
			}
			public void setBusinessprocessId(Integer businessprocessId) {
				BusinessprocessId = businessprocessId;
			}
			public Integer getFiletypeid() {
				return filetypeid;                           
			}
			public void setFiletypeid(Integer filetypeid) {
				this.filetypeid = filetypeid;
			}
			public Integer getIsactive() {
				return isactive;
			}
			public void setIsactive(Integer isactive) {
				this.isactive = isactive;
			}
			@Override
			public String toString() {
				StringBuilder builder = new StringBuilder();
				builder.append("CustomerFunctionInfo [customerLicenseId=");
				builder.append(customerLicenseId);
				builder.append(", businessprocessId=");
				builder.append(businessprocessId);
				builder.append(", businsprocsId=");
				builder.append(Arrays.toString(businsprocsId));
				builder.append(", userId=");
				builder.append(userId);
				builder.append(", bankbranchId=");
				builder.append(bankbranchId);
				builder.append(", customersId=");
				builder.append(customersId);
				builder.append(", filetypeid=");
				builder.append(filetypeid);
				builder.append(", isactive=");
				builder.append(isactive);
				builder.append(", filetypeId=");
				builder.append(Arrays.toString(filetypeId));
				builder.append(", fileType_Id_Names=");
				builder.append(fileType_Id_Names);
				builder.append(", filetypename=");
				builder.append(filetypename);
				builder.append(", licenseid=");
				builder.append(licenseid);
				builder.append(", processName=");
				builder.append(processName);
				builder.append(", licenseId=");
				builder.append(licenseId);
				builder.append(", hasVisibility=");
				builder.append(hasVisibility);
				builder.append(", isparent=");
				builder.append(isparent);
				builder.append(", otherLicensingId=");
				builder.append(otherLicensingId);
				builder.append(", BusinessprocessId=");
				builder.append(BusinessprocessId);
				builder.append(", BankbranchId=");
				builder.append(BankbranchId);
				builder.append(", notification=");
				builder.append(notification);
				builder.append(", irMReportfileFormats=");
				builder.append(irMReportfileFormats);
				builder.append(", irSCustomersId=");
		//		builder.append(irSCustomersId);
				builder.append(", irSBankreportingId=");
				builder.append(irSBankreportingId);
				builder.append(", reportId=");
				builder.append(reportId);
				builder.append(", irMModulesId=");
				builder.append(irMModulesId);
				builder.append(", irSBankId=");
				builder.append(irSBankId);
				builder.append(", fileFormatId=");
	//			builder.append(fileFormatId);
				builder.append(", fileFormatsId=");
				builder.append(Arrays.toString(fileFormatsId));
				builder.append(", irMFiletypesId=");
				builder.append(irMFiletypesId);
				builder.append(", fileformatName=");
				builder.append(fileformatName);
				builder.append("]");
				return builder.toString();
			}			
	
}