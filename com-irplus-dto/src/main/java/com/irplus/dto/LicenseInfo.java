package com.irplus.dto;

import java.util.Date;
import java.util.List;

import com.irplus.dto.IRPlusBusinessProcess;
import com.irplus.dto.IRPlusFileTypes;

public class LicenseInfo {
	 private static final long serialVersionUID = -3176074231815769436L;
	 
	 private String siteCode;
	 
	 private Long siteId;
	 
	 private String siteName;
	 
	 private String ctctPerson;
	 
	 private String ctctNo;
	 
	 private String address1;
	 
	 private String address2;
	 
	 private String licType;
	 
	 public String getLicType() {
		return licType;
	}

	public void setLicType(String licType) {
		this.licType = licType;
	}

	private String licPath;
	 
	 private String macAddr;
	 
	 private String validFrom;
	 
	 private String validTo;
	 
	 private Long remainingDays;
	 public Long getRemainingDays() {
		return remainingDays;
	}

	public void setRemainingDays(Long remainingDays) {
		this.remainingDays = remainingDays;
	}

	private String busProcId;
	 
	 private String fileProcId;
	 
	 private int subEntity;
	 
	 private int isactive;
	 
	 private Long licenseId;
	 
	 private String licenseno;
	 
	 private String ftypelist;
	 
	 private String bplist;
	 
	 private Integer userid;
	 
	 private UserBean userInfo;
	 
	 private Date createddate;
	 
	 private int isExpired;
	 
	 private Date startDate;
	 
	 private Date endDate;
	 
	 private List<IRPlusFileTypes> fileTypes;
	 
	 private List<FileTypeInfo> fileTypeInfo;
	 
	 public List<FileTypeInfo> getFileTypeInfo() {
		return fileTypeInfo;
	}

	public void setFileTypeInfo(List<FileTypeInfo> fileTypeInfo) {
		this.fileTypeInfo = fileTypeInfo;
	}

	private List<IRPlusBusinessProcess> busProcess;
	private List<BusinessProcessInfo> businessProcessInfo;
	
	
	public List<BusinessProcessInfo> getBusinessProcessInfo() {
		return businessProcessInfo;
	}

	public void setBusinessProcessInfo(List<BusinessProcessInfo> businessProcessInfo) {
		this.businessProcessInfo = businessProcessInfo;
	}

	public String getSiteCode() {
		return siteCode;
	}

	public void setSiteCode(String siteCode) {
		this.siteCode = siteCode;
	}

	public Long getSiteId() {
		return siteId;
	}

	public void setSiteId(Long siteId) {
		this.siteId = siteId;
	}

	public String getSiteName() {
		return siteName;
	}

	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}

	public String getCtctPerson() {
		return ctctPerson;
	}

	public void setCtctPerson(String ctctPerson) {
		this.ctctPerson = ctctPerson;
	}

	public String getCtctNo() {
		return ctctNo;
	}

	public void setCtctNo(String ctctNo) {
		this.ctctNo = ctctNo;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	
	

	public String getMacAddr() {
		return macAddr;
	}

	public void setMacAddr(String macAddr) {
		this.macAddr = macAddr;
	}

	public String getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(String validFrom) {
		this.validFrom = validFrom;
	}

	public String getValidTo() {
		return validTo;
	}

	public void setValidTo(String validTo) {
		this.validTo = validTo;
	}

	public String getBusProcId() {
		return busProcId;
	}

	public void setBusProcId(String busProcId) {
		this.busProcId = busProcId;
	}

	public String getFileProcId() {
		return fileProcId;
	}

	public void setFileProcId(String fileProcId) {
		this.fileProcId = fileProcId;
	}

	public int getSubEntity() {
		return subEntity;
	}

	public void setSubEntity(int subEntity) {
		this.subEntity = subEntity;
	}

	public int getIsactive() {
		return isactive;
	}

	public void setIsactive(int isactive) {
		this.isactive = isactive;
	}

	public Long getLicenseId() {
		return licenseId;
	}

	public void setLicenseId(Long licenseId) {
		this.licenseId = licenseId;
	}

	public String getLicenseno() {
		return licenseno;
	}

	public void setLicenseno(String licenseno) {
		this.licenseno = licenseno;
	}

	public String getFtypelist() {
		return ftypelist;
	}

	public void setFtypelist(String ftypelist) {
		this.ftypelist = ftypelist;
	}

	public String getBplist() {
		return bplist;
	}

	public void setBplist(String bplist) {
		this.bplist = bplist;
	}

	public Integer getUserid() {
		return userid;
	}

	public void setUserid(Integer userid) {
		this.userid = userid;
	}

	public UserBean getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(UserBean userInfo) {
		this.userInfo = userInfo;
	}

	public Date getCreateddate() {
		return createddate;
	}

	public void setCreateddate(Date createddate) {
		this.createddate = createddate;
	}

	public int getIsExpired() {
		return isExpired;
	}

	public void setIsExpired(int isExpired) {
		this.isExpired = isExpired;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public List<IRPlusFileTypes> getFileTypes() {
		return fileTypes;
	}

	public void setFileTypes(List<IRPlusFileTypes> fileTypes) {
		this.fileTypes = fileTypes;
	}

	public List<IRPlusBusinessProcess> getBusProcess() {
		return busProcess;
	}

	public void setBusProcess(List<IRPlusBusinessProcess> busProcess) {
		this.busProcess = busProcess;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("LicenseInfo [siteCode=").append(siteCode).append(", siteId=").append(siteId)
				.append(", siteName=").append(siteName).append(", ctctPerson=").append(ctctPerson).append(", ctctNo=")
				.append(ctctNo).append(", address1=").append(address1).append(", address2=").append(address2)
				.append(", licType=").append(licType).append(", macAddr=").append(macAddr).append(", validFrom=")
				.append(validFrom).append(", validTo=").append(validTo).append(", busProcId=").append(busProcId)
				.append(", fileProcId=").append(fileProcId).append(", subEntity=").append(subEntity)
				.append(", isactive=").append(isactive).append(", licenseId=").append(licenseId).append(", licenseno=")
				.append(licenseno).append(", ftypelist=").append(ftypelist).append(", bplist=").append(bplist)
				.append(", userid=").append(userid).append(", businessProcessInfo=").append(businessProcessInfo).append(", userInfo=").append(userInfo).append(", createddate=")
				.append(createddate).append(", isExpired=").append(isExpired).append(", startDate=").append(startDate)
				.append(", endDate=").append(endDate).append(", remainingDays=").append(remainingDays).append(", fileTypeInfo=").append(fileTypeInfo).append(", fileTypes=").append(fileTypes).append(", busProcess=")
				.append(busProcess).append("]");
		return builder.toString();
	}

	public String getLicPath() {
		return licPath;
	}

	public void setLicPath(String licPath) {
		this.licPath = licPath;
	}
	
	
	
	 
}
