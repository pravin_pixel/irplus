package com.irplus.dto;

import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

	public class UserBean implements java.io.Serializable {

	  private static final long serialVersionUID = -3176074231815769436L;
	  
		private Integer userid;
		private Integer isactive;	
		private String userCode;
		private String firstName;
		private String lastName;
		private String username;
		private String password;
		private Integer adminId;
		private String emailAddress;
		private String contactno;
		private String userPhoto;
		private Date createdDate;
		private Date modifiedDate;
		private int roleId;
		
		private int siteId;
		private UserBankBean[] userBank;
		
	 public UserBankBean[] getUserBank() {
			return userBank;
		}


		public int getRoleId() {
		return roleId;
	}


	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}


	public int getSiteId() {
		return siteId;
	}


	public void setSiteId(int siteId) {
		this.siteId = siteId;
	}


		public void setUserBank(UserBankBean[] userBank) {
			this.userBank = userBank;
		}


	public UserBean() {
		
		}


	public Integer getUserid() {
		return userid;
	}


	public void setUserid(Integer userid) {
		this.userid = userid;
	}


	public Integer getIsactive() {
		return isactive;
	}


	public void setIsactive(Integer isactive) {
		this.isactive = isactive;
	}


	public String getUserCode() {
		return userCode;
	}


	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}


	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public String getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public Integer getAdminId() {
		return adminId;
	}


	public void setAdminId(Integer adminId) {
		this.adminId = adminId;
	}


	public String getEmailAddress() {
		return emailAddress;
	}


	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}


	public String getContactno() {
		return contactno;
	}


	public void setContactno(String contactno) {
		this.contactno = contactno;
	}


	public String getUserPhoto() {
		return userPhoto;
	}


	public void setUserPhoto(String userPhoto) {
		this.userPhoto = userPhoto;
	}


	public Date getCreatedDate() {
		return createdDate;
	}


	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}


	public Date getModifiedDate() {
		return modifiedDate;
	}


	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}


	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("UserBean [userid=").append(userid).append(", isactive=").append(isactive).append(", userCode=")
				.append(userCode).append(", firstName=").append(firstName).append(", lastName=").append(lastName)
				.append(", username=").append(username).append(", roleId=").append(roleId).append(", siteId=").append(siteId).append(", password=").append(password).append(", adminId=")
				.append(adminId).append(", emailAddress=").append(emailAddress).append(", contactno=").append(contactno)
				.append(", userPhoto=").append(userPhoto).append(", userBank=").append(Arrays.toString(userBank)).append(", createdDate=").append(createdDate)
				.append(", modifiedDate=").append(modifiedDate).append("]");
		return builder.toString();
	}

	 
	 
		
	}
