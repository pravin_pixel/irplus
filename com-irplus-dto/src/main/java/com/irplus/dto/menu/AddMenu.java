package com.irplus.dto.menu;

import java.util.Date;

public class AddMenu {
	private static final long serialVersionUID =1L;	
	
	private Integer menuid;
	private String menuName;
	private String description;
	private Integer status;
	
	private String menuicon;
	private String modulepath;
	private Integer sortingorder;
	private Boolean ismodule;
	private Integer userid;
	private Date createddate;
	private Date modifieddate;








public Integer getMenuid() {
	return menuid;
}
public void setMenuid(Integer menuid) {
	this.menuid = menuid;
}

public String getMenuName() {
	return menuName;
}
public String getModulepath() {
	return modulepath;
}
public void setModulepath(String mopath) {
	this.modulepath = mopath;
}
public void setMenuName(String menuName) {
	this.menuName = menuName;
}

public String getDescription() {
	return description;
}
public void setDescription(String description) {
	this.description = description;
}
public Integer getStatus() {
	return status;
}
public void setStatus(Integer status) {
	this.status = status;
}

public String getMenuicon() {
	return menuicon;
}
public void setMenuicon(String menuicon) {
	this.menuicon = menuicon;
}

public Integer getSortingorder() {
	return sortingorder;
}
public void setSortingorder(Integer sortingorder) {
	this.sortingorder = sortingorder;
}
public Boolean getIsmodule() {
	return ismodule;
}
public void setIsmodule(Boolean ismodule) {
	this.ismodule = ismodule;
}
public Integer getUserid() {
	return userid;
}
public void setUserid(Integer userid) {
	this.userid = userid;
}
public Date getCreateddate() {
	return createddate;
}
public void setCreateddate(Date createddate) {
	this.createddate = createddate;
}
public Date getModifieddate() {
	return modifieddate;
}
public void setModifieddate(Date modifieddate) {
	this.modifieddate = modifieddate;
}

@Override
public String toString() {
	return "AddMenu [menuName=" + menuName + ", description=" + description + ", sortingOrder=" 
			+ ", menuicon=" + menuicon + ", modulepath=" + modulepath + ", sortingorder=" + sortingorder + ", ismodule="
			+ ismodule + ", userid=" + userid + ", createddate=" + createddate + ", modifieddate=" + modifieddate + "]";
}

}