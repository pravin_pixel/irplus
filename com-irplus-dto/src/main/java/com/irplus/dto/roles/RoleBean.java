package com.irplus.dto.roles;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class RoleBean implements java.io.Serializable {

	private Integer roleid;
	private Integer statusId;
	private Integer userId;
	private String rolename;
	private Integer isrestricted;
	private Boolean bankVisibility;
	private Date createddate;
	private Date modifieddate;
	private Set irMRolepermissionses = new HashSet(0);

	public RoleBean() {
	}

	public RoleBean(Integer statusId, String rolename, Date createddate) {
		this.statusId = statusId;
		this.rolename = rolename;
		this.createddate = createddate;
	}

	public RoleBean(Integer statusId, Integer userId, String rolename, Integer isrestricted,
			Boolean bankVisibility, Date createddate, Date modifieddate) {
		this.statusId = statusId;
		this.userId = userId;
		this.rolename = rolename;
		this.isrestricted = isrestricted;
		this.bankVisibility = bankVisibility;
		this.createddate = createddate;
		this.modifieddate = modifieddate;
		
	}

	public Integer getRoleid() {
		return this.roleid;
	}

	public void setRoleid(Integer roleid) {
		this.roleid = roleid;
	}

	public Integer getStatusId() {
		return statusId;
	}

	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getRolename() {
		return this.rolename;
	}

	public void setRolename(String rolename) {
		this.rolename = rolename;
	}

	public Integer getIsrestricted() {
		return this.isrestricted;
	}

	public void setIsrestricted(Integer isrestricted) {
		this.isrestricted = isrestricted;
	}

	public Boolean getBankVisibility() {
		return this.bankVisibility;
	}

	public void setBankVisibility(Boolean bankVisibility) {
		this.bankVisibility = bankVisibility;
	}

	public Date getCreateddate() {
		return this.createddate;
	}

	public void setCreateddate(Date createddate) {
		this.createddate = createddate;
	}

	public Date getModifieddate() {
		return this.modifieddate;
	}

	public void setModifieddate(Date modifieddate) {
		this.modifieddate = modifieddate;
	}

	public Set getIrMRolepermissionses() {
		return this.irMRolepermissionses;
	}

	public void setIrMRolepermissionses(Set irMRolepermissionses) {
		this.irMRolepermissionses = irMRolepermissionses;
	}

}
