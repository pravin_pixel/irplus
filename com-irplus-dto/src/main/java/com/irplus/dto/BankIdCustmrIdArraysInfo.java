package com.irplus.dto;

public class BankIdCustmrIdArraysInfo {
	
	private BankInfo[] bankArray; 
	private  CustomerBean[] customrArray;
		
	public BankInfo[] getBankArray() {
		return bankArray;
	}
	public void setBankArray(BankInfo[] bankArray) {
		this.bankArray = bankArray;
	}
	public CustomerBean[] getCustomrArray() {
		return customrArray;
	}
	public void setCustomrArray(CustomerBean[] customrArray) {
		this.customrArray = customrArray;
	}
	
}