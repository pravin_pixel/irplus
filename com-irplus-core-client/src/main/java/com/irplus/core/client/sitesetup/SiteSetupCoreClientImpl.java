package com.irplus.core.client.sitesetup;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.irplus.core.sitesetup.SiteSetupCore;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.SiteInfo;
import com.irplus.util.BusinessException;


@Component
public class SiteSetupCoreClientImpl implements SiteSetupCoreClient{

	@Autowired
	SiteSetupCore siteSetupCore;
	
	
	@Override
	public IRPlusResponseDetails getSiteInfo(String siteId) throws BusinessException {
		return siteSetupCore.getSiteInfo(siteId);
	}


	@Override
	public IRPlusResponseDetails updateSite(SiteInfo siteInfo) throws BusinessException {
	
		return siteSetupCore.updateSite(siteInfo);
	}


	@Override
	public IRPlusResponseDetails updateFolder(SiteInfo siteInfo) throws BusinessException {
		
		return siteSetupCore.updateFolder(siteInfo);
	}


	@Override
	public IRPlusResponseDetails licenseValidity() throws BusinessException {
		// TODO Auto-generated method stub
		return siteSetupCore.licenseValidity();
	}


	@Override
	public IRPlusResponseDetails siteInfo() throws BusinessException {
		// TODO Auto-generated method stub
		return siteSetupCore.siteInfo();
	}

}
