package com.irplus.core.client.rolepermissions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.irplus.core.rolepermissions.IRolePermissionCore;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.RolePrmUpdateNCreateInfo;
import com.irplus.dto.StatusIdInfo;
import com.irplus.dto.rolepermission.RolepermissionsBean;
import com.irplus.util.BusinessException;

@Component
public class RolePermissionsCoreClientImpl implements IRolePermissionCoreClient {

	@Autowired
	IRolePermissionCore iRolePermissionCore;

	@Override
	public IRPlusResponseDetails createRolePermission(RolepermissionsBean rolepermissionsBean)
			throws BusinessException {

		return iRolePermissionCore.createRolePermission(rolepermissionsBean);
	}

	@Override
	public IRPlusResponseDetails getRolePermissionById(String RolepermissionsBeanId) throws BusinessException {

		return iRolePermissionCore.getRolePermissionById(RolepermissionsBeanId);
	}

	@Override
	public IRPlusResponseDetails updateRolePermission(RolepermissionsBean rolepermissionsBean)
			throws BusinessException {
	
		return iRolePermissionCore.updateRolePermission(rolepermissionsBean);
	}

	@Override
	public IRPlusResponseDetails deleteRolePermissionById(String rolePermissionId) throws BusinessException {
		
		return iRolePermissionCore.deleteRolePermissionById(rolePermissionId);
	}

	@Override
	public IRPlusResponseDetails showAllRolePermission() throws BusinessException {

		return iRolePermissionCore.showAllRolePermission();
	}

	@Override
	public IRPlusResponseDetails updateStatusRolePrmn(StatusIdInfo statusIdInfo) throws BusinessException {

		return iRolePermissionCore.updateStatusRolePrmn(statusIdInfo);
	}

	@Override
	public IRPlusResponseDetails showAllRoleNMdl(StatusIdInfo statusIdInfo) throws BusinessException {

		return iRolePermissionCore.showAllRoleNMdl(statusIdInfo);
	}

	@Override
	public IRPlusResponseDetails updateRoleprmUpdNCrt(RolePrmUpdateNCreateInfo rolepermUpNCrte)
			throws BusinessException {

		return iRolePermissionCore.updateRoleprmUpdNCrt(rolepermUpNCrte);
	}

}