package com.irplus.core.client.module_menu;

import com.irplus.dto.menu_modules_association.ModuleMenuBean;
import com.irplus.dto.menu_modules_association.ModuleMenuResponseDetails;
import com.irplus.util.BusinessException;

public interface ModuleMenusCoreClient {
		
    public ModuleMenuResponseDetails createMenuModule(ModuleMenuBean moduleMenuInfo) throws BusinessException;

	public ModuleMenuResponseDetails getMenuModuleById(String moduleMenuId) throws BusinessException;		

	public ModuleMenuResponseDetails updateMenuModule(ModuleMenuBean moduleMenuInfo) throws BusinessException;

	public ModuleMenuResponseDetails deleteMenuModuleById(String moduleMenuId) throws BusinessException;
		
	public ModuleMenuResponseDetails findAllMenuModule() throws BusinessException;
	
}
