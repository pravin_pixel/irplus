package com.irplus.core.client.customer_reporting;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.irplus.core.customer_reporting.ICustomerReportingCore;
import com.irplus.dto.BankReportingsInfo;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.customerreporting.CustomerRepotingBean;
import com.irplus.dto.customerreporting.CustomerRepotingResponseDetails;
import com.irplus.util.BusinessException;

@Component
public class CustomerReportingCoreClientImpl implements ICustomerReportingCoreClient {

	@Autowired
	ICustomerReportingCore iCustomerReportingCore;
	
	@Override
	public IRPlusResponseDetails createCustomerReport(CustomerRepotingBean customerReportingBean)
			throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		irPlusResponseDetails = iCustomerReportingCore.createCustomerReport(customerReportingBean);
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails getCustomerReportById(String custommerReportingId) throws BusinessException {
		
		IRPlusResponseDetails customerRepotingResponseDetails = new IRPlusResponseDetails();		
		customerRepotingResponseDetails =  iCustomerReportingCore.getCustomerReportById(custommerReportingId);
		
		return customerRepotingResponseDetails;
	}

	@Override
	public CustomerRepotingResponseDetails updateCustomerReports(CustomerRepotingBean customerReportingBean)
			throws BusinessException {
		
		CustomerRepotingResponseDetails customerRepotingResponseDetails = new CustomerRepotingResponseDetails();		
		customerRepotingResponseDetails =  iCustomerReportingCore.updateCustomerReports(customerReportingBean);
		
		return customerRepotingResponseDetails;
	}

	@Override
	public CustomerRepotingResponseDetails deleteCustomerReportById(String custommerReportingId)
			throws BusinessException {
		
		CustomerRepotingResponseDetails customerRepotingResponseDetails = new CustomerRepotingResponseDetails();		
		customerRepotingResponseDetails =  iCustomerReportingCore.deleteCustomerReportById(custommerReportingId);
		
		return customerRepotingResponseDetails;
	}

	@Override
	public CustomerRepotingResponseDetails showAllCustomerReports() throws BusinessException {

		CustomerRepotingResponseDetails customerRepotingResponseDetails = new CustomerRepotingResponseDetails();
		customerRepotingResponseDetails =  iCustomerReportingCore.showAllCustomerReports();
		
		return customerRepotingResponseDetails;
	}

	@Override
	public IRPlusResponseDetails dynamicCustomerReportsBybankId(BankReportingsInfo banksReportsInfo)throws BusinessException {
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();		
		irPlusResponseDetails = iCustomerReportingCore.dynamicCustomerReportsBybankId(banksReportsInfo);
		return irPlusResponseDetails;
	}
	
}