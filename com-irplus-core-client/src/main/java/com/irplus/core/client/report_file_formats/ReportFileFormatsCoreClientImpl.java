package com.irplus.core.client.report_file_formats;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.irplus.core.report_file_formats.IReportFileFormatsCore;
import com.irplus.dao.client.report_file_formats.IReportFileFormatsDaoClient;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.ReportFileFormatInfo;
import com.irplus.util.BusinessException;

@Component
public class ReportFileFormatsCoreClientImpl implements IReportFileFormatsCoreClient{

	@Autowired 
	IReportFileFormatsCore iReportFileFormatsCore;
	
	@Override
	public IRPlusResponseDetails createReportFileFormat(ReportFileFormatInfo reportFileFormatInfo)
			throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		irPlusResponseDetails = iReportFileFormatsCore.createReportFileFormat(reportFileFormatInfo);
		return irPlusResponseDetails; 
	}

	@Override
	public IRPlusResponseDetails getReportFileFormatById(String reportFileFormatInfoId) throws BusinessException{
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		irPlusResponseDetails = iReportFileFormatsCore.getReportFileFormatById(reportFileFormatInfoId);
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails updateReportFileFormat(ReportFileFormatInfo reportFileFormatInfo)
			throws BusinessException {
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		irPlusResponseDetails =  iReportFileFormatsCore.updateReportFileFormat(reportFileFormatInfo);
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails deleteReportFileFormatById(String reportFileFormatInfoId) throws BusinessException {
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		irPlusResponseDetails =  iReportFileFormatsCore.deleteReportFileFormatById(reportFileFormatInfoId);
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails showAllReportFileFormat() throws BusinessException {
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		irPlusResponseDetails =  iReportFileFormatsCore.showAllReportFileFormat();
		return irPlusResponseDetails;
	}
	
}