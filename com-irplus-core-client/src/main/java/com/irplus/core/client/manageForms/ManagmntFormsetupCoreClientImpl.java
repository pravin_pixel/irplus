package com.irplus.core.client.manageForms;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.irplus.core.manageForms.IManagmntFormsetupCore;
import com.irplus.dto.FormsManagementDTO;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.StatusIdInfo;
import com.irplus.util.BusinessException;


@Component
public class ManagmntFormsetupCoreClientImpl implements IManagmntFormsetupCoreClient{

	@Autowired
	IManagmntFormsetupCore iManagmntFormsetupCore;
	
	@Override
	public IRPlusResponseDetails createMngmntformSetup(FormsManagementDTO formsManagementDTO) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails(); 
		
		irPlusResponseDetails =  iManagmntFormsetupCore.createMngmntformSetup(formsManagementDTO);
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails dynamicSelectFieldTypes() throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails(); 
		
		irPlusResponseDetails =  iManagmntFormsetupCore.dynamicSelectFieldTypes();
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails showOneMngmntformSetup(String id) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails(); 
		
		irPlusResponseDetails =  iManagmntFormsetupCore.showOneMngmntformSetup(id);
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails updateMngmntformSetup(FormsManagementDTO formsManagementDTO) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails(); 
		
		irPlusResponseDetails =  iManagmntFormsetupCore.updateMngmntformSetup(formsManagementDTO);
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails statusUpdate(StatusIdInfo statusIdInfo) throws BusinessException {		

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails(); 
		
		irPlusResponseDetails =  iManagmntFormsetupCore.statusUpdate(statusIdInfo);
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails showListofForms() throws BusinessException {
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails(); 
		
		irPlusResponseDetails =  iManagmntFormsetupCore.showListofForms();
		return irPlusResponseDetails;
	}
	@Override
	public IRPlusResponseDetails showAllCustomerForms(Integer customerid) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails(); 
		
		irPlusResponseDetails =  iManagmntFormsetupCore.showAllCustomerForms(customerid);
		return irPlusResponseDetails;
	}
}