package com.irplus.core.client.customerMappingMngmnt;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.irplus.core.customerMappingMngmnt.ICustomerMappingCore;
import com.irplus.dto.CustomerMapSupportInfo;
import com.irplus.dto.CustomerMappingInfo;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.StatusIdInfo;
import com.irplus.util.BusinessException;

@Component
public class CustomerMappingCoreClientImpl implements ICustomerMappingCoreClient{

	private static final Logger log = Logger.getLogger(CustomerMappingCoreClientImpl.class);
	
	@Autowired 
	ICustomerMappingCore iCustomerMappingCore ;

	@Override
	public IRPlusResponseDetails getCustomerMappingById(String custommerId) throws BusinessException {

		return iCustomerMappingCore.getCustomerMappingById(custommerId);		
	}

	@Override
	public IRPlusResponseDetails updateCustomerMapping(CustomerMappingInfo customerMappingInfo)
			throws BusinessException {

		return iCustomerMappingCore.updateCustomerMapping(customerMappingInfo);		
	}

	@Override
	public IRPlusResponseDetails showAllCustomerMapping() throws BusinessException {

		return iCustomerMappingCore.showAllCustomerMapping();		
	}

	@Override
	public IRPlusResponseDetails statusUpdateCustmrMpng(StatusIdInfo statusIdInfo) throws BusinessException {

		return iCustomerMappingCore.statusUpdateCustmrMpng(statusIdInfo);		
	}

	@Override
	public IRPlusResponseDetails CustomerBankingBanks(String SiteId) throws BusinessException {

		return iCustomerMappingCore.CustomerBankingBanks(SiteId);		
	}
	@Override
	public IRPlusResponseDetails createCustomerMapping(CustomerMapSupportInfo customerMapSupportInfo)
			throws BusinessException {
		
		return iCustomerMappingCore.createCustomerMapping(customerMapSupportInfo);		
	}
	
	@Override
	public IRPlusResponseDetails showBranchFile(String branchId) throws BusinessException {

		return iCustomerMappingCore.showBranchFile(branchId);		
	}
	
	
	@Override
	public IRPlusResponseDetails showBranchCustomer(String branchId) throws BusinessException {

		return iCustomerMappingCore.showBranchCustomer(branchId);		
	}
	
	
	@Override
	public IRPlusResponseDetails createCustomerMappingFields(CustomerMappingInfo[] customerMappingInfo)
			throws BusinessException {

		return iCustomerMappingCore.createCustomerMappingFields(customerMappingInfo);		
	}
	
	
	@Override
	public IRPlusResponseDetails showAllCustomerMappedFields() throws BusinessException {
	
		return iCustomerMappingCore.showAllCustomerMappedFields();
	}
}