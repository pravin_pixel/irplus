package com.irplus.core.client.roles;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.irplus.core.roles.IRolesCore;
import com.irplus.dao.client.roles.RolesDaoClientImpl;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.StatusIdInfo;
import com.irplus.dto.roles.RoleBean;
import com.irplus.util.BusinessException;

@Component
public class RolesCoreClientImpl implements IRolesCoreClient {

	private static final Logger log = Logger.getLogger(RolesDaoClientImpl.class);
	
	@Autowired
	IRolesCore roleCore;
	
	@Override
	public IRPlusResponseDetails createRole(RoleBean roleBean) throws BusinessException {

//		IRPlusResponseDetails roleResponseDetails = new IRPlusResponseDetails() ;
		
		log.debug("Inside of RolesDaoClientImpl :: createRole( ) ");
	
//		roleResponseDetails = roleCore.createRole(roleBean);
		return roleCore.createRole(roleBean);
	}

	@Override
	public IRPlusResponseDetails getRoleById(String roleId) throws BusinessException {
		return roleCore.getRoleById(roleId);
	}

	@Override
	public IRPlusResponseDetails updateRole(RoleBean roleBean) throws BusinessException {
		return roleCore.updateRole(roleBean);
	}

	@Override
	public IRPlusResponseDetails deleteRoleById(String roleId) throws BusinessException {
		return roleCore.deleteRoleById(roleId);
	}

	@Override
	public IRPlusResponseDetails ShowAllRoles() throws BusinessException {
		return roleCore.ShowAllRoles();
	}

	@Override
	public IRPlusResponseDetails removeDuplicateRole(RoleBean roleBean) throws BusinessException {
		return roleCore.removeDuplicateRole(roleBean);
	}

	@Override
	public IRPlusResponseDetails updateRoleStatus(StatusIdInfo statusIbInfo) throws BusinessException {

		return roleCore.updateRoleStatus(statusIbInfo);
	
	}

	@Override
	public IRPlusResponseDetails ShowAllRoles_roleprm() throws BusinessException {
		
		return roleCore.ShowAllRoles_roleprm();
	}
	
}