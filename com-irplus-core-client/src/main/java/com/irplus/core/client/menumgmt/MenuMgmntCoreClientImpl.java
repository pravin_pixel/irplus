package com.irplus.core.client.menumgmt;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.irplus.core.menumgmt.IrMenuMgmntCore;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.StatusIdInfo;
import com.irplus.dto.menu.AddMenu;
import com.irplus.util.BusinessException;

@Component
public class MenuMgmntCoreClientImpl implements IrMenuMgmntCoreClient{

    @Autowired
    IrMenuMgmntCore iRMenuMgmntCore;
     

	@Override
	public IRPlusResponseDetails createMenu(AddMenu menuInfo) throws BusinessException {				
		
		return iRMenuMgmntCore.createMenu(menuInfo);
	}

	@Override
	public IRPlusResponseDetails getMenu(String menuId) throws BusinessException {
		
		return iRMenuMgmntCore.getMenu(menuId);
	}

	@Override
	public IRPlusResponseDetails updateMenu(AddMenu menuInfo) throws BusinessException {
		
		return iRMenuMgmntCore.updateMenu(menuInfo);
	}

	@Override
	public IRPlusResponseDetails deleteMenu(String menuId) throws BusinessException {
		
		return iRMenuMgmntCore.deleteMenu(menuId);
	}

	@Override
	public IRPlusResponseDetails validateMenuAndAdd(AddMenu insert_menu) throws BusinessException {

		return iRMenuMgmntCore.validateMenuAndAdd(insert_menu);
	}

	@Override
	public IRPlusResponseDetails findAllMenus() throws BusinessException {
		
		return iRMenuMgmntCore.findAllMenus();
	}

	@Override
	public IRPlusResponseDetails getAllActiveMenus() throws BusinessException {
		
		return iRMenuMgmntCore.getAllActiveMenus();
	}

	@Override
	public IRPlusResponseDetails updateMenuStatusPrm(StatusIdInfo siInfo) throws BusinessException {

		return iRMenuMgmntCore.updateMenuStatusPrm(siInfo);
	}

	@Override
	public IRPlusResponseDetails createMenuWithoutDpct(AddMenu menuInfo) throws BusinessException {

		return iRMenuMgmntCore.createMenuWithoutDpct(menuInfo);
	}    
        
}
