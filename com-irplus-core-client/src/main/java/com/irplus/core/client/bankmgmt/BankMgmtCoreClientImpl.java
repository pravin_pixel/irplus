package com.irplus.core.client.bankmgmt;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.irplus.core.bankmgmt.IBankMgmtCore;
import com.irplus.dto.BankFilterInfo;
import com.irplus.dto.BankInfo;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.util.BusinessException;

@Component
public class BankMgmtCoreClientImpl implements IBankMgmtCoreClient{

	@Autowired
    IBankMgmtCore iBankMgmtCore;
	
	

	@Override
	public IRPlusResponseDetails createBank(BankInfo bankInfo) throws BusinessException {

		return iBankMgmtCore.createBank(bankInfo);
	}
	
	@Override
	public IRPlusResponseDetails listBanks(String siteId)throws BusinessException
	{
		return iBankMgmtCore.listBanks(siteId);
	}

	@Override
	public IRPlusResponseDetails updateBank(BankInfo bankInfo) throws BusinessException {
		
		return iBankMgmtCore.updateBank(bankInfo);
	}
	@Override
	public IRPlusResponseDetails updateBankStatus(BankInfo bankInfo) throws BusinessException
	{
		return iBankMgmtCore.updateBankStatus(bankInfo);
	}
	@Override
	public IRPlusResponseDetails updateBranchStatus(BankInfo bankInfo) throws BusinessException {
		
		return iBankMgmtCore.updateBranchStatus(bankInfo);
	}
	
	@Override
	public IRPlusResponseDetails getBankInfo(String bankId) throws BusinessException
	{
		return iBankMgmtCore.getBankInfo(bankId);
	}
	@Override
	public IRPlusResponseDetails filterBanks(BankFilterInfo bankFilterInfo) throws BusinessException
	{
		return iBankMgmtCore.filterBanks(bankFilterInfo);
	}
	@Override
	public IRPlusResponseDetails deleteBank(BankInfo bankInfo)throws BusinessException
	{
		return iBankMgmtCore.deleteBank(bankInfo);
	}
	
	@Override
	public IRPlusResponseDetails addLocation(BankInfo bankInfo) throws BusinessException
	{
		return iBankMgmtCore.addLocation(bankInfo);
	}
	
	@Override
	public IRPlusResponseDetails updateLocation(BankInfo bankInfo) throws BusinessException
	{
		return iBankMgmtCore.updateLocation(bankInfo);
	}

	@Override
	public IRPlusResponseDetails listLocations(String bankId) throws BusinessException
	{
		return iBankMgmtCore.listLocations(bankId);
	}

	@Override
	public IRPlusResponseDetails deleteLocation(BankInfo bankInfo) throws BusinessException
	{
		return iBankMgmtCore.deleteLocation(bankInfo);
	}

	@Override
	public IRPlusResponseDetails getBranchInfo(Long branchId) throws BusinessException
	{
		return iBankMgmtCore.getBranchInfo(branchId);
	}
	
	@Override
	public IRPlusResponseDetails filterBranches(BankFilterInfo bankFilterInfo) throws BusinessException
	{
		return iBankMgmtCore.filterBranches(bankFilterInfo);
	}
	@Override
	public IRPlusResponseDetails deleteContact(String contactId) throws BusinessException
	{
		return iBankMgmtCore.deleteContact(contactId);
	}

	
}
