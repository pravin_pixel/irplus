package com.irplus.core.client.fileprocessing.entrydetailrecord;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.irplus.core.fileprocessing.entrydetailrecord.EntryDetailRecordCore;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.fileprocessing.WEBEntryDetailRecordBean;
import com.irplus.util.BusinessException;

@Component
public class EntryDetailRecordCoreClientImpl implements EntryDetailRecordCoreClient{

	@Autowired
	private EntryDetailRecordCore entryDetailRecordCore;
	
	@Override
	public Boolean insertRecord(WEBEntryDetailRecordBean entryDetailRecordBean) {
		return entryDetailRecordCore.insertRecord(entryDetailRecordBean);
	}

	@Override
	public Boolean deleteRecord(WEBEntryDetailRecordBean entryDetailRecordBean) {
		return entryDetailRecordCore.deleteRecord(entryDetailRecordBean);
	}

	@Override
	public List<WEBEntryDetailRecordBean> listAllRecords() {
		return entryDetailRecordCore.listAllRecords();
	}

	@Override
	public IRPlusResponseDetails getRecordByEntryId(Integer fileId) throws BusinessException {
		return entryDetailRecordCore.getRecordByEntryId(fileId);
	}

	@Override
	public Boolean updateRecord(WEBEntryDetailRecordBean batchHeaderRecord) {
		return entryDetailRecordCore.updateRecord(batchHeaderRecord);
	}

	@Override
	public List<WEBEntryDetailRecordBean> getByQuery(Integer Id) {
		return entryDetailRecordCore.getByQuery(Id);
	}

}
