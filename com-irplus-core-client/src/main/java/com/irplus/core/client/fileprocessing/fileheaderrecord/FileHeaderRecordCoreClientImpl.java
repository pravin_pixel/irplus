package com.irplus.core.client.fileprocessing.fileheaderrecord;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.irplus.core.fileprocessing.fileheaderrecord.FileHeaderRecordCore;
import com.irplus.dto.CustomerGroupingMngmntInfo;
import com.irplus.dto.FileTypeInfo;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.fileprocessing.FileHeaderRecordBean;
import com.irplus.util.BusinessException;

@Component
public class FileHeaderRecordCoreClientImpl implements FileHeaderRecordCoreClient{
	
	@Autowired
	private FileHeaderRecordCore fileHeaderRecordCore;
	
	@Override
	public Boolean insertRecord(FileHeaderRecordBean fileHeaderRecord) {
		return fileHeaderRecordCore.insertRecord(fileHeaderRecord);
	}

	@Override
	public Boolean deleteRecord(FileHeaderRecordBean fileHeaderRecord) {
		return fileHeaderRecordCore.deleteRecord(fileHeaderRecord);
	}

	@Override
	public IRPlusResponseDetails listAllRecords(String userId)throws BusinessException{
		return fileHeaderRecordCore.listAllRecords(userId);
	}

	@Override
	public IRPlusResponseDetails getRecordByBatchId(Integer fileId) throws BusinessException {
		return fileHeaderRecordCore.getRecordByBatchId(fileId);
	}

	@Override
	public Boolean updateRecord(FileHeaderRecordBean fileHeaderRecord) {
		return fileHeaderRecordCore.updateRecord(fileHeaderRecord);
	}
	@Override
	public IRPlusResponseDetails getMasterDataChart(Integer userId,String siteId) throws BusinessException {
		return fileHeaderRecordCore.getMasterDataChart(userId,siteId);
	}
	
	@Override
	public IRPlusResponseDetails DaywiseFiles(CustomerGroupingMngmntInfo fileheadrec) throws BusinessException {
		return fileHeaderRecordCore.DaywiseFiles(fileheadrec);
	}

	@Override
	public IRPlusResponseDetails DaywiseFilesGroup(CustomerGroupingMngmntInfo fileheadrec) throws BusinessException {
		return fileHeaderRecordCore.DaywiseFilesGroup(fileheadrec);
	}
	@Override
	public IRPlusResponseDetails BankwiseFileList(CustomerGroupingMngmntInfo fileinfo) throws BusinessException {
		return fileHeaderRecordCore.BankwiseFileList(fileinfo);
	}
	@Override
	public IRPlusResponseDetails listfileLog() throws BusinessException {
		// TODO Auto-generated method stub
		return fileHeaderRecordCore.listfileLog();
	}
	@Override
	public IRPlusResponseDetails GroupwiseFileList(CustomerGroupingMngmntInfo Grpinfo) throws BusinessException {
		return fileHeaderRecordCore.GroupwiseFileList(Grpinfo);
	}
	@Override
	public IRPlusResponseDetails filterFiles(FileTypeInfo Grpinfo) throws BusinessException {
		return fileHeaderRecordCore.filterFiles(Grpinfo);
	}
}
