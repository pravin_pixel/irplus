package com.irplus.util;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;


public class IRPlusUtil {
	
	private static final Logger LOGGER = Logger.getLogger(IRPlusUtil.class);

	public static String convertTimestampToStringFormat(Timestamp timestamp){
		try{

			SimpleDateFormat format = new SimpleDateFormat(IRPlusConstants.USER_DATE_TIME_FORMAT);

			TimeZone userTimeZone = TimeZone.getTimeZone(IRPlusConstants.USER_TIMEZONE);

			format.setTimeZone(userTimeZone);

			return format.format(timestamp);

		}catch(Exception e){
			return null;
		}
	}
	
	public static String convertDateToString(Date date){
		try{

			SimpleDateFormat format = new SimpleDateFormat(IRPlusConstants.USER_DATE_FORMAT);

			TimeZone userTimeZone = TimeZone.getTimeZone(IRPlusConstants.USER_TIMEZONE);

			format.setTimeZone(userTimeZone);

			return format.format(date);

		}catch(Exception e){
			return null;
		}
	}

	public static byte[] irplusEncrypt(String strClearText) throws Exception{
		byte[] encrypted= null;
		try {
			if(strClearText!=null)
			{
				SecretKeySpec skeyspec=new SecretKeySpec(IRPlusConstants.CIPHER_PARAM_PASSWORD.getBytes(),"Blowfish");
				Cipher cipher=Cipher.getInstance("Blowfish");
				cipher.init(Cipher.ENCRYPT_MODE, skeyspec);
				encrypted=cipher.doFinal(strClearText.getBytes());
				encrypted = new Base64().encode(encrypted);
				
			}
			//strData=new String(encrypted);

		} catch (Exception e) {
			//e.printStackTrace();
			LOGGER.error("Exception in irplusEncrypt",e);
			throw new Exception(e);
		}
		return encrypted;
	}

	public static String irplusDecrypt(byte[] strEncrypted) throws Exception{
		String strData="";

		try {
			if(strEncrypted!=null)
			{
				SecretKeySpec skeyspec=new SecretKeySpec(IRPlusConstants.CIPHER_PARAM_PASSWORD.getBytes(),"Blowfish");
				Cipher cipher=Cipher.getInstance("Blowfish");
				cipher.init(Cipher.DECRYPT_MODE, skeyspec);
				byte[] decrypted= new Base64().decode(strEncrypted);
				decrypted=cipher.doFinal(decrypted);
				strData=new String(decrypted);
			}
			

		} catch (Exception e) {
			LOGGER.error("Exception in irplusDecrypt ",e);
			throw new Exception(e);
		}
		return strData;
	}

	/* public static void main(String[] args) {

		 try {
			 byte[] encryptedData = irplusEncrypt("Dhinakaran","dhivya");
			 //byte[] encoded = new Base64().encode(encryptedData);
			 //byte[] decoded = new Base64().decode(encoded);
			String originalData = irplusDecrypt(encryptedData,"dhivya");
			System.out.println("encryptedData : "+encryptedData+" originalData : "+originalData);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}*/

}
