package com.irplus.util;

import com.irplus.dto.BankInfo;
import com.irplus.dto.ContactInfo;
import com.irplus.dto.CustomerBean;
import com.irplus.dto.CustomerContactsBean;
import com.irplus.dto.CustomerFunctionInfo;
import com.irplus.dto.CustomerLicenceInfo;
import com.irplus.dto.CustomerMappingInfo;
import com.irplus.dto.CustomerReportLicenceInfo;
import com.irplus.dto.FieldTypesInfo;
import com.irplus.dto.ReportFileFormatInfo;
import com.irplus.dto.menu.AddMenu;
import com.irplus.dto.menu_modules_association.ModuleMenuBean;
import com.irplus.dto.module.ModuleBean;
import com.irplus.dto.roles.RoleBean;

public class ValidationHelper {

	public static boolean isCreateBankReqValid(BankInfo bankInfo) 
	{
		boolean isValidReq = false;
		
		if(bankInfo.getSiteId()!=null&&!bankInfo.getSiteId().isEmpty()&&
				bankInfo.getBankName()!=null&&!bankInfo.getBankName().isEmpty()&&
					bankInfo.getBankCode()!=null&&!bankInfo.getBankCode().isEmpty())
		{
			isValidReq = true;
		}
		return isValidReq;
	}

	public static boolean isUpdateBankReqValid(BankInfo bankInfo) {
		
		boolean isValidReq = false;
		
		if(bankInfo.getBankName()!=null&&!bankInfo.getBankName().isEmpty()&&
					bankInfo.getBankCode()!=null&&!bankInfo.getBankCode().isEmpty())
		{
			isValidReq = true;
		}
		return isValidReq;
	}

	public static boolean isValidReq(String siteId) 
	{
		boolean isValidReq = false;
		
		if(siteId!=null&&!siteId.isEmpty()&&siteId.matches("[0-9]+"))
		{
			isValidReq = true;
		}
		return isValidReq;
	}

	public static boolean isCreateContactReqValid(ContactInfo contactInfo) {
		boolean isValidReq = false;
		
		if(contactInfo.getFirstName()!=null&&!contactInfo.getFirstName().isEmpty())
		{
			isValidReq = true;
		}
		return isValidReq;
	}

	public static boolean isCreateBranchReqValid(BankInfo bankInfo) {
		boolean isValidReq = false;
		
		if(bankInfo.getBranchLocation()!=null&&!bankInfo.getBranchLocation().isEmpty()&&
					bankInfo.getBranchOwnCode()!=null&&!bankInfo.getBranchOwnCode().isEmpty())
		{
			isValidReq = true;
		}
		return isValidReq;
	}
	
	public static boolean isCreateFieldReqValid(FieldTypesInfo fieldTypesInfo) {

		boolean isValidReq = false;

		if (fieldTypesInfo.getFieldFormat() != null && fieldTypesInfo.getDefaultLength().intValue() != 0) {
			isValidReq = true;
		}
		return isValidReq;
	}

	public static boolean isCreateReportFileFromatReqValid(ReportFileFormatInfo reportFileFormatInfo) {

		boolean isValidReq = false;

		if (reportFileFormatInfo.getFileformatName()!= null) {
			isValidReq = true;
		}
		return isValidReq;
	}

	public static boolean isCustomerLicenceReqValid(CustomerLicenceInfo customerLicenceInfo) {

		boolean isValidReq = false;

		if (customerLicenceInfo.getCustomersId()!= null) {
			isValidReq = true;
		}
		return isValidReq;
	}

	public static boolean isManageModuleReqValid(ModuleMenuBean moduleMenuBean) {
		
		boolean isValidReq = false;

		if (moduleMenuBean.getMenuid() != null && moduleMenuBean.getModuleid()!=null) {
			isValidReq = true;
		}
		return isValidReq;
	}
	
	public static boolean isCustomerReportLicenceReqValid(CustomerReportLicenceInfo customerReportLicenceInfo) {
		
		boolean isValidReq = false;

		if (customerReportLicenceInfo.getIrMUsersId() != null && customerReportLicenceInfo.getIrSBankbranchId()!=null) {
			isValidReq = true;
		}
		return isValidReq;
	}
	
	public static boolean isValidMenuReq(AddMenu menuInfo) {
		
		boolean isValidReq = false;
		
		if (menuInfo.getMenuName() != null && menuInfo.getDescription() != null) {
			isValidReq = true;	
		}
		return isValidReq;
	}
	
	public static boolean isValidModuleReq(ModuleBean moduleInfo) {
		
//		log.info("inside of MenuValidationHelper class");

		boolean isValidReq = false;
		
		if (moduleInfo.getModulename()!= null && moduleInfo.getDescription() != null) {
			isValidReq = true;
//			log.debug("isValidModuleReq is true :: validation success");
		}
		return isValidReq;
	}
	
	public static boolean isValidModuleReq(RoleBean roleInfo) {
		boolean isValidReq = false;
		if (roleInfo.getRolename()!=null && roleInfo.getBankVisibility()!= null && 
				roleInfo.getStatusId() != null  && roleInfo.getRoleid() != null ){	
			isValidReq = true;
		}
		return isValidReq;
	}
	
	public static boolean isValidCustomerReq(CustomerBean customerBean){
		
		boolean isValidReq = false;
		
		if (customerBean.getCompanyName()!=null){	
			isValidReq = true;
		}
		return isValidReq;
	}
	public static boolean isCreateCustomerContactReqValid(CustomerContactsBean customerContacts) {
		boolean isValidReq = false;
		
		if(customerContacts.getFirstName()!=null&&!customerContacts.getFirstName().isEmpty())
		{
			isValidReq = true;
		}
		return isValidReq;
	}
	public static boolean isValidCustomerFuntnReq(CustomerFunctionInfo customerFunctionInfo){
		
		boolean isValidReq = false;
		
		if ( customerFunctionInfo.getCustomersId() != null && customerFunctionInfo.getUserId()!=null 
				&& customerFunctionInfo.getBankbranchId()!=null)
		{	
			isValidReq = true;
		}
		return isValidReq;
	}

/*	public static boolean isValidCustmrMappngReq(CustomerMappingInfo customerMappingInfo){
		
		boolean isValidReq = false;
		
		if ( customerMappingInfo.getCustomersId() != null && customerMappingInfo.getUserId()!=null 
				&& customerMappingInfo.getBankbranchId()!=null)
		{	
			isValidReq = true;
		}
		return isValidReq;
	}*/

}
