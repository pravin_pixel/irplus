package com.irplus.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.irplus.dto.menu_modules_association.ModuleMenuBean;

public class ModuleMenuValidationHelper {

	private static final Log log = LogFactory.getLog(ModuleMenuValidationHelper.class);
	
	public static boolean isValidModuleMenuReq(ModuleMenuBean moduleInfo) {
		
		log.info("inside of MenuValidationHelper class");

		boolean isValidReq = false;
		
/*		if (moduleInfo.getAddMenu()!= null && moduleInfo.getModule()!= null) {
			isValidReq = true;
			log.debug("isValidModuleReq is true :: validation success");
		}
*/	
		if (moduleInfo.getSortingorder()!= null) {
			isValidReq = true;
			log.debug("isValidModuleReq is true :: validation success");
		}		
		
		return isValidReq;
	}
	

	public static boolean isValidModuleMenuId(String modulemnuId) {
		
		log.info("inside of ModuleValidationHelper class");
		
		boolean isValidReq = false;
		
		if(modulemnuId!=null && !modulemnuId.isEmpty()&&modulemnuId.matches("[0-9]+")) {
			isValidReq=true;
			log.debug("isValidModuleReq is true :: validation success");
		}
		
		return isValidReq;
	}
	
}
