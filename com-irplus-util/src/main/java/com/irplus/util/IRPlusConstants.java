package com.irplus.util;

import java.util.ResourceBundle;


public class IRPlusConstants {

    static ResourceBundle bundle = ResourceBundle.getBundle("IRPlus-config");
    
    
    /** Server response properties **/
    public static final int SUCCESS_CODE = 200;
    public static final String SUCCESS_MSG = "OK";
    public static final int NO_RECORD_FOUND_CODE = 202;
    public static final String NO_RECORD_FOUND_MSG = "NO RECORD FOUND";
    public static final int ERR_CODE = 501;
    public static final String ERR_MSG = "EXCEPTION";
    public static final String VALIDATION_ERR_MSG = "VALIDATION_ERROR";
    
    

    /** Server timezone properties **/ 

	public static final String SERVER_TIMEZONE = "EST";	
	public static final String USER_DATE_FORMAT = "dd-MM-yyyy";
	public static final String SERVER_DATE_FORMAT = "yyyy-MM-dd";
	public static final String USER_DATE_TIME_FORMAT = "MM-dd-yyyy HH:mm:ss";
	public static final String SERVER_DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
	public static final String USER_TIMEZONE = "IST";
	public static final String FILE_DATE_FORMAT = "MM/dd/yyyy";
	
	/** License properties **/ 
	
	public static final String APP_NAME = bundle.getString("APP.NAME");
	public static final String DATA_FILE_EXTENSION = bundle.getString("DATA.FILE.EXTENSION");
	public static final String LIC_FILE_EXTENSION = bundle.getString("LIC.FILE.EXTENSION");
	public static final String KEYSTORE_FILENAME = bundle.getString("KEYSTORE.FILENAME");
	public static final String ALIAS = bundle.getString("ALIAS");
	public static final String KEYSTORE_PASSWORD = bundle.getString("KEYSTORE.PASSWORD");
	public static final String KEY_PASSWORD = bundle.getString("KEY.PASSWORD");
	public static final String CIPHER_PARAM_PASSWORD = bundle.getString("CIPHER.PARAM.PASSWORD");
	public static final String LIC_DIRECTORY = bundle.getString("LIC.DIRECTORY");
	public static final String BASE_FILENAME = bundle.getString("BASE.FILENAME");
	public static final String ISSUER_INFO = bundle.getString("ISSUER.INFO");
	public static final String PERPETUAL_LICENSE = "Perpetual";
	public static final String VALIDITY_LICENSE = "Validity";


	public static final String INVALID_NODE = bundle.getString("INVALID.NODE");  
	public static final String INVALID_LICENSE = bundle.getString("INVALID.LICENSE");
	public static final String EXPIRED_LICENSE = bundle.getString("EXPIRED.LICENSE");
	public static final String LICENSE_INACTIVE = bundle.getString("LICENSE.INACTIVE");
	public static final String LICENSE_VALIDATION_FAILED = bundle.getString("LICENSE.VALIDATION.FAILED");


	public static final String INVALID_CREDENTIALS = bundle.getString("INVALID.CREDENTIALS");
	public static final String DEFAULT_PWD = bundle.getString("DEFAULT.PWD");
	public static final String USERNAME_ALREADY_EXISTS = bundle.getString("USERNAME.ALREADY.EXISTS");


	public static final Object SUPERADMIN = bundle.getString("SUPERADMIN");
}	
