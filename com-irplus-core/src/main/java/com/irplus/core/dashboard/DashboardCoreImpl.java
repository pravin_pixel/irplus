package com.irplus.core.dashboard;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.irplus.dao.client.dashboard.DashboardDaoClient;
import com.irplus.dto.CustomerGroupingMngmntInfo;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.util.BusinessException;


@Component
public class DashboardCoreImpl implements  DashboardCore{

	@Autowired
	DashboardDaoClient dashboardDaoClient;
	
	@Override
	public IRPlusResponseDetails getDashboard(String siteId) throws BusinessException {
		
		return dashboardDaoClient.getDashboard(siteId);
	}
	@Override
	public IRPlusResponseDetails listAllFiles(Integer userId) throws BusinessException {
		
		return dashboardDaoClient.listAllFiles(userId);
	}
	
	@Override
	public IRPlusResponseDetails BankwiseFile(CustomerGroupingMngmntInfo user,Long branchId) throws BusinessException {
		
		return dashboardDaoClient.BankwiseFile(user,branchId);
	}
}
