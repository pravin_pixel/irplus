package com.irplus.core.fieldtypes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.irplus.dao.client.fieldtypes.IFieldTypesDaoClient;
import com.irplus.dto.FieldTypesInfo;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.util.BusinessException;

@Component
public class FieldTypesCoreImpl implements IFieldTypesCore{

	@Autowired 
	IFieldTypesDaoClient iFieldTypesDaoClient;
	
	@Override
	public IRPlusResponseDetails createFieldTypes(FieldTypesInfo fieldTypesInfo) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		irPlusResponseDetails =iFieldTypesDaoClient.createFieldTypes(fieldTypesInfo); 
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails getFieldTypesById(String fieldTypesInfoId) throws BusinessException {
	
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		irPlusResponseDetails =iFieldTypesDaoClient.getFieldTypesById(fieldTypesInfoId);
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails updateFieldTypes(FieldTypesInfo fieldTypesInfo) throws BusinessException {
	
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		irPlusResponseDetails =iFieldTypesDaoClient.updateFieldTypes(fieldTypesInfo);
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails deleteFieldTypesById(String fieldTypesInfoId) throws BusinessException {
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		irPlusResponseDetails =iFieldTypesDaoClient.deleteFieldTypesById(fieldTypesInfoId); 
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails showAllFieldTypes() throws BusinessException {
	
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		irPlusResponseDetails =iFieldTypesDaoClient.showAllFieldTypes(); 
		return irPlusResponseDetails;
	}
}
