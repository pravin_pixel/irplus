package com.irplus.core.module;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.irplus.dao.client.modulemngmt.IModulesDaoClient;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.StatusIdInfo;
import com.irplus.dto.module.ModuleBean;
import com.irplus.util.BusinessException;

@Component
public class ModuleCoreImpl implements IModuleCore{
	
	private static final Log log = LogFactory.getLog(ModuleCoreImpl.class);
	
	@Autowired
	IModulesDaoClient moduleDaoClientImpl;

	@Override
	public IRPlusResponseDetails createModule(ModuleBean moduleInfo) throws BusinessException {

		IRPlusResponseDetails moduleResponseDetails=new IRPlusResponseDetails();
		return   moduleResponseDetails=moduleDaoClientImpl.createModule(moduleInfo);
	}

	@Override
	public IRPlusResponseDetails getModuleById(String moduleId) throws BusinessException {
		IRPlusResponseDetails moduleResponseDetails=new IRPlusResponseDetails();
		return moduleResponseDetails=moduleDaoClientImpl.getModuleById(moduleId);
	}

	@Override
	public IRPlusResponseDetails updateModule(ModuleBean moduleInfo) throws BusinessException {
		IRPlusResponseDetails moduleResponseDetails=new IRPlusResponseDetails();
		return moduleResponseDetails=moduleDaoClientImpl.updateModule(moduleInfo);
	}

	@Override
	public IRPlusResponseDetails deleteModuleById(String moduleId) throws BusinessException {
		IRPlusResponseDetails moduleResponseDetails=new IRPlusResponseDetails();
		return moduleResponseDetails=moduleDaoClientImpl.deleteModuleById(moduleId);
	}

	@Override
	public IRPlusResponseDetails findAllModules() throws BusinessException {
		IRPlusResponseDetails moduleResponseDetails=new IRPlusResponseDetails();
		return moduleResponseDetails=moduleDaoClientImpl.findAllModules();
	}

	@Override
	public IRPlusResponseDetails createByConditionModule(ModuleBean moduleInfo) throws BusinessException {
		IRPlusResponseDetails moduleResponseDetails=new IRPlusResponseDetails();
		return moduleResponseDetails=moduleDaoClientImpl.createByConditionModule(moduleInfo);
	}

	@Override
	public IRPlusResponseDetails createModuleNoDuplct(ModuleBean moduleInfo) throws BusinessException {
		IRPlusResponseDetails moduleResponseDetails=new IRPlusResponseDetails();
		return moduleResponseDetails=moduleDaoClientImpl.createModuleNoDuplct(moduleInfo);
	}

	@Override
	public IRPlusResponseDetails updateModuleStatus(StatusIdInfo siInfo) throws BusinessException {
		IRPlusResponseDetails moduleResponseDetails=new IRPlusResponseDetails();
		return moduleResponseDetails=moduleDaoClientImpl.updateModuleStatus(siInfo);
	}
}