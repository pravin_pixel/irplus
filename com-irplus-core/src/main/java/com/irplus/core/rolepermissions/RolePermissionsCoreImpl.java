package com.irplus.core.rolepermissions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.irplus.dao.client.rolepermissions.IRolePermissionDaoClient;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.RolePrmUpdateNCreateInfo;
import com.irplus.dto.StatusIdInfo;
import com.irplus.dto.rolepermission.RolepermissionsBean;
import com.irplus.util.BusinessException;

@Component
public class RolePermissionsCoreImpl implements IRolePermissionCore {

	@Autowired
	IRolePermissionDaoClient iRolePermissionDaoClient;

	@Override
	public IRPlusResponseDetails createRolePermission(RolepermissionsBean rolepermissionsBean)
			throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		irPlusResponseDetails = iRolePermissionDaoClient.createRolePermission(rolepermissionsBean);

		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails getRolePermissionById(String RolepermissionsBeanId) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		irPlusResponseDetails = iRolePermissionDaoClient.getRolePermissionById(RolepermissionsBeanId);
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails updateRolePermission(RolepermissionsBean rolepermissionsBean)
			throws BusinessException {
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		irPlusResponseDetails = iRolePermissionDaoClient.updateRolePermission(rolepermissionsBean);

		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails deleteRolePermissionById(String rolePermissionId) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		irPlusResponseDetails = iRolePermissionDaoClient.deleteRolePermissionById(rolePermissionId);

		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails showAllRolePermission() throws BusinessException {
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		irPlusResponseDetails = iRolePermissionDaoClient.showAllRolePermission();

		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails updateStatusRolePrmn(StatusIdInfo statusIdInfo) throws BusinessException {
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		irPlusResponseDetails = iRolePermissionDaoClient.updateStatusRolePrmn(statusIdInfo);

		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails showAllRoleNMdl(StatusIdInfo statusIdInfo) throws BusinessException {
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails = iRolePermissionDaoClient.showAllRoleNMdl(statusIdInfo);

		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails updateRoleprmUpdNCrt(RolePrmUpdateNCreateInfo rolepermUpNCrte)throws BusinessException {
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
		irPlusResponseDetails = iRolePermissionDaoClient.updateRoleprmUpdNCrt(rolepermUpNCrte);
		return irPlusResponseDetails;
	}

}