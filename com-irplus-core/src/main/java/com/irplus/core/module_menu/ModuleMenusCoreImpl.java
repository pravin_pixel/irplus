package com.irplus.core.module_menu;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.irplus.dao.client.module_menu.IModuleMenusDaoClient;
import com.irplus.dao.client.module_menu.ModuleMenusDaoClientImpl;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.StatusIdInfo;
import com.irplus.dto.menu_modules_association.ModuleMenuBean;
import com.irplus.util.BusinessException;

@Component
public class ModuleMenusCoreImpl implements IModuleMenusCore {

	private static final Log log = LogFactory.getLog(ModuleMenusDaoClientImpl.class);

	@Autowired
	IModuleMenusDaoClient moduleMenusDaoClient;
	
	@Override
	public IRPlusResponseDetails createMenuModule(ModuleMenuBean moduleMenuInfo) throws BusinessException {

		IRPlusResponseDetails mmResponseDetails = new IRPlusResponseDetails();
		
		mmResponseDetails =   moduleMenusDaoClient.createMenuModule(moduleMenuInfo);
		return mmResponseDetails;
	}

	@Override
	public IRPlusResponseDetails getMenuModuleById(String moduleMenuId) throws BusinessException {
		
		IRPlusResponseDetails mmResponseDetails = new IRPlusResponseDetails();
		
		mmResponseDetails =   moduleMenusDaoClient.getMenuModuleById(moduleMenuId);
		return mmResponseDetails;
	}

	@Override
	public IRPlusResponseDetails updateMenuModule(ModuleMenuBean moduleMenuInfo) throws BusinessException {
		
		IRPlusResponseDetails mmResponseDetails = new IRPlusResponseDetails();
		
		mmResponseDetails =   moduleMenusDaoClient.updateMenuModule(moduleMenuInfo);
		return mmResponseDetails;
	}

	@Override
	public IRPlusResponseDetails deleteMenuModuleById(String moduleMenuId) throws BusinessException {
		
		IRPlusResponseDetails mmResponseDetails = new IRPlusResponseDetails();
		
		mmResponseDetails =   moduleMenusDaoClient.deleteMenuModuleById(moduleMenuId);
		return mmResponseDetails;
	}

	@Override
	public IRPlusResponseDetails findAllMenuModule() throws BusinessException {

		IRPlusResponseDetails mmResponseDetails = new IRPlusResponseDetails();
		
		mmResponseDetails =   moduleMenusDaoClient.findAllMenuModule();
		return mmResponseDetails;
	}

	@Override
	public IRPlusResponseDetails findAllByStatus() throws BusinessException {

		IRPlusResponseDetails mmResponseDetails = new IRPlusResponseDetails();
		
		mmResponseDetails =   moduleMenusDaoClient.findAllByStatus();
		return mmResponseDetails;
	}

	@Override
	public IRPlusResponseDetails UpdateStatus(StatusIdInfo sidInfo) throws BusinessException {

		IRPlusResponseDetails mmResponseDetails = new IRPlusResponseDetails();		
		mmResponseDetails =   moduleMenusDaoClient.UpdateStatus(sidInfo);
		return mmResponseDetails;
	}

	@Override
	public IRPlusResponseDetails DeleteUpdateStatus(StatusIdInfo sidInfo) throws BusinessException {

		IRPlusResponseDetails mmResponseDetails = new IRPlusResponseDetails();		
		mmResponseDetails =   moduleMenusDaoClient.DeleteUpdateStatus(sidInfo);
		return mmResponseDetails;
	}

	@Override
	public IRPlusResponseDetails createMenuModuleNoDuplicate(ModuleMenuBean moduleMenuInfo) throws BusinessException {
		
		IRPlusResponseDetails mmResponseDetails = new IRPlusResponseDetails();		
		mmResponseDetails =   moduleMenusDaoClient.createMenuModuleNoDuplicate(moduleMenuInfo);
		return mmResponseDetails;
	}

}
