package com.irplus.core.customerFunctionsMgnt;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.irplus.dao.client.customerFunctionsMgnt.ICustomerFunctionsMngmntDaoClient;
import com.irplus.dto.CustomerFunctionInfo;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.util.BusinessException;

@Component
public class CustomerFunctionsMngmntCoreImpl implements ICustomerFunctionsMngmntCore {

	private static final Logger log = Logger.getLogger(CustomerFunctionsMngmntCoreImpl.class);
	
	@Autowired
	ICustomerFunctionsMngmntDaoClient iCustomerFunctionsMngmntDaoClient; 
	
	@Override
	public IRPlusResponseDetails createCustomerFunctions(CustomerFunctionInfo customerFunctionInfo) throws BusinessException {
		
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		log.debug("Inside of CustomerFunctionsMngmntCoreImpl");
		return irPlusResponseDetails = iCustomerFunctionsMngmntDaoClient.createCustomerFunctions(customerFunctionInfo);
	}

	@Override
	public IRPlusResponseDetails getCustomerFunctionsById(String customerFunctionInfoId) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		log.debug("Inside of CustomerFunctionsMngmntCoreImpl : getCustomerFunctionsById ()");
		return irPlusResponseDetails = iCustomerFunctionsMngmntDaoClient.getCustomerFunctionsById(customerFunctionInfoId);
	}

	@Override
	public IRPlusResponseDetails updateCustomerFunctions(CustomerFunctionInfo customerFunctionInfo) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		log.debug("Inside of CustomerFunctionsMngmntCoreImpl : updateCustomerFunctions ()");		
		return irPlusResponseDetails = iCustomerFunctionsMngmntDaoClient.updateCustomerFunctions(customerFunctionInfo);
	}

	@Override
	public IRPlusResponseDetails statusUpdateCustmrFunctnsById(String customerFunctionInfoId) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		log.debug("Inside of CustomerFunctionsMngmntCoreImpl : updateCustomerFunctions ()");		
		return irPlusResponseDetails = iCustomerFunctionsMngmntDaoClient.statusUpdateCustmrFunctnsById(customerFunctionInfoId);
	}

	@Override
	public IRPlusResponseDetails showAllCustmrFunctions() throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		log.debug("Inside of CustomerFunctionsMngmntCoreImpl : updateCustomerFunctions ()");		
		return irPlusResponseDetails = iCustomerFunctionsMngmntDaoClient.showAllCustmrFunctions();
	}

}