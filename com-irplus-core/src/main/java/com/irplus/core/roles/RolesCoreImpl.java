package com.irplus.core.roles;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.irplus.dao.client.roles.IRolesDaoClient;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.StatusIdInfo;
import com.irplus.dto.roles.RoleBean;
import com.irplus.util.BusinessException;

@Component
public class RolesCoreImpl implements IRolesCore {

	private static final Log log = LogFactory.getLog(RolesCoreImpl.class);
	
	@Autowired
	IRolesDaoClient roleDaoClient;
	
	@Override
	public IRPlusResponseDetails createRole(RoleBean roleBean) throws BusinessException {

		IRPlusResponseDetails roleResponseDetails = new IRPlusResponseDetails() ;
		
		log.debug("Inside of RolesDaoClientImpl :: createRole( ) ");
	
		roleResponseDetails = roleDaoClient.createRole(roleBean);
		return roleResponseDetails;
	}

	@Override
	public IRPlusResponseDetails getRoleById(String roleId) throws BusinessException {
		IRPlusResponseDetails roleResponseDetails = new IRPlusResponseDetails() ;
		roleResponseDetails=roleDaoClient.getRoleById(roleId);
		return roleResponseDetails;
	}

	@Override
	public IRPlusResponseDetails updateRole(RoleBean roleBean) throws BusinessException {
		IRPlusResponseDetails roleResponseDetails = new IRPlusResponseDetails() ;
		roleResponseDetails=roleDaoClient.updateRole(roleBean);
		return roleResponseDetails;
	}

	@Override
	public IRPlusResponseDetails deleteRoleById(String roleId) throws BusinessException {
		IRPlusResponseDetails roleResponseDetails = new IRPlusResponseDetails() ;
		roleResponseDetails=roleDaoClient.deleteRoleById(roleId);
		return roleResponseDetails;
	}

	@Override
	public IRPlusResponseDetails ShowAllRoles() throws BusinessException {
		IRPlusResponseDetails roleResponseDetails = new IRPlusResponseDetails() ;
		roleResponseDetails = roleDaoClient.ShowAllRoles();
		return roleResponseDetails;
	}

	@Override
	public IRPlusResponseDetails removeDuplicateRole(RoleBean roleBean) throws BusinessException {

		IRPlusResponseDetails roleResponseDetails = new IRPlusResponseDetails() ;
		roleResponseDetails = roleDaoClient.removeDuplicateRole(roleBean);
		return roleResponseDetails;
	}

	@Override
	public IRPlusResponseDetails updateRoleStatus(StatusIdInfo statusIbInfo) throws BusinessException {
		
		IRPlusResponseDetails roleResponseDetails = new IRPlusResponseDetails() ;
		roleResponseDetails = roleDaoClient.updateRoleStatus(statusIbInfo);
		return roleResponseDetails;
	}

	@Override
	public IRPlusResponseDetails ShowAllRoles_roleprm() throws BusinessException {
		IRPlusResponseDetails roleResponseDetails = new IRPlusResponseDetails() ;
		roleResponseDetails = roleDaoClient.ShowAllRoles_roleprm();
		return roleResponseDetails;
	}

}