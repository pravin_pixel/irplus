package com.irplus.core.menumgmt;

import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.irplus.dao.client.menumgmt.IMenuMgmtDaoClient;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.StatusIdInfo;
import com.irplus.dto.menu.AddMenu;
import com.irplus.util.BusinessException;
import com.irplus.util.IRPlusConstants;

@Component
public class MenuMgmntCoreImpl implements IrMenuMgmntCore{
	
	private static final Logger log = Logger.getLogger(MenuMgmntCoreImpl.class);

    @Autowired
    IMenuMgmtDaoClient menuMgmtDaoClient;

	@Override
	public IRPlusResponseDetails createMenu(AddMenu menuInfo) throws BusinessException {
		
		IRPlusResponseDetails menuResponseDetails = new IRPlusResponseDetails();
		
		menuResponseDetails=menuMgmtDaoClient.createMenu(menuInfo);

		return menuResponseDetails;
	}

	@Override
	public IRPlusResponseDetails getMenu(String menuid) throws BusinessException {
		
		IRPlusResponseDetails menuResponseDetails = new IRPlusResponseDetails();
		
		menuResponseDetails=menuMgmtDaoClient.getMenu(menuid);
	
		return menuResponseDetails;
	}

	@Override
	public IRPlusResponseDetails updateMenu(AddMenu menuInfo) throws BusinessException {
		
		IRPlusResponseDetails menuResponseDetails = new IRPlusResponseDetails();
		
		menuResponseDetails=menuMgmtDaoClient.updateMenu(menuInfo);
	
		return menuResponseDetails;	
	}

	@Override
	public IRPlusResponseDetails deleteMenu(String menuid) throws BusinessException {
		
		IRPlusResponseDetails menuResponseDetails = new IRPlusResponseDetails();
		
		menuResponseDetails=menuMgmtDaoClient.deleteMenu(menuid);
		
		return menuResponseDetails;
	}
    	
	

	//while trying to add menu then it will check same name existed or not existed if not it will store
	
	public IRPlusResponseDetails validateMenuAndAdd(AddMenu insert_menu)throws BusinessException{
		
		IRPlusResponseDetails menuResponseDetails = new IRPlusResponseDetails();
		
	//	List<AddMenu> menu_list = menuMgmtDaoClient.findAllMenus();

		//code adding
		menuResponseDetails= menuMgmtDaoClient.findAllMenus();
		
		List<AddMenu> menu_list=menuResponseDetails.getMenus();
		///closing
		
		Boolean flag=true;
					
		//validating menu item exisitence with menus in db
					
			if(menu_list == null) {  //is empty
				
				log.debug("no menu instance found ::Menu list is empty :: null pointer exception");
				
				menuResponseDetails=menuMgmtDaoClient.createMenu(insert_menu);
				
				log.debug("Menu add get successful ");
				menuResponseDetails.setValidationSuccess(true);
				menuResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
				menuResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
				
			}else {				
						for (AddMenu menu : menu_list) {
							
						//first scenario : menu not equals flag=true 
						
						//second scenario: menu is equals flag = false
						
						//System.out.println(insert_menu.getMenuName()+ "     "+menu.getMenuName());
							log.debug(insert_menu.getMenuName()+ "     "+menu.getMenuName());
							
						//	System.out.println("flag :"+flag);
							
							log.debug("flag :"+flag);
							if(insert_menu.getMenuName().equals(menu.getMenuName())){   
								
								flag=false;
								
							}
						}
			}
			
			IRPlusResponseDetails menuResponseDetails1 = new IRPlusResponseDetails();
		if(flag){			
			menuResponseDetails1=menuMgmtDaoClient.createMenu(insert_menu);
		
			log.info("This create menu is placing db");
			menuResponseDetails1.setValidationSuccess(true);
			menuResponseDetails1.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			menuResponseDetails1.setStatusCode(IRPlusConstants.SUCCESS_CODE);
			
		}else{
			menuResponseDetails1.setValidationSuccess(false);
			menuResponseDetails1.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
			menuResponseDetails1.setStatusCode(IRPlusConstants.ERR_CODE);
			log.debug("This that menu already available so not adding menu");
		}
		return menuResponseDetails;
	}

	
		@Override
		public IRPlusResponseDetails findAllMenus() throws BusinessException {
	
			IRPlusResponseDetails menuResponseDetails = new IRPlusResponseDetails();
			
			menuResponseDetails= menuMgmtDaoClient.findAllMenus();
			
			return menuResponseDetails;
		}
	
	
	@Override
	public IRPlusResponseDetails getAllActiveMenus() throws BusinessException {

		IRPlusResponseDetails menuResponseDetails = new IRPlusResponseDetails();		
		
		menuResponseDetails= menuMgmtDaoClient.getActiveMenus();	
		List<AddMenu> isactive_menu_list = new ArrayList<AddMenu>();
		List<AddMenu> menu_list=menuResponseDetails.getMenus();
				
		for (AddMenu menu : menu_list) {
			
			log.debug("menu.getStatus()  : "+menu.getStatus() + "  menu.getMenuName() : "+menu.getMenuName());
					
			if(menu.getStatus()!=null){
				
				if(menu.getStatus()==1){
					isactive_menu_list.add(menu);
					log.debug("FF menu :"+menu);
				}				
			}			
		}
	
		if(isactive_menu_list.isEmpty()){
			menuResponseDetails.setValidationSuccess(false);
	//		menuResponseDetails.setMenus(isactive_menu_list);
			menuResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
			menuResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
		}else{
			menuResponseDetails.setValidationSuccess(true);
			menuResponseDetails.setMenus(isactive_menu_list);
			
			menuResponseDetails.setRecordsTotal(isactive_menu_list.size());
			menuResponseDetails.setRecordsFiltered(isactive_menu_list.size());
			menuResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
			menuResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
		}
		menuResponseDetails.setMenus(isactive_menu_list);		
		
		return menuResponseDetails;
	
	}

	@Override
	public IRPlusResponseDetails updateMenuStatusPrm(StatusIdInfo siInfo) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		irPlusResponseDetails= menuMgmtDaoClient.updateMenuStatusPrm(siInfo);
		
		return irPlusResponseDetails;
	}

	@Override
	public IRPlusResponseDetails createMenuWithoutDpct(AddMenu menuInfo) throws BusinessException {

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		irPlusResponseDetails= menuMgmtDaoClient.createMenuWithoutDpct(menuInfo);
		
		return irPlusResponseDetails;
	}
	
}