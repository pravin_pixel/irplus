
var isJpg = function(name) {
    return name.match(/jpg$/i)
};
    
var isPng = function(name) {
    return name.match(/png$/i)
};
	function uploadBranchLogo()
	{
		//alert("Submitting file");
		//var file =  $("#bank-img-upload");
	    var filename = $.trim($("#bank-img-upload").val());
		//alert("filename: "+JSON.stringify(new FormData(document.getElementById("fileForm"))));
        
        if (!(isJpg(filename) || isPng(filename))) {
            alert('Please browse a JPG/PNG file to upload ...');
            return;
        }
        
        $.ajax({
            url: '../branch/fileUpload',
            type: "POST",
            data: new FormData(document.getElementById("fileForm")),
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            success: function(data) {
             // alert("file uploaded");
			  $("#bnk-bnklogo").attr('src',".."+data.logoPath);
			  $("#bnk-bankLogo").val(data.logoPath);
			 // $("#fileForm").foundation('close'); 
			
			},
            failure : function(jqXHR, textStatus) {
              //alert(jqXHR.responseText);
              alert('File upload failed ...');
		   }
          });
		}
