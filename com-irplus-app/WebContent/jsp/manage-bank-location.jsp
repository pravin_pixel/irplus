<!DOCTYPE html>
<html lang="en">
<jsp:include page="includes/style.jsp"/>
<body class="page-md">
<!-- BEGIN HEADER -->
	<div class="page-header">
		<!-- BEGIN HEADER TOP -->
		<jsp:include page="includes/header-top.jsp" />
		<!-- END HEADER TOP -->
		<!-- BEGIN HEADER MENU -->
		<jsp:include page="includes/header-menu.jsp" />
		<!-- END HEADER MENU -->
	</div>
<!-- END HEADER -->
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<div class="container-fluid">
			<!-- BEGIN PAGE TITLE -->
			<!--<div class="col-md-6 col-sm-6 col-xs-12 page-title">
				<h1>Customers</h1>
			</div>-->
			<div class="col-md-6 col-sm-6 col-xs-12 text-right">
			<div class="fixedbtn-container">
				<a href="javascript:void(0);" class="btn serach m-t-15 search-trigger">
					<i class="fa fa-search"></i> search
				</a>
					<form action="banklocation-setup.jsp" method="post"><input type="hidden" name="bankId" value="<%=request.getParameter("bankId")%>" /><button type="submit" class="btn orange m-t-15"><i class="fa fa-plus"></i></button></form>
			</div>
			</div>
			<!-- END PAGE TITLE -->

		</div>
	</div>
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container">

			<!-- BEGIN PAGE BREADCRUMB -->
			<!--<ul class="page-breadcrumb breadcrumb">
				<li>
					<a href="#">Home</a><i class="fa fa-circle"></i>
				</li>

				<li class="active">
					 Manage Bank Locations
				</li>
			</ul>-->
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row margin-top-10">
				<div class="col-md-12">

					<!-- BEGIN PROFILE CONTENT -->
					<div class="profile-content">
										<div class="col-md-12 nopad searchmain-wraper" id="search-wraper">
					<div class="portlet light">
					<span class="searchclose"> &times; </span>
						<form role="form">


									<div class="row">
									<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="form-group">
										<!--<label for="form_control_1">Bank Customer id</label>-->
										<input type="text" class="form-control" id="form_control_1" placeholder="Location ID">
									</div>
									</div>
									<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="form-group">
										<!--<label for="form_control_1">Customer Name</label>-->
										<input type="text" class="form-control" id="form_control_1" placeholder="Location Name">
									</div>
									</div>
							

									
									<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="form-group">
										<!--<label for="form_control_1">Status</label>-->
										<select class="form-control select2" id="form_control_1">
											<option value="">Status</option>
											<option value="1">Active</option>
											<option value="0">Inactive</option>
										</select>
									</div>
									</div>
								<div class="col-md-12 col-sm-12 col-xs-12">
								<!-- <label for="form_control_1">&nbsp;</label>-->
								<div class="form-group ">
								<button type="button" class="btn blue">Search</button>
								<!-- <button type="button" class="btn default">Reset</button> -->

								</div>
								</div>
								</div>
							</form>
							</div>
							</div>
							
			
							<div class="col-md-12 nopad">

								<div class="portlet light portlet-border col-md-12 col-sm-12 col-xs-12">

						<div class="pagestick-title">
								<span>Manage Locations</span>
							</div>
							<div class="pagestick-left">
								<span id="bank-name"></span>
					</div>
						<div class="portlet-body">
						
							<div class="col-md-12 col-sm-12 col-xs-12 nopad">
							<div class="table-scrollable table-scrollable-borderless">
								<table id="managebnkloc" class="table">
								<thead>
								<tr class="uppercase">
									
									<th >
										 Location
									</th>
									<th>
										 Location id
									</th>
									<th>
										 Primary Contact
									</th>
									<th>
										 Email
									</th>
									<th>
										 Phone No
									</th>
									<th>
										 Customers
									</th>
									<th>
										 City
									</th>
									<th>
										 State
									</th>
									<th>
										Status
									</th>
									<th colspan="2">
										Options
									</th>
										<th>
										
									</th>
								</tr>
								</thead>
								</table>
							</div>
							</div>

						</div>
					</div>
					<!-- END SAMPLE FORM PORTLET-->

				</div>



					</div>
					<!-- END PROFILE CONTENT -->
				</div>
			</div>
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER -->
<!-- BEGIN FOOTER -->
<jsp:include page="includes/footer.jsp" />
<jsp:include page="includes/footer-js.jsp" />


<script>
var bankId=<%=request.getParameter("bankId") %>;

var branchId;
var bankName;

jQuery(document).ready(function() {
   	// initiate layout and plugins
   	Metronic.init(); // init metronic core components
	Layout.init(); // init current layout
	Demo.init(); // init demo features\
	
	

	

	
	
	$('[data-toggle="tooltip"]').tooltip();


	 $('#managebnkloc').DataTable( {searching: false,lengthChange:false,pageLength:50,

		 "ajax": {
    				"url": "../location/list/"+bankId,
    				"dataSrc": "locations",
    				
    					 dataFilter: function(response){
    				            var temp = JSON.parse(response);
    				            $("#bank-name").append(temp.locations[0].bankName);
    				            


    				            return response;

    				        }
  				},


		"processing": true,
		"destroy": true,
		"columns": [
            /* {
				data: null, render: function ( data, type, row ) {

				//alert("data.bankLogo"+data.bankLogo);

				var src = ".."+data.bankLogo;
				branchId=data.branchId;
				
				if(data.bankLogo==''){
					//var src = "/resources/assets/irplus/banklogos/"+data.bankLogo;
						return "<img src='../resources/assets/admin/layout3/img/avatar9.jpg' class='banklogo' alt='bank-logo'>"

					}
					else{
						return "<img src="+src+" class='banklogo' alt='bank-logo'>"
					}
            }
			}, */

			{ "data": "branchLocation" },
			{ "data": "locationId" },
            { "data": "firstname" },
            { "data": "email" },
          	{ "data": "contactno" },
			{"data": "totalCustomers" },
			{ "data": "city" },
			{ "data": "state" },
			{
				data: null, render: function ( data, type, row )
				{
					bankName=data.bankName;
					
					if(data.isdefault=='0'&& data.totalCustomers==0){
						if(data.isactive=='1'){
		            		return stat= '<div class="btn-group" data-toggle="btn-toggle" ><button class="btn btn-success btn-xs active" type="button"><i class="fa fa-square text-green"></i> Active</button><button class="btn btn-default btn-xs" type="button" onclick="funchangestatus('+data.branchId+',0);"><i class="fa fa-square text-red"></i> &nbsp;</button></div>';
						}else{
							
							return stat= '<div class="btn-group" data-toggle="btn-toggle"><button class="btn btn-default btn-xs" type="button" onclick="funchangestatus('+data.branchId+',1);"><i class="fa fa-square text-green"></i> &nbsp;</button><button class="btn btn-danger btn-xs active" type="button"><i class="fa fa-square text-red"></i> InActive</button></div>';
						}
					}else{
						return stat= '';
					}
				}
			},
			{
					data: null, render: function ( data, type, row )
					{


						return  '<form action="banklocation-setup.jsp" method="post"><input type="hidden" name="branchId" value="'+data.branchId+'" /><button type="submit" class="edit-btn btn blue btn-xs"><i class="fa fa-edit"></i></button></form>'
					}

			},
			{
				data: null, render: function ( data, type, row )
				{


					
					
					
					if(data.isdefault=='0'&& data.totalCustomers==0)
						return  '<a href="javascript:;" onclick="deleteBranch('+data.branchId+',2);" class="delete-btn btn red btn-xs" data-toggle="tooltip" data-placement="right" title="Delete!"><i class="fa fa-trash-o"></i></a>'

						else
							return  ''
					
				}

			}
        ]
    } );

	 
	
	 
	});


function filterLocation()
{


	var data =
		{
			bankId:bankId,
			branchLocation:$('#bnkloc-bname').val(),
			locationId:$('#bnkloc-locationId').val(),
			branchStatus: $('#bnkloc-bstatus').val()
		}
	//alert("filter data :"+JSON.stringify(data));
	//console.log(JSON.stringify(data));
	$('#managebnkloc').DataTable( {searching:false,lengthChange:false,pageLength:50,

		ajax : ({
					method: 'post',
					url: '../branch/filter',
					dataSrc: "locations",
					data:data,
					crossDomain:'true',


				 error:function(response,statusTxt,error){

				/* 	alert("Failed to add record :"+response.responseJSON); */
				 }
				}),
		"processing": true,
		"destroy": true,
        "columns": [
        	{
				data: null, render: function ( data, type, row ) {

				//alert("data.bankLogo"+data.bankLogo);

				var src = ".."+data.bankLogo;
				branchId=data.branchId;
				if(data.bankLogo==''){
					//var src = "/resources/assets/irplus/banklogos/"+data.bankLogo;
						return "<img src='../resources/assets/admin/layout3/img/avatar9.jpg' class='banklogo' alt='bank-logo'>"

					}
					else{
						return "<img src="+src+" class='banklogo' alt='bank-logo'>"
					}
            }
			},
            { "data": "bankName" },
            { "data": "branchLocation" },
			{ "data": "locationId" },
            { "data": "firstname" },
            { "data": "email" },
			{ "data": "contactno" },
			{ "data": "totalCustomers" },
			{ "data": "city" },
			{ "data": "state" },

			{ data: null, render: function ( data, type, row ) {
                // Combine the first and last names into a single table field
				if(data.isactive=='1')
	                stat= '<div class="btn-group" data-toggle="btn-toggle" ><button class="btn btn-success btn-xs active" type="button"><i class="fa fa-square text-green"></i> Active</button><button class="btn btn-default btn-xs" type="button" onclick="funchangestatus('+data.branchId+',0);"><i class="fa fa-square text-red"></i> &nbsp;</button></div>';
					else
					 stat= '<div class="btn-group" data-toggle="btn-toggle"><button class="btn btn-default btn-xs" type="button" onclick="funchangestatus('+data.branchId+',1);"><i class="fa fa-square text-green"></i> &nbsp;</button><button class="btn btn-danger btn-xs active" type="button"><i class="fa fa-square text-red"></i> InActive</button></div>';

					return stat;
					}
            },
            {
				data: null, render: function ( data, type, row )
				{

					if(data.isactive=='1')
		            return stat= '<div class="btn-group" data-toggle="btn-toggle" ><button class="btn btn-success btn-xs active" type="button"><i class="fa fa-square text-green"></i> Active</button><button class="btn btn-default btn-xs" type="button" onclick="funchangestatus('+data.branchId+',0);"><i class="fa fa-square text-red"></i> &nbsp;</button></div>';

					else
					return stat= '<div class="btn-group" data-toggle="btn-toggle"><button class="btn btn-default btn-xs" type="button" onclick="funchangestatus('+data.branchId+',1);"><i class="fa fa-square text-green"></i> &nbsp;</button><button class="btn btn-danger btn-xs active" type="button"><i class="fa fa-square text-red"></i> InActive</button></div>';
				}
			},
			{
				data: null, render: function ( data, type, row )
				{


					return  '<a href="update-location.jsp?branchId='+data.branchId+'" class="edit-btn btn blue btn-xs" data-toggle="tooltip" data-placement="right" title="Edit!"><i class="fa fa-edit"></i></a><a href="javascript:;" onclick="deleteBranch('+data.branchId+',2);" class="delete-btn btn red btn-xs" data-toggle="tooltip" data-placement="right" title="Delete!"><i class="fa fa-trash-o"></i></a>'
				}

		}


        ]
    } );
}

function funchangestatus(branchId,isactive)
{
	 swal({
	title: "want to change status?",
   text: "",
   type: "warning",
   showCancelButton: true,
   confirmButtonColor: "#DD6B55",
   confirmButtonText: "Yes, Change it!",
   closeOnConfirm: false,
	closeOnCancel: true
	},
		function () {
		var bankInfo = {
				siteId:"12",
				branchId:branchId,
				isactive:isactive
		}

			$.ajax({
				method: 'post',
				url: '../branch/updatestatus',
				data: JSON.stringify(bankInfo),
				contentType: 'application/json',
				dataType:'JSON',
				crossDomain:'true',
				success: function (response) {
					 swal({title: "Done",text: "Updated",type: "success"},function(){window.location = "manage-bank-location.jsp?bankId="+bankId;});
						// swal('Done!','Added','success'),function()
						// {
							 // window.location = 'manage-bank.jsp';
						// }
						//location.href='manage-bank.jsp';
				},

			 error:function(response,statusTxt,error){

				/* alert("Failed to add record :"+response.responseJSON); */
			 }
			});
	}
)}


function deleteBranch(branchId,isactive)
{
	 swal({
	title: "Want to delete?",
   text: "",
   type: "warning",
   showCancelButton: true,
   confirmButtonColor: "#DD6B55",
   confirmButtonText: "Yes, delete it!",
   closeOnConfirm: false,
	closeOnCancel: true
	},
		function () {
		var bankInfo = {
				siteId:"12",
				branchId:branchId,
				isactive:isactive
		}

			$.ajax({
				method: 'post',
				url: '../branch/delete',
				data: JSON.stringify(bankInfo),
				contentType: 'application/json',
				dataType:'JSON',
				crossDomain:'true',
				success: function (response) {
					 swal({title: "Done",text: "Deleted Successfully",type: "success"},function(){window.location = "manage-bank-location.jsp?bankId="+bankId;});
						// swal('Done!','Added','success'),function()
						// {
							 // window.location = 'manage-bank.jsp';
						// }
						//location.href='manage-bank.jsp';
				},

			 error:function(response,statusTxt,error){

				/* alert("Failed to add record :"+response.responseJSON); */
			 }
			});
	}
)}
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
