<!DOCTYPE html>
<html lang="en">
<jsp:include page="includes/style.jsp"/>
<body class="page-md">
<!-- BEGIN HEADER -->
	<div class="page-header">
		<!-- BEGIN HEADER TOP -->
		<jsp:include page="includes/header-top.jsp" />
		<!-- END HEADER TOP -->
		<!-- BEGIN HEADER MENU -->
		<jsp:include page="includes/header-menu.jsp" />
		<!-- END HEADER MENU -->
	</div>
<!-- END HEADER -->
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<div class="container-fluid">
			<div class="fixedbtn-container">
				
				
				<a href="edit-role.jsp" class="btn orange m-t-15"><i class="fa fa-plus"></i> Add Role</a>
				
				
			</div>
			
		</div>
	</div>
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container-fluid">
			<!-- BEGIN PAGE BREADCRUMB -->
		
				<!--<div class="col-md-12 col-sm-12 col-xs-12 form-group nopad">
			<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
				<ul class="page-breadcrumb breadcrumb">
				<li>
					<a href="#">Home</a><i class="fa fa-circle"></i>
				</li>
				
				<li class="active">
					Manage Role
				</li>
			</ul>
			</div>
			<div class="col-md-6 col-sm-6 col-xs-12 text-right">
				<a href="edit-role.jsp" class="btn blue"><i class="fa fa-plus"></i> Add Role </a>
			</div>
			</div>
			</div>-->
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row margin-top-10">
				<div class="col-md-12">
					
					<!-- BEGIN PROFILE CONTENT -->
					<div class="profile-content">
						<div class="row">
							<div class="col-md-12">
								<div class="portlet light col-md-12 col-sm-12 col-xs-12">
									<div class="pagestick-title">
									<span>Manage Roles</span>
									</div>
									<div class="portlet-body">
										<div class="table-scrollable table-scrollable-borderless">
											<table id="role-table-id" class="table">
												<thead>
													<tr class="uppercase">
														<th>
															 Role Name
														</th>
														<!-- <th>
															 Bank visibility
														</th>
														<th>
															 Restricted
														</th> -->
														<th>
															 Status
														</th>
														<th>
															 Options
														</th>
													</tr>
												</thead>

												
											</table>
										</div>
									</div>
								</div>
					<!-- END SAMPLE FORM PORTLET-->
					
				</div>
							
						</div>
						
					</div>
					<!-- END PROFILE CONTENT -->
				</div>
			</div>
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER -->
<!-- BEGIN FOOTER -->
<jsp:include page="includes/footer.jsp" />
<jsp:include page="includes/footer-js.jsp" />

<script>
$(document).ready(function()
{  	 
	  $('#role-table-id').DataTable( {searching: false,lengthChange:false,pageLength:50,
		  
		  ajax :{
			"url":"../role/showAll",  
			"dataSrc":"roleBeans"
		  },
		//"ajax": "../role/showAll",
		"processing": true,
        //"serverSide": true,x
        "columns": [
			{"data":"rolename"},
         /* {"data":"bankVisibility"},
			{
				data: null, render: function ( data, type, row ) {
		 
			if(data.isrestricted=='1')
			return '<button type="button" class="btn green btn-xs">YES</button>';
			else
			return '<button type="button" class="btn red btn-xs">NO</button>';
			}}, */
	
		{ data: null, render: function ( data, type, row ) {
		   // alert(data.isactive);
		   var urlval = 'role/status/'+data.roleid;
			if(data.statusId=='1')
			return '<div class="btn-group" data-toggle="btn-toggle" ><button class="btn btn-success btn-xs active" type="button"><i class="fa fa-square text-green"></i> Active</button><button class="btn btn-default btn-xs" type="button" onclick="funchangestatus(\''+urlval+'\',0);"><i class="fa fa-square text-red"></i> &nbsp;</button></div>';
			else
			return '<div class="btn-group" data-toggle="btn-toggle"><button class="btn btn-default btn-xs" type="button" onclick="funchangestatus(\''+urlval+'\',1);"><i class="fa fa-square text-green"></i> &nbsp;</button><button class="btn btn-danger btn-xs active" type="button"><i class="fa fa-square text-red"></i> InActive</button></div>';
			}},	
		
		{ data: null, render: function ( data, type, row ){
              //  alert("data.moduleid"+data.moduleid);
			  var urlval = 'role/delete/'+data.roleid;
                return  '<a href="edit-role.jsp?roleid='+data.roleid+'" class="edit-btn btn blue btn-xs" data-toggle="tooltip" data-placement="right" title="Edit!"><i class="fa fa-edit"></i></a><a href="#" class="delete-btn btn red btn-xs" data-toggle="tooltip" data-placement="right" title="Delete!" onclick="funStats(\''+urlval+'\',1)"><i class="fa fa-trash-o"></i></a>'
			}}			
        ]
    } );
});



function funchangestatus(mnu,stat){
	
	swal({
		title: "Are you sure?",
        text: "Sure You want to change the status?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, Change it!",
        closeOnConfirm: true
    },
	function () {
		$.ajax({		
			//url        : $frm,
			url : "../"+mnu+"/"+stat,
			method     : 'GET',
			dataType   : 'json',
			data       : 'menuid=&isactive=',
			beforeSend: function() {
				//loading();
 			},
			success: function(response){ 
				//unloading();
				/*if(response.rslt == '6')
				{					
					swal("Success!", statusmsg, "success");	
					datatblCal(dataGridHdn);
				}
				*/
				location.reload(true);
			}		
		});	
    }); 
}
</script>


<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>