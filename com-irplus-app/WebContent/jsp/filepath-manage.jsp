<!DOCTYPE html>
<html lang="en">
<jsp:include page="includes/style.jsp" />
<body class="page-md">
<!-- BEGIN HEADER -->
	<div class="page-header">
		<!-- BEGIN HEADER TOP -->
		<jsp:include page="includes/header-top.jsp" />
		<!-- END HEADER TOP -->
		<!-- BEGIN HEADER MENU -->
		<jsp:include page="includes/header-menu.jsp" />
		<!-- END HEADER MENU -->
	</div>
	<!-- END HEADER -->
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<div class="container">
			<div class="page-title">
				<h1>Manage File Path</h1>
			</div>
		</div>
	</div>
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container">
			<!-- BEGIN PAGE BREADCRUMB -->
			<!--<div class="col-md-12 col-sm-12 col-xs-12 form-group nopad">
				<div class="row">
					<div class="col-md-6 col-sm-6 col-xs-12">
						<ul class="page-breadcrumb breadcrumb">
							<li>
								<a href="#">Home</a><i class="fa fa-circle"></i>
							</li>
							
							<li class="active">
								Manage File Path
							</li>
						</ul>
					</div>	
				</div>
			</div>-->
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row margin-top-10">
				<div class="col-md-12">
					
					<div class="profile-content fileupload-container">
						<div class="row">
							<div class="portlet light col-md-12 col-sm-12 col-xs-12">
								<div class="portlet-body">
									<div class="col-md-8 col-md-offset-2 ">
										<form role="form" class="form-horizontal filepath-form">
											<fieldset class="col-md-12 col-sm-12 col-xs-12">
												<label class="col-sm-4">IR Plus Root Folder</label>
												<div class="col-sm-8">
														<div class="input-group">
													 <span class="input-group-addon" ><i class="fa fa-file"></i></span>
														<input type="text" class="form-control" id="rootFolderPath" >
													</div><!-- /input-group -->
												</div>
											</fieldset>
											<fieldset class="col-md-12 col-sm-12 col-xs-12">
												<label class="col-sm-4">Ephesoft XML & TIFF Process</label>
												<div class="col-sm-8">
														<div class="input-group">
													 <span class="input-group-addon" ><i class="fa fa-file"></i></span>
														<input type="text" class="form-control" id="ephesoftFolderPath" >
													</div><!-- /input-group -->
												</div>
											</fieldset>
											<fieldset class="col-md-12 col-sm-12 col-xs-12">
												<label class="col-sm-4">ImageHawk XML Process</label>
												<div class="col-sm-8">
												<div class="input-group">
													 <span class="input-group-addon" ><i class="fa fa-file"></i></span>
														<input type="text" class="form-control"  id="imagehawkXmlFolderPath">
													</div><!-- /input-group -->
												</div>
											</fieldset>
											<fieldset class="col-md-12 col-sm-12 col-xs-12">
												<label class="col-sm-4">Output Transmission</label>
												<div class="col-sm-8">
														<div class="input-group">
													 <span class="input-group-addon" ><i class="fa fa-file"></i></span>
														<input type="text" class="form-control" id="outputTransmissionFolderPath">
													</div><!-- /input-group -->
												</div>
											</fieldset>
											<fieldset class="col-md-12 col-sm-12 col-xs-12">
												<label class="col-sm-4">Archive Process Files</label>
												<div class="col-sm-8">
													<div class="input-group">
													 <span class="input-group-addon" ><i class="fa fa-file"></i></span>
														<input type="text" class="form-control" id="archiveProcessFilePath" >
													</div><!-- /input-group -->
												</div>
											</fieldset>
											<div class="col-md-12 col-sm-12 col-xs-12">
											<div class="col-md-12 col-sm-12 col-xs-12 form-actions noborder text-right">
									<button type="button" class="btn default">Cancel</button>
									<input type="submit" class="btn blue" value="Update" onclick="updateFolderPath();" />
								</div>
								</div>
										</form>
									</div>
									
								</div>
							</div>
						</div>
					</div>
					<!-- END PROFILE CONTENT -->
				</div>
			</div>
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER -->
<!-- BEGIN FOOTER -->
<jsp:include page="includes/footer.jsp" />
<jsp:include page="includes/footer-js.jsp" />


<script>
var siteId=<%=session.getAttribute("siteId")%>;
var userid=<%=session.getAttribute("userid")%>

$(document).ready(function(){ 
	
	
	$.ajax({
		method: 'post',
		url: '../site/folder/update',
		data: JSON.stringify(siteInfo),
		contentType: 'application/json',
		dataType:'JSON',
		crossDomain:'true',
		success: function (response) {
			 swal({title: "Done",text: "Updated",type: "success"},function(){window.location = "filepath-manage.jsp";});
				
		},

	 error:function(response,statusTxt,error){
	 
	/* 	alert("Failed to add record :"+response.responseJSON); */
	 }
	});
	
	
	
	
	
});


function updateFolderPath(){
	var siteInfo={
			siteId:siteId,
			rootFolderPath:$("#rootFolderPath").val(),
			emphesoftXmlFolderPath:$("#ephesoftFolderPath").val(),
			imagehawkXmlFolderPath:$("#imagehawkXmlFolderPath").val(),
			outputTransmissionFolderPath:$("#outputTransmissionFolderPath").val(),
			archiveProcessFilePath:$("#archiveProcessFilePath").val()
			
	}
	
	
	$.ajax({
		method: 'post',
		url: '../site/folder/update',
		data: JSON.stringify(siteInfo),
		contentType: 'application/json',
		dataType:'JSON',
		crossDomain:'true',
		success: function (response) {
			 swal({title: "Done",text: "Updated",type: "success"},function(){window.location = "filepath-manage.jsp";});
				
		},

	 error:function(response,statusTxt,error){
	 
		/* alert("Failed to add record :"+response.responseJSON); */
	 }
	});
	
}
</script>


<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>