<!DOCTYPE html>
<html lang="en">
<jsp:include page="includes/style.jsp"/>
<body class="page-md">
<!-- BEGIN HEADER -->
	<div class="page-header">
		<!-- BEGIN HEADER TOP -->
		<jsp:include page="includes/header-top.jsp" />
		<!-- END HEADER TOP -->
		<!-- BEGIN HEADER MENU -->
		<jsp:include page="includes/header-menu.jsp" />
		<!-- END HEADER MENU -->
	</div>
<!-- END HEADER -->
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<div class="container">
			<!-- END PAGE TITLE -->
			<div class="col-md-6 col-sm-6 col-xs-12 text-right ">
				<div class="fixedbtn-container">
				
				<a href="javascript:void(0);" class="btn serach m-t-15 search-trigger">
					<i class="fa fa-search"></i> search
				</a>
				<a href="bank-setup.jsp" class="btn orange m-t-15"><i class="fa fa-plus"></i> Add Bank</a>
				
				
			</div>
			</div>
		</div>
	</div>
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container-fluid">

			<!-- BEGIN PAGE BREADCRUMB -->
			<!--<div class="col-md-12 col-sm-12 col-xs-12 form-group nopad">
			<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
			<ul class="page-breadcrumb breadcrumb">
				<li>
					<a href="#">Home</a><i class="fa fa-circle"></i>
				</li>

				<li class="active">
					Manage Bank
				</li>

			</ul>
			</div>
			<div class="col-md-6 col-sm-6 col-xs-12 text-right">

				<a href="bank-setup.jsp" class="btn orange"><i class="fa fa-plus"></i> Add Bank</a>
			</div>
			</div>
			</div>-->
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row margin-top-10 " >
				<div class="col-md-12">

					<!-- BEGIN PROFILE CONTENT -->
					<div class="profile-content">

					<div class="col-md-12 nopad searchmain-wraper" id="search-wraper">
					<div class="portlet light">
						<span class="searchclose"> &times; </span>
						<form role="form">


									<div class="row">
									<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="form-group">
								<label for="form_control_1">Bank Name</label>
										<input type="text" class="form-control" id="filter-bname" placeholder="Bank Name" List="list"  autocomplete="off">
											<datalist id="list"></datalist>
									</div>
									</div>
									<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="form-group">
							<label for="form_control_1">Customer Name</label>
										<input type="text" class="form-control" id="filter-cname" placeholder="Customer Name" List="customerlist"  autocomplete="off">
											<datalist id="customerlist"></datalist>
									</div>
									</div> 
									<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="form-group">
											<label for="form_control_1">Customer Id</label>
										<input type="text" class="form-control" id="filter-custId" placeholder="Customer Id"  List="customerCode" autocomplete="off">
									<datalist id="customerCode"></datalist>
									</div>
									</div>
									<!--  <div class="col-md-2 col-sm-6 col-xs-12">
									<div class="form-group">
										<label for="form_control_1">License Type</label>
										<select class="form-control select2" id="filter-lictype">
											<option value="">Select</option>
											<option value="1">Option 1</option>
											<option value="2">Option 2</option>
											<option value="3">Option 3</option>
											<option value="4">Option 4</option>
										</select>

									</div>
									</div>-->

									<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="form-group">
									<label for="form_control_1">File Processing</label>
										<select class="form-control select2" id="fileprocessdd">
											<option value="">File Type</option>

										</select>

									</div>
									</div>

									<!--  <div class="col-md-12 col-sm-12 col-xs-12">
									<div class="form-group">
										<label for="form_control_1">Business processing</label>
										<select class="form-control select2" id="busiprocessdd">
											<option value="">Business Process</option>

										</select>
									</div>
									</div>-->
									<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="form-group">
								<label for="form_control_1">Status</label>
										<select class="form-control select2" id="filter-bstatus">
											<option value="">Status</option>
											<option value="1">Active</option>
											<option value="0">Inactive</option>
										</select>

									</div>
									</div>
									<div class="col-md-12 col-sm-12 col-xs-12 text-right">
									<!--<label for="form_control_1">&nbsp;</label>-->
									<div class="form-md-line-input pb-15">
									<button type="button" class="btn blue" onClick="loadBanks('filter')">SEARCH</button>

									</div>
									</div>
							</div>
							</form>
							 </div>
							</div>
							
							<div class="col-md-12 nopad searchmain-wraper" id="upload-wraper">
					<div class="portlet light">
						<span class="searchclose"> &times; </span>
						<form role="form" id="fileForm" class="" action="javascript:uploadLicense()">
											<div class="col-md-12 col-sm-12 col-xs-12 nopad">
														<div class="medium-title">UPLOAD MULTIPLE FILES FOR PROCESS</div>
											
														<fieldset class="col-md-12 col-sm-12 col-xs-12 form-group nopad">
																<label for="form_control_1">File Type <sup class="red-req">*</sup></label>
																<select class="form-control select2" id="fileType" name="fileType" required>
																<option value="ACH">ACH</option>
																<option value="LOCKBOX">LOCK BOX</option>
																</select>
															</fieldset>
															<fieldset class="col-md-12 col-sm-12 col-xs-12 form-group nopad">
																<label for="form_control_1">File Name<sup class="red-req"></sup></label>
																	<input id="bank-img-upload" name="licFile" type="file" multiple class="file-loading">

															</fieldset>
											
											
											
											
											</div>
										</form>
							 </div>
							</div>

						<div class="row">
							<div class="col-md-12">

					<!-- BEGIN SAMPLE FORM PORTLET-->

					<div class="portlet light col-md-12 col-sm-12 col-xs-12">
					<div class="pagestick-title">
								<span>Manage Banks</span>
					</div>
					
						<div class="portlet-body">
							
							<div class="col-md-12 col-sm-12 col-xs-12 nopad managebank-cont">
							<div class="table-scrollable table-scrollable-borderless">
								<table id="managebank" class="table table-striped">
								<col width="25%"></col>
								<col width="12%"></col>

								<col width="20%"></col>
								<col width="11%"></col>
								<col width="10%"></col>
								<col width="11%"></col>

								<col width="12%"></col>
								<col width="8%"></col>
								<thead>
								<tr class="uppercase">
									<th>
									 Bank
									</th>
									
									<!--<th>
										 Bank Own Id
									</th>-->
									<th>
										 Primary Contact
									</th>
									<th>
										 Email
									</th>
									<th>
										 Contact No
									</th>
									<th>
										 Customer
									</th>
									<th>
										 Location
									</th>
									<th>
										Status
									</th>
									<th>
										Options
									</th>

								</tr>
								</thead>

								</table>
							</div>
							</div>
						</div>
					</div>


					<!-- END SAMPLE FORM PORTLET-->

				</div>

						</div>

					</div>
					<!-- END PROFILE CONTENT -->
				</div>
			</div>
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER -->
<!-- BEGIN FOOTER -->
<jsp:include page="includes/footer.jsp" />
<jsp:include page="includes/footer-js.jsp" />
<script>
var siteId=<%=session.getAttribute("siteId")%>;
var userId=<%=session.getAttribute("userid")%>; 

jQuery(document).ready(function() {
   	// initiate layout and plugins
   	Metronic.init(); // init metronic core components
	Layout.init(); // init current layout
	Demo.init(); // init demo features\
	$('[data-toggle="tooltip"]').tooltip();
	 //$('.table').DataTable();
	loadBanks('list');

	$.ajax({
				method: 'get',
				url: '../master/getData/'+siteId,
				contentType: 'application/json',
				dataType:'JSON',
				crossDomain:'true',
				success: function (response) {

						//alert("Added"+JSON.stringify(response));
						//alert("response.masterData "+response.masterData.busPros[0].processName);
						//location.href='manage-bank.jsp';
					if(response.masterData!=undefined&&response.masterData!=null)
					{
						var buspros= response.masterData.busPros;

						for(j=0;j<buspros.length;j++)
						{
							//alert("In loop");
							//alert("buspros[j].processName"+JSON.stringify(buspros[j]));

							var chk = '<option value='+buspros[j].businessProcessId+'>'+buspros[j].processName+'</option>';

							$('#busiprocessdd').append(chk);
						}
						var fileTypes= response.masterData.fileTypes;

						for(j=0;j<fileTypes.length;j++)
						{
							//alert("In loop");
							//alert("buspros[j].processName"+JSON.stringify(buspros[j]));

							var chk = '<option value='+fileTypes[j].filetypeid+'>'+fileTypes[j].filetypename+'</option>';

							$('#fileprocessdd').append(chk);


						}
					}


				},

			 error:function(response,statusTxt,error){


			 }
			});
});


function loadBanks(operation)
{
	//var url ='../bank/list/'+siteId;
//	var method= 'get';
	//var data={};
	var url ='';
	var method = '';
	var data='';

	 if(operation=='list')
	{
		url="../bank/list/"+userId;
		data={};
		method= 'get';
	}
	if(operation=='filter')
	{
		url="../bank/filter";
		method =  'post';
		data =
		{
			siteId:siteId,
			bankName: $('#filter-bname').val(),
			customerName: $('#filter-cname').val(),
			customerCode: $('#filter-custId').val(),
		/* 	businessProcId:$('#busiprocessdd').val(), */
			fileTypeId:$('#fileprocessdd').val(),
			isactive: $('#filter-bstatus').val(),
			userId:userId
			
		}
		
	}
	$('#managebank').DataTable( {searching: false,lengthChange:false,pageLength:50,

		ajax : ({
					method: method,
					url: url,
					dataSrc: "bankViewInfo",
					data: data,
					crossDomain:'true',


				 error:function(response,statusTxt,error){


				 }
				}),

		"processing": true,
		"destroy": true,
		"ordering":true,
		"columns": [
			{
				data: null, render: function ( data, type, full )
				{
				var src = "http://tcmuonline.com:8080/fileimages/Images/BankLogo/"+data.bankLogo;
				if(data.bankLogo==''){
				//var src = "/resources/assets/irplus/banklogos/"+data.bankLogo;
					return "<img src='../resources/assets/admin/layout3/img/avatar9.jpg' class='banklogo' alt='bank-logo'>"+" "+full.bankName;

				}
				else{
					return "<img src="+src+" class='banklogo' alt='bank-logo'>"+" "+full.bankName;
				}
            }
			},


			
          //{ "data": "bankCode" },
            { "data":null,
            	 "render": function (data, type, full) {
						if(data.firstName != '')
							return full.firstname+" "+full.lastName;},
            },
            { "data": "email" },
			{ "data": "contactno" },
			//{ "data": "createddate" },

			{
				data: null, render: function ( data, type, row )
				{
				//alert("data.bankId"+data.bankId);
                					return '<form action="customer-setup.jsp" method="post"><input type="hidden" name="bankId" value="'+data.bankId+'" /><button type="submit" class="btn purple-plum btn-xs"><i class="fa fa-plus"></i></button></form><form action="manage-customer.jsp" method="post"><input type="hidden" name="bankId" value="'+data.bankId+'" /><button type="submit" class="btn btn-xs green-meadow"><i class="fa fa-eye"></i></button></form>'

				}
			},
			{
				data: null, render: function ( data, type, row )
				{
				//alert("data.bankId"+data.bankId);
                return '<form action="banklocation-setup.jsp" method="post"><input type="hidden" name="bankId" value="'+data.bankId+'" /><button type="submit" class="btn purple-plum btn-xs"><i class="fa fa-plus"></i></button></form><form action="manage-bank-location.jsp" method="post"><input type="hidden" name="bankId" value="'+data.bankId+'" /><button type="submit" class="btn btn-xs green-meadow"><i class="fa fa-eye"></i></button></form>'
				}
			},
			{
				data: null, render: function ( data, type, row )
				{
                  //alert(data.isactive);
				if(data.isactive=='1')
                stat= '<div class="btn-group" data-toggle="btn-toggle" ><button class="btn btn-success btn-xs active" type="button"><i class="fa fa-square text-green"></i> Active</button><button class="btn btn-default btn-xs" type="button" onclick="funchangestatus('+data.bankId+',0);"><i class="fa fa-square text-red"></i> &nbsp;</button></div>';
				else
				 stat= '<div class="btn-group" data-toggle="btn-toggle"><button class="btn btn-default btn-xs" type="button" onclick="funchangestatus('+data.bankId+',1);"><i class="fa fa-square text-green"></i> &nbsp;</button><button class="btn btn-danger btn-xs active" type="button"><i class="fa fa-square text-red"></i> InActive</button></div>';

				return stat;
				}
			},
			{
                data: null, render: function ( data, type, row )
				{
                return  '<form action="bank-setup.jsp" method="post"><input type="hidden" name="bankId" value="'+data.bankId+'" /><button type="submit" class="btn btn-info btn-xs"><i class="fa fa-edit"></i></button></form><a href="javascript:;" onclick="deleteBank('+data.bankId+',2);" class="delete-btn btn btn-danger btn-xs" data-toggle="tooltip" data-placement="right" title="Delete!"><i class="fa fa-trash"></i></a>'
				}
			}
		],

	// "initComplete": function (settings, json) {
        // $('.make-switch').bootstrapSwitch(
		// {
			 // size: 'mini'
		// });
        // },
		"fnDrawCallback": function( settings )
		{
      //alert( 'DataTables has redrawn the table' );
	  $('.make-switch').bootstrapSwitch(
		{
			 size: 'mini'
		});
		},
		responsive: true
    } );

}
function deleteBank(bankId,isactive)
{
	 swal({
	title: "Want to delete?",
   text: "",
   type: "warning",
   showCancelButton: true,
   confirmButtonColor: "#DD6B55",
   confirmButtonText: "Yes, delete it!",
   closeOnConfirm: false,
	closeOnCancel: true
	},
		function () {
		var bankInfo = {
				siteId:"12",
				bankId:bankId,
				isactive:isactive
		}

			$.ajax({
				method: 'post',
				url: '../bank/delete',
				data: JSON.stringify(bankInfo),
				contentType: 'application/json',
				dataType:'JSON',
				crossDomain:'true',
				success: function (response) {
					 swal({title: "Done",text: "Deleted Successfully",type: "success"},function(){window.location = "manage-bank.jsp";});
						// swal('Done!','Added','success'),function()
						// {
							 // window.location = 'manage-bank.jsp';
						// }
						//location.href='manage-bank.jsp';
				},

			 error:function(response,statusTxt,error){

			/* 	alert("Failed to add record :"+response.responseJSON); */
			 }
			});
	}
)}
function funchangestatus(bankId,isactive)
	 {
		 swal({
		title: "Want to change status?",
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, Change it!",
        closeOnConfirm: false,
		closeOnCancel: true
		},
			function () {
			//alert("I am here bankId: "+bankId+" isactive:"+isactive );
			var bankInfo = {
					siteId:siteId,
					bankId:bankId,
					isactive:isactive
			}

				$.ajax({
					method: 'post',
					url: '../bank/updatestatus',
					data: JSON.stringify(bankInfo),
					contentType: 'application/json',
					dataType:'JSON',
					crossDomain:'true',
					success: function (response) {
						 swal({title: "Done",text: "Updated",type: "success"},function(){window.location = "manage-bank.jsp";});
							// swal('Done!','Added','success'),function()
							// {
								 // window.location = 'manage-bank.jsp';
							// }
							//location.href='manage-bank.jsp';
					},

				 error:function(response,statusTxt,error){


				 }
				});
		}
	)}
	
$(function() {
	
	 $("#filter-bname").autocomplete({     

	    source : function(request, response) {
	    
	      $.ajax({
	           url : '../autocomplete/filter/bank',
	           type : 'get',
	           data : {
	        	   userid:userId,
	                  term : request.term
	           },
	           dataType : "json",
	           success : function(data) {
	        	 
	        	   var banklist="";
	        	   for(i=0;i<data.list.length;i++){
	        		   
	        		   banklist += "<option value='"+data.list[i].bankName +"'></option>";

	       
	        	   }
	        	   $("#list").html(banklist);
	          
	        	  
	           }
	    });
	 }
	});  
	});

	

$(function() {
		 $("#filter-cname").autocomplete({     

		    source : function(request, response) {
		    	
		      $.ajax({
		           url : '../autocomplete/filter/customer',
		           type : 'get',
		           data : {
		        	   userid:userId,
		                  term : request.term
		           },
		           dataType : "json",
		           success : function(data) {
		        	   
		        	   var customerlist="";
		        	   for(i=0;i<data.customerBean.length;i++){
		        		 
		        		   customerlist += "<option value='"+data.customerBean[i].companyName +"'></option>";

		        	   }
		        	   $("#customerlist").html(customerlist);
		          
		        	  
		           }
		    });
		 }
		}); 
		});
$(function() {
	 $("#filter-custId").autocomplete({     

	    source : function(request, response) {
	    	 autoFocus:true
	      $.ajax({
	    	     url : '../autocomplete/filter/customerCode',
	           type : 'get',
	           data : {
	        	   userid:userId,
	                  term : request.term
	           },
	           dataType : "json",
	           success : function(data) {
	        	   
	        	   var customercode="";
	        	   for(i=0;i<data.customerBean.length;i++){
	        		 
	        		   customercode += "<option value='"+data.customerBean[i].bankCustomerCode +"'></option>";

	        	   }
	        	   $("#customerCode").html(customercode);
	          
	        	  
	           }
	    });
	 }
	}); 
	}); 
	
</script>



<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
