<!DOCTYPE html>
<html lang="en">
<jsp:include page="includes/style.jsp"/>
<body class="page-md">
<!-- BEGIN HEADER -->
	<div class="page-header">
		<!-- BEGIN HEADER TOP -->
		<jsp:include page="includes/header-top.jsp" />
		<!-- END HEADER TOP -->
		<!-- BEGIN HEADER MENU -->
		<jsp:include page="includes/header-menu.jsp" />
		<!-- END HEADER MENU -->
	</div>
<!-- END HEADER -->
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<div class="container-fluid">
			<div class="fixedbtn-container">
				<a href="javascript:void(0);" class="btn serach m-t-15 search-trigger">
					<i class="fa fa-search"></i> search
				</a>
				
				<a href="create-user.jsp" class="btn orange m-t-15"><i class="fa fa-plus"></i> Add Role</a>
				
				
			</div>
			
		</div>
	</div>
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container-fluid">

			<!-- BEGIN PAGE BREADCRUMB -->
			<!--<div class="col-md-6 col-sm-6 col-xs-12 nopad">
			<ul class="page-breadcrumb breadcrumb">
				<li>
					<a href="#">Home</a><i class="fa fa-circle"></i>
				</li>

				<li class="active">
					 Manage users
				</li>

			</ul>
			</div>
			<div class="col-md-6 col-sm-6 col-xs-12 nopad">
			<div class="form-group text-right">
				<a href="user-setup.jsp" class="btn blue"><i class="fa fa-plus"></i> Add User</a>
			</div>
			</div>-->
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row margin-top-10">
				<div class="col-md-12">
					<div class="profile-content">
						
					<div class="col-md-12 nopad searchmain-wraper" id="search-wraper">
					<div class="portlet light">
						<span class="searchclose"> &times; </span>
						<form role="form">


									<div class="row">
														

								<!-- 	<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="form-group">
											<label for="form_control_1">IR Plus User id</label>
											<input type="text" class="form-control" id="userid" name="userid" placeholder="User ID">


									</div>
									</div> -->

									<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="form-group">
								<label for="form_control_1"> User Name</label>
											<input type="text" class="form-control" id="username" name="username" placeholder="Username" List="UserName" autocomplete="off">
	<datalist id="UserName"></datalist>
									</div>
									</div>
									
									
									<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="form-group">
										<label for="form_control_1">Role</label>
											<select class="form-control select2" id="roleId" name="roleId">
												<option value="">Role</option>

											</select>

									</div>
									</div>
									
									
										<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="form-group">
									<label for="form_control_1">Status</label>
										<select class="form-control select2" id="filter-bstatus">
											<option value="">Status</option>
											<option value="1">Active</option>
											<option value="0">Inactive</option>
										</select>

									</div>
									</div>
									<div class="col-md-12 col-sm-12 col-xs-12 text-right">
									<!--<label for="form_control_1">&nbsp;</label>-->
									<div class="form-md-line-input pb-15">
								<!-- 	<button type="button" class="btn blue" onClick="loadBanks('filter')">SEARCH</button> -->
	<button type="button" class="btn blue" onClick="loadBanks('filter')">SEARCH</button>
									</div>
									</div>
							</div>
							</form>
							 </div>
							</div>
					
					
				
						<div class="row">
							<div class="col-md-12">
					<div class="portlet light col-md-12 col-sm-12 col-xs-12">
						<div class="pagestick-title">
								<span>Manage Users</span>
							</div>
						<div class="portlet-body">
						
							<div class="table-scrollable table-scrollable-borderless ">
								<table id="user-table-id" class="table">
								<thead>
								<tr class="uppercase">
							<!--	<th>

									</th>
									<th>
										 Name
									</th>
									<th>
										 User name
									</th>
									<th>
										 Role
									</th>
									<th>
										 Default Bank
									</th>
									<th>
										 Other <br /> Bank
									</th>
									<th>
										 Email
									</th>
									<th>
										 Phone
									</th>
									<th>
										Created <br /> date
									</th>
									<th>
										Status
									</th>
									<th>
										Options
									</th>     -->
									<th>
										 User
									</th>
									<!-- <th>
										 User name
									</th> -->
									<th>
										 Role
									</th>
									<!-- <th>
										 Default Bank
									</th>
									<th>
										 Other <br/> Bank
									</th> -->
									<th>
										 Email
									</th>
									<th>
										 Phone
									</th>

									<th>
										Status
									</th>
									<th>
										Options
									</th>

								</tr>
								</thead>
								
								
					
								
						<!--	<tbody>

									<tr>
									<td class="fit">
										<img class="user-pic" src="../resources/assets/admin/layout3/img/avatar4.jpg">
									</td>
									<td>
										<span class="bold">Brain</span>
										<span class="bold">Brain</span>
									</td>
									<td>
										 Brain
									</td>
									<td>
										Supervisor
									</td>
									<td>
										Bank of america
									</td>
									<td>
										4
									</td>
									<td>
										info.brain@test.com
									</td>
									<td>
									215468764
									</td>
									<td>
										07/22/2016
									</td>
									<td>
										<input type="checkbox" class="make-switch" checked data-on-color="success" data-off-color="danger" data-on-text="Active" data-off-text="InActive" data-size="small" />
									</td>
									<td>
										<a class="btn-primary btn btn-xs" data-toggle="tooltip" data-placement="right" title="Edit!">
											<i class="fa fa-edit"></i>
										</a>
										<a class="btn btn-xs btn-danger" data-toggle="tooltip" data-placement="right" title="Edit!">
											<i class="fa fa-trash"></i>
										</a>
									</td>
								</tr>
								<tr>
									<td class="fit">
										<img class="user-pic" src="../resources/assets/admin/layout3/img/avatar5.jpg">
									</td>
									<td>
										<span class="bold">williams</span>
										<span class="bold">Brain</span>
									</td>
									<td>
										 Williams
									</td>
									<td>
										Supervisor
									</td>
									<td>
										Bank of america
									</td>
									<td>
										4
									</td>
									<td>
										info.brain@test.com
									</td>
									<td>
										215468764
									</td>
									<td>
										07/21/2016
									</td>
									<td>
										<input type="checkbox" class="make-switch" data-on-color="success" data-off-color="danger" data-on-text="Active" data-off-text="InActive" data-size="small" />
									</td>
									<td>
										<a class="btn-primary btn btn-xs" data-toggle="tooltip" data-placement="right" title="Edit!">
											<i class="fa fa-edit"></i>
										</a>
										<a class="btn btn-xs btn-danger" data-toggle="tooltip" data-placement="right" title="Edit!">
											<i class="fa fa-trash"></i>
										</a>
									</td>
								</tr>
								<tr>
									<td class="fit">
										<img class="user-pic" src="../resources/assets/admin/layout3/img/avatar5.jpg">
									</td>
									<td>
										<span class="bold">williams</span>
										<span class="bold">Brain</span>
									</td>
									<td>
										 Williams
									</td>
									<td>
										Supervisor
									</td>
									<td>
										Bank of america
									</td>
									<td>
										4
									</td>
									<td>
										info.brain@test.com
									</td>
									<td>
										215468764
									</td>
									<td>
										07/21/2016
									</td>
									<td>
										<input type="checkbox" class="make-switch" data-on-color="success" data-off-color="danger" data-on-text="Active" data-off-text="InActive" data-size="small" />
									</td>
									<td>
										<a class="btn-primary btn btn-xs" data-toggle="tooltip" data-placement="right" title="Edit!">
											<i class="fa fa-edit"></i>
										</a>
										<a class="btn btn-xs btn-danger" data-toggle="tooltip" data-placement="right" title="Edit!">
											<i class="fa fa-trash"></i>
										</a>
									</td>
								</tr>
							</tbody>  -->
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
					<!-- END PROFILE CONTENT -->
				</div>
			</div>
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER -->
<!-- BEGIN FOOTER -->
<jsp:include page="includes/footer.jsp" />
<jsp:include page="includes/footer-js.jsp" />





<script>
var siteId=<%= session.getAttribute("siteId")%>;
$(document).ready(function()
{
	
	loadBanks('list');	
	
	$.fn.dataTable.ext.errMode = 'throw'
	function getAllRoles()
	{
		//$(".select2").select2();
		$.ajax({

				method:'get',
				url:'../user/show/all_roles_banks/'+siteId,
				contentType:'application/json',
				dataType:'JSON',
				crossDomain:'true',
				success:function(response)
				{
					//alert(response.oneUserBean.roleArrayList)
					//oneUserBean
					var roleInfo = JSON.stringify(response.userBean.roleArrayList);
					var listItems= "<option value=''>Select Role</option>";
					var jsonData = roleInfo;
					for (var i = 0; i < response.userBean.roleArrayList.length; i++){
			
					  listItems += "<option value='" + response.userBean.roleArrayList[i].roleid + "'>" + response.userBean.roleArrayList[i].rolename + "</option>";
					}

				
					$("#roleId").html('');
					$("#roleId").html(listItems);

				},
				error:function(response,statusTxt,error){
				
					 }
			});
	}

	getAllRoles();
	
	
	
	
});
	
function loadBanks(operation)
{

	var url ='';
	var method = '';
	var data='';

	 if(operation=='list')
	{
		url="../user/show/all/"+siteId,
		
		data={};
		method= 'get';
	}
	if(operation=='filter')
	{
		url="../user/filter";
		method =  'post';
		data =
		{
				siteId:siteId,
				username : $("#username").val(),
				roleId : $("#roleId").val(),
				isactive: $('#filter-bstatus').val(),
		}
		
	}
	
	$('#user-table-id').DataTable( {searching: false,lengthChange:false,pageLength:50,

		ajax : ({
					method: method,
					url: url,
					dataSrc: "userBeans",
					data: data,
					crossDomain:'true',
			
				 error:function(response,statusTxt,error){


				 }
				}),

		"processing": true,
		"destroy": true,
		"ordering":true,
		"columns": [
			
		   	   {
					"mData": null,
                  "mRender": function (data, type, full) {

		
					var src = "http://tcmuonline.com:8080/fileimages/Images/UserLogo/"+data.userPhoto;
					if(data.userPhoto!='')
						
					return "<img src="+src+" class='banklogo' alt=''>"+" "+full.firstName+" "+full.lastName;
					else
					return "<img src='../resources/assets/admin/layout3/img/avatar9.jpg' class='banklogo' alt='user-logo'>"+" "+full.firstName+" "+full.lastName;
					}

				},
       	
			
			
			{"data":"roleName"},
			{"data":"emailAddress"},
			{"data":"contactno"},

		{ data: null, render: function ( data, type, row ) {
		 

			var urlval = 'user/update/status/'+data.userid;
					if(data.isactive=='1')
					return '<div class="btn-group" data-toggle="btn-toggle" ><button class="btn btn-success btn-xs active" type="button"><i class="fa fa-square text-green"></i> Active</button><button class="btn btn-default btn-xs" type="button" onclick="funchangestatus(\''+urlval+'\',0);"><i class="fa fa-square text-red"></i> &nbsp;</button></div>';
					else
					return '<div class="btn-group" data-toggle="btn-toggle"><button class="btn btn-default btn-xs" type="button" onclick="funchangestatus(\''+urlval+'\',1);"><i class="fa fa-square text-green"></i> &nbsp;</button><button class="btn btn-danger btn-xs active" type="button"><i class="fa fa-square text-red"></i> InActive</button></div>';
	}},

		//	<input type="checkbox" class="make-switch" data-on-color="success" data-off-color="danger" data-on-text="Active" data-off-text="InActive" data-size="small" />


		{ data: null, render: function ( data, type, row ){
            
			  var urlval = '/user/update/status/'+data.userid+'/2';
               return  '<form action="create-user.jsp" method="post"><input type="hidden" name="userid" value="'+data.userid+'" /><button type="submit" class="edit-btn btn blue btn-xs"><i class="fa fa-edit"></i></button></form><a href="#" class="delete-btn btn red btn-xs" data-toggle="tooltip" data-placement="right" title="Delete!" onclick="funStats(\''+urlval+'\',1)"><i class="fa fa-trash-o"></i></a>'
			}}
	],
	"fnDrawCallback": function( settings )
	{
 
  $('.make-switch').bootstrapSwitch(
	{
		 size: 'mini'
	});
	},
	responsive: true
	} );
			
}
			
	  $("#chksubmt").click(function(){
			
			getFiltered()
			});

function getFiltered()
{

$('#user-table-id').DataTable().clear().destroy();
	var update_CustomerInfo ={
				siteId:siteId,
				username : $("#username").val(),
				roleId : $("#roleId").val(),	
	}


	$.ajax({
			method: 'post',
			url: '../user/filter',
			data: JSON.stringify(update_CustomerInfo),
			contentType: 'application/json',
			dataType:'JSON',
			crossDomain:'true',
			success: function (response) {
			

				var arr = response.listUserFilter
				
			
				$('#user-table-id').DataTable( {

				
            	"data": response.listUserFilter,

				"processing": true,
				
				"columns": [
					
					   { //"mData":"irCustomerCode"
							"mData": null,
		                   "mRender": function (data, type, full) {

						//photofile //customer_photos
							var src = ".."+data.userPhoto;
							if(data.userPhoto!='')
								
							return "<img src="+src+" class='banklogo' alt=''>"+" "+full.firstName+" "+full.lastName;
							else
							return "<img src='../resources/assets/admin/layout3/img/avatar9.jpg' class='banklogo' alt='user-logo'>"+" "+full.firstName+" "+full.lastName;
							}

						},
						
					{ "data": "rolename" },
					
				    { "data": "emailAddress"},
				    { "data": "contactno" },
					{ data: null, render: function ( data, type, row ) {

						var urlval = 'user/update/status/'+data.userid;
						if(data.isactive=='1')
						return '<div class="btn-group" data-toggle="btn-toggle" ><button class="btn btn-success btn-xs active" type="button"><i class="fa fa-square text-green"></i> Active</button><button class="btn btn-default btn-xs" type="button" onclick="funchangestatus(\''+urlval+'\',0);"><i class="fa fa-square text-red"></i> &nbsp;</button></div>';
						else
						return '<div class="btn-group" data-toggle="btn-toggle"><button class="btn btn-default btn-xs" type="button" onclick="funchangestatus(\''+urlval+'\',1);"><i class="fa fa-square text-green"></i> &nbsp;</button><button class="btn btn-danger btn-xs active" type="button"><i class="fa fa-square text-red"></i> InActive</button></div>';
					}},
					{ data: null, render: function ( data, type, row ){

						  var urlval = 'user/delete/'+data.userid;
							return  '<a href="profile.jsp?userid='+data.userid+'" class="edit-btn btn blue btn-xs" data-toggle="tooltip" data-placement="right" title="Edit!"><i class="fa fa-edit"></i></a><a href="#" class="delete-btn btn red btn-xs" data-toggle="tooltip" data-placement="right" title="Delete!" onclick="funStats(\''+urlval+'\',1)"><i class="fa fa-trash-o"></i></a>'
						}}
				
					],
				//"serverSide": true,x
	/* 			"columns": [
					{//"data":"firstName"

								"data": null,
                                "render": function (data, type, full) {

								var fname = full.userBean.firstName;
								return fname;
							}
								//var urlval = 'customer/update/status/'+data.customerId;
					},
					{

							"data": null,
							"render": function (data, type, full) {

							var username = full.userBean.username;
							return username;
						}
					},
					{
						"data": null,
							"render": function (data, type, full) {

							var roleId = full.userBean.roleId;
							return roleId;
						}
					},
					{
						"data": null,
							"render": function (data, type, full) {

							var lastName = full.userBean.lastName;
							return lastName;
						}
					},
					{
						"data": null,
							"render": function (data, type, full) {

							var lastName = full.userBean.lastName;
							return lastName;
						}
					},
					{
						"data": null,
							"render": function (data, type, full) {

							var emailAddress = full.userBean.emailAddress;
							return emailAddress;
						}
					},
					{
						"data": null,
							"render": function (data, type, full) {

							var contactno = full.userBean.contactno;
							return contactno;
						}
					},

				{ data: null, render: function ( data, type, row ) {

					var urlval = 'user/update/status/'+data.userid;
					if(data.isactive=='1')
					return '<div class="btn-group" data-toggle="btn-toggle" ><button class="btn btn-success btn-xs active" type="button"><i class="fa fa-square text-green"></i> Active</button><button class="btn btn-default btn-xs" type="button" onclick="funchangestatus(\''+urlval+'\',0);"><i class="fa fa-square text-red"></i> &nbsp;</button></div>';
					else
					return '<div class="btn-group" data-toggle="btn-toggle"><button class="btn btn-default btn-xs" type="button" onclick="funchangestatus(\''+urlval+'\',1);"><i class="fa fa-square text-green"></i> &nbsp;</button><button class="btn btn-danger btn-xs active" type="button"><i class="fa fa-square text-red"></i> InActive</button></div>';
				}},

				//	<input type="checkbox" class="make-switch" data-on-color="success" data-off-color="danger" data-on-text="Active" data-off-text="InActive" data-size="small" />


				{ data: null, render: function ( data, type, row ){

					  var urlval = 'user/delete/'+data.userid;
						return  '<a href="profile.jsp?userid='+data.userid+'" class="edit-btn btn blue btn-xs" data-toggle="tooltip" data-placement="right" title="Edit!"><i class="fa fa-edit"></i></a><a href="#" class="delete-btn btn red btn-xs" data-toggle="tooltip" data-placement="right" title="Delete!" onclick="funStats(\''+urlval+'\',1)"><i class="fa fa-trash-o"></i></a>'
					}}

				] */
			} );
			},

		 error:function(response,statusTxt,error)
		 {
			var responsearr = response.responseJSON;


		}
		});
}


function funchangestatus(mnu,stat){

	swal({
		title: "Are you sure?",
        text: "Sure You want to change the status?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, Change it!",
        closeOnConfirm: true
    },
	function () {
		$.ajax({
			//url        : $frm,
			url : "../"+mnu+"/"+stat,
			method     : 'GET',
			dataType   : 'json',
			data       : 'menuid=&isactive=',
			beforeSend: function() {
				//loading();
 			},
			success: function(response){
				//unloading();
				/*if(response.rslt == '6')
				{
					swal("Success!", statusmsg, "success");
					datatblCal(dataGridHdn);
				}
				*/
				location.reload(true);
			}
		});
    });
}





$(function() {
	 $("#username").autocomplete({     

	    source : function(request, response) {
	    	 autoFocus:true
	      $.ajax({
	    	     url : '../autocomplete/filter/username',
	           type : 'get',
	           data : {
	        		siteId:siteId,
	                  term : request.term
	           },
	           dataType : "json",
	           success : function(data) {
	        	   
	        	   var customercode="";
	        	   for(i=0;i<data.list.length;i++){
	        		 
	        		   customercode += "<option value='"+data.list[i].userName+"'></option>";

	        	   }
	        	   $("#UserName").html(customercode);
	          
	        	  
	           }
	    });
	 }
	}); 
	});  
	
	
	


</script>



<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
