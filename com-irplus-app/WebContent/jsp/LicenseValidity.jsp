<!DOCTYPE html>
<html lang="en">
<jsp:include page="includes/style.jsp" />
<!-- BEGIN BODY -->
<body class="page-md login">
<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
<div class="menu-toggler sidebar-toggler">
</div>
<!-- END SIDEBAR TOGGLER BUTTON -->
<!-- BEGIN LOGO -->
<div class="logo page-logo">
	<a href="index.jsp">
				<span>
				<img src="../resources/assets/admin/layout3/img/logo-default.png" alt="logo" class="logo-default"></span>
				<span class="logo-sub">Ar</span>
	</a>
</div>
<!-- END LOGO -->
<!-- BEGIN LOGIN -->
<div class="content">
	<!-- BEGIN LOGIN FORM -->
	<form class="login-form" id="jvalidate">
		<h3 class="form-title">Secure Login</h3>
		<div class="alert alert-danger display-hide">
			<button class="close" data-close="alert"></button>
			<span>
			Enter any username and password. </span>
		</div>
		<div class="form-group">
			<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
			<label class="control-label">Username</label>
			<input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" name="username" id="loginuname" required />
		</div>
		<div class="form-group">
			<label class="control-label">Password</label>
			<input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" name="password" id="loginpwd" required />
		</div>
		<div class="form-actions">
			
			<a id="forget-password" class="forget-password pull-left" data-toggle="modal" data-target="#fgtpass">Forgot Password?</a>
			<input type="button" class="btn btn-success uppercase pull-right" onclick="loginUser()" value="Login" />
		</div>
	</form>
	<!-- END LOGIN FORM -->
</div>
<div class="copyright">
	 2017 � IR plus. Admin panel.
</div>
<!-- END LOGIN -->
<jsp:include page="includes/footer-js.jsp" />
<!-- Modal -->
  <div class="modal fade" id="fgtpass" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Forgot Password Request</h4>
        </div>
        <div class="modal-body">
			<form class="frgtpass-form" id="jvalidate">
				<fieldset>
					<label class="col-sm-4 nopad">Old Password</label>
					<input class="form-control" type="password" />
				</fieldset>
				<fieldset>
					<label class="col-sm-4 nopad">New Password</label>
					<input class="form-control" type="password" />
				</fieldset>
				<fieldset>
					<label class="col-sm-4 nopad">Retype New Password</label>
					<input class="form-control" type="password" />
				</fieldset>
				<div class="form-actions text-center">
					<input type="submit" class="btn" value="Change Password"/>
				</div>
			</form>
        </div>
        <div class="modal-footer">
           
        </div>
      </div>
      
    </div>
  </div>
<script>



  function loginUser()
  {
	 // validfn();
	  //alert("Login user");
	  var userInfo = {
					username: $("#loginuname").val(),
					password:$("#loginpwd").val()
									
				}
		//alert("userInfo :"+JSON.stringify(userInfo));
		if($('#jvalidate').valid())
			{
			$.ajax({
					method: 'post',
					url: '../login',
					data: JSON.stringify(userInfo),
					contentType: 'application/json',
					dataType:'JSON',
					crossDomain:'true',
					beforeSend: function () {
						//loading();
					},
					success: function (response) {
						
						/*alert("usermenu"+response.userMenus[0].menuName);//.empty();
						
						 $("#usermenu").empty();

						 var userMenuList = response.userMenus;

						 var menu = "<ul class='nav navbar-nav'>";

						 for (var i=0;i<userMenuList.length ;i++ )
						 {
							 alert(userMenuList[i].menuName);
							 menu+="<li>"+userMenuList[i].menuName+"</li>";
						 }
						
						menu +="</ul>";
						alert("menu :"+menu);

						 $("#usermenu").html(menu);*/
						 
						
						location.href='index.jsp';
							
						//unloading();		   
					},

				 error:function(response,statusTxt,error)
				 {
					swal({title: "Invalid Credentials",text: "",type: "error"},function(){});

				 }
				});
			}	
  }

  </script>
</body>
<!-- END BODY -->
</html>