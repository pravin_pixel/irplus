<!DOCTYPE html>
<html lang="en">
<jsp:include page="includes/style.jsp"/>
<body class="page-md">
<!-- BEGIN HEADER -->
	<div class="page-header">
		<!-- BEGIN HEADER TOP -->
		<jsp:include page="includes/header-top.jsp" />
		<!-- END HEADER TOP -->
		<!-- BEGIN HEADER MENU -->
		<jsp:include page="includes/header-menu.jsp" />
		<!-- END HEADER MENU -->
	</div>
<!-- END HEADER -->
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<div class="container-fluid">
			<!-- BEGIN PAGE TITLE -->

			<!-- END PAGE TITLE -->

		</div>
	</div>
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container">

			<!-- BEGIN PAGE BREADCRUMB -->
			<!--<ul class="page-breadcrumb breadcrumb">
				<li>
					<a href="#">Home</a><i class="fa fa-circle"></i>
				</li>

				<li class="active">
					 Manage Bank Locations
				</li>
			</ul>-->
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row margin-top-10">
				<div class="col-md-12">

					<!-- BEGIN PROFILE CONTENT -->
					<div class="profile-content">
										
			
							<div class="col-md-12 nopad">

								<div class="portlet light portlet-border col-md-12 col-sm-12 col-xs-12">

						<div class="pagestick-title">
								<span>View Customer Group</span>
							</div>
							<div class="pagestick-left">
								<span id="Group-name"></span>
					</div>
						<div class="portlet-body">
						
							<div class="col-md-12 col-sm-12 col-xs-12 nopad">
							<div class="table-scrollable table-scrollable-borderless">
								<table id="viewcustomerGrp" class="table">
								<thead>
									<tr class="uppercase">
								
									<th> 
										 Bank Name
									</th>
									<th> 
										Branch Location
									</th>
								     <th> 
									    Customer Id	 
									</th>
									<th> 
										Company Name
									</th>
									<th> 
										Created On
									</th>
									<th> 
										Created Time
									</th>
								</tr>
								</thead>
								</table>
							</div>
							</div>

						</div>
					</div>
					<!-- END SAMPLE FORM PORTLET-->

				</div>



					</div>
					<!-- END PROFILE CONTENT -->
				</div>
			</div>
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER -->
<!-- BEGIN FOOTER -->
<jsp:include page="includes/footer.jsp" />
<jsp:include page="includes/footer-js.jsp" />






<script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.2/rollups/aes.js"></script>
<script>



<%-- var customerGrpId = <%= request.getParameter("customerGrpId") %>; --%>
var userId=<%=session.getAttribute("userid")%>;
var isactive="2";
var customerGrpId =<%=request.getParameter("customerGrpId")%>;
jQuery(document).ready(function() {
	

	/* var url_string = window.location.href;
	var url = new URL(url_string);
	  var password = 'password';
	var customerGrpIdval = url.searchParams.get("customerGrpId");
	   	 var  urlv = customerGrpIdval.replace(/\s/g, "+"); 
	  var customerGrpIdddd = CryptoJS.AES.decrypt(urlv, password); 
	   var customerGrpId =customerGrpIdddd.toString(CryptoJS.enc.Utf8); */
	
   	Metronic.init(); // init metronic core components
	Layout.init(); // init current layout
	Demo.init(); // init demo features\
	$('[data-toggle="tooltip"]').tooltip();

		 $('#viewcustomerGrp').DataTable( {searching: false,lengthChange:false,pageLength:50,

			 "ajax": {
	    				"url": '../custmrGrp/customergroup/get/'+customerGrpId,
	    				"dataSrc": "custmrGrpngMngmntInfoList",
	    				 dataFilter: function(response){
 				            var temp = JSON.parse(response);
 				            $("#Group-name").append(temp.custmrGrpngMngmntInfoList[0].customerGroupName);
 				            


 				            return response;

 				        }	
	    			
	  				},


			"processing": true,
			"destroy": true,
			"columns": [
	 
				{ "data": "BankName" },
				{ "data": "branchLocation" },
	            { "data": "bankCustomerCode" },
	            { "data": "companyName" },
	      	  {
	            	data: null, render: function ( data, type, row )
					{
				
					if(data.createddate!=''){
						var src = data.createddate;
	    var d = new Date(src),
	        month = '' + (d.getMonth() + 1),
	        day = '' + d.getDate(),
	        year = d.getFullYear();
	    if (month.length < 2) month = '0' + month;
	    if (day.length < 2) day = '0' + day;
	     var groupdate=[month, day, year].join('-');
	     return groupdate;


					}
					            }
				},
	            { "data":"createdtime"}
		
	        ]
	    } );

		 
	});

	 function cancelGroup(){
		 	
		 	//alert("Cancelled Page");
		 	location.href='manage-custgroup.jsp';
		 }
 
	 
	</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
