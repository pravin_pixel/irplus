<!DOCTYPE html>
<html lang="en">

<jsp:include page="includes/style.jsp"/>

<body class="page-md">

<!-- BEGIN HEADER -->
	<div class="page-header">
		<!-- BEGIN HEADER TOP -->
		<jsp:include page="includes/header-top.jsp" />
		<!-- END HEADER TOP -->
		<!-- BEGIN HEADER MENU -->
		<jsp:include page="includes/header-menu.jsp" />
		<!-- END HEADER MENU -->
	</div>
<!-- END HEADER -->
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<div class="container">
			<!-- BEGIN PAGE TITLE -->
			<div class="col-md-6 col-sm-6 col-xs-12 page-title">
				<h1>Manage Module</h1>
			</div>
			<div class="col-md-6 col-sm-6 col-xs-12 text-right">
				<a href="add-module.jsp" class="btn orange m-t-15"><i class="fa fa-plus"></i> Add Module </a>
			</div>
			<!-- END PAGE TITLE -->
			
		</div>
	</div>
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container">
			<!-- BEGIN PAGE BREADCRUMB -->
		
				<!--<div class="col-md-12 col-sm-12 col-xs-12 form-group nopad">
			<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
				<ul class="page-breadcrumb breadcrumb">
				<li>
					<a href="#">Home</a><i class="fa fa-circle"></i>
				</li>
				
				<li class="active">
					Manage Module
				</li>
			</ul>
			</div>
			<div class="col-md-6 col-sm-6 col-xs-12 text-right">
				<a href="add-module.jsp" class="btn blue"><i class="fa fa-plus"></i> Add Module </a>
			</div>
			</div>
			</div>-->
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row margin-top-10">
				<div class="col-md-12">
					
					<!-- BEGIN PROFILE CONTENT -->
					<div class="profile-content">
						<div class="row">
							<div class="col-md-12">
								<div class="portlet light col-md-12 col-sm-12 col-xs-12">
									<div class="portlet-body">
										<div class="table-scrollable table-scrollable-borderless">
											<table id="manage-module-table" class="table">
												<thead>
													<tr class="uppercase">
														<th>
															 moduleid
														</th>
														<th>
															 modulename
														</th>
														<th>
															 description
														</th>
														<th>
															modulepath
														</th>
														<th>
															 Status
														</th>
														<th>
															 Options
														</th>
													</tr>
												</thead>
												
											</table>
										</div>
									</div>
								</div>
					<!-- END SAMPLE FORM PORTLET-->
					
				</div>
							
						</div>
						
					</div>
					<!-- END PROFILE CONTENT -->
				</div>
			</div>
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER -->
<!-- BEGIN FOOTER -->
<jsp:include page="includes/footer.jsp" />
<jsp:include page="includes/footer-js.jsp" /> 

<script>
jQuery(document).ready(function() {       
   	// initiate layout and plugins
   	Metronic.init(); // init metronic core components
	Layout.init(); // init current layout
	Demo.init(); // init demo features\
	$('[data-toggle="tooltip"]').tooltip();
	 //$('.table').DataTable();
	 
	 function validfn()
	{
		$("#jvalidate").validate();
	}

	 
	// $('#managebank').DataTable( { 
      
	$('.table-file').DataTable({
		
		"columnDefs": [
      { "width": "14%", "targets": 0 },
      { "width": "14%", "targets": 1 },
      { "width": "14%", "targets": 2 },
      { "width": "14%", "targets": 3 },
      { "width": "14%", "targets": 4 },
      { "width": "14%", "targets": 5 },
	  { "width": "14%", "targets": 6 }
    ],
	});
	var accordionsMenu = $('.cd-accordion-menu');

	if( accordionsMenu.length > 0 ) {
		
		accordionsMenu.each(function(){
			var accordion = $(this);
			//detect change in the input[type="checkbox"] value
			accordion.on('change', 'input[type="checkbox"]', function(){
				var checkbox = $(this);
				console.log(checkbox.prop('checked'));
				( checkbox.prop('checked') ) ? checkbox.siblings('ul').attr('style', 'display:none;').slideDown(300) : checkbox.siblings('ul').attr('style', 'display:block;').slideUp(300);
			});
		});
	} 
	
	 $('#manage-module-table').DataTable( {
		 
		 "ajax": {
    				"url": "../module/show/list",
    				"dataSrc": "module"
  				},
   
		
		"processing": true,
		"destroy": true,  
		
        //"serverSide": true,x
        "columns": [
            { "data": "moduleid" },
            { "data": "modulename" },
            { "data": "description" },
            { "data": "modulepath" },
			
			{ data: null, render: function ( data, type, row ) 
				{
					// alert(data.isactive);
					var urlval = 'module/status/'+data.moduleid;
					if(data.isactive=='1')
					return '<div class="btn-group" data-toggle="btn-toggle" ><button class="btn btn-success btn-xs active" type="button"><i class="fa fa-square text-green"></i> Active</button><button class="btn btn-default btn-xs" type="button" onclick="funchangestatus(\''+urlval+'\',0);"><i class="fa fa-square text-red"></i> &nbsp;</button></div>';
					else
					return '<div class="btn-group" data-toggle="btn-toggle"><button class="btn btn-default btn-xs" type="button" onclick="funchangestatus(\''+urlval+'\',1);"><i class="fa fa-square text-green"></i> &nbsp;</button><button class="btn btn-danger btn-xs active" type="button"><i class="fa fa-square text-red"></i> InActive</button></div>';
				}},	
			{
                data: null, render: function ( data, type, row ) {
              //  alert("data.moduleid"+data.moduleid);
			  	var urlval = 'module/delete/'+data.moduleid;
                return  '<a href="add-module.jsp?moduleid='+data.moduleid+'" class="edit-btn btn blue btn-xs" data-toggle="tooltip" data-placement="right" title="Edit!"><i class="fa fa-edit"></i></a><a href="#" class="delete-btn btn red btn-xs" data-toggle="tooltip" data-placement="right" title="Delete!" onclick="funStats(\''+urlval+'\',1)"><i class="fa fa-trash-o"></i></a>'
			}}	
				
				
        ]
    } );
	
/**	function submitForm()
{
	$("#addModuleForm").validate();	
	
	var manageModuleInfo = {
            modulename: $("#modulename").val(),
            description:$("#description").val(),
            modulepath:$("#modulepath").val()
        }

		//alert("data "+jQuery("#addModuleForm").serialize());

    $.ajax({
            method: 'post',
            url: '../module/create',
			data: JSON.stringify(manageModuleInfo),
			contentType: 'application/json',
			dataType:'JSON',
			crossDomain:'true',
			success: function (response) {
				
					alert("Added");
					location.href='manage-module.jsp';
					//jQuery("#manageModule").click();
							   
            },

		 error:function(response,statusTxt,error){
			alert("Failed to add record :"+response.responseJSON.statusMsg);
		 }
        });

} */
	});
	
function funchangestatus(mnu,stat){
	
	swal({
		title: "Are you sure?",
        text: "Sure You want to change the status?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, Change it!",
        closeOnConfirm: true
    },
	function () {
		$.ajax({		
			//url        : $frm,
			url : "../"+mnu+"/"+stat,
			method     : 'GET',
			dataType   : 'json',
			data       : 'menuid=&isactive=',
			beforeSend: function() {
				//loading();
 			},
			success: function(response){ 
				//unloading();
				/*if(response.rslt == '6')
				{					
					swal("Success!", statusmsg, "success");	
					datatblCal(dataGridHdn);
				}
				*/
				location.reload(true);
			}		
		});	
    }); 
}
</script>

</body>
<!-- END BODY -->
</html>