<!DOCTYPE html>
<html lang="en">
<jsp:include page="includes/style.jsp"/>
<jsp:include page="includes/Report.jsp"/>

<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<head>
<style type="text/css">
    .errspan {
       
        margin-left: -19px;
   
    }
</style>
</head>
<body class="page-md">
<!-- BEGIN HEADER -->
	<div class="page-header">
		<!-- BEGIN HEADER TOP -->
		<jsp:include page="includes/header-top.jsp" />
		<!-- END HEADER TOP -->
		<!-- BEGIN HEADER MENU -->
		<jsp:include page="includes/header-menu.jsp" />
		
		<!-- END HEADER MENU -->
	</div>
<!-- END HEADER -->
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<div class="container">
			<!-- BEGIN PAGE TITLE -->

		</div>
	</div>
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content customergrp-section">
		<div class="container">

			<!-- BEGIN PAGE BREADCRUMB -->
		
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row margin-top-10">
				<div class="col-md-12">
					<div class="profile-content">
			
						<div class="row">
							<div class="col-md-12 custreport-wraper">

				<div class="portlet light col-md-12 col-sm-12 col-xs-12" >
					<div class="pagestick-title">
								<span>Day Report</span>
							</div>
							
			<div  class="col-md-12 col-sm-12 col-xs-12">
				<div  class="col-md-6 col-sm-6 col-xs-12">
							<div  class="col-md-12 col-sm-12 col-xs-12">
								<div class="col-md-6 col-sm-6 processtitle">    
 									<label><strong>Process Date<span style='margin-right:4.21em;'>&nbsp;</span>:</strong></label>
							    </div>
							  
								<div class="col-md-7 col-sm-7 processDetails">
								
						<div class = "input-group">
       <input type="text" name="daterange"  id="Betweendate"  class="datePickerValue form-control" style="width:188px; height:28px " val="" autocomplete="off">
         <span class="input-group-addon fa fa-calendar newclass"></span>
      </div>		
      	</div>
							</div>
	
					<div class="col-md-12 col-sm-12 col-xs-12">
								<div class="col-md-6 col-sm-6 processtitle">
										<label><strong>Files Processed<span style='margin-right:3.21em;'>&nbsp;</span>:</strong></label>
								</div>
								<div class="col-md-6 col-sm-6" ><span id="totalFiles"  class="bankCount"></span><span> &nbsp&nbsp </span> <a id="datevalue" ><span id="filetypename1" class="TotalFiles"></span><span> &nbsp</span> <span id="totalACH" class="TotalFiles"></span></a><span id="achSep"></span><a id="lockbox"> <span id="filetypename2" class="TotalFiles"></span><span> &nbsp</span> <span id="totalLBX" class="TotalFiles"></span></a><span id="LbxSep"></span><a id="lockboxs"> <span id="filetypename3"></span><span id="totalLBXs"></span></a><span> &nbsp</span>
								</div>							
						    </div>	
																
			   			  <div class="col-md-12 col-sm-12 col-xs-12">
								<div class="col-md-6 col-sm-6 processtitle">
										<label><strong>Processed Banks<span style='margin-right:2.21em;'> &nbsp;</span>:</strong></label>
								</div>
							    <div class="col-md-6 col-sm-6"><span id="totalBanks" class="bankCount"></span><span>&nbsp</span><span class="Totalbanks">Banks</span>
							   </div>
						 </div>	
						  <div class="col-md-12 col-sm-12 col-xs-12">
								<div class="col-md-6 col-sm-6 processtitle">
									<label><strong>Process Customers<span style='margin-right:1.19em;'> &nbsp;</span>:</strong></label>
								</div>
								<div class="col-md-6 col-sm-6"><span id="totalCustomers" class="bankCount"></span><span> &nbsp</span><span class="Totalbanks">Customers</span>
								</div>									
						 </div>																			
						 <div class="col-md-12 col-sm-12 col-xs-12">
						        <div class="col-md-6 col-sm-6 processtitle">
										<label><strong>Processed Amount<span style='margin-right:1.59em;'>&nbsp;</span>:</strong></label>
								</div>
								<div class="col-md-6 col-sm-6"><span class="bankCount">USD</span><span>&nbsp</span><span id="totalBatchAmt" class="Totalbanks"></span>
								</div>		
						 </div>
			    </div>	
				<div class="col-md-6 col-sm-6 col-xs-12">
				<div class="well Summary">
					<label  class="procSummary"><strong>Process Summary</strong></label>
							</br>
					<div class="col-md-12 col-sm-12 col-xs-12 ">
					<div  class="col-md-3 col-sm-3">
					<label class="procSummaryHeading"><strong>File Type</strong></label>
					</div>
					<div  class="col-md-3 col-sm-3">
					<label class="procSummaryHeading"><strong>Transactions</strong></label>	
					</div>
					<div  class="col-md-3 col-sm-3">
					<label class="Item"><strong>Items</strong></label>	
					</div>
					<div  class="col-md-3 col-sm-3">
					<label class="Amount"><strong>Amount</strong></label>
 					</div>
											<div id="FileSummaryDetails">
											</div>

					</div>
				</div>
				</div>
		
			</div>
							
					
							
			<div  class="col-md-12 col-sm-12 col-xs-12">
				
</br>
				<div class="transaction-single"></div>										
	
				<div class="col-md-6 col-sm-6 col-xs-12">
				
						<div class="portlet-body example dyn-height lefttable-wraper BankTable">							
								<table id="example"  class="table">

								<col width="18%"></col>
								<col width="10%"></col>
								<col width="7%"></col>
								<col width="5%"></col>
								<col width="10%"></col>
	
										<thead class="ftablehead">
										<tr>
										<th>BANK NAME</th>
										<th>BATCH INFO</th>
										<th>TRANSACTIONS</th>
										<th>ITEMS</th>
										<th>AMOUNT</th>	
										</tr>							
										</thead>
								<tbody> 
								</tbody>
							
								</table>
									
					  </div>
				 </div>
						
				 <div class="col-md-6 col-sm-6 col-xs-12">
					<div class="well dyns-height customerTable" >
				<table id="example1"  >
					<thead class="ftablehead">
									<tr>
									<th>CUSTOMER ID</th>
									<th>COMPANY NAME</th>									
									<th>TRANSACTIONS</th>
									<th>ITEMS</th>
									<th>AMOUNT</th>
									</tr>							
					</thead>	
					<tbody>
					</tbody>				
					</table>
					</div> 
				</div>
								
			</div>
	</div>	
						
	</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>


<div class="modal fade dayrep-modal" id="imgupload" role="dialog">
    <div class="modal-dialog modal-lg">
	<!-- Modal content-->
      <div class="modal-content">
	  
         <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
		 <span>&times;</span>
		 </button>
     
       <div class="portlet-body example6 DateTable" style="display:block" >
			<table id="example6" class="display table table-striped table-bordered text-center" >
					<col width="5%"></col>
				<col width="12%"></col>
				<col width="22%"></col>
				<col width="20%"></col>
				<col width="10%"></col>
				<col width="10%"></col>
				<col width="10%"></col>
				<col width="10%"></col>
				<col width="10%"></col>
			<thead class="ftablehead">
					<tr>
						<th></th>
						<th>CUSTOMER ID</th>
						<th>COMPANY NAME</th>
						<th>BANK NAME</th>
						<th>ACH</th>
						<th>LBX</th>
						<th>TRANSACTIONS</th>
						<th>ITEMS</th>
						<th>AMOUNT</th>	
					</tr>
				
				</thead>
				<tbody>
				</tbody>	
			</table>
		</div>
	</div>
    </div>
  </div>
<!-- END PAGE CONTAINER -->
<!-- BEGIN FOOTER -->
<jsp:include page="includes/footer.jsp" />
<jsp:include page="includes/footer-js.jsp" />

<!-- BEGIN JAVASCRIPTS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.2/rollups/aes.js"></script>
<!--<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>-->
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

<script>
var siteId=<%=session.getAttribute("siteId")%>;
var userId=<%=session.getAttribute("userid")%>;


$(document).ready(function(){
	 $.ajaxSetup({ cache: false });
	 $('<script/>',{type:'text/javascript', src:'../resources/assets/global/plugins/jquery.base64.js'}).appendTo('head');

	 $('.fa-calendar').click(function() {
		    $("#Betweendate").focus();
		  });
	
	
	

	 $(function() {
		    $('input[name="daterange"]').daterangepicker({
		    "autoApply": true,
		    opens: 'right',
		    "showCustomRangeLabel": false,
		   
		   
		}, function(start, end, label) {
			
			
			var startDate=start.format('MM/DD/YYYY');
		var endDate=end.format('MM/DD/YYYY');
		
		data =
		{
	    fromdate:startDate,
		todate:endDate,
		siteId:siteId,
		userid:userId,

		}
	
		$.ajax({
		url: '../custmrGrp/report/Daywise/newreport',

		type: "post",
		data: data,
		dataType: "json",


		crossDomain:'true',
		success: function(response) {

			$("#example tbody").empty();
			$("#example1 tbody").empty();

			$("#totalFiles").text(response.customerGroupingMngmntResponse.totalFiles);
			$("#totalACH").text(response.customerGroupingMngmntResponse.AchfileCount);
			$("#totalLBX").text(response.customerGroupingMngmntResponse.lockboxfileCount);
			$("#totalBanks").text(response.customerGroupingMngmntResponse.totalBanks);
			$("#totalCustomers").text(response.customerGroupingMngmntResponse.totalCustomers);
			$("#totalBatchAmt").text(response.customerGroupingMngmntResponse.totalbatchamt);
			
		// since we are using jQuery, you don't need to parse response
		drawTableDet(response); 
		FileSummaryDet(response);
		}
		});

		function drawTableDet(response) {
		if(response.custmrGrpngMngmntInfoList=="")
		{

		var row = $("<tr></tr>")
		$("#example").append(row);
		row.append($("<td></td>")); 
		row.append($("<td colspan='2'>No data available in table</td>"));

		row.append($("<td></td>")); 

		row.append($("<td></td>"));
		}



		else{

		for (var i = 0; i < response.custmrGrpngMngmntInfoList.length; i++) {
		drawRowDet(response.custmrGrpngMngmntInfoList[i]);
		}
		}
		}
		var dy_html='';	 
		function FileSummaryDet(response) {
		for (var j = 0; j < response.FiletypeSummary.length; j++) {

		dy_html+='<div  class="col-md-3 col-sm-3" id="FileTypeName"  style="margin-left: -37px;font-size: 13px;color: #666666d6;font-weight: 600;">'+response.FiletypeSummary[j].fileTypeName+'</div><div  class="col-md-3 col-sm-3" id="SummaryTrans"  style="font-size: 13px;color: #666666d6;font-weight: 600;">'+response.FiletypeSummary[j].TotSummaryTrans+'</div><div  class="col-md-3 col-sm-3" id="SummaryItems"  style="font-size: 13px;color: #666666d6;margin-left: 14px;font-weight: 600;">'+response.FiletypeSummary[j].TotSummaryItems+'</div><div  class="col-md-3 col-sm-3" id="SummaryAmount" style="font-size: 13px;margin-left: -4px;color: #666666d6;font-weight: 600;">'+response.FiletypeSummary[j].TotSummaryAmount+'</div>';


		}
		$("#FileSummaryDetails").html(dy_html);
		}

		function drawRowDet(rowData) {
			
			var Betweendateval=$("#Betweendate").val();
			

		var filetypes= localStorage.getItem('filetype');
		var filetypeid= localStorage.getItem('filetypeid');
		
		 var password = 'password';
			
		 var strVal = ''+filetypes+''; 
		 var filetype = CryptoJS.AES.encrypt(strVal, password);
			
		 var strValue = ''+filetypeid+''; 
		 var filetypeidval = CryptoJS.AES.encrypt(strValue, password);
		 
		 var Betweendatevalue = ''+Betweendateval+''; 
		 var Betweendate = CryptoJS.AES.encrypt(Betweendatevalue, password);
		
		 var bankid = ''+rowData.bankwiseId+''; 
		 var bankidvalue = CryptoJS.AES.encrypt(bankid, password);
		

		var row = $("<tr class=myClass  id="+rowData.bankwiseId+"/>")
		$("#example").append(row);
		row.append($("<td>" +rowData.BankName+ "</td>"));
		row.append($("<td><a href=file-process-list.jsp?Betweendate="+Betweendate+"&filetypeid="+filetype+"&bankId="+bankidvalue+">"+ rowData.BankAchfileCount + "ACH"+"</a>|<a href=file-process-list.jsp?Betweendate="+Betweendate+"&filetypeid="+filetypeidval+"&bankId="+bankidvalue+">"+ rowData.BankLockboxfileCount+ "LBX"+"</a></td>"));
		row.append($("<td>" + rowData.totaltransactions + "</td>"));
		row.append($("<td>" + rowData.totalitems + "</td>"));
		row.append($("<td>" + rowData.BankwiseAmount + "</td>"));

		}  
		

		});
		
		    var startDates = $('#Betweendate').data('daterangepicker').startDate._d;
			  var endDates = $('#Betweendate').data('daterangepicker').endDate._d;
				
             month = '' + (startDates.getMonth() + 1),
            day = '' + startDates.getDate(),
            year = startDates.getFullYear();
        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;
         var startDate=[month, day, year].join('/');
    
              tmonth = '' + (endDates.getMonth() + 1),
            tday = '' + endDates.getDate(),
            tyear = endDates.getFullYear();
        if (tmonth.length < 2) tmonth = '0' + tmonth;
        if (tday.length < 2) tday = '0' + tday;
         var endDate=[tmonth, tday, tyear].join('/');
 
			data =
{
				  fromdate:startDate,
				  todate:endDate,
siteId:siteId,
userid:userId,

}

$.ajax({
url: '../custmrGrp/report/Daywise/newreport',

type: "post",
data: data,
dataType: "json",


crossDomain:'true',
success: function(response) {

	$("#example tbody").empty();
	$("#example1 tbody").empty();

	$("#totalFiles").text(response.customerGroupingMngmntResponse.totalFiles);
	$("#totalACH").text(response.customerGroupingMngmntResponse.AchfileCount);
	$("#totalLBX").text(response.customerGroupingMngmntResponse.lockboxfileCount);
	$("#totalBanks").text(response.customerGroupingMngmntResponse.totalBanks);
	$("#totalCustomers").text(response.customerGroupingMngmntResponse.totalCustomers);
$("#totalBatchAmt").text(response.customerGroupingMngmntResponse.totalbatchamt);
	
// since we are using jQuery, you don't need to parse response
drawTableDet(response); 
FileSummaryDet(response);
}
});

function drawTableDet(response) {
if(response.custmrGrpngMngmntInfoList=="")
{

var row = $("<tr></tr>")
$("#example").append(row);
row.append($("<td></td>")); 
row.append($("<td colspan='2'>No data available in table</td>"));

row.append($("<td></td>")); 

row.append($("<td></td>"));
}



else{

for (var i = 0; i < response.custmrGrpngMngmntInfoList.length; i++) {
drawRowDet(response.custmrGrpngMngmntInfoList[i]);
}
}
}
var dy_html='';	 
function FileSummaryDet(response) {
for (var j = 0; j < response.FiletypeSummary.length; j++) {

dy_html+='<div  class="col-md-3 col-sm-3" id="FileTypeName"  style="margin-left: -37px;font-size: 13px;color: #666666d6;font-weight: 600;">'+response.FiletypeSummary[j].fileTypeName+'</div><div  class="col-md-3 col-sm-3" id="SummaryTrans"  style="font-size: 13px;color: #666666d6;font-weight: 600;">'+response.FiletypeSummary[j].TotSummaryTrans+'</div><div  class="col-md-3 col-sm-3" id="SummaryItems"  style="font-size: 13px;color: #666666d6;margin-left: 14px;font-weight: 600;">'+response.FiletypeSummary[j].TotSummaryItems+'</div><div  class="col-md-3 col-sm-3" id="SummaryAmount" style="font-size: 13px;margin-left: -4px;color: #666666d6;font-weight: 600;">'+response.FiletypeSummary[j].TotSummaryAmount+'</div>';


}
$("#FileSummaryDetails").html(dy_html);
}



function drawRowDet(rowData) {
var filetypes= localStorage.getItem('filetype');
var filetypeid= localStorage.getItem('filetypeid');
var Betweendateval=$("#Betweendate").val();
var password = 'password';

var strVal = ''+filetypes+''; 
var filetype = CryptoJS.AES.encrypt(strVal, password);
	
var strValue = ''+filetypeid+''; 
var filetypeidval = CryptoJS.AES.encrypt(strValue, password);

var Betweendatevalue = ''+Betweendateval+''; 
var Betweendate = CryptoJS.AES.encrypt(Betweendatevalue, password);

var bankid = ''+rowData.bankwiseId+''; 
var bankidvalue = CryptoJS.AES.encrypt(bankid, password);


var row = $("<tr class=myClass id="+rowData.bankwiseId+"/>")
$("#example").append(row); 
row.append($("<td>" +rowData.BankName+ "</td>"));
row.append($("<td><a href=file-process-list.jsp?Betweendate="+Betweendate+"&filetypeid="+filetype+"&bankId="+bankidvalue+">"+ rowData.BankAchfileCount + "ACH"+"</a>|<a href=file-process-list.jsp?Betweendate="+Betweendate+"&filetypeid="+filetypeidval+"&bankId="+bankidvalue+">"+ rowData.BankLockboxfileCount+ "LBX"+"</a></td>"));
row.append($("<td>" + rowData.totaltransactions + "</td>"));
row.append($("<td>" + rowData.totalitems + "</td>"));
row.append($("<td>" + rowData.BankwiseAmount + "</td>"));
	  
}  
			
		});
		
		
		
	 $.ajax({
			method: 'get',
			url: '../master/getData/'+siteId,
			contentType: 'application/json',
			dataType:'JSON',
			crossDomain:'true',
			success: function (response) {

				if(response.masterData!=undefined&&response.masterData!=null)
				{
					var fileTypes= response.masterData.fileTypes;
					var fileach="";
					if(fileTypes.length==1){
					for(j=0;j<fileTypes.length;j++)
					{
	
						localStorage.setItem("filetype", fileTypes[0].filetypeid);
						 $("#filetypename1").text(fileTypes[0].filetypename);
						
					
					}
					}
					if(fileTypes.length==2){
						for(j=0;j<fileTypes.length;j++)
						{
			
							localStorage.setItem("filetype", fileTypes[0].filetypeid);
							 localStorage.setItem("filetypeid", fileTypes[1].filetypeid);
							 $("#filetypename1").text(fileTypes[0].filetypename);
							 $("#filetypename2").text(fileTypes[1].filetypename);
							 
							 $("#achSep").text("|");
						
	
						}
						}
				}


			},

		 error:function(response,statusTxt,error){


		 }
		});
	 

	  
});

$("#datevalue").click(function () {
	
	
	var Betweendateval=$("#Betweendate").val();

	 var totalACH=$("#totalACH").text();

	 if(totalACH==0)
	{
	swal("No Ach Files Processed!...")
	}

	else
		{
	var filetypeval= localStorage.getItem('filetype');
	 var password = 'password';
	
	 var strVal = ''+filetypeval+''; 
	 var filetype = CryptoJS.AES.encrypt(strVal, password);

	 var Betwdate = ''+Betweendateval+''; 
	 var Betweendate = CryptoJS.AES.encrypt(Betwdate, password);
	
	
	window.location.href= "file-process-list.jsp?Betweendate="+Betweendate+"&filetypeid="+filetype;
		}
		
});
	
$("#lockbox").click(function () {
	var Betweendateval=$("#Betweendate").val();
	 var totalLBX=$("#totalLBX").text();
	 
	 if(totalLBX==0)
	 	{
		 swal("No LockBox Files Processed!...")

	 	}
	 
	 	else
	 		{
		var filetypeval= localStorage.getItem('filetypeid');
		
		
		 var password = 'password';
			
		 var strVal = ''+filetypeval+''; 
		 var filetype = CryptoJS.AES.encrypt(strVal, password);

		 var Betwdate = ''+Betweendateval+''; 
		 var Betweendate = CryptoJS.AES.encrypt(Betwdate, password);

			window.location.href= "file-process-list.jsp?Betweendate="+Betweendate+"&filetypeid="+filetype;
	 		}
	});



$('#example tbody').on('click', 'tr', function () {
	
	$("#example1 tbody").empty();
		var bankId = $(this).attr('id');
	
		
		if(bankId!=null)
			{
			$(".lefttable-wraper tr").removeClass('current-row');
			$(this).addClass('current-row'); 
			}

		 var Betweendate=$("#Betweendate").val();

		data =
 		{
				bankwiseId:bankId,
				currentdate:Betweendate,
				userid:userId,
 		}

	$.ajax({
		
	    url: '../custmrGrp/Daywise/Customer/report',
	    type: "post",
	    data: data,
	    dataType: "json",
dataSrc: "custmrGrpngMngmntInfoList",
		
		crossDomain:'true',
	    success: function(response) {
	    	$("#example1 tbody").empty();
	        drawTable(response);
	    }
	});

	function drawTable(response) {
for (var i = 0; i < response.custmrGrpngMngmntInfoList.length; i++) {
	        drawRow(response.custmrGrpngMngmntInfoList[i]);
	      	       
	}
	}

	function drawRow(rowData) {
	        var row = $("<tr data-toggle=modal  data-target=#imgupload id="+rowData.CustId+" id1="+rowData.bankwiseId+"/>")
	    $("#example1").append(row);
        
	    row.append($("<td>" +rowData.bankCustomerCode+ "</td>"));
	    row.append($("<td>" + rowData.companyName+"</td>"));
	    row.append($("<td>" + rowData.totaltransactions + "</td>"));
	    row.append($("<td>" + rowData.totalitems + "</td>"));
	    row.append($("<td>" + rowData.BankwiseAmount + "</td>")); 
	  
	}
	  
});

	   
	   $('#example1 tbody').on('click', 'tr', function () {
		 
	 var custIdval = $(this).attr('id');
		var bankIdval = $(this).attr('id1');
		
		$(".lefttable-wraper tr").removeClass('current-row');
		$(this).addClass('current-row');
		var BetweenDate=$("#Betweendate").val();

		   data =
  		{
  				currentdate:BetweenDate,
  				customerid:custIdval,
  				bankwiseId:bankIdval,
  				userid:userId,
  			
  		}
		 
 var table = $('#example6').DataTable( {
	 ajax : ({
       url: "../custmrGrp/datewise/customerwise",
        type: "post",
	    data: data,
	    dataType: "json",
dataSrc: "custmrGrpngMngmntInfoList",
		
		
	
		}),
		
        "columns": [
            {
                "className":      'details-control',
                "orderable":      false,
                //"data":           null,
                "defaultContent": ''
            },
            { "data": "bankCustomerCode" },
            { "data": "companyName" },
            { "data": "BankName" },
            { "data": "AchfileCount" },
            { "data": "lockboxfileCount" },
            { "data": "totaltransactions" },
            { "data": "totalitems" },
          
            { "data": "BankwiseAmount" ,
            	
           	 "className": 'dt-right',
           }
        ],
        createdRow: function (row, data, indice) {

            $(row).find("td:eq(0)").attr('id', data.bankCustomerCode);
            $(row).addClass('myClass');
        },
    } ); 
	 table.destroy();
    // Add event listener for opening and closing details
    $('#example6 tbody').on('click', 'tr', function () {
    	
    
    
        var tr = $(this).closest('tr');
        var row = table.row( tr );
 
        if ( row.child.isShown() ) {

            row.child.hide();
            tr.removeClass('shown');
        }
        else {
          
            var custIdvalue =  custIdval ;
    		var bankIdvalue =  bankIdval ;
    		var BetweenDates =  BetweenDate ;
 
    		data =
    			{
    				bankwiseId:bankIdvalue,
    				currentdate:BetweenDates,
    				userid:userId,
    				customerid:custIdvalue
    			} 
           	$.ajax({
     
				method: 'post',
				data: data,
				"url": '../custmrGrp/customerwise/report/getdatewise',
	

				dataSrc: "custmrGrpngMngmntInfoList",
				
				crossDomain:'true',
				success: function (response) {
				
					if(response.custmrGrpngMngmntInfoList.length>0){
						var tr_str='';
					

					for(var i=0;i<response.custmrGrpngMngmntInfoList.length;i++)
					{
					        									
						   tr_str += "<tr>"+ "<td>" +response.custmrGrpngMngmntInfoList[i].highestdate+ "</td>"+"<td>" +response.custmrGrpngMngmntInfoList[i].AchfileCount+ "</td>" + "<td>"+response.custmrGrpngMngmntInfoList[i].lockboxfileCount+"</td>" + "<td>" +response.custmrGrpngMngmntInfoList[i].totaltransactions+ "</td>" + "<td>" +response.custmrGrpngMngmntInfoList[i].totalitems+ "</td>" + "<td style='text-align:right'>" +response.custmrGrpngMngmntInfoList[i].totalbatchamt+ "</td>" ;
							"</tr>";

							}
							
							var repTable = "<table class='display table text-center innertable innerCustomerTable'><thead class=ftablehead><tr><th><span></span>DATE</th><th>ACH</th><th>LBX</th><th>TRANSACTIONS</th><th>ITEMS</th><th>AMOUNT</th></tr></thead><tr>"+ tr_str +"</tr></table>";
						
						row.child(repTable).show();
						tr.addClass('shown');
					}
				
				}
	            
	             	});

        }
    } );

	
	   } );	
		
</script>


<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
