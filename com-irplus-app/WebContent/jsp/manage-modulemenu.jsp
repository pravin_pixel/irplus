<!DOCTYPE html>
<html lang="en">
<jsp:include page="includes/style.jsp"/>
<body class="page-md">
<!-- BEGIN HEADER -->
	<div class="page-header">
		<!-- BEGIN HEADER TOP -->
		<jsp:include page="includes/header-top.jsp" />
		<!-- END HEADER TOP -->
		<!-- BEGIN HEADER MENU -->
		<jsp:include page="includes/header-menu.jsp" />
		<!-- END HEADER MENU -->
	</div>
<!-- END HEADER -->
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<div class="container">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1>Manage Module Menu</h1>
			</div>
			<!-- END PAGE TITLE -->
			
		</div>
	</div>
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container">
			<!-- BEGIN PAGE BREADCRUMB -->
		
				<!--<div class="col-md-12 col-sm-12 col-xs-12 form-group nopad">
			<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
				<ul class="page-breadcrumb breadcrumb">
				<li>
					<a href="#">Home</a><i class="fa fa-circle"></i>
				</li>
				
				<li class="active">
					Manage Module Menu
				</li>
			</ul>
			</div>
			</div>
			</div>-->
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row margin-top-10">
				<div class="col-md-12">
					
					<!-- BEGIN PROFILE CONTENT -->
					<div class="profile-content">
						<div class="row">
							<div class="col-md-12">
								<div class="portlet light col-md-12 col-sm-12 col-xs-12">
									<div class="portlet-body">
										<div class="table-scrollable table-scrollable-borderless">
											<table id="module-menu-id" class="table">
												<thead>
													<tr class="uppercase">
														<th>
															 menu
														</th>
											<!--		<th>
															 module
														</th>
														<th>
															 description
														</th>
														<th>
															 modulepath
														</th>	-->
														<th>
															 Status
														</th>
														<th>
															 Options
														</th> 
													</tr>
												</thead>
												
											</table>
										</div>
									</div>
								</div>
					<!-- END SAMPLE FORM PORTLET-->
					
				</div>
							
						</div>
						
					</div>
					<!-- END PROFILE CONTENT -->
				</div>
			</div>
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER -->
<!-- BEGIN FOOTER -->
<jsp:include page="includes/footer.jsp" />
<jsp:include page="includes/footer-js.jsp" />

<script>

jQuery(document).ready(function() {  

	 $('#module-menu-id').removeAttr('width').DataTable( {
		 
		 "ajax": {
    				"url": "../menu/list/active",
    				"dataSrc": "menus"
  				},
    //    "ajax": "../module/show/list",
		
		"processing": true,
		
		"columnDefs": [
      { "width": "15%", "targets": 0 },
      { "width": "15%", "targets": 1 },
      { "width": "15%", "targets": 2 }],
	  
	    "columns": [
			  {	"data": "menuName" },
  	 
			{ data: null, render: function ( data, type, row )
				{
			
			//alert(data.status)
					var urlval = 'menumodule/update/status/'+data.menuid;

					if(data.status=='1')
					return '<div class="btn-group" data-toggle="btn-toggle" ><button class="btn btn-success btn-xs active" type="button"><i class="fa fa-square text-green"></i> Active</button><button class="btn btn-default btn-xs" type="button" onclick="funchangestatus(\''+urlval+'\',0);"><i class="fa fa-square text-red"></i> &nbsp;</button></div>';
					else
					return '<div class="btn-group" data-toggle="btn-toggle"><button class="btn btn-default btn-xs" type="button" onclick="funchangestatus(\''+urlval+'\',1);"><i class="fa fa-square text-green"></i> &nbsp;</button><button class="btn btn-danger btn-xs active" type="button"><i class="fa fa-square text-red"></i> InActive</button></div>';
				}},	
			{
                data: null, render: function ( data, type, row ){
             //menumodule/delete/17
			// alert(data.modulemenuid)
			 	var urlval = 'menumodule/delete/'+data.menuid;
                return  '<a href="edit-modulemenu.jsp?menuid='+data.menuid+'" class="edit-btn btn blue btn-xs" data-toggle="tooltip" data-placement="right" title="Edit!"><i class="fa fa-edit"></i></a><a href="#" class="delete-btn btn red btn-xs" data-toggle="tooltip" data-placement="right" title="Delete!" onclick="funStats(\''+urlval+'\',1)"><i class="fa fa-trash-o"></i></a>'
			}}	
        ]
		
    } );   
});


function funchangestatus(mnu,stat){
	
	swal({
		title: "Are you sure?",
        text: "Sure You want to change the status?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, Change it!",
        closeOnConfirm: true
    },
	function () {
		$.ajax({		
			//url        : $frm,
			url : "../"+mnu+"/"+stat,
			method     : 'GET',
			dataType   : 'json',
			data       : 'menuid=&isactive=',
			beforeSend: function() {
				//loading();
 			},
			success: function(response){ 
				//unloading();
				/*if(response.rslt == '6')
				{					
					swal("Success!", statusmsg, "success");	
					datatblCal(dataGridHdn);
				}
				*/
				location.reload(true);
			}		
		});	
    }); 
}
</script>
</body>
<!-- END BODY -->
</html>