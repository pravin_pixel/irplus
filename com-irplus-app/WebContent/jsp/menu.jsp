<div class="page-header-menu">
		<div class="container">
			<div class="hor-menu ">
				<ul class="nav navbar-nav">
					<li>
						<a href="index.jsp">Dashboard</a>
					</li>
					<li class="menu-dropdown classic-menu-dropdown ">
						<a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:void(0);">
						Admin 
						</a>
						<ul class="dropdown-menu pull-left">
							<li>
								<a href="manage-menu.jsp">Manage Menu</a>
							</li>
							<li>
								<a href="manage-module.jsp">Manage Module</a>
							</li>
							<li>
								<a href="module-menu.jsp">Module Menu</a>
							</li>
							<li>
								<a href="role-setup.jsp">Add Role </a>
							</li>
							<li>
								<a href="manage-roleperm.jsp">Manage Role </a>
							</li>
							<li>
								<a href="profile.jsp">Add User </a>
							</li>
							<li>
								<a href="manage-user.jsp">Manage Users </a>
							</li>
						</ul>
					</li>
					<li class="menu-dropdown classic-menu-dropdown ">
						<a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;">
						Bank 
						</a>
						<ul class="dropdown-menu pull-left">
							<li>
								<a href="bank-setup.jsp">
								
								Bank Setup </a>
								
							</li>
							<li>
								<a href="manage-bank.jsp">
								
								Manage Bank </a>
								
							</li>
							<li>
								<a href="manage-bank-location.jsp">
								
								Manage Location </a>
								
							</li>
							
						</ul>
					</li>
					<li class="menu-dropdown classic-menu-dropdown ">
						<a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;">
						Customer 
						</a>
						<ul class="dropdown-menu pull-left">
							<li>
								<a href="customer-setup.jsp">
								
								Create Customer </a>
								
							</li>
							<li>
								<a href="manage-customer.jsp">
								
								Manage Customer </a>
								
							</li>
							<li>
								<a href="create-group.jsp">
								
								Create Customer Group</a>
								
							</li>
							<li>
								<a href="manage-custgroup.jsp">
								
								Manage Customer Group</a>
								
							</li>
							
						</ul>
					</li>
					<li class="menu-dropdown classic-menu-dropdown ">
						<a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;">
						Settings 
						</a>
						<ul class="dropdown-menu pull-left">
							<li>
								<a href="license-setup.jsp">
								
								License Setup </a>								
							</li>
							<li>
								<a href="file-upload.jsp">
								
								Validate License</a>
								
							</li>
						
							
						</ul>
					</li>
					<li>
						<a href="#">File Process</a>
					</li>
				</ul>
			</div>
			<!-- END MEGA MENU -->
		</div>
	</div>