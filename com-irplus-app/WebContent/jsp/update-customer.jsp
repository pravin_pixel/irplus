<!DOCTYPE html>
<html lang="en">
<jsp:include page="includes/style.jsp"/>
<body class="page-md">
<!-- BEGIN HEADER -->
	<div class="page-header">
		<!-- BEGIN HEADER TOP -->
		<jsp:include page="includes/header-top.jsp" />
		<!-- END HEADER TOP -->
		<!-- BEGIN HEADER MENU -->
		<jsp:include page="includes/header-menu.jsp" />
		<!-- END HEADER MENU -->
	</div>
<!-- END HEADER -->
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<div class="container">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1>Customer Creation</h1>
			</div>
			<!-- END PAGE TITLE -->
			
		</div>
	</div>
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container">
			
			<!-- BEGIN PAGE BREADCRUMB -->
			<div class="col-md-12 col-sm-12 col-xs-12 form-group nopad">
			<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
			<ul class="page-breadcrumb breadcrumb">
				<li>
					<a href="index.jsp">Home</a><i class="fa fa-circle"></i>
				</li>
				
				<li class="active">
					 Customer Creation
				</li>
			</ul>
			</div>
			</div>
			</div>
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row margin-top-10">
				<div class="col-md-12">
					<!-- BEGIN PROFILE SIDEBAR -->
					<div class="profile-sidebar">
						<!-- PORTLET MAIN -->
						<div class="portlet light profile-sidebar-portlet">
							<!-- SIDEBAR USERPIC -->
						<div class="profile-userpic">
								<span class="userpic-inner">
									<img id="bnk-bnklogo" class="img-responsive" alt="" src="../resources/assets/admin/layout3/img/avatar9.jpg"/>
									<a class="edit-pic bank-img" data-toggle="modal" data-target="#imgupload">
										<i class="fa fa-edit"></i>
									</a>
								</span>	
							</div>
							<!-- END SIDEBAR USERPIC -->
							<!-- SIDEBAR USER TITLE -->
							<div class="profile-usertitle">
								<div class="profile-usertitle-name">
									
								</div>
								<div class="irplus-id">
								
								</div>
								<div class="profile-usertitle-job">
									 
								</div>
							</div>
							<!-- END SIDEBAR USER TITLE -->
							<!-- SIDEBAR MENU -->
							<div class="profile-usermenu">
								<ul class="nav"></ul>
							</div>
							<!-- END MENU -->
						</div>
						<!-- END PORTLET MAIN -->
					</div>
					<!-- END BEGIN PROFILE SIDEBAR -->
					<!-- BEGIN PROFILE CONTENT -->
				<div class="profile-content boxwith-sidebar">
					<div class="row">
						<div class="col-md-12">
							<div class="portlet light col-md-12 col-sm-12 col-xs-12 customer-setup">
								<div class="portlet-body form">
									<ul class="nav nav-tabs ">
										<li class="active">
											<a href="#tab1" data-toggle="tab">
												<span class="number">1</span>
												<span class="desc">Customer Info</span>
											</a>
										</li>
										<li>
											<a href="#tab2" data-toggle="tab">
												<span class="number">2</span>
												<span class="desc">Contacts </span>
											</a>
										</li>
										<li>
											<a href="#tab3" data-toggle="tab">
												<span class="number">3</span>
												<span class="desc">Functions </span>
											</a>
										</li>
										  <li>
											<a href="#tab4" data-toggle="tab">
												<span class="number">4</span>
												<span class="desc">ACH Form Setup</span>
											</a>
										</li>
										<li>
											<a href="#tab5" data-toggle="tab">
												<span class="number">5</span>
												<span class="desc">Auto Generated Report</span>
											</a>
										</li>
									</ul>
									<div id="bar-customer" class="progress progress-striped" role="progressbar">
										<div class="progress-bar progress-bar-success"></div>
									</div>
									<div class="tab-content">
										<div class="tab-pane active" id="tab1">
											<form id="jvalidate" action ="#" method="POST">
											<input type="hidden" class="form-control" id="bnk-bankLogo" name="bnk-bankLogo" />
											
											<div class="col-md-4 col-sm-4 col-xs-12">
												<div class="form-group">
													<label class="control-label">Bank Name <sup class="red-req">*</sup></label>
													<select class="form-control select2" id="bnk-name" name="bnk-name" required>
													</select>
												</div>
											</div>
											<div class="col-md-4 col-sm-4 col-xs-12">
												<div class="form-group">
													<label>Bank Location <sup class="red-req">*</sup></label>
													<select class="form-control select2" id="bnk-loc" name="bnk-loc" required>
													</select>
												</div>
											</div>
											<div class="col-md-4 col-sm-4 col-xs-12">
												<div class="form-group">
													<label>Company Name <sup class="red-req">*</sup></label>
													<input type="text" class="form-control" id="comp-name" name="comp-name" required />
												</div>
											</div>
											<div class="col-md-4 col-sm-4 col-xs-12">
												<div class="form-group">
													<label>Bank Customer Id <sup class="red-req">*</sup></label>
													<input type="text" class="form-control" id="comp-id" name="comp-id" required />
												</div>
											</div>
											<div class="col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<label>DDA Bank A/C # <sup class="red-req">*</sup></label>
												<input type="text" class="form-control" id="dda-bank" name="dda-bank" required /> 
											</div>
											</div>
											<div class="col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<label>Company Identification No. (ACH) <sup class="red-req">*</sup></label>
												<input type="text" class="form-control" id="comp-ach" name="comp-ach" required />
											</div>
											</div>
											<div class="portlet-title col-md-12 col-sm-12 col-xs-12 ">
												<div class="caption">
													<span class="caption-subject font-green-sharp bold uppercase mtb-10">Primary contact</span>
												</div>
											</div>
											<div class="col-md-4 col-sm-4 col-xs-12">
												<div class="form-group">
													<label>First Name</label>
													<input type="text" class="form-control" id="fName-id" name="fName-id">
												</div>
											</div>
											<div class="col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<label>Last Name</label>
												<input type="text" class="form-control" id="lastName-id" name="lastName-id">
											</div>
											</div>
											<div class="col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<label>Contact No</label>
												<input type="text" class="form-control" id="contact-id">
											</div>
											</div>
											<div class="col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<label>Email ID</label>
												<input type="email" class="form-control" id="email-id">
											</div>
											</div>
											<div class="col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<label>Address Line 1</label>
												<textarea class="form-control" rows="3" id="address1-id" ></textarea>
											</div>
											</div>
											<div class="col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<label>Address Line 2</label>
												<textarea class="form-control" rows="3" id="address2-id" ></textarea>
											</div>
											</div>
											<div class="col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<label>Website</label>
												<input type="text" class="form-control" id="website-id">
											</div>
											</div>
											<div class="col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<label>state</label>
												<input type="text" class="form-control" id="state-id">
											</div>
											</div>
											<div class="col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<label>City</label>
												<input type="text" class="form-control" id="city-id">
											</div>
											</div>
											<div class="col-md-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<label>Zip code</label>
												<input type="text" class="form-control" id="zipcode-id">
											</div>
											</div>
											
											<div class="col-sm-12 col-md-12 col-xs-12 text-right">
											<div class="form-actions noborder nopad">
												<button type="button" class="btn default">Cancel</button>
												<input type="submit" class="btn blue" value="Save" id="cust-setup" onClick="createCustomrContactsInfo();"/>
											</div>
											</div>
											</form>
										</div>
										<div class="tab-pane" id="tab2">
											<div class="col-md-12 col-sm-12 col-xs-12 bankaccount-listcont">
												<div class="form-group text-right">
													<button type="button" class="btn blue add-row-cust"><i class="fa fa-plus"></i> Add row</button>
												</div>
												<form class="jvalidate">
													<div class="col-md-12 col-sm-12 col-xs-12 bankaccount-single">
																	<div class="col-md-12 col-sm-12 col-xs-12 nopad">
																		<fieldset class="col-md-3 col-sm-6 col-xs-12">
																			<div class="form-group">
																				<label class="control-label">First Name <sup class="red-req">*</sup> </label>
																			<input type="text" class="form-control" name="contact_fname1" id="contact_fname1" required />
																			</div>
																		</fieldset>
																		<fieldset class="col-md-3 col-sm-6 col-xs-12">
																		<div class="form-group">
																			<label class="control-label">Last Name</label>
																			<input type="text" class="form-control" id="contact_lname1" name="contact_lname1">
																		</div>
																		</fieldset>
																		<fieldset class="col-md-3 col-sm-6 col-xs-12">
																			<div class="form-group">
																				<label class="control-label">Contact Role</label>
																				<input type="text" class="form-control" name="contact_crole1" id="contact_crole1">
																			</div>
																		</fieldset>
																		<fieldset class="col-md-3 col-sm-6 col-xs-12">
																		<div class="form-group">
																			<label class="control-label">Phone No. <sup class="red-req">*</sup></label>
																			<input type="text" class="form-control" name="contact_fphoneno1" id="contact_fphoneno1" required />
																		</div>
																		</fieldset>
																	</div>
																	<div class="col-md-12 col-sm-12 col-xs-12 nopad">
																		<fieldset class="col-md-3 col-sm-6 col-xs-12">
																		<div class="form-group">
																			<label class="control-label">Email</label>
																			<input type="email" class="form-control " name="contact_femail1" id="contact_femail1">
																		</div>
																		</fieldset>
																		<fieldset class="col-md-3 col-sm-6 col-xs-12">
																		<div class="form-group">
																			<label class="control-label">Address 1</label>
																			<input type="text" class="form-control" name="contact_faddr11" id="contact_faddr11">
																		</div>
																		</fieldset>
																		<fieldset class="col-md-3 col-sm-6 col-xs-12">
																		<div class="form-group">
																			<label class="control-label">Address 2</label>
																			<input type="text" class="form-control" name="contact_faddr21" id="contact_faddr21">
																		</div>
																		</fieldset>
																		<fieldset class="col-md-3 col-sm-6 col-xs-12">
																		<div class="form-group">
																			<label class="control-label">City</label>
																			<input type="text" class="form-control" name="contact_fcity1" id="contact_fcity1">
																		</div>
																		</fieldset>
																		<fieldset class="col-md-3 col-sm-6 col-xs-12">
																		<div class="form-group">
																			<label class="control-label">State</label>
																			<input type="text" class="form-control" name="contact_fstate1" id="contact_fstate1">
																		</div>
																		</fieldset>
																		<fieldset class="col-md-3 col-sm-6 col-xs-12">
																		<div class="form-group">
																			<label class="control-label">Zip code</label>
																			<input type="text" class="form-control" name="contact_fzcode1" id="contact_fzcode1">
																		</div>
																		</fieldset>
																	</div>
															</div>
												</form>	
												
												
											</div>
											<div class="col-sm-12 col-md-12 col-xs-12 text-right">
													<div class="form-actions noborder nopad">
														<button type="button" class="btn default">Cancel</button>
														<input type="submit" class="btn blue" value="Save" id="cust-cont"/>
													</div>
													</div>
										</div>
										<div class="tab-pane" id="tab3">
											<div class="form-body">
												<div class="form-group col-sm-12 nopad">
													<label class="col-md-4 control-label">
													File Processing : <sup class="red-req">*</sup></label>
													<div class="col-md-8">
														<div class="input-group">
															<div class="icheck-list">
																<label>
																<input type="checkbox" class="icheck" data-checkbox="icheckbox_square-grey" id = "ach_id" required  /> ACH </label>
																<label>
																<input type="checkbox" class="icheck" data-checkbox="icheckbox_square-grey" id = "wire_id" required /> Wire 
																</label>
																<label>
																<input type="checkbox" class="icheck" data-checkbox="icheckbox_square-grey" id = "remi_id" required /> Remi Trac 
																</label>
																
															</div>
														</div>
													</div>
												</div>
												<div class="form-group col-sm-12 nopad">
													<label class="col-md-4 control-label">Data Entry : <sup class="red-req">*</sup></label>
													<div class="col-md-8">
														<div class="icheck-list">
															<label>
																<input type="radio" class="icheck" id="dataentry-check-yes" data-radio="iradio_square-grey" name="dataentry-check" required /> Yes 
															</label>
															<label>
																<input type="radio" id="dataentry-check-no" class="icheck" data-radio="iradio_square-grey" name="dataentry-check" required /> No 
															</label>
														</div>
													</div>
												</div>
												<div class="form-group col-sm-12 nopad">
													<label class="col-md-4 control-label">Exceptions : <sup class="red-req">*</sup></label>
													<div class="col-md-8">
														<div class="icheck-list">
															<label>
																<input type="radio" class="icheck" id="expections-check-yes" data-radio="iradio_square-grey" name="exception-check" /> Yes 
															</label>
															<label>
																<input type="radio" id="expections-check-no" class="icheck" data-radio="iradio_square-grey" name="exception-check" /> No 
															</label>
														</div>
													</div>
												</div>
												<div class="exception-true col-sm-12 nopad" style="display:none;">
													<div class="form-group">
														<label class="col-md-4 control-label">Validations :</label>
														<div class="col-md-8">
															<div class="icheck-list">
																<label>
																	<input type="radio" id="valid-check-yes" data-radio="iradio_square-grey" name="valid-check" /> Yes 
																</label>
																<label>
																	<input type="radio" id="valid-check-no"  data-radio="iradio_square-grey" name="valid-check" /> No 
																</label>
															</div>
														</div>
													</div>
													<div class="form-group">
														<label class="col-md-4 control-label">Reports :</label>
														<div class="col-md-8">
															<div class="icheck-list">
																<label>
																	<input type="radio" class="icheck" id="rep-check-yes" data-radio="iradio_square-grey" name="rep-check" /> Yes 
																</label>
																<label>
																	<input type="radio" id="rep-check-no" class="icheck" data-radio="iradio_square-grey" name="rep-check" /> No 
																</label>
															</div>
														</div>
													</div>
												</div>
												<div class="form-group col-sm-12 nopad">
													<label class="col-md-4 control-label">
													Transmission File Format : <sup class="red-req">*</sup></label>
													<div class="col-md-8">
														<div class="input-group">
															<div class="icheck-list">
																<label>
																<input type="checkbox" id="format-one" class="icheck" data-checkbox="icheckbox_square-grey"> Format 1 </label>
																<label>
																<input type="checkbox" id="format-two" class="icheck" data-checkbox="icheckbox_square-grey"> Format 2
																</label>
																<label>
																<input type="checkbox" id="format-three" class="icheck" data-checkbox="icheckbox_square-grey"> Format 3
																</label>
																
															</div>
														</div>
													</div>
												</div>
												<div class="form-group col-sm-12 nopad">
													<label class="col-md-4 control-label">Notification : <sup class="red-req">*</sup></label>
													<div class="col-md-8">
														<div class="icheck-list">
															<label>
																<input type="radio" class="icheck" id="note-check-yes" data-radio="iradio_square-grey" name="notifi-check" /> Yes 
															</label>
															<label>
																<input type="radio" id="note-check-no" class="icheck" data-radio="iradio_square-grey" name="notifi-check" /> No 
															</label>
														</div>
													</div>
												</div>
											</div>
											<div class="col-sm-12 col-md-12 col-xs-12 text-right">
											<div class="form-actions noborder nopad">
												<button type="button" class="btn default">Cancel</button>
												<input type="submit" class="btn blue" value="Save" />
											</div>
											</div>
										</div>
										<div class="tab-pane" id="tab4">
											<div class="col-md-12 col-sm-12 col-xs-12 nopad">
												<div class="medium-title">5 Record - Position : 05 to 25</div>
													<div class="col-md-12 col-sm-12 col-xs-12 nopad">
														<div class="table-scrollable table-scrollable-borderless managebank-cont custmap-tble">
														<table class="table table-hover table-bordered tble-custstup-5rec">
															<thead>
															<tr class="uppercase">
																<th>
																	 ACH Field
																</th>
																<th>
																	IR +  Field Match
																</th>
																<th> 
																	Check Digit
																</th>
																<th> 
																	Scheme
																</th>
																
																<th>
																	Options
																</th>
															</tr>
															</thead>
															<tbody>
															<tr>
																<td width="16.7%">
																	<input type="text" class="form-control" placeholder="Company name" id="compname" /> 
																</td>
																<td width="16.7%">
																	<select class="form-control select2">
																		<option value="">Company Name</option>
																		<option value="1">Bank of America</option>
																		<option value="2">HSBC</option>
																		<option value="3">BB1</option>
																		<option value="4">AT &amp; T</option>
																		<option value="5">NIC</option>
																	</select>
																</td>
																<td width="16.7%">
																	<div class="icheck-list">
																		<label>
																			<input type="radio" class="icheck" id="checkdig5rec-1" data-radio="iradio_square-grey" name="checkdig5rec-1" /> Yes 
																		</label>
																		<label>
																			<input type="radio" id="checkdig5rec-1" class="icheck" data-radio="iradio_square-grey" name="checkdig5rec-1" /> No 
																		</label>
																	</div>
																</td>
																<td width="16.7%">
																	<select class="form-control select2">
																		<option value="">7777</option>
																		<option value="1">Bank of America</option>
																		<option value="2">HSBC</option>
																		<option value="3">BB1</option>
																		<option value="4">AT &amp; T</option>
																		<option value="5">NIC</option>
																	</select>
																</td>
																
																<td width="16.7%">
																	<a class="btn btn-info add-custstup-5rec"><i class="fa fa-plus"></i> Add</a>
																</td>	
															</tr>
															</tbody>
														</table>
													</div>
												</div>
												<div class="medium-title">6 Record - Position : 05 to 50</div>
													<div class="col-md-12 col-sm-12 col-xs-12 nopad">
														<div class="table-scrollable table-scrollable-borderless managebank-cont custmap-tble">
														<table class="table table-hover table-bordered tble-custstup-6rec">
															<thead>
																<tr class="uppercase">
																	<th>
																		 ACH Field
																	</th>
																	<th>
																		IR +  Field Match
																	</th>
																	<th> 
																		Check Digit
																	</th>
																	<th> 
																		Scheme
																	</th>
																	<th>
																		Options
																	</th>
																</tr>
															</thead>
															<tbody>
															<tr>
																<td width="16.7%">
																	<input type="text" class="form-control" placeholder="Company name" /> 
																</td>
																<td width="16.7%">
																	<select class="form-control select2">
																		<option value="">Company Name</option>
																		<option value="1">Bank of America</option>
																		<option value="2">HSBC</option>
																		<option value="3">BB1</option>
																		<option value="4">AT &amp; T</option>
																		<option value="5">NIC</option>
																	</select>
																</td>
																<td width="16.7%">
																	<div class="icheck-list">
																		<label>
																			<input type="radio" class="icheck" id="checkdig6rec-1" data-radio="iradio_square-grey" name="checkdig6rec-1" /> Yes 
																		</label>
																		<label>
																			<input type="radio" id="checkdig6rec-1" class="icheck" data-radio="iradio_square-grey" name="checkdig6rec-1" /> No 
																		</label>
																	</div>
																</td>
																<td width="16.7%">
																	<select class="form-control select2">
																		<option value="">7777</option>
																		<option value="1">Bank of America</option>
																		<option value="2">HSBC</option>
																		<option value="3">BB1</option>
																		<option value="4">AT &amp; T</option>
																		<option value="5">NIC</option>
																	</select>
																</td>
																<td width="16.7%">
																	<a class="btn btn-info add-custstup-6rec"><i class="fa fa-plus"></i> Add</a>
																</td>	
															</tr>
															</tbody>
														</table>
													</div>
												</div>
											</div>
											<div class="col-sm-12 col-md-12 col-xs-12 text-right">
											<div class="form-actions noborder nopad">
												<button type="button" class="btn default">Cancel</button>
												<input type="submit" class="btn blue" value="Save" />
											</div>
											</div>
										</div>
									<div class="tab-pane" id="tab5">
										<div class="row">
													<div class="col-md-6 col-sm-6 col-xs-6">
														<label><b>Choose Report</b></label>
													</div>
													<div class="col-md-6 col-sm-6 col-xs-6">
														<label><b>Set Time</b></label>
													</div>
												</div>
												<div class="row">
													<div class="col-md-6 col-sm-6 col-xs-6">
													<select class="form-control select2" id="">
																<option value="">Select</option>
																<option value="1">Deposit Summary</option>
																<option value="2">Option 2</option>
																<option value="3">Option 3</option>
																<option value="4">Option 4</option>
															</select>
													</div>
													<div class="col-md-6 col-sm-6 col-xs-6">
														<div class="form-group">
															<input type="text" class="form-control timepicker" placeholder="Time" id="timepick" onBlur="javascript:t()">
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-6 col-sm-6 col-xs-6">
													<select class="form-control select2" id="">
														<option value="">Select</option>
														<option value="1">Transaction Summary</option>
														<option value="2">Option 2</option>
														<option value="3">Option 3</option>
														<option value="4">Option 4</option>
													</select>
													</div>
													<div class="col-md-6 col-sm-6 col-xs-6">
														<div class="form-group">
															<input type="text" class="form-control timepicker" placeholder="Time" id="">
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-6 col-sm-6 col-xs-6">
													<select class="form-control select2" id="">
														<option value="">Select</option>
														<option value="1">Account Summary</option>
														<option value="2">Option 2</option>
														<option value="3">Option 3</option>
														<option value="4">Option 4</option>
													</select>
													</div>
													<div class="col-md-6 col-sm-6 col-xs-6">
														<div class="form-group">
															<input type="text" class="form-control timepicker" placeholder="Time" id="">
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-6 col-sm-6 col-xs-6">
													<select class="form-control select2" id="">
														<option value="">Select</option>
														<option value="1">Option 1</option>
														<option value="2">Option 2</option>
														<option value="3">Option 3</option>
														<option value="4">Option 4</option>
													</select>
													</div>
													<div class="col-md-6 col-sm-6 col-xs-6">
														<div class="form-group">
															<input type="text" class="form-control timepicker" placeholder="Time" id="">
														</div>
													</div>
												</div>
												<div class="col-sm-12 col-md-12 col-xs-12 text-right">
													<div class="form-actions noborder nopad">
														<button type="button" class="btn default">Cancel</button>
														<input type="submit" class="btn blue" value="Save" id="cust-cont"/>
													</div>
													</div>
									</div>
										</div>
									</form>	
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
	<!-- END PAGE CONTENT -->
</div>

<!-- END PAGE CONTAINER -->
<!-- BEGIN FOOTER -->
<jsp:include page="includes/footer.jsp" />
<jsp:include page="includes/footer-js.jsp" />
<!-- END JAVASCRIPTS -->

<script src="../resources/assets/irplus/js/customerLogoUpload.js"></script>
<div class="modal fade" id="imgupload" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Choose a file to upload</h4>
        </div>
        <div class="modal-body">
			<form role="form" id="fileForm" class="form-horizontal" action="javascript: uploadCustomerLogo()">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<input id="bank-img-upload" name="customerLogo" type="file" class="file-loading">
				</div>
			</form>
        </div>
        <div class="modal-footer">
          
        </div>
      </div>
      
    </div>
  </div>


















<script type="text/javascript">

$('#valid-check-yes').iCheck(
{
	checkboxClass: 'iradio_square-grey',
	radioClass: 'iradio_square-grey',
});
$('#valid-check-no').iCheck(
{
	checkboxClass: 'iradio_square-grey',
	radioClass: 'iradio_square-grey',
});
$('.customer-setup .nav-tabs a[data-toggle="tab"]').on('shown.bs.tab', function (e) 
{
	var sum=0;
	var totaldiv = $(".customer-setup .nav").width();
	//alert(totaldiv);
	
	$(".customer-setup .nav>li").each(function(){
		sum=sum+$(this).width();
		if($(this).hasClass("active"))
		{
			return false;
		}
		
	});
	//alert(sum);
	var barwidth = parseFloat(sum / totaldiv * 100);
	//alert(barwidth);
	$("#bar-customer .progress-bar").css('width',barwidth+'%');
	
	
		//console.log("inview");

                
});
	var sum=0;
	var totaldiv = $(".customer-setup .nav").width();
	//alert(totaldiv);
	
	$(".customer-setup .nav>li").each(function(){
		sum=sum+$(this).width();
		if($(this).hasClass("active"))
		{
			return false;
		}
		
	});
	//alert(sum);
	var barwidth = parseFloat(sum / totaldiv * 100);
	//alert(barwidth);
	$("#bar-customer .progress-bar").css('width',barwidth+'%');
	
	$('#expections-check-yes').on('ifChecked', function(event)
	{
		//alert("a");
		$('.exception-true').show();
	});
	$('#expections-check-no').on('ifChecked', function(event)
	{
		//alert("b");
		$('.exception-true').hide();
	});
	
	
$('.customer-setup .nav-tabs a[data-toggle="tab"]').on('shown.bs.tab', function (e) 
{
	var sum=0;
	var totaldiv = $(".customer-setup .nav").width();
	//alert(totaldiv);
	
	$(".customer-setup .nav>li").each(function(){
		sum=sum+$(this).width();
		if($(this).hasClass("active"))
		{
			return false;
		}
		
	});
	//alert(sum);
	var barwidth = parseFloat(sum / totaldiv * 100);
	//alert(barwidth);
	$("#bar-customer .progress-bar").css('width',barwidth+'%');
});
	var sum=0;
	var totaldiv = $(".customer-setup .nav").width();
	//alert(totaldiv);
	
	$(".customer-setup .nav>li").each(function(){
		sum=sum+$(this).width();
		if($(this).hasClass("active"))
		{
			return false;
		}
		
	});
	//alert(sum);
	var barwidth = parseFloat(sum / totaldiv * 100);
	//alert(barwidth);
	$("#bar-customer .progress-bar").css('width',barwidth+'%');
	
	$('#expections-check').on('ifChecked', function(event)
	{
		$('.exception-true').show();
	});
	$('#expections-check').on('ifUnchecked', function(event)
	{
		$('.exception-true').hide();
	});
	
	// Customer information Storing = tables Customer ,Customer Contacts Saving
	
	var customerid = <%= request.getParameter("customerId") %>;
//update Customer functionality

$(function(){

	
/////nw fr banks 13 11 2017
function getAllBanks()
{
	
	//alert('in 1')
	//$(".select2").select2();
	
		$.ajax({
			
				method:'get',
				url:'../customer/show/banks/',
				contentType:'application/json',
				dataType:'JSON',
				crossDomain:'true',
				success:function(response)
				{
					htmlbnk = "";
					var htmlbnk= "<option value=''>Select Bank</option>";
					var bnkdet = JSON.stringify(response.parentBank.parentBank);
					obj = JSON.parse(bnkdet);
					
					/* obj = {
						"1" : "Name",
						"2": "Age",
						"3" : "Gender"
					}*/
					for(var key in obj) {
						htmlbnk += "<option value=" + key  + ">" +obj[key] + "</option>"
					}
					
					$("#bnk-name").html('');
					$("#bnk-name").html(htmlbnk);	
					
					var brnchDet = JSON.stringify(response.parentBank.bankBranchInfo);
					objBrnch = JSON.parse(brnchDet);
					htmlbrnch = "";
					
					var htmlbrnch= "<option value=''>Select Branch</option>";
					for (var j = 0; j < objBrnch.length; j++)
					{
						//if(objBrnch[j]['branchLocation'] != '')
						htmlbrnch += "<option value=" + objBrnch[j]['branchId']  + ">" + objBrnch[j]['branchLocation'] + "</option>"
					}
					
					//$("#bnk-name").html('');
					//$("#bnk-name").html(htmlbnk);	
											
					$("#bnk-loc").html('');
					$("#bnk-loc").html(htmlbrnch);
				
					/*$("#other_bank").html('');
					$("#other_bank").html(listBnkItems);*/
				},
				error:function(response,statusTxt,error){
					 
					/* 	alert("Failed to add record :"+response.responseJSON); */
					 }
			});
			
			
	}
	
	getAllBanks();
/////nw fr banks 13 11 2017
	
	if(customerid !=null){
		//update
				
		$.ajax({
		
			method:'get',
			url:'../customer/showOne/'+customerid,
			contentType:'application/json',
			dataType:'JSON',
			crossDomain:'true',
			success:function(response){

			var customerInfo = JSON.stringify(response.customerBean);
			//alert(customerInfo);
			
			$.each(JSON.parse(customerInfo), function(idx,obj) {
				
			//	alert(obj.parentBankId)
				//alert(obj.defaultBankName)
			$("#comp-id").val(obj.bankCustomerCode);
			$("#comp-name").val(obj.companyName);
			$("#dda-bank").val(obj.ddaBankAc);
			$("#comp-ach").val(obj.compIdentNo);
			
			$("#bnk-loc").val(obj.BankBranchId).trigger('change');
			$("#bnk-name").val(obj.parentBankId).trigger('change');
			
			$("#state-id").val(obj.state);
			$("#website-id").val(obj.website);
			$("#city-id").val(obj.city);			
			$("#zipcode-id").val(obj.zipcode);      
								
			});         

			
		//	var customerInfomation = JSON.stringify(response.customerBean.customerContactsBeans_array);
		//	alert("****************************+"+customerInfomation);
		
		var customerContacts_js = JSON.stringify(response.customerBean[0].customerContactsBeans_array);
	
	//	alert(customerContacts_js+"dddddddddddddddddddddddddd");
			
			var firstName ="";
			var lastName_t ="";
			var constaNo_t ="";
			var address1 ="";
			var address2 ="";
			var emailId ="";
		//$('.add-row-cust').click
		//alert(response.customerBean[0].customerContactsBeans_array.length)
		
		$.each(JSON.parse(customerContacts_js), function(idx, obj) {			
			//alert(obj.isdefault);					//isdefault
	/*		$("#fName-id").val(obj.firstName);		$("#lastName-id").val(obj.lastName);
			$("#contact-id").val(obj.contactNo);   	$("#email-id").val(obj.emailId);
			$("#address1-id").val(obj.address1);	$("#address2-id").val(obj.address2);	*/		
		});  
		
		var customerContacts_js1 = JSON.stringify(response.customerBean[0].customerContactsBeans_array);
		
		//alert(customerContacts_js1);
		
		var i= 1;
		
	//	alert(response.customerBean[0].customerContactsBeans_array.length)
		
		for (var j = 0; j < response.customerBean[0].customerContactsBeans_array.length; j++)
		{
				//alert('len='+response.customerBean[0].customerContactsBeans_array.length)
				///	alert(response.customerBean[0].customerContactsBeans_array[j].firstName+"  ****"+j);				
					
				if(response.customerBean[0].customerContactsBeans_array[j].isdefault)
				{					
				//	alert(response.customerBean[0].customerContactsBeans_array[j].isdefault+"  primary contact"+j);

					var contactsInfo= response.customerBean[0].customerContactsBeans_array[j];						
				
				//	alert(contactsInfo.firstName +" : primary");
					$("#fName-id").val(contactsInfo.firstName);
					$("#lastName-id").val(contactsInfo.lastName);
					$("#contact-id").val(contactsInfo.contactNo);			
					$("#email-id").val(contactsInfo.emailId);
					$("#address1-id").val(contactsInfo.address1);			
					$("#address2-id").val(contactsInfo.address2);
					
																					
				}else{
					
					
					var contactsInfo= response.customerBean[0].customerContactsBeans_array[j];	
					
				// Hi lincy..! below line  have to add the condition is defoult = false have to enter loop, 
				//array Contacts misorder come , if in array last one is primary key that time fails .	
					if(j<((response.customerBean[0].customerContactsBeans_array.length)-1))					
					$('.add-row-cust').trigger('click');
					//	alert(response.customerBean[0].customerContactsBeans_array[j].isdefault+"  **Arjun"+j);

					//	alert(contactsInfo.isdefault);
			
					$("#contact_crole"+j).val(contactsInfo.contactRole);
					$("#contact_fname"+j).val(contactsInfo.firstName);
					$("#contact_lname"+j).val(contactsInfo.lastName);
					$("#contact_fphoneno"+j).val(contactsInfo.contactNo);			
					$("#contact_femail"+j).val(contactsInfo.emailId);
					$("#contact_fstate"+j).val(contactsInfo.state);			
					$("#contact_fcity"+j).val(contactsInfo.city);
					$("#contact_fzcode"+j).val(contactsInfo.zipcode);
					$("#contact_faddr1"+j).val(contactsInfo.address1);
					$("#contact_faddr2"+j).val(contactsInfo.address1);
					//alert('in 1')
					//++i;
				}
			
			}							
			
			},
			error:function(response,statusTxt,error){
				 
					/* alert("Failed to add record :"+response.responseJSON); */
				 }
		});
		
	/*	
		$("#dv_save").hide();
		$("#dv_update").show();   */
		
	}
	 
});

function createCustomrContactsInfo(){
		

 //if(validfn())
//	{
				
	var createCustomerInfo ={
		
					//	customerId : customerid,
						userId : '2',
		
			//			irCustomerCode : $("#menu-modulepath").val(),
						companyName : $("#comp-name").val(),
						bankCustomerCode : $("#comp-id").val(),
						ddaBankAc : $("#dda-bank").val(),
						website : $("#website-id").val(),
						photofile:$("#bnk-bankLogo").val(),
			//			country : $("#menu-description").val(),						
						state : $("#state-id").val(),
						city : $("#city-id").val(),
						
						zipcode	: $("#zipcode-id").val(),
						isactive : '1',
						compIdentNo : $("#comp-ach").val(),
						BankBranchId : '2',
						firstName : $("#fName-id").val(),
						lastName : $("#lastName-id").val(),
						
					//	contactRole	: chk_menu_ismodule,
						emailId : $("#email-id").val(),
						contactNo : $("#contact-id").val(),						
						address1 : $("#address1-id").val(),
						address2 : $("#address2-id").val(),
					//	$("#zipcode-id").val(obj.zipcode);      
						isdefault : '1'
						
	}
			/* 	alert(JSON.stringify(createCustomerInfo)+"  update function" ); */
	
//	}
	
	$.ajax({
			method: 'post',
			url: '../customer/create',
			data: JSON.stringify(createCustomerInfo),
			contentType: 'application/json',
			dataType:'JSON',
			crossDomain:'true',
			success: function (response) {
				
				/* 	alert("Created Data data"); */
			//		location.href='manage-customer.jsp';													  
			},

		 error:function(response,statusTxt,error){
		 
		/* 	alert("Failed to add record :"+response.responseJSON);
		 */
			for(i =0; i<response.responseJSON.errMsgs.length ; i++){
			/* alert("Failed to add record :"+response.responseJSON.errMsgs[i]);	 */	
			}
		 }
		});
		
}

function t()
{
	/* alert($('#timepick').val()); */
}

 function updateCustomerInfo(){

 if(validfn())
	{
				
	var update_CustomerInfo ={
		
						customerId : customerid,
						userId : '2',
		
					//	irCustomerCode : $("#menu-modulepath").val(),
						companyName : $("#comp-name").val(),
						bankCustomerCode : $("#comp-id").val(),
						ddaBankAc : $("#dda-bank").val(),
						website : $("#website-id").val(),
			//			country : $("#menu-description").val(),						
						state : $("#state-id").val(),
						city : $("#city-id").val(),
						
						zipcode	: $("#zipcode-id").val(),
						isactive : '1',
						compIdentNo : $("#comp-ach").val(),
						BankBranchId : $("#bnk-loc").val(),
						firstName : $("#fName-id").val(),
						lastName : $("#lastName-id").val(),
						
					//	contactRole	: chk_menu_ismodule,
						emailId : $("#email-id").val(),
						contactNo : $("#contact-id").val(),						
						address1 : $("#address1-id").val(),
						address2 : $("#address2-id").val(),
					//	$("#zipcode-id").val(obj.zipcode);      
						isdefault : '1'						
	}
				/* alert(JSON.stringify(update_CustomerInfo)+"  update function" ); */
	
	}
	
	$.ajax({
			method: 'post',
			url: '../cutomer/update',
			data: JSON.stringify(update_CustomerInfo),
			contentType: 'application/json',
			dataType:'JSON',
			crossDomain:'true',
			success: function (response) {
				
					/* alert("Updated data"); */
			//		location.href='manage-customer.jsp';													  
			},

		 error:function(response,statusTxt,error){
		 
	/* 		alert("Failed to add record :"+response.responseJSON); */
		
			for(i =0; i<response.responseJSON.errMsgs.length ; i++){
		/* 	alert("Failed to add record :"+response.responseJSON.errMsgs[i]);		 */
			}
		 }
		});
}


// customer functions getOnecustomer = Pop one Customer
/*
$(function(){
	
	alert(" Hi dude");
	alert(" Hi dude");
		if(customerid !=null){}
	
	$.ajax({
		
			method:'get',
			url:'../customer/showOne/'+customerid,
			
			contentType:'application/json',
			
			dataType:'JSON',
			crossDomain:'true',
			success:function(response){

			var customerInfo = JSON.stringify(response.customerBean);
			//alert(customerInfo);
			
					$.each(JSON.parse(customerInfo), function(idx, obj) {		
					
						
					$("#expections-check-yes").val(obj.bankCustomerCode);
					$("#expections-check-no").val(obj.companyName);
					
					$("#valid-check-no").val(obj.ddaBankAc);
					$("#valid-check-yes").val(obj.compIdentNo);													
					
					$("#format-three").val(obj.website);
					$("#format-one").val(obj.city);			
					$("#format-two").val(obj.zipcode);  
					
					$("#rep-check-yes).val(obj.BankBranchId).trigger('change');
					$("#rep-check-no").val(obj.parentBankId).trigger('change');			

					$("#note-check-yes").val(obj.state);					
					$("#note-check-no").val(obj.state);	
					
					});  
				
				}
			});	
	
		}
});
*/
//show one Customer close	
</script>
</body>
<!-- END BODY -->
</html>