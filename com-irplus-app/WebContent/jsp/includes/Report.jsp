<head>
<meta charset="utf-8"/>
<title>Image Scan</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description"/>
<meta content="" name="author"/>


<style>
table {
    width: 47em;
  margin-left:-3px;
}

.dyns-height{
    width:80px;
    max-height:240px;
    overflow-y:auto;
}
.dyn-height{
    width:100px;
    max-height:259px;     
    overflow-y:auto;
}
.grptble-height{
 		width:100px;
        max-height:364px;
    overflow-y:auto;
}
td, th {
    border: 1px solid #dddddd;
    text-align: left;
   
    
    padding: 0.3em;
}

tr:nth-child(even) {
    background-color: #dddddd;
}

.transaction-single{
border:1px solid #89c4f44a;
}
.ftablehead {
    background: #0f75bc;
    color: #fff;
}
.stablehead {
    background: #0f75bc82;
    color: #fff;
}
.datevalue{
hover color:#0f75bc82;
}

div.pager {
    text-align: left;
    margin: 1em 0;
}

div.pager span {
    display: inline-block;
    width: 1.8em;
    height: 1.8em;
    line-height: 1.8;
    text-align: center;
    cursor: pointer;
    background: #000;
    color: #fff;
    margin-right: 0.5em;
}

div.pager span.active {
    background: #c00;
}
.portlet {
  width: 1286px;
    margin-left: -17px;
    }
    
.processtitle{
    margin-left: -51px;
    }
    
  .processtitle label{
    color:#0f75bcab; font-size:18px;
    }
    
      .processtitle label strong{
  font-weight: 600;font-size: 18px;
    }
 .processDetails{
      margin-left: -22px;
   }
   .myClass{
   cursor:pointer;
   }
 .Summary{
  margin-top: 5px;
  width: 430px;
  margin-left: 4px;
  background-color: #f1f1f170;
  height: 136px;
 }  
 .BankTable {
width: 642px;
display:block;
margin-left: -32px;
 }
 
 .GroupTable {

 width: 1264px; 
display:block;
margin-left: -32px;
 }

 .GroupTable table thead tr th {
 font-size: 10px;
 }
  .GroupTable table tbody {
 background-color: #80808038;
 }


.innerCustomerTable table thead tr th {
 font-size: 9px;
 }
 
 .BankTable table thead tr th {
 font-size: 11px;
 }
  .BankTable table tbody {
 background-color: #80808038;
 }

 .DateTable table thead tr th {
 font-size: 11px;
 }

   .customerTable {
  margin-top: 6px;
    width: 611px;
    margin-left: 3px;
    height: 214px;
    }
    .customerTable table
    {
    margin-top: -19px;
    margin-left: -19px;
    }
    .customerTable table thead tr {
    font-size: 12px;
  
    }
     .customerTable table tbody tr {
    hover color:#0f75bc82;
    cursor:pointer;
    }
    
    .customerTable table tbody tr:hover {
   background-color:#fefefe;
    color:#0f75bcab;
    }
    
   .bankCount{
   font-family: inherit; margin-left: -22px;font-size: 18px;color: #808080ad;font-weight: 700;
   } 
   
    .bankCounts{
   font-family: inherit; margin-left: -22px;font-size: 18px;color: #808080ad;font-weight: 700;
   }
     
   .Totalbanks{
   font-family: inherit; font-size: 16px;color: #808080ad;font-weight: 700;
   }    
   .Date{
   margin-right: 10px; font-style: inherit; width: 79px;
   }
   .TotalFiles{
   font-size: 17px;color: #89c4f4;font-weight: 700;
   }
   
    .TotalFileserror{
   font-size: 17px;color: #89c4f4;font-weight: 700;
   }
   
   
   
   
    .procSummary{margin-top: -6px;margin-left: -9px;color:#0f75bcab;font-size:17px;padding-top: -17px;}
    .procSummaryHeading{ color:#0f75bcab;font-size: 14px;margin-left: -38px;margin-top: 10px;}
  	.Item{color:#0f75bcab;font-size: 14px;margin-left: -27px;margin-top: 10px;}
  	.Amount{ color:#0f75bcab;font-size: 14px;margin-left: -29px;margin-top: 10px;}
  	</style>
  	</head>