<!DOCTYPE html>
<html lang="en">
<jsp:include page="includes/style.jsp"/>
<body class="page-md">
<!-- BEGIN HEADER -->
	<div class="page-header">
		<!-- BEGIN HEADER TOP -->
		<jsp:include page="includes/header-top.jsp" />
		<!-- END HEADER TOP -->
		<!-- BEGIN HEADER MENU -->
		<jsp:include page="includes/header-menu.jsp" />
		<!-- END HEADER MENU -->
	</div>
<!-- END HEADER -->
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<div class="container">
			<!-- BEGIN PAGE TITLE -->
			<div class="col-md-6 col-sm-6 col-xs-12 page-title">
				<h1>Create menu</h1>
			</div>
			<div class="col-md-6 col-sm-6 col-xs-12 text-right">
							<a href="manage-menu.jsp" class="btn darkgrey m-t-15">
								<i class="fa fa-reply"></i> 
							Back 
						</a>
					</div>
			<!-- END PAGE TITLE -->
			
		</div>
	</div>
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container">
			<!-- BEGIN PAGE BREADCRUMB -->
			
				<!--<div class="col-md-12 col-sm-12 col-xs-12 form-group nopad">
			<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
			<ul class="page-breadcrumb breadcrumb">
				<li>
					<a href="#">Home</a><i class="fa fa-circle"></i>
				</li>
				
				<li class="active">
					 Create menu
				</li>
			</ul>
			</div>
			<div class="col-md-6 col-sm-6 col-xs-12 text-right">
							<a href="manage-menu.jsp" class="btn blue">
								<i class="fa fa-reply"></i> 
							Back 
						</a>
					</div>
			</div>
			</div>-->
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row margin-top-10">
				<div class="col-md-12">
					<!-- BEGIN PROFILE CONTENT -->
					<div class="profile-content">
						<div class="row">
							<div class="col-md-12">
				
					
					<div class="portlet light col-md-12 col-sm-12 col-xs-12">
						<div class="portlet-body">
							<div class="col-md-10 col-md-offset-1">
							<form class="form-horizontal " method="post" role="form" id="jvalidate">
								<div class="form-body">
															
									<input type="hidden" class="form-control" id="menu-id" name="menu-id" />
								<!--<input type="hidden" class="form-control" id="menu-ismodule" name="menu-ismodule" />
									<input type="hidden" class="form-control" id="menu-menuicon" name="menu-menuicon" /> 
									<input type="hidden" class="form-control" id="menu-createddate" name="menu-createddate"/>
									<input type="hidden" class="form-control" id="menu-userid" name="menu-userid" />   -->
															
									<div class="form-group">
										<label class="col-md-4 control-label">
										Menu Name : <sup class="red-req">*</sup></label>
										<div class="col-md-8">
											<input type="text" class="form-control" name="menu-name" id="menu-name" required />
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-md-4 control-label">
										Module Path : <sup class="red-req">*</sup></label>
										<div class="col-md-8">
											<input type="text" class="form-control" name="menu-modulepath" id="menu-modulepath" required />
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-md-4 control-label">
										Menu Icon : </label>
										<div class="col-md-8">
											<input type="text" class="form-control" name="menu-icon" id="menu-icon" />
										</div>
									</div>

									<div class="form-group">
										<label class="col-md-4 control-label">
										Description :</label>
										<div class="col-md-8">
											<textarea class="form-control" name="menu-description" id="menu-description"  placeholder=""></textarea>
										</div>
									</div>

									<div class="form-group">
										<label class="col-md-4 control-label">
										Sorting order : <sup class="red-req">*</sup></label>
										<div class="col-md-8">
											<input type="text" name="menu-sortingorder" id="menu-sortingorder" class="form-control" placeholder="" required />
										</div>
									</div>
			
									<div class="form-group">
										<label class="col-md-4 control-label">
											This is module
										</label>
										<div class="col-md-8">
											<label class="menu-checkbox">
												<input type="checkbox" class="icheck cls_status" id="menu-ismodule" data-checkbox="icheckbox_square-grey">  
											</label>
										</div>
									</div>

									<div class="form-group">
										<label class="col-md-4 control-label">
											Status
										</label>
										<div class="col-md-8">
											<label class="menu-checkbox">
												<input type="checkbox"  id="menu-status" class="icheck cls_status" data-checkbox="icheckbox_square-grey">  
											</label>
										</div>
									</div>
									<div class="col-md-8 col-md-offset-4 col-sm-8 col-sm-offset-4  col-xs-12">
										<div class="form-actions noborder">
																				
											<input id="dv_save" type="button" class="btn blue" value="Submit" onClick="menuCreate();"/>
											
											
											<input id="dv_update" style="display:none;" type="button" class="btn blue" value="Update" onClick="menuUpdate();"/>
											
											<button type="button" onClick="cancelMenu()" class="btn default">Cancel</button>		

										</div>
									</div>
								</div>
							</form>
						</div>
							
						</div>
					</div>
					<!-- END SAMPLE FORM PORTLET-->					
				</div>
							
						</div>
						
					</div>
					<!-- END PROFILE CONTENT -->
				</div>
			</div>
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER -->
<!-- BEGIN FOOTER -->
<jsp:include page="includes/footer.jsp" />
<jsp:include page="includes/footer-js.jsp" />

<script>

var menuid = <%= request.getParameter("menuid") %>;
var userId = <%= session.getAttribute("userid") %>;


//update menu functionality

// show menu based on id



$(function(){
	//alert(user_id);
	if(menuid !=null){
		//update
		
		$.ajax({
		
			method:'get',
			url:'../menu/show/'+menuid,
			contentType:'application/json',
			dataType:'JSON',
			crossDomain:'true',
			success:function(response){

			var menuInfo = JSON.stringify(response.menus);
			//alert(menuInfo);
			
			$.each(JSON.parse(menuInfo), function(idx, obj) {
				
			$("#menu-id").val(obj.menuid);
			$("#menu-name").val(obj.menuName);
			$("#menu-modulepath").val(obj.modulepath);
			$("#menu-sortingorder").val(obj.sortingorder);
			$("#menu-ismodule").val(obj.ismodule);
			$("#menu-createddate").val(obj.createddate);
			$("#menu-modifieddate").val(obj.modifieddate);
			$("#menu-icon").val(obj.menuicon);
			$("#menu-description").val(obj.description);
					
			if(obj.status == "1"){			
				$(".cls_status").prop('checked', 'checked');
				$(".cls_status").parent('div').addClass('checked');
			}else{
				$(".cls_status").prop('checked', false);
				$(".cls_status").parent('div').removeClass('checked');  
			}			
			
			$("#menu-userid").val(obj.userid);
			});
			
			},
			error:function(response,statusTxt,error){
				 
				/* 	alert("Failed to add record :"+response.responseJSON); */
				 }
		});
		
		$("#dv_save").hide();
		$("#dv_update").show();
		
	}
	 
});

//updated
function menuUpdate(){
	
	if($("#jvalidate").valid())
	{
			var chk_menu_status =0;
			var chk_menu_ismodule ="false";
			if($("#menu-status").is(':checked')){
				chk_menu_status=1;
			}
			if($("#menu-ismodule").is(':checked')){
				chk_menu_ismodule="true";
			}	

	var menuInfo ={
						menuid : $("#menu-id").val(),
						menuName : $("#menu-name").val(),
						modulepath : $("#menu-modulepath").val(),
						sortingorder : $("#menu-sortingorder").val(),
						ismodule	: chk_menu_ismodule,
					//	createddate : $("#menu-createddate").val(),
						menuicon : $("#menu-icon").val(),
						description : $("#menu-description").val(),						
						status : chk_menu_status,
						userid :userId
	}
				//alert(JSON.stringify(menuInfo));
	
	$.ajax({
			method: 'post',
			url: '../menu/update',
			data: JSON.stringify(menuInfo),
			contentType: 'application/json',
			dataType:'JSON',
			crossDomain:'true',
			success: function (response) {
				 swal({title: "Done",text: "Menu Updated",type: "success"},function(){window.location = "manage-menu.jsp";});
					//alert("Updated MENU");
					//location.href='manage-menu.jsp';
													   
			},

		 error:function(response,statusTxt,error){
		 
			//alert("Null error"+response.responseJSON);
			//alert("Failed to update record "+statusTxt);
		 }
		});
		}
	
}

//create menu functionality

	function menuCreate(){
		
	//	alert("inside :");//+validfn());
		if($("#jvalidate").valid())
		{
			//alert("defd");
			var chk_menu_status =0;
			var chk_menu_ismodule ="false";
			if($("#menu-status").is(':checked')){
				chk_menu_status=1;
			}
			if($("#menu-ismodule").is(':checked')){
				chk_menu_ismodule="true";
			}
		

			//alert("status : "+$("#menu-status").val());
			//alert("ismodule	: "+$("#menu-ismodule").val());
			
			var menusInfo ={
								menuName : $("#menu-name").val(),
								modulepath : $("#menu-modulepath").val(),
								sortingorder : $("#menu-sortingorder").val(),
							 	ismodule	: chk_menu_ismodule,
								menuicon : $("#menu-icon").val(),
								description : $("#menu-description").val(),
								status : chk_menu_status,					
								//userid : '10'
								userid :userId
			}

					$.ajax({
					method: 'post',
					url: '../menu/add',
					data: JSON.stringify(menusInfo),
					contentType: 'application/json',
					dataType: 'JSON',
					crossDomain: 'true',
					success: function (response) {
						 swal({title: "Done",text: "Menu Created",type: "success"},function(){window.location = "manage-menu.jsp";});
						//	alert("Added MENU");
						//	location.href='manage-menu.jsp';
					},

				 error:function(response,statusTxt,error){
				//	alert("Duplicate Menu");
			//	alert("Failed to add record :"+statusTxt);
			//	alert("Failed to add record :"+error);
				/* alert("Failed to add record :"+response.responseJSON.errMsgs[0]); */
					//location.href='manage-menu.jsp';
					
				 }
				});
			}
				
		}

	function cancelMenu(){
		
	//	alert("Cancelled Page");
		location.href='manage-menu.jsp';
	}
	

</script>

</body>
<!-- END BODY -->
</html>