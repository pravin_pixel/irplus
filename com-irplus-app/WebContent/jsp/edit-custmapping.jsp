<!DOCTYPE html>
<html lang="en">
<jsp:include page="includes/style.jsp"/>
<body class="page-md">
<!-- BEGIN HEADER -->
	<div class="page-header">
		<!-- BEGIN HEADER TOP -->
		<jsp:include page="includes/header-top.jsp" />
		<!-- END HEADER TOP -->
		<!-- BEGIN HEADER MENU -->
		<jsp:include page="includes/header-menu.jsp" />
		<!-- END HEADER MENU -->
	</div>
<!-- END HEADER -->
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<div class="container">
			<!-- BEGIN PAGE TITLE -->
			<div class="col-md-6 col-sm-6 col-xs-12  page-title">
				<h1> Edit  Customer Mapping</h1>
			</div>
				<div class="col-md-6 col-sm-6 col-xs-12 text-right">
							<a href="manage-custmapping.jsp" class="btn blue">
								<i class="fa fa-reply"></i> 
							Back 
						</a>
					</div>
			<!-- END PAGE TITLE -->
			
		</div>
	</div>
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container">
			
			<!-- BEGIN PAGE BREADCRUMB -->
			<!--<div class="col-md-12 col-sm-12 col-xs-12 form-group nopad">
				<div class="row">
					<div class="col-md-6 col-sm-6 col-xs-12">
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<a href="#">Home</a><i class="fa fa-circle"></i>
						</li>
						
						<li class="active">
							 Edit Customer Mapping
						</li>
					</ul>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12 text-right">
							<a href="manage-custmapping.jsp" class="btn blue">
								<i class="fa fa-reply"></i> 
							Back 
						</a>
					</div>
				</div>
			</div>-->
			
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row margin-top-10">
				<div class="col-md-12">
					
					<!-- BEGIN PROFILE CONTENT -->
					<div class="profile-content">
						<div class="row">
							<div class="col-md-12">
				
					
					<div class="portlet light col-md-12 col-sm-12 col-xs-12">
						<!-- <div class="portlet-body">
							<form role="form" class="form-horizontal">
								<div class="col-md-12 col-sm-12 col-xs-12 nopad">
								<div class="table-scrollable table-scrollable-borderless managebank-cont">
								<table class="table table-hover table-light table-bordered">
								<thead>
								<tr class="uppercase">
									<th >
										 Bank Name
									</th>
									<th>
										Customer ID
									</th>
									
									<th class="text-center"> Add/delete</th>
								</tr>
								</thead>
								<tbody>
								 
									<tr>
									
									<td>
										<select class="form-control select2" id="form_control_1">
											<option value="">select</option>
											<option value="1">Option 1</option>
											<option value="2">Option 2</option>
											<option value="3">Option 3</option>
											<option value="4">Option 4</option>
										</select> 
									</td>
									<td>
										<select class="form-control select2" id="form_control_1">
											<option value="">select</option>
											<option value="1">Option 1</option>
											<option value="2">Option 2</option>
											<option value="3">Option 3</option>
											<option value="4">Option 4</option>
										</select> 
									</td>
									<td class="text-center">
										<a class="btn btn-xs green" data-toggle="tooltip" data-placement="right" title="Add!">
											<i class="fa fa-plus"></i>
										</a>
										<a class="btn btn-xs red" data-toggle="tooltip" data-placement="right" title="Delete">
											<i class="fa fa-trash"></i>
										</a>
									</td>
								</tr>
								<tr>
									
									<td>
										<select class="form-control select2" id="form_control_1">
											<option value="">select</option>
											<option value="1">Option 1</option>
											<option value="2">Option 2</option>
											<option value="3">Option 3</option>
											<option value="4">Option 4</option>
										</select> 
									</td>
									<td>
										<select class="form-control select2" id="form_control_1">
											<option value="">select</option>
											<option value="1">Option 1</option>
											<option value="2">Option 2</option>
											<option value="3">Option 3</option>
											<option value="4">Option 4</option>
										</select> 
									</td>
									<td class="text-center">
										<a class="btn btn-xs green" data-toggle="tooltip" data-placement="right" title="Add!">
											<i class="fa fa-plus"></i>
										</a>
										<a class="btn btn-xs red" data-toggle="tooltip" data-placement="right" title="Delete">
											<i class="fa fa-trash"></i>
										</a>
									</td>
								</tr>
								<tr>
									
									<td>
										<select class="form-control select2" id="form_control_1">
											<option value="">select</option>
											<option value="1">Option 1</option>
											<option value="2">Option 2</option>
											<option value="3">Option 3</option>
											<option value="4">Option 4</option>
										</select> 
									</td>
									<td>
										<select class="form-control select2" id="form_control_1">
											<option value="">select</option>
											<option value="1">Option 1</option>
											<option value="2">Option 2</option>
											<option value="3">Option 3</option>
											<option value="4">Option 4</option>
										</select> 
									</td>
									<td class="text-center">
										<a class="btn btn-xs green" data-toggle="tooltip" data-placement="right" title="Add!">
											<i class="fa fa-plus"></i>
										</a>
										<a class="btn btn-xs red" data-toggle="tooltip" data-placement="right" title="Delete">
											<i class="fa fa-trash"></i>
										</a>
									</td>
								</tr>
								
									
								
								
								</tbody>
								</table>
							</div>
								</div>
									
								<div class="col-md-12 col-sm-12 col-xs-12 nopad">
								<div class="form-actions noborder text-right">
									<button type="button" class="btn default">Cancel</button>
									<button type="button" class="btn blue">Create</button>
								</div>
								</div>
							</form>
							
						</div> -->
					
					<div class="portlet-body">
			<form role="form" class="form-horizontal" name="submit_form" id="submit_form" method="POST">
										
								<div class="col-md-12 col-sm-12 col-xs-12 nopad">
								<div class="table-scrollable table-scrollable-borderless managebank-cont custmap-tble">
								<table class="table table-hover table-bordered tble-custmap">
								<thead>
								<tr class="uppercase">
									<th>
										Reference Data
									</th>
									<th>
										 NATCHA Fields
										
									</th>
									<th> 
										Bank
									</th>
									<th> 
										Customer ID
									</th>
									<th>
										Options
									</th>
								</tr>
								</thead>
								<tbody>
								
							</tbody>
								</table>
							</div>
								</div>
									
								<div class="col-md-12 col-sm-12 col-xs-12 nopad">
								<div class="form-actions noborder text-right">
					

 	 <button type="button"  onclick="cancelMapping()" class="btn default" >Cancel</button>
 	<a href="javascript:submitUpdateCustMappingForm()" class="btn blue button-submit">Update</a>
									
								</div>
								</div>
							</form>
							
						</div>
					
					</div>
					<!-- END SAMPLE FORM PORTLET-->
					
				</div>
							
						</div>
						
					</div>
					<!-- END PROFILE CONTENT -->
				</div>
			</div>
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>
<script type="text/javascript">
var customerMapId = <%= request.getParameter("customerMapId") %>;
var userId=<%=session.getAttribute("userid")%>;
var isactive="2";
	$(document).ready(function() {  
		
		 $.ajax({
				method: 'get',
				url: '../customerMapping/get/'+customerMapId,
				contentType: 'application/json',
				dataType:'JSON',
				crossDomain:'true',
				success: function (response) {
					
		 					for(g=0;g<response.customerMpngInfoList.length;g++){
		 				
		 						var newDivgroup = $('<tr class="newdiv-'+g+'"><td width="20%"><input type="text" class="form-control reference_data'+g+'"  id='+g+' name="reference_data'+g+'"/></td><td width="20%"><select class="form-control select2 natchafield'+g+'"  id='+g+' name="natchafield'+g+'"></select></td><td width="20%"><select class="form-control select2 branchname'+g+'"  id='+g+' name="branchname'+g+'" onchange="changeFunc.call(this);"></select></td><td width="20%"><select class="form-control select2 customername'+g+'"  id='+g+' name="customername'+g+'"></select></td><td width="20%"><a class="btn btn-info deltebnk" onclick="deltecustbnk('+g+');"><i class="fa fa-trash"></i> Delete</a></td></tr>');
		 						$('table.tble-custmap tbody').append(newDivgroup);
		 							 						
		 					}
		 		 m=response.customerMpngInfoList.length;
						var newDivgroups = $('<tr class="newdiv-'+m+'"><td width="20%"><input type="text" class="form-control reference_data'+m+'"  id='+m+' name="reference_data'+m+'"/></td><td width="20%"><select class="form-control select2 natchafield'+m+'"  id='+m+' name="natchafield'+m+'"><option value="" selected disabled>select</option><option value="1">Receiving DFI ID</option><option value="2">DFI Account ID</option><option value="3">Individual Account ID</option><option value="4">Individual Account Name</option><option value="5">Trace No</option></select></td><td width="20%"><select class="form-control select2 branchname'+m+'"  id='+m+' name="branchname'+m+'" onchange="changeMappingBranchfun.call(this);"></select></td><td width="20%"><select class="form-control select2 customername'+m+'"  id='+m+' name="customername'+m+'"></select></td><td width="20%"><a class="btn btn-info " onclick="addeditcustmapping();"><i class="fa fa-plus"></i> Add</a><a class="btn btn-info deltebnk" onclick="deletecustMapping('+g+');"><i class="fa fa-trash"></i> Delete</a></td></tr>');
			 		$('table.tble-custmap tbody').append(newDivgroups); 

			 		getappendbranch(m);
			 		
				for(i=0;i<response.customerMpngInfoList.length;i++){

		 				 		var branch="<OPTION VALUE="+response.customerMpngInfoList[i].bankbranchid+">"+response.customerMpngInfoList[i].branchName+"</OPTION>"; 
		 				 				
		 				$(".branchname"+i).html(branch);	

					}
					
				for(i=0;i<response.customerMpngInfoList.length;i++){

	 				 		var customer="<OPTION VALUE="+response.customerMpngInfoList[i].customersId+">"+response.customerMpngInfoList[i].companyName+"</OPTION>"; 
	 				 				
	 				$(".customername"+i).html(customer);	

				} 
				for(i=0;i<response.customerMpngInfoList.length;i++){

				 		var natchafld="<OPTION VALUE="+response.customerMpngInfoList[i].natchafld+">"+response.customerMpngInfoList[i].natchafieldName+"</OPTION>"; 
				 				
				$(".natchafield"+i).html(natchafld);	

		} 
				
				$(".reference_data0").val(response.customerMpngInfoList[0].referenceData);	
				},

			 error:function(response,statusTxt,error){
			 
				/* alert("Failed to add record :"+response.responseJSON); */
			 }
				
				
			});
		
			
			function getappendbranch(m)
			{
			
				var row=m;
			
			$.ajax({
				
				method:'get',
				
				
				url:'../Customermapping/branch/'+siteId,
				contentType:'application/json',
				dataType:'JSON',
				crossDomain:'true',
				success:function(response)
{
				
	 				var htmlbankid="";
	 				var htmlbankname= "<option value='' selected disabled>Select Bank</option>";
	 				var branch =response.dashboardResponse.bankInfo.bankbranchinfo;
	 				for(var i=0;i<response.dashboardResponse.bankInfo.length;i++){
	 					
	 					
	 		
	 					htmlbankname +="<OPTGROUP LABEL="+response.dashboardResponse.bankInfo[i].bankName+">";   
	 				
	 					for(var j=0;j<response.dashboardResponse.bankInfo[i].bankbranchinfo.length;j++){
	 				 		htmlbankname +="<OPTION VALUE="+response.dashboardResponse.bankInfo[i].bankbranchinfo[j].branchId+">"+response.dashboardResponse.bankInfo[i].bankbranchinfo[j].branchLocation+"</OPTION>"; 
	 					
	 				 	
	 					
	 					}
	 					

	 					$(".branchname"+row).html(htmlbankname);	
	 			
	 				}
	 	
		},
		error:function(response,statusTxt,error){
			 
		/* 	alert("Failed to add record :"+response.responseJSON); */
		 }
		});		
			}
		
		
	});
	
	
	  

	function appendmappingchangeFunc() {
		
		  var rowid  = $(this).attr("id");
		 
		      var branchId  = $(".branchname"+rowid).val();
		  
		      $.ajax({
		        	 method: 'get',
		             url: '../customer/getcustomer/'+branchId,
		             contentType: 'application/json',
					 dataType:'JSON',
					 crossDomain:'true',
		             success: function(response) {
		               
		             
		
		                	var appendcustomername= "<option value='' selected disabled>Select Customer</option>";
							
							for (var i = 0; i < response.bankBranches.bankCustomerList.length; i++){
							
							  appendcustomername += "<option value='" + response.bankBranches.bankCustomerList[i].customerid + "'>" + response.bankBranches.bankCustomerList[i].companyName + "</option>";
							}
			
							$(".customername"+rowid).html(appendcustomername);
			
		                	
		             },
		             error: function (e) {
		                
		                 console.log(e.message);
		             /*     alert("failed to get bank customer"); */
		             }
		         });
	}
function cancelMapping(){
 	
 	//alert("Cancelled Page");
 	location.href='manage-custmapping.jsp';
 }
 
 
function submitUpdateCustMappingForm()
{
	InactivateGroup(customerMapId,isactive);
	var numItems = $('.tble-custmap tbody tr').size();
	

	var url_data = ($("#submit_form").serialize());//.replace(/+/g,'%20');//replace(/ /g, '+');
	
	var result = queryStringToJSON(url_data);

	var Editcustdetails = [];

	 for(var k=0;k<numItems;k++)
		{
		   
		 Editcustdetails[k]=
			{
				 natchafld:result["natchafield"+k],
				 referenceData:result["reference_data"+k],
			
				 bankbranchid:result["branchname"+k],
				 customersId:result["customername"+k],
				 usersid:userId,
					 isactive:'1'
					
			}
		}		
	
		
$.ajax({
		method:'post',
		url:'../customermapping/update/customer',
		data:JSON.stringify(Editcustdetails),
		contentType:'application/json',
		dataType:'JSON',
		crossDomain:'true',
		success: function(response) 
		{
			swal({title:"Done",text:"Updated Successfully",type:"success"},function(){window.location="manage-custmapping.jsp";});
		},
		
	 error:function(response,statusTxt,error){
	 
		/* alert("Failed to add record :"+response.responseJSON.errMsgs); */
	 }
}); 
	
	
}

function InactivateGroup(customerMapId,isactive)
{
	

		 
	var groupInfo = {
			
			customerMapId:customerMapId,
				isactive:isactive,
				
		}
	
			$.ajax({
				method: 'post',
				url: '../Customer/Mapping/delete',
				data: JSON.stringify(groupInfo),
				contentType: 'application/json',
				dataType:'JSON',
				crossDomain:'true',
				success: function (response) {
						// swal('Done!','Added','success'),function()
						// {
							 // window.location = 'manage-bank.jsp';
						// }
						//location.href='manage-bank.jsp';
				},

			 error:function(response,statusTxt,error){
			 
				/* alert("Failed to add record :"+response.responseJSON); */
			 }
			});
	

}



var queryStringToJSON = function (url) {
    if (url === '')
        return '';
    var pairs = (url || location.search).slice(1).split('&');
    var result = {};
    for (var idx in pairs) {
        var pair = pairs[idx].split('=');
        if (!!pair[0])
            result[pair[0].toLowerCase()] = decodeURIComponent(pair[1] || '');
    }
    return result;
}
</script>
<!-- END PAGE CONTAINER -->
<!-- BEGIN FOOTER -->
<jsp:include page="includes/footer.jsp" />
<jsp:include page="includes/footer-js.jsp" />
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>