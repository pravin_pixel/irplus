<!DOCTYPE html>
<html lang="en">
<jsp:include page="includes/style.jsp"/>
<body class="page-md">
<!-- BEGIN HEADER -->
	<div class="page-header">
		<!-- BEGIN HEADER TOP -->
		<jsp:include page="includes/header-top.jsp" />
		<!-- END HEADER TOP -->
		<!-- BEGIN HEADER MENU -->
		<jsp:include page="includes/header-menu.jsp" />
		<!-- END HEADER MENU -->
	</div>
<!-- END HEADER -->
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
	
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container">
			

			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row margin-top-10">
				<div class="col-md-12">
					
					<!-- BEGIN PROFILE CONTENT -->
					<div class="profile-content">
						<div class="row">
						
						<div class="col-md-12">
				
					
					<div class="portlet light col-md-12 col-sm-12 col-xs-12">
						<div class="pagestick-title">
								<span>Customer Group & Map</span>
							</div>
						<div class="portlet-body">
							<form role="form" class="form-horizontal" name="submit_form" id="submit_form" method="POST">
					 	<div class="col-md-12 col-sm-12 col-xs-12">
															<label class="col-md-5 control-label">
															Group Name :</label>
															<div class="col-md-7">
																<div class="input-group">
																	<input type="text" class="form-control" placeholder="" id="groupName" name="groupName">
																</div>
															</div>
														</div> 
								<div class="col-md-12 col-sm-12 col-xs-12 nopad">
								<div class="table-scrollable table-scrollable-borderless managebank-cont custmap-tble">
								<table class="table table-hover table-bordered tble-custmapgroup">
								<thead>
								<tr class="uppercase">
								
									<th> 
										 Bank Name
									</th>
									<th> 
										Customer 
									</th>
									<th>
										Options
									</th>
								</tr>
								</thead> 
								<tbody class="add-Group">
								<tr>
								
									<td width="20%">
										 <select class="form-control select2 groupbranchname groupbranchname0" id="0"  name="groupbranchname0" onchange="changeFunc.call(this);">
											
										</select> 
			
										
									</td>
									<td width="20%">
								 <select class="form-control select2 groupcustomername groupcustomername0" id="0" name="groupcustomername0"  onchange="changeCustFunc.call(this);">
											
										</select> 
									</td>
									<td width="20%">
										<a class="btn btn-info add-custmapgroup"><i class="fa fa-plus"></i> Add</a>
									</td>	
									
								</tr>
							</tbody>
						<tbody class="Update-Group">
								</tbody> 
								</table>
							</div>
								</div>
									
								<div class="col-md-12 col-sm-12 col-xs-12 nopad">
							
								
												<div class="form-actions">
													<div class="">
														<div class="col-md-offset-3 col-md-9 text-right">
															
													
																	 <button type="button"  onclick="cancelGroup()" class="btn default" >Cancel</button>
																<a href="javascript:submitUpdateCustgroupForm()" class="btn blue button-submit" id="dv_update">
															Update
															</a>
															<a href="javascript:submitCustgroupForm()" class="btn blue button-submit button-save" id="dv_save">
															Create 
															</a>
														</div>
													</div>
												</div>		
								
								
								</div>
							</form>
							
						</div>
					</div>
					
					
				</div>
						
						

							
						</div> 
						
					</div>
					<!-- END PROFILE CONTENT -->
				</div>
			</div>
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.2/rollups/aes.js"></script>

<script type="text/javascript">
	var siteId=<%=session.getAttribute("siteId")%>;
	var userId=<%=session.getAttribute("userid")%>;
	var customerGrpIdval =<%=request.getParameter("customerGrpId")%>;

	$(document).ready(function() {  
		
 /* $('<script/>',{type:'text/javascript', src:'../resources/assets/global/plugins/jquery.base64.js'}).appendTo('head');
 */
 
 
 /* var url_string = window.location.href;
	  var url = new URL(url_string);
	  var password = 'password'; */
      //var customerGrpIdval = url.searchParams.get("customerGrpId");
	
	
	if(customerGrpIdval!=null){
	  /*  var  urlv = customerGrpIdval.replace(/\s/g, "+"); 
	   var customerGrpIdddd = CryptoJS.AES.decrypt(urlv, password);  */
	   var customerGrpId =customerGrpIdval;
	  /*  localStorage.setItem("customerGrpId",customerGrpId); */
	}
/* 		var url_string = window.location.href;
		var url = new URL(url_string);
		
		var customerGrpIdval = url.searchParams.get("customerGrpId");
	
		var customerGrpId = $.base64.decode(customerGrpIdval); */
		
		if(customerGrpIdval==null){
	

		$(".add-Group").show();
		$(".Update-Group").hide();
		$("#dv_update").hide();
		$("#dv_save").show();
		$.ajax({
			
				method:'get',
				
				
				url:'../Customermapping/branch/'+userId,
				contentType:'application/json',
				dataType:'JSON',
				crossDomain:'true',
				success:function(response)
{
					var htmlcustname= "<option  value=''>Select Customer</option>";
	 				var htmlbankid="";
	 				var htmlbankname= "<option value='' selected disabled>Select Bank</option>";
	 		
	 				var branch =response.dashboardResponse.bankInfo.bankbranchinfo;
	 				
	 			if(response.dashboardResponse.bankInfo.length=='')	{
	 				var	htmlbankname ="<OPTION>Select Bank</OPTION>"; 

 					$(".groupbranchname0").html(htmlbankname);
	 				
	 			}
	 			if(response.dashboardResponse.bankInfo.length!='')	{
	 				for(var i=0;i<response.dashboardResponse.bankInfo.length;i++){
 						
 				 		htmlbankname +="<OPTION VALUE="+response.dashboardResponse.bankInfo[i].bankId+" >"+response.dashboardResponse.bankInfo[i].bankName+"</OPTION>"; 

	 					$(".groupbranchname0").html(htmlbankname);	
	 			
	 				}
	 			}
	 			$(".groupcustomername0").html(htmlcustname);	
		},
	    error: function (e) {
            
            console.log(e.message);
         /*    alert("failed to get bank"); */
        }
		});
	
	
	
		
		}
		
		if(customerGrpIdval!=null){
			
			
			$(".add-Group").hide();
			$(".Update-Group").show();
			
			$("#dv_update").show();
			$("#dv_save").hide();
		 $.ajax({
				method: 'get',
				url: '../custmrGrp/customergroup/get/'+customerGrpId,
				contentType: 'application/json',
				dataType:'JSON',
				crossDomain:'true',
				success: function (response) {
					
					$("#groupName").val(response.custmrGrpngMngmntInfoList[0].customerGroupName);
	
		 				
		 					var newDivgroup = $('<tr class="newdivgroup-'+0+'"><td width="20%"><select class="form-control select2 updatebranchname updatebranchname'+0+'"  id='+0+' name="updatebranchname'+0+'" onchange="updatechangeFunc.call(this);"></select></td><td width="20%"><select class="form-control select2 updatecustomername updatecustomername'+0+'"  id='+0+' name="updatecustomername'+0+'" onchange="updateCustFunc.call(this);"></select></td><td width="20%"><a class="btn btn-info " onclick="addeditcustmapgroup();"><i class="fa fa-plus"></i> Add</a></td></tr>');
		 					$('table.tble-custmapgroup tbody.Update-Group').append(newDivgroup);	
		 					
		 					for(g=1;g<response.custmrGrpngMngmntInfoList.length;g++){
			 					var newDivgroup = $('<tr class="newdivgroup-'+g+'"><td width="20%"><select class="form-control select2 updatebranchname updatebranchname'+g+'"  id='+g+' name="updatebranchname'+g+'" onchange="updatechangeFunc.call(this);"></select></td><td width="20%"><select class="form-control select2 updatecustomername updatecustomername'+g+'"  id='+g+' name="updatecustomername'+g+'" onchange="updateCustFunc.call(this);"></select></td><td width="20%"><a class="btn btn-info deltebnk" onclick="deletecustgroup('+g+');"><i class="fa fa-trash"></i> Delete</a></td></tr>');
			 					$('table.tble-custmapgroup tbody.Update-Group').append(newDivgroup);	
			 					}			
		 			  m=response.custmrGrpngMngmntInfoList.length;
		 
			 		
				for(i=0;i<response.custmrGrpngMngmntInfoList.length;i++){

		 				 		var branch="<OPTION VALUE="+response.custmrGrpngMngmntInfoList[i].bankwiseId+">"+response.custmrGrpngMngmntInfoList[i].BankName+"</OPTION>"; 
		 				 				
		 				$(".updatebranchname"+i).html(branch);	
		 				
					} 
			 		getappendbranch(m);
				for(i=0;i<response.custmrGrpngMngmntInfoList.length;i++){

	 				 		var customer="<OPTION VALUE="+response.custmrGrpngMngmntInfoList[i].customerid+">"+response.custmrGrpngMngmntInfoList[i].companyName+"</OPTION>"; 
	 				 				
	 				$(".updatecustomername"+i).html(customer);	

				} 

									   
				},

			 error:function(response,statusTxt,error){
			 
			/* 	alert("Failed to add record :"+response.responseJSON); */
			 }
				
				
			});
		 

			
				function getappendbranch(m)
				{
				
					var row=m;
			
				$.ajax({
					
					method:'get',
					
					
					url:'../Customermapping/branch/'+userId,
					contentType:'application/json',
					dataType:'JSON',
					crossDomain:'true',
					success:function(response)
	{
		 				var htmlbankid="";
		 				var htmlbankname= "<option value='' disabled>Select Bank</option>";
		 			
		 				var branch =response.dashboardResponse.bankInfo.bankbranchinfo;
		 				
							
		 				for(var i=0;i<response.dashboardResponse.bankInfo.length;i++){
		 			
		 					htmlbankname +="<OPTION VALUE="+response.dashboardResponse.bankInfo[i].bankId+" >"+response.dashboardResponse.bankInfo[i].bankName+"</OPTION>"; 

		 				}
		 				for(var j=0;j<row;j++){
		 				$(".updatebranchname"+j).append(htmlbankname);
		 				
		 				
		 				}
			},
			error:function(response,statusTxt,error){
				 
				/* alert("Failed to add record :"+response.responseJSON); */
			 }
			});		
				}
				
		}
		
		
		
		});
	
	
	/* function add
	 */
	function changeFunc() {
	
		  var rowid  = $(this).attr("id");
		
		      var branchId  = $(".groupbranchname"+rowid).val();
		
		      $.ajax({
		        	 method: 'get',
		             url: '../customer/getcustomer/'+branchId,
		             contentType: 'application/json',
					 dataType:'JSON',
					 crossDomain:'true',
		             success: function(response) {
		               
		             
		
		                	var appendcustomername= "<option value='' selected disabled>Select Customer</option>";
							
							for (var i = 0; i < response.bankBranches.bankCustomerList.length; i++){
							
							  appendcustomername += "<option value='" + response.bankBranches.bankCustomerList[i].customerid + "'>" + response.bankBranches.bankCustomerList[i].companyName + "</option>";
							}
			
							$(".groupcustomername"+rowid).html(appendcustomername);
			
		                	
		             },
		             error: function (e) {
		                
		                 console.log(e.message);
		             /*     alert("failed to get bank customer"); */
		             }
		         });
	}

	function updatechangeFunc() {
		
		  var rowid  = $(this).attr("id");
		
		      var branchId  = $(".updatebranchname"+rowid).val();
		
		      $.ajax({
		        	 method: 'get',
		             url: '../customer/getcustomer/'+branchId,
		             contentType: 'application/json',
					 dataType:'JSON',
					 crossDomain:'true',
		             success: function(response) {
		               
		             
		
		                	var appendcustomername= "<option value='' selected disabled>Select Customer</option>";
							
							for (var i = 0; i < response.bankBranches.bankCustomerList.length; i++){
							
							  appendcustomername += "<option value='" + response.bankBranches.bankCustomerList[i].customerid + "'>" + response.bankBranches.bankCustomerList[i].companyName + "</option>";
							}
			
							$(".updatecustomername"+rowid).html(appendcustomername);
			
		                	
		             },
		             error: function (e) {
		                
		                 console.log(e.message);
		                /*  alert("failed to get bank customer"); */
		             }
		         });
	}
	function submitCustgroupForm()
	{
		var count=0;
		var numItems = $('.tble-custmapgroup tbody.add-Group tr').size();

		var url_data = ($("#submit_form").serialize());
		
		var result = queryStringToJSON(url_data);
		
		var mappingInfo="";
		var Editcustdetails = [];
	
	         var grpName=  $("#groupName").val();
var newcount=0;
var countval=0;

if(grpName!=''){
	$(".groupcustomername").each(function()
			
			{
						  var k  = $(this).attr("id");
	
			           Editcustdetails[countval]= 
							{
								 customerId:result["groupcustomername"+k],
								 bankId:result["groupbranchname"+k],
									userId:userId,
								isactive:'1'
									
							} 
			           
			           if(result["groupbranchname"+k]==null)
			        	 {
			        	   newcount++;
			        
			        	 swal("Select bank and customer")
			        
			        	 }
			           
			         if(result["groupcustomername"+k]==null)
			        	 {
			        	
			        newcount++;
			        	 swal("Map the Customer")
			        
			        	 }
			
			           countval++;
			});

if(newcount==0){

			$(".groupcustomername").each(function()
					
					{
								  var k  = $(this).attr("id");
			
					           Editcustdetails[count]= 
									{
										 customerId:result["groupcustomername"+k],
										 bankId:result["groupbranchname"+k],
											userId:userId,
										isactive:'1'
											
									} 
					 
								 mappingInfo={

										customerGroupName : $("#groupName").val(),
										customergroupingDetails:Editcustdetails,
								 userid:userId,
									isactive:'1'
							}
					           
							count++;
					           
					        	   
					});
			
			
 $.ajax({
			method:'post',
			url:'../custmrGrp/customergroup/mapping/customer',
			data:JSON.stringify(mappingInfo),
			contentType:'application/json',
			dataType:'JSON',
			crossDomain:'true',
			success: function(response) 
			{
				swal({title:"Done",text:"Customer Grouped Successfully",type:"success"},function(){window.location="manage-custgroup.jsp";});
			},
			
		 error:function(response,statusTxt,error){
		 
		/* 	alert("Failed to add record :"+response.responseJSON.errMsgs); */
		 }
	}); 
}


}
if(grpName==''){
	 swal("Pls fill the group name")
	
}

}
	


	 function cancelGroup(){
     location.href='manage-custgroup.jsp';
	 }

		function changeCustFunc() {
	 
			  var crowid  = $(this).attr("id");
			
			      var branchIdvalue  = $(".groupcustomername"+crowid).val();
			    
				$(".groupcustomername").each(function()
						{
										  var rowval  = $(this).attr("id");
	
				if(rowval!==crowid){
				
				var custid=$(".groupcustomername"+rowval).val();
					
				if(branchIdvalue==custid)
					{
					swal("customer already mapped")
						
					  
					  var branchId  = $(".groupbranchname"+crowid).val();
				      $.ajax({
				        	 method: 'get',
				             url: '../customer/getcustomer/'+branchId,
				             contentType: 'application/json',
							 dataType:'JSON',
							 crossDomain:'true',
						       success: function(response) {
					            
						   		
				                	var listItems= "<option value='' selected disabled>Select Customer</option>";
									for (var i = 0; i < response.bankBranches.bankCustomerList.length; i++){

									  listItems += "<option value='" + response.bankBranches.bankCustomerList[i].customerid + "'>" + response.bankBranches.bankCustomerList[i].companyName + "</option>";
									}
									$(".groupcustomername"+crowid).html('');
									$(".groupcustomername"+crowid).html(listItems);
				                	
				             },
				             error: function (e) {
				              
				                 console.log(e.message);
				             /*     alert("failed to get bank customer"); */
				             }
				         });
				
					}
				
				
			}	
						 });
		}
		function updateCustFunc() {
			 
			  var crowid  = $(this).attr("id");
		
			      var branchIdvalue  = $(".updatecustomername"+crowid).val();
			
				$(".updatecustomername").each(function()
						{
										  var rowval  = $(this).attr("id");
											
				if(rowval!==crowid){
				
				var custid=$(".updatecustomername"+rowval).val();
			
				if(branchIdvalue==custid)
					{
					swal("customer already mapped")
						
					  
					  var branchId  = $(".updatebranchname"+crowid).val();
				      $.ajax({
				        	 method: 'get',
				             url: '../customer/getcustomer/'+branchId,
				             contentType: 'application/json',
							 dataType:'JSON',
							 crossDomain:'true',
						       success: function(response) {
					            
						   		
				                	var listItems= "<option value='' selected disabled>Select Customer</option>";
									for (var i = 0; i < response.bankBranches.bankCustomerList.length; i++){

									  listItems += "<option value='" + response.bankBranches.bankCustomerList[i].customerid + "'>" + response.bankBranches.bankCustomerList[i].companyName + "</option>";
									}
									$(".updatecustomername"+crowid).html('');
									$(".updatecustomername"+crowid).html(listItems);
				                	
				             },
				             error: function (e) {
				              
				                 console.log(e.message);
				               /*   alert("failed to get bank customer"); */
				             }
				         });
				
					}
				
				
			}	
						 });
		}
	/* 	function update
		 */
		
		function addeditcustmapgroup()
		{
			m++;
			var newDivgroup = $('<tr class="newdivgroup-'+m+'"><td width="20%"><select class="form-control select2 updatebranchname updatebranchname'+m+'"  id='+m+' name="updatebranchname'+m+'" onchange="appendchangeFunc.call(this);"></select></td><td width="20%"><select class="form-control select2 updatecustomername updatecustomername'+m+'"  id='+m+' name="updatecustomername'+m+'" onchange="updateCustFunc.call(this);"></select></td><td width="20%"><a class="btn btn-info deltebnk" onclick="deltecustbnkgroup('+m+');"><i class="fa fa-trash"></i> Delete</a></td></tr>');
			$('table.tble-custmapgroup tbody.Update-Group').append(newDivgroup);
			var siteId=<%=session.getAttribute("siteId")%>;
			var userId=<%=session.getAttribute("userid")%>;
			$.ajax({		
				method:'get',

				url:'../Customermapping/branch/'+userId,
				contentType:'application/json',
				dataType:'JSON',
				crossDomain:'true',
				success:function(response)
		{
					
					
					var htmlcustname= "<option  value='' selected disabled>Select Customer</option>";
					
			var appendbankname="<OPTION value='' selected disabled >Select Bank</OPTION>";
						
						for(var i=0;i<response.dashboardResponse.bankInfo.length;i++){

							appendbankname +="<OPTION VALUE="+response.dashboardResponse.bankInfo[i].bankId+" >"+response.dashboardResponse.bankInfo[i].bankName+"</OPTION>"; 



				$(".updatebranchname"+m).html(appendbankname);			
						} 
						$(".updatecustomername"+m).html(htmlcustname);	
		},
		error:function(response,statusTxt,error){
			 
			/* alert("Failed to add record :"+response.responseJSON); */
		 }
		 


			});
		}

 		function appendchangeFunc() {
			
			  var rowid  = $(this).attr("id");
			
			      var branchId  = $(".updatebranchname"+rowid).val();
			
			      $.ajax({
			        	 method: 'get',
			             url: '../customer/getcustomer/'+branchId,
			             contentType: 'application/json',
						 dataType:'JSON',
						 crossDomain:'true',
			             success: function(response) {
			               
			             
			
			                	var appendcustomername= "<option value='' selected disabled>Select Customer</option>";
								
								for (var i = 0; i < response.bankBranches.bankCustomerList.length; i++){
								
								  appendcustomername += "<option value='" + response.bankBranches.bankCustomerList[i].customerid + "'>" + response.bankBranches.bankCustomerList[i].companyName + "</option>";
								}
				
								$(".updatecustomername"+rowid).html(appendcustomername);
				
			                	
			             },
			             error: function (e) {
			                
			                 console.log(e.message);
			               /*   alert("failed to get bank customer"); */
			             }
			         });
		} 
			 
		function submitUpdateCustgroupForm()


		{
			var customerGrpId= customerGrpIdval;
			var isactive="2";
		
			var count=0;
			var numItems = $('.tble-custmapgroup tbody.Update-Group tr').size();

			var url_data = ($("#submit_form").serialize());
			
			var result = queryStringToJSON(url_data);
			var mappingInfo="";
			var Editcustdetails = [];
			
			var grpName=  $("#groupName").val();
			var newcount=0;
			var countval=0;


			if(grpName!=''){
				$(".updatecustomername").each(function()
						
						{
									  var k  = $(this).attr("id");
				
						           Editcustdetails[countval]= 
										{
											 customerId:result["updatecustomername"+k],
											 bankId:result["updatebranchname"+k],
												userId:userId,
											isactive:'1'
												
										} 
						           if(result["updatebranchname"+k]==null && result["updatecustomername"+k]==null)
						        	 {
						        	   newcount++;
						        	 swal("Select bank and customer")
						        	 }
						           
						         if(result["updatecustomername"+k]==null && result["updatebranchname"+k]!=null)
						        	 {
						        newcount++;
						        	 swal("Map the Customer")
						        
						        	 }
						
						           countval++;
						});

			if(newcount==0){
			
				InactivateGroup(customerGrpId,isactive);
	 		$(".updatecustomername").each(function()
				
	{

				  var k  = $(this).attr("id");

	           Editcustdetails[count]= 
		
					{
						 customerId:result["updatecustomername"+k],
						 bankId:result["updatebranchname"+k],
							userId:userId,
						isactive:'1'
							
					} 

				 mappingInfo={

						customerGroupName : $("#groupName").val(),
						customergroupingDetails:Editcustdetails,
				 userid:userId,
					isactive:'1'
			}
	          
			count++;
	});

	 		 $.ajax({
	 			method:'post',
	 			url:'../custmrGrp/customergroup/update/customer',
	 			data:JSON.stringify(mappingInfo),
	 			contentType:'application/json',
	 			dataType:'JSON',
	 			crossDomain:'true',
	 			success: function(response) 
	 			{
	 				swal({title:"Done",text:"CustomerGroup Updated Successfully",type:"success"},function(){window.location="manage-custgroup.jsp";});
	 			},
	 			
	 		 error:function(response,statusTxt,error){
	 		 
	 			/* alert("Failed to add record :"+response.responseJSON.errMsgs); */
	 		 }
	 	}); 
	 		
			}


			}
			if(grpName==''){
				 swal("Pls fill the group name")
				
			}

			}
					
		
		function InactivateGroup(customerGrpId,isactive)
		{
		
			var delcustdetails = [];
			
		 delcustdetails[0]=
				{
					
				 customerGrpId:customerGrpId,
					isactive:isactive
						
				}
				 
			var groupInfo = {
					
						customerGrpId:customerGrpId,
						isactive:isactive,
						customergroupingDetails:delcustdetails
				}
			
					$.ajax({
						method: 'post',
						url: '../custmrGrp/CustGrp/delete',
						data: JSON.stringify(groupInfo),
						contentType: 'application/json',
						dataType:'JSON',
						crossDomain:'true',
						success: function (response) {
						
						},

					 error:function(response,statusTxt,error){
					 
					/* 	alert("Failed to add record :"+response.responseJSON); */
					 }
					});
			

		}

	
			 var queryStringToJSON = function (url) {
			    if (url === '')
			        return '';
			    var pairs = (url || location.search).slice(1).split('&');
			    var result = {};
			    for (var idx in pairs) {
			        var pair = pairs[idx].split('=');
			        if (!!pair[0])
			            result[pair[0].toLowerCase()] = decodeURIComponent(pair[1] || '');
			    }
			    return result;
			}
</script>

<!-- END PAGE CONTAINER -->
<!-- BEGIN FOOTER -->
<jsp:include page="includes/footer.jsp" />
<jsp:include page="includes/footer-js.jsp" />
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>