<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<jsp:include page="includes/style.jsp" />
<jsp:include page="includes/footer-js.jsp" />
<body class="page-md">
	<!-- BEGIN HEADER -->
	<div class="page-header">
		<!-- BEGIN HEADER TOP -->
		<jsp:include page="includes/header-top.jsp" />
		<!-- END HEADER TOP -->
		<!-- BEGIN HEADER MENU -->
		<jsp:include page="includes/header-menu.jsp" />
		<!-- END HEADER MENU -->
	</div>
	<!-- END HEADER -->
	<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<div class="container">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1>Dashboard</h1>
			</div>
			<!-- END PAGE TITLE -->
		</div>
	</div>
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container">

			<!-- BEGIN PAGE CONTENT INNER -->

				<div class="col-md-12 col-sm-12 col-xs-12 info-wrapper">
					<!-- BEGIN PORTLET-->

						<div class="row file-info">
							
							<div class="col-md-4 info-block infopurple-block">
								<div class="portlet box blue-irplus col-xs-12">
									<div class="portlet-body col-xs-12">
									<div class="port-icon">
											<img src="../resources/images/users.png" class="img-responsive">
										</div>
										<div class="number">
											<h3 class="font-green-sharp" id ="totalUsers">

											</h3>
										</div>
										<div class="icon">
											Users
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4 info-block infored-block">
								<div class="portlet box blue-irplus col-xs-12">
									<div class="portlet-body col-xs-12">
									<div class="port-icon">
											<img src="../resources/images/banks.png" class="img-responsive">
										</div>
										<div class="number">
											<h3 class="font-green-sharp" id="totalBanks">

											</h3>
										</div>
										<div class="icon">
											Banks
										</div>
									</div>
								</div>
							</div>
<div class="col-md-4 info-block infoblue-block">
								<div class="portlet box blue-irplus col-xs-12">
									<div class="portlet-body col-xs-12">
									<div class="port-icon">
											<img src="../resources/images/roles.png" class="img-responsive">
										</div>
										<div class="number">
											<h3 class="font-green-sharp" id="totalCustomers">

											</h3>
										</div>
										<div class="icon">
											Customers
										</div>
									</div>
								</div>
							</div>
						</div>

				</div>


				<div class="col-md-12 col-sm-12 col-xs-12 homebanks-wraper nopad">
					<div class="custom-title text-uppercase border-overline text-center">
					<span>Banks</span>

					</div>

					<div class="row">

					<div class="col-md-12 col-sm-12 col-md-12 nopad">
					<div class="homebanks-slider" id="bankslider">

					</div>
					</div>
					</div>
				</div>
		

				<div class="col-md-12 col-sm-12 col-xs-12 fileprocess-wraper">
				<div class="row">
					<div class="col-md-6 col-sm-6 col-xs-12 dailyprocess-wraper">
						<div class="medium-title"> Site Information</div>
						
						
						<div class="col-md-12 col-sm-12 col-xs-12 dailyprocess-single">
							<div class="col-md-3 col-sm-4 col-xs-12 dp-datecircle">
							<div class="datecircle">
							<div id="current_month">
							</div>
							<br>
							<div id="current_date">
							</div>
							<br>
							<div id="current_year">
							</div>
							
							
							
							</div>
					
							</div>
							<div class="col-md-9 col-sm-8 col-xs-12 dp-detailist">
								<div class="dp-detailsingle">
									<span>Files :</span><b><span id="totalFiles"></span></b>
								</div>
								<div class="dp-detailsingle">
									<span>Batches :</span> <b><span id="totalBatches"></span></b>
								</div>
								<div class="dp-detailsingle">
									<span>Transactions :</span> <b><span id="totalTransactions"></span></b>
								</div>
								<div class="dp-detailsingle no-border">
									<span>Amount :</span> <b><span>$ </span><span id="totalCredit"></span></b>
								</div>
							
							</div>
						</div> 
				<div class="col-md-12 col-sm-12 col-xs-12 dailyprocess-single">
							<div class="col-md-3 col-sm-4 col-xs-12 dp-datecircle">
							<div class="datecircle">
								<div id="sec_month">
							</div>
							<br>
							<div id="sec_date">
							</div>
							<br>
							<div id="sec_year">
							</div>
							</div>
					
							</div>
							<div class="col-md-9 col-sm-8 col-xs-12 dp-detailist">
								<div class="dp-detailsingle">
									<span>Files :</span><b><span id="sectotalFiles"></span></b>
								</div>
								<div class="dp-detailsingle">
									<span>Batches :</span><b><span id="sectotalBatches"></span></b>
								</div>
								<div class="dp-detailsingle">
									<span>Transactions :</span> <b><span id="sectotalTransactions"></span></b>
								</div>
							<div class="dp-detailsingle no-border">
									<span> Amount :</span> <b><span>$ </span><span id="sectotalCredit"></span></b>
								</div>
							
							</div>
						</div>
					<div class="col-md-12 col-sm-12 col-xs-12 dailyprocess-single">
							<div class="col-md-3 col-sm-4 col-xs-12 dp-datecircle">
							<div class="datecircle">
								<div id="third_month">
							</div>
							<br>
							<div id="third_date">
							</div>
							<br>
							<div id="third_year">
							</div>
							</div>
					
							</div>
							<div class="col-md-9 col-sm-8 col-xs-12 dp-detailist">
							<div class="dp-detailsingle">
									<span>Files :</span><b><span id="thirdtotalFiles"></span></b>
								</div>
								<div class="dp-detailsingle">
									<span>Batches :</span><b><span id="thirdsectotalBatches"></span></b>
								</div>
								<div class="dp-detailsingle">
									<span>Transactions :</span> <b><span id="thirdsectotalTransactions"></span></b>
								</div>
								<div class="dp-detailsingle no-border">
									<span>Amount :</span> <b><span>$ </span><span id="thirdsectotalCredit"></span></b>
								</div>
							
							</div>
						</div> 
					</div>

					
<div class="col-md-6 col-sm-6 col-xs-12 fileinfo-wraper">
					<div class="col-md-12 col-sm-12 col-xs-12 nopad">
					<div class="col-md-8 col-sm-6 col-xs-12 nopad">
						<div class="medium-title"> Bank Information
						</div>
						</div>
						<div class="col-md-4 col-sm-6 col-xs-12 nopad">
						<select  class="form-control" id="branchname">
						
						</select>
						</div>
						</div>
			
				
	     <div class="col-md-12 col-sm-12 col-xs-12 fileinfo-single">
	     <div class="col-md-12 col-sm-12 col-xs-12 nopad border-bottom">
	     <div class="col-md-6 col-sm-6 col-xs-12 nopad"><span id="firstdate" class="filename"></span></div>
	     <div class="col-md-6 col-sm-6 col-xs-12 text-right text-green amount-right"><span>Amount : </span><span><b>$<span id="firstbatchAmt" ></span></b></span></div></div>
	    
	     <div class="col-md-12 col-sm-12 col-xs-12 nopad text-darkgreen">
	     	     <div class="col-md-3 col-sm-3 col-xs-12 batchcount border-right">
	     <div style="font-size:14px;"><strong>FILES</strong></div><div class="batchcount-number"><b><span id="firstfilecount"></span></b></div></div>
	     <div class="col-md-3 col-sm-3 col-xs-12 batchcount border-right">
	     <div style="font-size:14px;"><strong>BATCHES</strong></div><div class="batchcount-number"><b><span id="firstbatchCount"></span></b></div></div>
	     <div class="col-md-3 col-sm-3 col-xs-12 batchcount border-right">
	     <div style="font-size:14px;"><strong>TRANSACTIONS</strong></div><div class="batchcount-number"><b><span id="firsttranscount"></span></b></div></div>
	     <div class="col-md-3 col-sm-3 col-xs-12 batchcount"><div style="font-size:14px;"><strong>ITEMS</strong></div>
	     <div class="batchcount-number "><b><span id="firstitemcount"></span></b></div></div>
	     
	     </div>
	     
	     </div>
				
			
			<div class="col-md-12 col-sm-12 col-xs-12 fileinfo-single">
	     <div class="col-md-12 col-sm-12 col-xs-12 nopad border-bottom">
	     <div class="col-md-6 col-sm-6 col-xs-12 nopad"><span id="secondate" class="filename"></span></div>
	     <div class="col-md-6 col-sm-6 col-xs-12 text-right text-green amount-right"><span>Amount : </span><span><b>$<span id="secbatchAmt" ></span></b></span></div></div>
	     <div class="col-md-12 col-sm-12 col-xs-12 nopad text-darkgreen">
	      <div class="col-md-3 col-sm-3 col-xs-12 batchcount border-right">
	     <div style="font-size:14px;"><strong>FILES</strong></div><div class="batchcount-number"><b><span id="secfilecount"></span></b></div></div>
	     <div class="col-md-3 col-sm-3 col-xs-12 batchcount border-right">
	     <div style="font-size:14px;"><strong>BATCHES</strong></div><div class="batchcount-number"><b><span id="secbatchCount"></span></b></div></div>
	     <div class="col-md-3 col-sm-3 col-xs-12 batchcount border-right"><div style="font-size:14px;"><strong>TRANSACTIONS</strong></div>
	     <div class="batchcount-number"><b><span id="sectranscount"></span></b></div></div><div class="col-md-3 col-sm-3 col-xs-12 batchcount"><div style="font-size:14px;"><strong>ITEMS</strong></div>
	     <div class="batchcount-number "><b><span id="secitemcount"></span></b></div></div></div></div>
			
			
			<div class="col-md-12 col-sm-12 col-xs-12 fileinfo-single">
	     <div class="col-md-12 col-sm-12 col-xs-12 nopad border-bottom">
	     <div class="col-md-6 col-sm-6 col-xs-12 nopad"><span id="thirddate" class="filename"></span></div>
	     <div class="col-md-6 col-sm-6 col-xs-12 text-right text-green amount-right"><span>Amount : </span><span><b>$<span id="thirdbatchAmt" ></span></b></span></div></div>
	     <div class="col-md-12 col-sm-12 col-xs-12 nopad text-darkgreen">
	      <div class="col-md-3 col-sm-3 col-xs-12 batchcount border-right">
	     <div style="font-size:14px;"><strong>FILES</strong></div><div class="batchcount-number"><b><span id="thirdfilecount"></span></b></div></div>
	     <div class="col-md-3 col-sm-3 col-xs-12 batchcount border-right">
	     <div style="font-size:14px;"><strong>BATCHES</strong></div><div class="batchcount-number"><b><span id="thirdbatchCount"></span></b></div></div>
	     <div class="col-md-3 col-sm-3 col-xs-12 batchcount border-right"><div style="font-size:14px;"><strong>TRANSACTIONS</strong></div>
	     <div class="batchcount-number"><b><span id="thirdtranscount"></span></b></div></div><div class="col-md-3 col-sm-3 col-xs-12 batchcount"><div style="font-size:14px;"><strong>ITEMS</strong></div>
	     <div class="batchcount-number "><b><span id="thirditemcount"></span></b></div></div></div></div>
	     
					</div>
				</div>
				</div>

		</div>

	</div>
	<!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER -->
<!-- BEGIN FOOTER -->
<jsp:include page="includes/footer.jsp" />
<jsp:include page="includes/footer-js.jsp" />
 <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.2/rollups/aes.js"></script>
<script>
var siteId=<%=session.getAttribute("siteId")%>;
var userId=<%=session.getAttribute("userid")%>;

     $(document).ready(function()
    	{
    /* 	 var strCookies = document.cookie;
    	 var cookiearray = strCookies.split(';')
    	 for(var i=0; i<cookiearray.length; i++){
    	   name = cookiearray[i].split('=')[0];
    	   value = cookiearray[i].split('=')[1];
    	   if(name == 'sid')
    	     var  sid = value;
    	   alert(sid)
    	 } */
/*     
    	
    	 
    	 function get_cookies_array() {

    		    var cookies = { };

    		    if (document.cookie && document.cookie != '') {
    		    	
    		        var split = document.cookie.split(';');
    		        for (var i = 0; i < split.length; i++) {
    		    
    		            var name_value = split[i].split("=");
    		        	
    		            name_value[0] = name_value[0].replace(/^ /, '');
    		            cookies[decodeURIComponent(name_value[0])] = decodeURIComponent(name_value[1]);
    		        }
    		    }

    		    return cookies;
    		   
    		}
    	 var cookies = get_cookies_array();
    	 for(var name in cookies) {
    		 document.write( name + " : " + cookies[name] + "<br />" );
    	 } */
	 		$.ajax({
	 		 	
	 			method:'get',
	 			url:'../dashboard/getData/'+userId,
	 			contentType:'application/json',
	 			dataType:'JSON',
	 			crossDomain:'true',
	 			success:function(response){
	 				var dashboard = response.dashboardResponse;
	 				
	 			
	 				$("#totalUsers").append(dashboard.totalUsers);
	 				$("#totalCustomers").append(dashboard.totalCustomerCount);
	 				$("#totalBanks").append(dashboard.totalBanks);
	 				
	 			
	 				if(response.dashboardResponse.bankInfo.length!=''){
	 			
	 				var htmlbankname="";
	 			var htmlbankname ="<OPTION VALUE="+response.dashboardResponse.bankInfo[0].bankId+" selected >"+response.dashboardResponse.bankInfo[0].bankName+"</OPTION>";
	 			$("#branchname").html(htmlbankname);	

	 				var branch =response.dashboardResponse.bankInfo.bankbranchinfo;
	 				for(var i=1;i<response.dashboardResponse.bankInfo.length;i++){
	 					
					 		 htmlbankname +="<OPTION VALUE="+response.dashboardResponse.bankInfo[i].bankId+">"+response.dashboardResponse.bankInfo[i].bankName+"</OPTION>"; 

	 					$("#branchname").html(htmlbankname);	
	 				 				
	 				}
	 				}
	 				
	 				if(response.dashboardResponse.bankInfo.length==0){
	 				
	 					var htmlbankname="";
	 		 			var htmlbankname ="<OPTION>Select Bank</OPTION>";
	 		 			$("#branchname").html(htmlbankname);	

	 					
	 				}
	 				var dy_html='';
			 	 	for(var i=0;i<response.dashboardResponse.bankInfo.length;i++){
			 			var src;
			 			if(response.dashboardResponse.bankInfo[i].bankLogo==''){
			 				 src="../resources/assets/admin/layout3/img/avatar9.jpg";
				 				dy_html+='<div class="homebanks-single"><div class="homebanks-inner"><div class="homebanks-top"><div  class="homebanks-icon"><img src="'+src+'" class="img-responsive"></div><div class="medium-title text-center homebanks-caption">'+response.dashboardResponse.bankInfo[i].bankName+'</div></div><div class="homebanks-bottom"><div class="col-md-6 col-sm-6 col-xs-6 bottom-single text-white border-right"><span><i class="fa fa-map-marker"></i></span><span><small>Bank locations</small><div class="count">'+response.dashboardResponse.bankInfo[i].totalBranches+'</div></span></div><div class="col-md-6 col-sm-6 col-xs-6 bottom-single text-white"><span><i class="fa fa-users"></i></span><span><small>Customers</small><div class="count">'+response.dashboardResponse.bankInfo[i].totalCustomers+'</div></span></div></div></div></div>';

			 			}else{
			 				
			 				 src="http://tcmuonline.com:8080/fileimages/Images/BankLogo/"+response.dashboardResponse.bankInfo[i].bankLogo;
				 			dy_html+='<div class="homebanks-single"><div class="homebanks-inner"><div class="homebanks-top"><div  class="homebanks-icon"><img src="'+src+'" class="img-responsive"></div><div class="medium-title text-center homebanks-caption">'+response.dashboardResponse.bankInfo[i].bankName+'</div></div><div class="homebanks-bottom"><div class="col-md-6 col-sm-6 col-xs-6 bottom-single text-white border-right"><span><i class="fa fa-map-marker"></i></span><span><small>Bank locations</small><div class="count">'+response.dashboardResponse.bankInfo[i].totalBranches+'</div></span></div><div class="col-md-6 col-sm-6 col-xs-6 bottom-single text-white"><span><i class="fa fa-users"></i></span><span><small>Customers</small><div class="count">'+response.dashboardResponse.bankInfo[i].totalCustomers+'</div></span></div></div></div></div>';

			 			}
			 			
			 			
			 				
			 		} 
	
					$("#bankslider").html(dy_html);
			 
					initialize_owl($('#bankslider'));
			 		
					if(response.dashboardResponse.bankInfo.length==''){
	
       var firstdate = new Date();
      
            month = '' + (firstdate.getMonth() + 1),
            day = '' + firstdate.getDate(),
            year = firstdate.getFullYear();
        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;
         var bankfirstdates=[month, day, year].join('-');
 
			$("#firstdate").append(bankfirstdates);
      	$("#firstbatchAmt").append("0.00");
      	$("#firstbatchCount").append(0);
      	$("#firsttranscount").append(0);
      	$("#firstitemcount").append(0);
      	$("#firstfilecount").append(0);	
		
		
      	$("#secfilecount").append(0);

        var secdate = new Date();
		      secdate.setDate(secdate.getDate()-1);
            month = '' + (secdate.getMonth() + 1),
            day = '' + secdate.getDate(),
            year = secdate.getFullYear();
        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;
         var banksecdates=[month, day, year].join('-');
		
			$("#secondate").append(banksecdates);
      	$("#secbatchAmt").append("0.00");
      	$("#secbatchCount").append(0);
      	$("#sectranscount").append(0);
      	$("#secitemcount").append(0);
      	
      	$("#thirdfilecount").append(0);
	    
        var thirddate = new Date();
        thirddate.setDate(thirddate.getDate()-2);
            month = '' + (thirddate.getMonth() + 1),
            day = '' + thirddate.getDate(),
            year = thirddate.getFullYear();
        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;
         var bankthirddates=[month, day, year].join('-');
		
			$("#thirddate").append(bankthirddates);
      	$("#thirdbatchAmt").append("0.00");
      	$("#thirdbatchCount").append(0);
      	$("#thirdtranscount").append(0);
      	$("#thirditemcount").append(0);
	}
		
	
					if(response.dashboardResponse.bankInfo.length!=''){
						
						data =
						{
								userid:userId,
							
						}
						
			        $.ajax({
			           	 method: 'get',
			                url: '../bankwise/fileprocess/'+response.dashboardResponse.bankInfo[0].bankId,
			            	data: data,
			                contentType: 'application/json',
			    			 dataType:'JSON',
			    			 crossDomain:'true',
			                success: function(response) {

			               	 
			    				var dashboard = response.dashboardResponse;
			    				
			    				var fileprocess='';
			    	 		
			    				
			    if(response.Filelist!==''){
			     	$("#firstfilecount").append(response.dashboardResponse.totalFiles);	
			
			     	var firstsrc =response.dashboardResponse.firstdate;
			     
	 				$("#firstdate").append(firstsrc);
			      	$("#firstbatchAmt").append(response.dashboardResponse.totalBatchamt);
			      	$("#firstbatchCount").append(response.dashboardResponse.totalBatches);
			      	$("#firsttranscount").append(response.dashboardResponse.totalTransactions);
			      	$("#firstitemcount").append(response.dashboardResponse.totalItemcount);
			    		
			     	$("#secfilecount").append(response.dashboardResponse.sectotalFiles);
			  
     				
     			 	var secsrc =response.dashboardResponse.secdate;
     			   
	 				$("#secondate").append(secsrc);
			      	$("#secbatchAmt").append(response.dashboardResponse.sectotalCreditAmt);
			      	$("#secbatchCount").append(response.dashboardResponse.sectotalBatches);
			      	$("#sectranscount").append(response.dashboardResponse.sectotalTransactions);
			      	$("#secitemcount").append(response.dashboardResponse.secitemcount);
			      	
			     	$("#thirdfilecount").append(response.dashboardResponse.thirdtotalFiles);
			    
			    	
					var thirdsrc =response.dashboardResponse.thirddate;
   			
	 			
	 				$("#thirddate").append(thirdsrc);
			      	$("#thirdbatchAmt").append(response.dashboardResponse.thirdtotalCreditAmt);
			      	$("#thirdbatchCount").append(response.dashboardResponse.thirdtotalBatches);
			      	$("#thirdtranscount").append(response.dashboardResponse.thirdtotalTransactions);
			      	$("#thirditemcount").append(response.dashboardResponse.thirditemcount);
			 
			    					
			    		
			    } 
			 }
			         });  		
					}
				},

			 error:function(response,statusTxt,error){
			 
	
			 }	

	 		});
	

		 	$.ajax({
			 	
				method:'get',
			
				url:'../filesToday/'+userId,
			
				contentType:'application/json',
				dataType:'JSON',
				crossDomain:'true',
				success:function(response){
					
					var dashboard = response.dashboardResponse;
	
			 			
		 		$("#totalBatches").append(dashboard.totalBatches);

		 				$("#totalTransactions").append(dashboard.totalTransactions);
		 				$("#totalCredit").append(dashboard.totalBatchamt);
		 				$("#totalFiles").append(dashboard.totalFiles);
		 				
		 			
		 				$("#sectotalBatches").append(dashboard.sectotalBatches);

		 				$("#sectotalTransactions").append(dashboard.sectotalTransactions);
		 				$("#sectotalCredit").append(dashboard.sectotalCreditAmt);
		 			
		 				$("#sectotalFiles").append(dashboard.sectotalFiles);
		 				
		 				
		 			 	$("#thirdsectotalBatches").append(dashboard.thirdtotalBatches);

		 				$("#thirdsectotalTransactions").append(dashboard.thirdtotalTransactions);
		 				$("#thirdsectotalCredit").append(dashboard.thirdtotalCreditAmt);
		 				$("#thirdtotalFiles").append(dashboard.thirdtotalFiles);
		 				
		 				var monthnames=['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

		 				
		 				var fdate=dashboard.firstdate;	
 						var dateTimeSplit = fdate.split(' ');
                        var dateSplit = dateTimeSplit[0].split('-');
                        if(dateSplit[0]<10){
                        	var fmonth=dateSplit[0].slice(1,2);
                        	
                        	var firstmonth=monthnames[fmonth-1];
                        }
                        if(dateSplit[0]>10){
                        
                        	var firstmonth=monthnames[dateSplit[0]];
                        }
		 		
		 			   
		 			
		
		 				 $("#current_date").text(dateSplit[1]);
		 				 $("#current_month").text(firstmonth);
		 				 $("#current_year").text(dateSplit[2]);
		 				
		 			var seconddate=dashboard.secdate;	
		 		
		 			var secdateTimeSplit = seconddate.split(' ');
                    var secdateSplit = secdateTimeSplit[0].split('-');
	 				
	 		       if(secdateSplit[0]<10){
                   	var smonth=secdateSplit[0].slice(1,2);
                   	var secmonth=monthnames[smonth-1];
                   }
                   if(secdateSplit[0]>10){
                   
                   	var secmonth=monthnames[secdateSplit[0]];
                   }
		 		
		 			
		 			   
		 				
		 				 $("#sec_date").text(secdateSplit[1]);
		 				 $("#sec_month").text(secmonth);
		 				 $("#sec_year").text(secdateSplit[2]);
		 			
		 				 
		 			var datethird=dashboard.thirddate;	

		 			var thirddateTimeSplit = datethird.split(' ');
                    var thirddateSplit = thirddateTimeSplit[0].split('-');
	 			
	 				 if(thirddateSplit[0]<10){
	                    	var tmonth=thirddateSplit[0].slice(1,2);
	                    	var thirdmonth=monthnames[tmonth-1];
	                    }
	                    if(thirddateSplit[0]>10){
	                    
	                    	var thirdmonth=monthnames[thirddateSplit[0]];
	                    }
		 			
		 			
		 				
		 				 $("#third_date").text(thirddateSplit[1]);
		 				 $("#third_month").text(thirdmonth);
		 				 $("#third_year").text(thirddateSplit[2]); 
 
				}
				
			}); 


    	});


function initialize_owl(el) {
    el.owlCarousel({
					autoplay: false,
		    			items: 3,
		    			slideBy: 1,
		    			loop: $('el').children().length > 1 ? true : false,
		    			infinite: false,
		    			smartSpeed: 900,
		    			nav: true,
		    			navText: [
		    			  "<i class='fa fa-angle-left'></i>",
		    			  "<i class='fa fa-angle-right'></i>"
		    			],

		    			dots: false
    });
}

$(function () {

    $("#branchname").change(function () {

		var	branchId = $(this).val();
		data=
		{
			
			userid:userId,
		
	}
			
    $.ajax({
       	 method: 'get',
            url: '../bankwise/fileprocess/'+branchId,
            contentType: 'application/json',
        	data: data,
			 dataType:'JSON',
			 crossDomain:'true',
            success: function(response) {

           	 
				var dashboard = response.dashboardResponse;
				
				var fileprocess='';
	 		
				
if(response.Filelist!==''){
	
	var firstsrc =response.dashboardResponse.firstdate;
 
		$("#firstdate").text(firstsrc);
		$("#firstfilecount").text(response.dashboardResponse.totalFiles);	
  	$("#firstbatchAmt").text(response.dashboardResponse.totalBatchamt);
  	$("#firstbatchCount").text(response.dashboardResponse.totalBatches);
  	$("#firsttranscount").text(response.dashboardResponse.totalTransactions);
  	$("#firstitemcount").text(response.dashboardResponse.totalItemcount);
		

 	var secsrc =response.dashboardResponse.secdate;
    

	$("#secondate").text(secsrc);
		$("#secfilecount").text(response.dashboardResponse.sectotalFiles);
	
  	$("#secbatchAmt").text(response.dashboardResponse.sectotalCreditAmt);
  	$("#secbatchCount").text(response.dashboardResponse.sectotalBatches);
  	$("#sectranscount").text(response.dashboardResponse.sectotalTransactions);
  	$("#secitemcount").text(response.dashboardResponse.secitemcount);
  	
  	var thirdsrc =response.dashboardResponse.thirddate;

	
		$("#thirddate").text(thirdsrc);
		$("#thirdfilecount").text(response.dashboardResponse.thirdtotalFiles);
		                       
  	$("#thirdbatchAmt").text(response.dashboardResponse.thirdtotalCreditAmt);
  	$("#thirdbatchCount").text(response.dashboardResponse.thirdtotalBatches);
  	$("#thirdtranscount").text(response.dashboardResponse.thirdtotalTransactions);
  	$("#thirditemcount").text(response.dashboardResponse.thirditemcount);		 			

		
} 
           	       
            }
     }); 
    });
});

</script>



<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
