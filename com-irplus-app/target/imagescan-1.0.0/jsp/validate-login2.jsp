<!DOCTYPE html>
<html lang="en">
<jsp:include page="includes/style.jsp" />
<body class="page-md">
<!-- BEGIN HEADER -->
	<div class="page-header">
		
	</div>
	<!-- END HEADER -->
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->

	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container">
			
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row margin-top-10">
				<div class="col-md-12">
					
					<div class="profile-content fileupload-container">
						<div class="row">
							
								<div class="portlet-body" style=" margin: 10% auto;">
								
								<div class="col-md-6 col-md-offset-3" id="license-validate">
									 
									  <div class="licensemodal-leftwrap">
										<div class="medium-title">
										  Activate with License File
										</div>
									  </div>
									  <div class="license-rightwrap">
										<form role="form" id="fileForm" class="form-horizontal" action="javascript:uploadLicense()">
											<div class="">
											<div class="medium-title">Upload License File</div>
											  <input id="bank-img-upload" name="licFile" type="file" multiple class="file-loading">
											</div>
										</form>
									  </div>


									</div>
								
								
									
									<div class="col-md-6 col-md-offset-3">
										<div id="license-success" class="success-bg" style="display:none">
											<i class="fa fa-check-circle"></i>
											<h1>License is valid, Now you can login & access with IR+</h1>
											<a href="login.jsp">Login</a>
										</div>
									</div>
								</div>
							
						</div>
					</div>
					<!-- END PROFILE CONTENT -->
				</div>
			</div>
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER -->
<!-- BEGIN FOOTER -->

<jsp:include page="includes/footer-js.jsp" />



















<script>

var isLicFile = function(name) {
    return name.match(/lic$/i)
};


	function uploadLicense()
	{
		//alert("uploading License file");
		//var file =  $("#bank-img-upload");
	    var filename = $.trim($("#bank-img-upload").val());
		//;/alert("filename: "+JSON.stringify(new FormData(document.getElementById("fileForm"))));

        if (!(isLicFile(filename) )) {
            alert('Please browse a lic file to upload ...');
            return;
        }

        $.ajax({
            url: '../license/upload',
            type: "POST",
            data: new FormData(document.getElementById("fileForm")),
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            success: function(data) {
              //alert("file uploaded");
             // $("#license-modal").modal().fadeOut();
            //  $(".modal-backdrop").fadeOut();
			 $("#license-validate").hide();
			  $("#license-success").show();

			  
			},
           error:function(response,statusTxt,error){

			//alert("Failed to add record :"+response.responseJSON.statusMsg);

			swal({title: "Oops...",text: response.responseJSON.statusMsg,type: "error"},function(){});

				 }
          });
		}
	$(window).load(function(){
//		alert("page loaded");
		$("#license-modal").modal().fadeIn();
	});
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
