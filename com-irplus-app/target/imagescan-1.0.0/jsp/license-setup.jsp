<!DOCTYPE html>
<html lang="en">
<jsp:include page="includes/style.jsp" />
<body class="page-md">
	<!-- BEGIN HEADER -->
	<div class="page-header">
		<!-- BEGIN HEADER TOP -->
		<jsp:include page="includes/header-top.jsp" />
		<!-- END HEADER TOP -->
		<!-- BEGIN HEADER MENU -->
		<jsp:include page="includes/header-menu.jsp" />
		<!-- END HEADER MENU -->
	</div>
	<!-- END HEADER -->
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<div class="container">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1>Licence Setup</h1>
			</div>
			<!-- END PAGE TITLE -->
		</div>
	</div>
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container">
			<!-- BEGIN PAGE BREADCRUMB -->
				<div class="col-md-12 col-sm-12 col-xs-12 form-group nopad">
					<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-12">
							<ul class="page-breadcrumb breadcrumb">
								<li>
									<a href="#">Home</a><i class="fa fa-circle"></i>
								</li>
								<li class="active">
									Licence Setup
								</li>
							</ul>
						</div>
					</div>
				</div>
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row margin-top-10">
				<div class="col-md-12">
					<!-- BEGIN PROFILE CONTENT -->
						<div class="profile-content">
							<div class="row">
								<div class="col-md-12">
									<!-- BEGIN LICENCE FORM PORTLET-->
										<div class="portlet light col-md-12 col-sm-12 col-xs-12">
											<div class="portlet-body form">
												<form class="col-sm-12 nopad license-form" id="jvalidate" method="post" >
													<fieldset class="col-md-4 col-sm-6 col-xs-12">
														<label>Site ID</label>
														<input type="text" class="form-control" id="site-id" name="site-id"/>
													</fieldset>
													<fieldset class="col-md-4 col-sm-6 col-xs-12">
														
															<label>Site Name <sup class="red-req">*</sup></label>
															<input id="site-name" name="site-name" type="text" class="form-control" required />
														
													</fieldset>
													<fieldset class="col-md-4 col-sm-6 col-xs-12">
														
															<label>Contact Person <sup class="red-req">*</sup></label>
															<input id="cont-person" name="cont-person" type="text" class="form-control" required />
														
													</fieldset>
													<fieldset class="col-md-4 col-sm-6 col-xs-12">
														
															<label>Contact No</label>
															<input type="text" class="form-control"  id="cont-num" name="cont-num"/>
														
													</fieldset>
													<fieldset class="col-md-8 col-sm-6 col-xs-12">
														
															<label>Site Address</label>
															<input type="text" class="form-control"  id="site-addre" name="site-addre"/>
														
													</fieldset>
													<div class="col-md-12 col-sm-12 col-xs-12 nopad">
														<div class="col-md-5 col-sm-6 col-xs-12">
															<div class="form-body">
																<div class="col-md-12 col-sm-12 col-xs-12 nopad">
																	<h3 class="medium-title">Licensing Type  <sup class="red-req">*</sup> <i class="fa fa-info-circle info-head"></i></h3>
																</div>
																<div class="form-group col-sm-12 nopad">
																	<select class="select2" id="license-select" required>	
																		<option value="">Select</option>
																		<option value="1">Perpetual Licence</option>
																		<option value="2">Validity Based</option>
																	</select>
																</div>
																<div class="form-group" id="val-period-div" style="display:none;">
																	<label class="col-md-3 control-label nopad">
																	<b>Validity Period :</b>
																	</label>
																		<div class="col-md-9 nopad">
																			<div class="col-md-6">
																				<div class="form-group">
																					<input type="text" class="form-control" id="form_control_1" placeholder="From">
																				</div>
																			</div>
																			<div class="col-md-6">
																				<div class="form-group">
																					<input type="text" class="form-control" id="form_control_1" placeholder="To">
																				</div>
																			</div>
																		</div>
																</div>
															</div>
														</div>
														<div class="col-md-6 col-sm-6 col-xs-12 ">
															<div class="form-body">
																<div class="col-md-12 col-sm-12 col-xs-12 nopad">
																	<h3 class="medium-title">Licensing Functioning <i class="fa fa-info-circle info-head"></i></h3>
																</div>
																<div class="form-group">
																	<label class="col-md-4 control-label nopad">
																	File Processing : <sup class="red-req">*</sup></label>
																	<div class="col-md-8">
																		<div class="input-group">
																			<div class="icheck-list">
																				<label>
																					<input type="checkbox" required class="icheck" data-checkbox="icheckbox_square-grey" name="checkbox-1" id="checkbox-1"> ACH 
																				</label>
																				<label>
																					<input type="checkbox" required class="icheck" data-checkbox="icheckbox_square-grey" name="checkbox-2" id="checkbox-2"> Wire 
																				</label>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="form-group">
																	<label class="col-md-4 control-label nopad">Business Processing :</label>
																	<div class="col-md-8">
																		<div class="input-group">
																			<div class="icheck-list">
																				<label>
																				<input type="checkbox" class="icheck" data-checkbox="icheckbox_square-grey"> Data Entry </label>
																				<label>
																				<input type="checkbox" checked class="icheck" data-checkbox="icheckbox_square-grey"> Exception </label>
																				
																			</div>
																		</div>
																	</div>
																</div>
																<div class="form-group">
																	<label class="col-md-4 control-label nopad">
																	Sub Entities :</label>
																	<div class="col-md-8">
																		<div class="input-group">
																			<div class="icheck-list">
																				<label>
																				<input type="checkbox" class="icheck" data-checkbox="icheckbox_square-grey"> </label>
																				<label>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div class="col-md-12 col-sm-12 col-xs-12 nopad">
															<div class="form-actions noborder text-right">
																<button type="button" class="btn default" >Cancel</button>
																<input type="submit" class="btn blue" 
																onclick="validfn();" value="Submit"/>
															</div>
														</div>
												</form>
											</div>
										</div>
									</div>	
								</div>
							</div>	
						</div>
					</div>
					
					
					<!-- END SAMPLE FORM PORTLET-->
					
				</div>
							
						</div>
						
					</div>
					<!-- END PROFILE CONTENT -->
				</div>
			</div>
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER -->
<!-- BEGIN FOOTER -->
<jsp:include page="includes/footer.jsp" />
<jsp:include page="includes/footer-js.jsp" />
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>