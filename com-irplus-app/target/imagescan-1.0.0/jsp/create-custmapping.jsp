<!DOCTYPE html>
<html lang="en">
<jsp:include page="includes/style.jsp"/>
<body class="page-md">
<!-- BEGIN HEADER -->
	<div class="page-header">
		<!-- BEGIN HEADER TOP -->
		<jsp:include page="includes/header-top.jsp" />
		<!-- END HEADER TOP -->
		<!-- BEGIN HEADER MENU -->
		<jsp:include page="includes/header-menu.jsp" />
		<!-- END HEADER MENU -->
	</div>
<!-- END HEADER -->
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<div class="container">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1> Create Customer Mapping</h1>
			</div>
			<!-- END PAGE TITLE -->
			
		</div>
	</div>
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container">
			
			<!-- BEGIN PAGE BREADCRUMB -->
			<div class="col-md-12 col-sm-12 col-xs-12 form-group nopad">
				<div class="row">
					<div class="col-md-6 col-sm-6 col-xs-12">
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<a href="#">Home</a><i class="fa fa-circle"></i>
						</li>
						
						<li class="active">
							 Create Customer Mapping
						</li>
					</ul>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12 text-right">
							<a href="manage-custmapping.jsp" class="btn blue">
								<i class="fa fa-reply"></i> 
							Back 
						</a>
					</div>
				</div>
			</div>
			
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row margin-top-10">
				<div class="col-md-12">
					
					<!-- BEGIN PROFILE CONTENT -->
					<div class="profile-content">
						<div class="row">
							<div class="col-md-12">
				
					
					<div class="portlet light col-md-12 col-sm-12 col-xs-12">
						<div class="portlet-body">
			<form role="form" class="form-horizontal" name="submit_form" id="submit_form" method="POST">
										
								<div class="col-md-12 col-sm-12 col-xs-12 nopad">
								<div class="table-scrollable table-scrollable-borderless managebank-cont custmap-tble">
								<table class="table table-hover table-bordered tble-custmap">
								<thead>
								<tr class="uppercase">
									<th>
										Reference Data
									</th>
									<th>
										 NATCHA Fields
										
									</th>
									<th> 
										Bank
									</th>
									<th> 
										Customer ID
									</th>
									<th>
										Options
									</th>
								</tr>
								</thead>
								<tbody>
								<tr>
							
									<!-- <td width="20%">
										<input type="text" class="form-control reference_data0" id="0" name="reference_data0"/> 
									</td> -->
									<td width="20%">
										<input type="text" class="form-control reference_data0" id="0" name="reference_data0"/> 
									</td>
									
									 	<td width="20%">
										<select class="form-control select2 natchafield0" id="0" name="natchafield0">
											<option value="">select</option>
											<option value="1">Receiving DFI ID</option>
											<option value="2">DFI Account ID</option>
											<option value="3">Individual Account ID</option>
											<option value="4">Individual Account Name</option>
											<option value="5">Trace No</option>
										</select> 
									</td> 
									<td width="20%">
										 <select class="form-control select2 branchname0" id="0"  name="branchname0">
											
										</select> 
			
										
									</td>
									<td width="20%">
								 <select class="form-control select2 customername0" id="0" name="customername0">
											
										</select> 
									</td>
									<td width="20%">
										<a class="btn btn-info add-custmap"><i class="fa fa-plus"></i> Add</a>
									</td>	
									
								</tr>
							</tbody>
								</table>
							</div>
								</div>
									
								<div class="col-md-12 col-sm-12 col-xs-12 nopad">
								<div class="form-actions noborder text-right">
					

 	 <button type="button"  onclick="cancelMapping()" class="btn default" >Cancel</button>
 	<a href="javascript:submitCustMappingForm()" class="btn blue button-submit">Create</a>
									
								</div>
								</div>
							</form>
							
						</div>
					</div>
					<!-- END SAMPLE FORM PORTLET-->
					
				</div>
							
						</div>
						
					</div>
					<!-- END PROFILE CONTENT -->
				</div>
			</div>
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>
<script type="text/javascript">
	var siteId=<%=session.getAttribute("siteId")%>;
	var userId=<%=session.getAttribute("userid")%>;
	
	$(document).ready(function() {  
		
		$.ajax({
			
				method:'get',
				
				
				url:'../Customermapping/branch/'+siteId,
				contentType:'application/json',
				dataType:'JSON',
				crossDomain:'true',
				success:function(response)
{
				
	 				var htmlbankid="";
	 				var htmlbankname= "<option value='' selected disabled>Select Bank</option>";
	 				var branch =response.dashboardResponse.bankInfo.bankbranchinfo;
	 				for(var i=0;i<response.dashboardResponse.bankInfo.length;i++){
	 					
	 					
	 		
	 					htmlbankname +="<OPTGROUP LABEL="+response.dashboardResponse.bankInfo[i].bankName+">";   
	 				
	 					for(var j=0;j<response.dashboardResponse.bankInfo[i].bankbranchinfo.length;j++){
	 				 		htmlbankname +="<OPTION VALUE="+response.dashboardResponse.bankInfo[i].bankbranchinfo[j].branchId+">"+response.dashboardResponse.bankInfo[i].bankbranchinfo[j].branchLocation+"</OPTION>"; 
	 					
	 				 	
	 					
	 					}
	 					

	 					$(".branchname0").html(htmlbankname);	
	 			
	 				}
	 	
		},
		error:function(response,statusTxt,error){
			 
		/* 	alert("Failed to add record :"+response.responseJSON); */
		 }
		});
	
	
	
		
		$(function () {
		     var branchId = $(this).val();
		     $(".branchname0").change(function () {
					var selectedText = $(this).find("option:selected").text();
					branchId = $(this).val();
				//	alert("branchId"+branchId);
		      $.ajax({
		        	 method: 'get',
		             url: '../customer/getcustomer/'+branchId,
		             contentType: 'application/json',
					 dataType:'JSON',
					 crossDomain:'true',
		             success: function(response) {
		               
		             
		
		                	var listItems= "<option value='' selected disabled>Select Customer</option>";
							
							for (var i = 0; i < response.bankBranches.bankCustomerList.length; i++){
							//alert(response.roleBeans[i].rolename)
							  listItems += "<option value='" + response.bankBranches.bankCustomerList[i].customerid + "'>" + response.bankBranches.bankCustomerList[i].companyName + "</option>";
							}
							$(".customername0").html('');
							$(".customername0").html(listItems);
		                	
		             },
		             error: function (e) {
		                 //called when there is an error
		                 console.log(e.message);
		                /*  alert("failed to get bank customer"); */
		             }
		         });
		     });
		 });
		});
	
	
	function changeFunc() {
  
		  var rowid  = $(this).attr("id");
		 
		      var branchId  = $(".branchname"+rowid).val();
		  
		      $.ajax({
		        	 method: 'get',
		             url: '../customer/getcustomer/'+branchId,
		             contentType: 'application/json',
					 dataType:'JSON',
					 crossDomain:'true',
		             success: function(response) {
		               
		             
		
		                	var appendcustomername= "<option value='' selected disabled>Select Customer</option>";
							
							for (var i = 0; i < response.bankBranches.bankCustomerList.length; i++){
							
							  appendcustomername += "<option value='" + response.bankBranches.bankCustomerList[i].customerid + "'>" + response.bankBranches.bankCustomerList[i].companyName + "</option>";
							}
			
							$(".customername"+rowid).html(appendcustomername);
			
		                	
		             },
		             error: function (e) {
		                
		                 console.log(e.message);
		               /*   alert("failed to get bank customer"); */
		             }
		         });
	}
	function submitCustMappingForm()
	{

		var numItems = $('.tble-custmap tbody tr').size();

		var url_data = ($("#submit_form").serialize());//.replace(/+/g,'%20');//replace(/ /g, '+');
		
		var result = queryStringToJSON(url_data);

		var CustomerMapping = [];

	
/* 	 CustomerMapping[0]=
		{
			 
			 referenceData:result["reference_data0"],
			 natchafld:result["natcha_fields0"],
			 bankbranchid:result["branchname0"],
			 customersId:result["customername0"],
			 usersid:userId,
				 isactive:'1'
		} */
	

		 for(var k=0;k<numItems;k++)
			{
			   
			 CustomerMapping[k]=
				{
					 natchafld:result["natchafield"+k],
					 referenceData:result["reference_data"+k],
				
					 bankbranchid:result["branchname"+k],
					 customersId:result["customername"+k],
					 usersid:userId,
						 isactive:'1'
				}
		
			 
			}	 
		
			 
 $.ajax({
			method:'post',
			url:'../customer/mapping/Fields',
			data:JSON.stringify(CustomerMapping),
			contentType:'application/json',
			dataType:'JSON',
			crossDomain:'true',
			success: function(response) 
			{
				swal({title:"Done",text:"Fields mapped Successfully",type:"success"},function(){window.location="manage-custmapping.jsp";});
			},
			
		 error:function(response,statusTxt,error){
		 
		/* 	alert("Failed to add record :"+response.responseJSON.errMsgs); */
		 }
	});  
		
		
}
	
	 var queryStringToJSON = function (url) {
		    if (url === '')
		        return '';
		    var pairs = (url || location.search).slice(1).split('&');
		    var result = {};
		    for (var idx in pairs) {
		        var pair = pairs[idx].split('=');
		        if (!!pair[0])
		            result[pair[0].toLowerCase()] = decodeURIComponent(pair[1] || '');
		    }
		    return result;
		}
	 function cancelMapping(){
		 	
		 	//alert("Cancelled Page");
		 	location.href='manage-custmapping.jsp';
		 }
</script>


<!-- END PAGE CONTAINER -->
<!-- BEGIN FOOTER -->
<jsp:include page="includes/footer.jsp" />
<jsp:include page="includes/footer-js.jsp" />
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>