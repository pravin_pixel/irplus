<head>
<meta charset="utf-8"/>
<title>ImageScan</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description"/>
<meta content="" name="author"/>
<link href="../resources/assets/global/plugins/timepicker/jquery.ui.timepicker.css" rel="stylesheet" type="text/css">
<link href="../resources/assets/global/plugins/multiselect/bootstrap-multiselect.css" rel="stylesheet" type="text/css">
<link href="../resources/assets/global/plugins/mcustom/jquery.mCustomScrollbar.min.css" rel="stylesheet" type="text/css">
<link href="../resources/assets/global/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css">

<link href="../resources/assets/global/plugins/owl.carousel/owl.carousel.css" rel="stylesheet" type="text/css">
<link href="../resources/assets/global/plugins/owl.carousel/owl.theme.css" rel="stylesheet" type="text/css">
<link href="../resources/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="../resources/assets/global/plugins/bootstrap-fileinput/fileinput.min.css" rel="stylesheet" type="text/css">
<link href="../resources/assets/global/plugins/bootstrap-fileinput/theme.min.css" rel="stylesheet" type="text/css">
<link href="../resources/assets/global/plugins/select2/select2.css" rel="stylesheet" type="text/css">
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->


<link href="../resources/assets/admin/pages/css/profile.css" rel="stylesheet" type="text/css"/>
<link href="../resources/assets/admin/pages/css/tasks.css" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="../resources/assets/global/plugins/icheck/skins/all.css" rel="stylesheet"/>
<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN DATA TABLE STYLES -->
<link href="../resources/assets/global/plugins/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet"/>
<!-- END DATA TABLE STYLES -->
<link href="../resources/assets/admin/pages/css/login.css" rel="stylesheet" type="text/css"/>
<!-- BEGIN THEME STYLES -->
<link href="../resources/assets/global/css/components-md.css" id="style_components" rel="stylesheet" type="text/css">
<link href="../resources/assets/global/css/plugins-md.css" rel="stylesheet" type="text/css">
<link href="../resources/assets/admin/layout3/css/layout.css" rel="stylesheet" type="text/css">
<link href="../resources/assets/admin/layout3/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color">
<link href="../resources/assets/admin/layout3/css/custom.css" rel="stylesheet" type="text/css">
<link href="../resources/assets/global/plugins/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
<link href="../resources/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.css" rel="stylesheet" type="text/css">
<link href="../resources/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="../resources/assets/global/plugins/sweet-alert/css/sweetalert.css" rel="stylesheet" type="text/css">
<link href="../resources/assets/admin/layout3/img/favicon.ico" rel="shortcut icon" />
<!-- END THEME STYLES -->



<script src="../resources/assets/global/plugins/jquery.min.js" type="text/javascript"></script>


</head>