<!DOCTYPE html>

<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>IR plus | Module menu</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description"/>
<meta content="" name="author"/>

<link href="../resources/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="../resources/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">

<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="../resources/assets/admin/pages/css/profile.css" rel="stylesheet" type="text/css"/>
<link href="../resources/assets/admin/pages/css/tasks.css" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="../resources/assets/global/plugins/icheck/skins/all.css" rel="stylesheet"/>
<!-- END PAGE LEVEL SCRIPTS -->

<!-- BEGIN DATA TABLE STYLES -->
<link href="../resources/assets/global/plugins/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet"/>
<!-- END DATA TABLE STYLES -->

<!-- BEGIN THEME STYLES -->
<link href="../resources/assets/global/css/components-md.css" id="style_components" rel="stylesheet" type="text/css">
<link href="../resources/assets/global/css/plugins-md.css" rel="stylesheet" type="text/css">
<link href="../resources/assets/admin/layout3/css/layout.css" rel="stylesheet" type="text/css">
<link href="../resources/assets/admin/layout3/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color">
<link href="../resources/assets/admin/layout3/css/custom.css" rel="stylesheet" type="text/css">
<!-- END THEME STYLES -->
<link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<!-- DOC: Apply "page-header-menu-fixed" class to set the mega menu fixed  -->
<!-- DOC: Apply "page-header-top-fixed" class to set the top menu fixed  -->
<body class="page-md">
<!-- BEGIN HEADER -->
<div class="page-header">
	<!-- BEGIN HEADER TOP -->
	<div class="page-header-top">
		<div class="container">
			<!-- BEGIN LOGO -->
			<div class="page-logo">
				<a href="index.jsp">
				<span>
				<img src="../resources/assets/admin/layout3/img/logo-default.png" alt="logo" class="logo-default"></span>
				<span class="logo-sub">AB Processing Center</span>
				</a>
			</div>
			<!-- END LOGO -->
			<!-- BEGIN RESPONSIVE MENU TOGGLER -->
			<a href="javascript:;" class="menu-toggler"></a>
			<!-- END RESPONSIVE MENU TOGGLER -->
			<!-- BEGIN TOP NAVIGATION MENU -->
			<div class="top-menu">
				<ul class="nav navbar-nav pull-right">
					<!-- BEGIN NOTIFICATION DROPDOWN -->
					<li class="dropdown dropdown-extended dropdown-dark dropdown-notification" id="header_notification_bar">
						<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
							<i class="fa fa-bell-o"></i>
							<span class="badge badge-default">7</span>
						</a>
						<ul class="dropdown-menu">
							<li>
								<ul class="dropdown-menu-list scroller" style="height: 250px;" data-handle-color="#637283">
									<li>
										<a href="javascript:;">
											<span class="details">
												<span class="label label-sm label-icon label-warning">
												<i class="fa fa-bell-o"></i>
												</span>
												Notification 1 
											</span>
										</a>
									</li>
									<li>
										<a href="javascript:;">
										
										<span class="details">
										<span class="label label-sm label-icon label-warning">
										<i class="fa fa-bell-o"></i>
										</span>
										Notification 2 </span>
										</a>
									</li>
									<li>
										<a href="javascript:;">
										
										<span class="details">
										<span class="label label-sm label-icon label-warning">
										<i class="fa fa-bell-o"></i>
										</span>
										Notification 3 </span>
										</a>
									</li>
									<li>
										<a href="javascript:;">
										
										<span class="details">
										<span class="label label-sm label-icon label-warning">
										<i class="fa fa-bell-o"></i>
										</span>
										Notification 4 </span>
										</a>
									</li>
									<li>
										<a href="javascript:;">
											<span class="details">
												<span class="label label-sm label-icon label-warning">
												<i class="fa fa-bell-o"></i>
												</span>
												Notification 5 
											</span>
										</a>
									</li>
								</ul>
							</li>
						</ul>
					</li>
					<!-- END NOTIFICATION DROPDOWN -->
					
					<li class="droddown dropdown-separator">
						<span class="separator"></span>
					</li>
					
					<!-- BEGIN USER LOGIN DROPDOWN -->
					<li class="dropdown dropdown-user dropdown-dark">
						<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
						<img alt="" class="img-circle" src="../resources/assets/admin/layout3/img/avatar9.jpg">
						<span class="username username-hide-mobile">Nick</span>
						</a>
						<ul class="dropdown-menu dropdown-menu-default">
							<li>
								<a href="profile.jsp">
								<i class="icon-user"></i> My Profile </a>
							</li>
							
							<li>
								<a href="login.jsp">
								<i class="icon-key"></i> Log Out </a>
							</li>
						</ul>
					</li>
					<!-- END USER LOGIN DROPDOWN -->
				</ul>
			</div>
			<!-- END TOP NAVIGATION MENU -->
		</div>
	</div>
	<!-- END HEADER TOP -->
	<!-- BEGIN HEADER MENU -->
	<div class="page-header-menu">
		<div class="container">
			<div class="hor-menu ">
				<ul class="nav navbar-nav">
					<li>
						<a href="index.jsp">Dashboard</a>
					</li>
					<li class="menu-dropdown classic-menu-dropdown ">
						<a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:void(0);">
						Admin 
						</a>
						<ul class="dropdown-menu pull-left">
							<li>
								<a href="manage-menu.jsp">Manage Menu</a>
							</li>
							<li>
								<a href="manage-module.jsp">Manage Module</a>
							</li>
							<li>
								<a href="module-menu.jsp">Module Menu</a>
							</li>
							<li>
								<a href="role-setup.jsp">Add Role </a>
							</li>
							<li>
								<a href="manage-roleperm.jsp">Manage Role </a>
							</li>
							<li>
								<a href="profile.jsp">Add User </a>
							</li>
							<li>
								<a href="manage-user.jsp">Manage Users </a>
							</li>
						</ul>
					</li>
					<li class="menu-dropdown classic-menu-dropdown ">
						<a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;">
						Bank 
						</a>
						<ul class="dropdown-menu pull-left">
							<li>
								<a href="bank-setup.jsp">
								
								Bank Setup </a>
								
							</li>
							<li>
								<a href="manage-bank.jsp">
								
								Manage Bank </a>
								
							</li>
							<li>
								<a href="manage-bank-location.jsp">
								
								Manage Location </a>
								
							</li>
							
						</ul>
					</li>
					<li class="menu-dropdown classic-menu-dropdown ">
						<a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;">
						Customer 
						</a>
						<ul class="dropdown-menu pull-left">
							<li>
								<a href="customer-setup.jsp">
								
								Create Customer </a>
								
							</li>
							<li>
								<a href="manage-customer.jsp">
								
								Manage Customer </a>
								
							</li>
							<li>
								<a href="create-group.jsp">
								
								Create Customer Group</a>
								
							</li>
							<li>
								<a href="manage-custgroup.jsp">
								
								Manage Customer Group</a>
								
							</li>
							
						</ul>
					</li>
					<li class="menu-dropdown classic-menu-dropdown ">
						<a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;">
						Settings 
						</a>
						<ul class="dropdown-menu pull-left">
							<li>
								<a href="license-setup.jsp">
								
								License Setup </a>
								
							</li>
							<li>
								<a href="file-upload.jsp">
								
								Validate License</a>
								
							</li>
						
							
						</ul>
					</li>
					<li>
						<a href="#">File Process</a>
					</li>
				</ul>
			</div>
			<!-- END MEGA MENU -->
		</div>
	</div>
	<!-- END HEADER MENU -->
</div>
<!-- END HEADER -->
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<div class="container">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1>Module menu</h1>
			</div>
			<!-- END PAGE TITLE -->
			
		</div>
	</div>
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container">
			<!-- BEGIN PAGE BREADCRUMB -->
		
				<div class="col-md-12 col-sm-12 col-xs-12 form-group nopad">
			<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
				<ul class="page-breadcrumb breadcrumb">
					<li>
						<a href="#">Home</a><i class="fa fa-circle"></i>
					</li>
					<li class="active">
						Module menu
					</li>
				</ul>
			</div>
			
			</div>
			</div>
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row margin-top-10">
				<div class="col-md-12">
					
					<!-- BEGIN PROFILE CONTENT -->
					<div class="profile-content">
						<div class="row">
							<div class="col-md-12">
								<div class="portlet light col-md-12 col-sm-12 col-xs-12">
									<div class="portlet-body">
										<div class="table-scrollable table-scrollable-borderless">
											<table class="table">
												<thead>
													<tr class="uppercase">
														<th>
															 Menu Name
														</th>
														<th>
															 Status
														</th>
														<th>
															option
														</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>
															Admin Settings
														</td>
														<td>
															<button type="button" class="btn green btn-xs">active</button>
														</td>
														<td>
															<a class="edit-btn" data-toggle="tooltip" data-placement="right" title="Edit!">
																<i class="fa fa-edit"></i>
															</a>
															<a class="edit-btn" data-toggle="tooltip" data-placement="right" title="Edit!">
																<i class="fa fa-trash"></i>
															</a>
														</td>
													</tr>	
													<tr>
														<td>
															Masters
														</td>
														<td>
															<button type="button" class="btn red btn-xs">In active</button>
														</td>
														<td>
															<a class="edit-btn" data-toggle="tooltip" data-placement="right" title="Edit!">
																<i class="fa fa-edit"></i>
															</a>
															<a class="edit-btn" data-toggle="tooltip" data-placement="right" title="Edit!">
																<i class="fa fa-trash"></i>
															</a>
														</td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
					<!-- END SAMPLE FORM PORTLET-->
					
				</div>
							
						</div>
						
					</div>
					<!-- END PROFILE CONTENT -->
				</div>
			</div>
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="page-footer">
	<div class="container">
		 Image Scan &copy; 2017 ALL RIGHTS RESERVED.
	</div>
</div>
<div class="scroll-to-top">
	<i class="icon-arrow-up"></i>
</div>

<script src="../resources/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="../resources/assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="../resources/assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="../resources/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../resources/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="../resources/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="../resources/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="../resources/assets/global/plugins/icheck/icheck.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN DATA TABLE PLUGINS -->
<script src="../resources/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<!-- END DATA TABLE PLUGINS -->

<script src="../resources/assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="../resources/assets/admin/layout3/scripts/layout.js" type="text/javascript"></script>
<script src="../resources/assets/admin/layout3/scripts/demo.js" type="text/javascript"></script>

<!-- END PAGE LEVEL SCRIPTS -->
<script>
$(document).ready(function()
{
	//alert("aaa");
    $('[data-toggle="tooltip"]').tooltip();   
});
jQuery(document).ready(function() {       
   	// initiate layout and plugins
   	Metronic.init(); // init metronic core components
	Layout.init(); // init current layout
	Demo.init(); // init demo features\
	 $('.table').DataTable();
});
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>