<!DOCTYPE html>
<html lang="en">
<jsp:include page="includes/style.jsp"/>
<body class="page-md">
<!-- BEGIN HEADER -->
	<div class="page-header">
		<!-- BEGIN HEADER TOP -->
		<jsp:include page="includes/header-top.jsp" />
		<!-- END HEADER TOP -->
		<!-- BEGIN HEADER MENU -->
		<jsp:include page="includes/header-menu.jsp" />
		<!-- END HEADER MENU -->
	</div>
<!-- END HEADER -->
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
			<div class="container-fluid">
			
			<div class="col-md-6 col-sm-6 col-xs-12 text-right">
			<div class="fixedbtn-container">
				
		 	<a href="add-dataentry-form.jsp?customerId=<%=request.getParameter("customerId")%>" class="btn orange m-t-15"><i class="fa fa-plus"></i> Add data entry form</a> 
			</div>
			</div>
			<!-- END PAGE TITLE -->

		</div>
	</div>
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container">
			<!-- BEGIN PAGE BREADCRUMB -->
			<div class="col-md-12 col-sm-12 col-xs-12 form-group nopad">
			<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
			<!-- <ul class="page-breadcrumb breadcrumb">
				<li>
					<a href="index.jsp">Home</a><i class="fa fa-circle"></i>
				</li>
				<li class="active">
					 Manage Data Entry Forms
				</li>
			</ul> -->
			</div>
			
			</div>
			</div>
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row margin-top-10">
				<div class="col-md-12">
					<!-- BEGIN PROFILE SIDEBAR -->
			
					<!-- END BEGIN PROFILE SIDEBAR -->
					<!-- BEGIN PROFILE CONTENT -->
					<div class="profile-content">
						<div class="">
					<div class="col-md-12 nopad">
					<div class="portlet light portlet-border">
						<div class="pagestick-title">
								<span>Manage Forms</span>
							</div>
							<div class="pagestick-left">
								<span id ="customerName"></span>
							</div>
					
						<div class="portlet-body">
						
						<div class="col-md-12 col-sm-12 col-xs-12 nopad">
							<div class="table-scrollable table-scrollable-borderless">
								<table id="manage-form-id" class="table">
								<thead>
							
								<tr class="uppercase">
									<th >
										ID
									</th>
									<th>
										Name
									</th>
									<th>
										 Batch Class
									</th>
									<th>
										Watch Folder
									</th>
									<th>No. of Fields</th>
									<th>Default</th>
									<th>Created On</th>
									<th>Options</th>
								</tr>
							
								</thead>
								<tbody>
								 
								</tbody>
								</table>
							</div>
								</div>
									
							
					
							
						</div>
					</div>
					<!-- END SAMPLE FORM PORTLET-->
					
				</div>
							
						</div>
						
					</div>
					<!-- END PROFILE CONTENT -->
				</div>
			</div>
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER -->
<!-- BEGIN FOOTER -->
<jsp:include page="includes/footer.jsp" />
<jsp:include page="includes/footer-js.jsp" />
<!-- END JAVASCRIPTS -->
<script type="text/javascript">
	var siteId=<%=session.getAttribute("siteId")%>;
	var userId=<%=session.getAttribute("userid")%>;
	var customerid = <%=request.getParameter("customerId") %>;
	
	var bankId=<%=request.getParameter("bankId") %>;
	//getting all the active banks on pageload
	
	$(document).ready(function() {  
	
		

		$.fn.dataTable.ext.errMode = 'throw'
	            $("#manage-form-id").dataTable({searching: false,lengthChange:false,pageLength:50,
				ajax :{
				"url":"../manageForms/customer/manageform/showAll/"+customerid,
				"dataSrc":"formMgntList",
				
					 dataFilter: function(response){
				            var temp = JSON.parse(response);
				            $("#customerName").append(temp.formMgntList[0].companyName);
				            


				            return response;

				        }
				
				
				
				
				//	$("#bank-name").append(customerBean[0].defaultBankName);
			  		},
	                //"aaData": arreglo,
	                "ordering":true,
	                "aoColumns": [
	                	
	                	{ "mData":"formCode"},
	                	{ "mData":"formName"},
	                	{ "mData":"batchClassDataentry"},
	                	{ "mData":"watchFolderDataentry"},
	                	{ "mData":"totalfields"},
	                	{ "mData":"formCode"},
	                	{ "mData":"createddate"},
	                
	                
	                	{
							data: null, render: function (data, type, row )
							{
								
								
							// alert("data.moduleid"+data.moduleid);
								var urlval = 'customer/delete/'+customerid;
								return  '<form action="customer-setup.jsp" method="post"><input type="hidden" name="customerId" value="'+customerid+'"/><button type="submit" class="btn btn-info btn-xs"><i class="fa fa-edit"></i></button></form>'
				//<a href="#" class="delete-btn btn red btn-xs" data-toggle="tooltip" data-placement="right" title="Delete!" onclick="funStats(\''+urlval+'\',1)"><i class="fa fa-trash-o"></i></a>
							}

						},
						
						
	                
	                            ],
	                "bDestroy": true
	            }).fnDraw();
	});
			
		
			
	
			
		
		

	</script>
	
</body>
<!-- END BODY -->
</html>