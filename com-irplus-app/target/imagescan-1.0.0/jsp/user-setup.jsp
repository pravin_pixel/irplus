<!DOCTYPE html>
<html lang="en">
<jsp:include page="includes/style.jsp" />
<body class="page-md">
<!-- BEGIN HEADER -->
	<div class="page-header">
		<!-- BEGIN HEADER TOP -->
		<jsp:include page="includes/header-top.jsp" />
		<!-- END HEADER TOP -->
		<!-- BEGIN HEADER MENU -->
		<jsp:include page="includes/header-menu.jsp" />
		<!-- END HEADER MENU -->
	</div>
<!-- END HEADER -->
<!-- BEGIN PAGE CONTAINER -->

<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<div class="container">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1>User Profile </h1>
			</div>
			<!-- END PAGE TITLE -->
			
		</div>
	</div>
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container">
			<!-- BEGIN PAGE BREADCRUMB -->
			<div class="col-md-12 col-sm-12 col-xs-12 form-group nopad">
				<div class="row">
					<div class="col-md-6 col-sm-6 col-xs-12">
						<ul class="page-breadcrumb breadcrumb">
							<li>
								<a href="#">Home</a><i class="fa fa-circle"></i>
							</li>
							<li class="active">
								 Admin User Profile
							</li>
						</ul>
					</div>
				</div>
			</div>		
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row margin-top-10">
				<div class="col-md-12">
					<!-- BEGIN PROFILE SIDEBAR -->
					<div class="profile-sidebar">
						<!-- PORTLET MAIN -->
						<div class="portlet light profile-sidebar-portlet">
							<!-- SIDEBAR USERPIC -->
							<div class="profile-userpic">
								
								<span class="userpic-inner">
									<img src="../resources/assets/admin/layout3/img/avatar9.jpg" id="usr-logo" class="img-responsive" alt="" />
									
									<a class="edit-pic bank-img" data-toggle="modal" data-target="#imgupload-prof">
										<i class="fa fa-edit"></i>
									</a>
								
								</span>
							</div>
							<!-- END SIDEBAR USERPIC -->
							<!-- SIDEBAR USER TITLE -->
							<div class="profile-usertitle">
								<div class="profile-usertitle-name">
									
								</div>
								<div class="irplus-id">
								
								</div>
							</div>
							<!-- END SIDEBAR USER TITLE -->
							
							<!-- SIDEBAR MENU -->
							<div class="profile-usermenu">
								<ul class="nav">
								</ul>
							</div>
							<!-- END MENU -->
						</div>
						<!-- END PORTLET MAIN -->
						
					</div>
					<!-- END BEGIN PROFILE SIDEBAR -->
					<!-- BEGIN PROFILE CONTENT -->
					<div class="profile-content boxwith-sidebar">
						<div class="row">
							<div class="col-md-12">
					<!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet light col-md-12 col-sm-12 col-xs-12">
						<div class="portlet-body form">
							<form role="form" id="jvalidate" class="prof-form" name="userProfile">
								<input type="hidden" class="form-control" id="usr-userLogo" name="usr-userLogo" />
								<fieldset class="col-md-6 col-sm-6 col-xs-12">
									<label>First Name <sup class="red-req">*</sup></label>
									<input type="text" class="form-control" id="licence-fname" name="licence-fname" required />
                                    <input type="hidden" name="userid" id="userid" value="" />
								</fieldset>
								<fieldset class="col-md-6 col-sm-6 col-xs-12">
									<label>Last Name</label>
									<input type="text" class="form-control" id="licence-lname" name="licence-lname" />
								</fieldset>
								<fieldset class="col-md-6 col-sm-6 col-xs-12">
									<label>Role</label>
									<select class="form-control" id="roleId" name="roleId" required = "required">
										
									</select>
								</fieldset>
								<fieldset class="col-md-6 col-sm-6 col-xs-12">
									<label>Email</label>
									<input type="Email" class="form-control" id="licence-email" name="licence-email" />
								</fieldset>
								<fieldset class="col-md-6 col-sm-6 col-xs-12">
									<label>Contact No.<sup class="red-req">*</sup></label>
									<input type="text" class="form-control" id="ct-no" name="ct-no" required />
								</fieldset>
								<fieldset class="col-md-6 col-sm-6 col-xs-12">
									<label>Username <sup class="red-req">*</sup></label>
									<input type="text" class="form-control" id="licence-uname" name="licence-uname" required />
								</fieldset>
                                
								<fieldset class="col-md-6 col-sm-6 col-xs-12" id="tsthide1">
									<label>Password <sup class="red-req">*</sup></label>
									<input type="password" class="form-control" id="licence-pswd" name="licence-pswd" required />
								</fieldset>
								<fieldset class="col-md-6 col-sm-6 col-xs-12" id="tsthide2">
									<label>Confirm Password <sup class="red-req">*</sup></label>
									<input type="password" class="form-control" id="licence-cpswd" name="licence-cpswd" required />
								</fieldset>
                              
								<fieldset class="col-md-6 col-sm-6 col-xs-12">
									<label>Default Bank</label>
									<select class="form-control select2" id="def_bank">
										
									</select>
								</fieldset>
								<fieldset class="col-md-6 col-sm-6 col-xs-12 custom-multiselect">
									<label>Other Bank</label>
									<select class="form-control multiselect" multiple="multiple" id="other_bank">
										
									</select>
								</fieldset>
								
								<!--add status -->
									<div class="form-group">
									<div class="col-md-6 col-sm-6 col-xs-12">
										<label class="col-md-3 control-label p-t-10">Status :</label>
										<div class="col-md-9 ">
											<div class="input-group">
												<div class="icheck-list">
													<label>
														<input class="cls_status icheck" id="role-status" name="role-status" type="checkbox" data-checkbox="icheckbox_square-grey"> 
													</label>
												</div>
											</div>
										</div>
									</div>
									</div>
								<!---close  -->
								
								<div class="col-md-12 col-sm-12 col-xs-12 nopad">
								<div class="form-actions noborder text-right">
									<button type="button" class="btn default" onClick="cancl();">Cancel</button>
                                    <input type="submit" id="dv_save" class="btn blue" value="Create" onClick="createUser()"/>
								</div>
								</div>
							</form>
						</div>
					</div>
					<!-- END SAMPLE FORM PORTLET-->
					
				</div>
							
						</div>
						
					</div>
					<!-- END PROFILE CONTENT -->
				</div>
			</div>
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER -->

<!-- BEGIN FOOTER -->
<jsp:include page="includes/footer.jsp" />
<jsp:include page="includes/footer-js.jsp" />
<script src="../resources/assets/irplus/js/userLogoUpload.js"></script>


<div class="modal fade" id="imgupload-prof" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Choose a file to upload</h4>
        </div>
        <div class="modal-body">
			<form role="form" id="fileForm" class="form-horizontal" action="javascript:uploadUserLogo()">
								<div class="col-md-12 col-sm-12 col-xs-12">
								
								<!--<input id="bank-img-upload" name="bankLogo" type="file" class="file-loading">-->
								<!-- <input id="bank-img-upload" name="input6[]" type="file" multiple class="file-loading"> -->
								<input id="user-img-upload" name="userPhoto" type="file"  class="file-loading">
								</div>
			</form>
        </div>
        <div class="modal-footer">
          
        </div>
      </div>
      
    </div>
  </div>

</body>



<script>

     var userid_t = <%= request.getParameter("userid") %>;
	 var admin_id = <%= session.getAttribute("userid") %>;
	 var otherBank=[];
	 
//alert('user='+userid_t)
	 jQuery(document).ready(function() {   

	//	alert(admin_id);
	 
   	// initiate layout and plugins
   	Metronic.init(); // init metronic core components
	Layout.init(); // init current layout
	Demo.init(); // init demo features
	
	if(userid_t != null){
		$("#dv_update").show();
		$("#dv_save").hide();
	}else{
		$("#dv_update").hide();
		$("#dv_save").show();
	}	
	
	//var roleid_t = <%= request.getParameter("userCode") %>;
	//alert(roleid_t);
	
	function getAllRoles()
	{
	
			//$(".select2").select2();
	
		$.ajax({
			
				method:'get',
				url:'../user/show/all_roles_banks',
				contentType:'application/json',
				dataType:'JSON',
				crossDomain:'true',
				success:function(response)
				{
				//alert(response.oneUserBean.roleArrayList)
				//oneUserBean
					var roleInfo = JSON.stringify(response.userBean.roleArrayList);
					var listItems= "<option value=''>Select Role</option>";
					var jsonData = roleInfo;
					for (var i = 0; i < response.userBean.roleArrayList.length; i++){
					//alert(response.roleBeans[i].rolename)
					  listItems += "<option value='" + response.userBean.roleArrayList[i].roleid + "'>" + response.userBean.roleArrayList[i].rolename + "</option>";
					}
					
					var bankInfo = JSON.stringify(response.userBean.bankList);
					var listBnkItems= "<option value='' disabled hidden>Select Bank</option>";
					//var jsonData = bankInfo;
					for (var j = 0; j < response.userBean.bankList.length; j++)
					{
					//alert(response.roleBeans[i].rolename)
					  listBnkItems += "<option value='" + response.userBean.bankList[j].bankId + "'>" + response.userBean.bankList[j].bankName + "</option>";
					  
					  
					}
						
					
					//alert(listItems)
					//alert($("#roleId").innerHTML)
				$("#roleId").html('');
				$("#roleId").html(listItems);
				
								
				$("#def_bank").html('');
				$("#def_bank").html(listBnkItems);
				
				$("#other_bank").html('');
				$("#other_bank").html(listBnkItems);
				$("#other_bank").multiselect({
					 nonSelectedText:'select'
				});
				
				//$('.multiselect-container.dropdown-menu').slimScroll();
				$(".multiselect-container.dropdown-menu").mCustomScrollbar({
				theme:"dark"
				});
				
				 data_fetch();

				},
				error:function(response,statusTxt,error){
					 
				/* 		alert("Failed to add record :"+response.responseJSON); */
					 }
			});
			
			
	}// end of getAllRoles function 
	
	getAllRoles();
	
	
	
	
	
	
	
	
	
});


	 function createUser()
	 {
	 	
	 	if($("#jvalidate").valid())
	 	{
	 	
	 		var chk_role_status ="0";
	 		var chk_role_restriction ="0";
	 		var	chk_role_bank = false;
	 		
	 			if($("#role-status").is(':checked')){
	 				chk_role_status="1";
	 			}
	 			if($("#role-bank").is(':checked')){
	 				chk_role_bank="true";
	 			}	
	 		//	if($("#role-restriction").is(':checked')){
	 		//		chk_role_restriction="1";
	 		//	}	
	 	
	 	
	 	
	 	 
        $.each($("#other_bank option:selected"), function(){            
            otherBank.push($(this).val());
        });
        alert("You have selected the country - " + otherBank.join(", "));

	 	
	 	
	 		var createUserInfo = {

	 				//rolename: $("#role-name").val(),						
	 				//statusId: chk_role_status,
	 				//userId : "5",		
	 				//isrestricted :chk_role_restriction,
	 				//bankVisibility :chk_role_bank,
	 				
	 				 zipcode:"600040", 
	 				 isactive:chk_role_status,
	 				 
	 				 internalUserCode:"505605",
	 				 userCode:"505605",
	 				 firstName:$("#licence-fname").val(),
	 				 lastName:$("#licence-lname").val(),
	 				 
	 				 username : $("#licence-uname").val(),
	 				 password : $("#licence-pswd").val(),
	 				 roleId  : $("#roleId").val(),
	 				 adminId : admin_id,
	 				 
	 				 parent_bank_defoult:"1",
	 				 other_bank_defoult:"0",
	 				 
	 				 emailAddress : $("#licence-email").val(),
	 				 contactno : $("#ct-no").val(),

	 				 userPhoto: $("#usr-userLogo").val(),

	 				 bankList :[{
	 									//below hardCoded
	 					branchId : "2" // This is missed  Add here code
	 				   
	 					}]
	 					
	 			}
	 	
	 			$.ajax({
	 					method: 'post',
	 					url: '../user_ubank/create',
	 					data: JSON.stringify(createUserInfo),
	 					contentType: 'application/json',
	 					dataType:'JSON',
	 					crossDomain:'true',
	 					success: function (response) {				
	 						swal({title: "Done",text: "User Created Successfully",type: "success"},function(){window.location="manage-user.jsp";});
	 											   
	 					},

	 				 error:function(response,statusTxt,error){
	 					//alert("Failed to add record :"+response.responseJSON.statusMsg);
	 					
	 					alert("Failed to add record ");
	 					
	 					}
	 			

	 					
	 					
	 					
	 						
	 			});
	 	
	 	}	
	         

	 }
	 
function icheck_reintialize(){
	$('input').iCheck({
		checkboxClass: 'icheckbox_square-blue',
		radioClass: 'iradio_square-blue',
		increaseArea: '20%' // optional
	});
}

  



function cancl(){
	
window.location.href = "manage-user.jsp" ;	
}	


	
</script>
<!-- END JAVASCRIPTS -->
<!-- END BODY -->
</html>