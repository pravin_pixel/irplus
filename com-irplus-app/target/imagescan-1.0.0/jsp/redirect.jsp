<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<html>
<head>

<title>Redirect</title>
<script src="../irplus/resources/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="../irplus/resources/assets/global/plugins/jquery-validation/js/jquery.validate.js"></script>
</head>
<body>
<script>
$(document).ready(function(){  	 
	
	$.ajax({
		method:'get',
		url:'site',
		contentType:'application/json',
		dataType:'JSON',
		crossDomain:'true',
		success: function(response) 
		{
			
			var site = response.siteInfo.licenseInfo;
			
			if(site.remainingDays>0){
			
				location.href='jsp/login.jsp'; 					
			}else{
			
				location.href='jsp/validate-login.jsp';
			}
			
			
		        //$(location).attr('href',link);  
			
		},

		 error:function(response,statusTxt,error)
		 {
			

		 }
		});
});
</script>
</body>



</html>