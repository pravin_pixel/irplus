<!DOCTYPE html>
<html lang="en">
<jsp:include page="includes/style.jsp" />
<!-- BEGIN BODY -->
<body class="page-md login">
<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
<div class="menu-toggler sidebar-toggler">
</div>
<!-- END SIDEBAR TOGGLER BUTTON -->
<!-- BEGIN LOGO -->

<!-- END LOGO -->
<!-- BEGIN LOGIN -->
<div class="content">
	<!-- BEGIN LOGIN FORM -->
	<form class="login-form" id="jvalidate">
		<div class="logo page-logo">
			<a href="login.jsp">
						<div class="text-center">
						<span class="logoimg"><img id="siteLogo" src=""  class="logo-default"></span>
						</div>
						<span class="logo-sub" id="site-fname"></span>
			</a>
		</div>
		<div class="alert alert-danger display-hide">
			<button class="close" data-close="alert"></button>
			<span>
			Enter any username and password. </span>
		</div>
		<div class="form-group">
			<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
			<label class="control-label">Username</label>
			<input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" name="username" id="loginuname" required autofocus />
		</div>
		<div class="form-group">
			<label class="control-label">Password</label>
			<input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" name="password" id="loginpwd" required />
		</div>
		<div class="form-actions">

			<a id="forget-password" class="forget-password pull-left" data-toggle="modal" data-target="#fgtpass">Forgot Password?</a>
			<input type="button" class="btn btn-success uppercase pull-right" onclick="loginUser()" value="Login" />
		</div>
	</form>
	<!-- END LOGIN FORM -->
</div>
<div class="copyright">
	 &copy; 2018 ImageScan
</div>
<!-- END LOGIN -->
<jsp:include page="includes/footer-js.jsp" />
<!-- Modal -->
  <div class="modal fade" id="fgtpass" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Forgot Password Request</h4>
        </div>
        <div class="modal-body">
			<form class="frgtpass-form" id="jvalidate">
				<fieldset>
					<label class="col-sm-4 nopad">Old Password</label>
					<input class="form-control" type="password" />
				</fieldset>
				<fieldset>
					<label class="col-sm-4 nopad">New Password</label>
					<input class="form-control" type="password" />
				</fieldset>
				<fieldset>
					<label class="col-sm-4 nopad">Retype New Password</label>
					<input class="form-control" type="password" />
				</fieldset>
				<div class="form-actions text-center">
					<input type="submit" class="btn" value="Change Password"/>
				</div>
			</form>
        </div>
        <div class="modal-footer">

        </div>
      </div>

    </div>
  </div>



	<!--<div class="modal fade license-modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" id="license-modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
			<span class="close-btn" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</span>
	    <div class="licensemodal-leftwrap">
				<div class="medium-title">
					Activate with License Key
				</div>
	    </div>
			<div class="license-rightwrap">
				<form role="form" id="fileForm" class="form-horizontal" action="javascript:uploadLicense()">
						<div class="">
						<div class="medium-title">Upload License File</div>
							<input id="bank-img-upload" name="licFile" type="file" multiple class="file-loading">
						</div>
				</form>
			</div>

    </div>
  </div>
</div>-->
<script>

$(document).ready(function(){

	$.ajax({
		method:'get',
		url:'../site',
		contentType:'application/json',
		dataType:'JSON',
		crossDomain:'true',
		beforeSend: function () {
			//loading();
		},
		success: function(response)
		{


			var site = response.siteInfo;

			$("#site-fname").append(site.siteName);

			if(site.siteLogo==''){
				$("#siteLogo").attr('src',"../resources/assets/irplus/sitelogo/sample-logo.jpg");
			}else{
				$("#siteLogo").attr('src',"http://tcmuonline.com:8080/fileimages/Images/SiteLogo/"+site.siteLogo);
			}

},

	 error:function(response,statusTxt,error){

		/* alert("Failed to add record :"+response.responseJSON); */
	 }
	});



});


  function loginUser()
  {
	 // validfn();
	  //alert("Login user");
	  var userInfo = {
					username: $("#loginuname").val(),
					password:$("#loginpwd").val()

				}
		//alert("userInfo :"+JSON.stringify(userInfo));
		if($('#jvalidate').valid())
			{
			$.ajax({
					method: 'post',
					url: '../login',
					data: JSON.stringify(userInfo),
					contentType: 'application/json',
					dataType:'JSON',
					crossDomain:'true',
					beforeSend: function () {
						//loading();
					},
					success: function (response) {

						/*alert("usermenu"+response.userMenus[0].menuName);//.empty();

						 $("#usermenu").empty();

						 var userMenuList = response.userMenus;

						 var menu = "<ul class='nav navbar-nav'>";

						 for (var i=0;i<userMenuList.length ;i++ )
						 {
							 alert(userMenuList[i].menuName);
							 menu+="<li>"+userMenuList[i].menuName+"</li>";
						 }

						menu +="</ul>";
						alert("menu :"+menu);

						 $("#usermenu").html(menu);*/


						location.href='index.jsp';

						//unloading();
					},

				 error:function(response,statusTxt,error)
				 {
					swal({title: "Invalid Credentials",text: "",type: "error"},function(){});

				 }
				});
			}
  }


$(window).load(function(){
//	alert("page loaded");
	$("#license-modal").modal().fadeIn();
});
  </script>
</body>
<!-- END BODY -->
</html>
