<!DOCTYPE html>
<html lang="en">
<jsp:include page="includes/style.jsp"/>
<body class="page-md">
<!-- BEGIN HEADER -->
	<div class="page-header">
		<!-- BEGIN HEADER TOP -->
		<jsp:include page="includes/header-top.jsp" />
		<!-- END HEADER TOP -->
		<!-- BEGIN HEADER MENU -->
		<jsp:include page="includes/header-menu.jsp" />
		<!-- END HEADER MENU -->
	</div>
<!-- END HEADER -->
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<div class="container">
			<!-- BEGIN PAGE TITLE -->
			<div class="col-md-6 col-sm-6 col-xs-12 page-title">
				<h1>Manage Customer Mapping </h1>
			</div>
				<div class="col-md-6 col-sm-6 col-xs-12 text-right">
							<a href="create-custmapping.jsp" class="btn orange m-t-15">
								<i class="fa fa-plus"></i>Add</a>
				</div>
			<!-- END PAGE TITLE -->
			
		</div>
	</div>
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container">
			
			<!-- BEGIN PAGE BREADCRUMB -->
			<!--<div class="col-md-12 col-sm-12 col-xs-12 form-group nopad">
			<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
			<ul class="page-breadcrumb breadcrumb">
				<li>
					<a href="#">Home</a><i class="fa fa-circle"></i>
				</li>
				
				<li class="active">
					Manage Customer Mapping
				</li>
			</ul>
			</div>
				<div class="col-md-6 col-sm-6 col-xs-12 text-right">
							<a href="create-custmapping.jsp" class="btn blue">
								<i class="fa fa-plus"></i>Add</a>
				</div>
			</div>
			</div>-->
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row margin-top-10">
				<div class="col-md-12">
					
					<!-- BEGIN PROFILE CONTENT -->
					<div class="profile-content">
						<div class="row">
							<div class="col-md-12">
				
					
					<div class="portlet light col-md-12 col-sm-12 col-xs-12">
						<div class="portlet-body">
							<div class="table-scrollable table-scrollable-borderless">
							<!-- 	<table class="table dtTable"> -->
									<table  class="table" id="manage-customermapping-id">
								<thead>
								<tr class="uppercase">
									<th>
										Customer ID
									</th>
									<th>
										 Bank
									</th>
									<th>
										 NATCHA fields
									</th>
									<th>
										 Reference Data
									</th>
									<th>
										Options
									</th>
								</tr>
								</thead>
		
								</table>
							</div>
						</div>
					</div>
					<!-- END SAMPLE FORM PORTLET-->
					
				</div>
							
						</div>
						
					</div>
					<!-- END PROFILE CONTENT -->
				</div>
			</div>
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>

<script>
$(document).ready(function(){

	$.fn.dataTable.ext.errMode = 'throw'
            $("#manage-customermapping-id").dataTable({
			ajax :{
			"url":"../customer/mapping/showAll",  
			"dataSrc":"customerMpngInfoList"
		  		},
                //"aaData": arreglo,
                 "aoColumns": [

                 
                    { "mData":"customersId"},
					{ "mData":"bankName"},	
					{ "mData":"natchafld"},
					{ "mData":"referenceData"},
				
			 		{ 
						data: null, render: function ( data, type, row ) 
						{
					
							//var urlval = 'customer/delete/'+data.customerGrpId;
						
							return  '<a  class="btn btn-xs green" data-toggle="tooltip" data-placement="right" title="Add!"><i class="fa fa-plus"></i></a><a href="view-mapping.jsp" class="btn btn-xs purple" data-toggle="tooltip" data-placement="right" title="View!"><i class="fa fa-eye"></i></a><a href="edit-group.jsp" class="btn btn-xs blue" data-toggle="tooltip" data-placement="right" title="Edit!"><i class="fa fa-edit"></i></a>'					
						
						}
						
					},
			
					
					
					
                    ], 
        "bDestroy": true
    }).fnDraw(); 
});
</script>
<!-- END PAGE CONTAINER -->
<!-- BEGIN FOOTER -->
<jsp:include page="includes/footer.jsp" />
<jsp:include page="includes/footer-js.jsp" />
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>