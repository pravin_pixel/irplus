package com.irplus.app;


import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.google.gson.Gson;
import com.irplus.core.client.fileprocessing.batchheaderrecord.BatchHeaderRecordCoreClient;
import com.irplus.core.client.fileprocessing.entrydetailrecord.EntryDetailRecordCoreClient;
import com.irplus.core.client.fileprocessing.fileheaderrecord.FileHeaderRecordCoreClient;
import com.irplus.dao.fileprocessing.fileheaderrecord.FileHeaderRecordDao;
import com.irplus.dao.hibernate.entities.IRSSite;
import com.irplus.dto.CustomerBean;
import com.irplus.dto.CustomerGroupingMngmntInfo;
import com.irplus.dto.FileTypeInfo;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.LicenseInfo;
import com.irplus.dto.fileprocessing.FileHeaderRecordBean;
import com.irplus.util.BusinessException;
import com.irplus.util.IRPlusConstants;
import com.irplus.util.IRPlusRestUtil;

@Controller
@Transactional
public class FileProcessingController {
	private static final Log LOGGER = LogFactory.getLog(FileProcessController.class);
	Gson gson = new Gson();
	
	private static final String INTERNAL_FILE="irregular-verbs-list.pdf";
    private static final String EXTERNAL_FILE_PATH="//root/Studios/2017/May_2017/VFS0070517PXSW/References/IRAPP/BANKS/IRPB0036/05032018/Lockbox/0000A600F.tiff";
    private static final String ACHPATH_FILE_PATH="";
     
	@Autowired
	private FileHeaderRecordCoreClient fileHeaderRecordCoreClient;
	
	@Autowired
	private FileHeaderRecordDao fileHeaderRecordDao;
	
	@Autowired
	private BatchHeaderRecordCoreClient batchHeaderRecordCoreClient;
	
	@Autowired
	private EntryDetailRecordCoreClient entryDetailRecordCoreClient;
	
	@Autowired
	private SessionFactory sessionFactory;

	public Session getMyCurrentSession(){
			
			Session session =null;
			
			try {	
			
				session=sessionFactory.getCurrentSession();
			
			}catch (HibernateException e) {
				LOGGER.error("Exception raised DaoImpl :: At current session creation time ::"+e);
			}		
			return session; 
		}
	

	@RequestMapping(value="/file-process/showAll/{userId}",method=RequestMethod.GET,produces="application/json")
	public ResponseEntity<String> userBankFilesList(@PathVariable String userId){

		HttpHeaders responseheaders = new HttpHeaders(); 

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails(); 
			
			try {
				
				
				irPlusResponseDetails = fileHeaderRecordCoreClient.listAllRecords(userId);
				
			} catch (BusinessException e) {

				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
				LOGGER.error("Exception raised in CustomerController :: showAllCustomers",e);
			}
			
			if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
				return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.OK);
			}
			else {
					if (!irPlusResponseDetails.isValidationSuccess()) {
						return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.CONFLICT);
					} else {
						return IRPlusRestUtil.getGenericErrorResponse();
					}
			}	
			
	}
	@RequestMapping(value="/bank-file-process/showAll",method=RequestMethod.GET,produces="application/json")
	public ResponseEntity<String> showAllfiles(){

		HttpHeaders responseheaders = new HttpHeaders(); 

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails(); 
			
			try {
				irPlusResponseDetails = fileHeaderRecordDao.bankWiseAllFiles();
				
			} catch (BusinessException e) {

				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
				LOGGER.error("Exception raised in CustomerController :: showAllCustomers",e);
			}
			
			if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
				return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.OK);
			}
			else {
					if (!irPlusResponseDetails.isValidationSuccess()) {
						return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.CONFLICT);
					} else {
						return IRPlusRestUtil.getGenericErrorResponse();
					}
			}	
			
	}
	
	//get
		@RequestMapping(value="/file-process/getBatch/{fileHeaderRecordId}",method=RequestMethod.GET,produces="application/json")
		public ResponseEntity<String> getBatch(@PathVariable String fileHeaderRecordId){
			
			LOGGER.info("FileHeaderRecordID ::" +fileHeaderRecordId);
			
			HttpHeaders responseheaders = new HttpHeaders(); 
			IRPlusResponseDetails irPlusResponseDetails = new  IRPlusResponseDetails();
			try{	
				irPlusResponseDetails = fileHeaderRecordCoreClient.getRecordByBatchId(Integer.parseInt(fileHeaderRecordId));				
			} catch (BusinessException e) {
			
				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);						
				LOGGER.error("Exception raised in FileProcessing Controller :: getRecordByBatchId",e);
			}
					
			
			if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
				return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseheaders, HttpStatus.OK);
			} else {
					if (!irPlusResponseDetails.isValidationSuccess()) {
						return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.CONFLICT);
					} else {
						return IRPlusRestUtil.getGenericErrorResponse();
					}
			}
		}
		
		@RequestMapping(value="/file-process/getEntry/{batchHeaderRecordId}",method=RequestMethod.GET,produces="application/json")
		public ResponseEntity<String> getEntry(@PathVariable String batchHeaderRecordId){
			
			LOGGER.info("batchHeaderRecordID ::" +batchHeaderRecordId);
			
			HttpHeaders responseheaders = new HttpHeaders(); 
			IRPlusResponseDetails irPlusResponseDetails = new  IRPlusResponseDetails();
			try{	
				irPlusResponseDetails = batchHeaderRecordCoreClient.getRecordByBatchId(Integer.parseInt(batchHeaderRecordId));				
			} catch (BusinessException e) {
			
				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);						
				LOGGER.error("Exception raised in FileProcessing Controller :: getRecordByBatchId",e);
			}
					
			
			if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
				return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseheaders, HttpStatus.OK);
			} else {
					if (!irPlusResponseDetails.isValidationSuccess()) {
						return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.CONFLICT);
					} else {
						return IRPlusRestUtil.getGenericErrorResponse();
					}
			}
		}
		
		@RequestMapping(value="/file-process/getEntryByID/{entryId}",method=RequestMethod.GET,produces="application/json")
		public ResponseEntity<String> getEntryByID(@PathVariable String entryId){
			
			LOGGER.info("entryId ::" +entryId);
			
			HttpHeaders responseheaders = new HttpHeaders(); 
			IRPlusResponseDetails irPlusResponseDetails = new  IRPlusResponseDetails();
			try{	
				irPlusResponseDetails = entryDetailRecordCoreClient.getRecordByEntryId(Integer.parseInt(entryId));				
			} catch (BusinessException e) {
			
				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);						
				LOGGER.error("Exception raised in FileProcessing Controller :: getRecordByBatchId",e);
			}
					
			
			if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
				return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseheaders, HttpStatus.OK);
			} else {
					if (!irPlusResponseDetails.isValidationSuccess()) {
						return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.CONFLICT);
					} else {
						return IRPlusRestUtil.getGenericErrorResponse();
					}
			}
		}
		@RequestMapping(value="/bankwise-processed-files/showAll/{bankId}",method=RequestMethod.GET,produces="application/json")
		public ResponseEntity<String> getFilesByBankId(@PathVariable String bankId){
			
			LOGGER.info("bankId ::" +bankId);
			
			HttpHeaders responseheaders = new HttpHeaders(); 
			IRPlusResponseDetails irPlusResponseDetails = new  IRPlusResponseDetails();
			try{	
				irPlusResponseDetails = fileHeaderRecordDao.getFilesByBankId(bankId);				
			} catch (BusinessException e) {
			
				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);						
				LOGGER.error("Exception raised in FileProcessing Controller :: getRecordByBatchId",e);
			}
					
			
			if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
				return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseheaders, HttpStatus.OK);
			} else {
					if (!irPlusResponseDetails.isValidationSuccess()) {
						return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.CONFLICT);
					} else {
						return IRPlusRestUtil.getGenericErrorResponse();
					}
			}
		}
		@RequestMapping(value="/download", method = RequestMethod.GET,consumes= "application/json")
	    public void downloadFile(HttpServletResponse response, @RequestBody String requestInputData) throws IOException {
	     
	        File file = null;
	         
	     /*   if(type.equalsIgnoreCase("internal")){
	            ClassLoader classloader = Thread.currentThread().getContextClassLoader();
	            file = new File(classloader.getResource(INTERNAL_FILE).getFile());
	        }else{
	         
	        //} */ 
	         
	        
	        
	        String fileUrl=gson.fromJson(requestInputData.toString(),null);
	        
	        String fileName=fileUrl.replace("\\", "/");
	        	
	        System.out.println("file name"+fileName);

	        
	        file = new File(fileName);
	        if(!file.exists()){
	            String errorMessage = "Sorry. The file you are looking for does not exist";
	            System.out.println(errorMessage);
	            OutputStream outputStream = response.getOutputStream();
	            outputStream.write(errorMessage.getBytes(Charset.forName("UTF-8")));
	            outputStream.close();
	            return;
	        }
	         
	        String mimeType= URLConnection.guessContentTypeFromName(file.getName());
	        if(mimeType==null){
	            System.out.println("mimetype is not detectable, will take default");
	            mimeType = "application/octet-stream";
	        }
	         
	        System.out.println("mimetype : "+mimeType);
	         
	        response.setContentType(mimeType);
	        
	         
	        /* "Content-Disposition : inline" will show viewable types [like images/text/pdf/anything viewable by browser] right on browser 
	            while others(zip e.g) will be directly downloaded [may provide save as popup, based on your browser setting.]*/
	        response.setHeader("Content-Disposition", String.format("inline; filename=\"" + file.getName() +"\""));
	 
	         
	        /* "Content-Disposition : attachment" will be directly download, may provide save as popup, based on your browser setting*/
	        //response.setHeader("Content-Disposition", String.format("attachment; filename=\"%s\"", file.getName()));
	         
	        response.setContentLength((int)file.length());
	 
	        InputStream inputStream = new BufferedInputStream(new FileInputStream(file));
	 
	        //Copy bytes from source to destination(outputstream in this example), closes both streams.
	        FileCopyUtils.copy(inputStream, response.getOutputStream());
	    }
	 
		
		
		 
		 @RequestMapping(value = "/fileprocess/upload/{fileType}", method = RequestMethod.POST, headers = "content-type=multipart/*", produces = {"application/json"})
		    public ResponseEntity<String> branchLogoUpload(MultipartHttpServletRequest request,
		    		  @RequestParam(value = "fileType") String fileType,@RequestParam(value="licFile",required = false) MultipartFile[] uploadingFiles) throws Exception {
				
				HttpHeaders responseHeaders = new HttpHeaders();

				IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
				String sitePath=null;		
				try
				{
		
				
				
				String filePath = null;
				
				
				
				String hql = "from IRSSite site where site.isactive=?";
				Query query = getMyCurrentSession().createQuery(hql);
				query.setParameter(0,1);

				List<IRSSite> siteList = query.list();
			
				if(siteList.size()>0){
					
					 sitePath=siteList.get(0).getRootFolderPath();
				}
			
				
				if(fileType.equals("ACH")){
					
					for(MultipartFile licFile : uploadingFiles) {
					
					filePath=sitePath+File.separator+"ACH-Files"+File.separator+licFile.getOriginalFilename();
				
					File newFile = new File(filePath);
					
					licFile.transferTo(newFile);
					LOGGER.info("File name : "+licFile.getOriginalFilename()+" File Type : " + fileType +" Uploaded Successfully");
					irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
					}	
				}else if(fileType.equals("LOCKBOX")){
				
					for(MultipartFile licFile : uploadingFiles) {
					
					filePath=sitePath+File.separator+"LOCKBOX-Files"+File.separator+licFile.getOriginalFilename();
						File newFile = new File(filePath);
						LOGGER.info("File name : "+licFile.getOriginalFilename()+" File Type : " + fileType +" Uploaded Successfully");

					licFile.transferTo(newFile);
					
					irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
					}
				}
				
				
			//	filePath ="C://IRPLUS//images//bankBranchLogo//"+branchLogo.getOriginalFilename();
				
			//	LOGGER.info("fileUploaded::" + branchLogo +" original File name : "+branchLogo.getOriginalFilename()+"filepath : "+filePath);
				
				
				
				//irPlusResponseDetails.setLogoPath("/resources/assets/irplus/branchlogos/"+branchLogo.getOriginalFilename());
				}
				
				catch (Exception e) {
					LOGGER.error("Exception in fileUpload", e);
					irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
					irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				}
				if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) 
				{ 			  			// Successfully Request Creation

					return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.OK);
				} 
				else 
				{
					if(!irPlusResponseDetails.isValidationSuccess())
					{
						return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
					}
					else
					{
						return IRPlusRestUtil.getGenericErrorResponse();
					}
				}
		    }
			

		 @RequestMapping(value="/master/getData/Fileprocess/{siteId}/{userId}", method=RequestMethod.GET, produces="application/json")
		 public ResponseEntity<String> getMasterData(@PathVariable String siteId,@PathVariable Integer userId)
			{
			 
				HttpHeaders responseHeaders = new HttpHeaders();

				IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

				try
				{
				
				irPlusResponseDetails = fileHeaderRecordCoreClient.getMasterDataChart(userId,siteId);
					irPlusResponseDetails.setValidationSuccess(false);
					

				} catch (Exception e) {
					LOGGER.error("Exception in listBank", e);
					irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
					irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				}
				if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) 
				{ 			  			// Successfully Request Creation

					return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.OK);
				} 
				else 
				{
					if(!irPlusResponseDetails.isValidationSuccess())
					{
						return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
					}
					else
					{
						return IRPlusRestUtil.getGenericErrorResponse();
					}
				}
			}
		 
/*		 
		 @RequestMapping(value="/file-process/showAll/datewise",method=RequestMethod.POST,produces="application/json")
			
		 public ResponseEntity<String> DaywiseFiles(@ModelAttribute FileHeaderRecordBean currentInfo){
				LOGGER.info("weeeeeeqqqqqqqqqqeeeee");
				HttpHeaders responseheaders = new HttpHeaders(); 

				IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails(); 
					
					try {
						
						LOGGER.info("qqqqqqqqqqqqqq");
						LOGGER.info("yyyyyyyyyyyy"+currentInfo.getProcessedDate());
						LOGGER.info("rrrrrrrrrrrrr"+currentInfo.getFiletypeId());
						irPlusResponseDetails = fileHeaderRecordCoreClient.DaywiseFiles(currentInfo);
						
					} catch (BusinessException e) {
						LOGGER.info("yyyyyyyyyqwww");
						irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
						irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
						LOGGER.error("Exception raised in CustomerController :: showAllCustomers",e);
					}
					
					if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
						return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.OK);
					}
					else {
							if (!irPlusResponseDetails.isValidationSuccess()) {
								return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.CONFLICT);
							} else {
								return IRPlusRestUtil.getGenericErrorResponse();
							}
					}	
					
			}*/
			
		 
		 
		 @RequestMapping(value="/file-process/new/datewise",method=RequestMethod.POST,produces="application/json")
			
		 public ResponseEntity<String> DaywiseFiles(@ModelAttribute CustomerGroupingMngmntInfo currentInfo){
					
				HttpHeaders responseheaders = new HttpHeaders(); 

				IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails(); 
					
					try {
						
						
						irPlusResponseDetails = fileHeaderRecordCoreClient.DaywiseFiles(currentInfo);
						
					} catch (BusinessException e) {

						irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
						irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
						LOGGER.error("Exception raised in CustomerController :: showAllCustomers",e);
					}
					
					if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
						return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.OK);
					}
					else {
							if (!irPlusResponseDetails.isValidationSuccess()) {
								return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.CONFLICT);
							} else {
								return IRPlusRestUtil.getGenericErrorResponse();
							}
					}	
					
			}
		 
		 
		 
		 @RequestMapping(value="/file-process/group/datewise",method=RequestMethod.POST,produces="application/json")
			
		 public ResponseEntity<String> DaywiseFilesGroup(@ModelAttribute CustomerGroupingMngmntInfo currentInfo){
					
				HttpHeaders responseheaders = new HttpHeaders(); 

				IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails(); 
					
					try {
						
						
						irPlusResponseDetails = fileHeaderRecordCoreClient.DaywiseFilesGroup(currentInfo);
						
					} catch (BusinessException e) {

						irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
						irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
						LOGGER.error("Exception raised in CustomerController :: showAllCustomers",e);
					}
					
					if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
						return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.OK);
					}
					else {
							if (!irPlusResponseDetails.isValidationSuccess()) {
								return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.CONFLICT);
							} else {
								return IRPlusRestUtil.getGenericErrorResponse();
							}
					}	
					
			}
		 	 
		 @RequestMapping(value="/fileprocess/bankwise/files",method=RequestMethod.POST,produces="application/json")
			
		 public ResponseEntity<String> BankwiseFileList(@ModelAttribute CustomerGroupingMngmntInfo fileinfo){
					
				HttpHeaders responseheaders = new HttpHeaders(); 

				IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails(); 
					
					try {
						
						irPlusResponseDetails = fileHeaderRecordCoreClient.BankwiseFileList(fileinfo);
						
					} catch (BusinessException e) {

						irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
						irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
						LOGGER.error("Exception raised in CustomerController :: showAllCustomers",e);
					}
					
					if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
						return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.OK);
					}
					else {
							if (!irPlusResponseDetails.isValidationSuccess()) {
								return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.CONFLICT);
							} else {
								return IRPlusRestUtil.getGenericErrorResponse();
							}
					}	
					
			}

		 @RequestMapping(value="/file-audit/showAll",method=RequestMethod.GET,produces="application/json")
			public ResponseEntity<String> showFileLogs(){

				HttpHeaders responseheaders = new HttpHeaders(); 

				IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails(); 
					
					try {
						irPlusResponseDetails = fileHeaderRecordCoreClient.listfileLog();
						
					} catch (BusinessException e) {

						irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
						irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
						LOGGER.error("Exception raised in CustomerController :: showAllCustomers",e);
					}
					
					if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
						return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.OK);
					}
					else {
							if (!irPlusResponseDetails.isValidationSuccess()) {
								return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.CONFLICT);
							} else {
								return IRPlusRestUtil.getGenericErrorResponse();
							}
					}	
					
			}
		 
		 
		 @RequestMapping(value="/fileprocess/groupwise/files",method=RequestMethod.POST,produces="application/json")
			
		 public ResponseEntity<String> GroupwiseFileList(@ModelAttribute CustomerGroupingMngmntInfo Grpinfo){
					
				HttpHeaders responseheaders = new HttpHeaders(); 

				IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails(); 
					
					try {
						
						irPlusResponseDetails = fileHeaderRecordCoreClient.GroupwiseFileList(Grpinfo);
						
					} catch (BusinessException e) {

						irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
						irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
						LOGGER.error("Exception raised in CustomerController :: showAllCustomers",e);
					}
					
					if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
						return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.OK);
					}
					else {
							if (!irPlusResponseDetails.isValidationSuccess()) {
								return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.CONFLICT);
							} else {
								return IRPlusRestUtil.getGenericErrorResponse();
							}
					}	
					
			}
			
			@RequestMapping(value="/file/filter", method=RequestMethod.POST, produces= "application/json")
				@ResponseBody
				public ResponseEntity<String> filterFiles(@ModelAttribute FileTypeInfo fileListInfo)
				{
						
					HttpHeaders responseHeaders = new HttpHeaders();

					IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
					
			
					
					try
					{
						
						irPlusResponseDetails = this.fileHeaderRecordCoreClient.filterFiles(fileListInfo);
						
					} catch (BusinessException e) {
						LOGGER.error("Exception in listFiles", e);
						irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
						irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
					}
					if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) 
					{ 			  			// Successfully Request Creation

						return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.OK);
					} 
					else 
					{
						if(!irPlusResponseDetails.isValidationSuccess())
						{
							return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
						}
						else
						{
							return IRPlusRestUtil.getGenericErrorResponse();
						}
					}
				}
			
}
