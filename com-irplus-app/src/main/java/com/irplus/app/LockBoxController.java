package com.irplus.app;


import java.awt.image.RenderedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import java.util.List;

import javax.imageio.ImageIO;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


import com.irplus.dao.hibernate.entities.IRSBank;
import com.irplus.dao.hibernate.entities.IRSSite;
import com.irplus.dao.hibernate.entities.IrFPBatchHeaderRecord;
import com.irplus.dao.hibernate.entities.IrFPCCDAddendaRecord;
import com.irplus.dao.hibernate.entities.IrFPEntryDetailRecord;
import com.irplus.dao.hibernate.entities.IrFPFileHeaderRecord;
import com.irplus.dao.hibernate.entities.IrMFileTypes;
import com.irplus.dao.hibernate.entities.IrSBankBranch;
import com.irplus.dao.hibernate.entities.IrSCustomers;

import com.irplus.file.util.FileHandler;
import com.sun.media.jai.codec.ImageCodec;
import com.sun.media.jai.codec.ImageDecoder;

@Controller
@Transactional
public class LockBoxController {
	
	private static final Logger LOGGER = Logger.getLogger(LockBoxController.class);
	File error = null;
	File source = null;
	String todayDate=null;
	String bankFolder = null;
	 String lockboxFolder=null;
	 String batchNumber=null;
	 String batchFol=null;
	File src =null;
	File dest=null;
	Long bankId;
	int customerId;
	Date date = new Date();
    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
    String currentDate = null;
    String currentTime = null;
    String rootFolderPath=null;
    String imagePath=null;
    String success="success";
	@Autowired
	private SessionFactory sessionFactory;

	public Session getMyCurrentSession(){
		
		Session session =null;
		
		try {	
		
			session=sessionFactory.getCurrentSession();
		
		}catch (HibernateException e) {
			LOGGER.error("Exception raised DaoImpl :: At current session creation time ::"+e);
		}		
		return session; 
	}
	public String doTask(){
		try{
			String folders[]={"ACH-Files","WIRE-Files","LOCKBOX-Files","BANKS","ERROR-DOC","temp"};
			
			String hql = "from IRSSite site where site.isactive=?";
			Query query = getMyCurrentSession().createQuery(hql);
			query.setParameter(0,1);

			List<IRSSite> siteList = query.list();
			
			if(siteList.size()>0){
		
				rootFolderPath=siteList.get(0).getRootFolderPath();
				
				 source = new File(siteList.get(0).getRootFolderPath());
				 
				 if(source!=null){
					 
					 for(int i=0;i<folders.length;i++){
						 File file = new File(source+File.separator+folders[i]);
					        if (!file.exists()) {
					            if (file.mkdir()) {
					                //System.out.println("Directory is created!");
					            } else {
					               // System.out.println("Failed to create directory!");
					            }
					        }
						 
					 }
					 
				 }
			}
			 dest=new File(source+File.separator+"LOCKBOX-Files");
		
			        	
			        	createFolder(dest);
			      	
		    
		
		}catch(Exception e){
			e.printStackTrace();
		}
		return success;
			}
	
	public void createFolder(File filePath) {
		String bankNo=null;
		String customerNo=null;
		String fileName=null;
		
		
		try {
		  	  File[] files = filePath.listFiles();
		  	  if (files.length!= 0) {
		         for(int z=0;z<files.length;z++){
		          	 fileName=stripExtension(files[z].getName());
					 String extension= getFileExtension(files[z].getName());
					 
					 	if(extension.equals("XML")){
								File fXmlFile = new File(filePath+File.separator+files[z].getName());
								DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
								DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
								Document doc = dBuilder.parse(fXmlFile);
									
								doc.getDocumentElement().normalize();
								
								NodeList lockBoxList = doc.getElementsByTagName("Lockbox_Def");
									if(lockBoxList.getLength()>0){
										Node nNode = lockBoxList.item(0);
										Element eElement = (Element) nNode;
										customerNo=eElement.getElementsByTagName("id").item(0).getTextContent();
									}
										
								NodeList dailyBatchList = doc.getElementsByTagName("Daily_Batch_Def");
									if(dailyBatchList.getLength()>0){
										Node nNode = dailyBatchList.item(0);
										Element eElement = (Element) nNode;
										bankNo=eElement.getElementsByTagName("banknum").item(0).getTextContent();
										batchNumber=eElement.getElementsByTagName("syscntl").item(0).getTextContent();

									}
									
									if(bankNo!=null) {
									     String hql = "from IRSBank bank where bank.IRBankCode=? and bank.isactive=1";
									     Query query = getMyCurrentSession().createQuery(hql);
									     query.setParameter(0,bankNo);
									     List<IRSBank> bankList = query.list();
									     	
									     if(bankList.size()>0){
										  	
								     		  String qry = "from IrSCustomers customer where customer.bankCustomerCode=?";
								              Query cust = getMyCurrentSession().createQuery(qry);
								                    cust.setParameter(0,customerNo);
								              List<IrSCustomers> customerList = cust.list();
								                   if (customerList.size() > 0){
								                        customerId = customerList.get(0).getCustomerId().intValue();
							                            
								                         todayDate = getMonthDay();
												         bankId=bankList.get(0).getBankId();
													     bankFolder = rootFolderPath+File.separator+"BANKS"+File.separator+bankList.get(0).getBankFolder()+File.separator+todayDate;
													     imagePath=bankList.get(0).getBankFolder()+File.separator+todayDate+File.separator+"Lockbox"+File.separator+batchNumber;
													     File createBank = new File(bankFolder);
													     if(!createBank.exists()){
													       	 createBank.mkdir();
													  	 }
								                        
													     lockboxFolder=bankFolder+File.separator+"Lockbox";
															File file = new File(lockboxFolder);
																if (!file.exists()) {
																	if (file.mkdir()) {
																	//	System.out.println("Directory is created!");
																	} else {
																//		System.out.println("Failed to create directory!");
																	}
																}
																
													     batchFol=lockboxFolder+File.separator+batchNumber;
															File batchFolder = new File(batchFol);
																if (!batchFolder.exists()) {
																	if (batchFolder.mkdir()) {
																//		System.out.println("Directory is created!");
																	} else {
																//		System.out.println("Failed to create directory!");
																	}
																}
														
								                        
								                        
								                        //start
								                        
								                        IrFPFileHeaderRecord fhr = new IrFPFileHeaderRecord();
								                        IrMFileTypes fileType = new IrMFileTypes(); 
								                        IrSBankBranch branch= new IrSBankBranch();
									               		IRSBank bank = new IRSBank();
									               		IrSCustomers customer = new IrSCustomers();
									               		IrFPBatchHeaderRecord bhr = null;
									               		IrFPEntryDetailRecord pedr = null;
									               		IrFPEntryDetailRecord transaction = null;
									               		IrFPCCDAddendaRecord payment = null;
								                        
								                  //file info      
									               		
									               	  currentDate = getCurrentDay();
									                  currentTime = getCurrentTime();
									               		
									               		
									                    bank.setBankId(bankId);
														fhr.setBank(bank);
														fhr.setProcessedDate(currentDate);
														fhr.setProcessedTime(currentTime);
														fhr.setAchFileName(fileName);
														fhr.setIsProcessed(1);
														fileType.setFiletypeid((long)130);
														fhr.setFileType(fileType);
														
								                  //batch info    
												NodeList nList = doc.getElementsByTagName("Daily_Batch_Def");
												for (int temp = 0; temp < nList.getLength(); temp++) {
													bhr = new IrFPBatchHeaderRecord();
													Node nNode = nList.item(temp);
													if(nNode.getNodeType() == Node.ELEMENT_NODE) {
														Element eElement = (Element) nNode;
														if(eElement.getElementsByTagName("syscntl").getLength()>0){
																batchNumber=eElement.getElementsByTagName("syscntl").item(0).getTextContent();
																bhr.setBatchNumber(batchNumber);
														}else{
															bhr.setBatchNumber("");
														}
														if(eElement.getElementsByTagName("procday").getLength()>0){
															bhr.setCompanyDescriptiveDate(eElement.getElementsByTagName("procday").item(0).getAttributes().getNamedItem("date").getNodeValue());
														}else{
															bhr.setCompanyDescriptiveDate("");
														}
														if(eElement.getElementsByTagName("balnamnt").getLength()>0){
															 bhr.setBatchAmount(eElement.getElementsByTagName("balnamnt").item(0).getTextContent());    		
														}else{
															bhr.setBatchAmount("0.00");
														}
										    			if(eElement.getElementsByTagName("trancnt").getLength()>0){
															bhr.setTransactionCount(eElement.getElementsByTagName("trancnt").item(0).getTextContent());
														}else{
															bhr.setTransactionCount("0");
														}
														if(eElement.getElementsByTagName("itemcnt").getLength()>0){
															bhr.setItemCount(eElement.getElementsByTagName("itemcnt").item(0).getTextContent());
														}else{
															bhr.setItemCount("0");
														}
								    					if(eElement.getElementsByTagName("paymcnt").getLength()>0){
															bhr.setPaymentCount(eElement.getElementsByTagName("paymcnt").item(0).getTextContent());
														}else{
															bhr.setPaymentCount("0");
														}
								    					if(eElement.getElementsByTagName("remtcnt").getLength()>0){
															bhr.setRemittanceCount(eElement.getElementsByTagName("remtcnt").item(0).getTextContent());
										    			}else{
															bhr.setRemittanceCount("0");
														}
														if(eElement.getElementsByTagName("scancnt").getLength()>0){
															bhr.setScandocCount(eElement.getElementsByTagName("scancnt").item(0).getTextContent());
														}else{
															bhr.setScandocCount("0");
														}
										    					
										    		}
										    				
										  NodeList transData = doc.getElementsByTagName("Trans_Data");
										  	 for(int j = 0; j < transData.getLength();j++) {
										  		 if(j==0) {
										    		 NodeList transDef = ((Element)transData.item(j)).getElementsByTagName("Trans_Def");
										        	  // 	System.out.println("transaction : "+j);				


										    		    			
										        					 
										        					 
										        					
										        					 NodeList itemData = ((Element)transData.item(j)).getElementsByTagName("Item_Data");
										        					 int count=0;
										    	    					 for(int i = 0; i < itemData.getLength(); i++) {
										    	    						 NodeList itemDef = ((Element)itemData.item(i)).getElementsByTagName("Item_Def");
										    	    						
										    	    						 
										    	    						 NodeList commonData = ((Element)itemData.item(i)).getElementsByTagName("Common_Data");
										    	    						 
										    	    						 for (int k = 0; k < commonData.getLength(); k++) {
										    	    							 
										    	    							 NodeList imagedataList = ((Element)commonData.item(k)).getElementsByTagName("ImageData_List");
										    		    							 for(int l = 0; l < imagedataList.getLength(); l++) {
										    		    								 
										    		    								 NodeList imageDataDef = ((Element)imagedataList.item(l)).getElementsByTagName("ImageData_Def");
										    		    								
										    		    								 for(int m = 0; m < imageDataDef.getLength(); m++) {
										    		    									 Node nNode1 = imageDataDef.item(m);
										    		    									 if (nNode1.getNodeType() == Node.ELEMENT_NODE) {

										    		 	    			    					Element eElements = (Element) nNode1;

										    		 	    			    					
										    		 	    			    				/*	System.out.println("imagoffset : " + eElements.getElementsByTagName("imagoffset").item(k).getTextContent());
										    		 	    			    					System.out.println("imaglen : " + eElements.getElementsByTagName("imaglen").item(k).getTextContent());
										    		 	    			    					System.out.println("img flag : " +eElements.getElementsByTagName("imagflag").item(k).getTextContent());
										    		 	    			    				*/	
										    		 	    			    					String imageOffset=eElements.getElementsByTagName("imagoffset").item(k).getTextContent();
										    		 	    			    					String imageLength=eElements.getElementsByTagName("imaglen").item(k).getTextContent();
										    		 	    			    					
										    		 	    			    					String imgFlag=eElements.getElementsByTagName("imagflag").item(k).getTextContent();
										    		 	    			    						
										    		 	    			    					String[] animals = imgFlag.split(" ");
										    		 	    			    				      int animalIndex = 1;
										    		 	    			    				  
										    		 	    			    				      for (String imageType : animals) {
										    		 	    			    				    	  
										    		 	    			    				    	  if(imageType.equals("FRNT")){
										    		 	    			    				    	try {
										    				 	    			    			        	
										    				 	    			    			        	String hex =imageOffset;
										    				 	    			    						 Integer hexa = Integer.parseInt(hex,16);
										    				 	    			    			            // convert file to byte[]
										    				 	    			    			           
										    				 	    			    						byte[] bFile = readBytesFromFile(filePath+File.separator+fileName+".FCI");
										    				 	    			    			            byte[] imagedata = FileHandler.readByte(bFile,hexa,Integer.parseInt(imageLength));
										    				 	    			    			            String tiffOutputFile=batchFol+File.separator+imageOffset+"F.tiff";
										    				 	    			    			            String tiffPath=imagePath+File.separator+imageOffset+"F.tiff";

										    				 	    			    			            FileOutputStream imageOutFile = new FileOutputStream(tiffOutputFile);
										    				 	    			    			            imageOutFile.write(imagedata);
										    																imageOutFile.close();
										    																
										    																String pngOutputFile=batchFol+File.separator+imageOffset+"F.png";
										    																 String pngPath=imagePath+File.separator+imageOffset+"F.png";
										    																FileOutputStream pngOutFile = new FileOutputStream(pngOutputFile);
										    					 	    			    			          
										    					 	    			    			        InputStream is = new ByteArrayInputStream(imagedata);
										    					 	    			    	                ImageDecoder decoder = ImageCodec.createImageDecoder("tiff",is,null);
										    					 	    			    	                RenderedImage ri = decoder.decodeAsRenderedImage();
										    					 	    			    	                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
										    					 	    			    	                ImageIO.write(ri,"PNG",outputStream);
										    					 	    			    	                outputStream.writeTo(pngOutFile);
										    					 	    			    	                outputStream.close();
										    					 	    			    	                pngOutFile.close();
										    					 	    			    	                
										    					 	    			    	               
										    					 	    			    	                	bhr.setBatchImage(tiffPath);
										    					 	    			    	                	bhr.setBatchPNG(pngPath);
										    					 	    			    	                
										    				 	    			    			        } catch (IOException e) {
										    				 	    			    			            e.printStackTrace();
										    				 	    			    			        }
										    		 	    			    				    		 
										    		 	    			    				    	  	}if(imageType.equals("REAR")){
										    		 	    			    				    			try {
										    					 	    			    			        	
										    					 	    			    			        	String hex =imageOffset;
										    					 	    			    						 Integer hexa = Integer.parseInt(hex,16);
										    					 	    			    			            // convert file to byte[]
										    					 	    			    						  byte[] bFile = readBytesFromFile(filePath+File.separator+fileName+".RCI");
										    					 	    			    		        
										    					 	    			    			            byte[] imagedata = FileHandler.readByte(bFile,hexa,Integer.parseInt(imageLength));
										    					 	    			    			             String tiffOutputFile=batchFol+File.separator+imageOffset+"R.tiff";
										    					 	    			    			             String tiffPath= imagePath+File.separator+imageOffset+"R.tiff";
										    					 	    			    			            FileOutputStream imageOutFile = new FileOutputStream(tiffOutputFile);
										    					 	    			    			            imageOutFile.write(imagedata);
										    																	String pngOutputFile=batchFol+File.separator+imageOffset+"R.png";
										    																	String pngOutput=imagePath+File.separator+imageOffset+"R.png";
										    					 	    			    			            FileOutputStream png = new FileOutputStream(pngOutputFile);
										    					 	    			    			            
										    					 	    			    			            imageOutFile.close();
										    					 	    			    			            
										    					 	    			    			            InputStream is = new ByteArrayInputStream(imagedata);
										    						 	    			    	                ImageDecoder decoder = ImageCodec.createImageDecoder("tiff",is,null);
										    						 	    			    	                RenderedImage ri = decoder.decodeAsRenderedImage();
										    						 	    			    	                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
										    						 	    			    	                ImageIO.write(ri,"PNG",outputStream);
										    						 	    			    	                outputStream.writeTo(png);
										    						 	    			    	                outputStream.close();
										    						 	    			    	                				 	    			    			            
										    					 	    			    			            png.close();
										    					 	    			    			           bhr.setRearTIFF(tiffPath);
										    					 	    			    			          bhr.setRearPNG(pngOutput);
										    					 	    			    			        } catch (IOException e) {
										    					 	    			    			            e.printStackTrace();
										    					 	    			    			        }
										    		 	    			    				    	  }
										    		 	    			    				         animalIndex++;
										    		 	    			    				      }
										    		 	    			    				      
										    		 	    			    				      
										    		 	    			    				   

										    		 	    			    				}
										    		    									 
										    		    								 }
										    		    								 
										    		    							 }
										    	    						 
										    	    						 }
										    	    						 count++;
										    								   
										        				 }
										    					 
										    					 
										    	    					// System.out.println("total count : "+count);
										    					 }else {
										    						 
										    						 transaction = new IrFPEntryDetailRecord();
										        					 NodeList transDef = ((Element)transData.item(j)).getElementsByTagName("Trans_Def");
										        					 
										        				   //	System.out.println("transaction : "+j);				

										        					 for (int k = 0; k < transDef.getLength(); k++) {
										    							int payCount=0;
										    							int remCount=0;
										    							int scanCount=0;
										    			    				Node transNode = transDef.item(k);
										    			    				
										    			    				if (transNode.getNodeType() == Node.ELEMENT_NODE) {

										    			    					Element transElement = (Element) transNode;

										    			    					
										    			    					if(transElement.getElementsByTagName("numpaym").getLength()>0){
										    			    						payCount=Integer.parseInt(transElement.getElementsByTagName("numpaym").item(0).getTextContent());
										    			    					}
										    			    					if(transElement.getElementsByTagName("numremt").getLength()>0){
										    			    						remCount=Integer.parseInt(transElement.getElementsByTagName("numremt").item(0).getTextContent());    			
										    			    					}
										    			    					if(transElement.getElementsByTagName("numscan").getLength()>0){
										    			    						scanCount=Integer.parseInt(transElement.getElementsByTagName("numscan").item(0).getTextContent());    			
										    			    					}
										    			    					

										    			    				}
										    								transaction.setPaymentCount(Integer.toString(payCount));
										    			    				transaction.setRemittanceCount(Integer.toString(remCount));
										    			    				transaction.setScandocCount(Integer.toString(scanCount));
										    			    				transaction.setItemCount(Integer.toString(payCount+remCount));
										    						 
										    						 }
										    		    					

										    		    					
										    		    				

										    		    			
										        					 
										        					 
										        					
										        					 NodeList itemData = ((Element)transData.item(j)).getElementsByTagName("Item_Data");
										        					 int count=0;
										    	    					 for(int i = 0; i < itemData.getLength(); i++) {
										    	    						 NodeList itemDef = ((Element)itemData.item(i)).getElementsByTagName("Item_Def");
										    	    						 payment = new IrFPCCDAddendaRecord();
										    	    						 for (int k = 0; k < itemDef.getLength(); k++) {

										    	    			    				Node nNode1 = itemDef.item(k);
										    	    			    				
										    	    			    				if (nNode1.getNodeType() == Node.ELEMENT_NODE) {

										    	    			    					Element eElements = (Element) nNode1;

										    	    			    					
										    	    			    				//	System.out.println("appamt : " + eElements.getElementsByTagName("appamt").item(k).getTextContent());
										    	    			    				//	System.out.println("ticktype : " + eElements.getElementsByTagName("ticktype").item(k).getTextContent());
										    	    			    					
										    	    			    					payment.setAmount(eElements.getElementsByTagName("appamt").item(k).getTextContent());
										       			    					        payment.setEntryType(eElements.getElementsByTagName("ticktype").item(k).getTextContent());

										    	    			    				}
										    	    						 
										    	    						 }
										    	    						 NodeList commonData = ((Element)itemData.item(i)).getElementsByTagName("Common_Data");
										    	    						 
										    	    						 for (int k = 0; k < commonData.getLength(); k++) {
										    	    							 
										    	    							 NodeList imagedataList = ((Element)commonData.item(k)).getElementsByTagName("ImageData_List");
										    		    							 for(int l = 0; l < imagedataList.getLength(); l++) {
										    		    								 
										    		    								 NodeList imageDataDef = ((Element)imagedataList.item(l)).getElementsByTagName("ImageData_Def");
										    		    								
										    		    								 for(int m = 0; m < imageDataDef.getLength(); m++) {
										    		    									 Node nNode1 = imageDataDef.item(m);
										    		    									 if (nNode1.getNodeType() == Node.ELEMENT_NODE) {

										    		 	    			    					Element eElements = (Element) nNode1;

										    		 	    			    					
										    		 	    			    				/*	System.out.println("imagoffset : " + eElements.getElementsByTagName("imagoffset").item(k).getTextContent());
										    		 	    			    					System.out.println("imaglen : " + eElements.getElementsByTagName("imaglen").item(k).getTextContent());
										    		 	    			    					System.out.println("img flag : " +eElements.getElementsByTagName("imagflag").item(k).getTextContent());
										    		 	    			    				*/	
										    		 	    			    					String imageOffset=eElements.getElementsByTagName("imagoffset").item(k).getTextContent();
										    		 	    			    					String imageLength=eElements.getElementsByTagName("imaglen").item(k).getTextContent();
										    		 	    			    					
										    		 	    			    					String imgFlag=eElements.getElementsByTagName("imagflag").item(k).getTextContent();
										    		 	    			    						
										    		 	    			    					String[] animals = imgFlag.split(" ");
										    		 	    			    				      int animalIndex = 1;
										    		 	    			    				  
										    		 	    			    				      for (String imageType : animals) {
										    		 	    			    				    	  
										    		 	    			    				    	  if(imageType.equals("FRNT")){
										    		 	    			    				    	try {
										    				 	    			    			        	
										    				 	    			    			        	String hex =imageOffset;
										    				 	    			    						 Integer hexa = Integer.parseInt(hex,16);
										    				 	    			    			            // convert file to byte[]
										    				 	    			    			           
										    				 	    			    						byte[] bFile = readBytesFromFile(filePath+File.separator+fileName+".FCI");
										    				 	    			    			            byte[] imagedata = FileHandler.readByte(bFile,hexa,Integer.parseInt(imageLength));
										    				 	    			    			            String tiffOutputFile=batchFol+File.separator+imageOffset+"F.tiff";
										    				 	    			    			            String tiffPath=imagePath+File.separator+imageOffset+"F.tiff";

										    				 	    			    			            FileOutputStream imageOutFile = new FileOutputStream(tiffOutputFile);
										    				 	    			    			            imageOutFile.write(imagedata);
										    																imageOutFile.close();
										    																
										    																String pngOutputFile=batchFol+File.separator+imageOffset+"F.png";
										    																String pngfront=imagePath+File.separator+imageOffset+"F.png";
										    																
										    																FileOutputStream pngOutFile = new FileOutputStream(pngOutputFile);
										    					 	    			    			          
										    					 	    			    			        InputStream is = new ByteArrayInputStream(imagedata);
										    					 	    			    	                ImageDecoder decoder = ImageCodec.createImageDecoder("tiff",is,null);
										    					 	    			    	                RenderedImage ri = decoder.decodeAsRenderedImage();
										    					 	    			    	                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
										    					 	    			    	                ImageIO.write(ri,"PNG",outputStream);
										    					 	    			    	                outputStream.writeTo(pngOutFile);
										    					 	    			    	                outputStream.close();
										    					 	    			    	                pngOutFile.close();
										    					 	    			    	                
										    					 	    			    	                if(j==0){
										    					 	    			    	                	bhr.setBatchImage(tiffPath);
										    					 	    			    	                	bhr.setBatchPNG(pngfront);
										    					 	    			    	                }else{
										    				 	    			    			            
										    																 payment.setFciName(tiffPath);
										    																 payment.setFciPng(pngfront);
										    					 	    			    	               }
										    				 	    			    			        } catch (IOException e) {
										    				 	    			    			            e.printStackTrace();
										    				 	    			    			        }
										    		 	    			    				    		 
										    		 	    			    				    	  	}if(imageType.equals("REAR")){
										    		 	    			    				    			try {
										    					 	    			    			        	
										    					 	    			    			        	String hex =imageOffset;
										    					 	    			    						 Integer hexa = Integer.parseInt(hex,16);
										    					 	    			    			            // convert file to byte[]
										    					 	    			    						  byte[] bFile = readBytesFromFile(filePath+File.separator+fileName+".RCI");
										    					 	    			    		        
										    					 	    			    			            byte[] imagedata = FileHandler.readByte(bFile,hexa,Integer.parseInt(imageLength));
										    					 	    			    			             String tiffOutputFile=batchFol+File.separator+imageOffset+"R.tiff";
										    					 	    			    			             String tiffImage=imagePath+File.separator+imageOffset+"R.tiff";
										    					 	    			    			            FileOutputStream imageOutFile = new FileOutputStream(tiffOutputFile);
										    					 	    			    			            imageOutFile.write(imagedata);
										    																	String pngOutputFile=batchFol+File.separator+imageOffset+"R.png";
										    																	String pngFile=imagePath+File.separator+imageOffset+"R.png";
										    					 	    			    			            FileOutputStream png = new FileOutputStream(pngOutputFile);
										    					 	    			    			            
										    					 	    			    			            imageOutFile.close();
										    					 	    			    			            
										    					 	    			    			            InputStream is = new ByteArrayInputStream(imagedata);
										    						 	    			    	                ImageDecoder decoder = ImageCodec.createImageDecoder("tiff",is,null);
										    						 	    			    	                RenderedImage ri = decoder.decodeAsRenderedImage();
										    						 	    			    	                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
										    						 	    			    	                ImageIO.write(ri,"PNG",outputStream);
										    						 	    			    	                outputStream.writeTo(png);
										    						 	    			    	                outputStream.close();
										    						 	    			    	                				 	    			    			            
										    					 	    			    			            png.close();
										    																	payment.setRciName(tiffImage);
										    																	payment.setRciPng(pngFile);
										    					 	    			    			        } catch (IOException e) {
										    					 	    			    			            e.printStackTrace();
										    					 	    			    			        }
										    		 	    			    				    	  }
										    		 	    			    				         animalIndex++;
										    		 	    			    				      }
										    		 	    			    				      
										    		 	    			    				      
										    		 	    			    				   

										    		 	    			    				}
										    		    									 
										    		    								 }
										    		    								 
										    		    							 }
										    	    						 
										    	    						 }
										    	    						 count++;
										    								    payment.setIrFPEntryDetailRecord(transaction);
										    									transaction.getIrFPCCDAddendaRecord().add(payment);
										    									payment.setIrFPFileHeaderRecord(fhr);
										    									fhr.getIrFPCCDAddendaRecord().add(payment);
										        				 }
										    					  transaction.setIrFPBatchHeaderRecord(bhr);
										       					 bhr.getIrFPEntryDetailRecord().add(transaction);
										       					 transaction.setIrFPFileHeaderRecord(fhr);
										       					 fhr.getIrFPEntryDetailRecord().add(transaction);
										    					 
										    	    					// System.out.println("total count : "+count);
										    						 
										    					 }
										    				
										    			}
										    			
										    				 customer.setCustomerId(customerId);
										    				 bhr.setCustomer(customer);
										    				 bhr.setIrFPFileHeaderRecord(fhr);
										    		         fhr.getIrFPBatchHeaderRecord().add(bhr);	 
										    				 
										    				 
										    				 
										    		} 
												

												//delete file
								    			
								    			InputStream inStream = null;
								    			OutputStream outStream = null;
								    				String fileTypes[] = {"XML","FCI","RCI"};
								    				
								    				for(int i=0;i<fileTypes.length;i++){
								    				if(fileTypes[i].equals("XML")){
								    					System.out.println("im in xml");
								    		    	try{
								    		    		String moveFile=filePath+File.separator+fileName+"."+fileTypes[i];
								    		    		String moveTo=batchFol+File.separator+fileName+"."+fileTypes[i];
								    		    	    File afile =new File(moveFile);
								    		    	    File bfile =new File(moveTo);
								    		    		
								    		    	    inStream = new FileInputStream(afile);
								    		    	    outStream = new FileOutputStream(bfile);
								    		        	
								    		    	    byte[] buffer = new byte[1024];
								    		    		
								    		    	    int length;
								    		    	    //copy the file content in bytes 
								    		    	    while ((length = inStream.read(buffer)) > 0){
								    		    	  
								    		    	    	outStream.write(buffer, 0, length);
								    		    	 
								    		    	    }
								    		    	 
								    		    	    inStream.close();
								    		    	    outStream.close();
								    		    	    
								    		    	    //delete the original file
								    		    	    afile.delete();
								    		    	    
								    		    	  //  System.out.println("File is copied successful!");
								    		    	    
								    		    	}catch(IOException e){
								    		    	    e.printStackTrace();
								    		    	}
								    				}
								    				
								    				if(fileTypes[i].equals("FCI")){
								    					
								    					System.out.println("im in FCI");
								    				  	try{
								    				  		String moveFile=filePath+File.separator+fileName+"."+fileTypes[i];
									    		    		String moveTo=batchFol+File.separator+fileName+"."+fileTypes[i];
								        		    	    File afile =new File(moveFile);
								        		    	    File bfile =new File(moveTo);
								        		    		
								        		    	    inStream = new FileInputStream(afile);
								        		    	    outStream = new FileOutputStream(bfile);
								        		        	
								        		    	    byte[] buffer = new byte[1024];
								        		    		
								        		    	    int length;
								        		    	    //copy the file content in bytes 
								        		    	    while ((length = inStream.read(buffer)) > 0){
								        		    	  
								        		    	    	outStream.write(buffer, 0, length);
								        		    	 
								        		    	    }
								        		    	 
								        		    	    inStream.close();
								        		    	    outStream.close();
								        		    	    
								        		    	    //delete the original file
								        		    	    afile.delete();
								        		    	    
								        		    	 //   System.out.println("File is copied successful!");
								        		    	    
								        		    	}catch(IOException e){
								        		    	    e.printStackTrace();
								        		    	}	
								    				}if(fileTypes[i].equals("RCI")){
								    					
								    					System.out.println("im in rci");
								    				  	try{
								    				  		String moveFile=filePath+File.separator+fileName+"."+fileTypes[i];
									    		    		String moveTo=batchFol+File.separator+fileName+"."+fileTypes[i];
								        		    	    File afile =new File(moveFile);
								        		    	    File bfile =new File(moveTo);
								        		    		
								        		    	    inStream = new FileInputStream(afile);
								        		    	    outStream = new FileOutputStream(bfile);
								        		        	
								        		    	    byte[] buffer = new byte[1024];
								        		    		
								        		    	    int length;
								        		    	    //copy the file content in bytes 
								        		    	    while ((length = inStream.read(buffer)) > 0){
								        		    	  
								        		    	    	outStream.write(buffer, 0, length);
								        		    	 
								        		    	    }
								        		    	 
								        		    	    inStream.close();
								        		    	    outStream.close();
								        		    	    
								        		    	    //delete the original file
								        		    	    afile.delete();
								        		    	    
								        		    	   // System.out.println("File is copied successful!");
								        		    	    
								        		    	}catch(IOException e){
								        		    	    e.printStackTrace();
								        		    	}	
								    				}
								    				
								    				
								    				
								    				
								    				
								    				
								    				
								    				
								    				
								    				}
								    			
								    			//end of delete file				
												
													Long flag = (Long)getMyCurrentSession().save(fhr);
												//	System.out.println("data inserted into fhr" + flag);
								                
								                        
								                        
								                        
								                        
								                       //end 
													
													
													
													
													
													
													
													
								                        
								                    }//end of customer
										 }else{
										
										}
			
			
			
			
									}
									
			
									
									
									
									
									
									
									
							        
			                        
			                        
											
									    						
									
									
									
									
									
									
									
									
									
									
									
									
									
									
									
									
					    			
									
		  }else{
		  		
		  		//System.out.println("extension not equal to xml");
		  	}
		}
		
		}
	  
		}catch(Exception e) {
			
		}
	}
	
	 
	 
	 private  byte[] readBytesFromFile(String filePath) {

	        FileInputStream fileInputStream = null;
	        byte[] bytesArray = null;

	        try {

	            File file = new File(filePath);
	            bytesArray = new byte[(int) file.length()];

	            //read file into bytes[]
	            fileInputStream = new FileInputStream(file);
	            fileInputStream.read(bytesArray);

	        } catch (IOException e) {
	            e.printStackTrace();
	        } finally {
	            if (fileInputStream != null) {
	                try {
	                    fileInputStream.close();
	                } catch (IOException e) {
	                    e.printStackTrace();
	                }
	            }

	        }

	        return bytesArray;

	    }
	  String stripExtension (String str) {
	        // Handle null case specially.

	        if (str == null) return null;

	        // Get position of last '.'.

	        int pos = str.lastIndexOf(".");

	        // If there wasn't any '.' just return the string as is.

	        if (pos == -1) return str;

	        // Otherwise return the string, up to the dot.

	        return str.substring(0, pos);
	    }
	     String getFileExtension(String file) {
	    	File files= new File(file);
	        String fileName = files.getName();
	        if(fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0)
	        return fileName.substring(fileName.lastIndexOf(".")+1);
	        else return "";
	    }
	     
	     private String getMonthDay()
	     {
	       String dayMonth = "";
	       Calendar ca1 = Calendar.getInstance();
	       int iDay = ca1.get(5);
	       Date date = new Date();
	       
	       SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyy");
	       dayMonth = sdf.format(date);
	       
	       return dayMonth;
	     }
	     private String getCurrentDay()
	     {
	       String dayMonth = "";
	       Calendar ca1 = Calendar.getInstance();
	       int iDay = ca1.get(5);
	       Date date = new Date();
	       
	       SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
	       dayMonth = sdf.format(date);
	       
	       return dayMonth;
	     }
	     
	      private String getCurrentTime()
	     {
	       String dayMonth = "";
	       Calendar ca1 = Calendar.getInstance();
	       int iDay = ca1.get(5);
	       Date date = new Date();
	       
	       SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
	       dayMonth = sdf.format(date);
	       
	       return dayMonth;
	     }
	     
	     
	     
	     
}