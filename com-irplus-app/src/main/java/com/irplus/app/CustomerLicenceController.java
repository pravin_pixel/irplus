package com.irplus.app;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.google.gson.Gson;
import com.irplus.core.client.customer_licence.ICustomerLicencingCoreClient;
import com.irplus.dto.CustomerLicenceInfo;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.util.BusinessException;
import com.irplus.util.IRPlusConstants;
import com.irplus.util.IRPlusRestUtil;
import com.irplus.util.ValidationHelper;

@Controller
public class CustomerLicenceController {

	private static final Logger log = Logger.getLogger(CustomerLicenceController.class);
	
	Gson gson = new Gson();
	
	@Autowired 
	ICustomerLicencingCoreClient iCustomerLicencingCoreClient;
	
	@RequestMapping(value="/customerlicence/create",method=RequestMethod.POST,produces="appliction/json",consumes="application/json")
	public ResponseEntity<String> createCustomerLicence(@RequestBody String customerlicenceData) {
		
		log.info("Inside of CustomerLicenceControll :: CreateCustomerLicence");
		
		HttpHeaders responseHeader = new HttpHeaders();
				
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails(); 
 		 
		CustomerLicenceInfo customerlicence= gson.fromJson(customerlicenceData.toString(), CustomerLicenceInfo.class);		
		
		boolean isValidReq = ValidationHelper.isCustomerLicenceReqValid(customerlicence);
		
			if (isValidReq) {
					try {
					
						irPlusResponseDetails = iCustomerLicencingCoreClient.createCustomerLicence(customerlicence);
		
					} catch (BusinessException e) {
						
						irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
						irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
						
						log.error("Exception raised in CustomerLicenceController :: createCustomerLicence",e);
					}
				}else{
			
					irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
					irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
						
					log.error("CustomerLicenceController :: Not a Valid CustomerLicenceInfo Object ");							
				}									
			
			if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
				return new ResponseEntity<>(gson.toJson(irPlusResponseDetails),responseHeader ,HttpStatus.OK);
			} 
			else {
				if (!irPlusResponseDetails.isValidationSuccess()) {
			
					return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseHeader, HttpStatus.CONFLICT);
				
				} else {			
					return IRPlusRestUtil.getGenericErrorResponse();
				}
			}
	}

@RequestMapping(value="/customerlicence/showall",method=RequestMethod.GET,produces="application/json")
public ResponseEntity<String> showAllCustomers(){

	HttpHeaders responseheaders = new HttpHeaders(); 

	IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails(); 
		
		try {
			irPlusResponseDetails = iCustomerLicencingCoreClient.showAllCustomerLicences();			
		} catch (BusinessException e) {

			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			log.error("Exception raised in CustomerLicencingController :: showAlliCustomerLicencing",e);
		}
		
		if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
			return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseheaders, HttpStatus.OK);
		}
		else {
				if (!irPlusResponseDetails.isValidationSuccess()) {
					return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.CONFLICT);
				} else {
					return IRPlusRestUtil.getGenericErrorResponse();
				}
		}	
		
}

	//delete
	@RequestMapping(value="customerlicence/delete/{customerlicenceId}",method=RequestMethod.GET,produces="application/json")
	public ResponseEntity<String> deleteCustomerLicencingId(@PathVariable String customerlicenceId){
			
			HttpHeaders responseheaders = new HttpHeaders(); 

			IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails(); 
	 	
			boolean isValidCustomId = ValidationHelper.isValidReq(customerlicenceId);
			
			if (isValidCustomId) {
							
						try {
							irPlusResponseDetails = iCustomerLicencingCoreClient.deleteCustomerLicenceById(customerlicenceId);
							} catch (BusinessException e) {

								irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
								irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
								log.error("Exception raised in CustomerLicencingController :: deleteCustomerLicencing",e);
						
							}
			} else {
				
				irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
			}
					
			if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
				return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseheaders, HttpStatus.OK);
			}
			else {
					if (!irPlusResponseDetails.isValidationSuccess()) {
						return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.CONFLICT);
					} else {
						return IRPlusRestUtil.getGenericErrorResponse();
					}
			}			
	}

//update
@RequestMapping(value="/cutomerlicence/update",method = RequestMethod.POST,consumes = "application/json" ,produces ="application/json")
public ResponseEntity<String> updateCustomerLicence(@RequestBody String customerlicenceData){
		
		
		HttpHeaders responseheaders = new HttpHeaders(); 
		IRPlusResponseDetails irPlusResponseDetails = new  IRPlusResponseDetails();
		
		CustomerLicenceInfo customerlicence = gson.fromJson(customerlicenceData.toString(), CustomerLicenceInfo.class);
		
		boolean isValidReq = ValidationHelper.isCustomerLicenceReqValid(customerlicence);
		log.info("UpdateCustomerLicence " +customerlicence.getCustomerLicenseId());
		
				if (isValidReq) {
							try {
							
								irPlusResponseDetails = iCustomerLicencingCoreClient.updateCustomerLicence(customerlicence);
							
							} catch (BusinessException e) {
								
								irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
								irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
								log.error("Exception raised inc ustomerlicence  Controller :: getcustomerlicence",e);
							}
				} else {
					irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
					irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
				}

				
				if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
					return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseheaders, HttpStatus.OK);
				}
				else {
						if (!irPlusResponseDetails.isValidationSuccess()) {
							return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.CONFLICT);
						} else {
							return IRPlusRestUtil.getGenericErrorResponse();
						}
				}		
	}

		//get
	@RequestMapping(value="/customerlicence/showOne/{customerlicenceId}",method=RequestMethod.GET,produces="application/json")
		
	public ResponseEntity<String> showOneCustomerLicenceById(@PathVariable String customerlicenceId){
		
		log.info("Role::" + customerlicenceId);
		
		HttpHeaders responseheaders = new HttpHeaders(); 
		IRPlusResponseDetails irPlusResponseDetails = new  IRPlusResponseDetails();
		
			
		boolean isValidCustomerLicenceId = ValidationHelper.isValidReq(customerlicenceId);
		
		if (isValidCustomerLicenceId) {							
				try{										
					irPlusResponseDetails = iCustomerLicencingCoreClient.getCustomerLicenceById(customerlicenceId);				
				
				} catch (BusinessException e) {
						
					irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
					irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);						
						log.error("Exception raised in customerlicenceController :: getcustomerlicence",e);
					}
				
		} else {
			irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
			}
		if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
			return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseheaders, HttpStatus.OK);
		} else {
				if (!irPlusResponseDetails.isValidationSuccess()) {
					return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.CONFLICT);
				} else {
					return IRPlusRestUtil.getGenericErrorResponse();
				}
		}
	}
	
}