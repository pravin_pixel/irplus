package com.irplus.app;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class IRPlusInterceptor extends HandlerInterceptorAdapter {
	
	private static final Log log = LogFactory.getLog(IRPlusInterceptor.class);
	
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		log.debug("IRPlusInterceptor: REQUEST Intercepted for URI: "
				+ request.getRequestURI());
		
		if(request.getRequestURI().contains("resources/")||request.getRequestURI().equalsIgnoreCase("/imagescan/license/upload"))
		{
			return true;
		}
		else if(request.getRequestURI().equalsIgnoreCase("/imagescan/login"))
		{
			request.getSession();
			return true;
		}
		else if(request.getRequestURI().equalsIgnoreCase("/imagescan/site"))
		{
			request.getSession();
			return true;
		}
		
		else
		{
			HttpSession session = request.getSession(false);
			
			if(session!=null&&session.getAttribute("username")!=null)
			{
				log.debug("session for user :"+session.getAttribute("username"));
				return true;
			}
			else
			{
				//request.getRequestDispatcher("login.jsp").forward(request, response);
				response.sendError(HttpStatus.BAD_REQUEST.value(),"SESSION EXPIRED");
			    return true;
			}
		}
		
		
	}

}
