package com.irplus.app;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.google.gson.Gson;
import com.irplus.core.client.customer_contacts.ICustomerContactsCoreClient;
import com.irplus.dto.BankInfo;
import com.irplus.dto.CustomeContactsArrayInfo;
import com.irplus.dto.CustomerBean;
import com.irplus.dto.CustomerContactsBean;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.util.BusinessException;
import com.irplus.util.CustomerContactsValidationHelper;
import com.irplus.util.IRPlusConstants;
import com.irplus.util.IRPlusRestUtil;
import com.irplus.util.RoleValidationHelper;
import com.irplus.util.ValidationHelper;

@Controller
public class CustomerContactsController {

	Gson gson = new Gson();
	private static final Log log = LogFactory.getLog(CustomerContactsController.class);

	@Autowired
	ICustomerContactsCoreClient iCustomerContactsCoreClient;

	@RequestMapping(value = "/customercontacts/create", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
	public ResponseEntity<String> createCustomerContacts(@RequestBody String customercontactsData) {

		log.info("Inside of CustomerContactsControll :: CreateCustomerContacts");
		
		List<String> errMsgs = new ArrayList<String>();
		
		HttpHeaders responseHeader = new HttpHeaders();

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();	

			try {
				CustomerContactsBean customerContactsBean = gson.fromJson(customercontactsData.toString(),CustomerContactsBean.class);

			//	boolean isValidReq = CustomerContactsValidationHelper.isValidCustomerContactReq(customerContactsBean);

				if (customerContactsBean!=null) {
				
				irPlusResponseDetails = iCustomerContactsCoreClient.createCustomerContacts(customerContactsBean);
				
				} else {
					errMsgs.add("Validation Error : Empty Please Enter Data");
					irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
					irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
					log.error("Customer Contacts Controller :: Not a Valid Customer Contacts Object ");
				}

			} catch (BusinessException e) {
				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
				
				log.error("Exception raised in CustomerContactsController :: createCustomerContacts", e);
			}

	
		if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
			return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeader, HttpStatus.OK);
		} else {
			if (!irPlusResponseDetails.isValidationSuccess()) {
				
				return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseHeader,
						HttpStatus.CONFLICT);
			} else {
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}

	}

	// get
	@RequestMapping(value = "/customercontacts/showOne/{customercontactsId}", method = RequestMethod.GET, produces = "application/json")

	public ResponseEntity<String> showOneCustomerContactsById(@PathVariable String customercontactsId) {

		log.info("Role::" + customercontactsId);

		HttpHeaders responseheaders = new HttpHeaders();
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		CustomerBean customerBean = (CustomerBean)this.gson.fromJson(customercontactsId.toString(), CustomerBean.class);
		boolean isValidRoleId = RoleValidationHelper.isValidRoleId(customercontactsId);

		if (isValidRoleId) {
			try {

				irPlusResponseDetails = iCustomerContactsCoreClient.getCustomerContactsById(customerBean);

			} catch (BusinessException e) {

				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);

				log.error("Exception raised in customercontactscontroller :: getCustomercontacts", e);

			}

		} else {
			irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
		}
		if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {

			return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseheaders, HttpStatus.OK);
		} else {
			if (!irPlusResponseDetails.isValidationSuccess()) {
				return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseheaders,
						HttpStatus.CONFLICT);
			} else {
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}
	}

	// update
	@RequestMapping(value = "/cutomercontacts/update", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public ResponseEntity<String> updateCustomerContacts(@RequestBody String customercontactsBeanData) {

		HttpHeaders responseheaders = new HttpHeaders();
		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		CustomerContactsBean customerContactsBean = gson.fromJson(customercontactsBeanData.toString(),
				CustomerContactsBean.class);

		boolean isValidReq = CustomerContactsValidationHelper.isValidCustomerContactReq(customerContactsBean);
		log.info("UpdateCustomer " + customerContactsBean.getCustomerId());

		if (isValidReq) {
			try {

				irPlusResponseDetails = iCustomerContactsCoreClient.updateCustomerConstatnts(customerContactsBean);

			} catch (BusinessException e) {

				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
				log.error("Exception raised in Customer Controller :: getCustomer", e);
			}
		} else {
			irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
		}

		if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
			return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseheaders, HttpStatus.OK);
		} else {
			if (!irPlusResponseDetails.isValidationSuccess()) {
				return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseheaders,
						HttpStatus.CONFLICT);
			} else {
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}
	}

	// updated - CutomerContacts : 12 14 17
	
		@RequestMapping(value = "/cutmrcntacts/update", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
		public ResponseEntity<String> updateCustomerContactsOnly(@RequestBody String customercontactsBeanData){

			HttpHeaders responseheaders = new HttpHeaders();
			IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();
		
				try {
					CustomeContactsArrayInfo customeContactsArrayInfo = gson.fromJson(customercontactsBeanData.toString(),
							CustomeContactsArrayInfo.class);

				
/*					boolean isValidReq = CustomerContactsValidationHelper.isValidCustomerContactReq(customerContactsBean);
					log.info("UpdateCustomer " + customerContactsBean.getCustomerId());
*/
					
					if (customeContactsArrayInfo.getCustomerContactsBean().length!=0) {
					irPlusResponseDetails = iCustomerContactsCoreClient.updateCustomerConstactsOnly(customeContactsArrayInfo);

					} else {
						irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
						irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
					}

					
				} catch (BusinessException e) {

					irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
					irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
					log.error("Exception raised in Customer Controller :: getCustomer", e);
				}
			
			if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
				return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseheaders, HttpStatus.OK);
			} else {
				if (!irPlusResponseDetails.isValidationSuccess()) {
					return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseheaders,
							HttpStatus.CONFLICT);
				} else {
					return IRPlusRestUtil.getGenericErrorResponse();
				}
			}
		}
	
		
	// delte
	@RequestMapping(value = "customercontacts/delete/{customercontactsId}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<String> deleteCutomerContactsId(@PathVariable String customercontactsId){

		HttpHeaders responseheaders = new HttpHeaders();

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		boolean isValidCustomId = ValidationHelper.isValidReq(customercontactsId);

		if (isValidCustomId) {

			try {
				irPlusResponseDetails = iCustomerContactsCoreClient.deleteCustomerContactsById(customercontactsId);
			} catch (BusinessException e) {

				irPlusResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
				log.error("Exception raised in CutomerController :: deleteCustomercontact", e);

			}
		} else {

			irPlusResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
		}

		if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
			return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseheaders, HttpStatus.OK);
		} else {
			if (!irPlusResponseDetails.isValidationSuccess()) {
				return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseheaders,
						HttpStatus.CONFLICT);
			} else {
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}
	}

	
	@RequestMapping(value = "/customercontacts/showAll", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<String> showAllCustomerContacts() {

		HttpHeaders responseheaders = new HttpHeaders();

		IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

		try {
			irPlusResponseDetails = iCustomerContactsCoreClient.showAllCustomerContacts();

		} catch (BusinessException e) {

			irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			log.error("Exception raised in CustomerContactsController :: showAllCustomerscontacts", e);
		}

		if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)){

			return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseheaders, HttpStatus.OK);
		} else {
			if (!irPlusResponseDetails.isValidationSuccess()) {
				return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails), responseheaders,
						HttpStatus.CONFLICT);
			} else {
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}
	}

}
