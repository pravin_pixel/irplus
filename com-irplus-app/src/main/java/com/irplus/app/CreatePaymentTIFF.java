package com.irplus.app;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.DataBuffer;
import java.awt.image.MultiPixelPackedSampleModel;
import java.awt.image.SampleModel;
import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;

import javax.media.jai.JAI;
import javax.media.jai.TiledImage;

import com.irplus.dao.hibernate.entities.IrFPCCDAddendaRecord;
import com.sun.media.jai.codec.ImageCodec;
import com.sun.media.jai.codec.PNGEncodeParam;
import com.sun.media.jai.codec.TIFFEncodeParam;

public class CreatePaymentTIFF {
	
	 private ByteArrayOutputStream stream = null;
	 public CreatePaymentTIFF(String[] text)
	    {
	        TiledImage image = null;
	        Graphics2D graphics = null;
	        SampleModel model = null;
	        ImageCodec codec = null;
	        TIFFEncodeParam param = null;
	        int yIndex = 30;
	        HashMap map = null;
	        int fontSize=20;
	         
	        model = new MultiPixelPackedSampleModel(DataBuffer.TYPE_BYTE,2400,3000,1);
	        image = new TiledImage(0,0,816,336,0,0,model,ImageCodec.createGrayIndexColorModel(model,false));
	                                
	        graphics = image.createGraphics();        
	         
	        map = new HashMap();
	        map.put(RenderingHints.KEY_TEXT_ANTIALIASING,RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
	        graphics.setRenderingHints(map);
	        graphics.setFont(new Font(null,Font.BOLD,12));
	        graphics.setColor(Color.BLACK);
	         
	      //  for(int i = 0; i < text.length; i++)
	        //{
	        
	        graphics.drawString(text[0],150,20);
            graphics.drawString("Date : "+text[1],650,35);
            
            graphics.drawString("Pay to the",15,100);
            graphics.drawString("Order of",15,120);
            
            graphics.drawString(text[2],80,120);
            
            graphics.drawRect(650,130,140,40);
            graphics.drawString("$ "+text[3],660,155);
            graphics.drawString(text[4],80,155);
            graphics.setFont(new Font("Verdana",Font.BOLD,15));
            graphics.drawString(text[5],90,220);
            graphics.drawString("Memo : "+text[7],90,250);
            graphics.drawString(text[0],580,250);
            graphics.drawLine(550,260,750,260); //(x1,y1,x2,y2)
            graphics.setFont(new Font("MICR Encoding",Font.BOLD,25));
            graphics.drawString(text[5],100,300);
            graphics.drawString(text[6],250,300);
	        //}
	                
	        stream = new ByteArrayOutputStream();
	         
	        //Setting the compression to be used.
	        param = new TIFFEncodeParam();
	        param.setCompression(TIFFEncodeParam.COMPRESSION_GROUP4);
	         
	        JAI.create("encode",image,stream,"TIFF",param);
	       // JAI.create("encode",image,stream,"PNG",param);
	       
	        
	    }
	 public CreatePaymentTIFF(List<IrFPCCDAddendaRecord> text)
	    {
	        TiledImage image = null;
	        Graphics2D graphics = null;
	        SampleModel model = null;
	        ImageCodec codec = null;
	        TIFFEncodeParam param = null;
	        int yIndex = 30;
	        HashMap map = null;
	        int fontSize = 30;
	         
	        model = new MultiPixelPackedSampleModel(DataBuffer.TYPE_BYTE,2400,3000,1);
	        image = new TiledImage(0,0,2480,3508,0,0,model,ImageCodec.createGrayIndexColorModel(model,false));
	                                
	        graphics = image.createGraphics();        
	         
	        map = new HashMap();
	        map.put(RenderingHints.KEY_TEXT_ANTIALIASING,RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
	        graphics.setRenderingHints(map);
	        graphics.setFont(new Font("Arial",Font.PLAIN,fontSize));
	        graphics.setColor(Color.BLACK);
	         
	       for(int i = 0; i < text.size(); i++)
	      {
	            graphics.drawString(text.get(i).getPaymentRelatedInformation(),50,yIndex);
	            
	            
	            yIndex += fontSize+5;
	        }
	                
	        stream = new ByteArrayOutputStream();
	         
	        //Setting the compression to be used.
	        param = new TIFFEncodeParam();
	        param.setCompression(TIFFEncodeParam.COMPRESSION_GROUP4);
	         
	        JAI.create("encode",image,stream,"TIFF",param);
	       
	        
	    }
	 
	 
	 public void CreatePNG(String[] text)
	    {
	        TiledImage image = null;
	        Graphics2D graphics = null;
	        SampleModel model = null;
	        ImageCodec codec = null;
	        PNGEncodeParam param = null;
	        int yIndex = 30;
	        HashMap map = null;
	        int fontSize=25;
	         
	        model = new MultiPixelPackedSampleModel(DataBuffer.TYPE_BYTE,2400,3000,1);
	        image = new TiledImage(0,0,816,336,0,0,model,ImageCodec.createGrayIndexColorModel(model,false));
	                                
	        graphics = image.createGraphics();        
	         
	        map = new HashMap();
	        map.put(RenderingHints.KEY_TEXT_ANTIALIASING,RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
	        graphics.setRenderingHints(map);
	        graphics.setFont(new Font(null,Font.PLAIN,fontSize));
	        graphics.setColor(Color.BLACK);
	         
	      //  for(int i = 0; i < text.length; i++)
	        //{
	            graphics.drawString(text[0],50,yIndex);
	            graphics.drawString(text[1],550,yIndex);
	            graphics.drawString(text[2],50,120);
	            graphics.drawString(text[3],650,120);
	            graphics.drawString(text[4],50,150);
	            graphics.drawString(text[5],50,180);
	            graphics.drawString(text[6],550,180);
	            
	            
	            
	            yIndex += fontSize+5;
	        //}
	                
	        stream = new ByteArrayOutputStream();
	         
	        //Setting the compression to be used.
	        param = new PNGEncodeParam.Gray();
	     
	         
	       // JAI.create("encode",image,stream,"TIFF",param);
	        JAI.create("encode",image,stream,"PNG",param);
	       
	        
	    }
	 	public ByteArrayOutputStream getStream()
	    {
	        return stream;
	    }
	     
	    public void setStream(ByteArrayOutputStream stream)
	    {
	        this.stream = stream;
	    }
	    
	    public CreatePaymentTIFF(String text)
	    {
	        TiledImage image = null;
	        Graphics2D graphics = null;
	        SampleModel model = null;
	        ImageCodec codec = null;
	        TIFFEncodeParam param = null;
	        int yIndex = 30;
	        HashMap map = null;
	        int fontSize=20;
	         
	        model = new MultiPixelPackedSampleModel(DataBuffer.TYPE_BYTE,2400,3000,1);
	        image = new TiledImage(0,0,816,336,0,0,model,ImageCodec.createGrayIndexColorModel(model,false));
	                                
	        graphics = image.createGraphics();        
	         
	        map = new HashMap();
	        map.put(RenderingHints.KEY_TEXT_ANTIALIASING,RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
	        graphics.setRenderingHints(map);
	        graphics.setFont(new Font(null,Font.PLAIN,fontSize));
	        graphics.setColor(Color.BLACK);
	         
	      //  for(int i = 0; i < text.length; i++)
	        //{
	         
	            graphics.drawString("BATCH HEADER  "+text,250,160);
	           
	            
	            
	            
	           
	        //}
	                
	        stream = new ByteArrayOutputStream();
	         
	        //Setting the compression to be used.
	        param = new TIFFEncodeParam();
	        param.setCompression(TIFFEncodeParam.COMPRESSION_GROUP4);
	         
	        JAI.create("encode",image,stream,"TIFF",param);
	       // JAI.create("encode",image,stream,"PNG",param);
	       
	        
	    }
}
