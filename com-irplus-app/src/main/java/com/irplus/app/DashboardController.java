package com.irplus.app;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.irplus.core.client.dashboard.DashboardCoreClient;
import com.irplus.dto.CustomerGroupingMngmntInfo;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.util.BusinessException;
import com.irplus.util.IRPlusConstants;
import com.irplus.util.IRPlusRestUtil;
import com.irplus.util.ValidationHelper;

@Controller
public class DashboardController {
	 private static final Logger LOGGER = Logger.getLogger(DashboardController.class);

	 @Autowired
	 DashboardCoreClient dashboardCoreClient;
	 
	 Gson gson = new Gson();
	 
	 
	 @RequestMapping(value="/dashboard/getData/{userId}", method=RequestMethod.GET, produces="application/json")
		@ResponseBody
		public ResponseEntity<String> listBanks(@PathVariable String userId)
		{
	
			HttpHeaders responseHeaders = new HttpHeaders();

			IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

			
			try
			{
				boolean isValidReq = ValidationHelper.isValidReq(userId);

				if (isValidReq)
				{
					irPlusResponseDetails = this.dashboardCoreClient.getDashboard(userId);
				}
				else
				{
					irPlusResponseDetails.setValidationSuccess(false);
					irPlusResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
					irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				}
			} catch (BusinessException e) {
				LOGGER.error("Exception in listBank", e);
				irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
				irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			}
			if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) 
			{ 			  			// Successfully Request Creation

				return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.OK);
			} 
			else 
			{
				if(!irPlusResponseDetails.isValidationSuccess())
				{
					return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
				}
				else
				{
					return IRPlusRestUtil.getGenericErrorResponse();
				}
			}
		}
		
	 @RequestMapping(value="/filesToday/{userId}",method=RequestMethod.GET,produces="application/json")
		
			@ResponseBody
			public ResponseEntity<String> showAllFiles(@PathVariable Integer userId)
			{
			HttpHeaders responseheaders = new HttpHeaders(); 

			IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails(); 
				
				try {
					irPlusResponseDetails = dashboardCoreClient.listAllFiles(userId);
					
				} catch (BusinessException e) {

					irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
					irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
					LOGGER.error("Exception raised in CustomerController :: showAllCustomers",e);
				}
				
				if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
					return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.OK);
				}
				else {
						if (!irPlusResponseDetails.isValidationSuccess()) {
							return new ResponseEntity<String>(gson.toJson(irPlusResponseDetails),responseheaders, HttpStatus.CONFLICT);
						} else {
							return IRPlusRestUtil.getGenericErrorResponse();
						}
				}	
				
		}
	 
		 @RequestMapping(value="/bankwise/fileprocess/{branchId}", method=RequestMethod.GET, produces="application/json")
		 public ResponseEntity<String> showDaywiseReport(@ModelAttribute CustomerGroupingMngmntInfo user,@PathVariable Long branchId){

			
				HttpHeaders responseHeaders = new HttpHeaders();

				IRPlusResponseDetails irPlusResponseDetails = new IRPlusResponseDetails();

			
				try
				{
					
						irPlusResponseDetails = this.dashboardCoreClient.BankwiseFile(user,branchId);
				
				} catch (BusinessException e) {
					LOGGER.error("Exception in listBank", e);
					irPlusResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
					irPlusResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
				}
				if (irPlusResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) 
				{ 			  			// Successfully Request Creation

					return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.OK);
				} 
				else 
				{
					if(!irPlusResponseDetails.isValidationSuccess())
					{
						return new ResponseEntity<>(gson.toJson(irPlusResponseDetails), responseHeaders, HttpStatus.CONFLICT);
					}
					else
					{
						return IRPlusRestUtil.getGenericErrorResponse();
					}
				}
			}
}
