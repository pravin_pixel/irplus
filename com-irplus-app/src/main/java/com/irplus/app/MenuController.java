package com.irplus.app;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.google.gson.Gson;
import com.irplus.core.client.menumgmt.IrMenuMgmntCoreClient;
import com.irplus.dto.IRPlusResponseDetails;
import com.irplus.dto.StatusIdInfo;
import com.irplus.dto.menu.AddMenu;
import com.irplus.util.BusinessException;
import com.irplus.util.IRPlusConstants;
import com.irplus.util.IRPlusRestUtil;
import com.irplus.util.ValidationHelper;

@Controller
public class MenuController {
	
	private static final Logger log= Logger.getLogger(MenuController.class);

	Gson gson = new Gson();
	
	@Autowired
	IrMenuMgmntCoreClient irMenuMgmntCoreClient;
	
	// menu is adding db irrespective of menu list : blindly adding menu in db
	
	@RequestMapping(value= "/menu/create" , method = RequestMethod.POST, produces= "application/json", consumes= "application/json")
	@ResponseBody
	public ResponseEntity<String> createMenu(@RequestBody String requestInputData) { 
	
		HttpHeaders responseHeaders = new HttpHeaders();
	
		IRPlusResponseDetails menuResponseDetails=new IRPlusResponseDetails();

		AddMenu menuInfo = gson.fromJson(requestInputData.toString(), AddMenu.class);

		log.info(" inside of addmenuInfo::" + menuInfo.getMenuName());
		
		try {
		
			boolean isValidReq =ValidationHelper.isValidMenuReq(menuInfo);
		
			if(isValidReq) {
				menuResponseDetails=this.irMenuMgmntCoreClient.createMenu(menuInfo);
			}
			else {
				menuResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
				menuResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			}
			
		} catch (BusinessException e) {
			log.error("Exception in Create Menu", e);
			menuResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			menuResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		}
		
		if(menuResponseDetails.getStatusMsg().equalsIgnoreCase( IRPlusConstants.SUCCESS_MSG)) {
			// Successfully Request Creation
			return new ResponseEntity<>(gson.toJson(menuResponseDetails), responseHeaders, HttpStatus.OK);
			
		}
		else
		{
			if(!menuResponseDetails.isValidationSuccess()) 
			{
				return new ResponseEntity<>(gson.toJson(menuResponseDetails), responseHeaders, HttpStatus.CONFLICT);
			}
			else
			{
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}
	}
			
	
	//validate menu :  available menu in db not store if not it will store db 
	
	@RequestMapping(value= "/menu/add" , method = RequestMethod.POST, produces= "application/json", consumes= "application/json")
	@ResponseBody
	public ResponseEntity<String> validateMenuAndAdd(@RequestBody String requestInputData){ 
		
		HttpHeaders responseHeaders = new HttpHeaders();
	
		IRPlusResponseDetails menuResponseDetails=new IRPlusResponseDetails();
		AddMenu menuInfo = gson.fromJson(requestInputData.toString(), AddMenu.class);

		log.info("addmenuInfo::" + menuInfo.getMenuName());
		try {
			
			boolean isValidReq = ValidationHelper.isValidMenuReq(menuInfo);
		
			if(isValidReq) {
				menuResponseDetails=this.irMenuMgmntCoreClient.createMenuWithoutDpct(menuInfo);
			}
			else {
				menuResponseDetails.setValidationSuccess(false);
				menuResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
				menuResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			}
			
		} catch (BusinessException e) {
			log.error("Exception in Create Menu", e);
			menuResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			menuResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		}
		
		if(menuResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
			// Successfully Request Creation
			return new ResponseEntity<>(gson.toJson(menuResponseDetails), responseHeaders, HttpStatus.OK);
		}
		else
		{
			if(!menuResponseDetails.isValidationSuccess()) 
			{
				return new ResponseEntity<>(gson.toJson(menuResponseDetails), responseHeaders, HttpStatus.CONFLICT);
			}
			else
			{
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}
	}
	
	
/* Status Modifying Active and Inactive	*/
	
	@RequestMapping(value="/menu/status/{menuid}/{status}",method=RequestMethod.GET, produces="application/json")
	@ResponseBody
	public ResponseEntity<String> statusMenu(@PathVariable String menuid , @PathVariable String status)
	{
		HttpHeaders responseHeaders = new HttpHeaders();

		IRPlusResponseDetails menuResponseDetails=new IRPlusResponseDetails();
		log.info("menuid::" + menuid);
		
//		if( Integer.parseInt(status)==0 || Integer.parseInt(status)==1){
		
			Integer status1 = Integer.parseInt(status);
		
//		}else if(Boolean.parseBoolean(status)== true || Boolean.parseBoolean(status)== false){
		
//			boolean status1 =Boolean.parseBoolean(status);
//		}
		
		StatusIdInfo sid = new StatusIdInfo();
		sid.setId(Integer.parseInt(menuid));
		sid.setStatus(status1);
		try
		{
		boolean isValidReq1 = ValidationHelper.isValidReq(menuid);
		boolean isValidReq2 = ValidationHelper.isValidReq(menuid);
		log.debug("given no is isValidReq :" + isValidReq1 );
			if (isValidReq1 && isValidReq2 )
			{
		//		menuResponseDetails=this.irMenuMgmntCoreClient.deleteMenu(menuid);
				menuResponseDetails=this.irMenuMgmntCoreClient.updateMenuStatusPrm(sid);
				menuResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			}
			else
			{
				menuResponseDetails.setValidationSuccess(false);
				menuResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
				menuResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			}
		} catch (BusinessException e) {

			log.error("Exception in deleteMenu", e);
			menuResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			menuResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		}
		if (menuResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) 
		{ 			  			// Successfully Request Creation

			return new ResponseEntity<>(gson.toJson(menuResponseDetails), responseHeaders, HttpStatus.OK);
		} 
		else 
		{
			if(!menuResponseDetails.isValidationSuccess())
			{
				return new ResponseEntity<>(gson.toJson(menuResponseDetails), responseHeaders, HttpStatus.CONFLICT);
			}
			else
			{
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}
	}

	
	
//close
	@RequestMapping(value="/menu/delete/{menuid}",method=RequestMethod.GET, produces="application/json")
	@ResponseBody
	public ResponseEntity<String> deleteMenu(@PathVariable String menuid)
	{
		HttpHeaders responseHeaders = new HttpHeaders();

		IRPlusResponseDetails menuResponseDetails=new IRPlusResponseDetails();
		log.info("menuid::" + menuid);
		try
		{
		boolean isValidReq = ValidationHelper.isValidReq(menuid);
		
		System.out.println("given no is isValidReq :" + isValidReq);
			if (isValidReq)
			{
		//		menuResponseDetails=this.irMenuMgmntCoreClient.deleteMenu(menuid);
				menuResponseDetails=this.irMenuMgmntCoreClient.deleteMenu(menuid);
				menuResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			}
			else
			{
				menuResponseDetails.setValidationSuccess(false);
				menuResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
				menuResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			}
		} catch (BusinessException e) {

			log.error("Exception in deleteMenu", e);
			menuResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			menuResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		}
		if (menuResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) 
		{ 			  			// Successfully Request Creation

			return new ResponseEntity<>(gson.toJson(menuResponseDetails), responseHeaders, HttpStatus.OK);
		} 
		else 
		{
			if(!menuResponseDetails.isValidationSuccess())
			{
				return new ResponseEntity<>(gson.toJson(menuResponseDetails), responseHeaders, HttpStatus.CONFLICT);
			}
			else
			{
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}
	}
	
	
	@RequestMapping(value= "/menu/update", method=RequestMethod.POST, produces="application/json", consumes="application/json")
	@ResponseBody
	public ResponseEntity<String> updateMenu(@RequestBody String requestInputData)
	{
		HttpHeaders responseHeaders = new HttpHeaders();
		
		IRPlusResponseDetails menuResponseDetails=new IRPlusResponseDetails();
		AddMenu addMenuInfo=(AddMenu)this.gson.fromJson(requestInputData.toString(), AddMenu.class);	
	
		log.info("Menu info :: "+addMenuInfo.getMenuName());
		try {			
				boolean isValidReq = ValidationHelper.isValidMenuReq(addMenuInfo);	
				
				if (isValidReq) {
				
					menuResponseDetails=this.irMenuMgmntCoreClient.updateMenu(addMenuInfo);
				} 
				else {
					menuResponseDetails.setValidationSuccess(false);
					menuResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
					menuResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);			
				}
			
			} catch (BusinessException e) {
				
				log.error("Exception in update Menu",e);				
				menuResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
				menuResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			}
		if (menuResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
			// successfully Response create
			
			return new ResponseEntity<>(gson.toJson(menuResponseDetails), responseHeaders, HttpStatus.OK);
			
		} else {
			
			if (!menuResponseDetails.isValidationSuccess()) {
				
				return new ResponseEntity<>(gson.toJson(menuResponseDetails), responseHeaders,HttpStatus.CONFLICT);
			} 
			else
			{
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}		
	}
	
	
	@RequestMapping(value="/menu/list",method=RequestMethod.GET,produces="application/json")
	@ResponseBody
	public ResponseEntity<String> showMenuList()
	{	
		HttpHeaders responseHeaders = new HttpHeaders();
		
		IRPlusResponseDetails menuResponseDetails=new IRPlusResponseDetails();
		
		try {
									
			menuResponseDetails=this.irMenuMgmntCoreClient.findAllMenus();
			
			if (!menuResponseDetails.getMenus().isEmpty()) {
				log.debug("Menu list show :: inside controller");
				menuResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
				menuResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
			} 
			else {
				menuResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
				menuResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);			
			}
		
		} catch (BusinessException e) {
			
			log.error("Exception in update Menu",e);				
			menuResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			menuResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		}
		
		
		if (menuResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
			// successfully Response create
			
			return new ResponseEntity<>(gson.toJson(menuResponseDetails), responseHeaders, HttpStatus.OK);
			
		} else {
			
			if (!menuResponseDetails.isValidationSuccess()) {
				
				return new ResponseEntity<>(gson.toJson(menuResponseDetails), responseHeaders,HttpStatus.CONFLICT);
			} 
			else
			{
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}
	}
	
	
	@RequestMapping(value="/menu/show/{menuid}", method=RequestMethod.GET, produces="application/json")
	@ResponseBody
	public ResponseEntity<String> showOneMenu(@PathVariable String menuid){
		
		HttpHeaders responseHeaders = new HttpHeaders();
		IRPlusResponseDetails menuResponseDetails=new IRPlusResponseDetails();		
		log.info("menuid::" + menuid);
		try
		{
		boolean isValidReq = ValidationHelper.isValidReq(menuid);
			if (isValidReq)
			{				
				menuResponseDetails=this.irMenuMgmntCoreClient.getMenu(menuid);
				menuResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
			}
			else
			{
				menuResponseDetails.setValidationSuccess(false);
				menuResponseDetails.setStatusMsg(IRPlusConstants.VALIDATION_ERR_MSG);
				menuResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
			}
		}
		catch (BusinessException e) {
			
			log.error("Exception in deleteMenu", e);
			menuResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			menuResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		}		
		if (menuResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) 
		{ 			  			// Successfully Request Creation
			
			return new ResponseEntity<>(gson.toJson(menuResponseDetails), responseHeaders, HttpStatus.OK);
		} 
		else 
		{
			if(!menuResponseDetails.isValidationSuccess())
			{
				return new ResponseEntity<>(gson.toJson(menuResponseDetails), responseHeaders, HttpStatus.CONFLICT);
			}
			else
			{
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}
	}
	

	@RequestMapping(value="/menu/list/active",method=RequestMethod.GET,produces="application/json")
	@ResponseBody
	public ResponseEntity<String> showMenuListIsActive()
	{	
		HttpHeaders responseHeaders = new HttpHeaders();
		
		IRPlusResponseDetails menuResponseDetails=new IRPlusResponseDetails();
		
		try {
									
			menuResponseDetails=this.irMenuMgmntCoreClient.getAllActiveMenus();
			
			if (!menuResponseDetails.getMenus().isEmpty()) {
				log.debug("Menu list show :: inside controller");
				menuResponseDetails.setStatusMsg(IRPlusConstants.SUCCESS_MSG);
				menuResponseDetails.setStatusCode(IRPlusConstants.SUCCESS_CODE);
			} 
			else {
				menuResponseDetails.setStatusMsg(IRPlusConstants.NO_RECORD_FOUND_MSG);
				menuResponseDetails.setStatusCode(IRPlusConstants.NO_RECORD_FOUND_CODE);			
			}
		
		} catch (BusinessException e) {
			
			log.error("Exception in update Menu",e);				
			menuResponseDetails.setStatusMsg(IRPlusConstants.ERR_MSG);
			menuResponseDetails.setStatusCode(IRPlusConstants.ERR_CODE);
		}
		
		if (menuResponseDetails.getStatusMsg().equalsIgnoreCase(IRPlusConstants.SUCCESS_MSG)) {
			// successfully Response create
			
			return new ResponseEntity<>(gson.toJson(menuResponseDetails), responseHeaders, HttpStatus.OK);
			
		} else {
			
			if (!menuResponseDetails.isValidationSuccess()) {				
				return new ResponseEntity<>(gson.toJson(menuResponseDetails), responseHeaders,HttpStatus.CONFLICT);
			} 
			else
			{
				return IRPlusRestUtil.getGenericErrorResponse();
			}
		}
	}
}	